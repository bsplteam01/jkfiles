﻿using System;
using System.Collections.Generic;
using JKFilesDAL;
using System.Data;
using JKFIlesEntity;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using JKFiles.WebApp.Entity;
using System.Net.Mail;

namespace JKFilesBAL
{
    public class BAL
    {
        //call getFileSize from Data Access Layer
        DAL dbObj = new DAL();

        #region customerDetails  by Archana
        public List<CustomerDetailsEntity> getCustomerList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerList();
            List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
            {
                Cm_Cust_Name = item.Field<string>("cm_cust_name"),
                Cm_Cust_Id = item.Field<string>("cm_cust_id")
            }).ToList();
            return customerList;
        }



        public List<CutLocationTpiMasterEntity> getCutStandards(string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCutStandards(fileTypeCode, FileSubType, fileSize, fileCutType);
            List<CutLocationTpiMasterEntity> cutStandardList = new List<CutLocationTpiMasterEntity>();

            cutStandardList = ds.Tables[0].AsEnumerable().Select(item => new CutLocationTpiMasterEntity()
            {
                vtpi_cut_std_name = item.Field<string>("vtpi_cut_std_name"),
                vtpi_cut_std_code = item.Field<string>("vtpi_cut_std_code"),
                // cutCode = item.Field<string>("tpi_cut_code"),
                // dimensionCode = item.Field<string>("tpi_dim_code"),
                // dimensionDrawingVal = item.Field<string>("tpi_dim_dwgval"),
                // versionNo = item.Field<Int16>("tpi_verno"),
                //dimensionName = item.Field<string>("tpi_dim_name")

            }).ToList();
            return cutStandardList;
        }

        public List<BrandDetailsEntity> getBrandList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getBrandList();
            List<BrandDetailsEntity> brandList = new List<BrandDetailsEntity>();

            brandList = ds.Tables[0].AsEnumerable().Select(item => new BrandDetailsEntity()
            {
                bm_brand_name = item.Field<string>("bm_brand_name"),
                bm_brand_id = item.Field<string>("bm_brand_id")
            }).ToList();
            return brandList;
        }

        public List<ShipToCountryEntity> getShipTOCountryList()
        {

            DataSet ds = new DataSet();
            ds = dbObj.getShipTOCountryList();
            List<ShipToCountryEntity> shipTOCountryList = new List<ShipToCountryEntity>();

            shipTOCountryList = ds.Tables[0].AsEnumerable().Select(item => new ShipToCountryEntity()
            {
                cm_country_name = item.Field<string>("cm_country_name"),
                cm_country_cd = item.Field<string>("cm_country_cd")
            }).ToList();
            return shipTOCountryList;
        }

        //by archana -14 May
        public string getCustomerID(string customerName)
        {
            var custId = dbObj.getCustomerID(customerName);
            return custId;
        }

        public string getBrandId(string brandName)
        {
            var brandId = dbObj.getBrandId(brandName);
            return brandId;
        }


        public string getCountryId(string countryName)
        {
            var countryId = dbObj.getCountryId(countryName);
            return countryId;
        }
        public void saveCustomerDetails(CustomerDetailsEntity custObj)
        {
            dbObj.saveCustomerDetails(custObj);
        }

        //cutSpecs tab- 16 May 2018 by Archana

        public CutSpecsEntity getCutLocations(string fileTypeCode)
        {
            CutSpecsEntity cutSpecsObj = new CutSpecsEntity();
            cutSpecsObj = dbObj.getCutLocations(fileTypeCode);
            return cutSpecsObj;
        }

        public RequestDeatilEntity getRequestId(string uniqId)
        {
            RequestDeatilEntity requestObj = new RequestDeatilEntity();

            requestObj = dbObj.getRequestId(uniqId);
            return requestObj;
        }
        public void saveCutSpecs(ProductionSKUCutSpecsEntity cutSpecsObj)
        {
            dbObj.saveCutSpecs(cutSpecsObj);
        }

        public void saveCutSpecs(CutSpecsEntity cutSpecsEntity)
        {
            dbObj.saveCutSpecs(cutSpecsEntity);
        }

        public void saveItemNum(ItemPartNumEntity itemPartNumObj)
        {
            dbObj.saveItemNum(itemPartNumObj);
        }

        public string getChartNum(StampMasterEntity stampObj)
        {
            stampObj.sm_chart_num = dbObj.getChartNum(stampObj);
            return stampObj.sm_chart_num;
        }

        public List<StampMasterEntity> getStampModeList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getStampModeList();
            List<StampMasterEntity> stampMOdelList = new List<StampMasterEntity>();

            stampMOdelList = ds.Tables[0].AsEnumerable().Select(item => new StampMasterEntity()
            {
                sm_stamp_mode = item.Field<string>("sty_desc"),
            }).ToList();
            return stampMOdelList;
        }

        public void saveStampDetailsToDispatchSKU(DispatchSKUEntity dispatchEntity)
        {
            dbObj.saveStampDetailsToDispatchSKU(dispatchEntity);
        }

        public List<StampDetailsEntity> getStampDetailsForModal(string chartNum)
        {
            List<StampDetailsEntity> stampDetails = new List<StampDetailsEntity>();
            stampDetails = dbObj.getStampDetailsForModal(chartNum);
            return stampDetails;
        }

        

        //dispatch >> handle

        public List<HandlePresenceMasterEntity> getHandelPresenceList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandelPresenceList();
            List<HandlePresenceMasterEntity> handlePresencelList = new List<HandlePresenceMasterEntity>();

            handlePresencelList = ds.Tables[0].AsEnumerable().Select(item => new HandlePresenceMasterEntity()
            {
                hp_presence_desc = item.Field<string>("hp_presence_desc"),
                //   hp_presence_code = item.Field<Byte>("hp_presence_desc")

            }).ToList();
            return handlePresencelList;
        }

        public List<HandleMasterEntity> getHandleMasterList(string fileTypeCode, string FileSubType)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandleMasterList(fileTypeCode, FileSubType);
            List<HandleMasterEntity> handleMasterList = new List<HandleMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new HandleMasterEntity()
            {
                hm_handle_type = item.Field<string>("hm_handle_type")

            }).ToList();
            return handleMasterList;
        }

        #region get Handle Subtype by Dhanashree
        public List<HandleMasterEntity> getHandleSubType(string handleType, string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandleSubType(handleType, fileTypeCode, FileSubType, fileSize);
            List<HandleMasterEntity> handleSubTypeList = new List<HandleMasterEntity>();

            handleSubTypeList = ds.Tables[0].AsEnumerable().Select(item => new HandleMasterEntity()
            {
                hm_handle_subtype = item.Field<string>("hm_handle_subtype")

            }).ToList();
            return handleSubTypeList;
        }
        #endregion

        public List<HandleMasterEntity> getHandleMasterList(string handleType, string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandleMasterList(handleType, fileTypeCode, FileSubType, fileSize);
            List<HandleMasterEntity> handleMasterList = new List<HandleMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new HandleMasterEntity()
            {
                hm_chart_num = item.Field<string>("hm_chart_num")

            }).ToList();
            return handleMasterList;
        }

        public HandleDetailsEntity getHandleDrawingDetails(int handleid)
        {
            DAL objDAL = new DAL();
            try
            {
                DataSet ds = new DataSet();
                ds = objDAL.getHandleDrawingDetails(handleid);
                HandleDetailsEntity handelDetails = new HandleDetailsEntity();
                handelDetails.HandleDetailsEntityList = ds.Tables[0].AsEnumerable().Select(item => new HandleDetailsEntity()
                {
                    hd_parm_name = item.Field<string>("hd_parm_name"),
                    hd_parm_scrnvalue = item.Field<string>("hd_parm_scrnvalue")

                }).ToList();
                //  handelDetails = objDAL.getHandleDrawingDetails(handleid);
                return handelDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public int getHandleId(string handleType)
        {
            int handleId = dbObj.getHandleId(handleType);
            return handleId;
        }

        public void saveDispatchDetailsToDispatchSKU(DispatchSKUEntity dispatchEntity)
        {
            dbObj.saveDispatchDetailsToDispatchSKU(dispatchEntity);
        }

        //dispatch wrapper getWrappingTypeList
        public List<PackingMaterialMAsterEntity> getWrappingTypeList(string usedFor)
        {
            // DataSet ds = new DataSet();
            List<PackingMaterialMAsterEntity> listObj = new List<PackingMaterialMAsterEntity>();
            listObj = dbObj.getWrappingTypeList(usedFor);

            return listObj;
        }

        public void saveWrapperTabDetails(DispatchSKUEntity dispatchEntity)
        {

            dbObj.saveWrapperTabDetails(dispatchEntity);
        }

        public int getWrapperId(string wrapperChart)
        {
            int wrapperId = dbObj.getWrapperId(wrapperChart);
            return wrapperId;
        }


        public List<WrapperDetailsEntity> getViewWrappingDetails(int wrapperId)
        {
            WrapperDetailsEntity wrapperEntity = new WrapperDetailsEntity();
            DataSet ds = new DataSet();
            ds = dbObj.getViewWrappingDetails(wrapperId);
            wrapperEntity.wrappperDetailsEntityList = ds.Tables[0].AsEnumerable().Select(item => new WrapperDetailsEntity()
            {
                wd_parm_name = item.Field<string>("wd_parm_name"),
                wd_parm_scrnvalue = item.Field<string>("wd_parm_scrnvalue")

            }).ToList();
            return wrapperEntity.wrappperDetailsEntityList;
        }
        public List<WrapperMasterEntity> getWrappingChartNums(string wrappingType, string fileTypeCode, string FileSubType, string fileSize, string custId, string brandId)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getWrappingChartNums(wrappingType, fileTypeCode, FileSubType, fileSize, custId, brandId);
            List<WrapperMasterEntity> handleMasterList = new List<WrapperMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new WrapperMasterEntity()
            {
                wm_chart_num = item.Field<string>("wm_chart_num")

            }).ToList();
            return handleMasterList;
        }

        

        public string getHandleImageName(string chartNum)
        {
            string chartImage = dbObj.getHandleImageName(chartNum);
            return chartImage;
        }


        #region get Pallet Image-Dhanashree Kolpe
        public string getPalletImageName(string chartNum)
        {
            string chartImage = dbObj.getPalletImageName(chartNum);
            return chartImage;
        }
        #endregion

        public List<InnerBoxMasterEntity> getQtyFOrInnerBox(string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            List<InnerBoxMasterEntity> handleMasterList = new List<InnerBoxMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxMasterEntity()
            {
                ib_qty_inbox = item.Field<string>("ib_qty_inbox"),
                ib_box_id = item.Field<int>("ib_box_id")
            }).ToList();
            return handleMasterList;

        }


        public void saveInnerLabel(DispatchSKUEntity disEntity)
        {
            dbObj.saveInnerLabel(disEntity);
        }

        public void saveInnerBoxDetails(DispatchSKUEntity dispatchEntity)
        {
            dbObj.saveInnerBoxDetails(dispatchEntity);
        }

        public List<InnerBoxMasterEntity> getInnerBoxChartNums(string innerBoxMaterial, string innerQuntity, string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getInnerBoxChartNums(innerBoxMaterial, innerQuntity, fileTypeCode, FileSubType, fileSize);
            List<InnerBoxMasterEntity> handleMasterList = new List<InnerBoxMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxMasterEntity()
            {
                ib_chartnum = item.Field<string>("ib_chartnum")

            }).ToList();
            return handleMasterList;
        }



        public List<OuterBoxMasterEntity> getOuterBoxChartNums(string dp_outerbox_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getOuterBoxChartNums(dp_outerbox_type);
            List<OuterBoxMasterEntity> handleMasterList = new List<OuterBoxMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new OuterBoxMasterEntity()
            {
                ob_box_chartnum = item.Field<string>("ob_box_chartnum")

            }).ToList();
            return handleMasterList;
        }

        public List<PalletMasterEntity> getPalletChartNums(string dp_pallet_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getPalletChartNums(dp_pallet_type);
            List<PalletMasterEntity> palletMasterList = new List<PalletMasterEntity>();

            palletMasterList = ds.Tables[0].AsEnumerable().Select(item => new PalletMasterEntity()
            {
                pm_pallet_chartnum = item.Field<string>("pm_pallet_chartnum")

            }).ToList();
            return palletMasterList;
        }


        public int getPalletId(string pallet_chart)
        {
            int palletId = dbObj.getPalletId(pallet_chart);
            return palletId;
        }

        public List<PalletDetailsEntity> getPalletDetials(int palletId)
        {
            PalletDetailsEntity entity = new PalletDetailsEntity();
            DataSet ds = new DataSet();
            ds = dbObj.getPalletDetials(palletId);

            entity.palletDetailsEntityList = ds.Tables[0].AsEnumerable().Select(item => new PalletDetailsEntity()
            {
                pd_parm_name = item.Field<string>("pd_parm_name"),
                pd_parm_scrnvalue = item.Field<string>("pd_parm_scrnvalue")

            }).ToList();
            return entity.palletDetailsEntityList;
        }

        public void saveOuterDetails(DispatchSKUEntity dispatchEntity)
        {
            dbObj.saveOuterDetails(dispatchEntity);
        }

        public void savePalletDetails(DispatchSKUEntity dispatchEntity)
        {
            dbObj.savePalletDetails(dispatchEntity);
        }

        public void saveOtherDetails(PartNumOtherDetailsEntity partEntity)
        {
            dbObj.saveOtherDetails(partEntity);
        }

        public void saveSetOtherDetails(PartNumOtherDetailsEntity partEntity)
        {
            dbObj.saveSetOtherDetails(partEntity);
        }
        public List<InnerBoxDetailsEntity> getInnerBoxDetails(string InnerBoxChart)
        {
            InnerBoxDetailsEntity Entity = new InnerBoxDetailsEntity();
            DataSet ds = new DataSet();
            // Entity.InnerBoxDetailsEntityList = dbObj.getInnerBoxDetails(InnerBoxChart);
            ds = dbObj.getInnerBoxDetails(InnerBoxChart);
            //InnerBoxDetailsEntity handelDetails = new InnerBoxDetailsEntity();
            Entity.InnerBoxDetailsEntityList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxDetailsEntity()
            {
                ibd_pkg_parm_name = item.Field<string>("ibd_pkg_parm_name"),
                ibd_pkg_parm_scrnvalue = item.Field<string>("ibd_pkg_parm_scrnvalue")

            }).ToList();
            //  handelDetails = objDAL.getHandleDrawingDetails

            return Entity.InnerBoxDetailsEntityList;
        }



        public string getChartNumForInnerLabel(string fileTypeCode, string FileSubType, string fileSize, string brandId, string custId, string country, string fileCutType)
        {
            string chartLabelNu = dbObj.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
            return chartLabelNu;
        }

        public List<InnerLabelDetailsEntity> getInnerLabelDetails(string chartNum)
        {
            InnerLabelDetailsEntity entity = new InnerLabelDetailsEntity();
            DataSet ds = new DataSet();
            ds = dbObj.getInnerLabelDetails(chartNum);
            //InnerBoxDetailsEntity handelDetails = new InnerBoxDetailsEntity();
            entity.InnerLabelDetailsEntityList = ds.Tables[0].AsEnumerable().Select(item => new InnerLabelDetailsEntity()
            {
                ilbd_parm_name = item.Field<string>("ilbd_parm_name"),
                ilbd_parm_scrnvalue = item.Field<string>("ilbd_parm_scrnvalue")
            }).ToList();
            return entity.InnerLabelDetailsEntityList;
        }



        public int getOuterBoxId(string chartNum)
        {
            int outerId = dbObj.getOuterBoxId(chartNum);
            return outerId;
        }
        public List<OuterDetailsEntity> getOuterDetials(int outerBoxId)
        {
            List<OuterDetailsEntity> outerList = new List<OuterDetailsEntity>();
            DataSet ds = new DataSet();
            ds = dbObj.getOuterDetials(outerBoxId);
            OuterDetailsEntity handelDetails = new OuterDetailsEntity();
            handelDetails.outerDeatialsEntityList = ds.Tables[0].AsEnumerable().Select(item => new OuterDetailsEntity()
            {
                obd_parm_name = item.Field<string>("obd_parm_name"),
                obd_parm_scrnvalue = item.Field<string>("obd_parm_scrnvalue")

            }).ToList();
            return handelDetails.outerDeatialsEntityList;
        }



        public RawMaterialMastrEntity getRawMaterialDetails(string fileTypeCode, string FileSubType, string fileSize)
        {
            RawMaterialMastrEntity entity = new RawMaterialMastrEntity();
            entity = dbObj.getRawMaterialDetails(fileTypeCode, FileSubType, fileSize);
            return entity;
        }

        public void savePackingDetailsToDispatchSKU(DispatchSKUEntity obj)
        {
            dbObj.savePackingDetailsToDispatchSKU(obj);
        }


        public List<CutLocationTpiMasterEntity> getCutLocationTpiMasterDetails(string selectedCutStandardName, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            CutLocationTpiMasterEntity cutEntity = new CutLocationTpiMasterEntity();
            DataSet ds = new DataSet();
            ds = dbObj.getCutLocationTpiMasterDetails(selectedCutStandardName, fileTypeCode, FileSubType, fileSize, fileCutType);
            CutLocationTpiMasterEntity CutDetails = new CutLocationTpiMasterEntity();
            CutDetails.cutLocationMasterEntityList = ds.Tables[0].AsEnumerable().Select(item => new CutLocationTpiMasterEntity()
            {
                EDGE = item.Field<string>("EDGE"),
                OVERCUT = item.Field<string>("OVERCUT"),
                UPCUT = item.Field<string>("UPCUT"),
                vtpi_edgecut_flg = item.Field<Boolean>("vtpi_edgecut_flg"),
                vtpi_overcut_flg = item.Field<Boolean>("vtpi_overcut_flg"),
                vtpi_parm_value = item.Field<string>("vtpi_parm_value"),
                vtpi_parm_code = item.Field<string>("vtpi_parm_code"),
                vtpi_stamphere_flg = item.Field<Boolean>("vtpi_stamphere_flg"),
                vtpi_cut_application = item.Field<string>("vtpi_cut_application"),
                vtpi_upcut_flg = item.Field<Boolean>("vtpi_upcut_flg"),
                vtpi_cut_std_name = item.Field<string>("vtpi_cut_std_name"),

                vtpi_cut_std_code = item.Field<string>("vtpi_cut_std_code")
            }).ToList();

            return CutDetails.cutLocationMasterEntityList;
        }

      


        #endregion


        #region Production Details-FileSpec-By Dhanashree


        #region sending mail alert to respective id
        public void sendingmail(string email,string body,string subject)
        {
            try
            {
                string Email = email;

                if (Email != "")
                {
                    MailMessage mail = new MailMessage();
                    mail.To.Add(Email);
                    mail.From = new MailAddress("decintell@gmail.com");
                    // mail.CC.Add("");

                    mail.Subject = subject;
                    //string Body = "Dear " + txt_oemname.Text + " " + "," + "<br/><br/>";
                    string Body = "<br/>Hello, <br/><br/> ";
                    // Body += "Selected Services are " + txt_iot.Text + " " + " " + txt_swas.Text + " " + " " + txt_did.Text + " " + " " + txt_project.Text + "<br/><br/>";
                    Body += body + "<br/><br/><br/>";
                    Body += "Thanks & Regards,<br/>";
                    Body += "Falco System" + "<br/>";


                    //mail.Body = Body;

                    //mail.IsBodyHtml = true;
                    //SmtpClient smtp = new SmtpClient();

                    //smtp.UseDefaultCredentials = true;
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    //smtp.Port = 587; 
                    //smtp.Host = "mail.decintell.in";   //-- Donot change.
                    //smtp.Port = 25;
                    //smtp.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                    ////smtp.Host = "localhost";
                    //smtp.Port = 25;
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;//--- Donot change
                    smtp.UseDefaultCredentials = true;


                    smtp.Credentials = new System.Net.NetworkCredential("decintell@gmail.com", "Brain@1003");
                    //smtp.Credentials = new System.Net.NetworkCredential("noreply@decintell.in", "fKx56f$3");

                    smtp.Send(mail);
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                throw ex;
            }
        }


        #endregion

        public void saveRawMaterial(RawMaterialMastrEntity rawm)
        {
            dbObj.saveRawMaterial(rawm);
        }


        public DataTable getDrawing(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getDrawing(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //submit from other details
        public void FinalSubmit(PartNumOtherDetailsEntity partEntity)
        {
            dbObj.FinalSubmit(partEntity);
        }



        //call getFileSize from Data Access Layer
        public DataTable getMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<FileTypeDataEntity> getftimageview(string ft_desc)
        {

            DataSet ds = new DataSet();
            ds = dbObj.getftimageview(ft_desc);
            List<FileTypeDataEntity> ReqParList = new List<FileTypeDataEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new FileTypeDataEntity()
            {
                ft_ftype_desc = item.Field<string>("FileType"),
                ft_ftype_remarks = item.Field<string>("ft_ftype_remarks"),
                ft_ftype_code = item.Field<string>("ft_ftype_code"),
                _id = item.Field<int>("_id"),
                ft_request_type = item.Field<string>("ft_request_type"),
                ft_ftype_profile_img = item.Field<string>("ft_ftype_profile_img"),
                ft_request_id = item.Field<int>("ft_request_id"),

            }).ToList();
            return ReqParList;
        }


        //Call to get sub file type
        public DataTable getSbtyprfileMasters(string flag, string filesize, string filetype)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getSubTypeMasters(flag, filesize, filetype);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        //call getFileSize from Data Access Layer
        public DataTable getfileSubType(FileSpec objFilespec)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getfileSubType(objFilespec);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }



        //call getProfiledetails from Data Access Layer
        public DataTable getProfileDetails(FileSpec objFilespec)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getProfileDetails(objFilespec);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public void UpdateFileSpec(FileSpec objFilespec)
        {
            dbObj.UpdateFileSpec(objFilespec);
        }

        public string getFileSpecIds(string Name, string flag, string UniqId)
        {
            var custId = dbObj.getFileSpecIds(Name, flag, UniqId);
            return custId;
        }

        //Master View-by Dhanashree 23.05.18n 
        //File Sub Type Master
        public DataTable getFileSubTypeMaster(FileSpec objfilespec)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getFileSubTypeMaster(objfilespec);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        #region Drawing library by Dhanashree
        //get profile Details-Dhanashree Kolpe 17.06-2018
        public DataTable get_DrawingValues(FileSpec objFilespec)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_DrawingValues(objFilespec);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        #endregion

        #region Label Library by Dhanashree
        //get profile Details-Dhanashree Kolpe 17.06-2018
        public DataTable get_LabelLibrary()
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_LabelLibrary();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        #endregion

        ////get CutSpec Master
        //public DataTable getCutSpecMaster()
        //{
        //    DAL objDAL = new DAL();
        //    try
        //    {
        //        return objDAL.getCutSpecMaster();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        objDAL = null;
        //    }
        //}

        //get Change Note approval grid-24 May 2018 by Dhanashree
        public DataTable getChangeNote(string flag, string userid)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getChangeNote(flag, userid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }



        //get Change Note approval grid-24 May 2018 by Dhanashree
        public DataTable getRequests(string flag, string user_id, string ReqNo, string pr_uniq_id)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getRequests(flag, user_id, ReqNo, pr_uniq_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        //Update Production Approval for reqNo
        public void UpdateProdApproval(string reqtype, int reqid)
        {
            dbObj.UpdateProdApproval(reqtype, reqid);
        }


        //Save Approval Messages
        public void SaveApprovalRemarks(ApprovalPreparationEntity objapprove)
        {
            dbObj.SaveApprovalRemarks(objapprove);
        }


        //Update Dispatch Approval for reqNo
        public void UpdateDispatchApproval(string reqtype, int reqid)
        {
            dbObj.UpdateDispatchApproval(reqtype, reqid);
        }

        //Overview Process-for Production by Dhanashree-26.05.2018
        public DataTable getOverviewProcess(string type, int id, string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getOverviewProcess(type, id, flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        //Update Customer Approval tab
        public void UpdateCustomerApproval(string reqtype, int reqid, string flag)
        {
            dbObj.UpdateCustomerApproval(reqtype, reqid, flag);
        }


        //get Master Lists
        public DataTable getNewMastersList(string type, int id, string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getNewMastersList(type, id, flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //Change Note Approval Remarks-30.05.2018-by Dhanashree
        //Overview Process-for Production by Dhanashree-26.05.2018
        public DataTable ApprovalRemarks(string type, int id, string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.ApprovalRemarks(type, id, flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        //Insert into ItemPart no for final approval-29.05.2018-by Dhanashree
        public void InsertforFinalApproval(string reqtype, int reqid)
        {
            dbObj.InsertforFinalApproval(reqtype, reqid);
        }


        //Update final approval-29.05.2018-by Dhanashree
        public void UpdateFinalApproval(string reqtype, int reqid, string approveflag, string remark, string status,string userid)
        {
            dbObj.UpdateFinalApproval(reqtype, reqid, approveflag, remark, status, userid);
        }


        //Update Rejection Remark-29.05.2018-by Dhanashree
        public void UpdateRejectionRemark(string reqtype, int reqid, string remark, string flag)
        {
            dbObj.UpdateRejectionRemark(reqtype, reqid, remark, flag);
        }


        public DataTable GetEmailId(string Role, string reqno, string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.GetEmailId(Role, reqno, flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        #region Edit Code for Req No
        public List<CustomerDetailsEntity> getEditCustDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getEditDetails(ReqNo, flag);
            List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
            {
                Cm_Cust_Name = item.Field<string>("pr_cust_name"),
                Cm_Cust_Id = item.Field<string>("pr_cust_id"),
                sapNo = item.Field<string>("pr_sap_no"),
                customerPartNO = item.Field<string>("pr_customer_part_no")
            }).ToList();
            return customerList;
        }


        public List<BrandDetailsEntity> getEditBrandDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getEditDetails(ReqNo, flag);
            List<BrandDetailsEntity> brandList = new List<BrandDetailsEntity>();

            brandList = ds.Tables[0].AsEnumerable().Select(item => new BrandDetailsEntity()
            {
                bm_brand_name = item.Field<string>("pr_brand_desc"),
                bm_brand_id = item.Field<string>("pr_brand_id")
            }).ToList();
            return brandList;
        }


        public List<ShipToCountryEntity> getEditCountryDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getEditDetails(ReqNo, flag);
            List<ShipToCountryEntity> countryList = new List<ShipToCountryEntity>();

            countryList = ds.Tables[0].AsEnumerable().Select(item => new ShipToCountryEntity()
            {
                cm_country_cd = item.Field<string>("pr_ship2country_id"),
                cm_country_name = item.Field<string>("pr_ship2country_name")
            }).ToList();
            return countryList;
        }


        //File Spec related edit view
        public DataSet getEditFileSpecDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getEditDetails(ReqNo, flag);

            return ds;
        }

        #endregion

        #region Clone part no by Dhanashree
        public DataTable CreateCloneReq(string pr_uniq_id, int Req_id, string flag)
        {
            return dbObj.CreateCloneReq(pr_uniq_id, Req_id, flag);
        }
        #endregion

        #region Item master Filters
        public DataTable Itemmasterfilter(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.Itemmasterfilter(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        #endregion

        #region Drawing Master
        public DataTable DrawingMaster(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.DrawingMaster(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        public void SendForApprovalNewRequest(string prod_sku, string flag, string req_no, string remark,
            string status)
        {
            dbObj.SendForApprovalNewRequest(prod_sku, flag, req_no, remark, status);
        }
        #endregion

        #region Run Batch jobs for Part no-Dhanashree
        public DataTable RunBatchJobs(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.RunBatchJobs(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        #endregion


        public DataTable Archives(string flag, string userid)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.Archives(flag, userid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }



        public DataTable AddNewStamp(StampMasterEntity stampObj)
        {
            return dbObj.AddNewStamp(stampObj);
        }

        public void AddNewHandle(HandleMasterEntity stampObj)
        {
            dbObj.AddNewHandle(stampObj);
        }


        #endregion


        #region New by Swapnil
        public DataSet gettangcolorddl()
        {
            DataSet ds = new DataSet();
            ds = dbObj.gettangcolorddl();
            return ds;
        }
        public string checknewmstrstamp(int id, string type, string fo)
        {
            string abc = dbObj.checknewmstrstamp(id, type, fo);
            return abc;
        }
        public string checknewmstr(int id, string type,string fo)
        {
           string abc= dbObj.checknewmstr(id, type,fo);
            return abc;
        }
        
        public string checkoutertype(int id, string type, string fo)
        {
            string abc = dbObj.checkoutertype(id, type, fo);
            return abc;
        }
        public string checkpallettype(int id, string type, string fo)
        {
            string abc = dbObj.checkpallettype(id, type, fo);
            return abc;
        }
        public string checkinnerboxtype(int id, string type, string fo)
        {
            string abc = dbObj.checkinnerboxtype(id, type, fo);
            return abc;
        }
        public string checkwrappertype(int id, string type, string fo)
        {
            string abc = dbObj.checkwrappertype(id, type, fo);
            return abc;
        }
        
        public DataTable checkpkgnewmstr(int id, string type, string fo)
        {
            DataTable abc = dbObj.checkpkgnewmstr(id, type, fo);
            return abc;
        }
        

        public List<FileSubTypeEntity> checknewmstrsubtye(int id, string type, string fo)
        {
            List<FileSubTypeEntity> ReqParList = new List<FileSubTypeEntity>();
            DataTable abc = new DataTable();
             abc = dbObj.checknewmstrsubtye(id, type, fo);
            if (abc.Rows.Count < 1)
            {
                abc = dbObj.checknewmstrsubtyenewsize(id, type, fo);
                if (abc.Rows.Count > 0)
                {
                    if (abc.Rows[0]["pm_fsize_code"].ToString() == "0Z")
                    {
                        ReqParList = abc.AsEnumerable().Select(item => new FileSubTypeEntity()
                        {
                            pm_fstype_code = item.Field<string>("pm_fstype_code"),
                            pm_ftype_code = item.Field<string>("pm_ftype_code"),
                            pm_fsize_code = item.Field<string>("pm_fsize_code"),
                            pm_ftype_desc = item.Field<string>("ft_ftype_desc"),
                            // pm_fsize_desc = item.Field<decimal>("fs_size_inches"),
                        }).ToList();
                    }
                }
            }
            else
            {           
                if (abc.Rows[0]["pm_fsize_code"].ToString() == "0Z")
                {
                    ReqParList = abc.AsEnumerable().Select(item => new FileSubTypeEntity()
                    {
                        pm_fstype_code = item.Field<string>("pm_fstype_code"),
                        pm_ftype_code = item.Field<string>("pm_ftype_code"),
                        pm_fsize_code = item.Field<string>("pm_fsize_code"),
                        pm_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        // pm_fsize_desc = item.Field<decimal>("fs_size_inches"),
                    }).ToList();
                }
                else
                {
                    ReqParList = abc.AsEnumerable().Select(item => new FileSubTypeEntity()
                    {
                        pm_fstype_code = item.Field<string>("pm_fstype_code"),
                        pm_ftype_code = item.Field<string>("pm_ftype_code"),
                        pm_fsize_code = item.Field<string>("pm_fsize_code"),
                        pm_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fsize_desc = item.Field<decimal>("fs_size_inches"),
                    }).ToList();
                }
            }
            return ReqParList;           
        }
        public void SetReqWithdrow(int id, string type)
        {
            dbObj.SetReqWithdrow(id, type);
        }
        public DataSet getprofileprocessCheck(int? id, string type)
        {
            DataSet ds = new DataSet();
            ds=dbObj.getprofileprocessCheck(id, type);
            return ds;
        }
        public void filesubtypeUpdate(FileSubTypeEntity model)
        {
            dbObj.filesubtypeUpdate(model);
        }
        public void filesubtypeactive(FileSubTypeEntity model)
        {
            dbObj.filesubtypeactive(model);
        }
        public string Checkinnerboxqty(int id, string type)
        {
            string a = dbObj.Checkinnerboxqty(id, type);
            return a;
        }
        public void FileSizeUpdate(FileSizeDetailsEntity model)
        {
            dbObj.FileSizeUpdate(model);
        }
        public List<ApprovalEntityModel> GetApprovalDetails(int pr_request_id, string pr_request_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetApprovalDetails(pr_request_id, pr_request_type);
            List<ApprovalEntityModel> ReqParList = new List<ApprovalEntityModel>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new ApprovalEntityModel()
            {
                Name = item.Field<string>("Name"),
                //ap_remarks = item.Field<string>("ap_remarks"),
                Status = item.Field<string>("Status"),
                Date = item.Field<DateTime?>("Date"),
                Remark = item.Field<string>("Remark"),
                CreatedBy= item.Field<string>("ap_createby")
            }).ToList();
            return ReqParList;
        }

        public List<InnerBoxDetailsEntity> getInnerBoxlistforEdit(int id, string type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getInnerBoxlistforEdit(id, type);
            List<InnerBoxDetailsEntity> ReqParList = new List<InnerBoxDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxDetailsEntity()
            {
                ib_chartnum = item.Field<string>("ib_chartnum"),
                ib_chartimg = item.Field<string>("ib_chartimg"),
                ibd_pkg_parm_name = item.Field<string>("ibd_pkg_parm_name"),
                //tpi_dim_dwgval = item.Field<string>("tpi_dim_dwgval"),
                //vtpi_cut_std_code = item.Field<string>("tpi_cut_std_code"),
                ibd_pkg_parm_scrnvalue = item.Field<string>("ibd_pkg_parm_scrnvalue"),
                ibd_request_id = item.Field<int>("ibd_request_id"),
                ibd_request_type = item.Field<string>("ibd_request_type")

            }).ToList();
            return ReqParList;
        }
        public List<CutSpecsEntity> getcutspeclistforEdit(int id, string type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getcutspeclistforEdit(id, type);
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {
                tpi_dim_name = item.Field<string>("tpi_dim_name"),
                tpi_dim_code = item.Field<string>("tpi_dim_code"),
                tpi_dim_scrnval = item.Field<string>("tpi_dim_scrnval"),
                tpi_dim_dwgval = item.Field<string>("tpi_dim_dwgval"),
                vtpi_cut_std_code = item.Field<string>("tpi_cut_std_code"),
                vtpi_cut_std_name = item.Field<string>("tpi_cut_std_name"),
                prc_request_id = item.Field<int>("tpi_request_id"),
                prc_request_type = item.Field<string>("tpi_request_type"),
                _id = item.Field<int>("_id"),
            }).ToList();
            return ReqParList;
        }
        public List<InnerLabelDetailsEntity> GetinnerlabellistforEdit(int id, string type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetinnerlabellistforEdit(id, type);
            List<InnerLabelDetailsEntity> ReqParList = new List<InnerLabelDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerLabelDetailsEntity()
            {
                ilbd_chart_num = item.Field<string>("ilb_chart_num"),
                ilbd_parm_name = item.Field<string>("ilbd_parm_name"),
                ilbd_parm_scrnvalue = item.Field<string>("ilbd_parm_scrnvalue"),

                ilbd_request_id = item.Field<int>("ilb_request_id"),
                ilbd_request_type = item.Field<string>("ilb_request_type"),

            }).ToList();
            return ReqParList;
        }
        public List<HistoryDetailEntity> GetHistoryDetails(int pr_request_id, string pr_request_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetHistoryDetails(pr_request_id, pr_request_type);
            List<HistoryDetailEntity> ReqParList = new List<HistoryDetailEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new HistoryDetailEntity()
            {
                ap_remarks_for = item.Field<string>("ap_remarks_for"),
                ap_remarks = item.Field<string>("ap_remarks"),
                ap_createby = item.Field<string>("ap_createby"),
                ap_createdate = item.Field<DateTime>("ap_createdate"),

            }).ToList();
            return ReqParList;
        }
        public int GetstampList(int i)
        { //List<StampDetailsEntity> ReqParList = new List<StampDetailsEntity>();
            int ds = dbObj.GetstampList(i);
            //ReqParList = ds.Tables[0].AsEnumerable().Select(item => new StampDetailsEntity()
            //{
            //    sd_parm_name = item.Field<string>("sd_parm_name"),
            //    sd_parm_code = item.Field<string>("sd_parm_code"),
            //    sd_parm_dwgvalue = item.Field<string>("sd_parm_dwgvalue"),
            //    sd_parm_scrnvalue = item.Field<string>("sd_parm_scrnvalue"),

            //}).ToList();
            return ds;
        }
        public List<InnerLabelDetailsEntity> GetinnerLabelList(string dp_innerlabel_chart, string pr_ftype_code, string pr_fstype_code, string pr_fsize_code, string pr_cust_id, string pr_brand_id, string pr_fcut_code, string pr_ship2country_id)
        {
            DataSet ds = new DataSet();
            // ds = dbObj.GetinnerLabelList("IP-LBL-CH-002", pr_ftype_code, pr_fstype_code, pr_fsize_code, pr_cust_id, pr_brand_id, pr_fcut_code, pr_ship2country_id);
            ds = dbObj.GetinnerLabelList(dp_innerlabel_chart, pr_ftype_code, pr_fstype_code, pr_fsize_code, pr_cust_id, pr_brand_id, pr_fcut_code, pr_ship2country_id);
            List<InnerLabelDetailsEntity> ReqParList = new List<InnerLabelDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerLabelDetailsEntity()
            {
                ilbd_parm_name = item.Field<string>("ilbd_parm_name"),
                ilbd_parm_code = item.Field<string>("ilbd_parm_code"),
                ilbd_parm_dwgvalue = item.Field<string>("ilbd_parm_dwgvalue"),
                ilbd_parm_scrnvalue = item.Field<string>("ilbd_parm_scrnvalue"),
                //sd_parm_chartimg = item.Field<string>("pm_pallet_chartimg"),
            }).ToList();
            return ReqParList;
        }
        public CostDetailsModel GetCostDetails(string partno)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetCostDetails(partno);
            CostDetailsModel ReqPar = new CostDetailsModel();
            ReqPar.othdt_prod_cost_onetm = ds.Tables[0].Rows[0]["othdt_prod_cost_onetm"].ToString();
            ReqPar.othdt_prod_cost_run = ds.Tables[0].Rows[0]["othdt_prod_cost_run"].ToString();
            ReqPar.othdt_dispatch_onetm = ds.Tables[0].Rows[0]["othdt_dispatch_onetm"].ToString();
            ReqPar.othdt_dispatch_run = ds.Tables[0].Rows[0]["othdt_dispatch_run"].ToString();
            ReqPar.othdt_req_startdt = ds.Tables[0].Rows[0]["othdt_req_startdt"].ToString();
            ReqPar.othdt_req_enddt = ds.Tables[0].Rows[0]["othdt_req_enddt"].ToString();
            ReqPar.othdt_req_enddt_current_sku = ds.Tables[0].Rows[0]["othdt_req_enddt_curr_sku"].ToString();
            return ReqPar;
        }
        public List<StampDetailsEntity> GetstampList(string dp_stamp_chart)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetstampList(dp_stamp_chart);
            List<StampDetailsEntity> ReqParList = new List<StampDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new StampDetailsEntity()
            {
                sd_parm_name = item.Field<string>("sd_parm_name"),
                sd_parm_code = item.Field<string>("sd_parm_code"),
                sd_parm_dwgvalue = item.Field<string>("sd_parm_dwgvalue"),
                sd_parm_scrnvalue = item.Field<string>("sd_parm_scrnvalue"),
                //sd_parm_chartimg = item.Field<string>("pm_pallet_chartimg"),
            }).ToList();
            return ReqParList;
        }
        public List<PalletDetailsEntity> GetPalletList(string dp_pallet_chart)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetPalletList(dp_pallet_chart);
            List<PalletDetailsEntity> ReqParList = new List<PalletDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new PalletDetailsEntity()
            {
                pd_parm_name = item.Field<string>("pd_parm_name"),
                pd_parm_code = item.Field<string>("pd_parm_code"),
                pd_parm_dwgvalue = item.Field<string>("pd_parm_dwgvalue"),
                pd_parm_scrnvalue = item.Field<string>("pd_parm_scrnvalue"),
                pm_pallet_chartimg = item.Field<string>("pm_pallet_chartimg"),
            }).ToList();
            return ReqParList;
        }
        public List<OuterChartMasterEntity> GetOuterChartlist(string dp_outerbox_chart)
        {

            DataSet ds = new DataSet();
            ds = dbObj.GetOuterChartlist(dp_outerbox_chart);
            List<OuterChartMasterEntity> ReqParList = new List<OuterChartMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new OuterChartMasterEntity()
            {
                obd_parm_name = item.Field<string>("obd_parm_name"),
                obd_parm_code = item.Field<string>("obd_parm_code"),
                obd_parm_dwgvalue = item.Field<string>("obd_parm_dwgvalue"),
                obd_parm_scrnvalue = item.Field<string>("obd_parm_scrnvalue"),
                ob_box_imgname = item.Field<string>("ob_box_imgname"),
            }).ToList();
            return ReqParList;
        }
        public List<InnerBoxDetailsEntity> getinnerboxlist(string dp_innerbox_chart, string pr_fstype_code, string pr_ftype_code, string pr_fsize_code)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getinnerboxlist(dp_innerbox_chart, pr_fstype_code, pr_ftype_code, pr_fsize_code);
            List<InnerBoxDetailsEntity> ReqParList = new List<InnerBoxDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxDetailsEntity()
            {
                ibd_pkg_parm_name = item.Field<string>("ibd_pkg_parm_name"),
                ibd_pkg_parm_code = item.Field<string>("ibd_pkg_parm_code"),
                ibd_pkg_parm_dwgvalue = item.Field<string>("ibd_pkg_parm_dwgvalue"),
                ibd_pkg_parm_scrnvalue = item.Field<string>("ibd_pkg_parm_scrnvalue"),
                ib_chartimg = item.Field<string>("ib_chartimg")
            }).ToList();
            return ReqParList;
        }
        public List<WrapperDetailsEntity> getwrappinglist(string dp_wrapping_chart)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getwrappinglist(dp_wrapping_chart);
            List<WrapperDetailsEntity> ReqParList = new List<WrapperDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new WrapperDetailsEntity()
            {
                wd_parm_name = item.Field<string>("wd_parm_name"),
                wd_parm_code = item.Field<string>("wd_parm_code"),
                wd_parm_dwgvalue = item.Field<string>("wd_parm_dwgvalue"),
                wd_parm_scrnvalue = item.Field<string>("wd_parm_scrnvalue"),
            }).ToList();
            return ReqParList;
        }
        public List<HandleDetailsEntity> gethandleimgpath(string dp_handle_chart)
        {

            DataSet ds = new DataSet();
            ds = dbObj.gethandleimgpath(dp_handle_chart);
            List<HandleDetailsEntity> ReqParList = new List<HandleDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new HandleDetailsEntity()
            {
                hm_chartimg_name = item.Field<string>("hm_chartimg_name"),
                hd_parm_name = item.Field<string>("hd_parm_name"),
                hd_parm_code = item.Field<string>("hd_parm_code"),
                hd_parm_dwgvalue = item.Field<string>("hd_parm_dwgvalue"),
                hd_parm_scrnvalue = item.Field<string>("hd_parm_scrnvalue")

            }).ToList();
            return ReqParList;
        }
        public int checkiduniqness1(string pm_fstype_code, string pm_fsize_code, string pm_ftype_code, string flag)
        {
            int i = dbObj.checkiduniqness1(pm_fstype_code, pm_fsize_code, pm_ftype_code, flag);
            return i;
        }
        public int checkiduniqnecut(int prc_request_id, string prc_request_type, string cutStandardCode, string flg)
        {
            int i = dbObj.checkiduniqnecut( prc_request_id,  prc_request_type,  cutStandardCode,  flg);
            return i;
        }
        public int checkiduniqness(string cm_Cust_Id,string flag)
        {
          int i= dbObj.checkiduniqness(cm_Cust_Id, flag);
            return i;
        }

        public List<CutSpecsEntity> getcustspeclist(string reqId)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getcustspeclist(reqId);
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {

                //prc_request_type = item.Field<string>("prc_request_type"),
                //prc_request_id = item.Field<int>("prc_request_id"),
                prc_parameter_name = item.Field<string>("prc_parameter_name"),
                prc_parameter_value = item.Field<string>("prc_parameter_value"),
                prc_upcut_flg = item.Field<Boolean>("prc_upcut_flg"),
                prc_overcut_flg = item.Field<Boolean>("prc_overcut_flg"),
                prc_edgecut_flg = item.Field<Boolean>("prc_edgecut_flg"),
                prc_upcut_value = item.Field<string>("prc_upcut_value"),
                prc_overcut_value = item.Field<string>("prc_overcut_value"),
                prc_edgecut_value = item.Field<string>("prc_edgecut_value"),
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public DataTable getdispatchdetails(int reqId)
        {
            DataTable dt = new DataTable();
            dt = dbObj.getdispatchdetails(reqId);
            return dt;
        }
        //public DataTable getdispatchdetails(string reqId)
        //{
        //    DataTable dt = new DataTable();
        //    dt = dbObj.getdispatchdetails(reqId);
        //    return dt;
        //}
        //public string getReqId(string partno)
        //{
        //    string ReqId = dbObj.getReqId(partno);
        //    return ReqId;
        //}
        public string getReqId(string Id)
        {
            string Completepartno = dbObj.getReqId(Id);
            return Completepartno;
        }
        public void newfilesizesaveave(FileSizeDetailsEntity model1)
        {
            dbObj.newfilesizesaveave(model1);
        }
        public void newWrapChartsave(WrapChartModelEntity model1)
        {
            dbObj.newWrapChartsave(model1);
        }
        public List<MasterReqParEntity> getWrappingchartList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getWrappingchartList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getinnerlableList(string c)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getinnerlableList(c);
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> _wrappingtypeMasterAdd()
        {
            DataSet ds = new DataSet();
            ds = dbObj._wrappingtypeMasterAdd();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getinnerboxTypeList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getinnerboxTypeList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getOuterchartList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getOuterchartList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getOuterTypeList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getOuterTypeList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getPalletChartList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getPalletChartList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getPalletTypeList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getPalletTypeList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public void UpdateStampMasternoapprove(StampDetailsEntity model)
        {
            dbObj.UpdateStampMasternoapprove(model);
        }
        public void UpdateStampMaster(StampDetailsEntity model)
        {
            dbObj.UpdateStampMaster(model);
        }
        public void newStampMastersave(RawMaterialMastrEntity model)
        {
            dbObj.newStampMastersave(model);
        }
        public void newBrandsave(BrandDetailsEntity model)
        {
            dbObj.newBrandsave(model);
        }
        public void NewRaw_MaterialSave(RawMaterialMastrEntity model)
        {
            dbObj.NewRaw_MaterialSave(model);
        }
        public void PalletchartSave(PalletDetailsEntity model)
        {
            dbObj.PalletchartSave(model);
        }
        public void NewWrapperChartSave(WrapChartModelEntity model)
        {
            dbObj.NewWrapperChartSave(model);
        }
        public void WrappertypeAdd(WrapperTypeEntity model)
        {
            dbObj.WrappertypeAdd(model);
        }
        public void innerlablenewSave(InnerLabelDetailsEntity model)
        {
            dbObj.innerlablenewSave(model);
        }
        public void NewinnerboxtypeSave(InnerBoxTypeEntity model)
        {
            dbObj.NewinnerboxtypeSave(model);
        }
        public void NewOutertypeSave(OutertypeEntity model)
        {
            dbObj.NewOutertypeSave(model);
        }
        public void NewOuterchartSave(OuterDetailsEntity model)
        {
            dbObj.NewOuterchartSave(model);
        }
        public void NewPallettypeSave(PallettypeEntity model)
        {
            dbObj.NewPallettypeSave(model);
        }
        public void EditInnerLabelDetails(InnerLabelDetailsEntity model)
        {
            dbObj.EditInnerLabelDetails(model);
        }
        public void EditInnerLabelDetailsNoApprove(InnerLabelDetailsEntity model)
        {
            dbObj.EditInnerLabelDetailsNoApprove(model);
        }
        public void EditWrapChartDetailsnoapprove(WrapChartModelEntity model)
        {
            dbObj.EditWrapChartDetailsnoapprove(model);
        }
        public void EditWrapChartDetails(WrapChartModelEntity model)
        {
            dbObj.EditWrapChartDetails(model);
        }
        public void EditPalletchart(PalletDetailsEntity model)
        {
            dbObj.EditPalletchart(model);
        }
        public void EditPalletchartactive(PalletDetailsEntity model)
        {
            dbObj.EditPalletchartactive(model);
        }
        public void EditOuterchartDetailsactive(OuterDetailsEntity model)
        {
            dbObj.EditOuterchartDetailsactive(model);
        }
        public void EditOuterchartDetails(OuterDetailsEntity model)
        {
            dbObj.EditOuterchartDetails(model);

        }
        public void AddHandletypeDetails(HandleDetailsEntity model)
        {
            dbObj.AddHandletypeDetails(model);
        }
        public int GetHandlestatus(string handletype)
        {
            int count = dbObj.GetHandlestatus(handletype);
            return count;
        }
        public int GetHandlestatusApprove(string handletype)
        {
            int count = dbObj.GetHandlestatusApprove(handletype);
            return count;
        }
        public void AddHandlechartDetails(HandleDetailsEntity model)
        {
            dbObj.AddHandlechartDetails(model);
        }
        public void AddHandlechartandpkgmasterDetails(HandleDetailsEntity model)
        {
            dbObj.AddHandlechartandpkgmasterDetails(model);
        }
        public void newCustomersave(masterModule5 model)
        {
            dbObj.newCustomersave(model);
        }
        public void AddInnerboxMasterandLink(InnerBoxDetailsEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            dbObj.AddInnerboxMasterandLink(model1, fileTypeCode, FileSubType, fileSize, fileCutType);
        }
        public void AddInnerLabelMasterandLink(InnerLabelDetailsEntity model, string fileTypeCode, string fileSubType, string fileSize, string fileCutType, string country, string brandId, string custId)
        {
            dbObj.AddInnerLabelMasterandLink(model, fileTypeCode, fileSubType, fileSize, fileCutType, country, brandId, custId);
        }
        public void AddInnerLabelMasterEditandLinkActive(InnerLabelDetailsEntity model)
        {
            dbObj.AddInnerLabelMasterEditandLinkActive(model);
        }
        public void AddInnerLabelMasterEditandLink(InnerLabelDetailsEntity model)
        {
            dbObj.AddInnerLabelMasterEditandLink(model);
        }
        public void EditInnerboxMaster(InnerBoxDetailsEntity model1)
        {
            dbObj.EditInnerboxMaster(model1);
        }
        public void InnerboxEditactive(InnerBoxDetailsEntity model1)
        {
            dbObj.InnerboxEditactive(model1);
        }
        public void AddCutspecsEditLink(int prc_request_id, string prc_request_type)
        {
            dbObj.AddCutspecsEditLink(prc_request_id, prc_request_type);
        }
        public void newfileSubtypesave(FileSubTypeEntity model1)
        {
            dbObj.newfileSubtypesave(model1);
        }
        public void CustomersaveApprove(int? id, string type)
        {
            dbObj.CustomersaveApprove(id, type);
        }
        public void UpdatefiletypeApprove(int? id, string type)
        {
            dbObj.UpdatefiletypeApprove(id, type);
        }
        public void UpdateStampApprove(int? id, string type,string status,string remark)
        {
            dbObj.UpdateStampApprove(id, type, status,remark);
        }
        public void UpdateStampApprove(int? id, string type)
        {
            dbObj.UpdateStampApprove(id, type);
        }

        public void UpdateInnerLabelApprove(int? id, string type)
        {
            dbObj.UpdateInnerLabelApprove(id, type);
        }
        public void UpdateWrapperApprove(int? id, string type)
        {
            dbObj.UpdateWrapperApprove(id, type);
        }
        public void UpdatePalletApprove(int? id, string type)
        {
            dbObj.UpdatePalletApprove(id, type);
        }
        public void UpdateHandleApprove(int? id, string type)
        {
            dbObj.UpdateHandleApprove(id, type);
        }
        public void UpdateOuterApprove(int? id, string type)
        {
            dbObj.UpdateOuterApprove(id, type);
        }
        public void UpdateRawMaterialApprove(int? id, string type)
        {
            dbObj.UpdateRawMaterialApprove(id, type);
        }
        public void UpdatefileSubtypeApprove(int? id, string type)
        {
            dbObj.UpdatefileSubtypeApprove(id, type);
        }
        public void UpdatecutspecApprove(int? id, string type)
        {
            dbObj.UpdatecutspecApprove(id, type);
        }
        public void UpdatesizeApprove(int? id, string type)
        {
            dbObj.UpdatesizeApprove(id, type);
        }
        public void RejectApprove(int? id, string type,string Flag)
        {
            dbObj.RejectApprove(id, type, Flag);
        }
        public void UpdateInnerBoxApprove(int? id, string type)
        {
            dbObj.UpdateInnerBoxApprove(id, type);
        }

        public void BrandsaveApprove(int? id, string type)
        {
            dbObj.BrandsaveApprove(id, type);
        }
        public void Customersaveactive(CustomerMasterModelForActive model)
        {
            dbObj.Customersaveactive(model);
        }
        public void CustomerUpdate(CustomerMasterModelForActive model)
        {
            dbObj.CustomerUpdate(model);
        }
        public void filetypeUpdate(FileTypeDataEntity model)
        {
            dbObj.filetypeUpdate(model);
        }
        public void CutSpecUpdate(CutSpecsEntity model)
        {
            dbObj.CutSpecUpdate(model);
        }

        public void RawMaterialUpdatenoactive(RawMaterialMastrEntity model)
        {
            dbObj.RawMaterialUpdatenoactive(model);
        }
        public void RawMaterialUpdate(RawMaterialMastrEntity model)
        {
            dbObj.RawMaterialUpdate(model);
        }
        public void BrandUpdate(BrandDetailsEntity model)
        {
            dbObj.BrandUpdate(model);
        }
        public void CreateNewReq(string pr_uniq_id, string user_id)
        {
            dbObj.CreateNewReq(pr_uniq_id, user_id);
        }
        public int GetNewReq(string pr_uniq_id)
        {
            int newreq;
            newreq = dbObj.GetNewReq(pr_uniq_id);
            return newreq;
        }

        //public void CreateNewReq(string pr_uniq_id, string user_id)
        //{
        //    dbObj.CreateNewReq(pr_uniq_id, user_id);
        //}

        public string GetNewReqtype(string pr_uniq_id)
        {
            string newreqtype;
            newreqtype = dbObj.GetNewReqtype(pr_uniq_id);
            return newreqtype;
        }

        public void editCutspecssavebyrow(CutSpecsrowwiseEntity model1)
        {
            dbObj.editCutspecssavebyrow(model1);
        }
        public void editCutspecssavebyrowActive(CutSpecsrowwiseEntity model1)
        {
            dbObj.editCutspecssavebyrowActive(model1);
        }
        public void Cutspecssavebyrow(CutSpecsrowwiseEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            dbObj.Cutspecssavebyrow(model1, fileTypeCode, FileSubType, fileSize, fileCutType);
        }

        public void EditInnerboxSinglesavebyrowACTIVE(InnerBoxDetailsEntity model1)
        {
            dbObj.EditInnerboxSinglesavebyrowACTIVE(model1);
        }
        public void EditInnerboxSinglesavebyrow(InnerBoxDetailsEntity model1)
        {
            dbObj.EditInnerboxSinglesavebyrow(model1);
        }
        public void InnerboxSinglesavebyrow(InnerBoxDetailsEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            dbObj.InnerboxSinglesavebyrow(model1, fileTypeCode, FileSubType, fileSize, fileCutType);
        }
        public void Innerlabelavebyrow(InnerLabelDetailsEntity model)
        {
            dbObj.Innerlabelavebyrow(model);
        }
        public void InnerlabelEditavebyrow(InnerLabelDetailsEntity model)
        {
            dbObj.InnerlabelEditavebyrow(model);
        }
        public void InnerlabelEditavebyrowActive(InnerLabelDetailsEntity model)
        {
            dbObj.InnerlabelEditavebyrowActive(model);
        }
        public void Cutspecssave(CutSpecsEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            dbObj.Cutspecssave(model1, fileTypeCode, FileSubType, fileSize, fileCutType);
        }

        public List<CutSpecFiledEntity> getcutspecsfieldListonfileType(string filetypecode)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getcutspecsfieldListonfileType(filetypecode);
            List<CutSpecFiledEntity> ReqParList = new List<CutSpecFiledEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecFiledEntity()
            {

                tpi_newrm_id = item.Field<int>("tpi_newrm_id"),
                //nrm_for = item.Field<string>("nrm_for"),
                tpi_newrm_seqno = item.Field<byte>("tpi_newrm_seqno"),
                tpi_newrm_parm1 = item.Field<string>("tpi_newrm_parm1"),
                tpi_newrm_parm2 = item.Field<string>("tpi_newrm_parm2"),
                tpi_newrm_numinputs = item.Field<byte>("tpi_newrm_numinputs"),
                tpi_newrm_maxlength = item.Field<byte>("tpi_newrm_maxlength")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getcutspecsfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getcutspecsfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<CutSpecsEntity> getCutSpecNoActiveMaster()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCutSpecNoActiveMaster();
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {

                fcl_ftype_code = item.Field<string>("File Code"),
                filesizecode = item.Field<string>("tpi_fsize_code"),
                cuttype = item.Field<string>("Cut Type"),
                cutStandardName = item.Field<string>("Cut Standard"),
                ReqNo= item.Field<string>("tpi_request_type")+item.Field<int>("tpi_request_id"),
                //upcutrange = item.Field<string>("UPCUT"),
                //edcutrange = item.Field<string>("EDGE"),
                //ovcutrange = item.Field<string>("OVER"),
                prc_request_id = item.Field<int>("tpi_request_id"),
                prc_request_type = item.Field<string>("tpi_request_type")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<CutSpecFiledEntity> getinnerboxDoublefieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getinnerboxDoublefieldList();
            List<CutSpecFiledEntity> ReqParList = new List<CutSpecFiledEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecFiledEntity()
            {

                tpi_newrm_id = item.Field<int>("nrm_id"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                tpi_newrm_seqno = item.Field<byte>("nrm_seqno"),
                tpi_newrm_parm1 = item.Field<string>("nrm_for"),
                tpi_newrm_parm2 = item.Field<string>("nrm_parameter"),
                tpi_newrm_numinputs = item.Field<byte>("nrm_numof_inputs"),
                tpi_newrm_maxlength = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<CutSpecFiledEntity> getinnerboxSinglenewfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getinnerboxSinglenewfieldList();
            List<CutSpecFiledEntity> ReqParList = new List<CutSpecFiledEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecFiledEntity()
            {

                tpi_newrm_id = item.Field<int>("nrm_id"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                tpi_newrm_seqno = item.Field<byte>("nrm_seqno"),
                tpi_newrm_parm1 = item.Field<string>("nrm_for"),
                tpi_newrm_parm2 = item.Field<string>("nrm_parameter"),
                tpi_newrm_numinputs = item.Field<byte>("nrm_numof_inputs"),
                tpi_newrm_maxlength = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }

        public List<CutSpecsEntity> getCutSpecNoApproveMaster()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCutSpecNoApproveMaster();
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {

                fcl_ftype_code = item.Field<string>("File Code"),
                filesize = item.Field<decimal>("Nominal Length in inches"),
                tpi_fsize_code = item.Field<string>("tpi_fsize_code"),
                cuttype = item.Field<string>("Cut Type"),
                cutStandardName = item.Field<string>("Cut Standard"),
                upcutrange = item.Field<string>("UPCUT"),
                tpi_cut_code = item.Field<string>("tpi_cut_code"),
                tpi_cut_std_code = item.Field<string>("tpi_cut_std_code"),
                edcutrange = item.Field<string>("EDGE"),
                ovcutrange = item.Field<string>("OVER"),
                ReqNo=item.Field<string>("tpi_request_type")+item.Field<int>("tpi_request_id"),
                prc_request_id = item.Field<int>("tpi_request_id"),
                prc_request_type = item.Field<string>("tpi_request_type")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }

       



        //public List<CutSpecsEntity> getCutSpecMaster()
        //{
        //    DataSet ds = new DataSet();
        //    ds = dbObj.getCutSpecMaster();
        //    List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

        //    ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
        //    {
        //        fcl_ftype_code = item.Field<string>("File Type Code"),
        //        filesize = item.Field<decimal>("File Size Inches"),
        //        filesizecode = item.Field<string>("File Size Code"),
        //        cuttype = item.Field<string>("Cut Type"),
        //        filetype = item.Field<string>("File Type"),
        //        cutStandardName = item.Field<string>("Cut Standard"),
        //        upcutrange = item.Field<string>("UPCUT"),
        //        edcutrange = item.Field<string>("EDGE"),
        //        ovcutrange = item.Field<string>("OVERCUT"),

        //        // nrm_createby = item.Field<string>("nrm_createby"),
        //        //nrm_createdate = item.Field<string>("nrm_createdate")
        //    }).ToList();
        //    return ReqParList;
        //}
        public List<CutSpecsEntity> getCutSpecMaster()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCutSpecMaster();
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {
                fcl_ftype_code = item.Field<string>("File_Type_Code"),
                filesizecode = item.Field<string>("File_Size_Code"),
                cuttype = item.Field<string>("Cut_Type"),
                Cut_code = item.Field<string>("fc_cut_code"),
                filetype = item.Field<string>("File_Type"),
                cutStandardName = item.Field<string>("Cut_Standard"),
                //fc_cut_code = item.Field<string>("fc_cut_code"),
                tpi_cut_std_code = item.Field<string>("tpi_cut_std_code"),
                //ovcutrange = item.Field<string>("OVERCUT"),

                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }

        public int CheckHandleTypeAvailability(string hm_handle_type)
        {
            int xy = dbObj.CheckHandleTypeAvailability(hm_handle_type);
            return xy;
        }

        public List<CutSpecsEntity> getCutSpecMastersearch(string fsizecode)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCutSpecMastersearch(fsizecode);
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {

                fcl_ftype_code = item.Field<string>("File Type Code"),
                filesize = item.Field<decimal>("File Size Inches"),
                filesizecode = item.Field<string>("File Size Code"),
                cuttype = item.Field<string>("Cut Type"),
                cutStandardName = item.Field<string>("Cut Standard"),
                upcutrange = item.Field<string>("UPCUT"),
                edcutrange = item.Field<string>("EDGE"),
                ovcutrange = item.Field<string>("OVERCUT"),

                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }


        public void newfiletypesaveImg(short ft_request_id, string ft_request_type, string fileName)
        {
            dbObj.newfiletypesaveImg(ft_request_id, ft_request_type, fileName);
        }
        public void filetypesaveactive(FileTypeDataEntity model)
        {
            dbObj.filetypesaveactive(model);
        }
        public void filesizesaveactive(FileSizeDetailsEntity model)
        {
            dbObj.filesizesaveactive(model);
        }
        public void Bandsaveactive(BrandDetailsEntity model)
        {
            dbObj.Bandsaveactive(model);
        }
        public void CutSpecssaveactive(CutSpecsEntity model)
        {
            dbObj.CutSpecssaveactive(model);
        }

        public void EditHandlechartandpkgmasterDetails(HandleDetailsEntity model)
        {
            dbObj.EditHandlechartandpkgmasterDetails(model);
        }
        public void EditHandlechartDetails(HandleDetailsEntity model)
        {
            dbObj.EditHandlechartDetails(model);
        }
        public void EditHandlechartDetailsactive(HandleDetailsEntity model)
        {
            dbObj.EditHandlechartDetailsactive(model);
        }
        public void newfiletypesaveave(FileTypeDataEntity model1)
        {
            dbObj.newfiletypesaveave(model1);
        }
        public List<Customersinactive> getCustomerListinactive()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerListinactive();
            List<Customersinactive> customerList = new List<Customersinactive>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new Customersinactive()
            {
                Cm_Cust_Name = item.Field<string>("cm_cust_name"),
                Cm_Cust_Id = item.Field<string>("cm_cust_id"),
                Cm_Cust_Remarks = item.Field<string>("cm_cust_remarks"),
                _id = item.Field<int>("_id"),
                cm_request_id = item.Field<int>("cm_request_id"),
                cm_request_type = item.Field<string>("cm_request_type")
            }).ToList();
            return customerList;
        }
        public IEnumerable<FileTypeDataEntity> fileTypeListinactive()
        {
            DataSet ds = new DataSet();
            ds = dbObj.fileTypeListinactive();
            List<FileTypeDataEntity> fileTypeList = new List<FileTypeDataEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new FileTypeDataEntity()
            {
                ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                ft_ftype_remarks = item.Field<string>("ft_ftype_remarks"),
                ft_ftype_code = item.Field<string>("ft_ftype_code"),
                _id = item.Field<int>("_id"),
                ft_request_id = item.Field<int>("ft_request_id"),
                ft_request_type = item.Field<string>("ft_request_type")
            }).ToList();
            return fileTypeList;
        }
        public IEnumerable<FileTypeDataEntity> fileTypeListNoApprove()
        {
            DataSet ds = new DataSet();
            ds = dbObj.FiletypeListNoApprove();
            List<FileTypeDataEntity> fileTypeList = new List<FileTypeDataEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new FileTypeDataEntity()
            {
                ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                ft_ftype_remarks = item.Field<string>("ft_ftype_remarks"),
                ft_ftype_code = item.Field<string>("ft_ftype_code"),
                _id = item.Field<int>("_id"),
                ft_request_id = item.Field<int>("ft_request_id"),
                ft_request_type = item.Field<string>("ft_request_type")

            }).ToList();
            return fileTypeList;
        }
        public DataTable getFileSubTypeMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getFileSubTypeMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public IEnumerable<FileSubTypeEntity> fileSubTypeListinactive()
        {
            DataSet ds = new DataSet();
            string flag = "getFileSubTypeInactive";
            ds = dbObj.fileSubTypeListinactive(flag);
            List<FileSubTypeEntity> fileTypeList = new List<FileSubTypeEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new FileSubTypeEntity()
            {
                pm_request_id = item.Field<int>("pm_request_id"),
                pm_request_type = item.Field<string>("pm_request_type")+item.Field<int>("pm_request_id"),
                reqNo1= item.Field<string>("pm_request_type"),
                ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                fs_size_inches = item.Field<decimal>("fs_size_inches")
            }).ToList();
            return fileTypeList;
        }
        public IEnumerable<FileSubTypeEntity> fileSubTypeListNoApprove()
        {
            DataSet ds = new DataSet();
            string flag = "getFileSubTypeNoApprove";
            ds = dbObj.FileSubtypeListNoApprove(flag);
            List<FileSubTypeEntity> fileTypeList = new List<FileSubTypeEntity>();

            //fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new FileSubTypeEntity()
            //{
            //    pm_request_id = item.Field<int>("pm_request_id"),
            //    pm_request_type = item.Field<string>("pm_request_type"),
            //    ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
            //    pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
            //    fs_size_inches = item.Field<decimal>("fs_size_inches")

            //}).ToList();
            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new FileSubTypeEntity()
            {
                pm_request_id = item.Field<int>("pm_request_id"),
                pm_request_type = item.Field<string>("pm_request_type"),
                reqNo1=item.Field<string>("pm_request_type")+item.Field<int>("pm_request_id"),
                ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                pm_fsize_code = item.Field<string>("pm_fsize_code"),
                pm_file_code = item.Field<string>("pm_file_code"),
                pm_fstype_code = item.Field<string>("pm_fstype_code"),
                fs_size_inches = item.Field<decimal>("fs_size_inches")

            }).ToList();
            return fileTypeList;
        }
        public IEnumerable<FileSizeDetailsEntity> FileSizeListNoApprove()
        {
            DataSet ds = new DataSet();
            ds = dbObj.FileSizeListNoApprove();
            List<FileSizeDetailsEntity> FileSizeList = new List<FileSizeDetailsEntity>();

            FileSizeList = ds.Tables[0].AsEnumerable().Select(item => new FileSizeDetailsEntity()
            {
                _id = item.Field<int>("_id"),
                fs_size_inches = item.Field<decimal>("fs_size_inches"),
                fs_size_code = item.Field<string>("fs_size_code"),
                fs_size_remarks = item.Field<string>("fs_size_remarks"),
                fs_request_id = item.Field<int>("fs_request_id"),
                fs_request_type = item.Field<string>("fs_request_type")
            }).ToList();
            return FileSizeList;
        }
        public IEnumerable<FileSizeDetailsEntity> FileSizeListinactive()
        {
            DataSet ds = new DataSet();
            ds = dbObj.FileSizeListinactive();
            List<FileSizeDetailsEntity> FileSizeList = new List<FileSizeDetailsEntity>();

            FileSizeList = ds.Tables[0].AsEnumerable().Select(item => new FileSizeDetailsEntity()
            {
                _id = item.Field<int>("_id"),
                fs_size_inches = item.Field<decimal>("fs_size_inches"),
                fs_size_code = item.Field<string>("fs_size_code"),
                fs_size_remarks = item.Field<string>("fs_size_remarks"),
                fs_request_id = item.Field<int>("fs_request_id"),
                fs_request_type = item.Field<string>("fs_request_type")

            }).ToList();
            return FileSizeList;
        }



        public List<BrandDetailsEntity> brandNoApproveList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.brandNoApproveList();
            List<BrandDetailsEntity> customerList = new List<BrandDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new BrandDetailsEntity()
            {
                _id = item.Field<int>("_id"),
                bm_brand_id = item.Field<string>("bm_brand_id"),
                bm_brand_name = item.Field<string>("bm_brand_name"),
                bm_brand_remarks = item.Field<string>("bm_brand_remarks"),
                bm_request_type = item.Field<string>("bm_request_type"),
                bm_request_id = item.Field<int>("bm_request_id")

            }).ToList();
            return customerList;
        }
        public List<BrandDetailsEntity> brandListNoActiveList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.brandListNoActiveList();
            List<BrandDetailsEntity> customerList = new List<BrandDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new BrandDetailsEntity()
            {
                bm_brand_id = item.Field<string>("bm_brand_id"),
                bm_brand_name = item.Field<string>("bm_brand_name"),
                bm_brand_remarks = item.Field<string>("bm_brand_remarks"),
                bm_request_type = item.Field<string>("bm_request_type"),
                bm_request_id = item.Field<int>("bm_request_id")

            }).ToList();
            return customerList;
        }
        public List<CustomerMasterNoApprove> getCustListNoApprove()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustListNoApprove();
            List<CustomerMasterNoApprove> customerList = new List<CustomerMasterNoApprove>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerMasterNoApprove()
            {
                Cm_Cust_Name = item.Field<string>("cm_cust_name"),
                Cm_Cust_Id = item.Field<string>("cm_cust_id"),
                Cm_Cust_Remarks = item.Field<string>("cm_cust_remarks"),
                _id = item.Field<int>("_id"),
                cm_request_id = item.Field<int>("cm_request_id"),
                cm_request_type = item.Field<string>("cm_request_type")
            }).ToList();
            return customerList;
        }
        public List<CustomerMasterModelForActive> getCustomerListbyid(int id)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerListbyid(id);
            List<CustomerMasterModelForActive> customerList = new List<CustomerMasterModelForActive>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerMasterModelForActive()
            {
                Cm_Cust_Name = item.Field<string>("cm_cust_name"),
                Cm_Cust_Id = item.Field<string>("cm_cust_id"),
                Cm_Cust_Remarks = item.Field<string>("cm_cust_remarks"),
                _id = item.Field<int>("_id")
            }).ToList();
            return customerList;
        }
        public List<MasterReqParEntity> getCustomerfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }


        public IEnumerable<NewMstrReq> getNewMstrReqList(int reqid)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getNewMstrReqList(reqid);
            List<NewMstrReq> ReqParList = new List<NewMstrReq>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new NewMstrReq()
            {
                ma_id = item.Field<int>("ma_id"),
                ma_mstrtbl_name = item.Field<string>("ma_mstrtbl_name"),
                ma_request_id = item.Field<int>("ma_request_id"),
                ma_request_type = item.Field<string>("ma_request_type"),
                ma_mstrtbl_resp_grp = item.Field<string>("ma_mstrtbl_resp_grp"),
                ma_isApproved_flg = item.Field<string>("ma_isApproved_flg"),
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getFiletypefieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getFiletypefieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> GetRaw_MaterialfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetRaw_MaterialfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> GetStampMasterfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetStampMasterfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }

        public List<MasterReqParEntity> getFilesubtypefieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getFilesubtypefieldList();
            //List<MasterReqforsubtype> ReqParList = new List<MasterReqforsubtype>();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();
            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_seqno = item.Field<int>("seq_no"),
                nrm_for = item.Field<string>("ppm_parm_catg"),
                nrm_parameter = item.Field<string>("ppm_parm_name"),
                nrm_input_type = item.Field<string>("ppm_parm_inputtype"),
                nrm_createby = item.Field<string>("ppm_parm_code"),
                nrm_numof_inputs = item.Field<byte>("ppm_numof_inputs")
                //nrm_max_length = item.Field<Int16>("nrm_max_length")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getBrandfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getBrandfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getHandlesubtypefieldList(string selectedhandle_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandlesubtypefieldList(selectedhandle_type);
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }

        public List<MasterReqParEntity> getHandlechartfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandlechartfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getHandlefieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getHandlefieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getFileSizefieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getFileSizefieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),
                nrm_max_length = item.Field<Int16>("nrm_max_length")
                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }
        public List<MasterReqParEntity> getWrappingChartfieldList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getWrappingChartfieldList();
            List<MasterReqParEntity> ReqParList = new List<MasterReqParEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new MasterReqParEntity()
            {

                nrm_id = item.Field<int>("nrm_id"),
                nrm_for = item.Field<string>("nrm_for"),
                nrm_seqno = item.Field<byte>("nrm_seqno"),
                nrm_parameter = item.Field<string>("nrm_parameter"),
                nrm_input_type = item.Field<string>("nrm_input_type"),
                nrm_numof_inputs = item.Field<byte>("nrm_numof_inputs"),

                // nrm_createby = item.Field<string>("nrm_createby"),
                //nrm_createdate = item.Field<string>("nrm_createdate")
            }).ToList();
            return ReqParList;
        }

        public string CheckFileSizeCode(string sizecode)
        {
            var SqNo = dbObj.CheckFileSizeCode(sizecode);
            return SqNo;
        }
        #endregion


        #region AssetMgnt  by Sanket
        public DataTable getuserscount(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getusercount(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        //insert user
        public bool getlogin(UsermgmtEntity Obj)
        {
            return dbObj.OEMlogin(Obj);
        }

        //insert role
        public void saveroleDetails(UsermgmtEntity roleObj)
        {
            dbObj.saveroleDetails(roleObj);
        }

        //get role list
        public DataTable getRolelist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getroles(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //get User list
        public DataTable getUserlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getusers(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //update role
        public void updateroleDetails(UsermgmtEntity roleObj)
        {
            dbObj.updateroleDetails(roleObj);
        }

        //delete role
        public void deleteroleDetails(UsermgmtEntity roleObj)
        {
            dbObj.deleteroleDetails(roleObj);
        }


        //get plant list
        public DataTable getPlantlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getplants(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }



        //get image list
        public DataTable getImage(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getimage(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //get cst list
        public DataTable getcstdetails(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getcstdetails(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //insert user
        public void saveuserDetails(UsermgmtEntity roleObj)
        {
            dbObj.saveuserDetails(roleObj);
        }


        //select user
        public void getuserDetails(UsermgmtEntity roleObj)
        {
            dbObj.getuserdetails(roleObj);
        }


        //Update Login
        public void updateuserlogin(UsermgmtEntity roleObj)
        {
            dbObj.updatemainflagDetails(roleObj);
        }

        //Delete  Login user
        public void deleteuserlogin(UsermgmtEntity roleObj)
        {
            dbObj.deleteuserDetails(roleObj);
        }


        //get Admin setting list
        public DataTable getAdminlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getadminsetting(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        //select user
        public void updateadminconfig(AdminsettingEntity Obj)
        {
            dbObj.updateadminconfig(Obj);
        }



        //get config details
        public DataTable getconfigdetails(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getadminsetting(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //user approval---by nisha
        public void updateapprove(UsermgmtEntity obj)
        {
            dbObj.updateapprove(obj);

        }

        public void insertrejection(UsermgmtEntity obj)
        {
            dbObj.insertrejection(obj);
        }


        //get request type DI --OEM
        public DataTable getReqtype(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getrqstype(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //insert request
        public void saverequestDetails(RequestMgmtEntity Obj)
        {
            dbObj.saverequestDetails(Obj);
        }


        //get User list
        public DataTable getrequestlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getrequest(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //get request type OEM--user
        public DataTable getReqtypeuser(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getrqstype(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //insert request type
        public void saverequesttypeDetails(RequestMgmtEntity Obj)
        {
            dbObj.saverequesttype(Obj);
        }

        //select user
        public void updaterequstmgnt(RequestMgmtEntity Obj)
        {
            dbObj.updaterequeststatus(Obj);
        }


        //get role list
        public DataTable getcstlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getcst(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<UsermgmtEntity> getmainmenu(UsermgmtEntity u)
        {
            DataSet ds = new DataSet();
            ds = dbObj.menulist(u);
            List<UsermgmtEntity> menulist = new List<UsermgmtEntity>();

            menulist = ds.Tables[0].AsEnumerable().Select(item => new UsermgmtEntity()
            {
                mstr_id = item.Field<int>("Id"),
                Menu = item.Field<string>("Menu"),
                Menu_Level = item.Field<int>("Menu_Level"),
                RefId = item.Field<int>("RefId"),
                Controller = item.Field<string>("Controller"),
                Action = item.Field<string>("Action"),
                IsSubMenuPresent = item.Field<string>("IsSubMenuPresent")
                //RoleName = item.Field<string>("Role_Name"),
                //MainMenuID = item.Field<int>("MainMenuID"),
                //RoleId = item.Field<int>("Role_ID"),
                //MainMenu = item.Field<string>("MainMenu")

            }).ToList();
            return menulist;
        }
        public List<CutSpecsEntity> getcutspectview1(string flag, string cutStandardName, string cuttype, string fcl_ftype_code, string filetype, string filesizecode)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getcutspectview1(flag, cutStandardName, cuttype, fcl_ftype_code, filetype, filesizecode);
            List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
            {

                side_edge = item.Field<string>("Side_Edge"),
                // seq_no = item.Field<string>("vtpi_parm_seq_no"),
                UPCUT = item.Field<string>("UPCUT"),
                OVERCUT = item.Field<string>("OVERCUT"),
                //  upcutrange = item.Field<string>("UPCUT"),
                EDGE = item.Field<string>("EDGE"),

            }).ToList();
            return ReqParList;
        }

        #endregion


        #region Masters view -- sanket


        public DataTable getCutSpecMaster(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getCutSpecMaster(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getrawmateriallist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getrawmaterial(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getrawmaterialedit(string flag, string code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getrawmaterialedit(flag, code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getcstmstrdit(string flag, string code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getcstmstredit(flag, code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        //processlist




        //public void updateprocessDetails(ProcessMasterDetailsEntity obj)
        //{
        //    dbObj.updateprocessDetails(obj);
        //}

        //public List<CustomerDetailsEntity> getCustomerBrandList()
        //{
        //    string flag = "getcstbrandmstr";
        //    DataSet ds = new DataSet();
        //    ds = dbObj.getCustomerbrandList(flag);
        //    List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

        //    customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
        //    {
        //        pr_ship2country_name = item.Field<string>("pr_ship2country_name"),
        //        pr_cust_id = item.Field<string>("pr_cust_id"),
        //        pr_cust_name = item.Field<string>("pr_cust_name"),
        //        pr_brand_id = item.Field<string>("pr_brand_id"),
        //        pr_brand_desc = item.Field<string>("pr_brand_desc")
        //    }).ToList();
        //    return customerList;
        //}

        public List<RawMaterialMastrEntity> getrawmateriallistnoapprove()
        {

            string flag = "getrawmaterialsNewlist";
            DataSet ds = new DataSet();
            ds = dbObj.getrawmateriallistnoapprove(flag);
            List<RawMaterialMastrEntity> customerList = new List<RawMaterialMastrEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new RawMaterialMastrEntity()
            {
                //rm_request_id=item.Field<int>("pr_ship2country_name"),
                rm_fsize_id = item.Field<string>("fs_size_code"),
                rm_ftype_id = item.Field<string>("ft_ftype_desc"),
                rm_fstype_id = item.Field<string>("pm_fstype_desc"),
                rm_code = item.Field<string>("rm_code"),
                rm_desc = item.Field<string>("rm_desc"),
                //rm_material      = item.Field<string>("pr_ship2country_name"),
                rm_wtinkg_perthsnd = item.Field<decimal>("rm_wtinkg_perthsnd"),
                rm_netwtingm_persku = item.Field<decimal>("rm_netwtingm_persku"),
                rm_remarks = item.Field<string>("rm_remarks")
            }).ToList();
            return customerList;
        }

        public DataTable wrapperNoApprove(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.wrapperNoApprove(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable wrapperNoActive(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.wrapperNoActive(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable stmaplistactive(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.stmaplistactive(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getstamplist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getstamp(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<StampDetailsEntity> getstampimgpathview(string stamp_chart)
        {

            DataSet ds = new DataSet();
            ds = dbObj.getstampimgpathview(stamp_chart);
            List<StampDetailsEntity> ReqParList = new List<StampDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new StampDetailsEntity()
            {
                
                Customer = item.Field<string>("Customer"),
                Brand = item.Field<string>("Brand"),
                Size = item.Field<string>("Size"),
                StampMode = item.Field<string>("Stamp_Mode"),
                sd_chart_num = item.Field<string>("Chart_No"),
                File_type = item.Field<string>("ft_ftype_desc"),
                File_subtype = item.Field<string>("pm_fstype_desc"),
                sd_stampimg = item.Field<string>("sm_chartdoc_name"),

            }).ToList();
            return ReqParList;
        }

        public List<StampDetailsEntity> getGetShowStampChart(string stamp_chart)
        {

            DataSet ds = new DataSet();
            ds = dbObj.getGetShowStampChart(stamp_chart);
            List<StampDetailsEntity> ReqParList = new List<StampDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new StampDetailsEntity()
            {

                Customer = item.Field<string>("Customer"),
                Brand = item.Field<string>("Brand"),
                Size = item.Field<string>("Size"),
                StampMode = item.Field<string>("Stamp_Mode"),
                sd_chart_num = item.Field<string>("Chart_No"),
                File_type = item.Field<string>("ft_ftype_desc"),
                File_subtype = item.Field<string>("pm_fstype_desc"),
                sd_stampimg = item.Field<string>("sm_chartdoc_name"),

            }).ToList();
            return ReqParList;
        }

        public DataTable getcstlist1(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getstamp(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable gethandleMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.gethndleMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<HandleMasterEntity> gethandleimgpathview(string handle_chart)
        {
            DataSet ds = new DataSet();
            ds = dbObj.gethandleimgpathview(handle_chart);
            List<HandleMasterEntity> ReqParList = new List<HandleMasterEntity>();
            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new HandleMasterEntity()
            {
                hm_chartimg_name = item.Field<string>("hm_chartimg_name"),
                hd_parm_name = item.Field<string>("hd_parm_name"),
                // hd_parm_code = item.Field<string>("hd_parm_code"),
                hd_parm_dwgvalue = item.Field<string>("hd_parm_dwgvalue"),
                hd_parm_scrnvalue = item.Field<string>("hd_parm_scrnvalue")
            }).ToList();
            return ReqParList;
        }

        public List<HandleMasterEntity> GetShowHandelChart(string handle_chart)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetShowHandelChart(handle_chart);
            List<HandleMasterEntity> ReqParList = new List<HandleMasterEntity>();
            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new HandleMasterEntity()
            {
                hm_chartimg_name = item.Field<string>("hm_chartimg_name"),
                hd_parm_name = item.Field<string>("hd_parm_name"),
                // hd_parm_code = item.Field<string>("hd_parm_code"),
                hd_parm_dwgvalue = item.Field<string>("hd_parm_dwgvalue"),
                hd_parm_scrnvalue = item.Field<string>("hd_parm_scrnvalue")
            }).ToList();
            return ReqParList;
        }

        public DataTable getwrapperMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getwrapperMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getinnerboxMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getwrapperMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<WrapperMasterEntity> getwrappinglistview(string wrapping_chart)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getwrappinglistview(wrapping_chart);
            List<WrapperMasterEntity> ReqParList = new List<WrapperMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new WrapperMasterEntity()
            {
                wd_parm_name = item.Field<string>("wd_parm_name"),
                // wd_parm_code = item.Field<string>("wd_parm_code"),
                wd_parm_dwgvalue = item.Field<string>("wd_parm_dwgvalue"),
                wd_parm_scrnvalue = item.Field<string>("wd_parm_scrnvalue"),
            }).ToList();
            return ReqParList;
        }
        public List<WrapperMasterEntity> GetShowWrapperdetails(string reqid,string reqtype)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetShowWrapperdetails(reqid, reqtype);
            List<WrapperMasterEntity> ReqParList = new List<WrapperMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new WrapperMasterEntity()
            {
                wd_parm_name = item.Field<string>("wd_parm_name"),
                wd_parm_dwgvalue = item.Field<string>("wd_parm_dwgvalue"),
                wd_parm_scrnvalue = item.Field<string>("wd_parm_scrnvalue"),
            }).ToList();
            return ReqParList;
        }
        public List<InnerLabelDetailsEntity> GetinnerLabelListview(string innerlabel_chart, string ft_ftype_desc)
        {
            DataSet ds = new DataSet();
            // ds = dbObj.GetinnerLabelList("IP-LBL-CH-002", pr_ftype_code, pr_fstype_code, pr_fsize_code, pr_cust_id, pr_brand_id, pr_fcut_code, pr_ship2country_id);
            ds = dbObj.GetinnerLabelListView(innerlabel_chart, ft_ftype_desc);
            List<InnerLabelDetailsEntity> ReqParList = new List<InnerLabelDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerLabelDetailsEntity()
            {
                ilbd_parm_name = item.Field<string>("ilbd_parm_name"),
                /// ilbd_parm_code = item.Field<string>("ilbd_parm_code"),
                ilbd_parm_dwgvalue = item.Field<string>("ilbd_parm_dwgvalue"),
                ilbd_parm_scrnvalue = item.Field<string>("ilbd_parm_scrnvalue"),
                //sd_parm_chartimg = item.Field<string>("pm_pallet_chartimg"),
            }).ToList();
            return ReqParList;
        }
        public List<InnerLabelDetailsEntity> GetShowinnerlabel(string innerlabel_chart, string ft_ftype_desc)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetShowinnerlabel(innerlabel_chart, ft_ftype_desc);
            List<InnerLabelDetailsEntity> ReqParList = new List<InnerLabelDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerLabelDetailsEntity()
            {
                ilbd_parm_name = item.Field<string>("ilbd_parm_name"),
                ilbd_parm_dwgvalue = item.Field<string>("ilbd_parm_dwgvalue"),
                ilbd_parm_scrnvalue = item.Field<string>("ilbd_parm_scrnvalue"),
            }).ToList();
            return ReqParList;
        }
        public List<InnerBoxDetailsEntity> getinnerboxlistview(string innerbox_chart, string pr_ftype_desc)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getinnerboxlistview(innerbox_chart, pr_ftype_desc);
            List<InnerBoxDetailsEntity> ReqParList = new List<InnerBoxDetailsEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxDetailsEntity()
            {
                ibd_pkg_parm_name = item.Field<string>("ibd_pkg_parm_name"),
                // ibd_pkg_parm_code = item.Field<string>("ibd_pkg_parm_code"),
                // ibd_pkg_parm_dwgvalue = item.Field<string>("ibd_pkg_parm_dwgvalue"),
                ibd_pkg_parm_scrnvalue = item.Field<string>("ibd_pkg_parm_scrnvalue"),
                ib_chartimg = item.Field<string>("ib_chartimg")
            }).ToList();
            return ReqParList;
        }
        public List<InnerBoxDetailsEntity> Getshowinnerbox(string innerbox_chart, string pr_ftype_desc)
        {
            DataSet ds = new DataSet();
            ds = dbObj.Getshowinnerbox(innerbox_chart, pr_ftype_desc);
            List<InnerBoxDetailsEntity> ReqParList = new List<InnerBoxDetailsEntity>();
            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxDetailsEntity()
            {
                ibd_pkg_parm_name = item.Field<string>("ibd_pkg_parm_name"),
                ibd_pkg_parm_scrnvalue = item.Field<string>("ibd_pkg_parm_scrnvalue"),
                ib_chartimg = item.Field<string>("ib_chartimg")
            }).ToList();
            return ReqParList;
        }

        public DataTable getouterboxMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getwrapperMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<OuterBoxMasterEntity> GetOuterChartlistview(string outerbox_chart, string boxtype)
        {

            DataSet ds = new DataSet();
            ds = dbObj.GetOuterChartlistview(outerbox_chart, boxtype);
            List<OuterBoxMasterEntity> ReqParList = new List<OuterBoxMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new OuterBoxMasterEntity()
            {
                obd_parm_name = item.Field<string>("obd_parm_name"),
                ob_box_chartnum = item.Field<string>("ob_box_chartnum"),
                // obd_parm_code = item.Field<string>("obd_parm_code"),
                obd_parm_dwgvalue = item.Field<string>("obd_parm_dwgvalue"),
                obd_parm_scrnvalue = item.Field<string>("obd_parm_scrnvalue"),
                ob_box_imgname = item.Field<string>("ob_box_imgname"),
            }).ToList();
            return ReqParList;
        }
        public List<OuterBoxMasterEntity> Getshowoutermaster(string outerbox_chart, string boxtype)
        {

            DataSet ds = new DataSet();
            ds = dbObj.Getshowoutermaster(outerbox_chart, boxtype);
            List<OuterBoxMasterEntity> ReqParList = new List<OuterBoxMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new OuterBoxMasterEntity()
            {
                obd_parm_name = item.Field<string>("obd_parm_name"),
                ob_box_chartnum = item.Field<string>("ob_box_chartnum"),
                // obd_parm_code = item.Field<string>("obd_parm_code"),
                obd_parm_dwgvalue = item.Field<string>("obd_parm_dwgvalue"),
                obd_parm_scrnvalue = item.Field<string>("obd_parm_scrnvalue"),
                ob_box_imgname = item.Field<string>("ob_box_imgname"),
            }).ToList();
            return ReqParList;
        }

        public DataTable getpalletMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getwrapperMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<PalletMasterEntity> GetPalletListview(string pallet_chart, string pallet_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetPalletListview(pallet_chart, pallet_type);
            List<PalletMasterEntity> ReqParList = new List<PalletMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new PalletMasterEntity()
            {
                pd_parm_name = item.Field<string>("pd_parm_name"),
                //  pd_parm_code = item.Field<string>("pd_parm_code"),
                pd_parm_dwgvalue = item.Field<string>("pd_parm_dwgvalue"),
                pd_parm_scrnvalue = item.Field<string>("pd_parm_scrnvalue"),
                pm_pallet_chartimg = item.Field<string>("pm_pallet_chartimg"),
            }).ToList();
            return ReqParList;
        }
        public List<PalletMasterEntity> getApprovepalletmstrview(string pallet_chart, string pallet_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getApprovepalletmstrview(pallet_chart, pallet_type);
            List<PalletMasterEntity> ReqParList = new List<PalletMasterEntity>();

            ReqParList = ds.Tables[0].AsEnumerable().Select(item => new PalletMasterEntity()
            {
                pd_parm_name = item.Field<string>("pd_parm_name"),
                //  pd_parm_code = item.Field<string>("pd_parm_code"),
                pd_parm_dwgvalue = item.Field<string>("pd_parm_dwgvalue"),
                pd_parm_scrnvalue = item.Field<string>("pd_parm_scrnvalue"),
                pm_pallet_chartimg = item.Field<string>("pm_pallet_chartimg"),
            }).ToList();
            return ReqParList;
        }


        public DataTable getfilesubtype(string flag, string sizecode, string filecode)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getfilesubtype(flag, sizecode, filecode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getinnerlabeldetails(string flag, string chartno)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getinnerlabledetails(flag, chartno);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public int UpdatedispatchSKU(ItemPartNumEntity itemPartNumObj)
        {
            return dbObj.UpdatedispatchSKU(itemPartNumObj);
        }


        public void DeactiveRequest(ItemPartNumEntity itemPartNumObj)
        {
            dbObj.DeactiveRequest(itemPartNumObj);
        }


        //Assign Module Management
        public DataTable getrolelist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getmoduledata(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        public DataTable getcutspectview(string flag, string cutStandardName, string cuttype, string fcl_ftype_code, string filetype, string filesizecode)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getcutspectview(flag, cutStandardName, cuttype, fcl_ftype_code, filetype, filesizecode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        //public List<CutSpecsEntity> getcutspectview1(string flag, string cutStandardName, string cuttype, string fcl_ftype_code, string filetype, string filesizecode)
        //{
        //    DataSet ds = new DataSet();
        //    ds = dbObj.getcutspectview1(flag, cutStandardName, cuttype, fcl_ftype_code, filetype, filesizecode);
        //    List<CutSpecsEntity> ReqParList = new List<CutSpecsEntity>();

        //    ReqParList = ds.Tables[0].AsEnumerable().Select(item => new CutSpecsEntity()
        //    {

        //        side_edge = item.Field<string>("Side_Edge"),
        //       // seq_no = item.Field<string>("vtpi_parm_seq_no"),
        //        UPCUT = item.Field<string>("UPCUT"),
        //        OVERCUT = item.Field<string>("OVERCUT"),
        //      //  upcutrange = item.Field<string>("UPCUT"),
        //        EDGE = item.Field<string>("EDGE"),

        //    }).ToList();
        //    return ReqParList;
        //}

        #endregion


        #region Master Insert--sanket
        public void insertfilesize(FileSizeDetailsEntity Obj)
        {
            dbObj.insertfilesize(Obj);
        }

        public void insertcustomer(CustomerDetailsEntity Obj)
        {
            dbObj.insertnewcustomer(Obj);
        }

        public void insertbrand(BrandDetailsEntity Obj)
        {
            dbObj.insertnewbrand(Obj);
        }
        #endregion

        #region Remark
        public void SaveRemark(PartnumgenRemarksEntity remarkObj)
        {
            dbObj.SaveCustomerRemark(remarkObj);
        }

        //public void getremarkdetail1(string remark_tab,int request_id,string request_type)
        //{
        //    dbObj.GetRemarkDetails(remark_tab, request_id, request_type);
        //}

        public List<PartnumgenRemarksEntity> getremarkdetail(string remark_tab, int request_id, string request_type)
        {
            DataSet ds = new DataSet();
            ds = dbObj.GetRemarkDetails(remark_tab, request_id, request_type);
            List<PartnumgenRemarksEntity> remarklist = new List<PartnumgenRemarksEntity>();

            remarklist = ds.Tables[0].AsEnumerable().Select(item => new PartnumgenRemarksEntity()
            {
                prm_request_type = item.Field<string>("prm_request_type"),
                prm_request_id = item.Field<int>("prm_request_id"),
                prm_remark_tab = item.Field<string>("prm_remark_tab"),
                prm_remarks = item.Field<string>("prm_remarks"),
                prm_createdate = item.Field<DateTime>("prm_createdate"),
                Stringcreatedate = item.Field<DateTime>("prm_createdate").ToString("dd/MM/yyyy"),
            }).ToList();
            return remarklist;
        }
        #endregion

        #region Manage Unit
        public DataTable getUnitlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getUnitlist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getUnitnormlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getUnitnormlist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable get_unitvaluestremlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_unitvaluestremlist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        public DataTable getAncillarylist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getAncillarylist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public List<PlantMasterEntity> getPlantDropdownList()
        {
            DataSet ds = new DataSet();
            string flag = "plandropdowntlist";
            ds = dbObj.getPlantDropownList(flag);
            List<PlantMasterEntity> plantlist = new List<PlantMasterEntity>();

            plantlist = ds.Tables[0].AsEnumerable().Select(item => new PlantMasterEntity()
            {
                pla_plant_name = item.Field<string>("pla_plant_name").ToString(),
                pla_plant_code = item.Field<string>("pla_plant_code").ToString()
            }).ToList();
            return plantlist;
        }

        public List<UnitMasterEntity> getunitDropdownList()
        {
            DataSet ds = new DataSet();
            string flag = "unitdropdownlist";
            ds = dbObj.getUnitDropownList(flag);
            List<UnitMasterEntity> unitlist = new List<UnitMasterEntity>();

            unitlist = ds.Tables[0].AsEnumerable().Select(item => new UnitMasterEntity()
            {
                uni_unit_code = item.Field<string>("uni_unit_code").ToString(),
                uni_unit_name = item.Field<string>("uni_unit_name").ToString(),
                uni_unit_type = item.Field<string>("uni_unit_type").ToString()

            }).ToList();
            return unitlist;
        }
        public List<UnitMasterEntity> getunitvalueStreamDropdownList()
        {
            DataSet ds = new DataSet();
            string flag = "unitvaluestreamlist";
            ds = dbObj.getUnitDropownList(flag);
            List<UnitMasterEntity> unitvaluestreamlist = new List<UnitMasterEntity>();

            unitvaluestreamlist = ds.Tables[0].AsEnumerable().Select(item => new UnitMasterEntity()
            {
                ft_ftype_code = item.Field<string>("ft_ftype_code").ToString(),
                ft_ftype_desc = item.Field<string>("ft_ftype_desc").ToString()
            }).ToList();
            return unitvaluestreamlist;
        }

        public List<valueStreammasterEntity> getvalueStreamDropdownList()
        {
            DataSet ds = new DataSet();
            string flag = "unitvaluestreamlist";
            ds = dbObj.getUnitDropownList(flag);
            List<valueStreammasterEntity> valuestreamlist = new List<valueStreammasterEntity>();

            valuestreamlist = ds.Tables[0].AsEnumerable().Select(item => new valueStreammasterEntity()
            {
                ft_ftype_code = item.Field<string>("ft_ftype_code").ToString(),
                ft_ftype_desc = item.Field<string>("ft_ftype_desc").ToString()
            }).ToList();
            return valuestreamlist;
        }

        public void SaveUnit(UnitMasterEntity obj)
        {
            dbObj.SaveUnitDetails(obj);
        }

        public void SaveValueStream(valueStreammasterEntity obj)
        {
            dbObj.SaveValueStream(obj);
        }
        public void AddUnitValueStream(UnitMasterEntity obj)
        {
            dbObj.AddUnitValueStream(obj);
        }

        public string PlantforPlantCode(string PlantID)
        {
            var plancode = dbObj.PlantforPlantCode(PlantID);
            return plancode;
        }

        public string ShowValueStreamNo(string VSNo)
        {
            var VSMNO = dbObj.ShowValueStreamNo(VSNo);
            return VSMNO;
        }
        public DataTable getPlantAncillarylist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getPlantAncillarylist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public string CheckValusreamCode(string ft_ftype_code)
        {
            var valScode = dbObj.CheckValusreamCode(ft_ftype_code);
            return valScode;
        }
        public string CheckUnitCode(string unitcode)
        {
            var SqNo = dbObj.CheckUnitCode(unitcode);
            return SqNo;
        }
        public int CheckValuestreamcode(string unitcode,string valuestream)
        {
            var VS = dbObj.CheckValuestreamcode(unitcode, valuestream);
            return VS;
        }
        #endregion

        #region Request New Set yogesh
        public void CreateNewReqSet(string pr_uniq_id, string UserId)
        {
            dbObj.CreateNewReqSet(pr_uniq_id, UserId);
        }
        #endregion

        #region SKUs Selection Write by yogesh

        public List<DispachProductSkuEntity> getProduSKUList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getProduSKUList();
            List<DispachProductSkuEntity> ProduSKUList = new List<DispachProductSkuEntity>();
            ProduSKUList = ds.Tables[0].AsEnumerable().Select(item => new DispachProductSkuEntity()
            {
                partno = item.Field<string>("pn_part_no"),
                dp_id = item.Field<Int32>("pn_id"),
                Skupartno = item.Field<string>("pn_part_no").Substring(2, 8),

            }).ToList();
            return ProduSKUList;
        }
        public List<PartNumMasterEntity> getPartNumList(string custID)
        {


            DataSet ds = new DataSet();
            ds = dbObj.getPartNumList(custID);
            List<PartNumMasterEntity> partlist = new List<PartNumMasterEntity>();

            partlist = ds.Tables[0].AsEnumerable().Select(item => new PartNumMasterEntity()
            {
                pn_part_no = item.Field<string>("pn_part_no"),


            }).ToList();


            return partlist;

        }
        public List<PartNumMasterEntity> getpartlistDetaillist(string PartNo)
        {


            DataSet ds = new DataSet();
            ds = dbObj.getpartlistDetaillist(PartNo);
            List<PartNumMasterEntity> partlistDetail = new List<PartNumMasterEntity>();

            partlistDetail = ds.Tables[0].AsEnumerable().Select(item => new PartNumMasterEntity()
            {
                dp_stamp_chart = item.Field<string>("dp_stamp_chart"),
                dp_wrapping_chart = item.Field<string>("dp_wrapping_chart"),
                dp_handle_chart = item.Field<string>("dp_handle_chart"),
                pn_start_date = item.Field<DateTime>("pn_start_date"),
                pn_end_date = item.Field<DateTime>("pn_end_date"),
                pn_request_type = item.Field<string>("pn_request_type"),
                pr_tang_color = item.Field<string>("pr_tang_color"),
                pn_request_id = item.Field<int>("pn_request_id")

            }).ToList();
            return partlistDetail;
        }
        public void saveCustomerSetDetails(CustomerDetailsEntity custObj)
        {
            dbObj.saveCustomerSetDetails(custObj);
        }
        public int SaveSKUSelectionMaster(SkuSelectionEntity obj)
        {
            int i = dbObj.SaveSKUSelectionMaster(obj);
            return i;
        }
        public List<WrapperMasterEntity> getSetWrappingChartNums(string wrappingType)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getSetWrappingChartNums(wrappingType);
            List<WrapperMasterEntity> handleMasterList = new List<WrapperMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new WrapperMasterEntity()
            {
                wm_chart_num = item.Field<string>("wm_chart_num")

            }).ToList();
            return handleMasterList;
        }
        public int SaveupdateskudispatchMaster(DispatchSKUEntity obj)
        {
            int i = dbObj.SaveupdateskudispatchMaster(obj);
            return i;
        }
        public int SaveupdatewrapperskuMaster(SkuSelectionEntity obj)
        {
            int i = dbObj.SaveupdatewrapperskuMaster(obj);
            return i;
        }
        public List<InnerBoxMasterEntity> getSetInnerBoxChartNums(string innerBoxMaterial, string maxfileSize)
        {
            DataSet ds = new DataSet();
            ds = dbObj.getSetInnerBoxChartNums(innerBoxMaterial, maxfileSize);
            List<InnerBoxMasterEntity> handleMasterList = new List<InnerBoxMasterEntity>();

            handleMasterList = ds.Tables[0].AsEnumerable().Select(item => new InnerBoxMasterEntity()
            {
                ib_chartnum = item.Field<string>("ib_chartnum")

            }).ToList();
            return handleMasterList;
        }
        public int Saveupdateskudispatchinnerbox(DispatchSKUEntity obj)
        {
            int i = dbObj.Saveupdateskudispatchinnerbox(obj);
            return i;
        }
        public int SaveSkuSetOuter(DispatchSKUEntity obj)
        {
            int i = dbObj.SaveSkuSetOuter(obj);
            return i;
        }
        public int SaveSkuSetPallet(DispatchSKUEntity obj)
        {
            int i = dbObj.SaveSkuSetPallet(obj);
            return i;
        }
        public DataTable get_ssmskulist(string flag, int requestId)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_ssmskulist(flag, requestId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public int SaveTempPartSku(PartNumMasterEntity obj)
        {
            int i = dbObj.SaveTempPartSku(obj);
            return i;
        }
        public List<SkuSelectionEntity> GetSkuDetails(int pr_request_id, string pr_request_type)
        {
            DataSet ds = new DataSet();
            string flag = "ViewSkudetailslist";
            ds = dbObj.GetSkuDetails(pr_request_id, pr_request_type, flag);
            List<SkuSelectionEntity> ReqParList = new List<SkuSelectionEntity>();
                ReqParList = ds.Tables[0].AsEnumerable().Select(item => new SkuSelectionEntity()
                {
                    rs_sku_code = item.Field<string>("rs_sku_code"),
                    rs_quantity = item.Field<int>("rs_quantity"),
                    rs_stamp_chart = item.Field<string>("rs_stamp_chart"),
                    rs_wrapper_chart = item.Field<string>("rs_wrapper_chart"),
                    rs_handle_chart = item.Field<string>("rs_handle_chart"),
                    rs_tang_tempr = item.Field<string>("rs_tang_tempr"),
                    rs_request_id = item.Field<int>("rs_request_id"),
                   pn_request_id = item.Field<int>("pn_request_id"),
                    rs_request_type = item.Field<string>("rs_request_type"),
                    pn_request_type = item.Field<string>("pn_request_type")
                }).ToList();
            return ReqParList;
        }
        public string CheckMaxSizeCode(int pr_request_id, string pr_request_type)
        {
            var SqNo = dbObj.CheckMaxSizeCode(pr_request_id, pr_request_type);
            return SqNo;
        }
        public string CheckWrapperSet(int pr_request_id, string pr_request_type)
        {
            var wrapperchart = dbObj.CheckWrapperSet(pr_request_id, pr_request_type);
            return wrapperchart;
        }
        public string SetSKUWrapperSet(int pr_request_id, string pr_request_type)
        {
            var wrapperchart = dbObj.SetSKUWrapperSet(pr_request_id, pr_request_type);
            return wrapperchart;
        }
        public string SetNewSKUWrapperSet(int pr_request_id, string pr_request_type)
        {
            var wrapperchart = dbObj.SetNewSKUWrapperSet(pr_request_id, pr_request_type);
            return wrapperchart;
        }
        #endregion


        #region Manage Process

        public void saveoprationDetails(OperationMasterEntity obj)
        {
            dbObj.saveopreatinDetails(obj);
        }
        public void saveprocessDetails(ProcessMasterDetailsEntity obj)
        {
            dbObj.saveProcessDetails(obj);
        }
        public DataTable getProcesslist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getProcesslist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable processtypewiselist(string flag, string pro_process_code, int requestID, string requesttype)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.processtypewiselist(flag, pro_process_code, requestID, requesttype);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable FileTypeprocesslist(string flag, string pro_process_code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.FileTypeprocesslist(flag, pro_process_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable GetAddApproveProcess(string flag, string pro_process_code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.GetAddApproveProcess(flag, pro_process_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getmasterList(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getopreationmasterList(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getValuesteramProcess(string flag,string val_valuestream_code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getValuesteramProcess(flag, val_valuestream_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public string CheckOprationSqNo(string opr_operation_seq)
        {
            var SqNo = dbObj.CheckOprationSqNo(opr_operation_seq);
            return SqNo;
        }
        public IEnumerable<ProcessMasterDetailsEntity> DispatchoprationList(string requestId, string requesttype)
        {
            DataSet ds = new DataSet();
            string flag = "Dispatchopration";
            ds = dbObj.DispatchoprationList(flag,requestId, requesttype);
            List<ProcessMasterDetailsEntity> DispatchoprationList = new List<ProcessMasterDetailsEntity>();

            DispatchoprationList = ds.Tables[0].AsEnumerable().Select(item => new ProcessMasterDetailsEntity()
            {
                pm_process_seqno = item.Field<Int16>("dpm_process_seqno"),
                opr_operation_name = item.Field<string>("opr_operation_name")
               
            }).ToList();
            return DispatchoprationList;
        }
        public IEnumerable<ProcessMasterDetailsEntity> getFileTypeUnitList(string FileTypeCode)
        {
            DataSet ds = new DataSet();
            string flag = "FileTypeUnitList";
            ds = dbObj.getFileTypeUnitList(flag, FileTypeCode);
            List<ProcessMasterDetailsEntity> DispatchoprationList = new List<ProcessMasterDetailsEntity>();

            DispatchoprationList = ds.Tables[0].AsEnumerable().Select(item => new ProcessMasterDetailsEntity()
            {
                val_unit_code = item.Field<string>("val_unit_code"),
                uni_unit_name = item.Field<string>("uni_unit_name")

            }).ToList();
            return DispatchoprationList;
        }
        public void ChangeProcessStatus(ProcessMasterDetailsEntity obj)
        {
            dbObj.ChangeProcessStatus(obj);
        }
        public IEnumerable<ProcessMasterDetailsEntity> processCompletelist()
        {
            DataSet ds = new DataSet();
            string flag = "getprocessCompletelist";
            ds = dbObj.processCompletelist(flag);
            List<ProcessMasterDetailsEntity> fileTypeList = new List<ProcessMasterDetailsEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new ProcessMasterDetailsEntity()
            {
                // ppm_id = item.Field<int>("ppm_id"),
                ppm_request_id = item.Field<int>("ppm_request_id"),
                ppm_process_code = item.Field<string>("ppm_process_code"),
                ppm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                ppm_ftype_desc = item.Field<string>("ppm_ftype_desc"),
                ppm_request_type = item.Field<string>("ppm_request_type"),
                ppm_ftype_code = item.Field<string>("ppm_ftype_code"),
                ppm_fstype_code = item.Field<string>("ppm_fstype_code")
            }).ToList();
            return fileTypeList;
        }

        public IEnumerable<ProductionSKUCutSpecsEntity> getnewdispatchprocess()
        {
            DataSet ds = new DataSet();
            string flag = "NewDispatchprocess";
            ds = dbObj.getNewDispatchprocess(flag);
            List<ProductionSKUCutSpecsEntity> fileTypeList = new List<ProductionSKUCutSpecsEntity>();
           
            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new ProductionSKUCutSpecsEntity()
            {
                pr_RequestNo = item.Field<string>("RequestNo"),
                pr_FileType = item.Field<string>("FileType"),
                pr_process_code = item.Field<string>("ft_process_code"),
                pr_ftype_code = item.Field<string>("pr_ftype_code")

            }).ToList();
            return fileTypeList;
        }
        public IEnumerable<ProductionSKUCutSpecsEntity> getApprovedispatchprocess()
        {
            DataSet ds = new DataSet();
            string flag = "NewApproveDispatchprocess";
            ds = dbObj.getApprovedispatchprocess(flag);
            List<ProductionSKUCutSpecsEntity> fileTypeList = new List<ProductionSKUCutSpecsEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new ProductionSKUCutSpecsEntity()
            {
                pr_RequestNo = item.Field<string>("RequestNo"),
                pr_FileType = item.Field<string>("FileType"),
                pr_process_code = item.Field<string>("ft_process_code"),
                pr_ftype_code = item.Field<string>("pr_ftype_code")

            }).ToList();
            return fileTypeList;
        }
        public void saveProcessAssetAllocation(DispatchAssetAllocationEntity obj)
        {
            dbObj.saveProcessAssetAllocation(obj);
        }
        public DataTable getSelectedprocesslist(string flag,int requestid)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getSelectedprocesslist(flag, requestid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public void ColseProcessDetails(ProcessMasterDetailsEntity obj)
        {
            dbObj.ColseProcessDetails(obj);
        }
        public void ApproveProcessStatus(OperationMasterEntity obj)
        {
            dbObj.ApproveProcessStatus(obj);
        }
        public void RejectProcessStatus(OperationMasterEntity obj)
        {
            dbObj.RejectProcessStatus(obj);
        }

        public DataTable ProcessMasterlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.ProcessMasterlist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable GetShowGridprocess(string flag, string partno)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.GetShowGridprocess(flag, partno);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        #endregion

        #region Manage Equipment

        public DataTable getequipmentlist(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getequipmentlist(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getEquipmentDropdownList(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getEquipmentDropdownList(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable EquipmentTypeDropdownList(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getEquipmentDropdownList(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public string processtypename(string pro_process_code)
        {
            var Filetypename = dbObj.processtypename(pro_process_code);
            return Filetypename;
        }
        public DataTable getEquipmentvalueStream(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getEquipmentvalueStream(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getEquipmentAssset(string flag, int EquipmentID)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getEquipmentAssset(flag, EquipmentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
       
        public int saveequipmentDetails(EquipmentMasterEntity obj)
        {
            int i = dbObj.saveequipmentDetails(obj);
            return i;
        }

        public void SaveAssetAllocation(EquipmentMasterEntity obj)
        {
            dbObj.SaveAssetAllocation(obj);

        }

        public DataTable getunitvaluestreamlist(string flag,string uni_unit_code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getunitvaluestreamlist(flag, uni_unit_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable getUnitEquipmentlist(string flag, string uni_unit_code)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getUnitEquipmentlist(flag, uni_unit_code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getunitequipmenttypelist(string flag, string uni_unit_code,string equ_name)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getunitequipmenttypelist(flag, uni_unit_code, equ_name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getUnitEquipmentAssetlist(string flag, string uni_unit_code, string equ_name, string equ_type)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getUnitEquipmentAssetlist(flag, uni_unit_code, equ_name, equ_type);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
       

        public void saveAssetValueStream(EquipmentMasterEntity obj)
        {
            dbObj.saveAssetValueStream(obj);
        }

        public string getAllocateequipmentqty(int aa_operseq_no, string aa_unit_code, string aa_main_valstream)
        {
            var plancode = dbObj.getAllocateequipmentqty(aa_operseq_no, aa_unit_code, aa_main_valstream);
            return plancode;
        }
        public string getAssetNoTotal(string uni_unit_code, string equ_name, string equ_type, string val_valuestream_code,string opr_operation_seq)
        {
            var plancode = dbObj.getAssetNoTotal(uni_unit_code, equ_name, equ_type, val_valuestream_code, opr_operation_seq);
            return plancode;
        }
        public string getAllocateAsset(string uni_unit_code, string equ_name, string equ_type, string val_valuestream_code, string opr_operation_seq)
        {
            var plancode = dbObj.getAllocateNoTotal(uni_unit_code, equ_name, equ_type, val_valuestream_code, opr_operation_seq);
            return plancode;
        }
        public string getAvailbleAsset(string uni_unit_code, string equ_name, string equ_type, string val_valuestream_code,string opr_operation_seq)
        {
            var plancode = dbObj.getAvailbleAsset(uni_unit_code, equ_name, equ_type, val_valuestream_code, opr_operation_seq);
            return plancode;
        }
        #endregion

        #region Manage FileSubType ECN
        public int SaveOtherRequest(OtherRequestMasterEntity obj)
        {
            int i = dbObj.SaveOtherRequest(obj);
            return i;
        }
        public int SaveProfileMaster(FileSubTypeEntity obj)
        {
            int i = dbObj.SaveProfileMaster(obj);
            return i;
        }

        public int SaveImpactedSku(ImpactedSkQMasterEntity obj)
        {
            int i = dbObj.SaveImpactedSku(obj);
            return i;
        }
        public DataTable get_PartNumList(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_PartNumList(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public string editSkudate(int requestid)
        {
            var RequestID = dbObj.editSkudate(requestid);
            return RequestID;
        }
        public string MaxVerno(string Filesizecode, string filecode)
        {
            var varNo = dbObj.MaxVerno(Filesizecode, filecode);
            return varNo;
        }
        public DataTable getnewvaluefilesubtype(string flag, string sizecode, string filecode, string pm_request_type)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getnewvaluefilesubtype(flag, sizecode, filecode, pm_request_type);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable CheckEcnRequestisUpdated(string flag, string sizecode, string filecode, string reuestType)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.CheckEcnRequestisUpdated(flag, sizecode, filecode, reuestType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public int ChangeRejectRequest(OtherRequestMasterEntity obj)
        {
            int i = dbObj.ChangeRejectRequest(obj);
            return i;
        }
        public int ChangeRejectRequestProfileMaster(FileSubTypeEntity obj)
        {
            int i = dbObj.ChangeRejectRequestProfileMaster(obj);
            return i;
        }
        public int ChangeRejectRequestoldProfileMaster(FileSubTypeEntity obj)
        {
            int i = dbObj.ChangeRejectRequestoldProfileMaster(obj);
            return i;
        }
        public string requestform(int requestid)
        {
            var RequestID = dbObj.requestform(requestid);
            return RequestID;
        }
        #endregion

        #region Manage Cut Specs ECN
        public DataTable getcutspectdetailvalue(string flag, string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getcutspectdetailvalue(flag, sizecode, filecode, fileSubtypecode, cuttype, cutstandar);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public int SaveTpiMaster(TpiCutStandardsEntity obj)
        {
            int i = dbObj.SaveTpiMaster(obj);
            return i;
        }
        public DataTable get_CutSpecPartNumList(string flag, string pn_part_no)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_CutSpecPartNumList(flag, pn_part_no);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public DataTable CheckEcnCutspectRequestisUpdated(string flag, string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar, string reuestType)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.CheckEcnCutspectRequestisUpdated(flag, sizecode, filecode, fileSubtypecode, cuttype, cutstandar, reuestType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        public DataTable getecnCutSpectnewValue(string flag, string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar, string reuestType)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getecnCutSpectnewValue(flag, sizecode, filecode, fileSubtypecode, cuttype, cutstandar, reuestType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public string MaxVernoCutSpect(string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar)
        {
            var varNo = dbObj.MaxVernoCutSpect(sizecode, filecode, fileSubtypecode, cuttype, cutstandar);
            return varNo;
        }


        public string getStringName(string name, string flag)
        {
            var varNo = dbObj.getStringName(name, flag);
            return varNo;
        }

        public int ChangeRejectRequestCutSpectMaster(TpiCutStandardsEntity obj)
        {
            int i = dbObj.ChangeRejectRequestCutSpectMaster(obj);
            return i;
        }
        public DataTable get_CutSpecPartNumDetailList(string flag, int requestID)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.get_CutSpecPartNumDetailList(flag, requestID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }


        #endregion

        #region Manage Norms
        public IEnumerable<NormsMasterEntity> getnewnormlist()
        {
            DataSet ds = new DataSet();
            string flag = "NormsGrid1";
            ds = dbObj.getnewnormlist(flag);
            List<NormsMasterEntity> fileTypeList = new List<NormsMasterEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new NormsMasterEntity()
            {
                pr_RequestNo = item.Field<string>("RequestNo"),
                SKU = item.Field<string>("SKU"),
                pr_process_code = item.Field<string>("ft_process_code"),

            }).ToList();
            return fileTypeList;
        }
        public IEnumerable<NormsMasterEntity> getApprovalNormslist()
        {
            DataSet ds = new DataSet();
            string flag = "NormsGrid2";
            ds = dbObj.getnewnormlist(flag);
            List<NormsMasterEntity> fileTypeList = new List<NormsMasterEntity>();

            fileTypeList = ds.Tables[0].AsEnumerable().Select(item => new NormsMasterEntity()
            {
                pr_RequestNo = item.Field<string>("RequestNo"),
                SKU = item.Field<string>("SKU"),
                pr_process_code = item.Field<string>("ft_process_code"),

            }).ToList();
            return fileTypeList;
        }
      
        public DataTable getNormsProcesslist(string flag, string RequestId, string Requesrtype, string UnitCode,string VsCode)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getNormsProcesslist(flag,RequestId, Requesrtype, UnitCode, VsCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }

        public void SavePtypenormsvlaue(NormsMasterEntity obj)
        {
            dbObj.SavePtypenormsvlaue(obj);
        }
        public void SaveDtypenormsvlaue(NormsMasterEntity obj)
        {
            dbObj.SaveDtypenormsvlaue(obj);
        }
        public void CloseNormsDetails(NormsMasterEntity obj)
        {
            dbObj.CloseNormsDetails(obj);
        }
       public void RejectNormsDetails(NormsMasterEntity obj)
        {
            dbObj.RejectNormsDetails(obj);
        }
        public DataTable getNormsProcessViewlist(string flag, string RequestId, string Requesrtype, string UnitCode)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getNormsProcessViewlist(flag, RequestId, Requesrtype, UnitCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDAL = null;
            }
        }
        #endregion

    }
}