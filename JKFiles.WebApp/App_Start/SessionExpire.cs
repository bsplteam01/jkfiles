﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.ActionFilters
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["Role"] == null)
            {

                filterContext.Result = new RedirectResult("~/LoginPage/LoginIndex");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}