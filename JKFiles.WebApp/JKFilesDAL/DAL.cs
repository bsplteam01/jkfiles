﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JKFilesDAL
{
    public class DAL
    {
        Utils objUtils = new Utils();
        string connectionstring;

        //get FileSize Details-Dhanashree Kolpe
        public DataTable getMasters(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {                
                cmd.Parameters.AddWithValue("@flag",flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
    }
}