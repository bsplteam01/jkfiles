﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFilesDAL;
using System.Data;

namespace JKFilesBAL
{
    public class BAL
    {
        

        //call getFileSize from Data Access Layer
        public DataTable getMasters(string flag)
        {
            DAL objDAL = new DAL();
            try
            {
                return objDAL.getMasters(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }finally
            {
                objDAL = null;
            }
        }
    }
}