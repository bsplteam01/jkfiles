﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using JKFilesBAL;
using JKFIlesEntity;
using System.Data;
using System.Data.SqlClient;
using PagedList;
using JKFiles.WebApp.Models;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using JKFiles.WebApp.ActionFilters;
using System.Linq.Dynamic;
namespace JKFiles.WebApp.Controllers1
{
    [HandleError]
    [SessionTimeout]
    public class FileSubTypeController : Controller
    {
        BAL objBAL = new BAL();
        FileSubTypeEntity fst = new FileSubTypeEntity();
        // GET: FileSubType

        public ActionResult Index()
        {
            //fst.fileSubTypelist = getFilesubtype();


            //return View(fst);

            return View();
        }
        public ActionResult NewRequest()
        {

            fst.fileSubTypeNoActiveList = objBAL.fileSubTypeListinactive();

            return PartialView("_NewRequest", fst);

        }
        public ActionResult ApprovedRequest()
        {

            fst.fileSubTypeNoApproveList = objBAL.fileSubTypeListNoApprove();

            return PartialView("_ApprovedRequest", fst);

        }
        public ActionResult Filesubtypedetailsmstr(string filesizecode, string ftypecode, string filetype, string filesubtype, int? page)
        {
            //fst.fileSubTypelist = getFilesubtype();


            //return View(fst);


            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<FileSubTypeEntity> fst = null;
            FileSubTypeEntity objcutspecs = new FileSubTypeEntity();
            List<FileSubTypeEntity> cstlist = new List<FileSubTypeEntity>();

            cstlist = getFilesubtype();

            if (!String.IsNullOrEmpty(filesizecode))
            {
                ViewBag.filesizecode = filesizecode;
                cstlist = cstlist.Where(x => x.pm_fsize_code == filesizecode).ToList();
            }

            if (!String.IsNullOrEmpty(ftypecode))
            {
                ViewBag.ftypecode = ftypecode;
                cstlist = cstlist.Where(x => x.pm_file_code == ftypecode).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                cstlist = cstlist.Where(x => x.ft_ftype_desc == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(filesubtype))
            {
                ViewBag.cuttype = filesubtype;
                cstlist = cstlist.Where(x => x.pm_fstype_desc == filesubtype).ToList();
            }


            objcutspecs.FileSubTypeEntityList = cstlist;
            fst = cstlist.ToPagedList(pageIndex, pageSize);



            return PartialView("_Filesubtypedetailsmstr", fst);

        }      
        public ActionResult Filesubtypeddlmstr()
        {

            //Swapnil code
            FileSubTypeEntity objfilesubtype = new FileSubTypeEntity();
            BindProcess();
            objfilesubtype.listsizecode = get_size();
            objfilesubtype.listtype = get_type();
            objfilesubtype.listsubtype = get_subtype();
            objfilesubtype.listfiletypecode = get_filetypecode();
           
            return PartialView("_Filesubtypeddlmstr", objfilesubtype);

        }    
        public List<FileSubTypeEntity> get_filetypecode()
        {
            string flag = "getfiletypecode";
            DataTable dt = objBAL.getrawmateriallist(flag);
            FileSubTypeEntity modelobj = new FileSubTypeEntity();
            List<FileSubTypeEntity> list = new List<FileSubTypeEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileSubTypeEntity obj = new FileSubTypeEntity();
                    obj.filetypecode = (dt.Rows[i]["File_Type_Code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<FileSubTypeEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            FileSubTypeEntity modelobj = new FileSubTypeEntity();
            List<FileSubTypeEntity> list = new List<FileSubTypeEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileSubTypeEntity obj = new FileSubTypeEntity();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<FileSubTypeEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            FileSubTypeEntity modelobj = new FileSubTypeEntity();
            List<FileSubTypeEntity> list = new List<FileSubTypeEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileSubTypeEntity obj = new FileSubTypeEntity();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "FileSubtype";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "FileSubType") }, JsonRequestBehavior.AllowGet);

        }
        public List<FileSubTypeEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            FileSubTypeEntity modelobj = new FileSubTypeEntity();
            List<FileSubTypeEntity> list = new List<FileSubTypeEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileSubTypeEntity obj = new FileSubTypeEntity();
                    obj.pm_fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Customer data  
                    IList<FileSubTypeEntity> Data = getFilesubtype();
                    var customerData = (from tempcustomer in Data
                                        select tempcustomer);
                    //Sorting  
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    //Search  
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                    customerData= customerData.Where(m => m.pm_fsize_code.ToUpper().Contains(searchValue.ToUpper()) ||
                    m.pm_file_code.ToUpper().Contains(searchValue.ToUpper())  ||
                    m.ft_ftype_desc.ToUpper().Contains(searchValue.ToUpper()) ||
                    m.pm_fstype_desc.ToUpper().Contains(searchValue.ToUpper()));
                       // customerData = customerData.Where(m => m.pm_fsize_code.ToUpper().Contains(searchValue.ToUpper())
                       // || m.ft_ftype_desc.ToUpper().Contains(searchValue.ToUpper()) || m.pm_fstype_desc.ToUpper().Contains(searchValue.ToUpper()));
                    }
                    //total number of rows count   
                    recordsTotal = customerData.Count();
                    //Paging   
                    var data = customerData.Skip(skip).Take(pageSize).ToList();
                    //Returning Json Data  
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpGet]
        public ActionResult getfilesubtype(string reqno)
        {
            FileSubTypeEntity rm = new FileSubTypeEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string sizecode = values[0].Trim().ToString();
            string filecode = values[1].Trim().ToString();
            //string sizecode = "04";
            //string filecode = "FLF";
            rm.pm_fsize_code = reqno;
            //string flag = "getfilesubtypes";
            rm.fileSubTypedetaillist = getdetailssubfile(sizecode, filecode);

            return PartialView("_FileSubTypeView", rm);

        }

        public List<FileSubTypeEntity> getdetailssubfile(string sizecode, string filecode)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getfilesubtypesview";
                dt = objBAL.getfilesubtype(flag, sizecode, filecode);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        // pm_request_id = item.Field<int>("pm_request_id"),
                        // pm_request_type = item.Field<string>("pm_request_type"),
                        pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                        pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                        pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                        pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                        pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                    }
                    ).ToList();
                }
                return listfiletype;
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult viewfilesubtype(FileSubTypeEntity obj)
        {

            //UsermgmtEntity user = new UsermgmtEntity();
            //user.Decline_Reason = obj.decline_reason;
            //user.User_Id = obj.User_Id;
            //user.flag = "insertrejection";

            //objBAL.insertrejection(user);

            // return RedirectToAction("Index");
            return RedirectToAction("Index", "FileSubType");
        }

        public List<FileSubTypeEntity> getFilesubtype()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileSubTypeview";
                dt = objBAL.getFileSubTypeMasters(flag);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        //pm_request_id = item.Field<int>("pm_request_id"),
                        //pm_request_type = item.Field<string>("pm_request_type"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        fs_size_inches = item.Field<decimal>("fs_size_inches"),
                        pm_file_code = item.Field<string>("pm_file_code"),
                        pm_fsize_code = item.Field<string>("pm_fsize_code")
                    }
                    ).ToList();
                }
                return listfiletype;
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult FileSubtypeDetails(int Id,string type)
        {
            Utils objUtils = new Utils();

            string connectionstring;
            connectionstring = objUtils.getConnString();
            FileSubTypeEntity FileSubTypeModelObj = new FileSubTypeEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_fileSubType_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);

                FileSubTypeModelObj.rmcode = dt.Rows[0]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.wtinkgthousand = dt.Rows[1]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.description = dt.Rows[2]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.width = Convert.ToDecimal(dt.Rows[3]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.thikness = Convert.ToDecimal(dt.Rows[4]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.gmssku = dt.Rows[5]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.mood_length = Convert.ToDecimal(dt.Rows[6]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.lengthmm = Convert.ToDecimal(dt.Rows[7]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.lengthP1mm = Convert.ToDecimal(dt.Rows[8]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.widthatShoulder = Convert.ToDecimal(dt.Rows[9]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.thiknessatshoulder = Convert.ToDecimal(dt.Rows[10]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.widthattip = Convert.ToDecimal(dt.Rows[11]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.thiknessattip = Convert.ToDecimal(dt.Rows[12]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.pointsizeatexactlength = Convert.ToDecimal(dt.Rows[13]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.bodydimentionwidth = Convert.ToDecimal(dt.Rows[14]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.bodydimentionthikness = Convert.ToDecimal(dt.Rows[15]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.bodypointsize = Convert.ToDecimal(dt.Rows[16]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.tapperlengthmm = Convert.ToDecimal(dt.Rows[17]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.lengthaftercuttingbefore = Convert.ToDecimal(dt.Rows[18]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.atshoulder = Convert.ToDecimal(dt.Rows[19]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.onedge = Convert.ToDecimal(dt.Rows[20]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.upcut = Convert.ToDecimal(dt.Rows[21]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.overcut = Convert.ToDecimal(dt.Rows[22]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.edgecut = Convert.ToDecimal(dt.Rows[23]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.hardness = Convert.ToDecimal(dt.Rows[24]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.angel = Convert.ToDecimal(dt.Rows[25]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.radious = Convert.ToDecimal(dt.Rows[26]["pm_dim_parm_dwgvalue"].ToString());
                //fields
                FileSubTypeModelObj.fieldwidth = dt.Rows[3]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldthikness = dt.Rows[4]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldmood_length = dt.Rows[6]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldlengthmm = dt.Rows[7]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldlengthP1mm = dt.Rows[8]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldwidthatShoulder = dt.Rows[9]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldthiknessatshoulder = dt.Rows[10]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldwidthattip = dt.Rows[11]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldthiknessattip = dt.Rows[12]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldpointsizeatexactlength = dt.Rows[13]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldbodydimentionwidth = dt.Rows[14]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldbodydimentionthikness = dt.Rows[15]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldbodypointsize = dt.Rows[16]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldtapperlengthmm = dt.Rows[17]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldlengthaftercuttingbefore = dt.Rows[18]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldatshoulder = dt.Rows[19]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldonedge = dt.Rows[20]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldupcut = dt.Rows[21]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldovercut = dt.Rows[22]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldedgecut = dt.Rows[23]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldhardness = dt.Rows[24]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldangel = dt.Rows[25]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldradious = dt.Rows[26]["pm_dim_parm_name"].ToString();
                //FileSubTypeModelObj.Remarks = Convert.ToDecimal(dt.Rows[0]["pm_dim_parm_dwgvalue"].ToString());

                //FileSubTypeModelObj.rmcoderange = dt.Rows[0]["pm_dim_parm_scrnvalue"].ToString();
                //FileSubTypeModelObj.wtinkgthousandrange = dt.Rows[1]["pm_dim_parm_scrnvalue"].ToString();
                //FileSubTypeModelObj.descriptionrange = dt.Rows[2]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.widthrange = dt.Rows[3]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.thiknessrange = dt.Rows[4]["pm_dim_parm_scrnvalue"].ToString();
                //FileSubTypeModelObj.gmsskurange = dt.Rows[5]["pm_dim_parm_dwgvalue"].ToString();
                FileSubTypeModelObj.mood_lengthrange = dt.Rows[6]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.lengthmmrange = dt.Rows[7]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.lengthP1mmrange = dt.Rows[8]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.widthatShoulderrange = dt.Rows[9]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.thiknessatshoulderrange = dt.Rows[10]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.widthattiptrange = dt.Rows[11]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.thiknessattiprange = dt.Rows[12]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.pointsizeatexactlengthrange = dt.Rows[13]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.bodydimentionwidthrange = dt.Rows[14]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.bodydimentionthiknessrange = dt.Rows[15]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.bodypointsizerange = dt.Rows[16]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.tapperlengthmmrange = dt.Rows[17]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.lengthaftercuttingbeforerange = dt.Rows[18]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.atshoulderrange = dt.Rows[19]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.onedgerange = dt.Rows[20]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.upcutrange = dt.Rows[21]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.overcutrange = dt.Rows[22]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.edgecutrange = dt.Rows[23]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.hardnessrange = dt.Rows[24]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.angelrange = dt.Rows[25]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.radiousrange = dt.Rows[26]["pm_dim_parm_scrnvalue"].ToString();

                FileSubTypeModelObj.pm_request_id = Convert.ToInt16(dt.Rows[0]["pm_request_id"].ToString());
                FileSubTypeModelObj.pm_request_type = dt.Rows[0]["pm_request_type"].ToString();
                FileSubTypeModelObj.filetypecode = dt.Rows[0]["pm_ftype_code"].ToString();
                FileSubTypeModelObj.filesizecode = dt.Rows[0]["pm_fsize_code"].ToString();
                FileSubTypeModelObj.subtypename = dt.Rows[0]["pm_fstype_desc"].ToString();
                FileSubTypeModelObj.subtypecode = dt.Rows[0]["pm_fstype_code"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("FileSubtypeDetails", FileSubTypeModelObj);
        }

        [HttpPost]
        public ActionResult EditfileSubTypeDetails(FileSubTypeEntity model)
        {
            if (ModelState.IsValid)
            {
                string flg = "FileSubtype";
                int count = objBAL.checkiduniqness1(model.subtypecode, model.filesizecode,model.filetypecode, flg);
                if (count >= 1)
                {
                    TempData["msg"] = "This Code Already Exist";
                }
                else
                {
                    objBAL.filesubtypeactive(model);
                    #region Mail Sending by Dhanashree 
                    string subject = "";
                    string body = "";

                    subject = "Request No:  " + model.pm_request_type + model.pm_request_id
                        + " and " + "Production Master Update";
                    body = "Request No: " + model.pm_request_type + model.pm_request_id +
                        " By User ID " + Session["UserId"].ToString()
                        + ",File Sub Type Master updated.Needs Approval.";

                    string flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                    #endregion
                }
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult UpdatefileSubtypeApprove(int? ID, string type)
        {
            DataSet ds = new DataSet();
            //ds=objBAL.getprofileprocessCheck(ID, type);
            //if(ds.Tables[0].Rows.Count>0)
            //{
            //    char active = Convert.ToChar(ds.Tables[0].Rows[0]["ppm_isActive"]);
            //    char approve = Convert.ToChar(ds.Tables[0].Rows[0]["ppm_isApproved"]);
            //    if (active == '1' && approve == 'Y')
            //    {
                    if (ID.HasValue)
                    {
                        objBAL.UpdatefileSubtypeApprove(ID, type);
                        //return Json("Approved Successfully!");
                    }
            //    }
            //    else
            //    {
            //        return Json("Error Please Complete Process First...Then Approve!");
            //    }
            //}
            //else
            //{
            //    return Json("Error Please Complete Process First...Then Approve!");
            //}

            return Json(new { url = @Url.Action("Index", "FileSubType") }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        #region ECN written by yogesh

        [HttpGet]
        public ActionResult getFileSubtypedetails(string sizecode,string filecode)
        {
            string requesttype = "E";
            string flag1 = "checkECNUpdatedstatus";
            DataTable dt1 = objBAL.CheckEcnRequestisUpdated(flag1, sizecode, filecode, requesttype);
            List<FileSubTypeEntity> checkrequestID = new List<FileSubTypeEntity>();
          
            if (dt1.Rows.Count > 0 && dt1 != null)
            {
                checkrequestID = dt1.AsEnumerable().Select(item => new FileSubTypeEntity
                {
                    RequestID = item.Field<string>("RequestID")
                }
                   ).ToList();
                var requestNo = checkrequestID[0].RequestID;
                return Json(new { success = true, Message ="Subtype being edited in request id=", checkrequestID = requestNo }, JsonRequestBehavior.AllowGet);
            }
                DataTable dt = new DataTable();
            try
            {
                string flag = "getfilesubtypesview";
                dt = objBAL.getfilesubtype(flag, sizecode, filecode);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        // pm_request_id = item.Field<int>("pm_request_id"),
                        // pm_request_type = item.Field<string>("pm_request_type"),
                        pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                        pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                        pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                        pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                    }
                    ).ToList();
                }
                return Json(new { success = true, filedtllist = listfiletype, Message = "", sizecode = sizecode, filecode= filecode }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
               [HttpGet]
        public ActionResult getFileSubtypeCheckApproveview(string sizecode, string filecode)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getfilesubtypesApproveview";
                dt = objBAL.getfilesubtype(flag, sizecode, filecode);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        // pm_request_id = item.Field<int>("pm_request_id"),
                        // pm_request_type = item.Field<string>("pm_request_type"),
                        pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                        pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                        pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                        pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                        pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                    }
                    ).ToList();
                }
                return Json(new { success = true, filedtllist = listfiletype }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult getFileSubtypedetailsview(string sizecode, string filecode)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getfilesubtypesview";
                dt = objBAL.getfilesubtype(flag, sizecode, filecode);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        // pm_request_id = item.Field<int>("pm_request_id"),
                        // pm_request_type = item.Field<string>("pm_request_type"),
                        pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                        pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                        pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                        pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                        pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                    }
                    ).ToList();
                }
                return Json(new { success = true, filedtllist = listfiletype }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindProcess()
        {
            DataTable dt = new DataTable();
            string flag = "getOpretionmasterlist";
            dt = objBAL.getmasterList(flag);

            List<OperationMasterEntity> listopretionlist = new List<OperationMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listopretionlist = dt.AsEnumerable().Select(item => new OperationMasterEntity
                    {
                        opr_operation_seq = item.Field<Int16>("opr_operation_seq"),
                        opr_operation_name = item.Field<string>("opr_operation_name"),

                    }).ToList();
                }
            ViewData["Process"] = listopretionlist.ToList();
        }

        public ActionResult ManageSKUList(string sizecode, string filecode,string subtypCode)
        {
            FileSubTypeModel obj = new FileSubTypeModel();
            string tempCode = filecode.Substring(0, 2);
            ViewBag.sizecode = sizecode;
            ViewBag.filecode = filecode;
            ViewBag.TodayDate = DateTime.Now;
            obj.skulist = getpartnum(sizecode, tempCode, subtypCode);
           
            TempData["skulist"] = obj.skulist;
            if ((obj.skulist != null) && (obj.skulist.Any()))
            {
             DateTime Stmax = obj.skulist.Min(x => x.pn_start_date);
            DateTime Endmin = obj.skulist.Min(x => x.pn_end_date);
            DateTime todaydate = DateTime.Now;
              int cmp = Stmax.CompareTo(todaydate);
            if (cmp > 0)
            {
                ViewBag.StartMaxDate = Stmax.ToString("yyyy-MM-dd");
                // date1 is greater means date1 is comes after date2
            }
            else if (cmp < 0)
            {
                ViewBag.StartMaxDate = todaydate.ToString("yyyy-MM-dd");
                // date2 is greater means date1 is comes after date1
            }
            else
            {
                ViewBag.StartMaxDate = Stmax.ToString("yyyy-MM-dd");
                // date1 is same as date2
            }
            ViewBag.EndMaxDate = Endmin.ToString("yyyy-MM-dd");
            }
           
          
            if (!String.IsNullOrEmpty(sizecode))
            {
                ViewBag.or_request_keydata = sizecode + filecode;
            }
            ViewBag.subtypCode = subtypCode;
            if (TempData["filelist"]!=null)
            {
             ViewBag.FileSubTypelist = TempData["filelist"].ToString();
             TempData["ECNalllistvalue"]= TempData["filelist"];
            }
            return View(obj);
        }
        [HttpPost]
        public ActionResult ManageSKUList1(List<FileSubTypeModel> AllArrayValue)
        {
            var sizecode = AllArrayValue[0].pm_file_code;
            var filecode = AllArrayValue[0].pm_fsize_code;
            var subtypCode = AllArrayValue[0].filesubtype;
            TempData["filelist"] = AllArrayValue;
            return Json(new { url = @Url.Action("ManageSKUList", "FileSubType", new { sizecode = sizecode, filecode = filecode, subtypCode = subtypCode }) }, JsonRequestBehavior.AllowGet);
           // return Json(new { }, JsonRequestBehavior.AllowGet);
        }
        protected List<FileSubTypeModel> getpartnum(string sizecode, string filecode, string fileSubtypcode)
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "partnumSKUlist";
            DataTable dt = objBAL.get_PartNumList(flag);
            List<FileSubTypeModel> partnumlist = new List<FileSubTypeModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                partnumlist = dt.AsEnumerable().Select(item => new FileSubTypeModel()
                {
                    pn_part_no = item.Field<string>("pn_part_no"),
                    pn_fsize_code = item.Field<string>("pn_fsize_code"),
                    pn_ftype_code = item.Field<string>("pn_ftype_code"),
                    pn_fstype_code = item.Field<string>("pn_fstype_code"),
                   // pn_version_no = item.Field<int>("pn_version_no"),
                    pn_start_date = item.Field<DateTime>("pn_start_date"),
                    pn_end_date = item.Field<DateTime>("pn_end_date"),
                    pn_request_id = item.Field<int>("pn_request_id")

                }).ToList();
            }
                if (!String.IsNullOrEmpty(sizecode))
                {
                    partnumlist = partnumlist.Where(x => x.pn_fsize_code == sizecode).ToList();
                }
                if (!String.IsNullOrEmpty(filecode))
                {
                    partnumlist = partnumlist.Where(x => x.pn_ftype_code == filecode).ToList();
                }
                if (!String.IsNullOrEmpty(fileSubtypcode))
                {
                    partnumlist = partnumlist.Where(x => x.pn_fstype_code == fileSubtypcode).ToList();
                }
           // int count = partnumlist.Count;
                 return partnumlist;
           
               
            
        }

        [HttpPost]
        public ActionResult Exoprttoexcel(string sizecode, string filecode, string subtypCode)
        {
          
            {
                FileSubTypeModel obj = new FileSubTypeModel();

                obj.skulist = getpartnum("", "", "");
                var grid = new System.Web.UI.WebControls.GridView();
                grid.DataSource = obj.skulist;
                grid.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=Expectations.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                grid.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                ViewBag.EmployeeList = obj.skulist;
                 
                return View("Index");
            }
        }

        [HttpPost]
        public ActionResult InsertOtherRequest(string or_request_keydata,string or_effective_dt)
        {
            if (ModelState.IsValid)
            {
                List<FileSubTypeModel> ecnlist = (List<FileSubTypeModel>)TempData["ECNalllistvalue"];
                List<FileSubTypeModel> skulist = (List<FileSubTypeModel>)TempData["skulist"];
                OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
                FileSubTypeEntity Fstobj = new FileSubTypeEntity();
                //// ECN Other Requesed
                Rorobj.or_effective_dt = Convert.ToDateTime(or_effective_dt);
                Rorobj.flag = "insertOtherrequest";
                Rorobj.or_request_from = "FileSubType";
                Rorobj.or_request_keydata = or_request_keydata;
                Rorobj.or_createby = Session["User_Id"].ToString();
                Rorobj.approveby = Session["User_Id"].ToString();
                // Please Add Your session
                int i= objBAL.SaveOtherRequest(Rorobj);
                int RequestID = i;
                ImpactedSkQMasterEntity imsobj = new ImpactedSkQMasterEntity();

                //// ECN Impact SkU Data
                foreach (var item1 in skulist)
                {
                    imsobj.im_request_type = "E";
                    imsobj.im_request_id = i;
                    imsobj.im_sku = item1.pn_part_no;
                    imsobj.im_sku_verno = item1.pn_version_no;
                    imsobj.im_pnum_updated = "N";
                    imsobj.im_sku_start_date = item1.pn_start_date;
                    imsobj.im_sku_end_date = item1.pn_end_date;
                    imsobj.flag = "insertimpactedsku";
                    int k = objBAL.SaveImpactedSku(imsobj);
                }
                //// ECN IProfile Master
                foreach (var item in ecnlist)
                    {
                        string temverNo = objBAL.MaxVerno(item.pm_file_code,item.pm_fsize_code);
                    int verNo = 0;
                    if (!String.IsNullOrEmpty(temverNo))
                    {
                         verNo = Convert.ToInt32(temverNo);
                    }
                    else
                    {
                        verNo = 1;
                    }
                    if (verNo>0)
                    {
                      verNo = verNo + 1;
                    }
                    else
                    {
                        verNo = 1;
                    }
                        Fstobj.pm_dim_parm_catg = item.pm_dim_parm_catg; //tang
                        Fstobj.pm_dim_parm_name = item.pm_dim_parm_name; //length
                        Fstobj.pm_dim_parm_code = item.pm_dim_parm_code; //RHS
                        Fstobj.pm_fstype_desc = item.pm_fstype_desc; //RHS
                        Fstobj.pm_dim_parm_dwgvalue = Convert.ToDecimal(item.pm_dim_parm_dwgvalue);//3.00
                        Fstobj.pm_dim_parm_scrnvalue = item.pm_dim_parm_scrnvalue; //9.25
                        Fstobj.pm_request_id = RequestID;//1004
                        Fstobj.pm_request_type = "E"; //E
                        Fstobj.pm_dim_parm_verno=Convert.ToInt32(verNo);//1
                        Fstobj.pm_fsize_code = item.pm_file_code; //04
                        string filecode = item.pm_fsize_code;
                        Fstobj.pm_file_code = filecode.Substring(0, 2);

                        Fstobj.pm_file_code = item.pm_fsize_code;//RDF
                        Fstobj.pm_fstype_code = item.filesubtype;//F
                        Fstobj.pm_ftype_code = filecode.Substring(0, 2);//RD
                        Fstobj.flag = "insertprofilemaster";
                        int j = objBAL.SaveProfileMaster(Fstobj);
                    }
                return Json(new { url = @Url.Action("Index", "FileSubType") }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { url = @Url.Action("Index", "FileSubType") }, JsonRequestBehavior.AllowGet);
        }



        public ActionResult FileSpecECN(string partno, string keydata)
        {
            string sizecode = keydata.Substring(0, 2);
            string filecode = keydata.Substring(2, 3);
            string filetypecode = keydata.Substring(2, 2);
            string subtypCode = keydata.Substring(4, 1);
           // string CutType = keydata.Substring(5, 1);
          //  string tpi_cut_std_code = keydata.Substring(6, 2);
            string flag = "getFileType";
            string filetype = objBAL.getStringName(filetypecode, flag);
            flag = "getFileSubType";
            string subtype = objBAL.getStringName(subtypCode, flag);
            return RedirectToAction("ManageRequestUpdatedforApproval", new
            {
                sizecode = sizecode,
                filecode = filecode,
                subtypCode = subtypCode,
                Filetype = filetype,
                pm_fstype_desc = subtype
            });
        }


        public ActionResult ManageRequestUpdatedforApproval(string sizecode, string filecode, string subtypCode, string Filetype, string pm_fstype_desc)
        {
            FileSubTypeModel obj = new FileSubTypeModel();
            string tempCode = filecode.Substring(0, 2);
            obj.oldvaluelist = geoldvaluelist(sizecode, filecode);
            obj.newvaluelist = genewvaluelistforapproval(sizecode, filecode);
            //  obj.skulist = getpartnum(sizecode, tempCode, subtypCode);
            var requestID = 0;
            string EditDate = "";
            if ((obj.newvaluelist != null))
            {
                requestID = obj.newvaluelist[0].pm_request_id;
                EditDate = objBAL.editSkudate(requestID);
                obj.SkuEditDAte = EditDate;
            }
            ViewBag.requestID = requestID;
            obj.skulist = get_CutSpecPartNumDetailList(requestID);
            obj.filesizecode = sizecode;
            obj.filetypecode = filecode;
            obj.Filetype = Filetype;
            obj.pm_fstype_desc = pm_fstype_desc;
            return View(obj);
        }

        public ActionResult ManageRequestUpdated(string sizecode,string filecode,string subtypCode, string Filetype,string pm_fstype_desc)
        {
            FileSubTypeModel obj = new FileSubTypeModel();
            string tempCode = filecode.Substring(0, 2);
            obj.oldvaluelist = geoldvaluelist(sizecode, filecode);
            obj.newvaluelist = genewvaluelist(sizecode, filecode);
          //  obj.skulist = getpartnum(sizecode, tempCode, subtypCode);
            var requestID = 0;
            string EditDate = "";
            if ((obj.newvaluelist != null))
            {
                 requestID = obj.newvaluelist[0].pm_request_id;
               EditDate = objBAL.editSkudate(requestID);
                obj.SkuEditDAte = EditDate;
            }
            ViewBag.requestID = requestID;
            obj.skulist = get_CutSpecPartNumDetailList(requestID);
            obj.filesizecode = sizecode;
            obj.filetypecode = filecode;
            obj.Filetype = Filetype;
            obj.pm_fstype_desc = pm_fstype_desc;
            return View(obj);
        }


        [HttpPost]
        public ActionResult ManageRequestUpdated1(string sizecode, string filecode,string subtypCode,string Filetype,string pm_fstype_desc)
        {
            return Json(new { url = @Url.Action("ManageRequestUpdated", "FileSubType", new { sizecode = sizecode, filecode = filecode, subtypCode = subtypCode, Filetype= Filetype, pm_fstype_desc=pm_fstype_desc }) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageRequestUpdatedView1(string reqIN)
        {
            return Json(new { url = @Url.Action("Index", "NewPartReqView", new { partno = reqIN }) }, JsonRequestBehavior.AllowGet);
        }

        protected List<FileSubTypeModel> geoldvaluelist(string sizecode, string filecode)
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "getfilesubtypesview";
            DataTable dt = objBAL.getfilesubtype(flag, sizecode, filecode);
            List<FileSubTypeModel> oldvaluelist = new List<FileSubTypeModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                oldvaluelist = dt.AsEnumerable().Select(item => new FileSubTypeModel()
                {
                    pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                    pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                    pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                    pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                    pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                }
                ).ToList();
            }
            
            return oldvaluelist;

        }

        protected List<FileSubTypeModel> genewvaluelist(string sizecode, string filecode)
        {
            FileSpec objfile = new FileSpec();
            string pm_request_type = "E";
            string flag = "getfilesubtypesNewvalue";
            DataTable dt = objBAL.getnewvaluefilesubtype(flag, sizecode, filecode, pm_request_type);
            List<FileSubTypeModel> newvaluelist = new List<FileSubTypeModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                newvaluelist = dt.AsEnumerable().Select(item => new FileSubTypeModel()
                {
                    pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                    pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                    pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                    pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                    pm_request_id = item.Field<int>("pm_request_id"),
                    pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                }
                ).ToList();
            }
            return newvaluelist;
        }

        protected List<FileSubTypeModel> genewvaluelistforapproval(string sizecode, string filecode)
        {
            FileSpec objfile = new FileSpec();
            string pm_request_type = "E";
            string flag = "getfilesubtypesNewlist";
            DataTable dt = objBAL.getnewvaluefilesubtype(flag, sizecode, filecode, pm_request_type);
            List<FileSubTypeModel> newvaluelist = new List<FileSubTypeModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                newvaluelist = dt.AsEnumerable().Select(item => new FileSubTypeModel()
                {
                    pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                    pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                    pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                    pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                    pm_request_id = item.Field<int>("pm_request_id"),
                    pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                }
                ).ToList();
            }
            return newvaluelist;
        }

        public ActionResult RejectECNRequest(string RequestID,string Remarks)
        {
            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
            string requestformcheck = objBAL.requestform(Convert.ToInt32(RequestID));
            Rorobj.or_request_id = Convert.ToInt32(RequestID);
            Rorobj.flag = "RejectRequestNo";
            Rorobj.or_remarks = Remarks;
            Rorobj.or_request_type = "E";
            int i = objBAL.ChangeRejectRequest(Rorobj);
            FileSubTypeEntity Fstobj = new FileSubTypeEntity();
            if (requestformcheck == "FileSubType")
            {
                Fstobj.pm_request_id = Convert.ToInt32(RequestID);
                Fstobj.pm_request_type = "E";
                Fstobj.pm_dim_parm_isApproved = "R";
                Fstobj.flag = "RejectRequestNoprofilemaster";
                int j = objBAL.ChangeRejectRequestProfileMaster(Fstobj);
              
            }
             else if(requestformcheck == "CutSpec") 
            {
                // Add cut spaces
            }
            else if (requestformcheck == "PartNum")
            {

            }
            else
            {

            }
            return Json(new { url = @Url.Action("Index", "FileSubType") }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveECNRequest(string RequestID)
        {
            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();

            Rorobj.or_request_id = Convert.ToInt32(RequestID);
            Rorobj.or_request_type = "E";
            Rorobj.flag = "ApproveRequestNo";
            Rorobj.or_request_from = "FileSubType";
            int i = objBAL.ChangeRejectRequest(Rorobj);
            FileSubTypeEntity Fstobj = new FileSubTypeEntity();
            Fstobj.pm_request_id = Convert.ToInt32(RequestID);
            Fstobj.flag = "ApproveRequestNoprofilemaster";
            Fstobj.pm_request_type = "E";
            int j = objBAL.ChangeRejectRequestProfileMaster(Fstobj);



            //Fstobj.flag = "ApproveOldRequestNoprofilemaster";
            //int k = objBAL.ChangeRejectRequestProfileMaster(Fstobj);
            return Json(new { url = @Url.Action("Index", "FileSubType") }, JsonRequestBehavior.AllowGet);
        }

        protected List<FileSubTypeModel> get_CutSpecPartNumDetailList(int tpi_request_id)
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "CutSpectpartnumDetailsSKUlist";
            DataTable dt = objBAL.get_CutSpecPartNumDetailList(flag, tpi_request_id);
            List<FileSubTypeModel> partnumlist = new List<FileSubTypeModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                partnumlist = dt.AsEnumerable().Select(item => new FileSubTypeModel()
                {
                    im_sku = item.Field<string>("im_sku").ToString(),
                    //  im_sku_verno = item.Field<int>("im_sku_verno"),
                    im_sku_start_date = Convert.ToDateTime(item.Field<DateTime>("im_sku_start_date")),
                    im_sku_end_date = Convert.ToDateTime(item.Field<DateTime>("im_sku_end_date")
                   )

                }).ToList();
            }


            return partnumlist;
        }

        #endregion

       
    }
}