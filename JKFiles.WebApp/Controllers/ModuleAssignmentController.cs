﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Models;

namespace JKFiles.WebApp.Controllers
{
    public class ModuleAssignmentController : Controller
    {
        MenuMaster mm = new MenuMaster();
        BAL objBAL = new BAL();
        // GET: ModuleAssignment
        public ActionResult Index()
        {
            mm.listrolename = get_rolelist();
            mm.listMainmenu = get_mainmenulist();
            mm.listSubMainmenu= get_submenulist();
            return View("Index", mm);
        }


        public ActionResult SubList(string mainmenu)
        {
            List<MenuMaster> allsubmenu = new List<MenuMaster>();
            MenuMaster menu = new MenuMaster();
            allsubmenu = get_submenulist();
            mm.listSubMainmenu = allsubmenu;

            if (!String.IsNullOrEmpty(mainmenu))
            {
                ViewBag.mainmenu = mainmenu;
                allsubmenu = allsubmenu.Where(x => Convert.ToInt32(x.MainMenuID).ToString() == mainmenu).ToList();
            }
            return PartialView("_Submenulist1", allsubmenu);
        }

        [HttpPost]
        public ActionResult Save(List<MenuMaster> hobbies)
        {
            List<MenuMaster> allsubmenu = new List<MenuMaster>();
            MenuMaster menu = new MenuMaster();
            allsubmenu = get_submenulist();
            mm.listSubMainmenu = allsubmenu;
            foreach (MenuMaster hobby in hobbies)
            {
                MenuMaster updatedHobby = allsubmenu.ToList().Find(p => p.SubMenu_Id == hobby.SubMenu_Id);
                updatedHobby.IsSelected = hobby.IsSelected;
            }

          //  entities.SaveChanges();

            return RedirectToAction("Index");
        }

        public List<MenuMaster> get_submenulist()
        {
            string flag = "getsubmenuelist";
            DataTable dt = objBAL.getrolelist(flag);
            MenuMaster modelobj = new MenuMaster();
            List<MenuMaster> list = new List<MenuMaster>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MenuMaster obj = new MenuMaster();
                    obj.SubMenu_Id = Convert.ToInt32(dt.Rows[i]["SubMenu_Id"].ToString());
                    obj.SubMenu = (dt.Rows[i]["SubMenu"].ToString());
                    obj.Controller = (dt.Rows[i]["Controller"].ToString());
                    obj.Action = (dt.Rows[i]["Action"].ToString());
                    obj.MainMenuID = Convert.ToInt32(dt.Rows[i]["MainMenuID"].ToString());

                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<MenuMaster> get_rolelist()
        {
            string flag = "getrolenamelist";
            DataTable dt = objBAL.getrolelist(flag);
            MenuMaster modelobj = new MenuMaster();
            List<MenuMaster> list = new List<MenuMaster>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MenuMaster obj = new MenuMaster();
                    obj.RoleName = (dt.Rows[i]["RoleName"].ToString());
                    obj.RoleId = (dt.Rows[i]["Role_Id"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<MenuMaster> get_mainmenulist()
        {
            string flag = "getmainmenulist";
            DataTable dt = objBAL.getrolelist(flag);
            MenuMaster modelobj = new MenuMaster();
            List<MenuMaster> list = new List<MenuMaster>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MenuMaster obj = new MenuMaster();
                    obj.MainMenuID = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    obj.MainMenu = (dt.Rows[i]["MainMenu"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
    }
}