﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{

    public class AddRequestTypeController : Controller
    {
        BAL objBAL = new BAL();
        // GET: AddRequestType
        public ActionResult RequestTypeIndex()
        {
            return View();
        }

        //inserting role
        [HttpPost]
        public ActionResult RequestTypeIndex(ComplaintmgntModel Obj)
        {
            if (ModelState.IsValid)
            {
                RequestMgmtEntity req = new RequestMgmtEntity();
                req.RequestType = Obj.RequestType;
                req.RequestDesc = Obj.RequestDesc;
                req.flag = "insertrequesttype";

                objBAL.saverequesttypeDetails(req);
                return RedirectToAction("RequestTypeIndex", "AddRequestType");
            }
            return RedirectToAction("RequestTypeIndex");
        }


        //list of request grid view
        public ActionResult RequestGrid()
        {
            ComplaintmgntModel modelobj = new ComplaintmgntModel();
            modelobj.listrequest = get_request();
            var v = modelobj.listrequest;
            return PartialView("_RequestType", v.ToList());
        }

        public List<ComplaintmgntModel> get_request()
        {
            string flag = "getrequestusertype";
            DataTable dt = objBAL.getrequestlist(flag);
            ComplaintmgntModel modelobj = new ComplaintmgntModel();
            List<ComplaintmgntModel> list = new List<ComplaintmgntModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ComplaintmgntModel objRole = new ComplaintmgntModel();
                    objRole.Request_Type_Id = Convert.ToInt32(dt.Rows[i]["Request_Type_Id"].ToString());
                    objRole.RequestType = dt.Rows[i]["Request_Type"].ToString();
                    objRole.RequestDesc = dt.Rows[i]["Request_Desc"].ToString();
                    list.Add(objRole);
                }
            }
            return list.ToList();
        }
    }
}