﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class DrawingController : Controller
    {
        // GET: Drawing
        public ActionResult Index()
        {          
            ICACB_Comp();
            return View();
        }

        public double DSCALE = (600.0 / 4000.0);
        Pen BPEN = new Pen(Color.White);
        public void ICACB_Comp()
        {
            Bitmap exbitmap = new Bitmap(1000, 800);
            Graphics graphObj = Graphics.FromImage(exbitmap);
            graphObj.DrawRectangle(BPEN, (int)((1000) * DSCALE), (int)((1000) * DSCALE),
                 (int)(500 * DSCALE), (int)(500 * DSCALE));
            Response.ContentType = "image/gif";
            exbitmap.Save(Response.OutputStream, ImageFormat.Gif);
            string filename = Server.MapPath("~/Images/image1.jpeg");
            FileStream fts = new FileStream(filename, FileMode.Create);
            exbitmap.Save(fts, ImageFormat.Gif);
            fts.Close();

        }
    }
}