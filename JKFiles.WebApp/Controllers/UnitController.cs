﻿using JKFiles.WebApp.ActionFilters;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class UnitController : Controller
    {
        BAL objBAL = new BAL();
        // GET: Unit
        public ActionResult Index(string UnitID)
        {
            
            return View();
        }

        public ActionResult ManagePlantUnit()
        {
            UnitMasterModel umobj = new UnitMasterModel();
            umobj.unitlist = get_unitlist();
            umobj.plantdropdownlist = objBAL.getPlantDropdownList();
            umobj.unitdropdownlist = objBAL.getunitDropdownList();
            return PartialView("_ManagePlantUnit", umobj);
        }

        public ActionResult ManagePlantSearch(string UnitID)
        {

            UnitMasterModel umobj = new UnitMasterModel();
            umobj.unitlist = get_unitlist();
            if (!String.IsNullOrEmpty(UnitID))
            {
                umobj.unitlist = umobj.unitlist.Where(x => x.uni_unit_code == UnitID).ToList();
            }
            umobj.plantdropdownlist = objBAL.getPlantDropdownList();
            umobj.unitdropdownlist = objBAL.getunitDropdownList();
            return PartialView("_ManagePlantUnit", umobj);
        }

        public ActionResult IndexUnitTest()
        {
            UnitMasterModel umobj = new UnitMasterModel();
            umobj.unitlist = get_unitlist();
            return Json(new { success = true, unitlist = umobj.unitlist }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ExportToExcel()
        //{
        //    var gv = new GridView();
        //    gv.DataSource = get_unitlist();
        //    gv.DataBind();

        //    Response.ClearContent();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xlsx");
        //    Response.ContentType = "application/ms-excel";

        //    Response.Charset = "";
        //    StringWriter objStringWriter = new StringWriter();
        //    HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);

        //    gv.RenderControl(objHtmlTextWriter);

        //    Response.Output.Write(objStringWriter.ToString());
        //    Response.Flush();
        //    Response.End();

        //    return View("Index");

        //}
        public ActionResult test()
        {
            return View();
        }
        public List<UnitMasterModel> get_unitlist()
        {
            string flag = "unitlist";
            DataTable dt = objBAL.getUnitlist(flag);
            UnitMasterModel modelobj = new UnitMasterModel();
            List<UnitMasterModel> list = new List<UnitMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UnitMasterModel objPlant = new UnitMasterModel();
                    objPlant.uni_unit_code = dt.Rows[i]["uni_unit_code"].ToString();
                    objPlant.uni_unit_name = dt.Rows[i]["uni_unit_name"].ToString();
                    objPlant.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();
                    objPlant.uni_unit_attachTo = dt.Rows[i]["uni_unit_attachTo"].ToString();
                    objPlant.uni_unit_absentisum = Convert.ToInt32(dt.Rows[i]["uni_unit_absentisum"].ToString());
                    objPlant.uni_unit_material_downtime = Convert.ToInt32(dt.Rows[i]["uni_unit_material_downtime"].ToString());
                    list.Add(objPlant);
                }
            }
            return list.ToList();
        }

        public ActionResult ManageUnitNorm(string UnitID,string UnitType, string valueStream)
        {
            UnitMasterModel umobj = new UnitMasterModel();
            umobj.unitNormlist = get_unitnormlist();
            if (!String.IsNullOrEmpty(UnitID))
            {
                umobj.unitNormlist = umobj.unitNormlist.Where(x => x.val_unit_code == UnitID).ToList();
            }
            if (!String.IsNullOrEmpty(UnitType))
            {
                umobj.unitNormlist = umobj.unitNormlist.Where(x => x.uni_unit_type == UnitType).ToList();
            }
            if (!String.IsNullOrEmpty(valueStream))
            {
                umobj.unitNormlist = umobj.unitNormlist.Where(x => x.val_valuestream_code == valueStream).ToList();
            }
           
            return Json(new { success = true, Unitlist = umobj.unitNormlist }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageUnitNormview()
        {
            UnitMasterModel umobj = new UnitMasterModel();
            //umobj.unitNormlist = get_unitnormlist();
            umobj.unitdropdownlist = objBAL.getunitDropdownList();
            umobj.unitvaluestreamlist = objBAL.getunitvalueStreamDropdownList();
            return PartialView("_ManageUnitNormview", umobj);
        }

        public List<UnitMasterModel> get_unitnormlist()
        {
            string flag = "unitnormlist";
            DataTable dt = objBAL.getUnitnormlist(flag);
            UnitMasterModel modelobj = new UnitMasterModel();
            List<UnitMasterModel> list = new List<UnitMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UnitMasterModel objPlant = new UnitMasterModel();
                    objPlant.val_unit_code = dt.Rows[i]["val_unit_code"].ToString();
                    objPlant.uni_unit_name = dt.Rows[i]["uni_unit_name"].ToString();
                    objPlant.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();
                    objPlant.uni_unit_attachTo = dt.Rows[i]["uni_unit_attachTo"].ToString();
                    objPlant.val_valuestream_code = dt.Rows[i]["val_valuestream_code"].ToString();
                    objPlant.val_alternate_valuestream_code = dt.Rows[i]["val_alternate_valuestream_code"].ToString();
                    objPlant.ft_ftype_desc = dt.Rows[i]["ft_ftype_desc"].ToString();
                    list.Add(objPlant);
                }
            }
            return list.ToList();
        }

        public ActionResult ManageValueStream(string valueStream)
        {
            ValueStreamMasterModel umobj = new ValueStreamMasterModel();
            //List<ValueStreamMasterModel> mvslist = new List<ValueStreamMasterModel>();
            umobj.unitvaluestreamlist = get_unitvaluestremlist();

            if (!String.IsNullOrEmpty(valueStream))
            {
                umobj.unitvaluestreamlist = umobj.unitvaluestreamlist.Where(x => x.ft_ftype_code == valueStream).ToList();
            }
            umobj.valueStreamDropdownList = objBAL.getvalueStreamDropdownList();
            return PartialView("_ManagevalueStreamview", umobj);
        }

        public ActionResult ManagevalueStreamview()
        {
            ValueStreamMasterModel umobj = new ValueStreamMasterModel();
            umobj.unitvaluestreamlist = get_unitvaluestremlist();
            umobj.valueStreamDropdownList = objBAL.getvalueStreamDropdownList();
            return PartialView("_ManagevalueStreamview", umobj);
        }

        public List<ValueStreamMasterModel> get_unitvaluestremlist()
        {
            string flag = "unitvaluestreamlist";
            DataTable dt = objBAL.get_unitvaluestremlist(flag);
            ValueStreamMasterModel modelobj = new ValueStreamMasterModel();
            List<ValueStreamMasterModel> list = new List<ValueStreamMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ValueStreamMasterModel objPlant = new ValueStreamMasterModel();
                    objPlant.ft_ftype_code = dt.Rows[i]["ft_ftype_code"].ToString();
                    objPlant.ft_ftype_desc = dt.Rows[i]["ft_ftype_desc"].ToString();
                    list.Add(objPlant);
                }
            }
            return list.ToList();
        }

        [HttpPost]
        public ActionResult AddUnit(UnitMasterModel model)
        {
            if (ModelState.IsValid)
            {
                UnitMasterEntity Ancobj = new UnitMasterEntity();
                Ancobj.uni_unit_code = model.uni_unit_attachTo + model.uni_unit_code;
                Ancobj.uni_unit_name = model.uni_unit_name;
                Ancobj.uni_unit_type = "A";
                Ancobj.uni_unit_attachTo = model.uni_unit_attachTo;
                Ancobj.uni_unit_absentisum = model.uni_unit_absentisum;
                Ancobj.uni_unit_material_downtime = model.uni_unit_material_downtime;
                Ancobj.uni_createby = Session["User_Id"].ToString();
                Ancobj.uni_approveby = Session["User_Id"].ToString();
                Ancobj.flag = "InsertUnit";
                objBAL.SaveUnit(Ancobj);
                ViewBag.alert = "Record insert successfully";
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult PlantCode(string PlantID )
        {
            if (!String.IsNullOrEmpty(PlantID))
            {
                string PlantCode = objBAL.PlantforPlantCode(PlantID);

                return Json(new { success = true, PlantCode = PlantCode }, JsonRequestBehavior.AllowGet);
            }
           
        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult AddValueStream(ValueStreamMasterModel model)
        {
            if (ModelState.IsValid)
            {
                valueStreammasterEntity vsmobj = new valueStreammasterEntity();
                vsmobj.val_valuestream_code = model.val_valuestream_code;
                vsmobj.val_valuestream_name = model.val_valuestream_name;

                vsmobj.flag = "InsertValueStream";
                objBAL.SaveValueStream(vsmobj);
                ViewBag.alert = "Record insert successfully";
            }
            return RedirectToAction("Index");
        }

        public ActionResult CheckValusreamCode(string val_valuestream_code)
        {
            string valScode = objBAL.CheckValusreamCode(val_valuestream_code);
            if (valScode == "")
            {
                return Json(new { success = false, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, Message = "Please Check the Asset No repeat Allowd" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddUnitValueStream(UnitMasterEntity model)
        {
            int Valuestreamcode = objBAL.CheckValuestreamcode(model.uni_unit_code, model.val_valuestream_code);
            if (Valuestreamcode==0)
            {


                if (ModelState.IsValid)
                {
                    UnitMasterEntity umobj = new UnitMasterEntity();
                    umobj.val_unit_code = model.uni_unit_code;
                    umobj.val_valuestream_code = model.val_valuestream_code;
                    umobj.val_alternate_valuestream_code = model.val_alternate_valuestream_code;
                    umobj.val_isApproved = "Y";
                    umobj.val_isActive = true;
                    umobj.val_createby = Session["User_Id"].ToString();
                    umobj.val_approveby = Session["User_Id"].ToString();
                    umobj.flag = "InsertUnitValueStream";
                    objBAL.AddUnitValueStream(umobj);
                    return Json(new { success = true, Message = "ValueStream insert succesfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = true, Message = "This ValueStream already exists for this unit" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowValueStreamNo(string VSNo)
        {
            if (!String.IsNullOrEmpty(VSNo))
            {
                string VSmNo = objBAL.ShowValueStreamNo(VSNo);

                return Json(new { success = true, VSmNo = VSmNo }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckUnitCode(string uni_unit_code,string uni_unit_attachTo)
        {
            var unit = uni_unit_attachTo + uni_unit_code;
            string unitCode = objBAL.CheckUnitCode(unit);
            if (unitCode == "")
            {
                return Json(new { success = false, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, Message = "Please Check the UnitCode repeat not Allowd" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}