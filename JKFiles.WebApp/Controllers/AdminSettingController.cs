﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;

namespace JKFiles.WebApp.Controllers
{
    public class AdminSettingController : Controller
    {
        BAL objBAL = new BAL();
        // GET: AdminSetting
        public ActionResult AdminSettingIndex()
        {
            AdminSettingModel um = new AdminSettingModel();
            um.listset = get_adminsetlist();
            var v = um.listset;
            return View("AdminSettingIndex", v.ToList());

        }

        public List<AdminSettingModel> get_adminsetlist()
        {
            string flag = "getadminsetdetails";
            DataTable dt = objBAL.getAdminlist(flag);
            AdminSettingModel modelobj = new AdminSettingModel();
            List<AdminSettingModel> list = new List<AdminSettingModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AdminSettingModel obj = new AdminSettingModel();
                    obj.Config_Id = Convert.ToInt32(dt.Rows[i]["Config_Id"].ToString());
                    obj.Parameter_Code = (dt.Rows[i]["Parameter_Code"].ToString());
                    obj.Parameter_Desc = dt.Rows[i]["Parameter_Desc"].ToString();
                    obj.Parameter_Value = dt.Rows[i]["Parameter_Value"].ToString();
                    obj.Operator = dt.Rows[i]["Operator"].ToString();
                    obj.Operation = dt.Rows[i]["Operation"].ToString();
                    list.Add(obj);
                }
            }
            return list.ToList();
        }


        [HttpGet]
        public ActionResult getconfigdesc(int reqno)
        {
            AdminSettingModel modelobj = new AdminSettingModel();
            modelobj.Config_Id = Convert.ToInt32(reqno);
            modelobj.flag = "getadmconfig";
            DataTable d = objBAL.getconfigdetails("getadmconfig");
            //   objBAL.updateadminconfig(adm);
            return PartialView("_AdminSettingUpdate", modelobj);
        }






        //update admin setting
        [HttpPost]
        public ActionResult SaveAdminSetting(AdminSettingModel Obj)
        {
            //if (ModelState.IsValid)
            //{
            int configid = Obj.Config_Id;

            AdminsettingEntity adm = new AdminsettingEntity();
            adm.Parameter_Value = Obj.Parameter_Value;
            adm.Operator = Obj.Operator;
            adm.Operation = Obj.Operation;
            adm.Config_Id = configid;
            adm.flag = "updateoemconfig";

            objBAL.updateadminconfig(adm);

            return RedirectToAction("AdminSettingIndex", "AdminSetting");
            //}
            //return RedirectToAction("UserManagementIndex", "UserManagement");
        }
    }
}