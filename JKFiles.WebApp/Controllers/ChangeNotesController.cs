﻿using JKFiles.WebApp.ActionFilters;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class ChangeNotesController : Controller
    {


        //Test Code
        //Testing 


        BAL objBAL = new BAL();
        #region Change Note-by Dhanashree 24 May 2018

        // GET: ChangeNotes
        public ActionResult ChangeNote()
        {
            ViewBag.Current = "Changenote";
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.listchangenote = get_custapproval();
            modelobj.listchangenoteEngg = get_custapprovalEngg();
            modelobj.listinprocess = get_inprocess();
            modelobj.itemgen_approval = ItemGenerationApproval();
            modelobj.completedreqlist = get_completed_req();
            Session["user"] = "user1";
            return PartialView("_changenote", modelobj);
        }

        public List<ChangeNoteModel> get_custapproval()
        {
            string flag = "cust_approval";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.getChangeNote(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.CreatedDate = dt.Rows[i]["CreatedDate"].ToString();
                    objChangeNote.IsApproved = dt.Rows[i]["pr_isApproved"].ToString();
                    objChangeNote.dis_IsApproved = dt.Rows[i]["dis_isApproved"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }

        public List<ChangeNoteModel> get_custapprovalEngg()
        {
            string flag = "custappr_engg";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.getChangeNote(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.CreatedDate = dt.Rows[i]["CreatedDate"].ToString();
                    objChangeNote.IsApproved = dt.Rows[i]["pr_isApproved"].ToString();
                    objChangeNote.dis_IsApproved = dt.Rows[i]["dis_isApproved"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }


        public List<ChangeNoteModel> get_inprocess()
        {
            string flag = "inprocess";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.getChangeNote(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.CreatedDate = dt.Rows[i]["ApprovedDate"].ToString();
                    objChangeNote.mstr_IsApproved = dt.Rows[i]["mstr_IsApproved"].ToString();
                    objChangeNote.disp_mstr_IsApproved = dt.Rows[i]["disp_mstr_IsApproved"].ToString();
                    // objChangeNote.norms_IsApproved = dt.Rows[i]["norms_IsApproved"].ToString();
                    list.Add(objChangeNote);



                    if ((objChangeNote.mstr_IsApproved == "Y" || objChangeNote.mstr_IsApproved == "NA"
                   || objChangeNote.mstr_IsApproved == "A") && (objChangeNote.disp_mstr_IsApproved == "Y"
                   || objChangeNote.disp_mstr_IsApproved == "NA"
                   || objChangeNote.disp_mstr_IsApproved == "A"))
                    {

                    }


                }
            }
            return list.ToList();
        }

        [HttpGet]
        public ActionResult CallApproveProduction(string reqno, string appr_from)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.Appr_from = appr_from;
            string letters = string.Empty;
            string numbers = string.Empty;
            string flag = "get_sales_prod";
            foreach (char c in reqno)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            DataTable dt = objBAL.ApprovalRemarks(letters, Convert.ToInt32(numbers), flag);
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.ap_createdDate = dt.Rows[i]["ap_createdate"].ToString();
                    objChangeNote.ap_remarks = dt.Rows[i]["ap_remarks"].ToString();
                    objChangeNote.ap_User = dt.Rows[i]["UserName"].ToString();
                    objChangeNote.ap_createby = dt.Rows[i]["ap_createby"].ToString();
                    list.Add(objChangeNote);
                }
            }
            modelobj.cust_approvalremarks_sales = list.ToList();


            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_SaveApprovalProd", modelobj);
        }



        [HttpGet]
        public ActionResult CallApproveDispatch(string reqno, string appr_from)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;

            modelobj.RequestNumber = reqno;
            modelobj.Appr_from = appr_from;
            string letters = string.Empty;
            string numbers = string.Empty;
            string flag = "get_sales_disp";
            foreach (char c in reqno)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            DataTable dt = objBAL.ApprovalRemarks(letters, Convert.ToInt32(numbers), flag);
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.ap_createdDate = dt.Rows[i]["ap_createdate"].ToString();
                    objChangeNote.ap_remarks = dt.Rows[i]["ap_remarks"].ToString();
                    objChangeNote.ap_User = dt.Rows[i]["UserName"].ToString();
                    objChangeNote.ap_createby = dt.Rows[i]["ap_createby"].ToString();
                    list.Add(objChangeNote);
                }
            }
            modelobj.cust_approvalremarks_engg = list.ToList();
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_SaveApproveDispatch", modelobj);
        }

        [HttpPost]
        public ActionResult SaveApprovalProd(ChangeNoteModel modelobj, FormCollection form)
        {
            string button = form["actionType"].ToString();
            string letters = string.Empty;
            string numbers = string.Empty;
            string reqNo = modelobj.RequestNumber;
            //  string reqNo = "a1b2c3  453bba 334s ";

            foreach (char c in reqNo)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            if (button.Equals("Save"))
            {
                ApprovalPreparationEntity appr_entityobj = new ApprovalPreparationEntity();
                appr_entityobj.req_type = letters;
                appr_entityobj.req_id = Convert.ToInt32(numbers);
                appr_entityobj.Appr_from = modelobj.Appr_from;
                appr_entityobj.ap_remarks_for = "P";
                appr_entityobj.ap_remarks = modelobj.enter_remark;
                appr_entityobj.ap_createby = Session["User_Id"].ToString();
                appr_entityobj.ap_createdDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                objBAL.SaveApprovalRemarks(appr_entityobj);

                string flag = "";
                string reqno = appr_entityobj.req_type + "" + appr_entityobj.req_id;

                string subject = "";
                string body = "";


                if (modelobj.Appr_from == "S")
                {
                    subject = "Request No:  " + reqno + " and " +
                        "Submission";
                    body = "Request No: " + reqno + " By User ID " + Session["UserId"].ToString()
                        + " Generated/Re-Submitted.Needs Attention.";
                    flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
                else
                {
                    subject = "Request No:  " + reqno + " and " +
                          "Submission to Sales from EMT Production";
                    body = "Request No: " + reqno + " By User ID " + Session["UserId"].ToString()
                        + " Prod Details Submitted.Needs Attention.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", reqno, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
            }
            else if (button.Equals("Approve"))
            {
                ApprovalPreparationEntity appr_entityobj = new ApprovalPreparationEntity();
                appr_entityobj.req_type = letters;
                appr_entityobj.req_id = Convert.ToInt32(numbers);
                appr_entityobj.Appr_from = modelobj.Appr_from;
                appr_entityobj.ap_remarks_for = "P";
                if (modelobj.enter_remark != null)
                {
                    appr_entityobj.ap_remarks = modelobj.enter_remark;
                }
                else
                {
                    appr_entityobj.ap_remarks = "Production approved";
                }
                appr_entityobj.ap_createby = Session["User_Id"].ToString();
                appr_entityobj.ap_createdDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                objBAL.SaveApprovalRemarks(appr_entityobj);
                objBAL.UpdateCustomerApproval(letters, Convert.ToInt32(numbers), "final_approval_prod");

                string flag = "production";
                DataTable dtMasters = objBAL.getNewMastersList(letters, Convert.ToInt32(numbers), flag);

                List<string> newmasterlist = new List<string>();
                if (dtMasters.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMasters.Rows.Count; i++)
                    {
                        string table_name = dtMasters.Rows[i]["ma_mstrtbl_name"].ToString();
                        newmasterlist.Add(table_name);
                    }

                }

                string subject = "";
                string body = "";
                subject = "Request No:  " + appr_entityobj.req_type +
                   appr_entityobj.req_id + " and " +
                       "Customer Aproval-Production";
                body = "Request No: " + appr_entityobj.req_type +
                   appr_entityobj.req_id + " By User ID " + Session["UserId"].ToString()
                    + " Prod Details Approved.";
                body = body + "<br/>Need to process following Masters.<br/>";
               foreach(string str in newmasterlist)
                {
                    body = body + "<br/>" + str;
                }
                
                flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }

            }
            return RedirectToAction("ChangeNote");
        }


        [HttpPost]
        public ActionResult SaveApprovalDis(ChangeNoteModel modelobj, FormCollection form)
        {
            string button = form["actionType"].ToString();
            string reqNo = modelobj.RequestNumber;
            //  string reqNo = "a1b2c3  453bba 334s ";
            string letters = string.Empty;
            string numbers = string.Empty;

            foreach (char c in reqNo)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            if (button.Equals("Save"))
            {
                ApprovalPreparationEntity appr_entityobj = new ApprovalPreparationEntity();
                appr_entityobj.req_type = letters;
                appr_entityobj.req_id = Convert.ToInt32(numbers);
                appr_entityobj.Appr_from = modelobj.Appr_from;
                appr_entityobj.ap_remarks_for = "D";
                appr_entityobj.ap_remarks = modelobj.enter_remark;
                appr_entityobj.ap_createby = Session["User_Id"].ToString();
                appr_entityobj.ap_createdDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                objBAL.SaveApprovalRemarks(appr_entityobj);



                string flag = "";
                string reqno = appr_entityobj.req_type + "" + appr_entityobj.req_id;

                string subject = "";
                string body = "";


                if (modelobj.Appr_from == "S")
                {
                    subject = "Request No:  " + reqno + " and " +
                       "Submission";
                    body = "Request No: " + reqno + " By User ID " + Session["UserId"].ToString()
                        + " Disp Details Submitted.Needs Attention.";
                    flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
                else
                {
                    subject = "Request No:  " + reqno + " and " +
                             "Submission to Sales from EMT Dispatch";
                    body = "Request No: " + reqno + " By User ID " + Session["UserId"].ToString()
                        + " Generated/Re-Submitted.Needs Attention.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", reqno, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
            }
            else if (button.Equals("Approve"))
            {
                ApprovalPreparationEntity appr_entityobj = new ApprovalPreparationEntity();
                appr_entityobj.req_type = letters;
                appr_entityobj.req_id = Convert.ToInt32(numbers);
                appr_entityobj.Appr_from = modelobj.Appr_from;
                appr_entityobj.ap_remarks_for = "D";
                if (modelobj.enter_remark != null)
                {
                    appr_entityobj.ap_remarks = modelobj.enter_remark;
                }
                else
                {
                    appr_entityobj.ap_remarks = "Dispatch Approved";
                }
                appr_entityobj.ap_createby = Session["User_Id"].ToString();
                appr_entityobj.ap_createdDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                objBAL.SaveApprovalRemarks(appr_entityobj);
                objBAL.UpdateCustomerApproval(letters, Convert.ToInt32(numbers), "final_approval_disp");

                string flag = "dispatch";
                DataTable dtMasters = objBAL.getNewMastersList(letters, Convert.ToInt32(numbers), flag);

                List<string> newmasterlist = new List<string>();
                if (dtMasters.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMasters.Rows.Count; i++)
                    {
                        string table_name = dtMasters.Rows[i]["ma_mstrtbl_name"].ToString();
                        newmasterlist.Add(table_name);
                    }
                }


                string subject = "";
                string body = "";
                subject = "Request No:  " + appr_entityobj.req_type +
                   appr_entityobj.req_id + " and " +
                       "Customer Approval-Dispatch";
                body = "Request No: " + appr_entityobj.req_type +
                   appr_entityobj.req_id + " By User ID " + Session["UserId"].ToString()
                    + " Disp Details Approved.";
                body = body + "<br/>Need to process following Masters.<br/>";
                foreach (string str in newmasterlist)
                {
                    body = body + "<br/>" + str;
                }
                flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return RedirectToAction("ChangeNote");
        }


        public ActionResult OverviewProcess(string reqno)
        {
            string req = reqno;
            string letters = string.Empty;
            string numbers = string.Empty;
            string flag = "overviewprocess";
            foreach (char c in req)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            DataTable dt = objBAL.getOverviewProcess(letters, Convert.ToInt32(numbers), flag);
            return View(dt);
        }


        public ActionResult OverviewDispatch(string reqno)
        {
            string req = reqno;
            string letters = string.Empty;
            string numbers = string.Empty;
            string flag = "overviewdispatch";
            foreach (char c in req)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            DataTable dt = objBAL.getOverviewProcess(letters, Convert.ToInt32(numbers), flag);
            return View(dt);
        }


        public List<ChangeNoteModel> ItemGenerationApproval()
        {
            string flag = "itemapproval";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.getChangeNote(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.Appr_flag1 = dt.Rows[i]["pn_approver1_flg"].ToString();
                    objChangeNote.Appr_flag2 = dt.Rows[i]["pn_approver2_flg"].ToString();
                    objChangeNote.Appr_flag3 = dt.Rows[i]["pn_approver3_flg"].ToString();
                    objChangeNote.Appr_flag4 = dt.Rows[i]["pn_approver4_flg"].ToString();
                    objChangeNote.Appr_flag5 = dt.Rows[i]["pn_approver5_flg"].ToString();
                    objChangeNote.Appr_from = dt.Rows[i]["or_request_from"].ToString();
                    objChangeNote.key_data = dt.Rows[i]["or_request_keydata"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }

        [HttpGet]
        public ActionResult ApproveFlag1(string reqno, string approve1)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.approve_flag = "approve1";
            modelobj.approve_from = "app1";
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_ApproveItemGeneration", modelobj);
        }


        [HttpPost]
        public ActionResult UpdateApproval1(ChangeNoteModel objmodel, FormCollection form)
        {
            string req = objmodel.RequestNumber;
            string approveflag = objmodel.approve_flag;
            string app_from = objmodel.approve_from;
            string type = form["actionType"].ToString();
            string status = "";
            string remark = objmodel.enter_remark;
            if (type == "approve")
            {
                if (remark == "" || remark == null)
                {
                    remark = "Approved";
                }
                else
                {
                    remark = objmodel.enter_remark;
                }
                status = "Y";
            }
            else if (type == "reject")
            {
                if (remark == "" || remark == null)
                {
                    remark = "Rejected";
                }
                else
                {
                    remark = objmodel.enter_remark;
                }
                status = "R";
            }
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in req)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            string userid = Session["User_Id"].ToString();
            objBAL.UpdateFinalApproval(letters, Convert.ToInt32(numbers), approveflag, remark, status, userid);
            string flag = "";
            string subject = "";
            string body = "";
            if (approveflag == "approve1")
            {
                if (type == "approve")
                {
                    subject = "Request No:  " + req + " and " +
                           "QA Head Approval";
                    body = "Request No: " + req + " Approved by UserName " + Session["UserId"].ToString()
                        + ",Role name QA Head.Needs Approval.";
                    flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("GM_Sales_Marketing", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
                else if (type == "reject")
                {
                    subject = "Request No:  " + req + " and " +
                              "QA Head Reject";
                    body = "Request No: " + req + " has been rejected by User Name " + Session["UserId"].ToString()
                        + ",Role name QA Head.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", req, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail1 = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                    if (dtEmail1.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail1.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail2 = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail2.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail2.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
            }
            else if (approveflag == "approve2")
            {
                if (type == "approve")
                {
                    subject = "Request No:  " + req + " and " +
                              "GM Sales and Marketing Approval";
                    body = "Request No: " + req + " Approved by UserName " + Session["UserId"].ToString()
                        + ",Role name GM Sales and Marketing.Needs Approval.";
                    flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("Director", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
                else if (type == "reject")
                {
                    subject = "Request No:  " + req + " and " +
                              "GM Sales Reject";
                    body = "Request No: " + req + " has been rejected by User Name " + Session["UserId"].ToString()
                        + ",Role name GM Sales and Marketing.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", req, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail1 = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                    if (dtEmail1.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail1.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail2 = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail2.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail2.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
            }
            else if (approveflag == "approve3")
            {
                if (type == "approve")
                {
                    subject = "Request No:  " + req + " and " +
                                "Director Approval";
                    body = "Request No: " + req + " Approved by UserName " + Session["UserId"].ToString()
                        + ",Role name Director.Needs Approval.";
                    flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("CFO", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
                else if (type == "reject")
                {
                    subject = "Request No:  " + req + " and " +
                              "Director Reject";
                    body = "Request No: " + req + " has been rejected by User Name " + Session["UserId"].ToString()
                        + ",Role name Director.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", req, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail1 = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                    if (dtEmail1.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail1.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail2 = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail2.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail2.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
            }
            else if (approveflag == "approve4")
            {
                if (type == "approve")
                {
                    subject = "Request No:  " + req + " and " +
                                   "CFO Approval";
                    body = "Request No: " + req + " Approved by UserName " + Session["UserId"].ToString()
                        + ",Role name CFO.Needs Approval.";
                    flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("CEO", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
                else if (type == "reject")
                {
                    subject = "Request No:  " + req + " and " +
                              "CFO Reject";
                    body = "Request No: " + req + " has been rejected by User Name " + Session["UserId"].ToString()
                        + ",Role name CFO.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", req, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail1 = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                    if (dtEmail1.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail1.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail2 = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail2.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail2.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }

            }
            else if (approveflag == "approve5")
            {
                if (type == "approve")
                {
                    string completepartno = objBAL.getReqId(req);
                    subject = "Request No:  " + req + " and " +
                                   "CEO Approval";
                    body = "Request No: " + req + " for SKU " + completepartno +
                        " has been approved.Need to enter into SAP";
                    flag = "getAllMailId";
                    DataTable dtEmail = objBAL.GetEmailId("", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        for (int i = 0; i < dtEmail.Rows.Count; i++)
                        {
                            string email = dtEmail.Rows[i]["User_Id"].ToString();
                            objBAL.sendingmail(email, body, subject);
                        }
                    }
                }
                else if (type == "reject")
                {
                    subject = "Request No:  " + req + " and " +
                              "CEO Reject";
                    body = "Request No: " + req + " has been rejected by User Name " + Session["UserId"].ToString()
                        + ",Role name CEO.";
                    flag = "getMailByReq";
                    DataTable dtEmail = objBAL.GetEmailId("", req, flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail1 = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                    if (dtEmail1.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail1.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }

                    flag = "getMailByRole";
                    DataTable dtEmail2 = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail2.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail2.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }
            }
            return RedirectToAction("ChangeNote");
        }



        [HttpGet]
        public ActionResult ApproveFlag2(string reqno, string approve1)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.approve_flag = "approve2";
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_ApproveItemGeneration", modelobj);
        }

        [HttpGet]
        public ActionResult ApproveFlag3(string reqno, string approve1)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.approve_flag = "approve3";
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_ApproveItemGeneration", modelobj);
        }

        [HttpGet]
        public ActionResult ApproveFlag4(string reqno, string approve1)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.approve_flag = "approve4";
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_ApproveItemGeneration", modelobj);
        }

        [HttpGet]
        public ActionResult ApproveFlag5(string reqno, string approve1)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.approve_flag = "approve5";
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_ApproveItemGeneration", modelobj);
        }

        public ActionResult SendforApproval(string reqno)
        {
            string req = reqno;
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in req)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            objBAL.InsertforFinalApproval(letters, Convert.ToInt32(numbers));

            string subject = "";
            string body = "";
            subject = "Request No:  " + reqno + " and " +
                   "Submision for Final Approval";
            body = "Request No: " + reqno + " By User ID " + Session["UserId"].ToString()
                + " All Masters Approved. Needs Approval.";

            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("QA_Head", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }
            return RedirectToAction("ChangeNote");
        }




        //Completed Requests-01.06.2018-by Dhanashree
        public List<ChangeNoteModel> get_completed_req()
        {
            string flag = "completed_req_changenote";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.getChangeNote(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.PartNo = dt.Rows[i]["PartNo"].ToString();
                    string partno = dt.Rows[i]["PartNo"].ToString();
                    objChangeNote.split1 = partno.Substring(0, 2);
                    objChangeNote.split2 = partno.Substring(2, 8);
                    objChangeNote.split3 = partno.Substring(10, 3);
                    objChangeNote.SAPNo = dt.Rows[i]["SapNo"].ToString();
                    objChangeNote.Description = dt.Rows[i]["Desc"].ToString();
                    objChangeNote.ApprovedDate = dt.Rows[i]["ApprovalDate"].ToString();
                    objChangeNote.VerNo = dt.Rows[i]["VerNo"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }


        public ActionResult CallApprovalDate(string reqno)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            string flag = "approvaldates";
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in reqno)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            // return Json(custId, JsonRequestBehavior.AllowGet);
            DataTable dt = objBAL.ApprovalRemarks(letters, Convert.ToInt32(numbers), flag);
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.Name = dt.Rows[i]["Name"].ToString();
                    objChangeNote.Status = dt.Rows[i]["Status"].ToString();
                    objChangeNote.ApprovalDate = dt.Rows[i]["Date"].ToString();
                    objChangeNote.enter_remark = dt.Rows[i]["Remark"].ToString();
                    objChangeNote.ap_createby = dt.Rows[i]["ap_createby"].ToString();
                    list.Add(objChangeNote);
                }
            }
            modelobj.approvaldateslist = list.ToList();
            return PartialView("_ApprovalDates", modelobj);
        }


        //Rejection Remark-02.06.2018-by Dhanashree


        [HttpGet]
        public ActionResult RejectionRemark(string reqno, string flag)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            modelobj.flag = flag;
            return PartialView("_RejectionRemark", modelobj);
        }

        [HttpPost]
        public ActionResult SaveRejectionRemark(ChangeNoteModel objmodel)
        {
            string letters = string.Empty;
            string numbers = string.Empty;
            string remark = objmodel.enter_remark;
            string flag = objmodel.flag;
            string reqno = objmodel.RequestNumber;
            foreach (char c in reqno)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            objBAL.UpdateRejectionRemark(letters, Convert.ToInt32(numbers), remark, flag);
            return RedirectToAction("ChangeNote");
        }


        //Norms

        [HttpGet]
        public ActionResult CallNorms(string reqno)
        {
            ChangeNoteModel modelobj = new ChangeNoteModel();
            modelobj.RequestNumber = reqno;
            return PartialView("_UpdateNorms", modelobj);
        }

        [HttpPost]
        public ActionResult UpdateNorms(ChangeNoteModel objmodel)
        {
            string letters = string.Empty;
            string numbers = string.Empty;
            string flag = "update_norms";
            string reqno = objmodel.RequestNumber;
            foreach (char c in reqno)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            objBAL.ApprovalRemarks(letters, Convert.ToInt32(numbers), flag);
            return RedirectToAction("ChangeNote");
        }
        #endregion
    }
}