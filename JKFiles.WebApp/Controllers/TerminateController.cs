﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class TerminateController : Controller
    {
        BAL objBAL = new BAL();
       
        // GET: Terminate
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            ChangeNoteModel modelobj = new ChangeNoteModel();
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            // modelobj.completedreqlist = get_completed_req();
            IPagedList<ChangeNoteModel> rmm = null;
            List<ChangeNoteModel> objlist = new List<ChangeNoteModel>();
            objlist = get_completed_req();

            //if (!String.IsNullOrEmpty(sku))
            //{
            //    ViewBag.sku = sku;
            //    objlist = objlist.Where(x => x.PartNo == sku).ToList();
            //}

            //if (!String.IsNullOrEmpty(cust_sku))
            //{
            //    ViewBag.cust_sku = cust_sku;
            //    objlist = objlist.Where(x => x.CustpartNo == cust_sku).ToList();
            //}

            //if (!String.IsNullOrEmpty(custname))
            //{
            //    ViewBag.custname = custname;
            //    objlist = objlist.Where(x => x.cust_name == custname).ToList();
            //}

            //if (!String.IsNullOrEmpty(brandname))
            //{
            //    ViewBag.brandname = brandname;
            //    objlist = objlist.Where(x => x.brand_name == brandname).ToList();
            //}

            //if (!String.IsNullOrEmpty(filesize))
            //{
            //    ViewBag.filesize = filesize;
            //    objlist = objlist.Where(x => x.size_inches == filesize).ToList();
            //}

            //if (!String.IsNullOrEmpty(filetype))
            //{
            //    ViewBag.filetype = filetype;
            //    objlist = objlist.Where(x => x.file_type == filetype).ToList();
            //}

            //if (!String.IsNullOrEmpty(cuttype))
            //{
            //    ViewBag.cuttype = cuttype;
            //    objlist = objlist.Where(x => x.cut_type == cuttype).ToList();
            //}

            modelobj.completedreqlist = objlist;
            Session["page"] = page;
            rmm = objlist.ToPagedList(pageIndex, pageSize);

            return PartialView(rmm);
        }

        //Completed Requests-01.06.2018-by Dhanashree
        public List<ChangeNoteModel> get_completed_req()
        {
            string flag = "terminategrid";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.Archives(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.PartNo = dt.Rows[i]["PartNo"].ToString();
                    string partno = dt.Rows[i]["PartNo"].ToString();
                    objChangeNote.split1 = partno.Substring(0, 2);
                    objChangeNote.split2 = partno.Substring(2, 8);
                    objChangeNote.split3 = partno.Substring(10, 3);
                    objChangeNote.SAPNo = dt.Rows[i]["SapNo"].ToString();
                    objChangeNote.Description = dt.Rows[i]["Desc"].ToString();
                    objChangeNote.ApprovedDate = dt.Rows[i]["ApprovalDate"].ToString();
                    objChangeNote.VerNo = dt.Rows[i]["VerNo"].ToString();
                    objChangeNote.CustpartNo = dt.Rows[i]["CutPartNo"].ToString();
                    objChangeNote.cust_name = dt.Rows[i]["cust_name"].ToString();
                    objChangeNote.brand_name = dt.Rows[i]["brand_name"].ToString();
                    objChangeNote.size_inches = dt.Rows[i]["size_inches"].ToString();
                    objChangeNote.file_type = dt.Rows[i]["file_type"].ToString();
                    objChangeNote.cut_type = dt.Rows[i]["cut_type"].ToString();
                    objChangeNote.endDate = dt.Rows[i]["end_date"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }

        public ActionResult RunBatchProcess()
        {
            string flag = "EndofPartNo";
            DataTable dt = objBAL.RunBatchJobs(flag);

            flag = "ECNRequests";
            DataTable dt1 = objBAL.RunBatchJobs(flag);

            return RedirectToAction("Index");
        }

    }
}