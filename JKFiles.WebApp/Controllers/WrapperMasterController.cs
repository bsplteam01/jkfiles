﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Models;
using JKFiles.WebApp.ActionFilters;
using System.Globalization;
using System.IO;


namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class WrapperMasterController : Controller
    {
        BAL objBAL = new BAL();
        WrapperMasterEntity wm = new WrapperMasterEntity();
        // GET: WrapperMaster
        public ActionResult Index()
        {
            //wm.wrapperMasterEntityList = getwrappermstr();
            //return View("Index", wm);

            return View();
        }

        public ActionResult UpdateWrapperApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdateWrapperApprove(ID, type);
            }

            string subject = "";
            string body = "";
            subject = "Request No:  " + type + ID + " and " +
                   "Dispatch Master Approval";
            body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                + ",Wrapper Master Approved.";


            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }
            return Json(new { url = @Url.Action("Index", "WrapperMaster") }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "Wrapper";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "WrapperMaster") }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult EditWrapChartDetailsnoapprove(WrapChartModelEntity model)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase photo = Request.Files["file1"];
                //photo = Request.Files["FILE2"];

                if (photo != null && photo.ContentLength > 0)
                {
                    string spath = Server.MapPath("~/ProjectImages/WrapperChart");
                    var fileName = Path.GetFileName(photo.FileName);
                    photo.SaveAs(Path.Combine(spath, fileName));

                }
                if (photo.FileName != "")
                {
                    model.img = photo.FileName;
                }

                objBAL.EditWrapChartDetailsnoapprove(model);


                string subject = "";
                string body = "";

                subject = "Request No:  " + model.wm_wrappertype + model.wm_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.wm_wrappertype + model.wm_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",Wrapper Master updated.Needs Approval.";

                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return RedirectToAction("Index");

        }
        public ActionResult NewRequest()
        {

            wm.wrapperNoActiveList = wrapperNoActive();
            return PartialView("_NewRequest", wm);

        }
        public ActionResult ApprovedRequest()
        {
            wm.wrapperNoApprove = wrapperNoApprove();
            return PartialView("_ApprovedRequest", wm);

        }
        public List<WrapperMasterEntity> wrapperNoActive()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "wrapperNoActive";
                dt = objBAL.wrapperNoActive(flag);
                List<WrapperMasterEntity> rl = new List<WrapperMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new WrapperMasterEntity
                    {
                        cm_cust_name = item.Field<string>("cm_cust_name"),
                        bm_brand_name = item.Field<string>("bm_brand_name"),
                        FileTypeCode = item.Field<string>("FileTypeCode"),
                        fs_size_inches = item.Field<decimal>("fs_size_inches"),
                        wm_chart_remarks = item.Field<string>("wm_chart_remarks"),
                        wm_wrappertype = item.Field<string>("wm_wrappertype"),
                        wm_request_id = item.Field<int>("wm_request_id"),
                        wm_request_type = item.Field<string>("wm_request_type"),
                        ReqNo = item.Field<string>("wm_request_type") + item.Field<int>("wm_request_id")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<WrapperMasterEntity> wrapperNoApprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "wrapperNoApprove";
                dt = objBAL.wrapperNoApprove(flag);
                List<WrapperMasterEntity> rl = new List<WrapperMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new WrapperMasterEntity
                    {
                        cm_cust_name = item.Field<string>("cm_cust_name"),
                        bm_brand_name = item.Field<string>("bm_brand_name"),
                        FileTypeCode = item.Field<string>("FileTypeCode"),
                        fs_size_inches = item.Field<decimal>("fs_size_inches"),
                        wm_chart_remarks = item.Field<string>("wm_chart_remarks"),
                        wm_wrappertype = item.Field<string>("wm_wrappertype"),
                        wm_request_id = item.Field<int>("wm_request_id"),
                        wm_request_type = item.Field<string>("wm_request_type"),
                        ReqNo = item.Field<string>("wm_request_type") + item.Field<int>("wm_request_id")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult REQWrapperChartDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            WrapChartModelEntity WrapChartModelObj = new WrapChartModelEntity();
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_WrapperChart_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                WrapChartModelObj.chartnum = dt.Rows[0]["wm_chart_num"].ToString();

                WrapChartModelObj.color = dt.Rows[0]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.colorphantomeshade1 = dt.Rows[1]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.colorphantomeshade2 = dt.Rows[2]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.colorphantomeshade3 = dt.Rows[3]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrapperNote = dt.Rows[4]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrapperwidth = dt.Rows[5]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrapperlength = dt.Rows[6]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.UnitBePack = dt.Rows[7]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrappingThiknes = dt.Rows[8]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.printed = dt.Rows[9]["wd_parm_scrnvalue"].ToString();

                WrapChartModelObj.Remarks = dt.Rows[0]["wm_chart_remarks"].ToString();
                WrapChartModelObj.image = dt.Rows[0]["wm_chartimg_name"].ToString();
                WrapChartModelObj.wm_wrappertype = dt.Rows[0]["wm_wrappertype"].ToString();
                WrapChartModelObj.wm_fsize_id = dt.Rows[0]["wm_fsize_id"].ToString();
                WrapChartModelObj.wm_request_type = dt.Rows[0]["wm_request_type"].ToString();
                WrapChartModelObj.wm_request_id = Convert.ToInt16(dt.Rows[0]["wm_request_id"].ToString());

                WrapChartModelObj.colorrange = dt.Rows[0]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.colorphantomeshade1range = dt.Rows[1]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.colorphantomeshade2range = dt.Rows[2]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.colorphantomeshade3range = dt.Rows[3]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrapperNoterange = dt.Rows[4]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrapperwidthrange = dt.Rows[5]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrapperlengthrange = dt.Rows[6]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.UnitBePackrange = dt.Rows[7]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrappingThiknesrange = dt.Rows[8]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.printedrange = dt.Rows[9]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.img = dt.Rows[0]["wm_chartimg_name"].ToString();
                WrapChartModelObj.wm_ftype_id = dt.Rows[0]["wm_ftype_id"].ToString();
                WrapChartModelObj.wm_fstype_id = dt.Rows[0]["wm_fstype_id"].ToString();
                WrapChartModelObj.wm_cust_id = dt.Rows[0]["wm_cust_id"].ToString();
                WrapChartModelObj.wm_brand_id = dt.Rows[0]["wm_brand_id"].ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
            return PartialView("REQWrapperChartDetails", WrapChartModelObj);
        }




        public ActionResult WrapperdetailsMaster(string filesize, string wrappertype, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<WrapperMasterEntity> wm = null;
            WrapperMasterEntity wrm = new WrapperMasterEntity();
            List<WrapperMasterEntity> wrmlist = new List<WrapperMasterEntity>();

            wrmlist = getwrappermstr();

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                wrmlist = wrmlist.Where(x => x.wm_fsize_id == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(wrappertype))
            {
                ViewBag.wrappertype = wrappertype;
                wrmlist = wrmlist.Where(x => x.wm_wrappertype == wrappertype).ToList();
            }
            wrm.wrapperMasterEntityList = wrmlist;
            wm = wrmlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_WrapperdetailsMaster", wm);
        }


        public ActionResult Wrapperddlmaster()
        {


            WrapperMasterEntity obj = new WrapperMasterEntity();

            obj.listsizecode = get_size();
            obj.listwrappertype = get_wrappertype();


            return PartialView("_Wrapperddlmaster", obj);

        }

        public List<WrapperMasterEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            WrapperMasterEntity modelobj = new WrapperMasterEntity();
            List<WrapperMasterEntity> list = new List<WrapperMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WrapperMasterEntity obj = new WrapperMasterEntity();
                    obj.wm_fsize_id = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<WrapperMasterEntity> get_wrappertype()
        {
            string flag = "getwrappertype";
            DataTable dt = objBAL.getrawmateriallist(flag);
            WrapperMasterEntity modelobj = new WrapperMasterEntity();
            List<WrapperMasterEntity> list = new List<WrapperMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WrapperMasterEntity obj = new WrapperMasterEntity();
                    obj.wm_wrappertype = (dt.Rows[i]["wm_wrappertype"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }



        public List<WrapperMasterEntity> getwrappermstr()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getwrappermstr";
                dt = objBAL.getwrapperMasters(flag);
                List<WrapperMasterEntity> listfilesize = new List<WrapperMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new WrapperMasterEntity
                    {
                        wm_fsize_id = item.Field<string>("wm_fsize_id"),
                        wm_wrappertype = item.Field<string>("wm_wrappertype"),
                        wm_chart_num = item.Field<string>("wm_chart_num")

                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpGet]
        public ActionResult editWrappermaster(string reqno)
        {
            WrapperMasterEntity wp = new WrapperMasterEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string chartno = values[0];
            //string boxtype = values[1];
            // pl.pm_pallet_type = boxtype;
            wp.wm_chart_num = chartno;
            wp.wrapperMasterdetailsList = objBAL.getwrappinglistview(wp.wm_chart_num);
            //objBAL.GetinnerLabelListview(pl.pm_pallet_chartnum, pl.pm_pallet_type);
            // ob.outerboxMasterList = objBAL.GetOuterChartlistview(ob.ob_box_chartnum, ob.ob_box_type);
            return PartialView("_EditWrapper", wp);

        }

        public ActionResult GetShowWrapperdetails(string reqno, string reqtype)
        {
            WrapperMasterEntity wp = new WrapperMasterEntity();
            wp.wrapperMasterdetailsList = objBAL.GetShowWrapperdetails(reqno, reqtype);
            return PartialView("_EditWrapper", wp);

        }



        #region ADD new Wrapper

        //public PartialViewResult getwrapper(WrapChartModelEntity model)
        //{
        //    MasterReqParModel ReqParlist = new MasterReqParModel();
        //    ReqParlist.ReqParlist = objBAL.getWrappingchartList();
        //    //  return PartialView(ReqParlist);
        //    //ReqParlist.listcstmstr = getcstlist();
        //    return PartialView("_AddNewWrapper", ReqParlist);
        //}


        [HttpGet]
        public ActionResult getwrapper(WrapChartModelEntity reqno)
        {
            WrapChartModelEntity obj = new WrapChartModelEntity();
            obj.listcstmstr = getcstlist();
            obj.listtype = get_type();
            obj.listsubtype = get_subtype();
            obj.listbrandmstr = getbrandlist();
            obj.listsizecode = get_size1();
            // return PartialView("_AddNewWrapper", modelobj);
            return PartialView("_AddWrapper", obj);
        }

        [HttpPost]
        public ActionResult WrapperImgUpload()
        {
            //string directory = @"D:\Temp\";
            HttpPostedFileBase photo = Request.Files["FILE12"];
            //photo = Request.Files["FILE2"];

            if (photo != null && photo.ContentLength > 0)
            {
                string spath = Server.MapPath("~/ProjectImages/WrapperChart");
                var fileName = Path.GetFileName(photo.FileName);
                photo.SaveAs(Path.Combine(spath, fileName));

            }
            //if (Session["requestype"].ToString() == "N")
            //{
            //    return RedirectToAction("Index");
            //}
            //else
            //{
            //    return RedirectToAction("Index");
            //}
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult btnSavewrapper(WrapChartModelEntity model)
        {
            //if (ModelState.IsValid)
            //{
            //    Session["SetWrapperDetailsTab"] = model;
            //    model.wm_request_id = Convert.ToInt16(Session["requestId"]);
            //    model.wm_request_type = Convert.ToString(Session["requestype"]);
            //    model.wm_ftype_id = "SE";
            //    model.wm_fstype_id = "T";
            //    model.wm_cust_id = Session["CoustomerId"].ToString();
            //    model.wm_brand_id = Session["BrandId"].ToString();
            //    string fileSize = Session["MaxFileSizeCode"].ToString();
            //    model.wm_fsize_id = fileSize;

            //   // objBAL.NewWrapperChartSave(model);
            //}

            WrapChartModelEntity wrp = new WrapChartModelEntity();
            wrp.CustomerName = model.CustomerName;
            wrp.wm_cust_id = model.CustomerID;
            wrp.wm_brand_id = model.Brand;
            wrp.wm_fsize_id = model.fsize_code;
            wrp.wm_ftype_id = model.filetype;
            wrp.wm_fstype_id = model.filesubtype;
            wrp.chartnum = model.chartnum;
            return RedirectToAction("Index");
        }

        public ActionResult Wrapperddlmstr()
        {


            WrapChartModelEntity obj = new WrapChartModelEntity();
            obj.listcstmstr = getcstlist();
            obj.listtype = get_type();
            obj.listsubtype = get_subtype();
            obj.listbrandmstr = getbrandlist();
            obj.listsizecode = get_size1();
            return PartialView("_AddWrapperDDL", obj);

        }

        public List<WrapChartModelEntity> getcstlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcstmstr";
                dt = objBAL.getcstlist1(flag);
                List<WrapChartModelEntity> rl = new List<WrapChartModelEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new WrapChartModelEntity
                    {
                        CustomerID = item.Field<string>("cm_cust_id"),
                        CustomerName = item.Field<string>("cm_cust_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WrapChartModelEntity> get_size1()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            WrapChartModelEntity modelobj = new WrapChartModelEntity();
            List<WrapChartModelEntity> list = new List<WrapChartModelEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WrapChartModelEntity obj = new WrapChartModelEntity();
                    obj.fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<WrapChartModelEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            WrapChartModelEntity modelobj = new WrapChartModelEntity();
            List<WrapChartModelEntity> list = new List<WrapChartModelEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WrapChartModelEntity obj = new WrapChartModelEntity();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<WrapChartModelEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            WrapChartModelEntity modelobj = new WrapChartModelEntity();
            List<WrapChartModelEntity> list = new List<WrapChartModelEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WrapChartModelEntity obj = new WrapChartModelEntity();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<WrapChartModelEntity> getbrandlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getbrandmstr";
                dt = objBAL.getcstlist1(flag);
                List<WrapChartModelEntity> rl = new List<WrapChartModelEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new WrapChartModelEntity
                    {
                        Brand = item.Field<string>("bm_brand_id"),
                        bm_brand_name = item.Field<string>("bm_brand_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //////insert nw file size
        //[HttpPost]
        //public ActionResult InsertNewfilesize(WrapChartModelEntity Obj)
        //{
        //    //if (ModelState.IsValid)
        //    //{
        //    //int configid = Obj.Config_Id;

        //    OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
        //    Rorobj.or_effective_dt = DateTime.Now.Date;
        //    Rorobj.flag = "insertOtherrequestMaters";
        //    Rorobj.or_request_from = "Wrapper";
        //    Rorobj.or_request_keydata = Obj.chartnum;
        //    Rorobj.or_createby = Session["User_Id"].ToString();
        //    Rorobj.approveby = Session["User_Id"].ToString();
        //    int i = objBAL.SaveOtherRequest(Rorobj);
        //    int RequestID = i;

        //    if (RequestID != 0)
        //    {
        //        int sizemm = 25;
        //        FileSizeDetailsEntity fls = new FileSizeDetailsEntity();
        //        fls.fs_size_inches = Obj.fs_size_inches;
        //        fls.fs_size_mm = sizemm * fls.fs_size_inches;
        //        fls.fs_size_code = Obj.fs_size_code;
        //        fls.fs_size_remarks = Obj.fs_size_remarks;
        //        fls.fs_request_id = RequestID;
        //        fls.fs_size_approveby = Session["User_Id"].ToString();
        //        fls.fs_size_createby = Session["User_Id"].ToString();
        //        fls.flag = "insertFileSize";

        //        objBAL.insertfilesize(fls);


        //    }
        //    return RedirectToAction("Index", "FileSizeMaster");


        //}
        #endregion
    }
}