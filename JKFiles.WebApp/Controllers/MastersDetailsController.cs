﻿using JKFiles.WebApp.Entity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Entity;
using JKFilesDAL;
using PagedList;
using PagedList.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class MastersDetailsController : Controller
    {
        DAL dbObj = new DAL();
        #region  Masters-by Dhanashree
        BAL objBAL = new BAL();
        CustomerMasterModel custModelObj = new CustomerMasterModel();
        // GET: MastersDetails
        //public ActionResult CustomerMaster()
        //{
        //    custModelObj.custList = objBAL.getCustomerList();
        //    custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
        //    custModelObj.custNoApproveList = objBAL.getCustListNoApprove();
        //    return PartialView("_CustomerMaster", custModelObj);
        //}

        //public ActionResult BrandMaster()
        //{
        //    custModelObj.brandList = objBAL.getBrandList();
        //    custModelObj.brandNoApproveList = objBAL.brandNoApproveList();
        //    custModelObj.brandListNoActiveList = objBAL.brandListNoActiveList();
        //    return PartialView("_BrandMaster", custModelObj);
        //}



        //FileSubType Master 23.05.18
        public ActionResult FileSubType()
        {
            //ProductionMasterModel objProdModel = new ProductionMasterModel();
            //objProdModel.filesizelist = getFileSize();
            //objProdModel.filetTypelist = getFileType();

            FileSpec objfilespec = new FileSpec();
            objfilespec.FileType = "Flat Files";
            DataTable dt = objBAL.getFileSubTypeMaster(objfilespec);
            return View(dt);
        }

        //Cut Specification Master

        #endregion
        #region New Custonmer brand by swapnil
        //public ActionResult CutSpecmaster()
        //{
        //    CutSpecsEntity objcutspecs = new CutSpecsEntity();
        //    //ProductionMasterModel objProdModel = new ProductionMasterModel();
        //    //objProdModel.filesizelist = getFileSize();
        //    //objProdModel.filetTypelist = getFileType();
        //    objcutspecs.cutSpecsNoActiveList = objBAL.getCutSpecNoActiveMaster();
        //    objcutspecs.cutSpecsNoApproveList = objBAL.getCutSpecNoApproveMaster();
        //    objcutspecs.cutSpecsEntityList = objBAL.getCutSpecMaster();
        //    return View(objcutspecs);
        //}
        public ActionResult Details(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            CustomerMasterModelForActive custModelObj = new CustomerMasterModelForActive();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_customer_master_details_byid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                custModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                custModelObj.Cm_Cust_Id = dt.Rows[0]["cm_cust_id"].ToString();
                custModelObj.Cm_Cust_Name = dt.Rows[0]["cm_cust_name"].ToString();
                custModelObj.Cm_Cust_Remarks = dt.Rows[0]["cm_cust_remarks"].ToString();
                custModelObj.cm_request_id = Convert.ToInt16(dt.Rows[0]["cm_request_id"].ToString());
                custModelObj.cm_request_type = dt.Rows[0]["cm_request_type"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_Details", custModelObj);
        }
        public ActionResult REQDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            CustomerMasterModelForActive custModelObj = new CustomerMasterModelForActive();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_customer_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                custModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                custModelObj.Cm_Cust_Id = dt.Rows[0]["cm_cust_id"].ToString();
                custModelObj.Cm_Cust_Name = dt.Rows[0]["cm_cust_name"].ToString();
                custModelObj.Cm_Cust_Remarks = dt.Rows[0]["cm_cust_remarks"].ToString();
                custModelObj.cm_request_id = Convert.ToInt16(dt.Rows[0]["cm_request_id"].ToString());
                custModelObj.cm_request_type = dt.Rows[0]["cm_request_type"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_ReqCustDetails", custModelObj);
        }
        public ActionResult REQfilesubtypeDetails(int Id, string type)
        {
            Utils objUtils = new Utils();

            string connectionstring;
            connectionstring = objUtils.getConnString();
            FileSubTypeEntity FileSubTypeModelObj = new FileSubTypeEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_fileSubType_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                FileSubTypeModelObj.rmcode = dt.Rows[0]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.wtinkgthousand = dt.Rows[1]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.description = dt.Rows[2]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.width = Convert.ToDecimal(dt.Rows[3]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.thikness = Convert.ToDecimal(dt.Rows[4]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.gmssku = dt.Rows[5]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.mood_length = Convert.ToDecimal(dt.Rows[6]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.lengthmm = Convert.ToDecimal(dt.Rows[7]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.lengthP1mm = Convert.ToDecimal(dt.Rows[8]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.widthatShoulder = Convert.ToDecimal(dt.Rows[9]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.thiknessatshoulder = Convert.ToDecimal(dt.Rows[10]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.widthattip = Convert.ToDecimal(dt.Rows[11]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.thiknessattip = Convert.ToDecimal(dt.Rows[12]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.pointsizeatexactlength = Convert.ToDecimal(dt.Rows[13]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.bodydimentionwidth = Convert.ToDecimal(dt.Rows[14]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.bodydimentionthikness = Convert.ToDecimal(dt.Rows[15]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.bodypointsize = Convert.ToDecimal(dt.Rows[16]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.tapperlengthmm = Convert.ToDecimal(dt.Rows[17]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.lengthaftercuttingbefore = Convert.ToDecimal(dt.Rows[18]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.atshoulder = Convert.ToDecimal(dt.Rows[19]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.onedge = Convert.ToDecimal(dt.Rows[20]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.upcut = Convert.ToDecimal(dt.Rows[21]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.overcut = Convert.ToDecimal(dt.Rows[22]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.edgecut = Convert.ToDecimal(dt.Rows[23]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.hardness = Convert.ToDecimal(dt.Rows[24]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.angel = Convert.ToDecimal(dt.Rows[25]["pm_dim_parm_dwgvalue"].ToString());
                FileSubTypeModelObj.radious = Convert.ToDecimal(dt.Rows[26]["pm_dim_parm_dwgvalue"].ToString());
                //fields
                FileSubTypeModelObj.fieldwidth = dt.Rows[3]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldthikness = dt.Rows[4]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldmood_length = dt.Rows[6]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldlengthmm = dt.Rows[7]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldlengthP1mm = dt.Rows[8]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldwidthatShoulder = dt.Rows[9]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldthiknessatshoulder = dt.Rows[10]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldwidthattip = dt.Rows[11]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldthiknessattip = dt.Rows[12]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldpointsizeatexactlength = dt.Rows[13]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldbodydimentionwidth = dt.Rows[14]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldbodydimentionthikness = dt.Rows[15]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldbodypointsize = dt.Rows[16]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldtapperlengthmm = dt.Rows[17]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldlengthaftercuttingbefore = dt.Rows[18]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldatshoulder = dt.Rows[19]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldonedge = dt.Rows[20]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldupcut = dt.Rows[21]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldovercut = dt.Rows[22]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldedgecut = dt.Rows[23]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldhardness = dt.Rows[24]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldangel = dt.Rows[25]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldradious = dt.Rows[26]["pm_dim_parm_name"].ToString();
                FileSubTypeModelObj.fieldRemarks = dt.Rows[27]["pm_dim_parm_name"].ToString();

               

                //FileSubTypeModelObj.rmcoderange = dt.Rows[0]["pm_dim_parm_scrnvalue"].ToString();
                //FileSubTypeModelObj.wtinkgthousandrange = dt.Rows[1]["pm_dim_parm_scrnvalue"].ToString();
                //FileSubTypeModelObj.descriptionrange = dt.Rows[2]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.widthrange = dt.Rows[3]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.thiknessrange = dt.Rows[4]["pm_dim_parm_scrnvalue"].ToString();
                //FileSubTypeModelObj.gmsskurange = dt.Rows[5]["pm_dim_parm_dwgvalue"].ToString();
                FileSubTypeModelObj.mood_lengthrange = dt.Rows[6]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.lengthmmrange = dt.Rows[7]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.lengthP1mmrange = dt.Rows[8]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.widthatShoulderrange = dt.Rows[9]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.thiknessatshoulderrange = dt.Rows[10]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.widthattiptrange = dt.Rows[11]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.thiknessattiprange = dt.Rows[12]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.pointsizeatexactlengthrange = dt.Rows[13]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.bodydimentionwidthrange = dt.Rows[14]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.bodydimentionthiknessrange = dt.Rows[15]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.bodypointsizerange = dt.Rows[16]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.tapperlengthmmrange = dt.Rows[17]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.lengthaftercuttingbeforerange = dt.Rows[18]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.atshoulderrange = dt.Rows[19]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.onedgerange = dt.Rows[20]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.upcutrange = dt.Rows[21]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.overcutrange = dt.Rows[22]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.edgecutrange = dt.Rows[23]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.hardnessrange = dt.Rows[24]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.angelrange = dt.Rows[25]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.radiousrange = dt.Rows[26]["pm_dim_parm_scrnvalue"].ToString();
                FileSubTypeModelObj.Remarks = dt.Rows[27]["pm_dim_parm_scrnvalue"].ToString();

                FileSubTypeModelObj.pm_request_id = Convert.ToInt16(dt.Rows[0]["pm_request_id"].ToString());
                FileSubTypeModelObj.pm_request_type = dt.Rows[0]["pm_request_type"].ToString();
                FileSubTypeModelObj.filetypecode = dt.Rows[0]["pm_ftype_code"].ToString();
                FileSubTypeModelObj.filesizecode = dt.Rows[0]["pm_fsize_code"].ToString();
                FileSubTypeModelObj.subtypename = dt.Rows[0]["pm_fstype_desc"].ToString();
                FileSubTypeModelObj.subtypecode = dt.Rows[0]["pm_fstype_code"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("REQfilesubtypeDetails", FileSubTypeModelObj);
        }
        [HttpGet]
        public JsonResult REQCutSpecDetails(int Id, string reqtype)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            CutSpecsEntity cutspecModelObj = new CutSpecsEntity();
            CutSpecsrowwiseEntity obj = new CutSpecsrowwiseEntity();

            obj.cutSpecsEntityList = objBAL.getcutspeclistforEdit(Id, reqtype);
            return Json(obj.cutSpecsEntityList, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult REQInnerLabelDetails(int Id, string reqtype)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            //CutSpecsEntity cutspecModelObj = new CutSpecsEntity();
            InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();

            string type = reqtype;
            int id = Id;
            string C = objBAL.Checkinnerboxqty(id, type);
            obj.ReqParlist = objBAL.getinnerlableList(C);

            obj.InnerLabelDetailsEntityList = objBAL.GetinnerlabellistforEdit(Id, reqtype);
            return Json(obj, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult REQInnerBoxDetails(int Id, string reqtype)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            //InnerBoxDetailsEntity cutspecModelObj = new InnerBoxDetailsEntity();
            InnerBoxDetailsEntity obj = new InnerBoxDetailsEntity();
            obj.InnerBoxDetailsEntityList = objBAL.getInnerBoxlistforEdit(Id, reqtype);
            return Json(obj.InnerBoxDetailsEntityList, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult ReqWithdrow()
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            int id = 0;
            string type = "";
            id = Convert.ToInt16(Session["requestId"]);
            type = Convert.ToString(Session["requestype"]);
            if (!string.IsNullOrEmpty(type))
            {
                objBAL.SetReqWithdrow(id, type);
            }
            return Json(new { url = @Url.Action("ItemMasterIndex", "ItemMaster") }, JsonRequestBehavior.AllowGet);

        }
        //public ActionResult REQCutSpecDetails(int Id)
        //{
        //    Utils objUtils = new Utils();
        //    string connectionstring;
        //    connectionstring = objUtils.getConnString();
        //    CutSpecsEntity cutspecModelObj = new CutSpecsEntity();
        //    // custModelObj = CustomerMasterModelForActive.Find(Id);
        //    // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
        //    DataTable dt = new DataTable();
        //    DataTable dt1 = new DataTable();
        //    SqlConnection con = new SqlConnection(connectionstring);
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("sp_get_Cutspecs_master_details_byid", con);
        //    SqlCommand cmd1 = new SqlCommand("sp_get_Cutspecs_values_details_byid", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd1.CommandType = CommandType.StoredProcedure;
        //    try
        //    {
        //        SqlParameter param = new SqlParameter("@id", Id);
        //        SqlParameter param1 = new SqlParameter("@id", Id);
        //        cmd.Parameters.Add(param);
        //        cmd1.Parameters.Add(param1);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        //        con.Close();
        //        da.Fill(dt);
        //        da1.Fill(dt1);
        //        cutspecModelObj.fcl_ftype_code = dt.Rows[0]["File Code"].ToString();
        //        cutspecModelObj.filesize = Convert.ToDecimal(dt.Rows[0]["Nominal Length in inches"].ToString());
        //        cutspecModelObj.cuttype = dt.Rows[0]["Cut Type"].ToString();
        //        cutspecModelObj.cutStandardName = dt.Rows[0]["Cut Standard"].ToString();
        //        cutspecModelObj.upcutrange = dt.Rows[0]["UPCUT"].ToString();
        //        cutspecModelObj.edcutrange = dt.Rows[0]["EDGE"].ToString();
        //        cutspecModelObj.ovcutrange = dt.Rows[0]["OVER"].ToString();
        //        cutspecModelObj.upcutvalue = Convert.ToDecimal(dt1.Rows[0]["UPCUT"].ToString());
        //        cutspecModelObj.edcutvalue = Convert.ToDecimal(dt1.Rows[0]["EDGE"].ToString());
        //        cutspecModelObj.ovcutvalue = Convert.ToDecimal(dt1.Rows[0]["OVER"].ToString());
        //        cutspecModelObj.prc_request_id = Convert.ToInt16(dt.Rows[0]["tpi_request_id"].ToString());
        //        cutspecModelObj.prc_request_type = dt.Rows[0]["tpi_request_type"].ToString();
        //        cutspecModelObj.cutStandardCode = dt.Rows[0]["tpi_cut_std_code"].ToString();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //        con.Close();
        //        con.Dispose();
        //    }

        //    return PartialView("REQCutSpecDetails", cutspecModelObj);

        //}
        public ActionResult filetypeDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            tb_filetype_master filetypeModelObj = new tb_filetype_master();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_filetype_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                filetypeModelObj.ft_request_id = Convert.ToInt16(dt.Rows[0]["ft_request_id"].ToString());
                filetypeModelObj.ft_request_type = dt.Rows[0]["ft_request_type"].ToString();
                filetypeModelObj.ft_ftype_code = dt.Rows[0]["ft_ftype_code"].ToString();
                filetypeModelObj.ft_ftype_desc = dt.Rows[0]["ft_ftype_desc"].ToString();
                filetypeModelObj.ft_ftype_remarks = dt.Rows[0]["ft_ftype_remarks"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_filetypeDetails", filetypeModelObj);
        }
        public ActionResult REQBrandDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            BrandDetailsEntity brandModelObj = new BrandDetailsEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Brand_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                brandModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                brandModelObj.bm_brand_id = dt.Rows[0]["bm_brand_id"].ToString();
                brandModelObj.bm_brand_name = dt.Rows[0]["bm_brand_name"].ToString();
                brandModelObj.bm_brand_remarks = dt.Rows[0]["bm_brand_remarks"].ToString();
                brandModelObj.bm_request_id = Convert.ToInt16(dt.Rows[0]["bm_request_id"].ToString());
                brandModelObj.bm_request_type = dt.Rows[0]["bm_request_type"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_ReqBranddetails", brandModelObj);
        }
  
        public ActionResult REQfileSizeDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            FileSizeDetailsEntity fileSizeModelObj = new FileSizeDetailsEntity();
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_FileSize_master_details_byREQid", con);
            SqlParameter param1 = new SqlParameter("@type", type);
            cmd.Parameters.Add(param1);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                fileSizeModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                fileSizeModelObj.fs_size_inches = Convert.ToDecimal(dt.Rows[0]["fs_size_inches"].ToString());
                fileSizeModelObj.fs_size_mm = Convert.ToDecimal(dt.Rows[0]["fs_size_mm"].ToString());
                fileSizeModelObj.fs_size_code = dt.Rows[0]["fs_size_code"].ToString();
                fileSizeModelObj.fs_size_remarks = dt.Rows[0]["fs_size_remarks"].ToString();
                fileSizeModelObj.fs_request_id = Convert.ToInt16(dt.Rows[0]["fs_request_id"].ToString());
                fileSizeModelObj.fs_request_type = dt.Rows[0]["fs_request_type"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("REQfileSizeDetails", fileSizeModelObj);
        }
        public ActionResult REQRawMaterialDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            RawMaterialMastrEntity RawmaterialModelObj = new RawMaterialMastrEntity();
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_RawMaterial_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                RawmaterialModelObj.rm_fsize_id = dt.Rows[0]["rm_fsize_id"].ToString();
                RawmaterialModelObj.rm_ftype_id = dt.Rows[0]["rm_ftype_id"].ToString();
                RawmaterialModelObj.rm_fstype_id = dt.Rows[0]["rm_fstype_id"].ToString();
                RawmaterialModelObj.rm_code = dt.Rows[0]["rm_code"].ToString();
                RawmaterialModelObj.rm_desc = dt.Rows[0]["rm_desc"].ToString();
                RawmaterialModelObj.rm_material = dt.Rows[0]["rm_material"].ToString();
                RawmaterialModelObj.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[0]["rm_wtinkg_perthsnd"].ToString());
                RawmaterialModelObj.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[0]["rm_netwtingm_persku"].ToString());
                RawmaterialModelObj.rm_height = Convert.ToDecimal(dt.Rows[0]["rm_height"].ToString());
                RawmaterialModelObj.rm_width = Convert.ToDecimal(dt.Rows[0]["rm_width"].ToString());
                RawmaterialModelObj.rm_request_type = dt.Rows[0]["rm_request_type"].ToString();
                RawmaterialModelObj.rm_request_id = Convert.ToInt16(dt.Rows[0]["rm_request_id"].ToString());
                RawmaterialModelObj.rm_remarks = dt.Rows[0]["rm_remarks"].ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("REQRawMaterialDetails", RawmaterialModelObj);
        }
        public ActionResult REQWrapperChartDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            WrapChartModelEntity WrapChartModelObj = new WrapChartModelEntity();
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_WrapperChart_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                WrapChartModelObj.chartnum = dt.Rows[0]["wm_chart_num"].ToString();

                WrapChartModelObj.color = dt.Rows[0]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.colorphantomeshade1 = dt.Rows[1]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.colorphantomeshade2 = dt.Rows[2]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.colorphantomeshade3 = dt.Rows[3]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrapperNote = dt.Rows[4]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrapperwidth = dt.Rows[5]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrapperlength = dt.Rows[6]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.UnitBePack = dt.Rows[7]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.wrappingThiknes = dt.Rows[8]["wd_parm_scrnvalue"].ToString();
                WrapChartModelObj.printed = dt.Rows[9]["wd_parm_scrnvalue"].ToString();

                WrapChartModelObj.Remarks = dt.Rows[0]["wm_chart_remarks"].ToString();
                WrapChartModelObj.image = dt.Rows[0]["wm_chartimg_name"].ToString();
                WrapChartModelObj.wm_wrappertype = dt.Rows[0]["wm_wrappertype"].ToString();
                WrapChartModelObj.wm_fsize_id = dt.Rows[0]["wm_fsize_id"].ToString();
                WrapChartModelObj.wm_request_type = dt.Rows[0]["wm_request_type"].ToString();
                WrapChartModelObj.wm_request_id = Convert.ToInt16(dt.Rows[0]["wm_request_id"].ToString());
                WrapChartModelObj.wm_cust_id = dt.Rows[0]["wm_cust_id"].ToString();
                WrapChartModelObj.wm_brand_id = dt.Rows[0]["wm_brand_id"].ToString();
                WrapChartModelObj.wm_ftype_id = dt.Rows[0]["wm_ftype_id"].ToString();
                WrapChartModelObj.wm_fstype_id = dt.Rows[0]["wm_fstype_id"].ToString();
                WrapChartModelObj.colorrange = dt.Rows[0]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.colorphantomeshade1range = dt.Rows[1]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.colorphantomeshade2range = dt.Rows[2]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.colorphantomeshade3range = dt.Rows[3]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrapperNoterange = dt.Rows[4]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrapperwidthrange = dt.Rows[5]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrapperlengthrange = dt.Rows[6]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.UnitBePackrange = dt.Rows[7]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.wrappingThiknesrange = dt.Rows[8]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.printedrange = dt.Rows[9]["wd_parm_dwgvalue"].ToString();
                WrapChartModelObj.img = dt.Rows[0]["wm_chartimg_name"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
            return PartialView("REQWrapperChartDetails", WrapChartModelObj);
        }
        //public ActionResult REQInnerLabelDetails(int Id, string type)
        //{
        //    Utils objUtils = new Utils();
        //    string connectionstring;
        //    connectionstring = objUtils.getConnString();
        //    InnerLabelDetailsEntity InnerLabelModelObj = new InnerLabelDetailsEntity();
        //    DataTable dt = new DataTable();
        //    SqlConnection con = new SqlConnection(connectionstring);
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("sp_get_innerlabel_details_byREQid", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    try
        //    {
        //        SqlParameter param = new SqlParameter("@id", Id);
        //        SqlParameter param1 = new SqlParameter("@type", type);
        //        cmd.Parameters.Add(param1);
        //        cmd.Parameters.Add(param);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        con.Close();
        //        da.Fill(dt);
        //        InnerLabelModelObj.ilbd_chart_num = dt.Rows[0]["ilb_chart_num"].ToString();
        //        InnerLabelModelObj.INNERPACKINGTOPLABEL = dt.Rows[0]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGBOTTOMLABEL = dt.Rows[1]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGSIDE1LABEL = dt.Rows[2]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGSIDE2LABEL = dt.Rows[3]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGEND1LABEL = dt.Rows[4]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGEND2LABEL = dt.Rows[5]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGBOXSAMPLE = dt.Rows[6]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGLABELLOCATIONCHARTID = dt.Rows[7]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.INNERPACKINGBOARDSUPPLIER = dt.Rows[8]["ilbd_parm_scrnvalue"].ToString();
        //        InnerLabelModelObj.ilbd_request_id = Convert.ToInt16(dt.Rows[0]["ilb_request_id"].ToString());
        //        InnerLabelModelObj.ilbd_request_type = dt.Rows[0]["ilb_request_type"].ToString();
        //        InnerLabelModelObj.fs_size_code = dt.Rows[0]["ilb_fsize_code"].ToString();
        //        InnerLabelModelObj.ft_ftype_desc = dt.Rows[0]["ilb_ftype_code"].ToString();
        //        InnerLabelModelObj.pm_fstype_desc = dt.Rows[0]["ilb_fstype_code"].ToString();
        //        InnerLabelModelObj.bm_brand_name = dt.Rows[0]["ilb_brand_id"].ToString();
        //        InnerLabelModelObj.cm_cust_name = dt.Rows[0]["ilb_cust_id"].ToString();
        //        InnerLabelModelObj.fc_cut_desc = dt.Rows[0]["ilb_fcut_code"].ToString();
        //        InnerLabelModelObj.ShipToCountryCode = dt.Rows[0]["ibl_shiptocountry_cd"].ToString();







        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //        con.Close();
        //        con.Dispose();
        //    }
        //    return PartialView("REQInnerLabelDetails", InnerLabelModelObj);
        //}
        public ActionResult Brand(string Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            BrandDetailsEntity BrandModelObj = new BrandDetailsEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_brand_master_details_bycode", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@bm_request_id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                BrandModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                BrandModelObj.bm_brand_id = dt.Rows[0]["bm_brand_id"].ToString();
                BrandModelObj.bm_brand_name = dt.Rows[0]["bm_brand_name"].ToString();
                BrandModelObj.bm_brand_remarks = dt.Rows[0]["bm_brand_remarks"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_BrandDetails", BrandModelObj);
        }
        public ActionResult REQOuterChartDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            OuterDetailsEntity OuterModelObj = new OuterDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Outer_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                OuterModelObj.obd_boxdtl_recid = dt.Rows[0]["ob_box_chartnum"].ToString();
                OuterModelObj.OUTERPACKMATERIAL = dt.Rows[0]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKDESCRIPTION = dt.Rows[1]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKFINISH = dt.Rows[2]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKAPPLICATION = dt.Rows[3]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKIMG = dt.Rows[0]["ob_box_imgname"].ToString();
                OuterModelObj.OUTERPACKLENGTH = dt.Rows[4]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKWIDTH = dt.Rows[5]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKHEIGHT = dt.Rows[6]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKBATTENPOSITION = dt.Rows[7]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER1 = dt.Rows[8]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER2 = dt.Rows[9]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER3 = dt.Rows[10]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER4 = dt.Rows[11]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER5 = dt.Rows[12]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKLENGTHrange = dt.Rows[4]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OUTERPACKWIDTHrange = dt.Rows[5]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OUTERPACKHEIGHTrange = dt.Rows[6]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OUTERPACKBATTENPOSITIONrange = dt.Rows[7]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER1range = dt.Rows[8]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER2range = dt.Rows[9]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER3range = dt.Rows[10]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER4range = dt.Rows[11]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER5range = dt.Rows[12]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.ob_request_type = dt.Rows[12]["ob_request_type"].ToString();
                OuterModelObj.ob_request_id = Convert.ToInt16(dt.Rows[12]["ob_request_id"].ToString());
                OuterModelObj.ob_box_type = dt.Rows[12]["ob_box_type"].ToString();
                OuterModelObj.ob_box_fsize_code = dt.Rows[12]["ob_box_fsize_code"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("OuterChartDetails", OuterModelObj);
        }
        public ActionResult REQStampChartDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            StampDetailsEntity stampModelObj = new StampDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Stamp_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                stampModelObj.request_type = dt.Rows[0]["sm_request_type"].ToString();
                stampModelObj.request_id = Convert.ToInt16(dt.Rows[0]["sm_request_id"].ToString());
                stampModelObj.sm_cust_id = dt.Rows[0]["sm_cust_id"].ToString();
                stampModelObj.sm_brand_id = dt.Rows[0]["sm_brand_id"].ToString();
                stampModelObj.sm_ftype_id = dt.Rows[0]["sm_ftype_id"].ToString();
                stampModelObj.sm_fstype_id = dt.Rows[0]["sm_fstype_id"].ToString();
                stampModelObj.sm_fsize_id = dt.Rows[0]["sm_fsize_id"].ToString();
                stampModelObj.sm_fcut_id = dt.Rows[0]["sm_fcut_id"].ToString();
                stampModelObj.sm_stamp_mode = dt.Rows[0]["sm_stamp_mode"].ToString();
                stampModelObj.stampsize=dt.Rows[0]["sm_stmp_size"].ToString();
                stampModelObj.stamplocation=dt.Rows[0]["sm_locn"].ToString();
                stampModelObj.Orientation=dt.Rows[0]["sm_orientation"].ToString();
                stampModelObj.fontsize =dt.Rows[0]["sm_font_size"].ToString();
                stampModelObj.noofsides= dt.Rows[0]["sm_sides"].ToString();
                stampModelObj.side1image=dt.Rows[0]["sm_img1"].ToString();
                stampModelObj.side2image=dt.Rows[0]["sm_img2"].ToString();
                stampModelObj.side3image=dt.Rows[0]["sm_img3"].ToString();
                stampModelObj.side4image=dt.Rows[0]["sm_img4"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("StampChartDetails", stampModelObj);
        }
        public ActionResult REQHandleDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            HandleDetailsEntity HandleModelObj = new HandleDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_handle_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                HandleModelObj.fileTypeCode = dt.Rows[0]["hm_ftype_id"].ToString();
                HandleModelObj.FileSubType = dt.Rows[0]["hm_fstype_id"].ToString();
                HandleModelObj.fileSize = dt.Rows[0]["hm_fsize_id"].ToString();
                HandleModelObj.hm_request_type = dt.Rows[0]["hm_request_type"].ToString();
                HandleModelObj.hm_request_id = Convert.ToInt16(dt.Rows[0]["hm_request_id"].ToString());
                HandleModelObj.hm_handle_type = dt.Rows[0]["hm_handle_type"].ToString();
                HandleModelObj.tempChartNum = dt.Rows[0]["hm_chart_num"].ToString();
                HandleModelObj.hm_chartimg_name = dt.Rows[0]["hm_chartimg_name"].ToString();
                HandleModelObj.remark = dt.Rows[0]["hm_chart_remarks"].ToString();
                if (HandleModelObj.hm_handle_type != null && HandleModelObj.hm_handle_type != "")
                {
                    int flag = objBAL.GetHandlestatusApprove(HandleModelObj.hm_handle_type);
                    ViewBag.flag = flag;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("REQHandleChartDetails", HandleModelObj);
        }
        public ActionResult REQHandleTypeDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            HandleDetailsEntity HandleModelObj = new HandleDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_handle_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                HandleModelObj.fileTypeCode = dt.Rows[0]["hm_ftype_id"].ToString();
                HandleModelObj.FileSubType = dt.Rows[0]["hm_fstype_id"].ToString();
                HandleModelObj.fileSize = dt.Rows[0]["hm_fsize_id"].ToString();
                HandleModelObj.hm_request_type = dt.Rows[0]["hm_request_type"].ToString();
                HandleModelObj.hm_request_id = Convert.ToInt16(dt.Rows[0]["hm_request_id"].ToString());
                HandleModelObj.hm_handle_type = dt.Rows[0]["hm_handle_type"].ToString();
                HandleModelObj.tempChartNum = dt.Rows[0]["hm_chart_num"].ToString();
                HandleModelObj.hm_chartimg_name = dt.Rows[0]["hm_chartimg_name"].ToString();
                HandleModelObj.remark = dt.Rows[0]["hm_chart_remarks"].ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("REQHandleChartDetails", HandleModelObj);
        }
        public ActionResult REQPalletChartDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            PalletDetailsEntity PalletModelObj = new PalletDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Pallet_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                PalletModelObj.pm_pallet_chartnum = dt.Rows[0]["pm_pallet_chartnum"].ToString();
                PalletModelObj.pm_pallet_type = dt.Rows[0]["pm_pallet_type"].ToString();
                PalletModelObj.pm_pallet_chartimg = dt.Rows[0]["pm_pallet_chartimg"].ToString();
                PalletModelObj.PALLETMATERIALRANGE = dt.Rows[0]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETDESCRIPTIONRANGE = dt.Rows[1]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETTYPERANGE = dt.Rows[2]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETLENGTHRANGE = dt.Rows[3]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETWIDTHRANGE = dt.Rows[4]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHEIGHTRANGE = dt.Rows[5]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERRANGE = dt.Rows[6]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERLENGTHRANGE = dt.Rows[7]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERWIDTHRANGE = dt.Rows[8]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERHEIGHTRANGE = dt.Rows[9]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETMATERIAL = dt.Rows[0]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETDESCRIPTION = dt.Rows[1]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETTYPE = dt.Rows[2]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETLENGTH = dt.Rows[3]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETWIDTH = dt.Rows[4]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHEIGHT = dt.Rows[5]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVER = dt.Rows[6]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERLENGTH = dt.Rows[7]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERWIDTH = dt.Rows[8]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERHEIGHT = dt.Rows[9]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.pm_request_type = dt.Rows[0]["pm_request_type"].ToString();
                PalletModelObj.pm_request_id = Convert.ToInt16(dt.Rows[0]["pm_request_id"].ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("PalletChartDetails", PalletModelObj);
        }
        [HttpPost]
        public ActionResult SaveDetails(CustomerMasterModelForActive model)
        {
            string flg = "Customer";
           int count= objBAL.checkiduniqness(model.Cm_Cust_Id, flg);
            if(count>=1)
            {
                TempData["msg"] = "This Code Already Exist";
            }
            else
            {
                objBAL.Customersaveactive(model);
                string subject = "";
                string body = "";

                subject = "Request No:  " + model.cm_request_type + model.cm_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.cm_request_type + model.cm_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",Customer Master updated.Needs Approval.";

                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return RedirectToAction("Customer_Brand_Master");
        }
        [HttpPost]
        public ActionResult SaveBrandDetails(BrandDetailsEntity model)
        {
            string flg = "Brand";
            int count = objBAL.checkiduniqness(model.bm_brand_id, flg);
            if (count >= 1)
            {
                TempData["msg"] = "This Code Already Exist";
            }
            else
            {
                objBAL.Bandsaveactive(model);
                //custModelObj.custList = objBAL.getCustomerList();
                //custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
                //custModelObj.custNoApproveList = objBAL.getCustListNoApprove();

                string subject = "";
                string body = "";

                subject = "Request No:  " + model.bm_request_type + model.bm_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.bm_request_type + model.bm_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",Brand Master updated.Needs Approval.";

                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return RedirectToAction("Customer_Brand_Master");
            // return PartialView("_BrandMasterView");
        }
        public ActionResult UpdateApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.CustomersaveApprove(ID, type);
            }

            string subject = "";
            string body = "";
            subject = "Request No:  " + type + ID + " and " +
                   "Dispatch Master Approval";
            body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                + ",Customer Master Approved.";


            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }
            return Json(new { url = @Url.Action("Customer_Brand_Master", "MastersDetails") }, JsonRequestBehavior.AllowGet);

            //return RedirectToAction("Customer_Brand_Master");
        }
        public ActionResult RejectbrandApprove(int? ID, string type)
        {
            string Flag = "Brand";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Customer_Brand_Master", "MastersDetails") }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "Customer";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Customer_Brand_Master", "MastersDetails") }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateBrandApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.BrandsaveApprove(ID, type);
            }

            string subject = "";
            string body = "";
            subject = "Request No:  " + type + ID + " and " +
                   "Dispatch Master Approval";
            body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                + ",Customer Master Approved.";


            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }
            //return RedirectToAction("Customer_Brand_Master");
            return Json(new { url = @Url.Action("Customer_Brand_Master", "MastersDetails") }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Customer brand
        public ActionResult Customer_Brand_Master()
        {
            return View();
        }
        public ActionResult Customerbrandddl()
        {
            CustomerDetailsEntity cstm = new CustomerDetailsEntity();

            // rm.listrawm = get_rawlist();
            cstm.custList = getCustomerList();
            cstm.listbrandmstr = getbrandlist();
            cstm.listcountry = getcountrylist();
            return PartialView("_CstbrandDDLmaster", cstm);
        }
        //public List<RawMaterialMasterModel> get_subtype()
        //{
        //    string flag = "getfilesubtypes";
        //    DataTable dt = objBAL.getrawmateriallist(flag);
        //    RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
        //    List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
        //    if (dt.Rows.Count > 0 && dt != null)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            RawMaterialMasterModel obj = new RawMaterialMasterModel();
        //            obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
        //            list.Add(obj);
        //        }
        //    }
        //    return list.ToList();
        //}
        public ActionResult CustomerBrand(string cstname, string brand, string countryname, int? page)
        {

            //   custModelObj.custList = objBAL.getCustomerBrandList();


            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<CustomerDetailsEntity> cmb = null;
            CustomerDetailsEntity cm = new CustomerDetailsEntity();
            List<CustomerDetailsEntity> cmlist = new List<CustomerDetailsEntity>();

            cmlist = getCustomerBrandList();

            if (!String.IsNullOrEmpty(countryname))
            {
                ViewBag.countryname = countryname;
                cmlist = cmlist.Where(x => x.pr_ship2country_name == countryname).ToList();
            }

            if (!String.IsNullOrEmpty(cstname))
            {
                ViewBag.cstname = cstname;
                cmlist = cmlist.Where(x => x.pr_cust_name == cstname).ToList();
            }

            if (!String.IsNullOrEmpty(brand))
            {
                ViewBag.brand = brand;
                cmlist = cmlist.Where(x => x.pr_brand_desc == brand).ToList();
            }

            cm.custList = cmlist;
            cmb = cmlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_CustomerBrand", cmb);
            // return PartialView("_CustomerBrand", custModelObj);
        }
        public List<CustomerDetailsEntity> getCustomerBrandList()
        {
            string flag = "getcstbrandmstr";
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerbrandList(flag);
            List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
            {
                pr_ship2country_name = item.Field<string>("pr_ship2country_name"),
                pr_cust_id = item.Field<string>("pr_cust_id"),
                pr_cust_name = item.Field<string>("pr_cust_name"),
                pr_brand_id = item.Field<string>("pr_brand_id"),
                pr_brand_desc = item.Field<string>("pr_brand_desc")
            }).ToList();
            return customerList;
        }
        public ActionResult Customer()
        {

            //custModelObj.custList = objBAL.getCustomerList();
            custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
            custModelObj.custNoApproveList = objBAL.getCustListNoApprove();
            return View("_Customer", custModelObj);

            //return PartialView("_Customer", custModelObj);
        }
        //public ActionResult CustomerMasterList()
        //{
        //    custModelObj.custList = objBAL.getCustomerList();
        //    return PartialView("_CustomerMasterList", custModelObj);
        //}
        public ActionResult Cstmstrddlmaster()
        {


            CustomerDetailsEntity obj = new CustomerDetailsEntity();
            obj.cstnameList = getcstlist();


            return PartialView("_Cstmstrddlmaster", obj);

        }
        public List<CustomerDetailsEntity> getcstlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcstmstr";
                dt = objBAL.getcstlist1(flag);
                List<CustomerDetailsEntity> rl = new List<CustomerDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new CustomerDetailsEntity
                    {
                        //costomerId = item.Field<string>("cm_cust_id"),
                        pr_cust_name = item.Field<string>("cm_cust_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult CustomerMasterList(string cstname, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<CustomerDetailsEntity> cm = null;
            CustomerDetailsEntity cmm = new CustomerDetailsEntity();
            List<CustomerDetailsEntity> cstist = new List<CustomerDetailsEntity>();

            cstist = getCustomerList();

            if (!String.IsNullOrEmpty(cstname))
            {
                ViewBag.cstname = cstname;
                cstist = cstist.Where(x => x.Cm_Cust_Name == cstname).ToList();
            }


            cmm.custList = cstist;
            cm = cstist.ToPagedList(pageIndex, pageSize);

            return PartialView("_CustomerMasterList", cm);
        }
        public List<CustomerDetailsEntity> getCustomerList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerList();
            List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
            {
                Cm_Cust_Name = item.Field<string>("cm_cust_name"),
                Cm_Cust_Id = item.Field<string>("cm_cust_id")
            }).ToList();
            return customerList;
        }
        //get Countrylist
        public List<CustomerDetailsEntity> getcountrylist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcountrylist";
                dt = objBAL.getcstlist1(flag);
                List<CustomerDetailsEntity> rl = new List<CustomerDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new CustomerDetailsEntity
                    {
                        // countryId  = item.Field<string>("_id"),
                        countryName = item.Field<string>("cm_country_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get brandlist
        public List<CustomerDetailsEntity> getbrandlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getbrandmstr";
                dt = objBAL.getcstlist1(flag);
                List<CustomerDetailsEntity> rl = new List<CustomerDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new CustomerDetailsEntity
                    {
                        brandId = item.Field<string>("bm_brand_id"),
                        brandName = item.Field<string>("bm_brand_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult editcustomer(string reqno)
        {
            CustomerMasterModel cm = new CustomerMasterModel();
            cm.costomerName = reqno;
            string flag = "editcustomer";
            DataTable dt = objBAL.getcstmstrdit(flag, reqno);
            cm.costomerName = dt.Rows[0]["cm_cust_name"].ToString();
            cm.costomerId = (dt.Rows[0]["cm_cust_id"].ToString());
            return PartialView("_EditCustomer", cm);

        }
        [HttpPost]
        public ActionResult Updatercustomer(CustomerMasterModel obj)
        {

            //UsermgmtEntity user = new UsermgmtEntity();
            //user.Decline_Reason = obj.decline_reason;
            //user.User_Id = obj.User_Id;
            //user.flag = "insertrejection";

            //objBAL.insertrejection(user);

            // return RedirectToAction("Index");
            return RedirectToAction("Customer", "MastersDetails");
        }
        public ActionResult BrandMasterView()
        {

            //partial list opreation
            custModelObj.brandList = objBAL.getBrandList();
            custModelObj.brandNoApproveList = objBAL.brandNoApproveList();
            custModelObj.brandListNoActiveList = objBAL.brandListNoActiveList();
            return PartialView("_BrandMasterView", custModelObj);

            //  return PartialView("_BrandMasterView");
        }
        #endregion







        #region ADD new Customer

        [HttpGet]
        public ActionResult getcustomer(CustomerDetailsEntity reqno)
        {
            CustomerDetailsEntity modelobj = new CustomerDetailsEntity();
            return PartialView("_AddCustomerView", modelobj);
        }

        //insert new customer
        [HttpPost]
        public ActionResult InsertNewCustomer(CustomerDetailsEntity Obj)
        {
            //if (ModelState.IsValid)
            //{
            //int configid = Obj.Config_Id;

            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
            Rorobj.or_effective_dt = DateTime.Now.Date;
            Rorobj.flag = "insertOtherrequestMaters";
            Rorobj.or_request_from = "Customer";
            Rorobj.or_request_keydata = Obj.Cm_Cust_Id;
            Rorobj.or_createby = Session["User_Id"].ToString();
            Rorobj.approveby = Session["User_Id"].ToString();
            int i = objBAL.SaveOtherRequest(Rorobj);
            int RequestID = i;

            if (RequestID != 0)
            {
                //  int sizemm = 25;
                CustomerDetailsEntity fls = new CustomerDetailsEntity();
                fls.Cm_Cust_Name = Obj.Cm_Cust_Name;
                // fls.fs_size_mm = sizemm * fls.fs_size_inches;
                fls.Cm_Cust_Id = Obj.Cm_Cust_Id;
                fls.Cm_Cust_Remarks = Obj.Cm_Cust_Remarks;
                fls.cm_request_id = RequestID;
                fls.Cm_Cust_Approvedby = Session["User_Id"].ToString();
                fls.Cm_Cust_Createby = Session["User_Id"].ToString();
                fls.flag = "insertnewcustomer";

                objBAL.insertcustomer(fls);


            }
            return RedirectToAction("Customer_Brand_Master", "MastersDetails");

            //return View();


        }



        [HttpGet]
        public ActionResult getbrand(BrandDetailsEntity reqno)
        {
            BrandDetailsEntity modelobj = new BrandDetailsEntity();
            return PartialView("_AddBrandView", modelobj);
        }


        //insert new customer
        [HttpPost]
        public ActionResult InsertNewBrand(BrandDetailsEntity Obj)
        {
            //if (ModelState.IsValid)
            //{
            //int configid = Obj.Config_Id;

            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
            Rorobj.or_effective_dt = DateTime.Now.Date;
            Rorobj.flag = "insertOtherrequestMaters";
            Rorobj.or_request_from = "Brand";
            Rorobj.or_request_keydata = Obj.bm_brand_id;
            Rorobj.or_createby = Session["User_Id"].ToString();
            Rorobj.approveby = Session["User_Id"].ToString();
            int i = objBAL.SaveOtherRequest(Rorobj);
            int RequestID = i;

            if (RequestID != 0)
            {
                //  int sizemm = 25;
                BrandDetailsEntity fls = new BrandDetailsEntity();
                fls.bm_brand_name = Obj.bm_brand_name;
                // fls.fs_size_mm = sizemm * fls.fs_size_inches;
                fls.bm_brand_id = Obj.bm_brand_id;
                fls.bm_brand_remarks = Obj.bm_brand_remarks;
                fls.bm_request_id = RequestID;
                fls.bm_brand_approveby = Session["User_Id"].ToString();
                fls.bm_brand_createby = Session["User_Id"].ToString();
                fls.flag = "insertnewbrand";

                objBAL.insertbrand(fls);


            }
            return RedirectToAction("Customer_Brand_Master", "MastersDetails");

            //return View();


        }
        #endregion
    }
}