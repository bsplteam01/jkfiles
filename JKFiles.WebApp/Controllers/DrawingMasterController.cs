﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class DrawingMasterController : Controller
    {
        BAL objBAL = new BAL();
        // GET: DrawingMaster
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewRequest()
        {
            DrawingLibraryModel objDwgModel = new DrawingLibraryModel();
            objDwgModel.new_request_list = NewRequests();
            return PartialView("_NewRequest", objDwgModel);

        }

        public List<DrawingLibraryModel> NewRequests()
        {
            string flag = "new_requests";
            DataTable dt = objBAL.DrawingMaster(flag);
            List<DrawingLibraryModel> new_req = new List<DrawingLibraryModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                new_req = dt.AsEnumerable().Select(item => new DrawingLibraryModel()
                {
                    req_no = item.Field<string>("ReqNo"),
                    prod_sku = item.Field<string>("pn_tmp_itemno")
                }).ToList();
            }
            return new_req;
        }

        public ActionResult SendforApproval(string prod_sku, string req_no)
        {
            string flag = "approve_new_request";
            objBAL.SendForApprovalNewRequest(prod_sku, flag,"","","");
            #region Mail Sending by Dhanashree 

            string subject = "";
            string body = "";

            subject = "Request No:  " + req_no
                + " and " + "Production Master Update";
            body = "Request No: " + req_no+
                " By User ID " + Session["UserId"].ToString()
                + ",Drawing Master updated.Needs Approval.";

            flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email,body,subject);
            }
            #endregion
            return RedirectToAction("Index");
        }
        public ActionResult ApprovedRequest()
        {
            DrawingLibraryModel objDwgModel = new DrawingLibraryModel();
            objDwgModel.approved_records_list = ApprovedRecords();
            return PartialView("_RecordsforApproval", objDwgModel);
        }

        public List<DrawingLibraryModel> ApprovedRecords()
        {
            string flag = "approved_requests";
            DataTable dt = objBAL.DrawingMaster(flag);
            List<DrawingLibraryModel> approved_req = new List<DrawingLibraryModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                approved_req = dt.AsEnumerable().Select(item => new DrawingLibraryModel()
                {
                    req_no = item.Field<string>("ReqNo"),
                    prod_sku = item.Field<string>("pn_tmp_itemno"),
                    dwg_pdfname= item.Field<string>("dwg_pdfname")
                }).ToList();
            }
            return approved_req;
        }

        [HttpGet]
        public ActionResult Approve(string req_no)
        {
            DrawingLibraryModel objDwgModel = new DrawingLibraryModel();
            objDwgModel.req_no = req_no;
            return PartialView("_Approval",objDwgModel);
        }

        [HttpPost]
        public ActionResult UpdateApproval(DrawingLibraryModel objmodel, FormCollection form)
        {
            string req = objmodel.req_no;
            string type = form["actionType"].ToString();
            string status = "";
            string remark = objmodel.dwg_remarks;
            if (type == "approve")
            {
                if (remark == "" || remark == null)
                {
                    remark = "Approved";
                }
                else
                {
                    remark = objmodel.dwg_remarks;
                }
                status = "Y";
            }
            else if (type == "reject")
            {
                if (remark == "" || remark == null)
                {
                    remark = "Rejected";
                }
                else
                {
                    remark = objmodel.dwg_remarks;
                }
                status = "N";
            }
            string flag = "final_approve";
            objBAL.SendForApprovalNewRequest("", flag, req,remark,status);
            string subject = "";
            string body = "";
            if (type == "approve")
            {
                subject = "Request No:  " + req + " and " +
                       "Production Master Approval";
                body = "Request No: " + req + " By User ID " + Session["UserId"].ToString()
                    + ",Drawing Master Approved.";
               
            }

            else if (type == "reject")
            {
                subject = "Request No:  " + req + " and " +
                       "Production Master Reject";
                body = "Request No: " + req + " By User ID " + Session["UserId"].ToString()
                    + ",Drawing Master Rejected.";

            }
            flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }






            return RedirectToAction("Index");
        }
    }
}