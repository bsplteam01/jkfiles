﻿using JKFiles.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFilesBAL;
using JKFIlesEntity;
using System.Data.SqlClient;
using System.Data;
using JKFilesDAL;
using JKFIlesEntity;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class SKUSelectionController : Controller
    {
        jkfiles_dbEntities db = new jkfiles_dbEntities();

        // GET: SKUSelection
        BAL objBAL = new BAL();
        DAL dbObj = new DAL();
       
        
        public ActionResult getSKUSelection(string pn_part_no)
        {
            SkuSelectionModel  skuModelObj = new SkuSelectionModel();
            PartNumMasterEntity entity = new PartNumMasterEntity();
            SkuSelectionEntity sseobj = new SkuSelectionEntity();
            sseobj.skudetailslist = objBAL.GetSkuDetails(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            ViewBag.ReqNo = ReqNo;
            entity.costomerId = Session["CoustomerId"].ToString();
            string BrandID = Session["BrandId"].ToString();
            Session["MaxFileSizeCode"] = "";
            if ((sseobj.skudetailslist != null) && (sseobj.skudetailslist.Any()))
            {
                string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
              var maxcode= maxsize.Substring(2);
                var f = maxcode[0];
                var l= maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a+b;
                Session["MaxFileSizeCode"] = Mxcode;
                ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
                ViewBag.ReqNo = ReqNo;
                entity.costomerId = Session["CoustomerId"].ToString();
                BrandID = Session["BrandId"].ToString();
                skuModelObj.partlist = objBAL.getPartNumList(entity.costomerId);
                var obj = skuModelObj.partlistDetaillist = objBAL.getpartlistDetaillist(pn_part_no);
                ViewBag.abc = obj;
                ViewBag.Savebtn = "S";
                ViewBag.skudetailslist = sseobj.skudetailslist;
                return View(skuModelObj);
            }
            else
            {
                ViewBag.Savebtn = "N";

                ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            ViewBag.ReqNo = ReqNo;
            entity.costomerId = Session["CoustomerId"].ToString();
            BrandID = Session["BrandId"].ToString();
            skuModelObj.partlist = objBAL.getPartNumList(entity.costomerId);
            var obj = skuModelObj.partlistDetaillist = objBAL.getpartlistDetaillist(pn_part_no);
            ViewBag.abc = obj;
            return View(skuModelObj);
}
        }
        [HttpGet]
        public JsonResult getSkuDetails(string  pn_part_no)
        {
            SkuSelectionModel skuobj = new Models.SkuSelectionModel();
            PartNumMasterEntity entity = new PartNumMasterEntity();

            entity.costomerId = Session["CoustomerId"].ToString();
           
           skuobj.partlistDetaillist = objBAL.getpartlistDetaillist(pn_part_no);

            var dp_stamp_chart = "";
            var dp_wrapping_chart = "";
            var dp_handle_chart = "";
            var rs_sku_start_date = "";
            var rs_sku_end_date = "";
            var pn_request_type = "";
            var pr_tang_color = "";
            var pn_request_id = 0;
            var requestID = "";
            if ((skuobj.partlistDetaillist != null))
            {
                dp_stamp_chart = skuobj.partlistDetaillist[0].dp_stamp_chart;
                dp_wrapping_chart = skuobj.partlistDetaillist[0].dp_wrapping_chart;
                dp_handle_chart = skuobj.partlistDetaillist[0].dp_handle_chart;
                rs_sku_start_date = Convert.ToString(skuobj.partlistDetaillist[0].pn_start_date);
                rs_sku_end_date = Convert.ToString(skuobj.partlistDetaillist[0].pn_end_date);
                pn_request_type = skuobj.partlistDetaillist[0].pn_request_type;
                pr_tang_color = skuobj.partlistDetaillist[0].pr_tang_color;
                pn_request_id=skuobj.partlistDetaillist[0].pn_request_id;
                requestID = pn_request_type + pn_request_id;
            }
                if (skuobj.partlistDetaillist.Count == 0)
            {
                return Json(new { success = false, remarks1 = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, remarks1 = "", stampchart = dp_stamp_chart, wrampchart = dp_wrapping_chart, handalechart = dp_handle_chart, selectpartno= pn_part_no,startdate= rs_sku_start_date,enddate= rs_sku_end_date,tangtemp= pr_tang_color, requestID = requestID }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpGet]
        public JsonResult getBackSkuDetails(string pn_part_no)
        {
            SkuSelectionEntity sseobj = new SkuSelectionEntity();

            sseobj.skudetailslist = objBAL.GetSkuDetails(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            var list = sseobj.skudetailslist;
            if (sseobj.skudetailslist.Count == 0)
            {
                return Json(new { success = false, remarks1 = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, list = list }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveSelectSKU(List<SkuSelectionEntity> AllArrayValue)
        {
            SkuSelectionEntity sseobj = new SkuSelectionEntity();
            string SKUCode = "";
            Session["MaxFileSizeCode"] = "";
            foreach (var item in AllArrayValue)
            {
                string brand = Session["BrandId"].ToString();
                SKUCode = brand + item.SetSKUID;
                Session["MaxFileSizeCode"] = item.MaxFileSizeCode;
                if (item.rs_quantity > 0)
                {
                    sseobj.rs_request_type = "S";
                    sseobj.rs_sku_code = item.rs_sku_code;
                    sseobj.rs_quantity = item.rs_quantity;
                    sseobj.rs_stamp_chart = item.rs_stamp_chart;
                    sseobj.rs_wrapper_chart = item.rs_wrapper_chart;
                    sseobj.rs_handle_chart = item.rs_handle_chart;
                    sseobj.rs_tang_tempr = item.rs_tang_tempr;
                    sseobj.rs_sku_end_date = item.rs_sku_end_date;
                    sseobj.rs_sku_start_date = item.rs_sku_start_date;
                    sseobj.rs_request_id = Convert.ToInt32(Session["requestId"].ToString());
                    sseobj.rs_createby = Session["User_Id"].ToString();
                    sseobj.flag = "inserSKUSelection";
                    int j = objBAL.SaveSKUSelectionMaster(sseobj);
                }
            }
            PartNumMasterEntity inmobj = new PartNumMasterEntity();
            inmobj.flag = "insertTempPartNo";
            inmobj.pn_tmp_reqtype = "S";
            inmobj.pn_tmp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            inmobj.pn_tmp_itemno = SKUCode;
            int k = objBAL.SaveTempPartSku(inmobj);
            return Json(new { url = @Url.Action("ManageNewSetDispatch", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewSetStamps(string chartNum)
        {
            StampDetailsModel stampDetailsObj = new StampDetailsModel();
            StampDetailsEntity stampsEntity = new StampDetailsEntity();
            stampDetailsObj.StampDetailsEntityList = objBAL.getStampDetailsForModal(chartNum);
            ViewBag.stampDetailsList = stampDetailsObj.StampDetailsEntityList;
            return View("_ViewSetStamps", stampDetailsObj);
        }
        [HttpGet]
        public ActionResult ViewSetWrappersDetails(string wrapperChart)
        {
            // wrapperChart = "PROV-02A";
           
            WrapperDetailsModel wrapperMasterObj = new WrapperDetailsModel();
            WrapperDetailsEntity wraperEntity = new WrapperDetailsEntity();
            wraperEntity.wd_wrapper_id = objBAL.getWrapperId(wrapperChart);

            wraperEntity.wrappperDetailsEntityList = objBAL.getViewWrappingDetails(wraperEntity.wd_wrapper_id);
            
            wrapperMasterObj.wrappperDetailsEntityList = wraperEntity.wrappperDetailsEntityList;
            wrapperMasterObj.FileType = "SE";
            wrapperMasterObj.FileSize = "08";
            return PartialView("_ViewSetWrappersDetails", wrapperMasterObj);
        }

        [HttpGet]
        public ActionResult ViewSetHandleDrawing(string handleType)
        {
            HandleMasterModel handleMasterObj = new HandleMasterModel();
            HandleDetailsEntity handleDetailsObj = new HandleDetailsEntity();
            handleDetailsObj.hd_handle_id = objBAL.getHandleId(handleType);
            // handleMasterObj.handleDetailsList = objBAL.getHandleDrawingDetails(handleDetailsObj.hd_handle_id);

            handleDetailsObj = objBAL.getHandleDrawingDetails(handleDetailsObj.hd_handle_id);
            handleDetailsObj.tempChartNum = handleType;
            handleDetailsObj.HandleImage = objBAL.getHandleImageName(handleDetailsObj.tempChartNum);
            // handleDetailsObj = objBAL.getHandleDrawingDetails(handleType);
            HandleDetailsModel handleObj = new HandleDetailsModel();
            handleObj.HandleDetailsEntityList = handleDetailsObj.HandleDetailsEntityList;
            handleObj.HandleImage = handleDetailsObj.HandleImage;
            return PartialView("_ViewSetHandleDrawing", handleObj);
        }

        public ActionResult SubmitRemarkCustomerDetails(string prm_remarks, string prm_remark_tab)
        {
            try
            {
                PartnumgenRemarksModel remarkObj = new PartnumgenRemarksModel();
                int i = 0;
                if (ModelState.IsValid)
                {
                    PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
                    remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
                    remarkEntityObj.prm_request_type = Session["requestype"].ToString();
                    remarkEntityObj.prm_remark_tab = prm_remark_tab;
                    remarkEntityObj.prm_createby = Session["UserId"].ToString();
                    remarkEntityObj.prm_remarks = prm_remarks;
                    objBAL.SaveRemark(remarkEntityObj);
                    i = 1;
                }
                if (i == 0)
                {
                    return Json(new { success = false, responseText = "Remark not added" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, responseText = "Remark added successfully." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}