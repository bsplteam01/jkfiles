﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;

namespace JKFiles.WebApp.Controllers
{
    public class UserManagementController : Controller
    {
        BAL objBAL = new BAL();

        // GET: UserManagement
        public ActionResult UserManagementIndex()
        {
            UserLoginModel um = new UserLoginModel();
            um.listrole = getrolelist();
            DataTable dt = objBAL.getuserscount("getusercount");
            int total = Convert.ToInt32(dt.Rows[0]["Total"].ToString());
            int active = Convert.ToInt32(dt.Rows[0]["Active"].ToString());
            um.totaluser = total;
            um.totalactiveuser = active;
            return View("UserManagementIndex", um);
        }

        //get rolelist
        public List<UserRoleModel> getrolelist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getrolelist";
                dt = objBAL.getRolelist(flag);
                List<UserRoleModel> rl = new List<UserRoleModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new UserRoleModel
                    {
                        Role_Id = item.Field<int>("Role_Id"),
                        RoleName = item.Field<string>("RoleName")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //inserting user
        [HttpPost]
        public ActionResult UserManagementIndex(UserLoginModel custObj)
        {
            //if (ModelState.IsValid)
            //{
            UsermgmtEntity user = new UsermgmtEntity();
            user.PlantName = custObj.PlantName;
            user.RoleName = custObj.RoleName;
            user.User_Id = custObj.User_Id;

            user.flag = "getuserdetails";

            objBAL.getuserDetails(user);

            return RedirectToAction("UserManagementIndex", "UserManagement");
            //}
            //return RedirectToAction("UserManagementIndex", "UserManagement");
        }

        //list of user grid view
        public ActionResult UserList()
        {
            UserLoginModel modelobj = new UserLoginModel();
            modelobj.listuser = get_userlist();
            var v = modelobj.listuser;
            return PartialView("_UserList", v.ToList());
        }

        public List<UserLoginModel> get_userlist()
        {
            string flag = "getalluserdetails";
            DataTable dt = objBAL.getUserlist(flag);
            UserRoleModel modelobj = new UserRoleModel();
            List<UserLoginModel> list = new List<UserLoginModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserLoginModel objRole = new UserLoginModel();
                    objRole.Login_Id = (dt.Rows[i]["Login_Id"].ToString());
                    objRole.User_Id = (dt.Rows[i]["User_Id"].ToString());
                    objRole.RoleName = dt.Rows[i]["RoleName"].ToString();
                    objRole.PlantName = dt.Rows[i]["PlantName"].ToString();
                    objRole.FirstName = dt.Rows[i]["FirstName"].ToString();
                    objRole.LastName = dt.Rows[i]["LastName"].ToString();
                    objRole.Main_Login_Flag = dt.Rows[i]["Main_Login_Flag"].ToString();
                    objRole.Report_Login_Flag = dt.Rows[i]["Report_Login_Flag"].ToString();
                    list.Add(objRole);
                }
            }
            return list.ToList();
        }

        public ActionResult Logout(string id)
        {
            UsermgmtEntity modelobj = new UsermgmtEntity();
            modelobj.Login_Id = id;
            modelobj.flag = "updatemainloginflag";

            objBAL.updateuserlogin(modelobj);
            return RedirectToAction("UserManagementIndex", "UserManagement");

        }

        public ActionResult Delete(string id)
        {
            UsermgmtEntity modelobj = new UsermgmtEntity();
            modelobj.Login_Id = id;
            modelobj.flag = "deletelogin";

            objBAL.deleteuserlogin(modelobj);
            return RedirectToAction("UserManagementIndex", "UserManagement");

        }
    }
}