﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using System.IO;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class InnerLabelMasterController : Controller
    {
        BAL objBAL = new BAL();
        InnerLabelDetailsEntity ib = new InnerLabelDetailsEntity();
        // GET: InnerLabelMaster
        public ActionResult Index()
        {
            ib.innerlableMasterEntityList = getinnerlabblemstr();
            return View("Index", ib);
        }
        public ActionResult NewRequest()
        {
            ib.innerlableMasterNoActiveList = innerlabelNoActive();         
            return PartialView("_NewRequest", ib);
        }
        public ActionResult ApprovedRequest()
        {
            ib.innerlableMasterNoApproveList = innerlabelNoApprove();
            return PartialView("_ApprovedRequest", ib);
        }
        public ActionResult UpdateInnerLabelApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdateInnerLabelApprove(ID, type);

                string subject = "";
                string body = "";
                subject = "Request No:  " + type + ID + " and " +
                       "Dispatch Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",InnerLabel Master Approved.";


                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return Json(new { url = @Url.Action("Index", "InnerLabelMaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "InnerLabel";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "InnerLabelMaster") }, JsonRequestBehavior.AllowGet);

        }
        public List<InnerLabelDetailsEntity> innerlabelNoActive()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getinnerlabelNoActive";
                dt = objBAL.getinnerboxMasters(flag);
                List<InnerLabelDetailsEntity> listfilesize = new List<InnerLabelDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new InnerLabelDetailsEntity
                    {
                        fs_size_code = item.Field<string>("Size"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        cm_cust_name = item.Field<string>("Customer"),
                        bm_brand_name = item.Field<string>("Brand"),
                        fc_cut_desc = item.Field<string>("Cut_Type"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        ilbd_chart_num = item.Field<string>("Chart_No"),
                        ilbd_request_id = item.Field<int>("ilb_request_id"),
                        ilbd_request_type = item.Field<string>("ilb_request_type"),
                        ReqNo = item.Field<string>("ilb_request_type") + item.Field<int>("ilb_request_id")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<InnerLabelDetailsEntity> innerlabelNoApprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getinnerlabelNoApprove";
                dt = objBAL.getinnerboxMasters(flag);
                List<InnerLabelDetailsEntity> listfilesize = new List<InnerLabelDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new InnerLabelDetailsEntity
                    {
                        fs_size_code = item.Field<string>("Size"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        cm_cust_name = item.Field<string>("Customer"),
                        bm_brand_name = item.Field<string>("Brand"),
                        fc_cut_desc = item.Field<string>("Cut_Type"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        ilbd_chart_num = item.Field<string>("Chart_No"),
                        ilbd_request_id = item.Field<int>("ilb_request_id"),
                        ilbd_request_type = item.Field<string>("ilb_request_type"),
                        ReqNo = item.Field<string>("ilb_request_type") + item.Field<int>("ilb_request_id")
                    }).ToList();
                }
                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult EditInnerLabelDetails(InnerLabelDetailsEntity model)
        {
            var file1 = Request.Files["file1"];
            var file2 = Request.Files["file2"];
            var file3 = Request.Files["file3"];
            var file4 = Request.Files["file4"];
            var file5 = Request.Files["file5"];
            var file6 = Request.Files["file6"];
            var file7 = Request.Files["file7"];
            var file8 = Request.Files["file8"];
            if (file1 != null)
            {
                model.INNERPACKINGTOPLABEL = file1.FileName;
            }
            if (file2 != null)
            {
                model.INNERPACKINGBOTTOMLABEL = file2.FileName;
            }
            if (file3 != null)
            {
                model.INNERPACKINGSIDE1LABEL = file3.FileName;
            }
            if (file4 != null)
            {
                model.INNERPACKINGSIDE2LABEL = file4.FileName;
            }
            if (file5 != null)
            {
                model.INNERPACKINGEND1LABEL = file5.FileName;
            }
            if (file6 != null)
            {
                model.INNERPACKINGEND2LABEL = file6.FileName;
            }
            if (file7 != null)
            {
                model.INNERPACKINGBOXSAMPLE = file7.FileName;
            }
            if (file8 != null)
            {
                model.INNERPACKINGLABELLOCATIONCHARTID = file8.FileName;
            }
            //model.INNERPACKINGTOPLABEL = Path.GetFileName(Request.Files[0].FileName);
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/InnerLabel/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            objBAL.EditInnerLabelDetailsNoApprove(model);


            #region Mail Sendin by Dhanashree 

            string subject = "";
            string body = "";

            subject = "Request No:  " + model.ilbd_request_type+ model.ilbd_request_id
                + " and " + "Dispatch Master Update";
            body = "Request No: " + model.ilbd_request_type + model.ilbd_request_id +
                " By User ID " + Session["UserId"].ToString()
                + ",InnerLabel Master updated.Needs Approval.";

            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
               objBAL.sendingmail(email,body,subject);
            }
            #endregion


            return RedirectToAction("Index");
        }
        public ActionResult REQInnerLabelDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            InnerLabelDetailsEntity InnerLabelModelObj = new InnerLabelDetailsEntity();
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_innerlabel_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                InnerLabelModelObj.ilbd_chart_num = dt.Rows[0]["ilb_chart_num"].ToString();
                InnerLabelModelObj.INNERPACKINGTOPLABEL = dt.Rows[0]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGBOTTOMLABEL = dt.Rows[1]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGSIDE1LABEL = dt.Rows[2]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGSIDE2LABEL = dt.Rows[3]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGEND1LABEL = dt.Rows[4]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGEND2LABEL = dt.Rows[5]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGBOXSAMPLE = dt.Rows[6]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGLABELLOCATIONCHARTID = dt.Rows[7]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.INNERPACKINGBOARDSUPPLIER = dt.Rows[8]["ilbd_parm_scrnvalue"].ToString();
                InnerLabelModelObj.ilbd_request_id = Convert.ToInt16(dt.Rows[0]["ilb_request_id"].ToString());
                InnerLabelModelObj.ilbd_request_type = dt.Rows[0]["ilb_request_type"].ToString();
                InnerLabelModelObj.fs_size_code = dt.Rows[0]["ilb_fsize_code"].ToString();
                InnerLabelModelObj.ft_ftype_desc = dt.Rows[0]["ilb_ftype_code"].ToString();
                InnerLabelModelObj.pm_fstype_desc = dt.Rows[0]["ilb_fstype_code"].ToString();
                InnerLabelModelObj.bm_brand_name = dt.Rows[0]["ilb_brand_id"].ToString();
                InnerLabelModelObj.cm_cust_name = dt.Rows[0]["ilb_cust_id"].ToString();
                InnerLabelModelObj.fc_cut_desc = dt.Rows[0]["ilb_fcut_code"].ToString();
                InnerLabelModelObj.ShipToCountryCode = dt.Rows[0]["ibl_shiptocountry_cd"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
            return PartialView("REQInnerLabelDetails", InnerLabelModelObj);
        }

        public ActionResult InnerLabeldetailmaster(string filesize, string filetype, string filesubtype, string cuttype, string cstname, string brand, int? page)
        {
            //ib.innerlableMasterEntityList = getinnerlabblemstr();
            //return PartialView("_InnerLabeldetailmaster", ib);


            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<InnerLabelDetailsEntity> ib = null;
            InnerLabelDetailsEntity ibl = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> ibllist = new List<InnerLabelDetailsEntity>();

            ibllist = getinnerlabblemstr();

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                ibllist = ibllist.Where(x => x.fs_size_code == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(filesubtype))
            {
                ViewBag.fsubtypecode = filesubtype;
                ibllist = ibllist.Where(x => x.pm_fstype_desc == filesubtype).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                ibllist = ibllist.Where(x => x.ft_ftype_desc == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(cuttype))
            {
                ViewBag.cuttype = cuttype;
                ibllist = ibllist.Where(x => x.fc_cut_desc == cuttype).ToList();
            }

            if (!String.IsNullOrEmpty(cstname))
            {
                ViewBag.cstname = cstname;
                ibllist = ibllist.Where(x => x.cm_cust_name == cstname).ToList();
            }

            if (!String.IsNullOrEmpty(brand))
            {
                ViewBag.brand = brand;
                ibllist = ibllist.Where(x => x.bm_brand_name == brand).ToList();
            }

            ibl.innerlableMasterEntityList = ibllist;
            ib = ibllist.ToPagedList(pageIndex, pageSize);

            return PartialView("_InnerLabeldetailmaster", ib);
        }
        public ActionResult InnerLabelddlmaster()
        {


            InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();

            obj.listsizecode = get_size();
            obj.listtype = get_type();
            obj.listsubtype = get_subtype();
            obj.listcuttype = get_cuttype();
            obj.listcstmstr = getcstlist();
            obj.listbrandmstr = getbrandlist();

            return PartialView("_InnerLabelddlmaster", obj);

        }
        //get cstlist
        public List<InnerLabelDetailsEntity> getcstlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcstmstr";
                dt = objBAL.getcstlist1(flag);
                List<InnerLabelDetailsEntity> rl = new List<InnerLabelDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new InnerLabelDetailsEntity
                    {
                     //   costomerId = item.Field<string>("cm_cust_id"),
                        CustomerName = item.Field<string>("cm_cust_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get brandlist
        public List<InnerLabelDetailsEntity> getbrandlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getbrandmstr";
                dt = objBAL.getcstlist1(flag);
                List<InnerLabelDetailsEntity> rl = new List<InnerLabelDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new InnerLabelDetailsEntity
                    {
                       // bm_brand_id = item.Field<string>("bm_brand_id"),
                        bm_brand_name = item.Field<string>("bm_brand_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<InnerLabelDetailsEntity> get_cuttype()
        {
            string flag = "getcuttype";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerLabelDetailsEntity modelobj = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> list = new List<InnerLabelDetailsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();
                    obj.Cut_Type = (dt.Rows[i]["Cut_Type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerLabelDetailsEntity> get_filetypecode()
        {
            string flag = "getfiletypecode";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerLabelDetailsEntity modelobj = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> list = new List<InnerLabelDetailsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();
                    obj.filetypecode = (dt.Rows[i]["File_Type_Code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerLabelDetailsEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerLabelDetailsEntity modelobj = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> list = new List<InnerLabelDetailsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerLabelDetailsEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerLabelDetailsEntity modelobj = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> list = new List<InnerLabelDetailsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerLabelDetailsEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerLabelDetailsEntity modelobj = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> list = new List<InnerLabelDetailsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerLabelDetailsEntity obj = new InnerLabelDetailsEntity();
                    obj.fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerLabelDetailsEntity> getinnerlabblemstr()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getinnerlabelmstr";
                dt = objBAL.getinnerboxMasters(flag);
                List<InnerLabelDetailsEntity> listfilesize = new List<InnerLabelDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new InnerLabelDetailsEntity
                    {
                        fs_size_code = item.Field<string>("Size"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        cm_cust_name = item.Field<string>("Customer"),
                        bm_brand_name = item.Field<string>("Brand"),
                        fc_cut_desc = item.Field<string>("Cut_Type"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        ilbd_chart_num = item.Field<string>("Chart_No")

                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult editInnerLabelmaster(string reqno)
        {
            InnerLabelDetailsEntity ib = new InnerLabelDetailsEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string chartno = values[0];
            string filetype = values[1];
            ib.ft_ftype_desc = filetype;
            ib.ilbd_chart_num = chartno;
            ib.innerlabellist = objBAL.GetinnerLabelListview(ib.ilbd_chart_num, ib.ft_ftype_desc);
           // model.innerLabelList = objBAL.GetinnerLabelList(model.dp_innerlabel_chart, model.pr_ftype_code, model.pr_fstype_code, model.pr_fsize_code, model.pr_cust_id, model.pr_brand_id, model.pr_fcut_code, model.pr_ship2country_id);
            //model.innerboxlist = objBAL.getinnerboxlist(model.dp_innerbox_chart, model.pr_fstype_code, model.pr_ftype_code, model.pr_fsize_code);
           
            return PartialView("_EditInnerLabel", ib);

        }
        [HttpGet]
        public ActionResult GetShowinnerlabel(string ilbd_chart_num,string ft_ftype_desc)
        {
            InnerLabelDetailsEntity ib = new InnerLabelDetailsEntity();
            
            ib.innerlabellist = objBAL.GetShowinnerlabel(ilbd_chart_num,ft_ftype_desc);
          
            return PartialView("_EditInnerLabel", ib);

        }

        [HttpPost]
        public ActionResult InnerLabelEditDetailsEntityListActive(List<InnerLabelDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerLabelDetailsEntity model = new InnerLabelDetailsEntity();

            model.ilbd_request_id = InnerBoxList[0].ilbd_request_id;
            model.ilbd_request_type = InnerBoxList[0].ilbd_request_type;

            if (InnerBoxList.Count >= 1)
            {
                model.ilbd_chart_num = InnerBoxList[0].ilbd_chart_num;

                foreach (var item in InnerBoxList)
                {
                    
                        model.ilbd_parm_name = item.ilbd_parm_name;

                        if (string.IsNullOrEmpty(item.ilbd_parm_scrnvalue))
                        {
                            model.ilbd_parm_scrnvalue = item.ilbd_parm_dwgvalue;
                        }
                        else
                        {
                            model.ilbd_parm_scrnvalue = item.ilbd_parm_scrnvalue;
                        }
                        objBAL.InnerlabelEditavebyrowActive(model);
                    
                    i++;
                }
                objBAL.AddInnerLabelMasterEditandLinkActive(model);
            }


            return RedirectToAction("Index");
        }

        //[HttpGet]
        //public ActionResult viewInnerlabledetails(string reqno)
        //{
        //    InnerLabelDetailsEntity rm = new InnerLabelDetailsEntity();
        //    rm.ilbd_chart_num = reqno;
        //    string flag = "getinnerlabeldetail";
        //    DataTable dt = objBAL.getinnerlabeldetails(flag, reqno);

        //    // return Json(custId, JsonRequestBehavior.AllowGet);
        //    return PartialView("_InnerLabelView", rm);

        //}
    }
}