﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;
using PagedList;
using PagedList.Mvc;
using System.Data.SqlClient;
using System.IO;
using JKFilesDAL;
using JKFiles.WebApp.ActionFilters;
using System.Linq.Dynamic;
namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class StampMasterController : Controller
    {
        BAL objBAL = new BAL();
        StampDetailsModel sm = new StampDetailsModel();
        // GET: StampMaster
        #region by swapnil

        public ActionResult Index()
        {
            //sm.stampsDetailsList = getstamlist();
            //sm.listcst = getcstlist();
            //sm.listbrand = getbrandlist();
            //return View(sm);

            return View();
        }
        public ActionResult StampDetails(int Id, string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            StampDetailsEntity stampModelObj = new StampDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Stamp_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                stampModelObj.request_type = dt.Rows[0]["sm_request_type"].ToString();
                stampModelObj.request_id = Convert.ToInt16(dt.Rows[0]["sm_request_id"].ToString());
                stampModelObj.sm_cust_id = dt.Rows[0]["sm_cust_id"].ToString();
                stampModelObj.sm_brand_id = dt.Rows[0]["sm_brand_id"].ToString();
                stampModelObj.sm_ftype_id = dt.Rows[0]["sm_ftype_id"].ToString();
                stampModelObj.sm_fstype_id = dt.Rows[0]["sm_fstype_id"].ToString();
                stampModelObj.sm_fsize_id = dt.Rows[0]["sm_fsize_id"].ToString();
                stampModelObj.sm_fcut_id = dt.Rows[0]["sm_fcut_id"].ToString();
                stampModelObj.sm_stamp_mode = dt.Rows[0]["sm_stamp_mode"].ToString();
                stampModelObj.sd_chart_num = dt.Rows[0]["sm_chart_num"].ToString();
                stampModelObj.ImageName = dt.Rows[0]["sm_chartdoc_name"].ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("StampChartDetails", stampModelObj);

        }
        [HttpPost]
        public ActionResult EditstampDetails(StampDetailsEntity model)
        {
            HttpPostedFileBase photo1 = Request.Files["file1"];
            if (photo1 != null)
            {
                model.ImageName = photo1.FileName;
            }
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Stamps/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            objBAL.UpdateStampMasternoapprove(model);

            #region Mail Sending by Dhanashree 
            string subject = "";
            string body = "";

            subject = "Request No:  " + model.request_type + model.request_id
                + " and " + "Production Master Update";
            body = "Request No: " + model.request_type + model.request_id +
                " By User ID " + Session["UserId"].ToString()
                + ",Stamp Master updated.Needs Approval.";

            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email,body,subject);
            }
            #endregion
            return RedirectToAction("Index");
        }
        public ActionResult UpdateStampApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdateStampApprove(ID, type);
            }
            return RedirectToAction("Index");
        }
        public ActionResult NewRequest()
        {

            StampDetailsModel obj = new StampDetailsModel();
            obj.stmaplistactive = stmaplistactive1();
            return PartialView("_NewRequest", obj);
        }
        public List<StampDetailsModel> stmaplistactive1()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "stampmasteractivelist";
                dt = objBAL.stmaplistactive(flag);
                List<StampDetailsModel> rl = new List<StampDetailsModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new StampDetailsModel
                    {
                        Customer = item.Field<string>("Customer"),
                        Brand = item.Field<string>("Brand"),
                        File_code = item.Field<string>("FileCode"),
                        Size = item.Field<string>("Size"),
                        Cut_Type = item.Field<string>("Cut_Type"),
                        StampMode = item.Field<string>("Stamp_Mode"),
                        sd_chart_num = item.Field<string>("Chart_No"),
                        //File_type = item.Field<string>("ft_ftype_desc"),
                        //File_subtype = item.Field<string>("pm_fstype_desc"),
                        //sd_stampimg = item.Field<string>("sm_chartdoc_name"),
                        sm_request_id = item.Field<int>("sm_request_id"),
                        sm_request_type = item.Field<string>("sm_request_type"),
                        ReqNo=item.Field<string>("sm_request_type")+item.Field<int>("sm_request_id")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ApprovedRequest()
        {

            StampDetailsModel objj = new StampDetailsModel();
            objj.stmaplistNoApprove = stmaplistNoApprove();
            return PartialView("_ApprovedRequest", objj);

        }
        public List<StampDetailsModel> stmaplistNoApprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "stampmasternoapprove";
                dt = objBAL.stmaplistactive(flag);
                List<StampDetailsModel> rl = new List<StampDetailsModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new StampDetailsModel
                    {
                        Customer = item.Field<string>("Customer"),
                        Brand = item.Field<string>("Brand"),
                        File_code = item.Field<string>("FileCode"),
                        Size = item.Field<string>("Size"),
                        Cut_Type = item.Field<string>("Cut_Type"),
                        StampMode = item.Field<string>("Stamp_Mode"),
                        sd_chart_num = item.Field<string>("Chart_No"),
                        sm_chartdoc_name= item.Field<string>("sm_chartdoc_name"),
                        //File_type = item.Field<string>("ft_ftype_desc"),
                        //File_subtype = item.Field<string>("pm_fstype_desc"),
                        //sd_stampimg = item.Field<string>("sm_chartdoc_name"),
                        sm_request_id = item.Field<int>("sm_request_id"),
                        sm_request_type = item.Field<string>("sm_request_type"),
                        ReqNo = item.Field<string>("sm_request_type") + item.Field<int>("sm_request_id")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        public ActionResult StampdetailsMaster(string filesize, string filetype, string filesubtype, string cuttype, string cstname, string brand, string chartno, int? page)
        {

            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<StampDetailsModel> sm = null;
            StampDetailsModel sdm = new StampDetailsModel();
            List<StampDetailsModel> stamplist = new List<StampDetailsModel>();

            stamplist = getstamlist();

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                stamplist = stamplist.Where(x => x.Size == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(filesubtype))
            {
                ViewBag.fsubtypecode = filesubtype;
                stamplist = stamplist.Where(x => x.File_subtype == filesubtype).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                stamplist = stamplist.Where(x => x.File_type == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(cuttype))
            {
                ViewBag.cuttype = cuttype;
                stamplist = stamplist.Where(x => x.Cut_Type == cuttype).ToList();
            }

            if (!String.IsNullOrEmpty(cstname))
            {
                ViewBag.cstname = cstname;
                stamplist = stamplist.Where(x => x.Customer == cstname).ToList();
            }

            if (!String.IsNullOrEmpty(brand))
            {
                ViewBag.brand = brand;
                stamplist = stamplist.Where(x => x.Brand == brand).ToList();
            }
            if (!String.IsNullOrEmpty(chartno))
            {
                ViewBag.chartno = chartno;
                stamplist = stamplist.Where(x => x.sd_chart_num == chartno).ToList();
            }

            sdm.stampsDetailsList = stamplist;
            sm = stamplist.ToPagedList(pageIndex, pageSize);

            return PartialView("_StampdetailsMaster", sm);
        }
        public ActionResult Stampddlmaster()
        {


            StampDetailsModel obj = new StampDetailsModel();

            obj.listsizecode = get_size();
            obj.listtype = get_type();
            obj.listsubtype = get_subtype();
            obj.listcuttype = get_cuttype();
            obj.listcstmstr = getcstlist();
            obj.listbrandmstr = getbrandlist();
            obj.listchartno = getchartlist();
            return PartialView("_Stampddlmaster", obj);

        }
        public List<StampDetailsModel> getchartlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getchartnostamp";
                dt = objBAL.getcstlist1(flag);
                List<StampDetailsModel> rl = new List<StampDetailsModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new StampDetailsModel
                    {
                        // bm_brand_id = item.Field<string>("bm_brand_id"),
                        sd_chart_num = item.Field<string>("sm_chart_num")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<StampDetailsModel> get_cuttype()
        {
            string flag = "getcuttype";
            DataTable dt = objBAL.getrawmateriallist(flag);
            StampDetailsModel modelobj = new StampDetailsModel();
            List<StampDetailsModel> list = new List<StampDetailsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StampDetailsModel obj = new StampDetailsModel();
                    obj.Cut_Type = (dt.Rows[i]["Cut_Type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<StampDetailsModel> get_filetypecode()
        {
            string flag = "getfiletypecode";
            DataTable dt = objBAL.getrawmateriallist(flag);
            StampDetailsModel modelobj = new StampDetailsModel();
            List<StampDetailsModel> list = new List<StampDetailsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StampDetailsModel obj = new StampDetailsModel();
                    obj.filetypecode = (dt.Rows[i]["File_Type_Code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<StampDetailsModel> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            StampDetailsModel modelobj = new StampDetailsModel();
            List<StampDetailsModel> list = new List<StampDetailsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StampDetailsModel obj = new StampDetailsModel();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    obj.pm_fstype_code = (dt.Rows[i]["pm_fstype_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<StampDetailsModel> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            StampDetailsModel modelobj = new StampDetailsModel();
            List<StampDetailsModel> list = new List<StampDetailsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StampDetailsModel obj = new StampDetailsModel();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    obj.ft_ftype_code = (dt.Rows[i]["ft_ftype_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<StampDetailsModel> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            StampDetailsModel modelobj = new StampDetailsModel();
            List<StampDetailsModel> list = new List<StampDetailsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StampDetailsModel obj = new StampDetailsModel();
                    obj.fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        [HttpGet]
        public JsonResult getCustomerId(string slectedName)
        {
            string custId = objBAL.getCustomerID(slectedName);
            return Json(custId, JsonRequestBehavior.AllowGet);
        }
        //get Stamp_Library
        public List<StampDetailsModel> getstamlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "stampmastermasterview";
                dt = objBAL.getstamplist(flag);
                List<StampDetailsModel> rl = new List<StampDetailsModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new StampDetailsModel
                    {
                        Customer = item.Field<string>("Customer"),
                        Brand = item.Field<string>("Brand"),
                        // File_code = item.Field<string>("FileCode"),
                        Size = item.Field<string>("Size"),
                        // Cut_Type = item.Field<string>("Cut_Type"),
                        StampMode    = item.Field<string>("Stamp_Mode"),
                        sd_chart_num = item.Field<string>("Chart_No"),
                        File_type    = item.Field<string>("ft_ftype_desc"),
                        File_subtype = item.Field<string>("pm_fstype_desc"),
                        sd_stampimg  = item.Field<string>("sm_chartdoc_name"),

                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get cstlist
        public List<StampDetailsModel> getcstlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcstmstr";
                dt = objBAL.getcstlist1(flag);
                List<StampDetailsModel> rl = new List<StampDetailsModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new StampDetailsModel
                    {
                        costomerId = item.Field<string>("cm_cust_id"),
                        costomerName = item.Field<string>("cm_cust_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get brandlist
        public List<StampDetailsModel> getbrandlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getbrandmstr";
                dt = objBAL.getcstlist1(flag);
                List<StampDetailsModel> rl = new List<StampDetailsModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new StampDetailsModel
                    {
                        bm_brand_id = item.Field<string>("bm_brand_id"),
                        bm_brand_name = item.Field<string>("bm_brand_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult editstamprmaster(string reqno)
        {
            StampDetailsEntity hm = new StampDetailsEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string chartno = values[0];
            //string boxtype = values[1];
            // pl.pm_pallet_type = boxtype;
            hm.sd_chart_num = chartno;
            hm.listchartname = objBAL.getstampimgpathview(hm.sd_chart_num);
            //objBAL.GetinnerLabelListview(pl.pm_pallet_chartnum, pl.pm_pallet_type);
            // ob.outerboxMasterList = objBAL.GetOuterChartlistview(ob.ob_box_chartnum, ob.ob_box_type);
            return PartialView("_ViewStamp", hm);

        }
        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                IList<StampDetailsModel> Data = getstamlist();
                var customerData = (from tempcustomer in Data
                                    select tempcustomer);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Customer.ToUpper().Contains(searchValue.ToUpper())
                        || m.Brand.ToUpper().Contains(searchValue.ToUpper()) || m.Size.ToUpper().Contains(searchValue.ToUpper()) ||
                         m.StampMode.ToUpper().Contains(searchValue.ToUpper()) || m.sd_chart_num.ToUpper().Contains(searchValue.ToUpper()) || m.File_type.ToUpper().Contains(searchValue.ToUpper()) || m.File_subtype.ToUpper().Contains(searchValue.ToUpper()) || m.sd_stampimg.ToUpper().Contains(searchValue.ToUpper()));

                }
                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpGet]
        public ActionResult GetShowStampChart(string stampchart)
        {
            StampDetailsEntity hm = new StampDetailsEntity();
            
            hm.listchartname = objBAL.getGetShowStampChart(stampchart);
            //objBAL.GetinnerLabelListview(pl.pm_pallet_chartnum, pl.pm_pallet_type);
            // ob.outerboxMasterList = objBAL.GetOuterChartlistview(ob.ob_box_chartnum, ob.ob_box_type);
            return PartialView("_ViewStamp", hm);

        }
       
        #region Add/Edit Stamp-Dhanashree 07.08.2018
        public ActionResult AddNew()
        {
            StampDetailsModel objStampModel = new StampDetailsModel();
            objStampModel.listsizecode = get_size();
            objStampModel.listtype = get_type();
            objStampModel.listsubtype = get_subtype();
            objStampModel.listcstmstr = getcstlist();
            objStampModel.listbrandmstr = getbrandlist();
            objStampModel.stmpModeList = getStampModeList();
            return PartialView("_AddEditNew", objStampModel);
        }



        public List<StampDetailsModel> getStampModeList()
        {
            DataSet ds = new DataSet();
            DAL dbObj = new DAL();
            ds = dbObj.getStampModeList();
            List<StampDetailsModel> stampMOdelList = new List<StampDetailsModel>();

            stampMOdelList = ds.Tables[0].AsEnumerable().Select(item => new StampDetailsModel()
            {
                sm_stamp_mode = item.Field<string>("sty_desc"),
            }).ToList();
            return stampMOdelList;
        }



        [HttpGet]
        public JsonResult getChartNum(StampDetailsModel stampObj)
        {
            // stampObj.stamp_mode = slectedStampMode;
            //stampObj.brand_id = Session["BrandId"].ToString();
            //stampObj.cust_id = Session["CoustomerId"].ToString();
            //stampObj.ftype_id = Session["FileTypeCode"].ToString();
            //stampObj.fstype_id = Session["FileSubTypeCode"].ToString();
            //stampObj.fsize_id = Session["FileSizeCode"].ToString();

            StampMasterEntity stampEntity = new StampMasterEntity();
            stampEntity.sm_stamp_mode = stampObj.sm_stamp_mode;
            stampEntity.sm_brand_id = stampObj.bm_brand_id;
            stampEntity.sm_cust_id = stampObj.costomerId;
            stampEntity.sm_ftype_id = stampObj.ft_ftype_code;
            stampEntity.sm_fstype_id = stampObj.pm_fstype_code;
            stampEntity.sm_fsize_id = stampObj.fsize_code;
            //stampEntity.sm_fcut_id = stampObj.fcut_id;

            stampEntity.sm_chart_num = objBAL.getChartNum(stampEntity);
            ////return chartNum;
            //Session["currentStampChartNum"] = stampEntity.sm_chart_num;
            //string message = "test";
            return Json(stampEntity.sm_chart_num, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNewStamp(StampDetailsModel stampObj)
        {
            if (ModelState.IsValid)
            {

                OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
                Rorobj.or_effective_dt = DateTime.Now.Date;
                Rorobj.flag = "insertOtherrequestMaters";
                Rorobj.or_request_from = "Stamp";
                Rorobj.or_request_keydata = "";
                Rorobj.or_createby = Session["User_Id"].ToString();
                Rorobj.approveby = Session["User_Id"].ToString();
                int i = objBAL.SaveOtherRequest(Rorobj);
                int RequestID = i;


                string flag = "";
                HttpPostedFileBase photo = Request.Files["imagefield"];
                if (photo != null)
                {
                    stampObj.sd_stampimg = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/Stamps/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        string file = filename.Replace(filename, stampObj.sm_chart_num);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, file + ".jpg"));
                    }
                    StampMasterEntity entityobj = new StampMasterEntity();
                    entityobj.sm_fsize_id = stampObj.fsize_code;
                    entityobj.sm_ftype_id = stampObj.filetype;
                    entityobj.sm_fstype_id = stampObj.filesubtype;
                    entityobj.sm_cust_id = stampObj.costomerName;
                    entityobj.sm_brand_id = stampObj.bm_brand_name;
                    entityobj.sm_chart_num = stampObj.sm_chart_num;
                    entityobj.sm_chartdoc_name = stampObj.sm_chart_num + ".jpg";
                    entityobj.sm_stamp_mode = stampObj.sm_stamp_mode;
                    entityobj.sm_chart_remarks = "test";
                    entityobj.sm_ver_no = 0;
                    entityobj.sm_createby = Session["UserId"].ToString();
                    entityobj.sm_request_type = "M";
                    entityobj.sm_request_id = RequestID;
                    if (stampObj.sm_new_overwrite == "new")
                    {
                        entityobj.sm_flag = "new";
                        objBAL.AddNewStamp(entityobj);
                    }
                    else if (stampObj.sm_new_overwrite == "exist")
                    {
                        entityobj.sm_flag = "overwrite";
                        objBAL.AddNewStamp(entityobj);
                    }
                }
            }
            return RedirectToAction("Index");
        }



        //New Requests-Approval-1st grid
        public ActionResult GetNewRquestApproval(string RequestNo)
        {
           // string RequestNo = "N10922";
            StampMasterEntity objEntity = new StampMasterEntity();
            objEntity.sm_request_type = RequestNo.Substring(0, 1);
            objEntity.sm_request_id = Convert.ToInt32(RequestNo.Substring(1));
            objEntity.sm_flag = "newreqAdd";
            DataTable dt = objBAL.AddNewStamp(objEntity);
            StampDetailsModel objModel = new StampDetailsModel();
            if (dt.Rows.Count > 0 && dt != null)
            {
                objModel.fsize_code = dt.Rows[0]["sm_fsize_id"].ToString();
                objModel.ft_ftype_code = dt.Rows[0]["ft_ftype_desc"].ToString();
                objModel.pm_fstype_code = dt.Rows[0]["pm_fstype_desc"].ToString();
                objModel.Customer = dt.Rows[0]["cm_cust_name"].ToString();
                objModel.bm_brand_name = dt.Rows[0]["bm_brand_name"].ToString();
                objModel.StampMode = dt.Rows[0]["sm_stamp_mode"].ToString();
                objModel.stamp_size = dt.Rows[0]["sm_stmp_size"].ToString();
                objModel.stamp_location = dt.Rows[0]["sm_locn"].ToString();
                objModel.orientation = dt.Rows[0]["sm_orientation"].ToString();
                objModel.fontSize = dt.Rows[0]["sm_font_size"].ToString();
                objModel.NumofSides = dt.Rows[0]["sm_sides"].ToString();
                objModel.side1img = dt.Rows[0]["sm_img1"].ToString();
                objModel.side2img = dt.Rows[0]["sm_img2"].ToString();
                objModel.side3img = dt.Rows[0]["sm_img3"].ToString();
                objModel.side4img = dt.Rows[0]["sm_img4"].ToString();
                objModel.stamp_id= dt.Rows[0]["sm_stamp_id"].ToString();
                objModel.sm_request_type= dt.Rows[0]["sm_request_type"].ToString();
                objModel.sm_request_id=Convert.ToInt32(dt.Rows[0]["sm_request_id"]);
            }



            return PartialView("_NewRequestAddEdit", objModel);
        }


        [HttpPost]
        public ActionResult SaveStamp(StampDetailsModel stampObj)
        {
            if (ModelState.IsValid)
            {
                string flag = "";
                HttpPostedFileBase photo = Request.Files["imagefieldnew"];
                if (photo != null)
                {
                    stampObj.sd_stampimg = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/Stamps/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        string file = filename.Replace(filename, stampObj.sm_chart_num);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, file + ".jpg"));
                    }
                    StampMasterEntity entityobj = new StampMasterEntity();
                    entityobj.sm_fsize_id = stampObj.fsize_code;
                    entityobj.sm_ftype_id = stampObj.ft_ftype_code;
                    entityobj.sm_fstype_id = stampObj.pm_fstype_code;
                    entityobj.sm_cust_id = stampObj.Customer;
                    entityobj.sm_brand_id = stampObj.bm_brand_name;
                    entityobj.sm_chart_num = stampObj.sm_chart_num;
                    entityobj.sm_chartdoc_name = stampObj.sm_chart_num + ".jpg";
                    entityobj.sm_stamp_mode = stampObj.StampMode;
                    entityobj.sm_chart_remarks = "test";
                    entityobj.sm_ver_no = 0;
                    entityobj.sm_createby = Session["UserId"].ToString();
                    entityobj.sm_request_type = stampObj.sm_request_type;
                    entityobj.sm_request_id = stampObj.sm_request_id;
                    entityobj.sm_stamp_id = stampObj.stamp_id;
                    
                        entityobj.sm_flag = "newstampfirstapprove";
                        objBAL.AddNewStamp(entityobj);
                    
                    //else if (stampObj.sm_new_overwrite == "exist")
                    //{
                    //    entityobj.sm_flag = "overwrite";
                    //    objBAL.AddNewStamp(entityobj);
                    //}
                }
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult RejectionRemark(string reqno)
        {
            StampDetailsModel objModel = new StampDetailsModel();
            string req_type= reqno;
            objModel.sm_request_type = reqno;
            return PartialView("_ApproveItemGeneration", objModel);
        }
        [HttpPost]
        public ActionResult UpdateStampApprove(int ID, string reqtype)
        {
            objBAL.UpdateStampApprove(ID, reqtype);
            return Json(new { url = @Url.Action("Index", "StampMaster") }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateApproval(StampDetailsModel objmodel, FormCollection form)
        {
            string req = objmodel.sm_request_type;
           // string approveflag = objmodel.approve_flag;
           // string app_from = objmodel.approve_from;
            string type = form["actionType"].ToString();
            string status = "";
            string remark = objmodel.sm_chart_remarks;
            if (type == "approve")
            {
                if (remark == "" || remark == null)
                {
                    remark = "Approved";
                }
                else
                {
                    remark = objmodel.sm_chart_remarks;
                }
                status = "Y";
            }
            else if (type == "reject")
            {
                if (remark == "" || remark == null)
                {
                    remark = "Rejected";
                }
                else
                {
                    remark = objmodel.sm_chart_remarks;
                }
                status = "R";
            }
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in req)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            string userid = Session["User_Id"].ToString();
            string reqtype = req.Substring(0, 1);
            int ID = Convert.ToInt32(req.Substring(1));
            //if ()
            //{
                objBAL.UpdateStampApprove(ID, reqtype, status,remark);
            //}

            string subject = "";
            string body = "";
            if (type == "approve")
            {
                subject = "Request No:  " + type + ID + " and " +
                       "Production Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",Stamp Master Approved.";

            }
            else if(type == "reject")
            {
                subject = "Request No:  " + type + ID + " and " +
                       "Production Master Reject";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",Stamp Master Approved.";
            }
            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }

            return RedirectToAction("Index");
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "Stamp";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "StampMaster") }, JsonRequestBehavior.AllowGet);

        }
        #endregion

    }
}