﻿using JKFiles.WebApp.Entity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Entity;
using JKFilesDAL;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class CustomerBrandController : Controller
    {
        DAL dbObj = new DAL();
        BAL objBAL = new BAL();
        CustomerMasterModel custModelObj = new CustomerMasterModel();
        public ActionResult Index()
        {
            return View();
        }
        
        #region Customer brand
        public ActionResult Customer_Brand_Master()
        {
            return View();
        }
        public ActionResult Customerbrandddl()
        {
            CustomerDetailsEntity cstm = new CustomerDetailsEntity();

            // rm.listrawm = get_rawlist();
            cstm.custList = getCustomerList();
            cstm.listbrandmstr = getbrandlist();
            cstm.listcountry = getcountrylist();
            return PartialView("_cstbrndddl", cstm);
        }
        //public List<RawMaterialMasterModel> get_subtype()
        //{
        //    string flag = "getfilesubtypes";
        //    DataTable dt = objBAL.getrawmateriallist(flag);
        //    RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
        //    List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
        //    if (dt.Rows.Count > 0 && dt != null)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            RawMaterialMasterModel obj = new RawMaterialMasterModel();
        //            obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
        //            list.Add(obj);
        //        }
        //    }
        //    return list.ToList();
        //}
        public ActionResult CustomerBrand(string cstname, string brand, string countryname, int? page)
        {

            //   custModelObj.custList = objBAL.getCustomerBrandList();


            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<CustomerDetailsEntity> cmb = null;
            CustomerDetailsEntity cm = new CustomerDetailsEntity();
            List<CustomerDetailsEntity> cmlist = new List<CustomerDetailsEntity>();

            cmlist = getCustomerBrandList();

            if (!String.IsNullOrEmpty(countryname))
            {
                ViewBag.countryname = countryname;
                cmlist = cmlist.Where(x => x.pr_ship2country_name == countryname).ToList();
            }

            if (!String.IsNullOrEmpty(cstname))
            {
                ViewBag.cstname = cstname;
                cmlist = cmlist.Where(x => x.pr_cust_name == cstname).ToList();
            }

            if (!String.IsNullOrEmpty(brand))
            {
                ViewBag.brand = brand;
                cmlist = cmlist.Where(x => x.pr_brand_desc == brand).ToList();
            }

            cm.custList = cmlist;
            cmb = cmlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_CustomerBrand", cmb);
            // return PartialView("_CustomerBrand", custModelObj);
        }
        public List<CustomerDetailsEntity> getCustomerBrandList()
        {
            string flag = "getcstbrandmstr";
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerbrandList(flag);
            List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
            {
                pr_ship2country_name = item.Field<string>("pr_ship2country_name"),
                pr_cust_id = item.Field<string>("pr_cust_id"),
                pr_cust_name = item.Field<string>("pr_cust_name"),
                pr_brand_id = item.Field<string>("pr_brand_id"),
                pr_brand_desc = item.Field<string>("pr_brand_desc")
            }).ToList();
            return customerList;
        }
        public ActionResult Customer()
        {

            //custModelObj.custList = objBAL.getCustomerList();
            custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
            custModelObj.custNoApproveList = objBAL.getCustListNoApprove();
            return View("_Customer", custModelObj);

            //return PartialView("_Customer", custModelObj);
        }
        //public ActionResult CustomerMasterList()
        //{
        //    custModelObj.custList = objBAL.getCustomerList();
        //    return PartialView("_CustomerMasterList", custModelObj);
        //}
        public ActionResult Cstmstrddlmaster()
        {


            CustomerDetailsEntity obj = new CustomerDetailsEntity();
            obj.cstnameList = getcstlist();


            return PartialView("_Cstmstrddlmaster", obj);

        }
        public List<CustomerDetailsEntity> getcstlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcstmstr";
                dt = objBAL.getcstlist1(flag);
                List<CustomerDetailsEntity> rl = new List<CustomerDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new CustomerDetailsEntity
                    {
                        //costomerId = item.Field<string>("cm_cust_id"),
                        pr_cust_name = item.Field<string>("cm_cust_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult CustomerMasterList(string cstname, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<CustomerDetailsEntity> cm = null;
            CustomerDetailsEntity cmm = new CustomerDetailsEntity();
            List<CustomerDetailsEntity> cstist = new List<CustomerDetailsEntity>();

            cstist = getCustomerList();

            if (!String.IsNullOrEmpty(cstname))
            {
                ViewBag.cstname = cstname;
                cstist = cstist.Where(x => x.Cm_Cust_Name == cstname).ToList();
            }


            cmm.custList = cstist;
            cm = cstist.ToPagedList(pageIndex, pageSize);

            return PartialView("_CustomerMasterList", cm);
        }
        public List<CustomerDetailsEntity> getCustomerList()
        {
            DataSet ds = new DataSet();
            ds = dbObj.getCustomerList();
            List<CustomerDetailsEntity> customerList = new List<CustomerDetailsEntity>();

            customerList = ds.Tables[0].AsEnumerable().Select(item => new CustomerDetailsEntity()
            {
                Cm_Cust_Name = item.Field<string>("cm_cust_name"),
                Cm_Cust_Id = item.Field<string>("cm_cust_id")
            }).ToList();
            return customerList;
        }
        //get Countrylist
        public List<CustomerDetailsEntity> getcountrylist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getcountrylist";
                dt = objBAL.getcstlist1(flag);
                List<CustomerDetailsEntity> rl = new List<CustomerDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new CustomerDetailsEntity
                    {
                        // countryId  = item.Field<string>("_id"),
                        countryName = item.Field<string>("cm_country_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get brandlist
        public List<CustomerDetailsEntity> getbrandlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getbrandmstr";
                dt = objBAL.getcstlist1(flag);
                List<CustomerDetailsEntity> rl = new List<CustomerDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new CustomerDetailsEntity
                    {
                        brandId = item.Field<string>("bm_brand_id"),
                        brandName = item.Field<string>("bm_brand_name")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult editcustomer(string reqno)
        {
            CustomerMasterModel cm = new CustomerMasterModel();
            cm.costomerName = reqno;
            string flag = "editcustomer";
            DataTable dt = objBAL.getcstmstrdit(flag, reqno);
            cm.costomerName = dt.Rows[0]["cm_cust_name"].ToString();
            cm.costomerId = (dt.Rows[0]["cm_cust_id"].ToString());
            return PartialView("_EditCustomer", cm);

        }
        [HttpPost]
        public ActionResult Updatercustomer(CustomerMasterModel obj)
        {

            //UsermgmtEntity user = new UsermgmtEntity();
            //user.Decline_Reason = obj.decline_reason;
            //user.User_Id = obj.User_Id;
            //user.flag = "insertrejection";

            //objBAL.insertrejection(user);

            // return RedirectToAction("Index");
            return RedirectToAction("Customer", "CustomerBrand");
        }
        public ActionResult BrandMasterView()
        {

            //partial list opreation
            custModelObj.brandList = objBAL.getBrandList();
            custModelObj.brandNoApproveList = objBAL.brandNoApproveList();
            custModelObj.brandListNoActiveList = objBAL.brandListNoActiveList();
            return PartialView("_BrandMasterView", custModelObj);

            //  return PartialView("_BrandMasterView");
        }
        #endregion
    }
}