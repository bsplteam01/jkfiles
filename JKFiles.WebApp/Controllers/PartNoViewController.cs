﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class PartNoViewController : Controller
    {
        // GET: PartNoView
        public ActionResult CustomerDetails()
        {
            return PartialView("_ViewCustomerDetails");
        }
    }
}