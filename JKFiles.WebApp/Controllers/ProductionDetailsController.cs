﻿using JKFilesBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFilesDAL;
using System.Data;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Web.UI;
using netDxf;
using netDxf.Entities;
using netDxf.Tables;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class ProductionDetailsController : Controller
    {
        BAL objBAL = new BAL();
        // GET: ProductionDetails
        #region CutSpecs by Archana
        public ActionResult CutSpecs()
        {
            CutSpecsModel cutSpecsObj = new CutSpecsModel();
            CutSpecsEntity cutSpecsEntity = new CutSpecsEntity();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"].ToString()!=null)
            {
                 fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if(Session["FileSubTypeCode"].ToString()!=null)
            {
                 FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if(Session["FileSizeCode"]!=null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if(Session["FileCutTypeCode"].ToString()!=null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            //string fileTypeCode = "FL";
            //string FileSubType = "F";
            //string fileSize = "08";
            //string fileCutType = "1";
            // cutSpecsEntity = objBAL.getCutLocations(fileTypeCode);
            cutSpecsEntity.tpiCutStandardList = objBAL.getCutStandards(fileTypeCode, FileSubType, fileSize, fileCutType);
            cutSpecsObj.tpiCutStandardList = cutSpecsEntity.tpiCutStandardList;
            List<CutLocationTpiMasterEntity> newList = new List<CutLocationTpiMasterEntity>();
            cutSpecsObj.CutLocationTpiMasterModelList = newList;

            // cutSpecsObj.CutSpecsList = cutSpecsEntity.cutSpecsEntityList;  File Size, Type, Subtype and Cut Code, 
            return PartialView("_cutSpecsTab", cutSpecsObj);
        }


        [HttpGet]
        public ActionResult getCutLocationTpiMasterDetails(string selectedCutStandardName)
        {
            CutSpecsModel cutObj = new CutSpecsModel();
            CutSpecsEntity cutEntity = new CutSpecsEntity();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";

            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null &&
                Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            //string fileTypeCode = "FL";
            //string FileSubType = "F";
            //string fileSize = "08";
            //string fileCutType = "1";
            cutEntity.CutLocationTpiMasterModelList = objBAL.getCutLocationTpiMasterDetails(selectedCutStandardName, fileTypeCode, FileSubType, fileSize, fileCutType);
            if (cutEntity.CutLocationTpiMasterModelList.Count > 0)
            {
                cutObj.CutLocationTpiMasterModelList = cutEntity.CutLocationTpiMasterModelList;
            }
            else
            {
                List<CutLocationTpiMasterModel> newList = new List<CutLocationTpiMasterModel>();
                cutObj.CutLocationTpiMasterModelListModel = newList;
            }
            //return Json(cutObj.CutLocationTpiMasterModelList, JsonRequestBehavior.AllowGet);
            return PartialView("_CutSpecsTableView", cutObj);
        }

        [HttpPost]
        public ActionResult CutSpecs(CutSpecsModel cutSpecsObj)
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "CutStandard";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                Session["CutspecsDetailsTab"] = cutSpecsObj;
                CutSpecsEntity cutSpecsEntity = new CutSpecsEntity();
                ProductionSKUCutSpecsEntity prodSKUCUtSpecsObj = new ProductionSKUCutSpecsEntity();
                string itemNum;
                cutSpecsObj.vtpi_cut_std_code = cutSpecsObj.vtpi_cut_std_code;
                cutSpecsObj.vtpi_cut_std_name = cutSpecsObj.vtpi_cut_std_name;
                Session["CutSpecification"] = cutSpecsObj.vtpi_cut_std_name;
                string brandId = Session["BrandId"].ToString();
                string custId = Session["CoustomerId"].ToString();
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
                string fileCutType = Session["FileCutTypeCode"].ToString();


                if (Session["BrandId"] != null && Session["FileSizeCode"] != null && Session["Filecode"] != null && Session["FileCutTypeCode"] != null && Session["CutSpecification"] != null && Session["CoustomerId"] != null)
                {
                    itemNum = Session["BrandId"].ToString() + Session["FileSizeCode"].ToString() + Session["Filecode"].ToString() + Session["FileCutTypeCode"].ToString() + Session["CutSpecification"].ToString() + Session["CoustomerId"].ToString();
                }
                else
                {
                    itemNum = "SomeSessionIsExpired";
                }

                //string uniqId = Session["requestIdFinder"].ToString();
                RequestDeatilModel reqModelObj = new RequestDeatilModel();
                RequestDeatilEntity reqEntity = new RequestDeatilEntity();

                // reqEntity = objBAL.getRequestId(uniqId);
                //cutSpecsEntity.prc_request_id = reqEntity.requestId;
                //cutSpecsEntity.prc_request_type = reqEntity.requestType;


                cutSpecsEntity.prc_request_id = Convert.ToInt32(Session["requestId"]);
                cutSpecsEntity.prc_request_type = Session["requestype"].ToString();

                // cutSpecsEntity.prc_request_id = cutSpecsObj.prc_request_id;
                //cutSpecsEntity.prc_request_type = cutSpecsObj.prc_request_type;
                cutSpecsEntity.CutLocationTpiMasterModelList = cutSpecsObj.CutLocationTpiMasterModelList;
                //prodSKUCUtSpecsObj.prc_overcut_flg = cutSpecsObj.prc_overcut_flg;
                //prodSKUCUtSpecsObj.prc_upcut_flg = cutSpecsObj.prc_upcut_flg;
                //prodSKUCUtSpecsObj.prc_parameter_name = cutSpecsObj.vtpi_parm_value;
                //prodSKUCUtSpecsObj.prc_parameter_value = cutSpecsObj.cutStandardCode;
                //prodSKUCUtSpecsObj.EDGE = cutSpecsObj.EDGE;
                //prodSKUCUtSpecsObj.UPCUT = cutSpecsObj.UPCUT;
                //prodSKUCUtSpecsObj.OVERCUT = cutSpecsObj.OVERCUT;
                objBAL.saveCutSpecs(cutSpecsEntity);

                ItemPartNumModel itemNumObj = new ItemPartNumModel();
                itemNumObj.requestId = Convert.ToInt32(Session["requestId"]);
                // Session["CurrentRequestId"] = reqEntity.requestId;
                itemNumObj.requestType = Session["requestype"].ToString();
                //Session["CurrentRequestType"] = reqEntity.requestType;
                itemNumObj.itemNum = itemNum;
                ItemPartNumEntity itemNumEntity = new ItemPartNumEntity();
                itemNumEntity.itemNum = itemNumObj.itemNum;
                Session["itemNo"] = itemNumEntity.itemNum;
                itemNumEntity.requestId = itemNumObj.requestId;
                itemNumEntity.requestType = itemNumObj.requestType;
                //objBAL.saveItemNum(itemNumEntity);

                //  return RedirectToAction("Stamp");
                return RedirectToAction("getStampBacksave");
            }
           else
            {
                Session["CutSpecification"] = "0Z";
                return RedirectToAction("getStampBacksave");
            }

        }



        [HttpGet]
        public ActionResult getfilesubtypedetails(string sizecode, string filecode)
        {
            FileSubTypeEntity rm = new FileSubTypeEntity();
            //string s = reqno;
            //string[] values = s.Split(',');
            //string sizecode = values[0].Trim().ToString();
            //string filecode = values[1].Trim().ToString();
            ////string sizecode = "04";
            ////string filecode = "FLF";
            //rm.pm_fsize_code = reqno;
            //string flag = "getfilesubtypes";
            rm.fileSubTypedetaillist = getdetailssubfile(sizecode, filecode);

            return PartialView("_FileSubTypeView", rm);

        }


        public List<FileSubTypeEntity> getdetailssubfile(string sizecode, string filecode)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getfilesubtypesview";
                dt = objBAL.getfilesubtype(flag, sizecode, filecode);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        // pm_request_id = item.Field<int>("pm_request_id"),
                        // pm_request_type = item.Field<string>("pm_request_type"),
                        pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                        pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                        pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                        pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                        pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                    }
                    ).ToList();
                }
                return listfiletype;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region back save for CutSpecs TAb by Archana Mahajan
        public ActionResult getCutSpecsBacksave()
        {
            CutSpecsModel cutSpecsObj = new CutSpecsModel();

            #region edit code by Dhanashree
            CutSpecsEntity cutEntity = new CutSpecsEntity();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null
                && Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "cutspec_details";
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            if (ds.Tables[0].Rows.Count > 0)
            {
                cutSpecsObj.vtpi_cut_std_name = ds.Tables[0].Rows[0]["prc_parameter_value"].ToString();
            }
            string selectedCutStandardName = cutSpecsObj.vtpi_cut_std_name;
            cutEntity.CutLocationTpiMasterModelList = objBAL.getCutLocationTpiMasterDetails(selectedCutStandardName, fileTypeCode, FileSubType, fileSize, fileCutType);
            if (cutEntity.CutLocationTpiMasterModelList.Count > 0)
            {
                cutSpecsObj.CutLocationTpiMasterModelList = cutEntity.CutLocationTpiMasterModelList;
            }
            else
            {
                List<CutLocationTpiMasterModel> newList = new List<CutLocationTpiMasterModel>();
                cutSpecsObj.CutLocationTpiMasterModelListModel = newList;
            }
            Session["CutspecsDetailsTab"] = cutSpecsObj;
            CutSpecsModel sessionObj = (CutSpecsModel)Session["CutspecsDetailsTab"];
            #endregion
            if (Session["CutspecsDetailsTab"] != null)
            {

                CutSpecsEntity cutSpecsEntity = new CutSpecsEntity();

                cutSpecsEntity.tpiCutStandardList = objBAL.getCutStandards(fileTypeCode, FileSubType, fileSize, fileCutType);

                cutSpecsObj.tpiCutStandardList = cutSpecsEntity.tpiCutStandardList;

                //List<CutLocationTpiMasterEntity> newList = new List<CutLocationTpiMasterEntity>();
                //cutSpecsObj.CutLocationTpiMasterModelList = newList;

                cutSpecsObj.CutLocationTpiMasterModelList = sessionObj.CutLocationTpiMasterModelList;

                cutSpecsObj.vtpi_cut_std_name = sessionObj.vtpi_cut_std_name;
                Session["CutSpecification"] = cutSpecsObj.vtpi_cut_std_name;
                return PartialView("_cutSpecsTab", cutSpecsObj);
            }
            return RedirectToAction("CutSpecs");
        }
        #endregion

        //Stamp Table 22 /05/2018 
        public ActionResult Stamp()
        {
            StampMasterModel stampObj = new StampMasterModel();
            StampMasterEntity stampEntity = new StampMasterEntity();
            stampObj.stmpModeList = objBAL.getStampModeList();
            StampDetailsModel stampDetailsObj = new StampDetailsModel();
            StampDetailsEntity stampsEntity = new StampDetailsEntity();

            return PartialView("_stampTab", stampObj);
        }

        [HttpPost]
        public ActionResult Stamp(StampMasterModel stampModelObj)
        {
            //StampMasterEntity stampEntity = new StampMasterEntity();
            //stampEntity.sm_stamp_mode = stampModelObj.stamp_mode;u 
            //stampEntity.sm_chart_num = stampModelObj.chart_num;
            Session["StampDetailsTab"] = stampModelObj;
            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            //if (Session["requestId"] != null)
            //{
            //    string id = Session["requestId"].ToString();
            //}
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();

            //dispatchEntity.dp_request_id = (int)Session["CurrentRequestId"];
            //dispatchEntity.dp_request_type = Session["CurrentRequestType"].ToString();
            dispatchEntity.dp_stamp_type = stampModelObj.stamp_mode;
            dispatchEntity.dp_stamp_chart = stampModelObj.chart_num;
            if (stampModelObj.chart_num != null)
            {
                Session["currentStampChartNum"] = stampModelObj.chart_num;
            }
            else
            {
                Session["currentStampChartNum"] = "No Stamp Chart";
            }
            objBAL.saveStampDetailsToDispatchSKU(dispatchEntity);

            //return RedirectToAction("RawMaterial");
            return RedirectToAction("getHandelTabDetails", "DispatchDetails");
        }
        [HttpGet]
        public JsonResult getChartNum(string slectedStampMode)
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"].ToString() != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if (Session["FileSubTypeCode"].ToString() != null)
            {
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["FileSizeCode"] != null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["FileCutTypeCode"].ToString() != null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            StampMasterModel stampObj = new StampMasterModel();
            stampObj.stamp_mode = slectedStampMode;
            stampObj.brand_id = Session["BrandId"].ToString();
            stampObj.cust_id = Session["CoustomerId"].ToString();
            stampObj.ftype_id = fileTypeCode;
            stampObj.fstype_id = FileSubType;
            stampObj.fsize_id = fileSize;
            stampObj.fcut_id = fileCutType;

            StampMasterEntity stampEntity = new StampMasterEntity();
            stampEntity.sm_stamp_mode = stampObj.stamp_mode;
            stampEntity.sm_brand_id = stampObj.brand_id;
            stampEntity.sm_cust_id = stampObj.cust_id;
            stampEntity.sm_ftype_id = stampObj.ftype_id;
            stampEntity.sm_fstype_id = stampObj.fstype_id;
            stampEntity.sm_fsize_id = stampObj.fsize_id;
            stampEntity.sm_fcut_id = stampObj.fcut_id;

            stampEntity.sm_chart_num = objBAL.getChartNum(stampEntity);
            //return chartNum;
            if (stampEntity.sm_chart_num!= null)
            {
                Session["currentStampChartNum"] = stampEntity.sm_chart_num;
            }
            else
            {
                Session["currentStampChartNum"] = "No Stamp Chart";
            }
            return Json(stampEntity.sm_chart_num, JsonRequestBehavior.AllowGet);
        }

        //modal view 25/05/2018 -Archana Started Dispatch

        public ActionResult ViewStamps()
        {
            string chartNum1 = Session["currentStampChartNum"].ToString();
            StampDetailsModel stampDetailsObj = new StampDetailsModel();
            StampDetailsEntity stampsEntity = new StampDetailsEntity();
            //  stampsEntity = objBAL.getStampDetailsForModal(chartNum); 
            stampDetailsObj.StampDetailsEntityList = objBAL.getStampDetailsForModal(chartNum1);
            // return PartialView("_ViewStamps", stampDetailsObj);
            ViewBag.stampDetailsList = stampDetailsObj.StampDetailsEntityList;
            return View("_ViewStamps", stampDetailsObj);
            // return View(stampDetailsObj.StampDetailsEntityList);
        }



        #region back save for Stamp TAb by Archana Mahajan
        public ActionResult getStampBacksave()
        {
            StampMasterModel stampObj = new StampMasterModel();

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "stamp_details";
            stampObj.stampsModelList = getEditStamp(ReqNo, flag);
            Session["StampDetailsTab"] = stampObj.stampsModelList;
            #endregion


            List<StampMasterModel> sessionObj = (List<StampMasterModel>)Session["StampDetailsTab"];

            if (Session["StampDetailsTab"] != null && stampObj.stampsModelList.Count > 0)
            {
                stampObj.stmpModeList = objBAL.getStampModeList();
                stampObj.stamp_mode = sessionObj[0].stamp_mode;
                stampObj.chart_num = sessionObj[0].chart_num;
                Session["currentStampChartNum"] = stampObj.chart_num;
                return PartialView("_stampTab", stampObj);
            }
            return RedirectToAction("Stamp");
        }
        #endregion

        #region Stamp-Edit by Dhanashree
        public List<StampMasterModel> getEditStamp(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<StampMasterModel> modellist = new List<StampMasterModel>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new StampMasterModel()
                {
                    stamp_mode = item.Field<string>("dp_stamp_type"),
                    chart_num = item.Field<string>("dp_stamp_chart")

                }).ToList();
            }
            return modellist;
        }
        #endregion

        public ActionResult RawMaterial()
        {
            RawMaterialMasterModel obj = new RawMaterialMasterModel();
            RawMaterialMastrEntity entity = new RawMaterialMastrEntity();

            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();

            entity = objBAL.getRawMaterialDetails(fileTypeCode, FileSubType, fileSize);

            obj.rm_code = entity.rm_code;
            obj.rm_netwtingm_persku = entity.rm_netwtingm_persku;
            obj.rm_wtinkg_perthsnd = entity.rm_wtinkg_perthsnd;
            obj.rm_height = entity.rm_height;
            obj.rm_width = entity.rm_width;
            return View("_RawMaterialTab", obj);
        }

        [HttpPost]
        public ActionResult RawMaterial(RawMaterialMasterModel obj)
        {
            Session["RawMaterialDetailsTab"] = obj;
            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();
            RawMaterialMastrEntity rm = new RawMaterialMastrEntity();
            rm.rm_ftype_id = fileTypeCode;
            rm.rm_fstype_id = FileSubType;
            rm.rm_fsize_id = fileSize;
            rm.rm_code = obj.rm_code;
            rm.rm_netwtingm_persku = obj.rm_netwtingm_persku;
            rm.rm_wtinkg_perthsnd = obj.rm_wtinkg_perthsnd;
            rm.rm_height = obj.rm_height;
            rm.rm_width = obj.rm_width;
            objBAL.saveRawMaterial(rm);

            return RedirectToAction("DrawingTab");
        }

        #region back save for Raw material TAb by Archana Mahajan
        public ActionResult getRawMaterialBacksave()
        {
            // RawMaterialMasterModel rm = new RawMaterialMasterModel();
            RawMaterialMasterModel sessionObj = (RawMaterialMasterModel)Session["RawMaterialDetailsTab"];

            RawMaterialMasterModel obj = new RawMaterialMasterModel();
            RawMaterialMastrEntity entity = new RawMaterialMastrEntity();

            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }

            entity = objBAL.getRawMaterialDetails(fileTypeCode, FileSubType, fileSize);


            if (entity != null)
            {
                obj.rm_ftype_id = fileTypeCode;
                obj.rm_fstype_id = FileSubType;
                obj.rm_fsize_id = fileSize;
                obj.rm_code = entity.rm_code;
                obj.rm_netwtingm_persku = entity.rm_netwtingm_persku;
                obj.rm_wtinkg_perthsnd = entity.rm_wtinkg_perthsnd;
                obj.rm_height = entity.rm_height;
                obj.rm_width = entity.rm_width;
                return View("_RawMaterialTab", obj);
            }
            return RedirectToAction("RawMaterial");
        }
        #endregion

        public List<StampMasterModel> getEditRawMaterial(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<StampMasterModel> modellist = new List<StampMasterModel>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new StampMasterModel()
                {
                    stamp_mode = item.Field<string>("dp_stamp_type"),
                    chart_num = item.Field<string>("dp_stamp_chart")

                }).ToList();
            }
            return modellist;
        }
        [HttpPost]
        public ActionResult DrawingTab(RawMaterialMasterModel obj)
        {
            return RedirectToAction("DispatchIndex", "DispatchDetails");
        }






        #endregion

        #region fileSpecs by Dhanashree



        public ActionResult DrawingTab()
        {
            int request_id = Convert.ToInt32(Session["requestId"]);
            string request_type = Session["requestype"].ToString();
            //  objBAL.getDrawing();
            return PartialView("_DrawingTab");
        }

        public ActionResult Index()
        {
            ProductionMasterModel objProdModel = new ProductionMasterModel();
            objProdModel.filesizelist = getFileSize();
            objProdModel.filetTypelist = getFileType();



            // getFileSize();
            // getFileType();
            objProdModel.CutTypelist = getFileCutType();

            //objProdModel.TangColorlist = getTangColor();

            //  objProdModel.filesubtypelist = getFileSubType();
            return PartialView("_Index", objProdModel);
        }


        //get fileSize
        public List<FileSizeDetailsEntity> getFileSize()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileSize";
                dt = objBAL.getMasters(flag);
                List<FileSizeDetailsEntity> listfilespec = new List<FileSizeDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilespec = dt.AsEnumerable().Select(item => new FileSizeDetailsEntity
                    {
                        fs_size_inches = item.Field<decimal>("FileSize"),
                        fs_size_code = item.Field<string>("fs_size_code")
                    }).ToList();


                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.FileSize = dt.Rows[i]["FileSize"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                }

                // ViewData["FileSize"] = listfilespec.ToList();
                return listfilespec;
                // TempData["FileSize"] = listfilespec.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //get fileType
        public List<FileTypeDataEntity> getFileType()
        {
            DataTable dt = new DataTable();
            List<FileTypeDataEntity> fileTypelist = new List<FileTypeDataEntity>();
            try
            {
                string flag = "getFileType";
                dt = objBAL.getMasters(flag);
                //List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    fileTypelist = dt.AsEnumerable().Select(item => new FileTypeDataEntity
                    {
                        ft_ftype_desc = item.Field<string>("FileType"),
                        ft_ftype_code = item.Field<string>("ft_ftype_code")
                    }).ToList();
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.FileType = dt.Rows[i]["FileType"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                }
                return fileTypelist;
                //ViewData["FileType"] = listfilespec.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get file sub type Write by yogesh
        public List<FileSubTypeEntity> getSubTypeFile(string filesize, string filetype)
        {
            DataTable dt = new DataTable();
            List<FileSubTypeEntity> filesubTypelist = new List<FileSubTypeEntity>();
            try
            {
                string flag = "getFileTypeSub";
                dt = objBAL.getSbtyprfileMasters(flag, filesize, filetype);

                if (dt.Rows.Count > 0 && dt != null)
                {
                    filesubTypelist = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                    }).ToList();


                }
                return filesubTypelist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public JsonResult getFileSubType(string filesize, string filetype)
        {
            DataTable dt = new DataTable();
            try
            {
                FileSpec objfilespec = new FileSpec();
                objfilespec.flag = "getFileSubType";
                objfilespec.filesizecode = filesize;
                objfilespec.filetypecode = filetype;
                dt = objBAL.getfileSubType(objfilespec);
                List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        listfilespec.Add(new FileSpec
                        {
                            FileSubType = dt.Rows[i]["pm_fstype_desc"].ToString(),
                            // FileSubType = dt.Rows[i]["pm_fstype_desc"].ToString()
                        });
                    }
                }
                ViewBag.FileSubType = listfilespec;
                return Json(listfilespec, JsonRequestBehavior.AllowGet);
                //return listfilespec;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpGet]
        public JsonResult getProfileDetails(string filesizecode, string filetypecode, string filesubtype)
        {
            DataTable dt = new DataTable();
            try
            {
                FileSpec objfilespec = new FileSpec();

                objfilespec.filesizecode = filesizecode;
                objfilespec.filetypecode = filetypecode;
                objfilespec.FileSubType = filesubtype;
                dt = objBAL.getProfileDetails(objfilespec);
                List<ProductionMasterModel> listprofile = new List<ProductionMasterModel>();

                ProductionMasterModel objprofile = new ProductionMasterModel();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objprofile.USL = dt.Rows[i]["USL"].ToString();
                        objprofile.UEL = dt.Rows[i]["UEL"].ToString();
                        objprofile.IUP = dt.Rows[i]["IUP"].ToString();
                        objprofile.IOV = dt.Rows[i]["IOV"].ToString();
                        objprofile.IEG = dt.Rows[i]["IEG"].ToString();
                        objprofile.HRC = dt.Rows[i]["HRC"].ToString();
                        objprofile.Filecode = dt.Rows[i]["Filecode"].ToString();
                        //  listprofile.Add(objprofile);
                    }


                    //for (int i = 0; i < dt.Rows.Count; i++)   
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.FileSize = dt.Rows[i]["FileSize"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                }

                // ViewData["FileSize"] = listfilespec.ToList();
                return Json(objprofile, JsonRequestBehavior.AllowGet);
                // TempData["FileSize"] = listfilespec.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //get fileCutType
        public List<CutTypeDetailsEntity> getFileCutType()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileCutType";
                dt = objBAL.getMasters(flag);
                List<CutTypeDetailsEntity> CutTypelist = new List<CutTypeDetailsEntity>();
                // List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    CutTypelist = dt.AsEnumerable().Select(item => new CutTypeDetailsEntity
                    {
                        fc_cut_type = item.Field<string>("CutType"),
                        fc_cut_code = item.Field<string>("fc_cut_code")

                    }).ToList();
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.CutType = dt.Rows[i]["CutType"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                }
                //ViewBag.CutType = listfilespec.ToList();
                return CutTypelist;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get TangColor
        public List<TangColorDetailsEntity> getTangColor()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getTangColor";
                dt = objBAL.getMasters(flag);
                List<TangColorDetailsEntity> listtangcolor = new List<TangColorDetailsEntity>();
                List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.TangColor = dt.Rows[i]["TangColor"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                    listtangcolor = dt.AsEnumerable().Select(item => new TangColorDetailsEntity
                    {
                        tc_color_name = item.Field<string>("TangColor")
                    }).ToList();
                }
                return listtangcolor;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //get subtypelist
                                                                                                                               
        [HttpPost]
        public ActionResult Index(ProductionMasterModel objProdModel)
        {

            //if (ModelState.IsValid)
            //{
                //string uniqId = Session["requestIdFinder"].ToString();
                RequestDeatilModel reqModelObj = new RequestDeatilModel();
                RequestDeatilEntity reqEntity = new RequestDeatilEntity();
                Session["ProductionDetailsTab"] = objProdModel;
                // reqEntity = objBAL.getRequestId(uniqId);
                FileSpec objfilespec = new FileSpec();

                if (objProdModel.filesizecode == "#fileSizenew")
                {
                    objfilespec.filesizecode = "0Z";
                    Session["FileSizeCode"] = objfilespec.filesizecode;
                    objProdModel.filesizecode = "0Z";
                    //Session["Customer"] = "0Z";
                }
                else
                {
                    objfilespec.filesizecode = objProdModel.filesizecode;
                    Session["FileSizeCode"] = objProdModel.filesizecode;

                }
                if (objProdModel.filetypecode == "#filetypenew")
                {
                    objfilespec.filetypecode = "0Z";
                    Session["FileTypeCode"] = objfilespec.filetypecode;
                    objfilespec.FileType = "0Z";
                }
                else
                {
                    objfilespec.filetypecode = objProdModel.filetypecode;
                    objfilespec.FileType = objProdModel.FileType;
                    Session["FileTypeCode"] = objfilespec.filetypecode;

                }
            if (objProdModel.FileSubType == "#FileSubType"|| objProdModel.FileSubType==null)
                {
                    objfilespec.FileSubType = "0";
                    Session["FileSubTypeCode"] = objfilespec.FileSubType;
                    objfilespec.filesubtypecode = "0";

                }
                else
                {
                    objfilespec.filesubtypecode = objProdModel.filesubtypecode;
                    Session["FileSubTypeCode"] = objProdModel.filesubtypecode;
                    objfilespec.FileSubType = objProdModel.FileSubType;
                }


                //Session["FileSizeCode"] = objProdModel.filesizecode;
                //objfilespec.filesizecode = objProdModel.filesizecode;            
                //Session["FileTypeCode"] = objProdModel.filetypecode;
                //objfilespec.filesubtypecode = objProdModel.filesubtypecode;
                //Session["FileSubTypeCode"] = objProdModel.filesubtypecode;
                //objfilespec.FileSubType = objProdModel.FileSubType;      

                Session["Filecode"] = objProdModel.Filecode;
                //  objfilespec.HRC = objProdModel.HRC;

                if (objProdModel.HRC != null)
                {
                    objfilespec.HRC = objProdModel.HRC;
                }
                else
                {
                    objfilespec.HRC = "0Z";
                }
                objfilespec.TangColor = objProdModel.TangColor;
                objfilespec.USL = objProdModel.USL;
                if (objProdModel.UEL != null)
                {
                    objfilespec.UEL = objProdModel.UEL;
                }
                else
                {
                    objfilespec.UEL = "0";
                }
                objfilespec.UPL = objProdModel.UPL;
                objfilespec.IUP = objProdModel.IUP;
                objfilespec.IOV = objProdModel.IOV;
                objfilespec.IEG = objProdModel.IEG;
                objfilespec.cuttypecode = objProdModel.cuttypecode;
                objfilespec.CutType = objProdModel.CutType;
                objfilespec.ReqType = Session["requestype"].ToString();
                objfilespec.req_id = Convert.ToInt32(Session["requestId"]);
                //objfilespec.ReqType = reqEntity.requestType;
                //objfilespec.req_id = reqEntity.requestId;
                Session["FileCutTypeCode"] = objProdModel.cuttypecode;
                if (Session["Customer"] != null)
                {
                    objfilespec.CustName = Session["Customer"].ToString();
                }

                objBAL.UpdateFileSpec(objfilespec);
                return RedirectToAction("CutSpecs", "ProductionDetails");
            //}
            // return RedirectToAction("CutSpecs", "ProductionDetails");
            //return RedirectToAction("Index");
        }

        #region  back save -File Spec details - Archana Mahajan
        public ActionResult getFileSpecsDetails()
        {
            ProductionMasterModel objProdModel = new ProductionMasterModel();

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "file_specdetails";
            objProdModel.modellist = getEditFileSpec(ReqNo, flag);
            Session["ProductionDetailsTab"] = objProdModel.modellist;
            #endregion

            List<ProductionMasterModel> sessionObj = (List<ProductionMasterModel>)Session["ProductionDetailsTab"];
            if (Session["ProductionDetailsTab"] != null)
            {
                //string uniqId = Session["requestIdFinder"].ToString();
                RequestDeatilModel reqModelObj = new RequestDeatilModel();
                RequestDeatilEntity reqEntity = new RequestDeatilEntity();
                objProdModel.filesizelist = getFileSize();
                objProdModel.filetTypelist = getFileType();
                objProdModel.CutTypelist = getFileCutType();
                objProdModel.TangColorlist = getTangColor();

                objProdModel.filesizecode = sessionObj[0].filesizecode;
                objProdModel.FileSize = sessionObj[0].file_size_inches.ToString();
                objProdModel.filetypecode = sessionObj[0].filetypecode;
                objProdModel.FileType = sessionObj[0].FileType;

                if (sessionObj[0].filesubtypecode == "0")
                {
                    objProdModel.filesubtypecode = "New";
                    objProdModel.FileSubType = "#FileSubType";
                }
                else
                {
                    objProdModel.filesubtypecode = sessionObj[0].filesubtypecode;
                    objProdModel.FileSubType = sessionObj[0].FileSubType;
                    objProdModel.subtype= sessionObj[0].FileSubType;
                }
                objProdModel.Filecode = sessionObj[0].Filecode;
                objProdModel.HRC = sessionObj[0].HRC;
                objProdModel.TangColor = sessionObj[0].TangColor;
                objProdModel.USL = sessionObj[0].USL;
                objProdModel.UEL = sessionObj[0].UEL;
                objProdModel.UPL = sessionObj[0].UPL;
                objProdModel.IUP = sessionObj[0].IUP;
                objProdModel.IOV = sessionObj[0].IOV;
                objProdModel.IEG = sessionObj[0].IEG;
                objProdModel.cuttypecode = sessionObj[0].cuttypecode;
                objProdModel.CutType = sessionObj[0].CutType;
                if (objProdModel.CutType != null)
                {
                    string[] str = objProdModel.CutType.Split(' ');
                    string CutTypecode = str[0].ToString();
                    Session["CutType"] = str[1].ToString();
                }
                Session["FileSizeCode"] = objProdModel.filesizecode;
                Session["FileTypeCode"] = objProdModel.filetypecode;
                Session["FileSubTypeCode"] = objProdModel.filesubtypecode;
                Session["Filecode"] = objProdModel.Filecode;
                Session["FileCutTypeCode"] = sessionObj[0].cuttypecode;

                return PartialView("_Index", objProdModel);
            }
            return RedirectToAction("Index");
        }


        public List<ProductionMasterModel> getEditFileSpec(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<ProductionMasterModel> modellist = new List<ProductionMasterModel>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new ProductionMasterModel()
                {
                    filesizecode = item.Field<string>("pr_fsize_code"),
                    file_size_inches = item.Field<decimal?>("fs_size_inches"),
                    filetypecode = item.Field<string>("pr_ftype_code"),
                    FileType = item.Field<string>("pr_ftype_desc"),
                    filesubtypecode = item.Field<string>("pr_fstype_code"),
                    FileSubType = item.Field<string>("pr_fstype_desc"),
                    Filecode = item.Field<string>("filecode"),
                    cuttypecode = item.Field<string>("pr_fcut_code"),
                    CutType = item.Field<string>("pr_fcut_desc"),
                    TangColor = item.Field<string>("pr_tang_color"),
                    HRC = item.Field<string>("pr_hardness"),
                    USL = item.Field<string>("pr_uncutattang"),
                    UEL = item.Field<string>("pr_uncutatedge"),
                    UPL = "",
                    IUP = item.Field<string>("pr_upcutncline"),
                    IOV = item.Field<string>("pr_overcutincline"),
                    IEG = item.Field<string>("pr_edgecutincline"),

                }).ToList();
            }
            return modellist;
        }


        #endregion

        [HttpGet]
        public JsonResult getFizeSizeCode(string selectFileSize)
        {
            string flag = "getFileSizeCode";
            string FileSizecode = objBAL.getFileSpecIds(selectFileSize, flag, null);
            Session["FileSizeCode"] = FileSizecode;
            return Json(FileSizecode, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getFizeTypeCode(string selectFileType)
        {
            string flag = "getFileTypeCode";
            string FileSizecode = objBAL.getFileSpecIds(selectFileType, flag, null);
            Session["FileTypeCode"] = FileSizecode;
            return Json(FileSizecode, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getCutTypeCode(string selectCutType)
        {
            //string flag = "getCutTypeCode";
            string[] str = selectCutType.Split(' ');
            string CutTypecode = str[0].ToString();
            Session["CutType"] = str[1].ToString();
            //string name = str[1].ToString();
            //string CutTypecode = objBAL.getFileSpecIds(name, flag);
            return Json(CutTypecode, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getFileSubTypeCode(string selectSubType)
        {
            string flag = "getFileSubTypeCode";
            string SubTypecode = objBAL.getFileSpecIds(selectSubType, flag, null);
            return Json(SubTypecode, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult getRequestNumber()
        {
            string flag = "getReqNo";
            string CustName = "";
            if (Session["Customer"] != null)
            {
                CustName = Session["Customer"].ToString();
            }
            string uniqId = "";
            if (Session["requestIdFinder"] != null)
            {
                uniqId = Session["requestIdFinder"].ToString();
            }

            string requestno = objBAL.getFileSpecIds(CustName, flag, uniqId);
            Session["requestno"] = requestno;
            return Json(requestno, JsonRequestBehavior.AllowGet);
        }

        #region Drawing Code by Dhanashree

        [HttpGet]
        public void GenerateDrawing(string prod_sku, string req_no)
        {
            Bitmap exbitmap = new Bitmap(gconst.MAXX, gconst.MAXY);
            Graphics graphObj = Graphics.FromImage(exbitmap);
            Component comp = new Component();

            // Panel panelinfo = new Panel();
            SolidBrush solidBrush = new SolidBrush(Color.White);

            graphObj.FillRectangle(solidBrush, new System.Drawing.Rectangle(0, 0, gconst.MAXX, gconst.MAXY));
            FileSpec objfile = new FileSpec();

            objfile.filesizecode = prod_sku.Substring(0, 2);
            objfile.filetypecode = prod_sku.Substring(2, 2);
            objfile.filesubtypecode = prod_sku.Substring(4, 1);
            string cuttypecode = prod_sku.Substring(5, 1);
            string cuttype = prod_sku.Substring(5, 1);
            string cutstandard = prod_sku.Substring(6, 2);
            string ReqNo = req_no;

            objfile.cuttypecode = cuttypecode;
            objfile.cutstandard = cutstandard;
            //if (Session["CutSpecification"] != null)
            //{
            //    cutstandard = Session["CutSpecification"].ToString();
            //}

            //objfile.filesizecode = Session["FileSizeCode"].ToString();
            //objfile.filetypecode = Session["FileTypeCode"].ToString();
            //objfile.filesubtypecode = Session["FileSubTypeCode"].ToString();
            //string cuttypecode = Session["FileCutTypeCode"].ToString();
            //string cuttype = Session["CutType"].ToString();
            //string cutstandard = "";
            //string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            //if (Session["CutSpecification"] != null)
            //{
            //    cutstandard = Session["CutSpecification"].ToString();
            //}
            objfile.ReqNo = ReqNo;
            objfile.cutstandard = cutstandard;
            objfile.flag = "get_dwg_val";
            //get drawing values
            DataTable dt = objBAL.get_DrawingValues(objfile);
            Profile objprofiledwgval = new Profile();

            if (objfile.filetypecode == "RD")
            {

            }
            else if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.RMW_dimval = Convert.ToDouble(dt.Rows[0]["RMW"]);
                objprofiledwgval.RMT_dimval = Convert.ToDouble(dt.Rows[0]["RMT"]);
                objprofiledwgval.W_dimval = Convert.ToDouble(dt.Rows[0]["GBW"]);
                objprofiledwgval.T_dimval = Convert.ToDouble(dt.Rows[0]["GBT"]);
                objprofiledwgval.FPL_dimval = Convert.ToDouble(dt.Rows[0]["FPL"]);
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.RMD_dimval = Convert.ToDouble(dt.Rows[0]["RMW"]);
            }
            else
            {
                objprofiledwgval.RMW_dimval = Convert.ToDouble(dt.Rows[0]["RMW"]);
                objprofiledwgval.RMT_dimval = Convert.ToDouble(dt.Rows[0]["RMT"]);
                objprofiledwgval.W_dimval = Convert.ToDouble(dt.Rows[0]["GBW"]);
                objprofiledwgval.T_dimval = Convert.ToDouble(dt.Rows[0]["GBT"]);
                objprofiledwgval.FPL_dimval = Convert.ToDouble(dt.Rows[0]["FPL"]);
                objprofiledwgval.GPL_dimval = Convert.ToDouble(dt.Rows[0]["GPL"]);
            }
            objprofiledwgval.CML_dimval = Convert.ToDouble(dt.Rows[0]["CML"]);
            objprofiledwgval.BL_dimval = Convert.ToDouble(dt.Rows[0]["BLG"]);
            objprofiledwgval.TL_dimval = Convert.ToDouble(dt.Rows[0]["TLG"]);
            objprofiledwgval.TWS_dimval = Convert.ToDouble(dt.Rows[0]["TWS"]);
            objprofiledwgval.TTS_dimval = Convert.ToDouble(dt.Rows[0]["TTS"]);
            objprofiledwgval.TWT_dimval = Convert.ToDouble(dt.Rows[0]["TWT"]);
            objprofiledwgval.TTT_dimval = Convert.ToDouble(dt.Rows[0]["TTT"]);
            objprofiledwgval.TPL_dimval = Convert.ToDouble(dt.Rows[0]["TPL"]);
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.L_dimval = Convert.ToDouble(dt.Rows[0]["BSC"]);
            }

            objprofiledwgval.USL_dimval = Convert.ToDouble(dt.Rows[0]["USL"]);
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.UEL_dimval = Convert.ToDouble(dt.Rows[0]["UEL"]);
            }
            else if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.L_dimval = Convert.ToDouble(dt.Rows[0]["BSC"]);
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.D_dimval = Convert.ToDouble(dt.Rows[0]["GBD"]);
                objprofiledwgval.ASH_dimval = Convert.ToDouble(dt.Rows[0]["TAD"]);
                objprofiledwgval.RSH_dimval = Convert.ToDouble(dt.Rows[0]["TRM"]);
            }
            else if(objfile.filetypecode == "MI")
            {
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.IEG_dimval = Convert.ToDouble(dt.Rows[0]["IEG"]);
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
                objprofiledwgval.ASH_dimval = Convert.ToDouble(dt.Rows[0]["ASH"]);
            }
            else
            {
                objprofiledwgval.RMD_dimval = Convert.ToDouble(dt.Rows[0]["RMD"]);
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.IEG_dimval = Convert.ToDouble(dt.Rows[0]["IEG"]);
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
                objprofiledwgval.ASH_dimval = Convert.ToDouble(dt.Rows[0]["ASH"]);
            }
            if (objfile.filetypecode == "RD")
            {
                objprofiledwgval.D_dimval_R = Convert.ToDouble(dt.Rows[0]["GBD"]);
                objprofiledwgval.RMD_dimval_R = Convert.ToDouble(dt.Rows[0]["RMD"]);
            }

            else
            {

            }
            objfile.flag = "get_display_val";
            DataTable dt1 = objBAL.get_DrawingValues(objfile);
            objprofiledwgval.FNM_dispval = dt1.Rows[0]["File Size"].ToString() + " INCH " +
                dt1.Rows[0]["File Type"].ToString();
            objprofiledwgval.FSZ_dispval = dt1.Rows[0]["File Size"].ToString();
            if (objfile.filetypecode == "RD")
            {
                objprofiledwgval.ATN_dispval_R = (dt1.Rows[0]["TAG"]).ToString();
                objprofiledwgval.RSH_dispval_R = (dt1.Rows[0]["TRM"]).ToString();
                objprofiledwgval.D_dispval_R = (dt1.Rows[0]["GBD"]).ToString();
                objprofiledwgval.RMD_dispval_R = (dt1.Rows[0]["RMD"]).ToString();
                objprofiledwgval.GPL_dispval = (dt1.Rows[0]["GPL"]).ToString();
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.RMW_dispval = (dt1.Rows[0]["RMW"]).ToString();
                objprofiledwgval.ATN_dispval_SQ = (dt1.Rows[0]["TAD"]).ToString();
                objprofiledwgval.RSH_dispval = (dt1.Rows[0]["TRM"]).ToString();
                objprofiledwgval.D_dispval = (dt1.Rows[0]["GBD"]).ToString();
                objprofiledwgval.ASH_dispval = dt.Rows[0]["TAD"].ToString();
                objprofiledwgval.RSH_dispval = dt.Rows[0]["TRM"].ToString();
                objprofiledwgval.GPL_dispval = dt.Rows[0]["GBL"].ToString();
                //objprofiledwgval.RMD_dispval_SQ = (dt1.Rows[0]["RMD"]).ToString();

            }
            else if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.RMW_dispval = (dt1.Rows[0]["RMW"]).ToString();
                objprofiledwgval.RMT_dispval = (dt1.Rows[0]["RMT"]).ToString();
                objprofiledwgval.FPL_dispval = (dt1.Rows[0]["FPL"]).ToString();
                objprofiledwgval.W_dispval = (dt1.Rows[0]["GBW"]).ToString();
                objprofiledwgval.T_dispval = (dt1.Rows[0]["GBT"]).ToString();
                objprofiledwgval.GPL_dispval = (dt1.Rows[0]["GPL"]).ToString();
            }

            else
            {
                objprofiledwgval.RMW_dispval = (dt1.Rows[0]["RMW"]).ToString();
                objprofiledwgval.RMT_dispval = (dt1.Rows[0]["RMT"]).ToString();
                objprofiledwgval.FPL_dispval = (dt1.Rows[0]["FPL"]).ToString();
                objprofiledwgval.W_dispval = (dt1.Rows[0]["GBW"]).ToString();
                objprofiledwgval.T_dispval = (dt1.Rows[0]["GBT"]).ToString();
                objprofiledwgval.GPL_dispval = (dt1.Rows[0]["GPL"]).ToString();
            }
            objprofiledwgval.CML_dispval = (dt1.Rows[0]["CML"]).ToString();
            objprofiledwgval.BL_dispval = (dt1.Rows[0]["BLG"]).ToString();
            objprofiledwgval.TL_dispval = (dt1.Rows[0]["TLG"]).ToString();
            objprofiledwgval.TWS_dispval = (dt1.Rows[0]["TWS"]).ToString();
            objprofiledwgval.TTS_dispval = (dt1.Rows[0]["TTS"]).ToString();
            objprofiledwgval.TWT_dispval = (dt1.Rows[0]["TWT"]).ToString();
            objprofiledwgval.TTT_dispval = (dt1.Rows[0]["TTT"]).ToString();
            objprofiledwgval.TPL_dispval = (dt1.Rows[0]["TPL"]).ToString();
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }

            objprofiledwgval.USL_dispval = (dt1.Rows[0]["USL"]).ToString();
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.UEL_dispval = (dt1.Rows[0]["UEL"]).ToString();
            }
            if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.USLF_dispval_HR = (dt1.Rows[0]["USL"]).ToString();
                objprofiledwgval.USLR_dispval_HR = (dt1.Rows[0]["UEL"]).ToString();
                objprofiledwgval.IUPF_dispval_HR = (dt1.Rows[0]["IUF"]).ToString();
                objprofiledwgval.IUPR_dispval_HR = (dt1.Rows[0]["IUR"]).ToString();
                objprofiledwgval.IOVF_dispval_HR = (dt1.Rows[0]["IOF"]).ToString();
                objprofiledwgval.IOVR_dispval_HR = (dt1.Rows[0]["IOR"]).ToString();
                objprofiledwgval.HRC_dispval = (dt1.Rows[0]["HRC"]).ToString();
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.IUP_dispval = (dt1.Rows[0]["IUP"]).ToString();
                objprofiledwgval.IOV_dispval = (dt1.Rows[0]["IOV"]).ToString();
                objprofiledwgval.HRC_dispval = (dt1.Rows[0]["HRC"]).ToString();
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }
            else
            {
                objprofiledwgval.IUP_dispval = (dt1.Rows[0]["IUP"]).ToString();
                objprofiledwgval.IOV_dispval = (dt1.Rows[0]["IOV"]).ToString();
                objprofiledwgval.IEG_dispval = (dt1.Rows[0]["IEG"]).ToString();
                objprofiledwgval.HRC_dispval = (dt1.Rows[0]["HRC"]).ToString();
                objprofiledwgval.ASH_dispval = (dt1.Rows[0]["ASH"]).ToString();
            }

            objprofiledwgval.CSP_dispval = cutstandard;
            objprofiledwgval.CT_dispval = cuttype;

            objfile.flag = "get_tpi_values";
            DataTable dt3 = objBAL.get_DrawingValues(objfile);
            if (dt3 != null && dt3.Rows.Count > 0)
            {
                objprofiledwgval.UCT_dispval = dt3.Rows[0]["U/C"].ToString();
                objprofiledwgval.OCT_dispval = dt3.Rows[0]["O/C"].ToString();
                objprofiledwgval.ECT_dispval = dt3.Rows[0]["E/C"].ToString();

                if (objfile.filetypecode == "HR")
                {
                    objprofiledwgval.UP1GN_dispval_HR = dt3.Rows[0]["U/C"].ToString();
                    objprofiledwgval.OV1GN_dispval_HR = dt3.Rows[0]["O/C"].ToString();
                }
                if (objfile.filetypecode=="RD")
                {
                    objprofiledwgval.OCR_dispval = dt3.Rows[1]["O/C"].ToString();
                    objprofiledwgval.UCR_dispval = dt3.Rows[1]["U/C"].ToString();
                }
            }
            if (objfile.filetypecode == "FL")
            {
                Profile_Manager.Flat_profile(graphObj, comp, objprofiledwgval);
            }
            else if (objfile.filetypecode == "MI")
            {
                Profile_Manager.Mil_profile(graphObj, comp, objprofiledwgval);
            }

            else if (objfile.filetypecode == "HR")
            {
                Profile_Manager.HalfRound_profile(graphObj, comp, objprofiledwgval);
            }
            else if (objfile.filetypecode == "RD")
            {
                Profile_Manager.Round_profile(graphObj, comp, objprofiledwgval);
            }
            else if (objfile.filetypecode == "SQ")
            {
                Profile_Manager.SQUARE_profile(graphObj, comp, objprofiledwgval);
            }
            else if (objfile.filetypecode == "TP")
            {
                Profile_Manager.Taper_profile(graphObj, comp, objprofiledwgval);
            }

            // Insert Drawing details in drawing library
            string imagename = objfile.filesizecode + objfile.filetypecode + objfile.filesubtypecode + cuttypecode + cutstandard;
            //objfile.dwg_prod_sku = imagename;
            //objfile.dwg_imgname = imagename + ".jpeg";
            //objfile.dwg_pdfname = imagename + ".pdf";
            //objfile.dwg_dxfname = imagename + ".dxf";
            //objfile.flag = "insert_drawing_library";
            //objBAL.get_DrawingValues(objfile);


            Response.ContentType = "image/gif";
            exbitmap.Save(Response.OutputStream, ImageFormat.Gif);
            string filename = Server.MapPath("~/pdf/dwg/" + imagename + ".jpeg");
            FileStream fts = new FileStream(filename, FileMode.Create);
            exbitmap.Save(fts, ImageFormat.Gif);
            // Bitmap finalBitmap = ....; //from disk or whatever
            fts.Close();

            

        }

        [HttpGet]
        public void DownloadPDF(string prod_sku, string req_no)
        {
            Document doc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 0f, 0f, 0f, 0f);
            FileSpec objfile = new FileSpec();

            objfile.filesizecode = prod_sku.Substring(0, 2);
            objfile.filetypecode = prod_sku.Substring(2, 2);
            objfile.filesubtypecode = prod_sku.Substring(4, 1);
            string cuttypecode = prod_sku.Substring(5, 1);
            string cuttype = prod_sku.Substring(5, 1);
            string cutstandard = prod_sku.Substring(6, 2);
            string ReqNo = req_no;
            try
            {
                //if (sheets != null && sheets.Count > 0)
                //{ 
                string imagename = "";
                //if (Session["FileSizeCode"] != null && Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileCutTypeCode"] != null &&
                //    Session["CutSpecification"] != null)
                //{
                imagename = objfile.filesizecode + objfile.filetypecode + objfile.filesubtypecode + cuttypecode + cutstandard;
                // }
                string pdfFilePath = Server.MapPath("~/pdf/dwg/" + imagename );
                FileStream pdfFile = new FileStream(pdfFilePath + ".pdf", FileMode.Create);
                PdfWriter writer = PdfWriter.GetInstance(doc, pdfFile);
                doc.Open();
                //foreach (Sheet sheetObj in sheets)
                //{
                Paragraph paragraph = new Paragraph("Drawing");
                string imageURL = Server.MapPath("~/pdf/dwg/" + imagename + ".jpeg");
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                //Resize image depend upon your need
                //jpg.SetAbsolutePosition();

                jpg.ScaleToFit(850f, 1500f);
                //jpg.ScaleToFit(PageSize.A4.Width , PageSize.A4.Height * 2f);
                //Give space before image
                jpg.SpacingBefore = 90f;
                //Give some space after the image
                //jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.ALIGN_JUSTIFIED_ALL;
                //doc.Add(paragraph);
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(" "));
                doc.Add(jpg);
                doc.NewPage();
                // }

                doc.Close();

                Response.Clear();

                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=drawing.pdf");
                // Response.TransmitFile(Server.MapPath(".") + "/Default.pdf");
                string path = Server.MapPath("~/pdf/dwg/") + imagename + ".pdf";
                Response.TransmitFile(path);
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(path);
                if (buffer != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.BinaryWrite(buffer);
                }
                Response.End();
                //}
            }
            finally
            {
                doc.Close();
            }
        }


        [HttpGet]
        public ActionResult DownloadDXF(string prod_sku, string req_no)
        {
            //  double x_start = 25;
            //  double y_start = 120;




            //DxfDocument test = new DxfDocument();
            //DxfDocument dxf = new DxfDocument();
            //Profile prf1 = new Profile();
            //Coordinate vertcord = new Coordinate();


            //test.DrawingVariables.LtScale = 10;
            //test.DrawingVariables.TextStyle = "Romans.ttf";
            //test.DrawingVariables.TextSize = 2.5;




            // dimension style settings

            //DimensionStyle myStyle = new DimensionStyle("MyStyle");
            // myStyle.TextStyle = new TextStyle("Romans.ttf");
            // // myStyle.DimSuffix = "mm";
            // myStyle.TextHeight = 2.5;
            // myStyle.LengthPrecision = 2;
            // myStyle.DimLineColor = AciColor.Red;
            // myStyle.TextColor = AciColor.ByLayer;

            // myStyle.DimArrow1 = DimensionArrowhead.Closed;
            // myStyle.DimArrow2 = DimensionArrowhead.Closed;
            // myStyle.ArrowSize = 2.5;
            // myStyle.DimLineExtend = 20;
            // myStyle.TextOffset = 1.5;
            // myStyle.ExtLineOffset = 1;
            // myStyle.ExtLineExtend = 1.5;
            // myStyle.DimScaleOverall = 1;


            //double x_start = 25;
            //double y_start = 120;






            FileSpec objfile = new FileSpec();
            //objfile.filesizecode = Session["FileSizeCode"].ToString();
            //objfile.filetypecode = Session["FileTypeCode"].ToString();
            //objfile.filesubtypecode = Session["FileSubTypeCode"].ToString();
            //string cuttypecode = Session["FileCutTypeCode"].ToString();
            //string cuttype = Session["CutType"].ToString();
            //string cutstandard = "";
            //if (Session["CutSpecification"] != null)
            //{
            //    cutstandard = Session["CutSpecification"].ToString();
            //}
            //objfile.cutstandard = cutstandard;

            objfile.filesizecode = prod_sku.Substring(0, 2);
            objfile.filetypecode = prod_sku.Substring(2, 2);
            objfile.filesubtypecode = prod_sku.Substring(4, 1);
            string cuttypecode = prod_sku.Substring(5, 1);
            string cuttype = prod_sku.Substring(5, 1);
            string cutstandard = prod_sku.Substring(6, 2);
            objfile.cuttypecode = cuttypecode;
            objfile.cutstandard = cutstandard;
            string ReqNo = req_no;
            objfile.ReqNo = ReqNo;





            objfile.flag = "get_dwg_val";
            //get drawing values
            DataTable dt = objBAL.get_DrawingValues(objfile);
            Profile objprofiledwgval = new Profile();

            if (objfile.filetypecode == "RD")
            {

            }
            else if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.RMW_dimval = Convert.ToDouble(dt.Rows[0]["RMW"]);
                objprofiledwgval.RMT_dimval = Convert.ToDouble(dt.Rows[0]["RMT"]);
                objprofiledwgval.W_dimval = Convert.ToDouble(dt.Rows[0]["GBW"]);
                objprofiledwgval.T_dimval = Convert.ToDouble(dt.Rows[0]["GBT"]);
                objprofiledwgval.FPL_dimval = Convert.ToDouble(dt.Rows[0]["FPL"]);
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.RMW_dimval = Convert.ToDouble(dt.Rows[0]["RMW"]);
            }
            else
            {
                objprofiledwgval.RMW_dimval = Convert.ToDouble(dt.Rows[0]["RMW"]);
                objprofiledwgval.RMT_dimval = Convert.ToDouble(dt.Rows[0]["RMT"]);
                objprofiledwgval.W_dimval = Convert.ToDouble(dt.Rows[0]["GBW"]);
                objprofiledwgval.T_dimval = Convert.ToDouble(dt.Rows[0]["GBT"]);
                objprofiledwgval.FPL_dimval = Convert.ToDouble(dt.Rows[0]["FPL"]);
                objprofiledwgval.GPL_dimval = Convert.ToDouble(dt.Rows[0]["GPL"]);
            }
            objprofiledwgval.CML_dimval = Convert.ToDouble(dt.Rows[0]["CML"]);
            objprofiledwgval.BL_dimval = Convert.ToDouble(dt.Rows[0]["BLG"]);
            objprofiledwgval.TL_dimval = Convert.ToDouble(dt.Rows[0]["TLG"]);
            objprofiledwgval.TWS_dimval = Convert.ToDouble(dt.Rows[0]["TWS"]);
            objprofiledwgval.TTS_dimval = Convert.ToDouble(dt.Rows[0]["TTS"]);
            objprofiledwgval.TWT_dimval = Convert.ToDouble(dt.Rows[0]["TWT"]);
            objprofiledwgval.TTT_dimval = Convert.ToDouble(dt.Rows[0]["TTT"]);
            objprofiledwgval.TPL_dimval = Convert.ToDouble(dt.Rows[0]["TPL"]);
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.L_dimval = Convert.ToDouble(dt.Rows[0]["BSC"]);
            }

            objprofiledwgval.USL_dimval = Convert.ToDouble(dt.Rows[0]["USL"]);
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.UEL_dimval = Convert.ToDouble(dt.Rows[0]["UEL"]);
            }
            else if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.L_dimval = Convert.ToDouble(dt.Rows[0]["BSC"]);
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.D_dimval = Convert.ToDouble(dt.Rows[0]["GBD"]);
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.IEG_dimval = Convert.ToDouble(dt.Rows[0]["IEG"]);
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
                objprofiledwgval.ASH_dimval = Convert.ToDouble(dt.Rows[0]["ASH"]);
            }
            else if (objfile.filetypecode == "MI")
            {
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.IEG_dimval = Convert.ToDouble(dt.Rows[0]["IEG"]);
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
                objprofiledwgval.ASH_dimval = Convert.ToDouble(dt.Rows[0]["ASH"]);
            }

            else
            {
                objprofiledwgval.RMD_dimval = Convert.ToDouble(dt.Rows[0]["RMD"]);
                objprofiledwgval.IUP_dimval = Convert.ToDouble(dt.Rows[0]["IUP"]);
                objprofiledwgval.IOV_dimval = Convert.ToDouble(dt.Rows[0]["IOV"]);
                objprofiledwgval.IEG_dimval = Convert.ToDouble(dt.Rows[0]["IEG"]);
                objprofiledwgval.HRC_dimval = Convert.ToDouble(dt.Rows[0]["HRC"]);
                objprofiledwgval.ASH_dimval = Convert.ToDouble(dt.Rows[0]["ASH"]);
            }
            if (objfile.filetypecode == "RD")
            {
                objprofiledwgval.D_dimval_R = Convert.ToDouble(dt.Rows[0]["GBD"]);
                objprofiledwgval.RMD_dimval_R = Convert.ToDouble(dt.Rows[0]["RMD"]);
            }

            else
            {

            }
            objfile.flag = "get_display_val";
            DataTable dt1 = objBAL.get_DrawingValues(objfile);
            objprofiledwgval.FNM_dispval = dt1.Rows[0]["File Size"].ToString() + " INCH " +
                dt1.Rows[0]["File Type"].ToString();
            objprofiledwgval.FSZ_dispval = dt1.Rows[0]["File Size"].ToString();
            if (objfile.filetypecode == "RD")
            {
                objprofiledwgval.ATN_dispval_R = (dt1.Rows[0]["TAG"]).ToString();
                objprofiledwgval.RSH_dispval_R = (dt1.Rows[0]["TRM"]).ToString();
                objprofiledwgval.D_dispval_R = (dt1.Rows[0]["GBD"]).ToString();
                objprofiledwgval.RMD_dispval_R = (dt1.Rows[0]["RMD"]).ToString();
                objprofiledwgval.GPL_dispval = (dt1.Rows[0]["GPL"]).ToString();
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.RMW_dispval = (dt1.Rows[0]["RMW"]).ToString();
                objprofiledwgval.ATN_dispval_SQ = (dt1.Rows[0]["TAD"]).ToString();
                objprofiledwgval.RSH_dispval = (dt1.Rows[0]["TRM"]).ToString();
                objprofiledwgval.D_dispval = (dt1.Rows[0]["GBD"]).ToString();
                objprofiledwgval.ASH_dispval = dt1.Rows[0]["TAD"].ToString();
                objprofiledwgval.RSH_dispval = dt1.Rows[0]["TRM"].ToString();
                objprofiledwgval.GPL_dispval = dt1.Rows[0]["GBL"].ToString();
                objprofiledwgval.HRC_dispval = (dt1.Rows[0]["HRC"]).ToString();
                // objprofiledwgval.RMD_dispval_SQ = (dt1.Rows[0]["RMD"]).ToString();

            }
            else if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.RMW_dispval = (dt1.Rows[0]["RMW"]).ToString();
                objprofiledwgval.RMT_dispval = (dt1.Rows[0]["RMT"]).ToString();
                objprofiledwgval.FPL_dispval = (dt1.Rows[0]["FPL"]).ToString();
                objprofiledwgval.W_dispval = (dt1.Rows[0]["GBW"]).ToString();
                objprofiledwgval.T_dispval = (dt1.Rows[0]["GBT"]).ToString();
                objprofiledwgval.GPL_dispval = (dt1.Rows[0]["GPL"]).ToString();
            }

            else
            {
                objprofiledwgval.RMW_dispval = (dt1.Rows[0]["RMW"]).ToString();
                objprofiledwgval.RMT_dispval = (dt1.Rows[0]["RMT"]).ToString();
                objprofiledwgval.FPL_dispval = (dt1.Rows[0]["FPL"]).ToString();
                objprofiledwgval.W_dispval = (dt1.Rows[0]["GBW"]).ToString();
                objprofiledwgval.T_dispval = (dt1.Rows[0]["GBT"]).ToString();
                objprofiledwgval.GPL_dispval = (dt1.Rows[0]["GPL"]).ToString();
            }
            objprofiledwgval.CML_dispval = (dt1.Rows[0]["CML"]).ToString();
            objprofiledwgval.BL_dispval = (dt1.Rows[0]["BLG"]).ToString();
            objprofiledwgval.TL_dispval = (dt1.Rows[0]["TLG"]).ToString();
            objprofiledwgval.TWS_dispval = (dt1.Rows[0]["TWS"]).ToString();
            objprofiledwgval.TTS_dispval = (dt1.Rows[0]["TTS"]).ToString();
            objprofiledwgval.TWT_dispval = (dt1.Rows[0]["TWT"]).ToString();
            objprofiledwgval.TTT_dispval = (dt1.Rows[0]["TTT"]).ToString();
            objprofiledwgval.TPL_dispval = (dt1.Rows[0]["TPL"]).ToString();
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }

            objprofiledwgval.USL_dispval = (dt1.Rows[0]["USL"]).ToString();
            if (objfile.filetypecode == "FL")
            {
                objprofiledwgval.UEL_dispval = (dt1.Rows[0]["UEL"]).ToString();
            }
            if (objfile.filetypecode == "HR")
            {
                objprofiledwgval.USLF_dispval_HR = (dt1.Rows[0]["USL"]).ToString();
                objprofiledwgval.USLR_dispval_HR = (dt1.Rows[0]["UEL"]).ToString();
                objprofiledwgval.IUPF_dispval_HR = (dt1.Rows[0]["IUF"]).ToString();
                objprofiledwgval.IUPR_dispval_HR = (dt1.Rows[0]["IUR"]).ToString();
                objprofiledwgval.IOVF_dispval_HR = (dt1.Rows[0]["IOF"]).ToString();
                objprofiledwgval.IOVR_dispval_HR = (dt1.Rows[0]["IOR"]).ToString();
                objprofiledwgval.HRC_dispval = (dt1.Rows[0]["HRC"]).ToString();
                objprofiledwgval.L_dispval= (dt1.Rows[0]["BSC"]).ToString();
            }
            else if (objfile.filetypecode == "SQ")
            {
                objprofiledwgval.IUP_dispval = (dt1.Rows[0]["IUP"]).ToString();
                objprofiledwgval.IOV_dispval = (dt1.Rows[0]["IOV"]).ToString();
                objprofiledwgval.L_dispval = (dt1.Rows[0]["BSC"]).ToString();
            }
            else
            {
                objprofiledwgval.IUP_dispval = (dt1.Rows[0]["IUP"]).ToString();
                objprofiledwgval.IOV_dispval = (dt1.Rows[0]["IOV"]).ToString();
                objprofiledwgval.IEG_dispval = (dt1.Rows[0]["IEG"]).ToString();
                objprofiledwgval.HRC_dispval = (dt1.Rows[0]["HRC"]).ToString();
                objprofiledwgval.ASH_dispval = (dt1.Rows[0]["ASH"]).ToString();
            }

            objprofiledwgval.CSP_dispval = cutstandard;
            objprofiledwgval.CT_dispval = cuttype;

            objfile.flag = "get_tpi_values";
            DataTable dt3 = objBAL.get_DrawingValues(objfile);
            if (dt3 != null && dt3.Rows.Count > 0)
            {
                objprofiledwgval.UCT_dispval = dt3.Rows[0]["U/C"].ToString();
                objprofiledwgval.OCT_dispval = dt3.Rows[0]["O/C"].ToString();
                objprofiledwgval.ECT_dispval = dt3.Rows[0]["E/C"].ToString();

                if(objfile.filetypecode=="HR")
                {
                    objprofiledwgval.UP1GN_dispval_HR = dt3.Rows[0]["U/C"].ToString();
                    objprofiledwgval.OV1GN_dispval_HR= dt3.Rows[0]["O/C"].ToString();
                }

                if (objfile.filetypecode == "RD")
                {
                    objprofiledwgval.OCR_dispval = dt3.Rows[1]["O/C"].ToString();
                    objprofiledwgval.UCR_dispval = dt3.Rows[1]["U/C"].ToString();
                }
            }

            objprofiledwgval.CT_dispval = cuttype;
            objprofiledwgval.CSP_dispval = cutstandard;
            objprofiledwgval.FSZ_dispval = objfile.filesizecode;

            objprofiledwgval.FNM_dispval = dt1.Rows[0]["File Size"].ToString() + " INCH " +
               dt1.Rows[0]["File Type"].ToString();
            objprofiledwgval.FSZ_dispval = dt1.Rows[0]["File Size"].ToString();

            DxfDocument test = new DxfDocument();
            DxfDocument dxf = new DxfDocument();
            //Profile prf1 = new Profile();
            Coordinate vertcord = new Coordinate();

            string path = "";
            if (objfile.filetypecode == "FL")
            {
                path = Server.MapPath("~/A4 FLAT.dxf");
                string path1 = Server.MapPath("~/A3-FLAT.dxf");
                switch (objprofiledwgval.FSZ_dispval)
                {
                    case "4.0":
                        DxfDocument test1 = DxfDocument.Load(path);
                        test = test1;
                        objprofiledwgval.VST_x = 25;
                        objprofiledwgval.VST_y = 120;
                        break;
                    case "6.0":
                        DxfDocument test3 = DxfDocument.Load(path);
                        test = test3;
                        objprofiledwgval.VST_x = 25;
                        objprofiledwgval.VST_y = 120;
                        break;
                    default:
                        DxfDocument test4 = DxfDocument.Load(path1);
                        test = test4;
                        objprofiledwgval.VST_x = 40;
                        objprofiledwgval.VST_y = 180;
                        break;
                }
            }
            else if (objfile.filetypecode == "MI")
            {
                path = Server.MapPath("~/A4 MIL.dxf");

                switch (objprofiledwgval.FSZ_dispval)
                {
                    case "4.0":
                        DxfDocument test1 = DxfDocument.Load(path);
                        test = test1;
                        break;
                    case "6.0":
                        DxfDocument test3 = DxfDocument.Load(path);
                        test = test3;
                        break;
                }
            }
            else if (objfile.filetypecode == "HR")
            {
                test = DxfDocument.Load(Server.MapPath("~/A4 HALF ROUND.dxf"));

            }
            else if (objfile.filetypecode == "SQ")
            {
                test = DxfDocument.Load(Server.MapPath("~/A4 SQUARE.dxf"));
            }
            else if (objfile.filetypecode == "RD")
            {
                test = DxfDocument.Load(Server.MapPath("~/A4 ROUND.dxf"));
            }
            else if (objfile.filetypecode == "TP")
            {
                test = DxfDocument.Load(Server.MapPath("~/A4 TAPER.dxf"));
            }
            //if (prf1.FSZ_dispval == "4" || prf1.FSZ_dispval == "6")
            //{
            //    DxfDocument test = DxfDocument.Load(@"G:\A4-FLAT.dxf");
            //}
            //else
            //{
            //    DxfDocument test = DxfDocument.Load(@"G:\A3-FLAT.dxf");
            //}

            test.DrawingVariables.LtScale = 10;
            test.DrawingVariables.TextStyle = "Romans.ttf";
            test.DrawingVariables.TextSize = 2.5;




            // dimension style settings

            DimensionStyle myStyle = new DimensionStyle("MyStyle");
            myStyle.TextStyle = new TextStyle("Romans.ttf");
            // myStyle.DimSuffix = "mm";
            myStyle.TextHeight = 2.5;
            myStyle.LengthPrecision = 2;
            myStyle.DimLineColor = AciColor.Red;
            myStyle.TextColor = AciColor.ByLayer;

            myStyle.DimArrow1 = DimensionArrowhead.Closed;
            myStyle.DimArrow2 = DimensionArrowhead.Closed;
            myStyle.ArrowSize = 2.5;
            myStyle.DimLineExtend = 20;
            myStyle.TextOffset = 1.5;
            myStyle.ExtLineOffset = 1;
            myStyle.ExtLineExtend = 1.5;
            myStyle.DimScaleOverall = 1;


            if (objfile.filetypecode == "FL")
                Flatfileprofile(objprofiledwgval, myStyle, test);
            else if (objfile.filetypecode == "MI")
                Milfileprofile(objprofiledwgval, myStyle, test);
            else if (objfile.filetypecode == "HR")
            {
                Halfroundprofile(objprofiledwgval, myStyle, test);
            }
            else if (objfile.filetypecode == "SQ")
                squarefileprofile(objprofiledwgval, myStyle, test);
            else if (objfile.filetypecode == "RD")
                Roundfileprofile(objprofiledwgval, myStyle, test);
            else if (objfile.filetypecode == "TP")
                Taperprofile(objprofiledwgval, myStyle, test);
            string imagename = "";
            //if (objfile.filesizecode != null && objfile.filetypecode != null && objfile.filesubtypecode != null
            //    && objfile.cuttypecode != null &&
            //    objfile.cutstandard != null)
            //{
                imagename = objfile.filesizecode.ToString() + objfile.filetypecode.ToString()
                    + objfile.filesubtypecode.ToString() + objfile.cuttypecode.ToString() + objfile.cutstandard.ToString();
            //}
            // drawrectwithhatch(200, 400, 200, 200, test);
            test.Save(Server.MapPath("~/pdf/dwg/" + imagename + ".dxf"));
            //return JavaScript(alert("Saved Successfully"));
            // return new Javascript() { Script = "alert('Successfully registered');" };
            // return RedirectToAction("DrawingTab");


            

            return RedirectToAction("Index", "DrawingMaster");
            // ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('File Generated Successfully');", true);
        }

        #region DXF code
        private void Flatfileprofile(Profile prf, DimensionStyle myStyle, DxfDocument tst1)
        {
            //string s = "G:\\trupti\\A4-FLAT.dxf";
            //DxfDocument v = DxfDocument.Load("s");
            //v.Save(s);
            // Calculate coordinates of all vertices     

            prf.V1_x = prf.VST_x;
            prf.V1_y = prf.VST_y + prf.TWT_dimval / 2;
            prf.V2_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V2_y = prf.VST_y + prf.TWS_dimval / 2;
            prf.V3_x = prf.VST_x + prf.TL_dimval;
            prf.V3_y = prf.VST_y + prf.W_dimval / 2;
            prf.V4_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V4_y = prf.VST_y + prf.W_dimval / 2;
            prf.V5_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V5_y = prf.VST_y + prf.FPL_dimval / 2;
            prf.V6_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V6_y = prf.VST_y - prf.FPL_dimval / 2;
            prf.V7_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V7_y = prf.VST_y - prf.W_dimval / 2;
            prf.V8_x = prf.VST_x + prf.TL_dimval;
            prf.V8_y = prf.VST_y - prf.W_dimval / 2;

            prf.V9_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V9_y = prf.VST_y - prf.TWS_dimval / 2;

            prf.V10_x = prf.VST_x;
            prf.V10_y = prf.VST_y - prf.TWT_dimval / 2;
            prf.VC1_x = prf.VST_x - 10;
            prf.VC1_y = prf.VST_y;
            prf.VC2_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 80;
            prf.VC2_y = prf.VST_y;
            prf.V11_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 60;
            prf.V11_y = prf.VST_y + prf.W_dimval / 2;
            prf.Vuc1_x = prf.V8_x + prf.USL_dimval;
            prf.Vuc1_y = prf.V8_y;
            prf.Vuc2_x = prf.V8_x + prf.USL_dimval - (prf.USL_dimval / 2);
            prf.Vuc2_y = prf.V8_y + prf.W_dimval;
            prf.Vuc3_x = prf.V8_x + prf.USL_dimval + (prf.USL_dimval / 2);
            prf.Vuc3_y = prf.V8_y + prf.W_dimval;
            prf.V12_x = prf.V11_x + prf.T_dimval;
            prf.V12_y = prf.V11_y;
            prf.V13_x = prf.V11_x;
            prf.V13_y = prf.V11_y - prf.W_dimval;
            prf.V14_x = prf.V11_x + prf.T_dimval;
            prf.V14_y = prf.V11_y - prf.W_dimval;
            prf.Vh1_x = prf.Vuc3_x + 35;
            prf.Vh1_y = prf.Vuc3_y;
            prf.Vh2_x = prf.Vuc1_x + 35;
            prf.Vh2_y = prf.Vuc1_y;

            //prf.CT_dispval = Session["CutType"].ToString();
            //prf.CSP_dispval = Session["CutSpecification"].ToString();
            //prf.FSZ_dispval = Session["FileSizeCode"].ToString();

            //Draw lines joining above 10 vertices for basic profile
            Line l1 = new Line(new Vector2(prf.VST_x, prf.VST_y), new Vector2(prf.V1_x, prf.V1_y));
            tst1.AddEntity(l1);
            Line l2 = new Line(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V2_x, prf.V2_y));
            tst1.AddEntity(l2);
            Line l3 = new Line(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V3_x, prf.V3_y));
            tst1.AddEntity(l3);
            Line l4 = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V4_x, prf.V4_y));
            tst1.AddEntity(l4);
            Line l5 = new Line(new Vector2(prf.V4_x, prf.V4_y), new Vector2(prf.V5_x, prf.V5_y));
            tst1.AddEntity(l5);
            Line l6 = new Line(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y));
            tst1.AddEntity(l6);
            Line l7 = new Line(new Vector2(prf.V6_x, prf.V6_y), new Vector2(prf.V7_x, prf.V7_y));
            tst1.AddEntity(l7);
            Line l8 = new Line(new Vector2(prf.V7_x, prf.V7_y), new Vector2(prf.V8_x, prf.V8_y));
            tst1.AddEntity(l8);
            Line l9 = new Line(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.V9_x, prf.V9_y));
            tst1.AddEntity(l9);
            Line l10 = new Line(new Vector2(prf.V9_x, prf.V9_y), new Vector2(prf.V10_x, prf.V10_y));
            tst1.AddEntity(l10);
            Line l11 = new Line(new Vector2(prf.V10_x, prf.V10_y), new Vector2(prf.VST_x, prf.VST_y));
            tst1.AddEntity(l11);

            Line l13 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc2_x, prf.Vuc2_y));
            tst1.AddEntity(l13);
            Line l14 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc3_x, prf.Vuc3_y));
            tst1.AddEntity(l14);


            Layer layer2 = new Layer("Layer2");
            layer2.Color = AciColor.Green;

            // draw side view
            //  drawrect (prf.V11_x, prf.V11_y,prf.T,prf.W,tst1);
            LwPolyline poly3 = new LwPolyline();



            poly3.Vertexes.Add(new LwPolylineVertex(prf.V11_x, prf.V11_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V12_x, prf.V12_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V14_x, prf.V14_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V13_x, prf.V13_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V11_x, prf.V11_y));
            tst1.AddEntity(poly3);

            // draw centre line
            layer2.Linetype = Linetype.Center;
            Line l12 = new Line(new Vector2(prf.VC1_x, prf.VC1_y), new Vector2(prf.VC2_x, prf.VC2_y));
            tst1.AddEntity(l12);
            l12.Layer = layer2;

            //draw hatching for overcut
            LwPolyline poly1 = new LwPolyline();

            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            //poly1.Layer = layer1;

            tst1.AddEntity(poly1);
            HatchBoundaryPath boundary1 = new HatchBoundaryPath(new List<EntityObject> { poly1 });
            HatchPattern pattern1 = HatchPattern.Line;
            pattern1.Scale = 10;
            pattern1.Angle = 115;
            Hatch hatch1 = new Hatch(pattern1, true);
            hatch1.BoundaryPaths.Add(boundary1);
            tst1.AddEntity(hatch1);
            //draw hatching for upcut
            LwPolyline poly = new LwPolyline();
            Layer layer1 = new Layer("Layer1");


            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc3_x, prf.Vuc3_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));

            poly.Layer = layer1;
            tst1.AddEntity(poly);
            HatchBoundaryPath boundary = new HatchBoundaryPath(new List<EntityObject> { poly });
            HatchPattern pattern = HatchPattern.Line;
            pattern.Scale = 3;
            pattern.Angle = 65;
            Hatch hatch = new Hatch(pattern, true);
            hatch.BoundaryPaths.Add(boundary);
            tst1.AddEntity(hatch);






            // draw all dimensions
            AlignedDimension t1 = new AlignedDimension(new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), new Vector2(prf.VST_x, prf.VST_y), 2.5, myStyle);
            // draw dimension A
            AlignedDimension aln1 = new AlignedDimension(new Vector2(prf.V1_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y), 4.5, myStyle);
            tst1.AddEntity(aln1);
            // draw dimension B
            AlignedDimension aln2 = new AlignedDimension(new Vector2(prf.V3_x, prf.V5_y), new Vector2(prf.V5_x, prf.V5_y), 6, myStyle);
            tst1.AddEntity(aln2);
            // draw dimension F
            AlignedDimension aln3 = new AlignedDimension(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y), -6, myStyle);
            tst1.AddEntity(aln3);
            // draw dimension E
            AlignedDimension aln4 = new AlignedDimension(new Vector2(prf.V6_x, prf.V7_y), new Vector2(prf.V7_x, prf.V7_y), -6, myStyle);
            tst1.AddEntity(aln4);
            // draw dimension D
            AlignedDimension aln5 = new AlignedDimension(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V10_x, prf.V10_y), 6, myStyle);
            tst1.AddEntity(aln5);

            // side view dimension
            // W dimension
            AlignedDimension aln6 = new AlignedDimension(new Vector2(prf.V12_x, prf.V12_y), new Vector2(prf.V14_x, prf.V14_y), -6, myStyle);
            tst1.AddEntity(aln6);
            // T dimension
            AlignedDimension aln7 = new AlignedDimension(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V12_x, prf.V12_y), 6, myStyle);
            tst1.AddEntity(aln7);

            // angle dimension
            Line tmpln = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y + 2));
            tst1.AddEntity(tmpln);

            // Angular2LineDimension dim1 = new Angular2LineDimension(tmpln, l3, 3, myStyle);
            // tst1.AddEntity(dim1);

            //uncut dimension
            // draw dimension D
            AlignedDimension aln8 = new AlignedDimension(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.Vuc1_x, prf.Vuc1_y), -6, myStyle);
            tst1.AddEntity(aln8);
            Text tx4 = new Text("UNCUT", new Vector2(prf.V8_x + 2, prf.V8_y - 10), 2.5);
            tst1.AddEntity(tx4);
            AlignedDimension aln9 = new AlignedDimension(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V9_x, prf.V9_y), 4, myStyle);
            tst1.AddEntity(aln9);

            //leader text OVERCUT and UPCUT
            TextStyle txtst = new TextStyle("Tahoma.ttf");
            Leader leader1 = new Leader("OVERCUT", new List<Vector2> { new Vector2(prf.V4_x - 12, prf.V4_y - 3), new Vector2(prf.V4_x + 10, prf.V4_y + 10) }, myStyle);
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader1);
            Leader leader2 = new Leader("UPCUT", new List<Vector2> { new Vector2(prf.V3_x + 12, prf.V3_y - 3), new Vector2(prf.V3_x + 15, prf.V3_y + 10) }, myStyle);
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader2);
            //--------------------- text---------------------------
            Double h1 = 1.8;
            Double h2 = 3.5;
            //if (prf.FSZ_dispval == "4" || prf.FSZ_dispval == "6")
            //{
            //string FSZ_dispval = "6";
            switch (prf.FSZ_dispval)
            {
                case "4.0":
                    Text tx3 = new Text(prf.RMW_dispval, new Vector2(prf.RMW_dispvalloc_x, prf.RMW_dispvalloc_y), h1);
                    tst1.AddEntity(tx3);
                    Text tx5 = new Text(prf.RMT_dispval, new Vector2(prf.RMT_dispvalloc_x, prf.RMT_dispvalloc_y), h1);
                    tst1.AddEntity(tx5);
                    Text tx6 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalloc_x, prf.CML_dispvalloc_y), h1);
                    tst1.AddEntity(tx6);
                    Text tx7 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalloc_x, prf.BL_dispvalloc_y), h1);
                    tst1.AddEntity(tx7);
                    Text tx8 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalloc_x, prf.TL_dispvalloc_y), h1);
                    tst1.AddEntity(tx8);
                    Text tx9 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), h1);
                    tst1.AddEntity(tx9);
                    Text tx10 = new Text(prf.TTS_dispval, new Vector2(prf.TTS_dispvalloc_x, prf.TTS_dispvalloc_y), h1);
                    tst1.AddEntity(tx10);
                    Text tx11 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalloc_x, prf.TWT_dispvalloc_y), h1);
                    tst1.AddEntity(tx11);
                    Text tx12 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), h1);
                    tst1.AddEntity(tx12);
                    Text tx13 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dispvalloc_x, prf.TTT_dispvalloc_y), h1);
                    tst1.AddEntity(tx13);
                    Text tx14 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dispvalloc_x, prf.TTT_dispvalloc_y), h1);
                    tst1.AddEntity(tx14);
                    Text tx15 = new Text(prf.W_dispval, new Vector2(prf.W_dispvalloc_x, prf.W_dispvalloc_y), h1);
                    tst1.AddEntity(tx15);
                    Text tx16 = new Text(prf.T_dispval, new Vector2(prf.T_dispvalloc_x, prf.T_dispvalloc_y), h1);
                    tst1.AddEntity(tx16);
                    Text tx17 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalloc_x, prf.GPL_dispvalloc_y), h1);
                    tst1.AddEntity(tx17);
                    Text tx18 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_x, prf.TPL_dispvalloc_y), h1);
                    tst1.AddEntity(tx18);
                    Text tx20 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalloc_x, prf.L_dispvalloc_y), h1);
                    tst1.AddEntity(tx20);
                    Text tx21 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalloc_x, prf.IUP_dispvalloc_y), h1);
                    tst1.AddEntity(tx21);
                    Text tx22 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_x, prf.IOV_dispvalloc_y), h1);
                    tst1.AddEntity(tx22);
                    Text tx23 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_x, prf.IOV_dispvalloc_y), h1);
                    tst1.AddEntity(tx23);
                    Text tx24 = new Text(prf.IEG_dispval, new Vector2(prf.IEG_dispvalloc_x, prf.IEG_dispvalloc_y), h1);
                    tst1.AddEntity(tx24);
                    Text tx26 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalloc_x, prf.HRC_dispvalloc_y), h1);
                    tst1.AddEntity(tx26);

                    Text tx27 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalloc_x, prf.CT_dispvalloc_y), h1);
                    tst1.AddEntity(tx27);
                    Text tx28 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalloc_x, prf.CSP_dispvalloc_y), h1);
                    tst1.AddEntity(tx28);
                    Text tx29 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalloc_x, prf.UCT_dispvalloc_y), h1);
                    tst1.AddEntity(tx29);
                    Text tx30 = new Text(prf.ECT_dispval, new Vector2(prf.ECT_dispvalloc_x, prf.ECT_dispvalloc_y), h1);
                    tst1.AddEntity(tx30);
                    Text tx31 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalloc_x, prf.OCT_dispvalloc_y), h1);
                    tst1.AddEntity(tx31);
                    Text tx32 = new Text(prf.FPL_dispval, new Vector2(prf.FPL_dispvalloc_x, prf.FPL_dispvalloc_y), h1);
                    tst1.AddEntity(tx32);

                    Text tx33 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalloc_x, prf.FSZ_dispvalloc_y), h1);
                    tst1.AddEntity(tx33);
                    Text tx34 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalloc_x, prf.PTN_dispvalloc_y), h1);
                    tst1.AddEntity(tx34);
                    Text tx35 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalloc_x, prf.FNM_dispvalloc_y), h1);
                    tst1.AddEntity(tx35);
                    Text tx36 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalloc_x, prf.USL_dispvalloc_y), h1);
                    tst1.AddEntity(tx36);
                    Text tx37 = new Text(prf.UEL_dispval, new Vector2(prf.UEL_dispvalloc_x, prf.UEL_dispvalloc_y), h1);
                    tst1.AddEntity(tx37);
                    Text tx38 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalloc_x, prf.RMW1_dispvalloc_y), h1);
                    tst1.AddEntity(tx38);
                    Text tx39 = new Text(prf.RMT_dispval, new Vector2(prf.RMT1_dispvalloc_x, prf.RMT1_dispvalloc_y), h1);
                    tst1.AddEntity(tx39);
                    break;
                case "6.0":
                    Text tx40 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalA3_x, prf.RMW1_dispvalA3_y), h2);
                    tst1.AddEntity(tx40);
                    Text tx41 = new Text(prf.RMT_dispval, new Vector2(prf.RMT_dispvalA3_x, prf.RMT_dispvalA3_y), h2);
                    tst1.AddEntity(tx41);
                    Text tx42 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalA3_x, prf.CML_dispvalA3_y), h2);
                    tst1.AddEntity(tx42);
                    Text tx44 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalA3_x, prf.BL_dispvalA3_y), h2);
                    tst1.AddEntity(tx44);
                    Text tx45 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalA3_x, prf.TL_dispvalA3_y), h2);
                    tst1.AddEntity(tx45);
                    Text tx46 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalA3_x, prf.TWS_dispvalA3_y), h2);
                    tst1.AddEntity(tx46);
                    Text tx47 = new Text(prf.TTS_dispval, new Vector2(prf.TWS_dispvalA3_x, prf.TWS_dispvalA3_y), h2);
                    tst1.AddEntity(tx47);
                    Text tx48 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalA3_x, prf.TWT_dispvalA3_y), h2);
                    tst1.AddEntity(tx48);
                    Text tx49 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dispvalA3_x, prf.TTT_dispvalA3_y), h2);
                    tst1.AddEntity(tx49);
                    Text tx50 = new Text(prf.W_dispval, new Vector2(prf.W_dispvalA3_x, prf.W_dispvalA3_y), h2);
                    tst1.AddEntity(tx50);
                    Text tx51 = new Text(prf.T_dispval, new Vector2(prf.T_dispvalA3_x, prf.T_dispvalA3_y), h2);
                    tst1.AddEntity(tx51);
                    Text tx52 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalA3_x, prf.GPL_dispvalA3_y), h2);
                    tst1.AddEntity(tx52);
                    Text tx53 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalA3_x, prf.TPL_dispvalA3_y), h2);
                    tst1.AddEntity(tx53);
                    Text tx54 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalA3_x, prf.L_dispvalA3_y), h2);
                    tst1.AddEntity(tx54);
                    Text tx55 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalA3_x, prf.IUP_dispvalA3_y), h2);
                    tst1.AddEntity(tx55);
                    Text tx56 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalA3_x, prf.IOV_dispvalA3_y), h2);
                    tst1.AddEntity(tx56);
                    Text tx57 = new Text(prf.IEG_dispval, new Vector2(prf.IEG_dispvalA3_x, prf.IEG_dispvalA3_y), h2);
                    tst1.AddEntity(tx57);
                    Text tx58 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalA3_x, prf.HRC_dispvalA3_y), h2);
                    tst1.AddEntity(tx58);

                    Text tx59 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalA3_x, prf.CT_dispvalA3_y), h2);
                    tst1.AddEntity(tx59);
                    Text tx60 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalA3_x, prf.CSP_dispvalA3_y), h2);
                    tst1.AddEntity(tx60);
                    Text tx61 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalA3_x, prf.UCT_dispvalA3_y), h2);
                    tst1.AddEntity(tx61);
                    Text tx62 = new Text(prf.ECT_dispval, new Vector2(prf.ECT_dispvalA3_x, prf.ECT_dispvalA3_y), h2);
                    tst1.AddEntity(tx62);
                    Text tx63 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalA3_x, prf.OCT_dispvalA3_y), h2);
                    tst1.AddEntity(tx63);
                    Text tx64 = new Text(prf.FPL_dispval, new Vector2(prf.FPL_dispvalA3_x, prf.FPL_dispvalA3_y), h2);
                    tst1.AddEntity(tx64);

                    Text tx65 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalA3_x, prf.FSZ_dispvalA3_y), h2);
                    tst1.AddEntity(tx65);
                    Text tx66 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalA3_x, prf.PTN_dispvalA3_y), h2);
                    tst1.AddEntity(tx66);
                    Text tx67 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalA3_x, prf.FNM_dispvalA3_y), h2);
                    tst1.AddEntity(tx67);
                    Text tx68 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalA3_x, prf.USL_dispvalA3_y), h2);
                    tst1.AddEntity(tx68);
                    Text tx69 = new Text(prf.UEL_dispval, new Vector2(prf.UEL_dispvalA3_x, prf.UEL_dispvalA3_y), h2);
                    tst1.AddEntity(tx69);
                    Text tx70 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalA3_x, prf.RMW1_dispvalA3_y), h2);
                    tst1.AddEntity(tx70);
                    Text tx71 = new Text(prf.RMT_dispval, new Vector2(prf.RMT1_dispvalA3_x, prf.RMT1_dispvalA3_y), h2);
                    tst1.AddEntity(tx71);
                    break;
            }





            //}
        }

        private void Milfileprofile(Profile prf, DimensionStyle myStyle, DxfDocument tst1)
        {
            //string s = "G:\\trupti\\A4-FLAT.dxf";
            //DxfDocument v = DxfDocument.Load("s");
            //v.Save(s);
            // Calculate coordinates of all vertices     
            prf.VST_x = 25;
            prf.VST_y = 120;
            prf.V1_x = prf.VST_x;
            prf.V1_y = prf.VST_y + prf.TWT_dimval / 2;
            prf.V2_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V2_y = prf.VST_y + prf.TWS_dimval / 2;
            prf.V3_x = prf.VST_x + prf.TL_dimval;
            prf.V3_y = prf.VST_y + prf.W_dimval / 2;
            prf.V4_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V4_y = prf.VST_y + prf.W_dimval / 2;
            prf.V5_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V5_y = prf.VST_y + prf.FPL_dimval / 2;
            prf.V6_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V6_y = prf.VST_y - prf.FPL_dimval / 2;
            prf.V7_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V7_y = prf.VST_y - prf.W_dimval / 2;
            prf.V8_x = prf.VST_x + prf.TL_dimval;
            prf.V8_y = prf.VST_y - prf.W_dimval / 2;

            prf.V9_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V9_y = prf.VST_y - prf.TWS_dimval / 2;

            prf.V10_x = prf.VST_x;
            prf.V10_y = prf.VST_y - prf.TWT_dimval / 2;
            prf.VC1_x = prf.VST_x - 10;
            prf.VC1_y = prf.VST_y;
            prf.VC2_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 80;
            prf.VC2_y = prf.VST_y;
            prf.V11_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 60;
            prf.V11_y = prf.VST_y + prf.W_dimval / 2;
            prf.Vuc1_x = prf.V8_x + prf.USL_dimval;
            prf.Vuc1_y = prf.V8_y;
            prf.Vuc2_x = prf.V8_x + prf.USL_dimval - (prf.USL_dimval / 2);
            prf.Vuc2_y = prf.V8_y + prf.W_dimval;
            prf.Vuc3_x = prf.V8_x + prf.USL_dimval + (prf.USL_dimval / 2);
            prf.Vuc3_y = prf.V8_y + prf.W_dimval;
            prf.V12_x = prf.V11_x + prf.T_dimval;
            prf.V12_y = prf.V11_y;
            prf.V13_x = prf.V11_x;
            prf.V13_y = prf.V11_y - prf.W_dimval;
            prf.V14_x = prf.V11_x + prf.T_dimval;
            prf.V14_y = prf.V11_y - prf.W_dimval;
            prf.Vh1_x = prf.Vuc3_x + 35;
            prf.Vh1_y = prf.Vuc3_y;
            prf.Vh2_x = prf.Vuc1_x + 35;
            prf.Vh2_y = prf.Vuc1_y;


            //Draw lines joining above 10 vertices for basic profile
            Line l1 = new Line(new Vector2(prf.VST_x, prf.VST_y), new Vector2(prf.V1_x, prf.V1_y));
            tst1.AddEntity(l1);
            Line l2 = new Line(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V2_x, prf.V2_y));
            tst1.AddEntity(l2);
            Line l3 = new Line(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V3_x, prf.V3_y));
            tst1.AddEntity(l3);
            Line l4 = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V4_x, prf.V4_y));
            tst1.AddEntity(l4);
            Line l5 = new Line(new Vector2(prf.V4_x, prf.V4_y), new Vector2(prf.V5_x, prf.V5_y));
            tst1.AddEntity(l5);
            Line l6 = new Line(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y));
            tst1.AddEntity(l6);
            Line l7 = new Line(new Vector2(prf.V6_x, prf.V6_y), new Vector2(prf.V7_x, prf.V7_y));
            tst1.AddEntity(l7);
            Line l8 = new Line(new Vector2(prf.V7_x, prf.V7_y), new Vector2(prf.V8_x, prf.V8_y));
            tst1.AddEntity(l8);
            Line l9 = new Line(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.V9_x, prf.V9_y));
            tst1.AddEntity(l9);
            Line l10 = new Line(new Vector2(prf.V9_x, prf.V9_y), new Vector2(prf.V10_x, prf.V10_y));
            tst1.AddEntity(l10);
            Line l11 = new Line(new Vector2(prf.V10_x, prf.V10_y), new Vector2(prf.VST_x, prf.VST_y));
            tst1.AddEntity(l11);

            Line l13 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc2_x, prf.Vuc2_y));
            tst1.AddEntity(l13);
            Line l14 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc3_x, prf.Vuc3_y));
            tst1.AddEntity(l14);


            Layer layer2 = new Layer("Layer2");
            layer2.Color = AciColor.Green;

            // draw side view
            //  drawrect (prf.V11_x, prf.V11_y,prf.T,prf.W,tst1);
            LwPolyline poly3 = new LwPolyline();



            poly3.Vertexes.Add(new LwPolylineVertex(prf.V11_x, prf.V11_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V12_x, prf.V12_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V14_x, prf.V14_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V13_x, prf.V13_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V11_x, prf.V11_y));
            tst1.AddEntity(poly3);

            // draw centre line
            layer2.Linetype = Linetype.Center;
            Line l12 = new Line(new Vector2(prf.VC1_x, prf.VC1_y), new Vector2(prf.VC2_x, prf.VC2_y));
            tst1.AddEntity(l12);
            l12.Layer = layer2;

            //draw hatching for overcut
            LwPolyline poly1 = new LwPolyline();

            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            //poly1.Layer = layer1;

            tst1.AddEntity(poly1);
            HatchBoundaryPath boundary1 = new HatchBoundaryPath(new List<EntityObject> { poly1 });
            HatchPattern pattern1 = HatchPattern.Line;
            pattern1.Scale = 10;
            pattern1.Angle = 115;
            Hatch hatch1 = new Hatch(pattern1, true);
            hatch1.BoundaryPaths.Add(boundary1);
            tst1.AddEntity(hatch1);
            //draw hatching for upcut
            LwPolyline poly = new LwPolyline();
            Layer layer1 = new Layer("Layer1");


            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc3_x, prf.Vuc3_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));

            poly.Layer = layer1;
            tst1.AddEntity(poly);
            HatchBoundaryPath boundary = new HatchBoundaryPath(new List<EntityObject> { poly });
            HatchPattern pattern = HatchPattern.Line;
            pattern.Scale = 3;
            pattern.Angle = 65;
            Hatch hatch = new Hatch(pattern, true);
            hatch.BoundaryPaths.Add(boundary);
            tst1.AddEntity(hatch);






            // draw all dimensions
            AlignedDimension t1 = new AlignedDimension(new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), new Vector2(prf.VST_x, prf.VST_y), 2.5, myStyle);
            // draw dimension A
            AlignedDimension aln1 = new AlignedDimension(new Vector2(prf.V1_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y), 4.5, myStyle);
            tst1.AddEntity(aln1);
            // draw dimension B
            AlignedDimension aln2 = new AlignedDimension(new Vector2(prf.V3_x, prf.V5_y), new Vector2(prf.V5_x, prf.V5_y), 6, myStyle);
            tst1.AddEntity(aln2);
            // draw dimension F
            AlignedDimension aln3 = new AlignedDimension(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y), -6, myStyle);
            tst1.AddEntity(aln3);
            // draw dimension E
            AlignedDimension aln4 = new AlignedDimension(new Vector2(prf.V6_x, prf.V7_y), new Vector2(prf.V7_x, prf.V7_y), -6, myStyle);
            tst1.AddEntity(aln4);
            // draw dimension D
            AlignedDimension aln5 = new AlignedDimension(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V10_x, prf.V10_y), 6, myStyle);
            tst1.AddEntity(aln5);

            // side view dimension
            // W dimension
            AlignedDimension aln6 = new AlignedDimension(new Vector2(prf.V12_x, prf.V12_y), new Vector2(prf.V14_x, prf.V14_y), -6, myStyle);
            tst1.AddEntity(aln6);
            // T dimension
            AlignedDimension aln7 = new AlignedDimension(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V12_x, prf.V12_y), 6, myStyle);
            tst1.AddEntity(aln7);

            // angle dimension
            Line tmpln = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y + 2));
            tst1.AddEntity(tmpln);

            // Angular2LineDimension dim1 = new Angular2LineDimension(tmpln, l3, 3, myStyle);
            // tst1.AddEntity(dim1);

            //uncut dimension
            // draw dimension D
            AlignedDimension aln8 = new AlignedDimension(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.Vuc1_x, prf.Vuc1_y), -6, myStyle);
            tst1.AddEntity(aln8);
            Text tx4 = new Text("UNCUT", new Vector2(prf.V8_x + 2, prf.V8_y - 10), 2.5);
            tst1.AddEntity(tx4);
            AlignedDimension aln9 = new AlignedDimension(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V9_x, prf.V9_y), 4, myStyle);
            tst1.AddEntity(aln9);

            //leader text OVERCUT and UPCUT
            TextStyle txtst = new TextStyle("Tahoma.ttf");
            Leader leader1 = new Leader("OVERCUT", new List<Vector2> { new Vector2(prf.V4_x - 12, prf.V4_y - 3), new Vector2(prf.V4_x + 10, prf.V4_y + 10) }, myStyle);
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader1);
            Leader leader2 = new Leader("UPCUT", new List<Vector2> { new Vector2(prf.V3_x + 12, prf.V3_y - 3), new Vector2(prf.V3_x + 15, prf.V3_y + 10) }, myStyle);
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader2);
            //--------------------- text---------------------------
            Double h1 = 1.8;
            Double h2 = 3.5;
            //if (prf.FSZ_dispval == "4" || prf.FSZ_dispval == "6")
            //{
            //string FSZ_dispval = "6";
            switch (prf.FSZ_dispval)
            {
                case "4.0":
                    Text tx3 = new Text(prf.RMW_dispval, new Vector2(prf.RMW_dispvalloc_x, prf.RMW_dispvalloc_y), h1);
                    tst1.AddEntity(tx3);
                    Text tx5 = new Text(prf.RMT_dispval, new Vector2(prf.RMT_dispvalloc_x, prf.RMT_dispvalloc_y), h1);
                    tst1.AddEntity(tx5);
                    Text tx6 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalloc_x, prf.CML_dispvalloc_y), h1);
                    tst1.AddEntity(tx6);
                    Text tx7 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalloc_x, prf.BL_dispvalloc_y), h1);
                    tst1.AddEntity(tx7);
                    Text tx8 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalloc_x, prf.TL_dispvalloc_y), h1);
                    tst1.AddEntity(tx8);
                    Text tx9 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), h1);
                    tst1.AddEntity(tx9);
                    Text tx10 = new Text(prf.TTS_dispval, new Vector2(prf.TTS_dispvalloc_x, prf.TTS_dispvalloc_y), h1);
                    tst1.AddEntity(tx10);
                    Text tx11 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalloc_x, prf.TWT_dispvalloc_y), h1);
                    tst1.AddEntity(tx11);
                    Text tx12 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), h1);
                    tst1.AddEntity(tx12);
                    Text tx13 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dispvalloc_x, prf.TTT_dispvalloc_y), h1);
                    tst1.AddEntity(tx13);
                    Text tx14 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dispvalloc_x, prf.TTT_dispvalloc_y), h1);
                    tst1.AddEntity(tx14);
                    Text tx15 = new Text(prf.W_dispval, new Vector2(prf.W_dispvalloc_x, prf.W_dispvalloc_y), h1);
                    tst1.AddEntity(tx15);
                    Text tx16 = new Text(prf.T_dispval, new Vector2(prf.T_dispvalloc_x, prf.T_dispvalloc_y), h1);
                    tst1.AddEntity(tx16);
                    Text tx17 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalloc_x, prf.GPL_dispvalloc_y), h1);
                    tst1.AddEntity(tx17);
                    Text tx18 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_x, prf.TPL_dispvalloc_y), h1);
                    tst1.AddEntity(tx18);
                    //Text tx20 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalloc_x, prf.L_dispvalloc_y), h1);
                    //     tst1.AddEntity(tx20);
                    Text tx21 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalloc_x, prf.IUP_dispvalloc_y), h1);
                    tst1.AddEntity(tx21);
                    Text tx22 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_x, prf.IOV_dispvalloc_y), h1);
                    tst1.AddEntity(tx22);
                    Text tx23 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_x, prf.IOV_dispvalloc_y), h1);
                    tst1.AddEntity(tx23);
                    Text tx24 = new Text(prf.IEG_dispval, new Vector2(prf.IEG_dispvalloc_x, prf.IEG_dispvalloc_y), h1);
                    tst1.AddEntity(tx24);
                    Text tx26 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalloc_x, prf.HRC_dispvalloc_y), h1);
                    tst1.AddEntity(tx26);

                    Text tx27 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalloc_x, prf.CT_dispvalloc_y), h1);
                    tst1.AddEntity(tx27);
                    Text tx28 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalloc_x, prf.CSP_dispvalloc_y), h1);
                    tst1.AddEntity(tx28);
                    Text tx29 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalloc_x, prf.UCT_dispvalloc_y), h1);
                    tst1.AddEntity(tx29);
                    Text tx30 = new Text(prf.ECT_dispval, new Vector2(prf.ECT_dispvalloc_x, prf.ECT_dispvalloc_y), h1);
                    tst1.AddEntity(tx30);
                    Text tx31 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalloc_x, prf.OCT_dispvalloc_y), h1);
                    tst1.AddEntity(tx31);
                    Text tx32 = new Text(prf.FPL_dispval, new Vector2(prf.FPL_dispvalloc_x, prf.FPL_dispvalloc_y), h1);
                    tst1.AddEntity(tx32);

                    Text tx33 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalloc_x, prf.FSZ_dispvalloc_y), h1);
                    tst1.AddEntity(tx33);
                    Text tx34 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalloc_x, prf.PTN_dispvalloc_y), h1);
                    tst1.AddEntity(tx34);
                    Text tx35 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalloc_x, prf.FNM_dispvalloc_y), h1);
                    tst1.AddEntity(tx35);
                    Text tx36 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalloc_x, prf.USL_dispvalloc_y), h1);
                    tst1.AddEntity(tx36);
                    Text tx37 = new Text(prf.UEL_dispval, new Vector2(prf.UEL_dispvalloc_x, prf.UEL_dispvalloc_y), h1);
                    tst1.AddEntity(tx37);
                    Text tx38 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalloc_x, prf.RMW1_dispvalloc_y), h1);
                    tst1.AddEntity(tx38);
                    Text tx39 = new Text(prf.RMT_dispval, new Vector2(prf.RMT1_dispvalloc_x, prf.RMT1_dispvalloc_y), h1);
                    tst1.AddEntity(tx39);
                    break;
                case "6.0":
                    Text tx40 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalA3_x, prf.RMW1_dispvalA3_y), h2);
                    tst1.AddEntity(tx40);
                    Text tx41 = new Text(prf.RMT_dispval, new Vector2(prf.RMT_dispvalA3_x, prf.RMT_dispvalA3_y), h2);
                    tst1.AddEntity(tx41);
                    Text tx42 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalA3_x, prf.CML_dispvalA3_y), h2);
                    tst1.AddEntity(tx42);
                    Text tx44 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalA3_x, prf.BL_dispvalA3_y), h2);
                    tst1.AddEntity(tx44);
                    Text tx45 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalA3_x, prf.TL_dispvalA3_y), h2);
                    tst1.AddEntity(tx45);
                    Text tx46 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalA3_x, prf.TWS_dispvalA3_y), h2);
                    tst1.AddEntity(tx46);
                    Text tx47 = new Text(prf.TTS_dispval, new Vector2(prf.TWS_dispvalA3_x, prf.TWS_dispvalA3_y), h2);
                    tst1.AddEntity(tx47);
                    Text tx48 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalA3_x, prf.TWT_dispvalA3_y), h2);
                    tst1.AddEntity(tx48);
                    Text tx49 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dispvalA3_x, prf.TTT_dispvalA3_y), h2);
                    tst1.AddEntity(tx49);
                    Text tx50 = new Text(prf.W_dispval, new Vector2(prf.W_dispvalA3_x, prf.W_dispvalA3_y), h2);
                    tst1.AddEntity(tx50);
                    Text tx51 = new Text(prf.T_dispval, new Vector2(prf.T_dispvalA3_x, prf.T_dispvalA3_y), h2);
                    tst1.AddEntity(tx51);
                    Text tx52 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalA3_x, prf.GPL_dispvalA3_y), h2);
                    tst1.AddEntity(tx52);
                    Text tx53 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalA3_x, prf.TPL_dispvalA3_y), h2);
                    tst1.AddEntity(tx53);
                    // Text tx54 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalA3_x, prf.L_dispvalA3_y), h2);
                    // tst1.AddEntity(tx54);
                    Text tx55 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalA3_x, prf.IUP_dispvalA3_y), h2);
                    tst1.AddEntity(tx55);
                    Text tx56 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalA3_x, prf.IOV_dispvalA3_y), h2);
                    tst1.AddEntity(tx56);
                    Text tx57 = new Text(prf.IEG_dispval, new Vector2(prf.IEG_dispvalA3_x, prf.IEG_dispvalA3_y), h2);
                    tst1.AddEntity(tx57);
                    Text tx58 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalA3_x, prf.HRC_dispvalA3_y), h2);
                    tst1.AddEntity(tx58);

                    Text tx59 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalA3_x, prf.CT_dispvalA3_y), h2);
                    tst1.AddEntity(tx59);
                    Text tx60 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalA3_x, prf.CSP_dispvalA3_y), h2);
                    tst1.AddEntity(tx60);
                    Text tx61 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalA3_x, prf.UCT_dispvalA3_y), h2);
                    tst1.AddEntity(tx61);
                    Text tx62 = new Text(prf.ECT_dispval, new Vector2(prf.ECT_dispvalA3_x, prf.ECT_dispvalA3_y), h2);
                    tst1.AddEntity(tx62);
                    Text tx63 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalA3_x, prf.OCT_dispvalA3_y), h2);
                    tst1.AddEntity(tx63);
                    Text tx64 = new Text(prf.FPL_dispval, new Vector2(prf.FPL_dispvalA3_x, prf.FPL_dispvalA3_y), h2);
                    tst1.AddEntity(tx64);

                    Text tx65 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalA3_x, prf.FSZ_dispvalA3_y), h2);
                    tst1.AddEntity(tx65);
                    Text tx66 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalA3_x, prf.PTN_dispvalA3_y), h2);
                    tst1.AddEntity(tx66);
                    Text tx67 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalA3_x, prf.FNM_dispvalA3_y), h2);
                    tst1.AddEntity(tx67);
                    Text tx68 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalA3_x, prf.USL_dispvalA3_y), h2);
                    tst1.AddEntity(tx68);
                    Text tx69 = new Text(prf.UEL_dispval, new Vector2(prf.UEL_dispvalA3_x, prf.UEL_dispvalA3_y), h2);
                    tst1.AddEntity(tx69);
                    Text tx70 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalA3_x, prf.RMW1_dispvalA3_y), h2);
                    tst1.AddEntity(tx70);
                    Text tx71 = new Text(prf.RMT_dispval, new Vector2(prf.RMT1_dispvalA3_x, prf.RMT1_dispvalA3_y), h2);
                    tst1.AddEntity(tx71);
                    break;
            }





            //}
        }

        private void Halfroundprofile(Profile prf, DimensionStyle myStyle, DxfDocument tst1)
        {

            // Calculate coordinates of all vertices     
            prf.VST_x = 25;
            prf.VST_y = 120;
            prf.V1_x = prf.VST_x;
            prf.V1_y = prf.VST_y + prf.TWT_dimval / 2;
            prf.V2_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V2_y = prf.VST_y + prf.TWS_dimval / 2;
            prf.V3_x = prf.VST_x + prf.TL_dimval;
            prf.V3_y = prf.VST_y + prf.W_dimval / 2;
            prf.V4_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V4_y = prf.VST_y + prf.W_dimval / 2;
            prf.V5_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V5_y = prf.VST_y + prf.FPL_dimval / 2;
            prf.V6_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V6_y = prf.VST_y - prf.FPL_dimval / 2;
            prf.V7_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V7_y = prf.VST_y - prf.W_dimval / 2;
            prf.V8_x = prf.VST_x + prf.TL_dimval;
            prf.V8_y = prf.VST_y - prf.W_dimval / 2;

            prf.V9_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V9_y = prf.VST_y - prf.TWS_dimval / 2;

            prf.V10_x = prf.VST_x;
            prf.V10_y = prf.VST_y - prf.TWT_dimval / 2;
            prf.VC1_x = prf.VST_x - 10;
            prf.VC1_y = prf.VST_y;
            prf.VC2_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 40;
            prf.VC2_y = prf.VST_y;
            prf.V11_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + +30;
            prf.V11_y = prf.VST_y + prf.W_dimval / 2;
            prf.Vuc1_x = prf.V8_x + prf.USL_dimval;
            prf.Vuc1_y = prf.V8_y;
            prf.Vuc2_x = prf.V8_x + prf.USL_dimval - (prf.USL_dimval / 2);
            prf.Vuc2_y = prf.V8_y + prf.W_dimval;
            prf.Vuc3_x = prf.V8_x + prf.USL_dimval + (prf.USL_dimval / 2);
            prf.Vuc3_y = prf.V8_y + prf.W_dimval;
            prf.V12_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 30;
            prf.V12_y = prf.VST_y;
            prf.V13_x = prf.V11_x;
            prf.V13_y = prf.V11_y - prf.W_dimval;
            prf.V14_x = prf.V11_x + prf.T_dimval;
            prf.V14_y = prf.V11_y - prf.W_dimval;
            prf.Vh1_x = prf.Vuc3_x + 35;
            prf.Vh1_y = prf.Vuc3_y;
            prf.Vh2_x = prf.Vuc1_x + 35;
            prf.Vh2_y = prf.Vuc1_y;
            prf.V16_x = prf.V12_x + prf.T_dimval;
            prf.V16_y = prf.VST_y;

            //Draw lines joining above 10 vertices for basic profile
            Line l1 = new Line(new Vector2(prf.VST_x, prf.VST_y), new Vector2(prf.V1_x, prf.V1_y));
            tst1.AddEntity(l1);
            Line l2 = new Line(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V2_x, prf.V2_y));
            tst1.AddEntity(l2);
            Line l3 = new Line(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V3_x, prf.V3_y));
            tst1.AddEntity(l3);
            Line l4 = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V4_x, prf.V4_y));
            tst1.AddEntity(l4);
            Line l5 = new Line(new Vector2(prf.V4_x, prf.V4_y), new Vector2(prf.V5_x, prf.V5_y));
            tst1.AddEntity(l5);
            Line l6 = new Line(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y));
            tst1.AddEntity(l6);
            Line l7 = new Line(new Vector2(prf.V6_x, prf.V6_y), new Vector2(prf.V7_x, prf.V7_y));
            tst1.AddEntity(l7);
            Line l8 = new Line(new Vector2(prf.V7_x, prf.V7_y), new Vector2(prf.V8_x, prf.V8_y));
            tst1.AddEntity(l8);
            Line l9 = new Line(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.V9_x, prf.V9_y));
            tst1.AddEntity(l9);
            Line l10 = new Line(new Vector2(prf.V9_x, prf.V9_y), new Vector2(prf.V10_x, prf.V10_y));
            tst1.AddEntity(l10);
            Line l11 = new Line(new Vector2(prf.V10_x, prf.V10_y), new Vector2(prf.VST_x, prf.VST_y));
            tst1.AddEntity(l11);

            Line l13 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc2_x, prf.Vuc2_y));
            tst1.AddEntity(l13);
            Line l14 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc3_x, prf.Vuc3_y));
            tst1.AddEntity(l14);


            Layer layer2 = new Layer("Layer2");
            layer2.Color = AciColor.Green;

            // draw side view
            Line l15 = new Line(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V13_x, prf.V13_y));
            tst1.AddEntity(l15);
            if (prf.FSZ_dispval == "4.0")
            {
                Vector2 center = new Vector2(prf.V12_x - 2.87, prf.V12_y);
                Vector2 start = new Vector2(prf.V13_x, prf.V13_y);
                Vector2 end = new Vector2(prf.V11_x, prf.V11_y);
                Arc arc = new Arc(center, 6.47, -63.65, 63.65);
                tst1.AddEntity(arc);
            }
            else
            {
                Vector2 center = new Vector2(prf.V12_x - 5.02, prf.V12_y);
                Vector2 start = new Vector2(prf.V13_x, prf.V13_y);
                Vector2 end = new Vector2(prf.V11_x, prf.V11_y);
                Arc arc1 = new Arc(center, 9.32, -57.43, 57.43);
                tst1.AddEntity(arc1);
            }
            // draw centre line
            layer2.Linetype = Linetype.Center;
            Line l12 = new Line(new Vector2(prf.VC1_x, prf.VC1_y), new Vector2(prf.VC2_x, prf.VC2_y));
            tst1.AddEntity(l12);
            l12.Layer = layer2;

            //draw hatching for overcut
            LwPolyline poly1 = new LwPolyline();

            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            //poly1.Layer = layer1;

            tst1.AddEntity(poly1);
            HatchBoundaryPath boundary1 = new HatchBoundaryPath(new List<EntityObject> { poly1 });
            HatchPattern pattern1 = HatchPattern.Line;
            pattern1.Scale = 10;
            pattern1.Angle = 115;
            Hatch hatch1 = new Hatch(pattern1, true);
            hatch1.BoundaryPaths.Add(boundary1);
            tst1.AddEntity(hatch1);
            //draw hatching for upcut
            LwPolyline poly = new LwPolyline();
            Layer layer1 = new Layer("Layer1");


            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc3_x, prf.Vuc3_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));

            poly.Layer = layer1;
            tst1.AddEntity(poly);
            HatchBoundaryPath boundary = new HatchBoundaryPath(new List<EntityObject> { poly });
            HatchPattern pattern = HatchPattern.Line;
            pattern.Scale = 3;
            pattern.Angle = 65;
            Hatch hatch = new Hatch(pattern, true);
            hatch.BoundaryPaths.Add(boundary);
            tst1.AddEntity(hatch);






            // draw all dimensions
            AlignedDimension t1 = new AlignedDimension(new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), new Vector2(prf.VST_x, prf.VST_y), 2.5, myStyle);
            // draw dimension A
            AlignedDimension aln1 = new AlignedDimension(new Vector2(prf.V1_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y), 4.5, myStyle);
            tst1.AddEntity(aln1);
            // draw dimension B
            AlignedDimension aln2 = new AlignedDimension(new Vector2(prf.V3_x, prf.V5_y), new Vector2(prf.V5_x, prf.V5_y), 6, myStyle);
            tst1.AddEntity(aln2);
            // draw dimension F
            AlignedDimension aln3 = new AlignedDimension(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y), -6, myStyle);
            tst1.AddEntity(aln3);
            // draw dimension E
            AlignedDimension aln4 = new AlignedDimension(new Vector2(prf.V6_x, prf.V7_y), new Vector2(prf.V7_x, prf.V7_y), -6, myStyle);
            tst1.AddEntity(aln4);
            // draw dimension D
            AlignedDimension aln5 = new AlignedDimension(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V10_x, prf.V10_y), 6, myStyle);
            tst1.AddEntity(aln5);

            //// side view dimension
            //// W dimension`
            AlignedDimension aln6 = new AlignedDimension(new Vector2(prf.V12_x, prf.V12_y), new Vector2(prf.V16_x, prf.V16_y), 9, myStyle);
            tst1.AddEntity(aln6);
            // T dimension
            AlignedDimension aln7 = new AlignedDimension(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V13_x, prf.V13_y), 6, myStyle);
            tst1.AddEntity(aln7);

            // angle dimension
            Line tmpln = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y + 2));
            tst1.AddEntity(tmpln);

            // Angular2LineDimension dim1 = new Angular2LineDimension(tmpln, l3, 3, myStyle);
            // tst1.AddEntity(dim1);

            //uncut dimension
            // draw dimension D
            AlignedDimension aln8 = new AlignedDimension(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.Vuc1_x, prf.Vuc1_y), -6, myStyle);
            tst1.AddEntity(aln8);
            Text tx4 = new Text("UNCUT", new Vector2(prf.V8_x + 2, prf.V8_y - 10), 2.5);
            tst1.AddEntity(tx4);
            AlignedDimension aln9 = new AlignedDimension(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V9_x, prf.V9_y), 4, myStyle);
            tst1.AddEntity(aln9);

            //leader text OVERCUT and UPCUT
            TextStyle txtst = new TextStyle("Tahoma.ttf");
            Leader leader1 = new Leader("OVERCUT", new List<Vector2> { new Vector2(prf.V4_x - 12, prf.V4_y - 3), new Vector2(prf.V4_x + 10, prf.V4_y + 10) }, myStyle);
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader1);
            Leader leader2 = new Leader("UPCUT", new List<Vector2> { new Vector2(prf.V3_x + 12, prf.V3_y - 3), new Vector2(prf.V3_x + 15, prf.V3_y + 10) }, myStyle);
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader2);
            //--------------------- text---------------------------
            Double h1 = 1.8;
            Text tx72 = new Text(prf.RMW_dispval, new Vector2(prf.RMW_dispvalloc_HR_x, prf.RMW_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx72);
            Text tx45 = new Text(prf.RMT_dispval, new Vector2(prf.RMT_dispvalloc_HR_x, prf.RMT_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx45);
            Text tx73 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalloc_HR_x, prf.CML_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx73);
            Text tx74 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalloc_HR_x, prf.BL_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx74);
            Text tx75 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalloc_HR_x, prf.TL_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx75);
            Text tx76 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_HR_x, prf.TWS_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx76);
            Text tx77 = new Text(prf.TTS_dispval, new Vector2(prf.TTS_dispvalloc_HR_x, prf.TTS_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx77);
            Text tx11 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalloc_HR_x, prf.TWT_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx11);
            Text tx13 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dimval_HR_x, prf.TTT_dimval_HR_y), h1);
            tst1.AddEntity(tx13);
            Text tx78 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_HR_x, prf.TPL_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx78);
            Text tx79 = new Text(prf.FPL_dispval, new Vector2(prf.FPL_dispvalloc_HR_x, prf.FPL_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx79);
            Text tx80 = new Text(prf.W_dispval, new Vector2(prf.W_dispvalloc_HR_x, prf.W_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx80);
            Text tx1 = new Text(prf.T_dispval, new Vector2(prf.T_dispvalloc_HR_x, prf.T_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx1);
            Text tx81 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalloc_HR_x, prf.GPL_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx81);
            Text tx82 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_HR_x, prf.TPL_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx82);
            Text tx83 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalloc_HR_x, prf.L_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx83);
            Text tx84 = new Text(prf.IUPF_dispval_HR, new Vector2(prf.IUPF_dispvalloc_HR_x, prf.IUPF_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx84);
            Text tx2 = new Text(prf.IUPR_dispval_HR, new Vector2(prf.IUPR_dispvalloc_HR_x, prf.IUPR_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx2);
            Text tx3 = new Text(prf.IOVF_dispval_HR, new Vector2(prf.IOVF_dispvalloc_HR_x, prf.IOVF_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx3);
            Text tx5 = new Text(prf.IOVR_dispval_HR, new Vector2(prf.IOVR_dispvalloc_HR_x, prf.IOVR_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx5);
            Text tx86 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalloc_HR_x, prf.HRC_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx86);

            Text tx87 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispval_HR_x, prf.CT_dispval_HR_y), h1);
            tst1.AddEntity(tx87);
            Text tx88 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispval_HR_x, prf.CSP_dispval_HR_y), h1);
            tst1.AddEntity(tx88);

            Text tx91 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalloc_x, prf.FSZ_dispvalloc_y), h1);
            tst1.AddEntity(tx91);
            Text tx92 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalloc_x, prf.PTN_dispvalloc_y), h1);
            tst1.AddEntity(tx92);
            Text tx93 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalloc_x, prf.FNM_dispvalloc_y), h1);
            tst1.AddEntity(tx93);
            Text tx94 = new Text(prf.USLF_dispval_HR, new Vector2(prf.USLF_dispvalloc_HR_x, prf.USLF_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx94);
            Text tx7 = new Text(prf.USLR_dispval_HR, new Vector2(prf.USLR_dispvalloc_HR_x, prf.USLR_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx7);
            Text tx37 = new Text(prf.RMW_dispval, new Vector2(prf.RMT1_dispvalloc_HR_x, prf.RMT1_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx37);
            Text tx38 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalloc_HR_x, prf.RMW1_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx38);
            Text tx39 = new Text(prf.UP1GN_dispval_HR, new Vector2(prf.UP1GNF_dispvalloc_HR_x, prf.UP1GNF_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx39);
            Text tx40 = new Text(prf.UP1GN_dispval_HR, new Vector2(prf.UP1GNR_dispvalloc_HR_x, prf.UP1GNR_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx40);
            Text tx41 = new Text(prf.OV1GN_dispval_HR, new Vector2(prf.OV1GNF_dispvalloc_HR_x, prf.OV1GNF_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx41);
            Text tx42 = new Text(prf.OV1GN_dispval_HR, new Vector2(prf.OV1GNR_dispvalloc_HR_x, prf.OV1GNR_dispvalloc_HR_y), h1);
            tst1.AddEntity(tx42);

        }

        private void squarefileprofile(Profile prf, DimensionStyle myStyle, DxfDocument tst1)
        {

            prf.VST_x = 25;
            prf.VST_y = 120;
            prf.V1_x = prf.VST_x;
            prf.V1_y = prf.VST_y + prf.TWT_dimval / 2;
            prf.V2_x = prf.VST_x + prf.TL_dimval - (prf.D_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V2_y = prf.VST_y + prf.TWS_dimval / 2;
            prf.V3_x = prf.VST_x + prf.TL_dimval;
            prf.V3_y = prf.VST_y + prf.D_dimval / 2;
            prf.V4_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V4_y = prf.VST_y + prf.D_dimval / 2;
            prf.V5_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V5_y = prf.VST_y + prf.GPL_dimval / 2;
            prf.V6_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V6_y = prf.VST_y - prf.GPL_dimval / 2;
            prf.V7_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V7_y = prf.VST_y - prf.D_dimval / 2;
            prf.V8_x = prf.VST_x + prf.TL_dimval;
            prf.V8_y = prf.VST_y - prf.D_dimval / 2;

            prf.V9_x = prf.VST_x + prf.TL_dimval - (prf.D_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V9_y = prf.VST_y - prf.TWS_dimval / 2;

            prf.V10_x = prf.VST_x;
            prf.V10_y = prf.VST_y - prf.TWT_dimval / 2;
            prf.VC1_x = prf.VST_x - 10;
            prf.VC1_y = prf.VST_y;
            prf.VC2_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 80;
            prf.VC2_y = prf.VST_y;
            prf.V11_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 60;
            prf.V11_y = prf.VST_y + prf.D_dimval / 2;
            prf.Vuc1_x = prf.V8_x + prf.USL_dimval;
            prf.Vuc1_y = prf.V8_y;
            prf.Vuc2_x = prf.V8_x + prf.USL_dimval - (prf.USL_dimval / 2);
            prf.Vuc2_y = prf.V8_y + prf.D_dimval;
            prf.Vuc3_x = prf.V8_x + prf.USL_dimval + (prf.USL_dimval / 2);
            prf.Vuc3_y = prf.V8_y + prf.D_dimval;
            prf.V12_x = prf.V11_x + prf.RMD_dimval;
            prf.V12_y = prf.V11_y;
            prf.V13_x = prf.V11_x;
            prf.V13_y = prf.V11_y - prf.RMD_dimval;
            prf.V14_x = prf.V11_x + prf.RMD_dimval;
            prf.V14_y = prf.V11_y - prf.RMD_dimval;
            prf.Vh1_x = prf.Vuc3_x + 35;
            prf.Vh1_y = prf.Vuc3_y;
            prf.Vh2_x = prf.Vuc1_x + 35;
            prf.Vh2_y = prf.Vuc1_y;


            //Draw lines joining above 10 vertices for basic profile
            Line l1 = new Line(new Vector2(prf.VST_x, prf.VST_y), new Vector2(prf.V1_x, prf.V1_y));
            tst1.AddEntity(l1);
            Line l2 = new Line(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V2_x, prf.V2_y));
            tst1.AddEntity(l2);
            Line l3 = new Line(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V3_x, prf.V3_y));
            tst1.AddEntity(l3);
            Line l4 = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V4_x, prf.V4_y));
            tst1.AddEntity(l4);
            Line l5 = new Line(new Vector2(prf.V4_x, prf.V4_y), new Vector2(prf.V5_x, prf.V5_y));
            tst1.AddEntity(l5);
            Line l6 = new Line(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y));
            tst1.AddEntity(l6);
            Line l7 = new Line(new Vector2(prf.V6_x, prf.V6_y), new Vector2(prf.V7_x, prf.V7_y));
            tst1.AddEntity(l7);
            Line l8 = new Line(new Vector2(prf.V7_x, prf.V7_y), new Vector2(prf.V8_x, prf.V8_y));
            tst1.AddEntity(l8);
            Line l9 = new Line(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.V9_x, prf.V9_y));
            tst1.AddEntity(l9);
            Line l10 = new Line(new Vector2(prf.V9_x, prf.V9_y), new Vector2(prf.V10_x, prf.V10_y));
            tst1.AddEntity(l10);
            Line l11 = new Line(new Vector2(prf.V10_x, prf.V10_y), new Vector2(prf.VST_x, prf.VST_y));
            tst1.AddEntity(l11);

            Line l13 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc2_x, prf.Vuc2_y));
            tst1.AddEntity(l13);
            Line l14 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc3_x, prf.Vuc3_y));
            tst1.AddEntity(l14);


            Layer layer2 = new Layer("Layer2");
            layer2.Color = AciColor.Green;

            // draw side view

            LwPolyline poly3 = new LwPolyline();



            poly3.Vertexes.Add(new LwPolylineVertex(prf.V11_x, prf.V11_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V12_x, prf.V12_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V14_x, prf.V14_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V13_x, prf.V13_y));
            poly3.Vertexes.Add(new LwPolylineVertex(prf.V11_x, prf.V11_y));
            tst1.AddEntity(poly3);

            // draw centre line
            layer2.Linetype = Linetype.Center;
            Line l12 = new Line(new Vector2(prf.VC1_x, prf.VC1_y), new Vector2(prf.VC2_x, prf.VC2_y));
            tst1.AddEntity(l12);
            l12.Layer = layer2;

            //draw hatching for overcut
            LwPolyline poly1 = new LwPolyline();

            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            //poly1.Layer = layer1;

            tst1.AddEntity(poly1);
            HatchBoundaryPath boundary1 = new HatchBoundaryPath(new List<EntityObject> { poly1 });
            HatchPattern pattern1 = HatchPattern.Line;
            pattern1.Scale = 10;
            pattern1.Angle = 115;
            Hatch hatch1 = new Hatch(pattern1, true);
            hatch1.BoundaryPaths.Add(boundary1);
            tst1.AddEntity(hatch1);
            //draw hatching for upcut
            LwPolyline poly = new LwPolyline();
            Layer layer1 = new Layer("Layer1");


            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc3_x, prf.Vuc3_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));

            poly.Layer = layer1;
            tst1.AddEntity(poly);
            HatchBoundaryPath boundary = new HatchBoundaryPath(new List<EntityObject> { poly });
            HatchPattern pattern = HatchPattern.Line;
            pattern.Scale = 3;
            pattern.Angle = 65;
            Hatch hatch = new Hatch(pattern, true);
            hatch.BoundaryPaths.Add(boundary);
            tst1.AddEntity(hatch);






            // draw all dimensions
            AlignedDimension t1 = new AlignedDimension(new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), new Vector2(prf.VST_x, prf.VST_y), 2.5, myStyle);
            // draw dimension A
            AlignedDimension aln1 = new AlignedDimension(new Vector2(prf.V1_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y), 4.5, myStyle);
            tst1.AddEntity(aln1);
            // draw dimension B
            AlignedDimension aln2 = new AlignedDimension(new Vector2(prf.V3_x, prf.V5_y), new Vector2(prf.V5_x, prf.V5_y), 6, myStyle);
            tst1.AddEntity(aln2);
            // draw dimension F
            AlignedDimension aln3 = new AlignedDimension(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y), -6, myStyle);
            tst1.AddEntity(aln3);
            // draw dimension E
            AlignedDimension aln4 = new AlignedDimension(new Vector2(prf.V6_x, prf.V7_y), new Vector2(prf.V7_x, prf.V7_y), -6, myStyle);
            tst1.AddEntity(aln4);
            // draw dimension D
            AlignedDimension aln5 = new AlignedDimension(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V10_x, prf.V10_y), 6, myStyle);
            tst1.AddEntity(aln5);

            // side view dimension
            // W dimension
            AlignedDimension aln6 = new AlignedDimension(new Vector2(prf.V12_x, prf.V12_y), new Vector2(prf.V14_x, prf.V14_y), -6, myStyle);
            tst1.AddEntity(aln6);
            // T dimension
            AlignedDimension aln7 = new AlignedDimension(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V12_x, prf.V12_y), 6, myStyle);
            tst1.AddEntity(aln7);

            // angle dimension
            Line tmpln = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y + 2));
            tst1.AddEntity(tmpln);

            // Angular2LineDimension dim1 = new Angular2LineDimension(tmpln, l3, 3, myStyle);
            // tst1.AddEntity(dim1);

            //uncut dimension
            // draw dimension D
            AlignedDimension aln8 = new AlignedDimension(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.Vuc1_x, prf.Vuc1_y), -6, myStyle);
            tst1.AddEntity(aln8);
            Text tx4 = new Text("UNCUT", new Vector2(prf.V8_x + 2, prf.V8_y - 10), 2.5);
            tst1.AddEntity(tx4);
            AlignedDimension aln9 = new AlignedDimension(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V9_x, prf.V9_y), 4, myStyle);
            tst1.AddEntity(aln9);

            //leader text OVERCUT and UPCUT
            TextStyle txtst = new TextStyle("Tahoma.ttf");
            Leader leader1 = new Leader("OVERCUT", new List<Vector2> { new Vector2(prf.V4_x - 12, prf.V4_y - 3), new Vector2(prf.V4_x + 10, prf.V4_y + 10) }, myStyle);
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader1);
            Leader leader2 = new Leader("UPCUT", new List<Vector2> { new Vector2(prf.V3_x + 12, prf.V3_y - 3), new Vector2(prf.V3_x + 15, prf.V3_y + 10) }, myStyle);
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader2);
            //--------------------- text---------------------------
            Double h1 = 1.8;

            // FOR SQUARE------------------------
            Text tx72 = new Text(prf.RMW_dispval, new Vector2(prf.RMD_dispvalloc_SQ_x, prf.RMD_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx72);
            Text tx73 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalloc_SQ__x, prf.CML_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx73);
            Text tx74 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalloc_SQ_x, prf.BL_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx74);
            Text tx75 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalloc_SQ_x, prf.TL_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx75);
            Text tx76 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_SQ_x, prf.TWS_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx76);
            Text tx77 = new Text(prf.TTS_dispval, new Vector2(prf.TTS_dispvalloc_SQ_x, prf.TTS_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx77);
            Text tx11 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvallo_SQ_x, prf.TWT_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx11);
            Text tx13 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dimval_SQ_x, prf.TTT_dimval_SQ_y), h1);
            tst1.AddEntity(tx13);
            Text tx78 = new Text(prf.ATN_dispval_SQ, new Vector2(prf.TAD_dispvalloc_SQ_x, prf.TAD_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx78);
            Text tx79 = new Text(prf.RSH_dispval, new Vector2(prf.TRM_dispvalloc_SQ_x, prf.TRM_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx79);
            Text tx80 = new Text(prf.D_dispval, new Vector2(prf.D_dispvalloc_SQ_x, prf.D_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx80);
            Text tx81 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalloc_SQ_x, prf.GPL_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx81);
            Text tx82 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_SQ_x, prf.TPL_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx82);
            Text tx83 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalloc_SQ_x, prf.L_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx83);
            Text tx84 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalloc_SQ_x, prf.IUP_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx84);
            Text tx85 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_SQ_x, prf.IOV_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx85);
            Text tx86 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalloc_SQ_x, prf.HRC_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx86);

            Text tx87 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalloc_x, prf.CT_dispvalloc_y), h1);
            tst1.AddEntity(tx87);
            Text tx88 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalloc_x, prf.CSP_dispvalloc_y), h1);
            tst1.AddEntity(tx88);
            Text tx89 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalloc_SQ_x, prf.UCT_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx89);
            Text tx90 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalloc_SQ_x, prf.OCT_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx90);

            Text tx91 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalloc_x, prf.FSZ_dispvalloc_y), h1);
            tst1.AddEntity(tx91);
            Text tx92 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalloc_x, prf.PTN_dispvalloc_y), h1);
            tst1.AddEntity(tx92);
            Text tx93 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalloc_x, prf.FNM_dispvalloc_y), h1);
            tst1.AddEntity(tx93);
            Text tx94 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalloc_SQ_x, prf.USL_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx94);
            Text tx37 = new Text(prf.RMW_dispval, new Vector2(prf.RMT1_dispvalloc_SQ_x, prf.RMT1_dispvalloc_SQ_y), h1);
            tst1.AddEntity(tx37);
            Text tx100 = new Text(prf.note1_dispval, new Vector2(prf.nt1_x_SQ, prf.nt1_y_SQ), h1);
            tst1.AddEntity(tx100);
            Text tx101 = new Text(prf.note2_dispval, new Vector2(prf.nt2_x_SQ, prf.nt2_y_SQ), h1);
            tst1.AddEntity(tx101);
            Text tx102 = new Text(prf.note3_dispval, new Vector2(prf.nt3_x_SQ, prf.nt3_y_SQ), h1);
            tst1.AddEntity(tx102);

        }

        private void Roundfileprofile(Profile prf, DimensionStyle myStyle, DxfDocument tst1)
        {

            prf.VST_x = 25;
            prf.VST_y = 120;
            prf.V1_x = prf.VST_x;
            prf.V1_y = prf.VST_y + prf.TWT_dimval / 2;
            prf.V2_x = prf.VST_x + prf.TL_dimval - (prf.RMD_dimval_R / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V2_y = prf.VST_y + prf.TWS_dimval / 2;
            prf.V3_x = prf.VST_x + prf.TL_dimval;
            prf.V3_y = prf.VST_y + prf.RMD_dimval_R / 2;
            prf.V4_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V4_y = prf.VST_y + prf.RMD_dimval_R / 2;
            prf.V5_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V5_y = prf.VST_y + prf.GPL_dimval / 2;
            prf.V6_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V6_y = prf.VST_y - prf.GPL_dimval / 2;
            prf.V7_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V7_y = prf.VST_y - prf.RMD_dimval_R / 2;
            prf.V8_x = prf.VST_x + prf.TL_dimval;
            prf.V8_y = prf.VST_y - prf.RMD_dimval_R / 2;

            prf.V9_x = prf.VST_x + prf.TL_dimval - (prf.RMD_dimval_R / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V9_y = prf.VST_y - prf.TWS_dimval / 2;

            prf.V10_x = prf.VST_x;
            prf.V10_y = prf.VST_y - prf.TWT_dimval / 2;
            prf.VC1_x = prf.VST_x - 10;
            prf.VC1_y = prf.VST_y;
            prf.VC2_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 80;
            prf.VC2_y = prf.VST_y;
            prf.V11_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 60;
            prf.V11_y = prf.VST_y + prf.RMD_dimval_R / 2;
            prf.Vuc1_x = prf.V8_x + prf.USL_dimval;
            prf.Vuc1_y = prf.V8_y;
            prf.Vuc2_x = prf.V8_x + prf.USL_dimval - (prf.USL_dimval / 2);
            prf.Vuc2_y = prf.V8_y + prf.RMD_dimval_R;
            prf.Vuc3_x = prf.V8_x + prf.USL_dimval + (prf.USL_dimval / 2);
            prf.Vuc3_y = prf.V8_y + prf.RMD_dimval_R;
            prf.V12_x = prf.V11_x + prf.RMD_dimval_R;
            prf.V12_y = prf.V11_y;
            prf.V13_x = prf.V11_x;
            prf.V13_y = prf.V11_y - prf.RMD_dimval_R;
            prf.V14_x = prf.V11_x + prf.RMD_dimval_R;
            prf.V14_y = prf.V11_y - prf.RMD_dimval_R;
            prf.Vh1_x = prf.Vuc3_x + 35;
            prf.Vh1_y = prf.Vuc3_y;
            prf.Vh2_x = prf.Vuc1_x + 35;
            prf.Vh2_y = prf.Vuc1_y;


            //Draw lines joining above 10 vertices for basic profile
            Line l1 = new Line(new Vector2(prf.VST_x, prf.VST_y), new Vector2(prf.V1_x, prf.V1_y));
            tst1.AddEntity(l1);
            Line l2 = new Line(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V2_x, prf.V2_y));
            tst1.AddEntity(l2);
            Line l3 = new Line(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V3_x, prf.V3_y));
            tst1.AddEntity(l3);
            Line l4 = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V4_x, prf.V4_y));
            tst1.AddEntity(l4);
            Line l5 = new Line(new Vector2(prf.V4_x, prf.V4_y), new Vector2(prf.V5_x, prf.V5_y));
            tst1.AddEntity(l5);
            Line l6 = new Line(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y));
            tst1.AddEntity(l6);
            Line l7 = new Line(new Vector2(prf.V6_x, prf.V6_y), new Vector2(prf.V7_x, prf.V7_y));
            tst1.AddEntity(l7);
            Line l8 = new Line(new Vector2(prf.V7_x, prf.V7_y), new Vector2(prf.V8_x, prf.V8_y));
            tst1.AddEntity(l8);
            Line l9 = new Line(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.V9_x, prf.V9_y));
            tst1.AddEntity(l9);
            Line l10 = new Line(new Vector2(prf.V9_x, prf.V9_y), new Vector2(prf.V10_x, prf.V10_y));
            tst1.AddEntity(l10);
            Line l11 = new Line(new Vector2(prf.V10_x, prf.V10_y), new Vector2(prf.VST_x, prf.VST_y));
            tst1.AddEntity(l11);

            Line l13 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc2_x, prf.Vuc2_y));
            tst1.AddEntity(l13);
            Line l14 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc3_x, prf.Vuc3_y));
            tst1.AddEntity(l14);


            Layer layer2 = new Layer("Layer2");
            layer2.Color = AciColor.Green;

            // draw side view

            Circle c1 = new Circle(new Vector2(prf.VC2_x - 40, prf.VC1_y), prf.RMD_dimval_R / 2);
            tst1.AddEntity(c1);
            // draw centre line
            layer2.Linetype = Linetype.Center;
            Line l12 = new Line(new Vector2(prf.VC1_x, prf.VC1_y), new Vector2(prf.VC2_x, prf.VC2_y));
            tst1.AddEntity(l12);
            l12.Layer = layer2;

            //draw hatching for overcut
            LwPolyline poly1 = new LwPolyline();

            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            //poly1.Layer = layer1;

            tst1.AddEntity(poly1);
            HatchBoundaryPath boundary1 = new HatchBoundaryPath(new List<EntityObject> { poly1 });
            HatchPattern pattern1 = HatchPattern.Line;
            pattern1.Scale = 10;
            pattern1.Angle = 115;
            Hatch hatch1 = new Hatch(pattern1, true);
            hatch1.BoundaryPaths.Add(boundary1);
            tst1.AddEntity(hatch1);
            //draw hatching for upcut
            LwPolyline poly = new LwPolyline();
            Layer layer1 = new Layer("Layer1");


            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc3_x, prf.Vuc3_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));

            poly.Layer = layer1;
            tst1.AddEntity(poly);
            HatchBoundaryPath boundary = new HatchBoundaryPath(new List<EntityObject> { poly });
            HatchPattern pattern = HatchPattern.Line;
            pattern.Scale = 3;
            pattern.Angle = 65;
            Hatch hatch = new Hatch(pattern, true);
            hatch.BoundaryPaths.Add(boundary);
            tst1.AddEntity(hatch);






            // draw all dimensions
            AlignedDimension t1 = new AlignedDimension(new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), new Vector2(prf.VST_x, prf.VST_y), 2.5, myStyle);
            // draw dimension A
            AlignedDimension aln1 = new AlignedDimension(new Vector2(prf.V1_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y), 4.5, myStyle);
            tst1.AddEntity(aln1);
            // draw dimension B
            AlignedDimension aln2 = new AlignedDimension(new Vector2(prf.V3_x, prf.V5_y), new Vector2(prf.V5_x, prf.V5_y), 6, myStyle);
            tst1.AddEntity(aln2);
            // draw dimension F
            AlignedDimension aln3 = new AlignedDimension(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y), -6, myStyle);
            tst1.AddEntity(aln3);
            // draw dimension E
            AlignedDimension aln4 = new AlignedDimension(new Vector2(prf.V6_x, prf.V7_y), new Vector2(prf.V7_x, prf.V7_y), -6, myStyle);
            tst1.AddEntity(aln4);
            // draw dimension D
            AlignedDimension aln5 = new AlignedDimension(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V10_x, prf.V10_y), 6, myStyle);
            tst1.AddEntity(aln5);

            // side view dimension
            // D dimension
            AlignedDimension aln6 = new AlignedDimension(new Vector2(prf.VC2_x - 40 - prf.RMD_dimval_R / 2, prf.VC1_y), new Vector2(prf.VC2_x - 40 + prf.RMD_dimval_R / 2, prf.VC1_y), -8, myStyle);
            tst1.AddEntity(aln6);


            // angle dimension
            Line tmpln = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y + 2));
            tst1.AddEntity(tmpln);

            // Angular2LineDimension dim1 = new Angular2LineDimension(tmpln, l3, 3, myStyle);
            // tst1.AddEntity(dim1);

            //uncut dimension
            // draw dimension D
            AlignedDimension aln8 = new AlignedDimension(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.Vuc1_x, prf.Vuc1_y), -6, myStyle);
            tst1.AddEntity(aln8);
            Text tx4 = new Text("UNCUT", new Vector2(prf.V8_x + 2, prf.V8_y - 10), 2.5);
            tst1.AddEntity(tx4);
            AlignedDimension aln9 = new AlignedDimension(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V9_x, prf.V9_y), 4, myStyle);
            tst1.AddEntity(aln9);

            //leader text OVERCUT and UPCUT
            TextStyle txtst = new TextStyle("Tahoma.ttf");
            Leader leader1 = new Leader("OVERCUT", new List<Vector2> { new Vector2(prf.V4_x - 12, prf.V4_y - 3), new Vector2(prf.V4_x + 10, prf.V4_y + 10) }, myStyle);
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader1);
            Leader leader2 = new Leader("UPCUT", new List<Vector2> { new Vector2(prf.V3_x + 12, prf.V3_y - 3), new Vector2(prf.V3_x + 15, prf.V3_y + 10) }, myStyle);
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader2);
            //--------------------- text---------------------------
            Double h1 = 1.8;

            // FOR SQUARE------------------------
            Text tx72 = new Text(prf.RMD_dispval_R, new Vector2(prf.RMD_dispvalloc_x_R, prf.RMD_dispvalloc_y_R), h1);
            tst1.AddEntity(tx72);
            Text tx73 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalloc_x_R, prf.CML_dispvalloc_y_R), h1);
            tst1.AddEntity(tx73);
            Text tx74 = new Text(prf.BL_dispval, new Vector2(prf.BL_dispvalloc_x_R, prf.BL_dispvalloc_y_R), h1);
            tst1.AddEntity(tx74);
            Text tx75 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalloc_x_R, prf.TL_dispvalloc_y_R), h1);
            tst1.AddEntity(tx75);
            Text tx76 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_x_R, prf.TWS_dispvalloc_y_R), h1);
            tst1.AddEntity(tx76);
            Text tx77 = new Text(prf.TTS_dispval, new Vector2(prf.TTS_dispvalloc_x_R, prf.TTS_dispvalloc_y_R), h1);
            tst1.AddEntity(tx77);
            Text tx11 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalloc_x_R, prf.TWT_dispvalloc_y_R), h1);
            tst1.AddEntity(tx11);
            Text tx13 = new Text(prf.TTT_dispval, new Vector2(prf.TTT_dimval_x_R, prf.TTT_dimval_y_R), h1);
            tst1.AddEntity(tx13);
            Text tx78 = new Text(prf.ATN_dispval_R, new Vector2(prf.ATN_dispvalloc_x_R, prf.ATN_dispvalloc_y_R), h1);
            tst1.AddEntity(tx78);
            Text tx79 = new Text(prf.RSH_dispval_R, new Vector2(prf.RSH_dispvalloc_x_R, prf.RSH_dispvalloc_y_R), h1);
            tst1.AddEntity(tx79);
            Text tx80 = new Text(prf.D_dispval_R, new Vector2(prf.D_dispvalloc_x_R, prf.D_dispvalloc_y_R), h1);
            tst1.AddEntity(tx80);
            Text tx81 = new Text(prf.GPL_dispval, new Vector2(prf.GPL_dispvalloc_x_R, prf.GPL_dispvalloc_y_R), h1);
            tst1.AddEntity(tx81);
            Text tx82 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_x_R, prf.TPL_dispvalloc_y_R), h1);
            tst1.AddEntity(tx82);
            Text tx83 = new Text(prf.L_dispval, new Vector2(prf.L_dispvalloc_x_R, prf.L_dispvalloc_y_R), h1);
            tst1.AddEntity(tx83);
            Text tx84 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalloc_x_R, prf.IUP_dispvalloc_y_R), h1);
            tst1.AddEntity(tx84);
            Text tx85 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_x_R, prf.IOV_dispvalloc_y_R), h1);
            tst1.AddEntity(tx85);
            Text tx86 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalloc_x_R, prf.HRC_dispvalloc_y_R), h1);
            tst1.AddEntity(tx86);

            Text tx87 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalloc_x_R, prf.CT_dispvalloc_y_R), h1);
            tst1.AddEntity(tx87);
            Text tx88 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalloc_x_R, prf.CSP_dispvalloc_y_R), h1);
            tst1.AddEntity(tx88);
            Text tx89 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalloc_x_R, prf.UCT_dispvalloc_y_R), h1);
            tst1.AddEntity(tx89);
            Text tx90 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalloc_x_R, prf.OCT_dispvalloc_y_R), h1);
            tst1.AddEntity(tx90);

            Text tx91 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalloc_x_R, prf.FSZ_dispvalloc_y_R), h1);
            tst1.AddEntity(tx91);
            Text tx92 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalloc_x_R, prf.PTN_dispvalloc_y_R), h1);
            tst1.AddEntity(tx92);
            Text tx93 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalloc_x_R, prf.FNM_dispvalloc_y_R), h1);
            tst1.AddEntity(tx93);
            Text tx94 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalloc_x_R, prf.USL_dispvalloc_y_R), h1);
            tst1.AddEntity(tx94);
            Text tx37 = new Text(prf.RMD_dispval_R, new Vector2(prf.RMD1_dispvalloc_x_R, prf.RMD1_dispvalloc_y_R), h1);
            tst1.AddEntity(tx37);
            Text tx1 = new Text(prf.OCR_dispval, new Vector2(prf.OCR_dispvalloc_x_R, prf.OCR_dispvalloc_y_R), h1);
            tst1.AddEntity(tx1);
            Text tx2 = new Text(prf.UCR_dispval, new Vector2(prf.UCR_dispvalloc_x_R, prf.UCR_dispvalloc_y_R), h1);
            tst1.AddEntity(tx2);

            Text tx100 = new Text(prf.note1_dispval, new Vector2(prf.nt1_x_R, prf.nt1_y_R), h1);
            tst1.AddEntity(tx100);
            Text tx101 = new Text(prf.note2_dispval, new Vector2(prf.nt2_x_R, prf.nt2_y_R), h1);
            tst1.AddEntity(tx101);
            Text tx102 = new Text(prf.note3_dispval, new Vector2(prf.nt3_x_R, prf.nt3_y_R), h1);
            tst1.AddEntity(tx102);
        }

        private void Taperprofile(Profile prf, DimensionStyle myStyle, DxfDocument tst1)
        {

            // Calculate coordinates of all vertices     
            prf.VST_x = 25;
            prf.VST_y = 120;
            prf.V1_x = prf.VST_x;
            prf.V1_y = prf.VST_y + prf.TWT_dimval / 2;
            prf.V2_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V2_y = prf.VST_y + prf.TWS_dimval / 2;
            prf.V3_x = prf.VST_x + prf.TL_dimval;
            prf.V3_y = prf.VST_y + prf.W_dimval / 2;
            prf.V4_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V4_y = prf.VST_y + prf.W_dimval / 2;
            prf.V5_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V5_y = prf.VST_y + prf.GPL_dimval / 2;
            prf.V6_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval;
            prf.V6_y = prf.VST_y - prf.GPL_dimval / 2;
            prf.V7_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval - prf.TPL_dimval;
            prf.V7_y = prf.VST_y - prf.W_dimval / 2;
            prf.V8_x = prf.VST_x + prf.TL_dimval;
            prf.V8_y = prf.VST_y - prf.W_dimval / 2;

            prf.V9_x = prf.VST_x + prf.TL_dimval - (prf.W_dimval / 2 - prf.TWS_dimval / 2) / 1.14;
            prf.V9_y = prf.VST_y - prf.TWS_dimval / 2;

            prf.V10_x = prf.VST_x;
            prf.V10_y = prf.VST_y - prf.TWT_dimval / 2;
            prf.VC1_x = prf.VST_x - 10;
            prf.VC1_y = prf.VST_y;
            prf.VC2_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 80;
            prf.VC2_y = prf.VST_y;
            prf.V11_x = prf.VST_x + prf.TL_dimval + prf.BL_dimval + 60;
            prf.V11_y = prf.VST_y + prf.W_dimval / 2;
            prf.Vuc1_x = prf.V8_x + prf.USL_dimval;
            prf.Vuc1_y = prf.V8_y;
            prf.Vuc2_x = prf.V8_x + prf.USL_dimval - (prf.USL_dimval / 2);
            prf.Vuc2_y = prf.V8_y + prf.W_dimval;
            prf.Vuc3_x = prf.V8_x + prf.USL_dimval + (prf.USL_dimval / 2);
            prf.Vuc3_y = prf.V8_y + prf.W_dimval;

            prf.V13_x = prf.V11_x - prf.W_dimval / 2;
            prf.V13_y = prf.V11_y - prf.W_dimval;
            prf.V14_x = prf.V11_x + prf.W_dimval / 2;
            prf.V14_y = prf.V11_y - prf.W_dimval;
            prf.Vh1_x = prf.Vuc3_x + 35;
            prf.Vh1_y = prf.Vuc3_y;
            prf.Vh2_x = prf.Vuc1_x + 35;
            prf.Vh2_y = prf.Vuc1_y;


            //Draw lines joining above 10 vertices for basic profile
            Line l1 = new Line(new Vector2(prf.VST_x, prf.VST_y), new Vector2(prf.V1_x, prf.V1_y));
            tst1.AddEntity(l1);
            Line l2 = new Line(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V2_x, prf.V2_y));
            tst1.AddEntity(l2);
            Line l3 = new Line(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V3_x, prf.V3_y));
            tst1.AddEntity(l3);
            Line l4 = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V4_x, prf.V4_y));
            tst1.AddEntity(l4);
            Line l5 = new Line(new Vector2(prf.V4_x, prf.V4_y), new Vector2(prf.V5_x, prf.V5_y));
            tst1.AddEntity(l5);
            Line l6 = new Line(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y));
            tst1.AddEntity(l6);
            Line l7 = new Line(new Vector2(prf.V6_x, prf.V6_y), new Vector2(prf.V7_x, prf.V7_y));
            tst1.AddEntity(l7);
            Line l8 = new Line(new Vector2(prf.V7_x, prf.V7_y), new Vector2(prf.V8_x, prf.V8_y));
            tst1.AddEntity(l8);
            Line l9 = new Line(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.V9_x, prf.V9_y));
            tst1.AddEntity(l9);
            Line l10 = new Line(new Vector2(prf.V9_x, prf.V9_y), new Vector2(prf.V10_x, prf.V10_y));
            tst1.AddEntity(l10);
            Line l11 = new Line(new Vector2(prf.V10_x, prf.V10_y), new Vector2(prf.VST_x, prf.VST_y));
            tst1.AddEntity(l11);

            Line l13 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc2_x, prf.Vuc2_y));
            tst1.AddEntity(l13);
            Line l14 = new Line(new Vector2(prf.Vuc1_x, prf.Vuc1_y), new Vector2(prf.Vuc3_x, prf.Vuc3_y));
            tst1.AddEntity(l14);


            Layer layer2 = new Layer("Layer2");
            layer2.Color = AciColor.Green;

            // draw side view
            Line l15 = new Line(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V13_x, prf.V13_y));
            tst1.AddEntity(l15);
            Line l16 = new Line(new Vector2(prf.V11_x, prf.V11_y), new Vector2(prf.V14_x, prf.V14_y));
            tst1.AddEntity(l16);
            Line l17 = new Line(new Vector2(prf.V14_x, prf.V14_y), new Vector2(prf.V13_x, prf.V13_y));
            tst1.AddEntity(l17);


            // draw centre line
            layer2.Linetype = Linetype.Center;
            Line l12 = new Line(new Vector2(prf.VC1_x, prf.VC1_y), new Vector2(prf.VC2_x, prf.VC2_y));
            tst1.AddEntity(l12);
            l12.Layer = layer2;

            //draw hatching for overcut
            LwPolyline poly1 = new LwPolyline();

            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));
            poly1.Vertexes.Add(new LwPolylineVertex(prf.Vuc2_x, prf.Vuc2_y));
            //poly1.Layer = layer1;

            tst1.AddEntity(poly1);
            HatchBoundaryPath boundary1 = new HatchBoundaryPath(new List<EntityObject> { poly1 });
            HatchPattern pattern1 = HatchPattern.Line;
            pattern1.Scale = 10;
            pattern1.Angle = 115;
            Hatch hatch1 = new Hatch(pattern1, true);
            hatch1.BoundaryPaths.Add(boundary1);
            tst1.AddEntity(hatch1);
            //draw hatching for upcut
            LwPolyline poly = new LwPolyline();
            Layer layer1 = new Layer("Layer1");


            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc3_x, prf.Vuc3_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh1_x, prf.Vh1_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vh2_x, prf.Vh2_y));
            poly.Vertexes.Add(new LwPolylineVertex(prf.Vuc1_x, prf.Vuc1_y));

            poly.Layer = layer1;
            tst1.AddEntity(poly);
            HatchBoundaryPath boundary = new HatchBoundaryPath(new List<EntityObject> { poly });
            HatchPattern pattern = HatchPattern.Line;
            pattern.Scale = 3;
            pattern.Angle = 65;
            Hatch hatch = new Hatch(pattern, true);
            hatch.BoundaryPaths.Add(boundary);
            tst1.AddEntity(hatch);


            // draw all dimensions
            AlignedDimension t1 = new AlignedDimension(new Vector2(prf.TWS_dispvalloc_x, prf.TWS_dispvalloc_y), new Vector2(prf.VST_x, prf.VST_y), 2.5, myStyle);
            // draw dimension A
            AlignedDimension aln1 = new AlignedDimension(new Vector2(prf.V1_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y), 4.5, myStyle);
            tst1.AddEntity(aln1);
            // draw dimension B
            AlignedDimension aln2 = new AlignedDimension(new Vector2(prf.V3_x, prf.V5_y), new Vector2(prf.V5_x, prf.V5_y), 6, myStyle);
            tst1.AddEntity(aln2);
            // draw dimension F
            AlignedDimension aln3 = new AlignedDimension(new Vector2(prf.V5_x, prf.V5_y), new Vector2(prf.V6_x, prf.V6_y), -6, myStyle);
            tst1.AddEntity(aln3);
            // draw dimension E
            AlignedDimension aln4 = new AlignedDimension(new Vector2(prf.V6_x, prf.V7_y), new Vector2(prf.V7_x, prf.V7_y), -6, myStyle);
            tst1.AddEntity(aln4);
            // draw dimension D
            AlignedDimension aln5 = new AlignedDimension(new Vector2(prf.V1_x, prf.V1_y), new Vector2(prf.V10_x, prf.V10_y), 6, myStyle);
            tst1.AddEntity(aln5);

            //// side view dimension
            // W dimension
            AlignedDimension aln6 = new AlignedDimension(new Vector2(prf.V13_x, prf.V13_y), new Vector2(prf.V14_x, prf.V14_y), -6, myStyle);
            tst1.AddEntity(aln6);


            // angle dimension
            Line tmpln = new Line(new Vector2(prf.V3_x, prf.V3_y), new Vector2(prf.V3_x, prf.V3_y + 2));
            tst1.AddEntity(tmpln);

            //uncut dimension
            // draw dimension D
            AlignedDimension aln8 = new AlignedDimension(new Vector2(prf.V8_x, prf.V8_y), new Vector2(prf.Vuc1_x, prf.Vuc1_y), -6, myStyle);
            tst1.AddEntity(aln8);
            Text tx4 = new Text("UNCUT", new Vector2(prf.V8_x + 2, prf.V8_y - 10), 2.5);
            tst1.AddEntity(tx4);
            AlignedDimension aln9 = new AlignedDimension(new Vector2(prf.V2_x, prf.V2_y), new Vector2(prf.V9_x, prf.V9_y), 4, myStyle);
            tst1.AddEntity(aln9);

            //leader text OVERCUT and UPCUT
            TextStyle txtst = new TextStyle("Tahoma.ttf");
            Leader leader1 = new Leader("OVERCUT", new List<Vector2> { new Vector2(prf.V4_x - 12, prf.V4_y - 3), new Vector2(prf.V4_x + 10, prf.V4_y + 10) }, myStyle);
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader1.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader1);
            Leader leader2 = new Leader("UPCUT", new List<Vector2> { new Vector2(prf.V3_x + 12, prf.V3_y - 3), new Vector2(prf.V3_x + 15, prf.V3_y + 10) }, myStyle);
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.LeaderArrow, DimensionArrowhead.Dot));
            leader2.StyleOverrides.Add(new DimensionStyleOverride(DimensionStyleOverrideType.DimLineColor, AciColor.Red));
            tst1.AddEntity(leader2);
            //--------------------- text---------------------------
            Double h1 = 1.8;
            //Add all text information
            Text tx72 = new Text(prf.RMW_dispval, new Vector2(prf.RMW_dispvalloc_x_T, prf.RMW_dispvalloc_y_T), h1);
            tst1.AddEntity(tx72);
            Text tx73 = new Text(prf.CML_dispval, new Vector2(prf.CML_dispvalloc_x_T, prf.CML_dispvalloc_y_T), h1);
            tst1.AddEntity(tx73);
            Text tx75 = new Text(prf.TL_dispval, new Vector2(prf.TL_dispvalloc_x_T, prf.TL_dispvalloc_y_T), h1);
            tst1.AddEntity(tx75);
            Text tx10 = new Text(prf.TWS_dispval, new Vector2(prf.TWS_dispvalloc_x_T, prf.TWS_dispvalloc_y_T), h1);
            tst1.AddEntity(tx10);
            Text tx76 = new Text(prf.TWT_dispval, new Vector2(prf.TWT_dispvalloc_x_T, prf.TWT_dispvalloc_y_T), h1);
            tst1.AddEntity(tx76);
            Text tx77 = new Text(prf.GBD_dispval, new Vector2(prf.GBD_dimval_x, prf.GBD_dimval_y), h1);
            tst1.AddEntity(tx77);
            Text tx11 = new Text(prf.GPG_dispval, new Vector2(prf.GPG_dispvalloc_x, prf.GPG_dispvalloc_y), h1);
            tst1.AddEntity(tx11);
            Text tx3 = new Text(prf.GAG_dispval, new Vector2(prf.GAG_dispvalloc_x, prf.GAG_dispvalloc_y), h1);
            tst1.AddEntity(tx3);
            Text tx13 = new Text(prf.TPL_dispval, new Vector2(prf.TPL_dispvalloc_x_T, prf.TPL_dispvalloc_y_T), h1);
            tst1.AddEntity(tx13);

            Text tx83 = new Text(prf.IEG_dispval, new Vector2(prf.IEG_dispvalloc_x_T, prf.IEG_dispvalloc_y_T), h1);
            tst1.AddEntity(tx83);
            Text tx84 = new Text(prf.IUP_dispval, new Vector2(prf.IUP_dispvalloc_x_T, prf.IUP_dispvalloc_y_T), h1);
            tst1.AddEntity(tx84);
            Text tx85 = new Text(prf.IOV_dispval, new Vector2(prf.IOV_dispvalloc_x_T, prf.IOV_dispvalloc_y_T), h1);
            tst1.AddEntity(tx85);
            Text tx86 = new Text(prf.HRC_dispval, new Vector2(prf.HRC_dispvalloc_x_T, prf.HRC_dispvalloc_y_T), h1);
            tst1.AddEntity(tx86);

            Text tx87 = new Text(prf.CT_dispval, new Vector2(prf.CT_dispvalloc_x_T, prf.CT_dispvalloc_y_T), h1);
            tst1.AddEntity(tx87);
            Text tx88 = new Text(prf.CSP_dispval, new Vector2(prf.CSP_dispvalloc_x_T, prf.CSP_dispvalloc_y_T), h1);
            tst1.AddEntity(tx88);
            Text tx89 = new Text(prf.UCT_dispval, new Vector2(prf.UCT_dispvalloc_x_T, prf.UCT_dispvalloc_y_T), h1);
            tst1.AddEntity(tx89);
            Text tx90 = new Text(prf.OCT_dispval, new Vector2(prf.OCT_dispvalloc_x_T, prf.OCT_dispvalloc_y_T), h1);
            tst1.AddEntity(tx90);
            Text tx2 = new Text(prf.ECT_dispval, new Vector2(prf.ECT_dispvalloc_x_T, prf.ECT_dispvalloc_y_T), h1);
            tst1.AddEntity(tx2);

            Text tx91 = new Text(prf.FSZ_dispval, new Vector2(prf.FSZ_dispvalloc_x_T, prf.FSZ_dispvalloc_y_T), h1);
            tst1.AddEntity(tx91);
            Text tx92 = new Text(prf.PTN_dispval, new Vector2(prf.PTN_dispvalloc_x_T, prf.PTN_dispvalloc_y_T), h1);
            tst1.AddEntity(tx92);
            Text tx93 = new Text(prf.FNM_dispval, new Vector2(prf.FNM_dispvalloc_x_T, prf.FNM_dispvalloc_y_T), h1);
            tst1.AddEntity(tx93);
            Text tx94 = new Text(prf.USL_dispval, new Vector2(prf.USL_dispvalloc_x_T, prf.USL_dispvalloc_y_T), h1);
            tst1.AddEntity(tx94);
            Text tx1 = new Text(prf.UEL_dispval, new Vector2(prf.UEL_dispvalloc_x_T, prf.UEL_dispvalloc_y_T), h1);
            tst1.AddEntity(tx1);
            Text tx37 = new Text(prf.RMW_dispval, new Vector2(prf.RMW1_dispvalloc_x_T, prf.RMW1_dispvalloc_y_T), h1);
            tst1.AddEntity(tx37);
            Text tx100 = new Text(prf.note1_dispval, new Vector2(prf.nt1_x_T, prf.nt1_y_T), h1);
            tst1.AddEntity(tx100);
            Text tx101 = new Text(prf.note2_dispval, new Vector2(prf.nt2_x_T, prf.nt2_y_T), h1);
            tst1.AddEntity(tx101);
            Text tx102 = new Text(prf.note3_dispval, new Vector2(prf.nt3_x_T, prf.nt3_y_T), h1);
            tst1.AddEntity(tx102);
        }
        #endregion

        #endregion

        #endregion

        #region New By Swapnil
        
        [HttpGet]
        public JsonResult checkstampchart()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Stamp";
            string validation = objBAL.checknewmstrstamp(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {               
                return Json(validation, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult checkfilesize()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "FileSize";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkfilesubtype()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "FileSubType";
            FileSubTypeModel obj = new FileSubTypeModel();
            obj.filesubtypecheck = objBAL.checknewmstrsubtye(id, type, fo);
            if (obj.filesubtypecheck.Count > 1)
            {
                return Json(obj.filesubtypecheck[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                string no = "ok";
                return Json(no, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult checkCutspec()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "CutStandard";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult StampUpload(StampDetailsEntity model)
        {
            HttpPostedFileBase photo = Request.Files["1"];
            if(photo==null)
            {
                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].ContentLength == 0) continue;
                    string pathToSave = Server.MapPath("~/ProjectImages/Stamps/");
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                }
            }
           
            return RedirectToAction("Stamp");
        }
        [HttpPost]
        public ActionResult EditstampDetails(StampDetailsEntity model)
        {
            HttpPostedFileBase photo1 = Request.Files["side1"];
            HttpPostedFileBase photo2 = Request.Files["side2"];
            HttpPostedFileBase photo3 = Request.Files["side3"];
            HttpPostedFileBase photo4 = Request.Files["side4"];
            if (photo1.FileName != "")
            {
                model.side1image = photo1.FileName;
            }
            if (photo2.FileName != "")
            {
                model.side2image = photo2.FileName;
            }
            if (photo3.FileName != "")
            {
                model.side3image = photo3.FileName;
            }
            if (photo4.FileName != "")
            {
                model.side4image = photo4.FileName;
            }
           
            model.request_type = Convert.ToString(Session["requestype"]);
            model.request_id = Convert.ToInt16(Session["requestId"]);
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if (Session["FileSubTypeCode"] != null)
            {
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["FileSizeCode"] != null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["FileCutTypeCode"] != null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            //string fileTypeCode = Session["FileTypeCode"].ToString();
            //string FileSubType = Session["FileSubTypeCode"].ToString();
            //string fileSize = Session["FileSizeCode"].ToString();
            //string cuttype = Session["FileCutTypeCode"].ToString();
            model.sm_cust_id = Session["CoustomerId"].ToString();
            model.sm_brand_id = Session["BrandId"].ToString();
            model.sm_ftype_id = fileTypeCode;
            model.sm_fstype_id = FileSubType;
            model.sm_fsize_id = fileSize;
            model.sm_fcut_id = fileCutType;
            model.sm_stamp_mode = model.sm_stamp_mode;
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Stamps/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            objBAL.UpdateStampMaster(model);
            return RedirectToAction("Stamp");
        }

        [HttpPost]
        public ActionResult upload()
        {
            //string directory = @"D:\Temp\";
            HttpPostedFileBase photo = Request.Files["FILE1"];
            //photo = Request.Files["FILE2"];

            if (photo != null && photo.ContentLength > 0)
            {
                string spath = Server.MapPath("~/SharedCss");
                var fileName = Path.GetFileName(photo.FileName);
                photo.SaveAs(Path.Combine(spath, fileName));
                var ft_request_id = Convert.ToInt16(Session["requestId"]);
                var ft_request_type = Convert.ToString(Session["requestype"]);
                objBAL.newfiletypesaveImg(ft_request_id, ft_request_type, fileName);
            }

            return RedirectToAction("Index");
        }
        public PartialViewResult _FileSizeMasterAdd(FileSizeDetailsEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getFileSizefieldList();
            return PartialView(ReqParlist);
        }
        //public PartialViewResult _cutspecsMasterAdd(CutSpecsEntity model)
        //{
        //    MasterReqParModel ReqParlist = new MasterReqParModel();
        //    ReqParlist.ReqParlist = objBAL.getcutspecsfieldList();
        //    return PartialView(ReqParlist);
        //}
        [HttpGet]
        public JsonResult _cutspecsMasterAdd()
        {
             var fileTypeCode = Session["FileTypeCode"].ToString();
            // var FileSubType = Session["FileSubTypeCode"].ToString();
            // var fileSize = Session["FileSizeCode"].ToString();
            // var fileCutType = Session["FileCutTypeCode"].ToString();
           // string fileTypeCode = "";
           
            CutSpecFiledModel ReqParlist = new CutSpecFiledModel();
            ReqParlist.ReqParlist = objBAL.getcutspecsfieldListonfileType(fileTypeCode);
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult _FileTypeMasterAdd(FileTypeDataEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getFiletypefieldList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _FileSubTypeMasterAdd(ProductionMasterModel model)
        {
            //    MasterReqforsubtype ReqParlist = new MasterReqforsubtype();
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getFilesubtypefieldList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _Raw_MaterialMasterAdd(RawMaterialMastrEntity model)
        {
            //    MasterReqforsubtype ReqParlist = new MasterReqforsubtype();
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.GetRaw_MaterialfieldList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult StampMasterAdd(StampDetailsEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.GetStampMasterfieldList();
            return PartialView(ReqParlist);
        }
        //[HttpPost]
        //public ActionResult _StampMasterAdd(StampDetailsEntity model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        StampDetailsEntity model1 = new StampDetailsEntity();
        //        model1.sd_chart_num = model.sd_chart_num;
        //        model1.ImageName = model.ImageName;
        //        model1.request_type = Convert.ToString(Session["requestype"]);
        //        model1.request_id = Convert.ToInt16(Session["requestId"]);
        //        string fileTypeCode = Session["FileTypeCode"].ToString();
        //        string FileSubType = Session["FileSubTypeCode"].ToString();
        //        string fileSize = Session["FileSizeCode"].ToString();
        //        string cuttype = Session["FileCutTypeCode"].ToString();
        //        model1.sm_cust_id = Session["CoustomerId"].ToString();
        //        model1.sm_brand_id = Session["BrandId"].ToString();
        //        model1.sm_ftype_id = fileTypeCode;
        //        model1.sm_fstype_id = FileSubType;
        //        model1.sm_fsize_id = fileSize;
        //        model1.sm_fcut_id = cuttype;
        //        model1.sm_stamp_mode = model.sm_stamp_mode;
        //        objBAL.newStampMastersave(model1);
        //    }
        //    return RedirectToAction("Index");
        //}
        [HttpPost]
        public ActionResult StampMasterAdd1(RawMaterialMastrEntity model)
        {
            if (ModelState.IsValid)
            {
                RawMaterialMastrEntity model1 = new RawMaterialMastrEntity();
                model1.sd_chart_num = model.sd_chart_num;
                model1.ImageName = model.ImageName;
                model1.request_type = Convert.ToString(Session["requestype"]);
                model1.request_id = Convert.ToInt16(Session["requestId"]);
                //string fileTypeCode = Session["FileTypeCode"].ToString();
                //string FileSubType = Session["FileSubTypeCode"].ToString();
                //string fileSize = Session["FileSizeCode"].ToString();
                //string cuttype = Session["FileCutTypeCode"].ToString();
                string fileTypeCode = "";
                string FileSubType = "";
                string fileSize = "";
                string fileCutType = "";
                if (Session["FileTypeCode"].ToString() != null)
                {
                    fileTypeCode = Session["FileTypeCode"].ToString();
                }
                if (Session["FileSubTypeCode"].ToString() != null)
                {
                    FileSubType = Session["FileSubTypeCode"].ToString();
                }
                if (Session["FileSizeCode"] != null)
                {
                    fileSize = Session["FileSizeCode"].ToString();
                }
                if (Session["FileCutTypeCode"].ToString() != null)
                {
                    fileCutType = Session["FileCutTypeCode"].ToString();
                }
                model1.sm_cust_id = Session["CoustomerId"].ToString();
                model1.sm_brand_id = Session["BrandId"].ToString();
                model1.sm_ftype_id = fileTypeCode;
                model1.sm_fstype_id = FileSubType;
                model1.sm_fsize_id = fileSize;
                model1.sm_fcut_id = fileCutType;
                model1.sm_stamp_mode = model.sm_stamp_mode;
                model1.fontsize = model.fontsize;

                model1.stampsize = model.stampsize;
                model1.stamplocation = model.stamplocation;
                model1.Orientation = model.Orientation;
                model1.noofsides = model.noofsides;
                model1.side1image = model.side1image;
                model1.side2image = model.side2image;
                model1.side3image = model.side3image;
                model1.side4image = model.side4image;
                objBAL.newStampMastersave(model1);
            }
            return RedirectToAction("Index");
        }
      
        [HttpPost]
        public ActionResult Raw_MaterialMasterAdd(RawMaterialMastrEntity model)
        {
            if (ModelState.IsValid)
            {
                model.rm_request_id = Convert.ToInt16(Session["requestId"]);
                model.rm_request_type = Convert.ToString(Session["requestype"]);
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
                model.rm_fsize_id = fileSize;
                model.rm_ftype_id = fileTypeCode;
                model.rm_fstype_id = FileSubType;
                objBAL.NewRaw_MaterialSave(model);
            }
            return RedirectToAction("RawMaterial");
        }
        [HttpPost]
        public ActionResult CutspecsAdd(List<CutSpecsEntity> cutspecList)
        {
            int i = 0;
            CutSpecsrowwiseEntity model = new CutSpecsrowwiseEntity();
            foreach (var item in cutspecList)
            {
                
              // string fileTypeCode = Session["FileTypeCode"].ToString();
              // string FileSubType = Session["FileSubTypeCode"].ToString();
              // string fileSize = Session["FileSizeCode"].ToString();
              //  string fileCutType = Session["FileCutTypeCode"].ToString();
                string fileTypeCode = "";
                string FileSubType = "";
                string fileSize = "";
                string fileCutType = "";
                if (Session["FileTypeCode"].ToString() != null)
                {
                    fileTypeCode = Session["FileTypeCode"].ToString();
                }
                if (Session["FileSubTypeCode"].ToString() != null)
                {
                    FileSubType = Session["FileSubTypeCode"].ToString();
                }
                if (Session["FileSizeCode"]!= null)
                {
                    fileSize = Session["FileSizeCode"].ToString();
                }
                if (Session["FileCutTypeCode"].ToString() != null)
                {
                    fileCutType = Session["FileCutTypeCode"].ToString();
                }
                model.prc_request_id   = Convert.ToInt16(Session["requestId"]);
                model.prc_request_type = Session["requestype"].ToString();
                model.cutStandardCode = cutspecList[1].pm_dim_parm_dwgvalue;
                model.cutStandardName = cutspecList[0].pm_dim_parm_dwgvalue;
                if (i >= 2)
                {
                    model.prc_parameter_name    = item.prc_parameter_name;
                    model.prc_parameter_value   = item.prc_parameter_value;
                    model.pm_dim_parm_dwgvalue  = item.pm_dim_parm_dwgvalue;
                    model.pm_dim_parm_scrnvalue = item.pm_dim_parm_scrnvalue;
                    objBAL.Cutspecssavebyrow(model, fileTypeCode, FileSubType, fileSize, fileCutType);
                }
                i++; 
            }
            objBAL.AddCutspecsEditLink(model.prc_request_id,model.prc_request_type);
                                      
            return RedirectToAction("_cutSpecsTab");
        }
        [HttpPost]
        public ActionResult EditCutspecsAdd(List<CutSpecsEntity> cutspecList)
        {
            int i = 0;
            CutSpecsrowwiseEntity model = new CutSpecsrowwiseEntity();
            foreach (var item in cutspecList)
            {
                //string fileTypeCode = Session["FileTypeCode"].ToString();
                //string FileSubType = Session["FileSubTypeCode"].ToString();
                //string fileSize = Session["FileSizeCode"].ToString();
                //string fileCutType = Session["FileCutTypeCode"].ToString();
                model.prc_request_id = Convert.ToInt16(Session["requestId"]);
                model.prc_request_type = Session["requestype"].ToString();
                model.cutStandardCode = cutspecList[1].pm_dim_parm_dwgvalue;
                model.cutStandardName = cutspecList[0].pm_dim_parm_dwgvalue;
                if (i >= 2)
                {
                    model.prc_parameter_name = item.prc_parameter_name;
                    model.prc_parameter_value = item.prc_parameter_value;
                    model.pm_dim_parm_dwgvalue = item.pm_dim_parm_dwgvalue;
                    model.pm_dim_parm_scrnvalue = item.pm_dim_parm_scrnvalue;
                    objBAL.editCutspecssavebyrow(model);
                }
                i++;
            }
            //objBAL.AddCutspecsEditLink(model.prc_request_id, model.prc_request_type);

            return RedirectToAction("_cutSpecsTab");
        }
        [HttpPost]
        public ActionResult BrandAdd(BrandModel model)
        {
            // string abc =Request["1"].ToString();

            if (ModelState.IsValid)
            {
                BrandDetailsEntity model1 = new BrandDetailsEntity();
                model1.bm_brand_id = model.bm_brand_id;
                model1.bm_brand_name = model.bm_brand_name;
                model1.bm_brand_remarks = model.bm_brand_remarks;
                model1.bm_request_type = Convert.ToString(Session["requestype"]);
                model1.bm_request_id = Convert.ToInt16(Session["requestId"]);

                objBAL.newBrandsave(model1);
            }
            return RedirectToAction("Index");
        }
        public ActionResult FileSizAdd(FileSizeModel model)
        {
            // string abc =Request["1"].ToString();

            if (ModelState.IsValid)
            {
                FileSizeDetailsEntity model1 = new FileSizeDetailsEntity();
                model1.fs_size_inches = model.fs_size_inches;
                model1.fs_size_code = model.fs_size_code;
                model1.fs_size_remarks = model.fs_size_remarks;
                model1.fs_size_mm = model.fs_size_inches * 25;
                model1.fs_request_id = Convert.ToInt16(Session["requestId"]);
                model1.fs_request_type = Convert.ToString(Session["requestype"]);
                objBAL.newfilesizesaveave(model1);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult FiletypeAdd(tb_filetype_master model)
        {
            if (ModelState.IsValid)
            {
                FileTypeDataEntity model1 = new FileTypeDataEntity();
                model1.ft_ftype_code = model.ft_ftype_code;
                model1.ft_ftype_desc = model.ft_ftype_desc;
                model1.ft_ftype_remarks = model.ft_ftype_remarks;
                model1.ft_request_id = Convert.ToInt16(Session["requestId"]);
                model1.ft_request_type = Convert.ToString(Session["requestype"]);
                objBAL.newfiletypesaveave(model1);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditFileTypeDetails(tb_filetype_master model)
        {
            if (ModelState.IsValid)
            {
                FileTypeDataEntity model1 = new FileTypeDataEntity();
                model1.ft_ftype_code = model.ft_ftype_code;
                model1.ft_ftype_desc = model.ft_ftype_desc;
                model1.ft_ftype_remarks = model.ft_ftype_remarks;
                model1.ft_request_id = model.ft_request_id;
                model1.ft_request_type = model.ft_request_type;

                HttpPostedFileBase photo = Request.Files["FILE1"];
                //photo = Request.Files["FILE2"];
                if (photo != null && photo.ContentLength > 0)
                {
                    string spath = Server.MapPath("~/SharedCss");
                    var fileName = Path.GetFileName(photo.FileName);
                    photo.SaveAs(Path.Combine(spath, fileName));
                    model1.ft_ftype_profile_img = fileName;
                }

                objBAL.filetypeUpdate(model1);
            }
            return RedirectToAction("getFileSpecsDetails");
        }
        [HttpPost]
        public ActionResult EditfileSubTypeDetails(FileSubTypeEntity model)
        {
            if (ModelState.IsValid)
            {
                objBAL.filesubtypeUpdate(model);
            }
            return RedirectToAction("getFileSpecsDetails");
        }
        [HttpPost]
        public ActionResult EditfileSizeDetails(FileSizeDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                objBAL.FileSizeUpdate(model);
            }
            return RedirectToAction("getFileSpecsDetails");
        }
        [HttpPost]
        public ActionResult EditCutSpecDetails(CutSpecsEntity model)
        {
            if (ModelState.IsValid)
            {
                objBAL.CutSpecUpdate(model);

            }
            return RedirectToAction("CutSpecs");
        }
        [HttpPost]
        public ActionResult EditRawMaterialDetails(RawMaterialMastrEntity model)
        {
            if (ModelState.IsValid)
            {
                objBAL.RawMaterialUpdate(model);

            }
            return RedirectToAction("RawMaterial");
        }
        [HttpPost]
        public ActionResult FilesubtypeAdd(FileSubTypeModel model)
        {

            FileSubTypeEntity model1 = new FileSubTypeEntity();
            model1.fieldsubtypename = model.fieldsubtypename;
            model1.fieldsubtypecode = model.fieldsubtypecode;
            model1.fieldrmcode = model.fieldrmcode;
            model1.fieldwtinkgthousand = model.fieldwtinkgthousand;
            model1.fielddescription = model.fielddescription;
            model1.fieldwidth = model.fieldwidth;
            model1.fieldthikness = model.fieldthikness;
            model1.fieldgmssku = model.fieldgmssku;
            model1.fieldmood_length = model.fieldmood_length;
            model1.fieldlengthmm = model.fieldlengthmm;
            model1.fieldlengthP1mm = model.fieldlengthP1mm;
            model1.fieldwidthatShoulder = model.fieldwidthatShoulder;
            model1.fieldthiknessatshoulder = model.fieldthiknessatshoulder;
            model1.fieldwidthattip = model.fieldwidthattip;
            model1.fieldthiknessattip = model.fieldthiknessattip;
            model1.fieldpointsizeatexactlength = model.fieldpointsizeatexactlength;
            model1.fieldbodydimentionwidth = model.fieldbodydimentionwidth;
            model1.fieldbodydimentionthikness = model.fieldbodydimentionthikness;
            model1.fieldbodypointsize = model.fieldbodypointsize;
            model1.fieldtapperlengthmm = model.fieldtapperlengthmm;
            model1.fieldlengthaftercuttingbefore = model.fieldlengthaftercuttingbefore;
            model1.fieldatshoulder = model.fieldatshoulder;
            model1.fieldonedge = model.fieldonedge;
            model1.fieldupcut = model.fieldupcut;
            model1.fieldovercut = model.fieldovercut;
            model1.fieldedgecut = model.fieldedgecut;
            model1.fieldhardness = model.fieldhardness;
            model1.fieldangel = model.fieldangel;
            model1.fieldradious = model.fieldradious;
            model1.fieldRemarks = model.fieldRemarks;
            model1.subtypename = model.subtypename;
            model1.subtypecode = model.subtypecode;
            model1.rmcode = model.rmcode;
            model1.wtinkgthousand = model.wtinkgthousand;
            model1.description = model.description;
            model1.width = model.width;
            model1.thikness = model.thikness;
            model1.gmssku = model.gmssku;
            model1.mood_length = model.mood_length;
            model1.lengthmm = model.lengthmm;
            model1.lengthP1mm = model.lengthP1mm;
            model1.widthatShoulder = model.widthatShoulder;
            model1.thiknessatshoulder = model.thiknessatshoulder;
            model1.widthattip = model.widthattip;
            model1.thiknessattip = model.thiknessattip;
            model1.pointsizeatexactlength = model.pointsizeatexactlength;
            model1.bodydimentionwidth = model.bodydimentionwidth;
            model1.bodydimentionthikness = model.bodydimentionthikness;
            model1.bodypointsize = model.bodypointsize;
            model1.tapperlengthmm = model.tapperlengthmm;
            model1.lengthaftercuttingbefore = model.lengthaftercuttingbefore;
            model1.atshoulder = model.atshoulder;
            model1.onedge = model.onedge;
            model1.upcut = model.upcut;
            model1.overcut = model.overcut;
            model1.edgecut = model.edgecut;
            model1.hardness = model.hardness;
            model1.angel = model.angel;
            model1.radious = model.radious;
            model1.Remarks = model.Remarks;
            model1.rmcoderange = model.rmcoderange;
            model1.wtinkgthousandrange = model.wtinkgthousandrange;
            model1.descriptionrange = model.descriptionrange;
            model1.widthrange = model.widthrange;
            model1.thiknessrange = model.thiknessrange;
            model1.gmsskurange = model.gmsskurange;
            model1.mood_lengthrange = model.mood_lengthrange;
            model1.lengthmmrange = model.lengthmmrange;
            model1.lengthP1mmrange = model.lengthP1mmrange;
            model1.widthatShoulderrange = model.widthatShoulderrange;
            model1.thiknessatshoulderrange = model.thiknessatshoulderrange;
            model1.widthattiprange = model.widthattiprange;
            model1.thiknessattiprange = model.thiknessattiprange;
            model1.pointsizeatexactlengthrange = model.pointsizeatexactlengthrange;
            model1.bodydimentionwidthrange = model.bodydimentionwidthrange;
            model1.bodydimentionthiknessrange = model.bodydimentionthiknessrange;
            model1.bodypointsizerange = model.bodypointsizerange;
            model1.tapperlengthmmrange = model.tapperlengthmmrange;
            model1.lengthaftercuttingbeforerange = model.lengthaftercuttingbeforerange;
            model1.atshoulderrange = model.atshoulderrange;
            model1.onedgerange = model.onedgerange;
            model1.upcutrange = model.upcutrange;
            model1.overcutrange = model.overcutrange;
            model1.edgecutrange = model.edgecutrange;
            model1.hardnessrange = model.hardnessrange;
            model1.angelrange = model.angelrange;
            model1.radiousrange = model.radiousrange;


            model1.pm_request_id = Convert.ToInt16(Session["requestId"]);
            model1.pm_request_type = Convert.ToString(Session["requestype"]);

            string fileTypeCode = Session["FileTypeCode"].ToString();
            //string FileSubType = Session["FileSubTypeCode"].ToString();
            //string fileSize = Session["FilesizeCode"].ToString();
            string fileSize;
            if (Session["FileSizeCode"] != null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }                       
            else
            {
                 fileSize = "0Z";
            }
            model1.filetypecode = fileTypeCode;
            model1.filesizecode = fileSize;
            objBAL.newfileSubtypesave(model1);

            return RedirectToAction("Index");
        }

        #endregion

        #region Remarks written by yogesh

        public ActionResult SubmitRemarkProductionDetails(string prm_remarks, string prm_remark_tab)
        {
            try
            {
                PartnumgenRemarksModel remarkObj = new PartnumgenRemarksModel();
                int i = 0;
                if (ModelState.IsValid)
                {
                    PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
                    remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
                    remarkEntityObj.prm_request_type = Session["requestype"].ToString();
                    remarkEntityObj.prm_remark_tab = prm_remark_tab;
                    remarkEntityObj.prm_createby = "ABC";
                    remarkEntityObj.prm_remarks = prm_remarks;
                    objBAL.SaveRemark(remarkEntityObj);
                    i = 1;
                }
                if (i == 0)
                {
                    return Json(new { success = false, responseText = "Remark not added" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, responseText = "Remark added successfully." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SelectRemarkCustomerDetails(string prm_remark_tab)
        {
            PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
            PartnumgenRemarksModel objmodel = new PartnumgenRemarksModel();
            remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
            remarkEntityObj.prm_request_type = Session["requestype"].ToString();
            PartnumgenRemarksModel remarkobj = new PartnumgenRemarksModel();
            remarkEntityObj.remarklist = objBAL.getremarkdetail(prm_remark_tab, remarkEntityObj.prm_request_id, remarkEntityObj.prm_request_type);
            var RList = remarkEntityObj.remarklist;
            if (remarkEntityObj.remarklist.Count == 0)
            {
                return Json(new { success = true, remarks = "", remarkdate = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, remarks = RList }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Unit Master Written by yogesh

        //public ActionResult ManagePlant()
        //{
        //    UnitMasterModel umobj = new UnitMasterModel();
        //    umobj.unitlist = get_plantlist();
        //    return View(umobj);
        //}

        //public List<PlantMasterModel> get_plantlist()
        //{
        //    string flag = "plantlist";
        //    DataTable dt = objBAL.getPlnatlist(flag);
        //    PlantMasterModel modelobj = new PlantMasterModel();
        //    List<PlantMasterModel> list = new List<PlantMasterModel>();
        //    if (dt.Rows.Count > 0 && dt != null)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            PlantMasterModel objPlant = new PlantMasterModel();
        //            objPlant.PlantID = Convert.ToInt32(dt.Rows[i]["PlantID"]);
        //            objPlant.plantname = dt.Rows[i]["plantname"].ToString();
        //            objPlant.plantcode = dt.Rows[i]["plantcode"].ToString();
        //            objPlant.plantlocation = dt.Rows[i]["plantlocation"].ToString();
        //            list.Add(objPlant);
        //        }
        //    }
        //    return list.ToList();
        //}

        public ActionResult ManageAncillary()
        {
            AncillaryMasterModel amobj = new AncillaryMasterModel();
            amobj.ancilllarylist = get_ancillarylist();
            amobj.plantdropdownlist = objBAL.getPlantDropdownList();
            return View(amobj);
        }

        public List<AncillaryMasterModel> get_ancillarylist()
        {
            string flag = "ancillarylist";
            DataTable dt = objBAL.getAncillarylist(flag);
            AncillaryMasterModel modelobj = new AncillaryMasterModel();
            List<AncillaryMasterModel> list = new List<AncillaryMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AncillaryMasterModel objAnc = new AncillaryMasterModel();
                    objAnc.AncillaryID = Convert.ToInt32(dt.Rows[i]["AncillaryID"]);
                    objAnc.Ancillary_Name = dt.Rows[i]["Ancillary_Name"].ToString();
                    objAnc.Ancillary_Code = dt.Rows[i]["Ancillary_Code"].ToString();
                    objAnc.PlantName = dt.Rows[i]["PlantName"].ToString();
                    list.Add(objAnc);
                }
            }
            return list.ToList();
        }

        //[HttpPost]
        //public ActionResult AddAncillary(int PlantID,string Name, string Location)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        AncillaryMasterEntity Ancobj = new AncillaryMasterEntity();
        //        Ancobj.PlantID = PlantID;
        //      //  string plantCode = objBAL.PlantforPlantCode(PlantID);
        //        string tempPlantCode = plantCode + Name;
        //        Ancobj.PlantID =PlantID;
        //        Ancobj.Ancillary_Code = tempPlantCode;
        //        Ancobj.Ancillary_Name = Name;
        //        Ancobj.Ancillary_Location = Location;
        //        Ancobj.flag = "InsertAncillary";
        //        //objBAL.SaveAncillary(Ancobj);
        //    }
        //    return Json(new { success = true, alert = "Record insert successfully" }, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult PlantAncillaryManage()
        {
            AncillaryMasterModel amobj = new AncillaryMasterModel();
            amobj.ancilllarylist = get_plantancillarylist();

            return View(amobj);
        }
        public List<AncillaryMasterModel> get_plantancillarylist()
        {
            string flag = "plantancillarylist";
            DataTable dt = objBAL.getPlantAncillarylist(flag);
            AncillaryMasterModel modelobj = new AncillaryMasterModel();
            List<AncillaryMasterModel> list = new List<AncillaryMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AncillaryMasterModel objAnc = new AncillaryMasterModel();
                    objAnc.AncillaryID = Convert.ToInt32(dt.Rows[i]["AncillaryID"]);
                    objAnc.Ancillary_Name = dt.Rows[i]["Ancillary_Name"].ToString();
                    objAnc.plantname = dt.Rows[i]["plantname"].ToString();
                    objAnc.plantname = dt.Rows[i]["plantname"].ToString();
                    objAnc.plantcode = dt.Rows[i]["plantcode"].ToString();
                    objAnc.ValueStremCode = dt.Rows[i]["plantcode"].ToString() + dt.Rows[i]["Ancillary_Code"].ToString();
                    objAnc.Ancillary_Code = dt.Rows[i]["Ancillary_Code"].ToString();
                    list.Add(objAnc);
                }
            }
            return list.ToList();
        }
        #endregion

        #region Manage Equipment
        public ActionResult ManageEquipment()
        {
            return View();
        }
        #endregion
    }
}