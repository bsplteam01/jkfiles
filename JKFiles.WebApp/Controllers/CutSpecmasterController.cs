﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFilesBAL;
using JKFIlesEntity;
using System.Data;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Models;
using System.Linq.Dynamic;
namespace JKFiles.WebApp.Controllers
{
    public class CutSpecmasterController : Controller
    {
        BAL objBAL = new BAL();
        #region by Swapnil
        // GET: CutSpecmaster
        public ActionResult Index()
        {
            // string flag = "fulllist";
            // DataTable dt = objBAL.getCutSpecMaster(flag);
            // return View(dt);

            //Swapnil code
          //  CutSpecsEntity objcutspecs = new CutSpecsEntity();
          //  objcutspecs.cutSpecsNoActiveList = objBAL.getCutSpecNoActiveMaster();
           // objcutspecs.cutSpecsNoApproveList = objBAL.getCutSpecNoApproveMaster();
           // objcutspecs.cutSpecsEntityList = objBAL.getCutSpecMaster();
           // return View(objcutspecs);
             
            return View();
        }
        #region for grids
        public ActionResult cutspectmastr(string selectedCutStandardName, string ftypecode, string filetype, string cuttype, string cutstand, int? page)
        {

            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<CutSpecsEntity> cst = null;
            CutSpecsEntity objcutspecs = new CutSpecsEntity();
            List<CutSpecsEntity> cstlist = new List<CutSpecsEntity>();

            cstlist = objBAL.getCutSpecMaster();

            if (!String.IsNullOrEmpty(selectedCutStandardName))
            {
                ViewBag.selectedCutStandardName = selectedCutStandardName;
                cstlist = cstlist.Where(x => x.filesizecode == selectedCutStandardName).ToList();
            }

            if (!String.IsNullOrEmpty(ftypecode))
            {
                ViewBag.ftypecode = ftypecode;
                cstlist = cstlist.Where(x => x.fcl_ftype_code == ftypecode).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                cstlist = cstlist.Where(x => x.filetype == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(cuttype))
            {
                ViewBag.cuttype = cuttype;
                cstlist = cstlist.Where(x => x.cuttype == cuttype).ToList();
            }

            if (!String.IsNullOrEmpty(cutstand))
            {
                ViewBag.cutstand = cutstand;
                cstlist = cstlist.Where(x => x.cutStandardName == cutstand).ToList();
            }



            // cstlist = objBAL.getCutSpecMastersearch(selectedCutStandardName);
            objcutspecs.cutSpecsEntityList = cstlist;
            cst = cstlist.ToPagedList(pageIndex, pageSize);



            return PartialView("_cutspectmastr", cst);

        }
        [HttpGet]
        public ActionResult viewCutspectdetails(string reqno)
        {
            CutSpecsEntity ct = new CutSpecsEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string cutStandardName = values[0];
            string cuttype = values[1];
            string fcl_ftype_code = values[2];
            string filetype = values[3];
            string filesizecode = values[4];
            string flag = "viewcutspect";
            ct.listcutspectview = objBAL.getcutspectview1(flag, cutStandardName, cuttype, fcl_ftype_code, filetype, filesizecode);
            //DataTable dt = objBAL.getcutspectview(flag, cutStandardName, cuttype, fcl_ftype_code,filetype, filesizecode);
            //ct.side_edge = dt.Rows[0]["Side_Edge"].ToString();
            //ct.seq_no = (dt.Rows[0]["vtpi_parm_seq_no"].ToString());
            //ct.UPCUT = (dt.Rows[0]["UPCUT"].ToString());
            //ct.OVERCUT = dt.Rows[0]["OVERCUT"].ToString();
            //ct.EDGE = dt.Rows[0]["EDGE"].ToString();
            return PartialView("_Cutspectview", ct);

        }
        [HttpGet]
        public ActionResult viewCutspectAppdetails(string cutStandardName,string cuttype,string fcl_ftype_code,string filetype,string filesizecode)
        {
            CutSpecsEntity ct = new CutSpecsEntity();
            string flag = "viewcutspect";
            ct.listcutspectview = objBAL.getcutspectview1(flag, cutStandardName, cuttype, fcl_ftype_code, filetype, filesizecode);
            return PartialView("_Cutspectview", ct);

        }
        public ActionResult NewRequest()
        {

            //Swapnil code
            CutSpecsEntity objcutspecs = new CutSpecsEntity();

            objcutspecs.cutSpecsNoActiveList = objBAL.getCutSpecNoActiveMaster();

            return PartialView("_NewRequest", objcutspecs);

        }
        public ActionResult ApprovedRequest()
        {

            //Swapnil code
            CutSpecsEntity objcutspecs = new CutSpecsEntity();

            objcutspecs.cutSpecsNoApproveList = objBAL.getCutSpecNoApproveMaster();

            return PartialView("_ApprovedRequest", objcutspecs);

        }
        public ActionResult Cutspectmaster()
        {

            //Swapnil code
            CutSpecsEntity objcutspecs = new CutSpecsEntity();

            // objcutspecs.cutSpecsEntityList = objBAL.getCutSpecMaster();


            objcutspecs.listsizecode = get_size();
            objcutspecs.listtype = get_type();
            objcutspecs.listsubtype = get_subtype();
            objcutspecs.listfiletypecode = get_filetypecode();
            objcutspecs.listcuttype = get_cuttype();
            objcutspecs.listcutstandard = get_cutstandrd();
            return PartialView("_Cutspectmaster", objcutspecs);

        }
        public List<CutSpecsEntity> get_cutstandrd()
        {
            string flag = "getcutstandard";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.cut_standard = (dt.Rows[i]["Cut_Standard"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_cuttype()
        {
            string flag = "getcuttype";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.cut_type = (dt.Rows[i]["Cut_Type"].ToString());
                    obj.fc_cut_code = (dt.Rows[i]["fc_cut_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_filetypecode()
        {
            string flag = "getfiletypecode";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.filetypecode = (dt.Rows[i]["File_Type_Code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        #endregion
        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                IList<CutSpecsEntity> Data = objBAL.getCutSpecMaster();                    
                var customerData = (from tempcustomer in Data
                                    select tempcustomer);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.fcl_ftype_code.ToUpper().Contains(searchValue.ToUpper())
                        || m.filesizecode.ToString().ToUpper().Contains(searchValue.ToUpper()) || m.cuttype.ToUpper().ToString().Contains(searchValue.ToUpper())
                        || m.Cut_code.ToUpper().ToString().Contains(searchValue.ToUpper()) || m.filetype.ToUpper().ToString().Contains(searchValue.ToUpper())
                        || m.cutStandardName.ToUpper().ToString().Contains(searchValue.ToUpper()) || m.tpi_cut_std_code.ToUpper().ToString().Contains(searchValue.ToUpper())
                        );
                }
                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost]
        public ActionResult CutSpecsDetails(CutSpecsEntity model)
        {
            objBAL.CutSpecssaveactive(model);
            return RedirectToAction("Index");
        }
        public ActionResult UpdatecustspecApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdatecutspecApprove(ID, type);

                string subject = "";
                string body = "";
                subject = "Request No:  " + type + ID + " and " +
                       "Production Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",Cut Spec Master Approved.";


                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return Json(new { url = @Url.Action("Index", "CutSpecmaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "CutSpec";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "CutSpecmaster") }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult REQCutSpecDetails(int Id, string reqtype)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            CutSpecsEntity cutspecModelObj = new CutSpecsEntity();
            CutSpecsrowwiseEntity obj = new CutSpecsrowwiseEntity();

            obj.cutSpecsEntityList = objBAL.getcutspeclistforEdit(Id, reqtype);
            return Json(obj.cutSpecsEntityList, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult EditCutspecsAdd(List<CutSpecsEntity> cutspecList)
        {
            string flg = "CutSpec";
            int count = objBAL.checkiduniqnecut(cutspecList[0].prc_request_id, cutspecList[0].prc_request_type, cutspecList[1].pm_dim_parm_dwgvalue, flg);
            if (count >= 1)
            {
               // TempData["msg"] = "This Code Already Exist";
                return Json(new { success = true, url = @Url.Action("Index", "CutSpecmaster"), Message = "This Code Already Exist" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                int i = 0;
                CutSpecsrowwiseEntity model = new CutSpecsrowwiseEntity();
                foreach (var item in cutspecList)
                {
                    //string fileTypeCode = Session["FileTypeCode"].ToString();
                    //string FileSubType = Session["FileSubTypeCode"].ToString();
                    //string fileSize = Session["FileSizeCode"].ToString();
                    //string fileCutType = Session["FileCutTypeCode"].ToString();
                    //model.prc_request_id = Convert.ToInt16(Session["requestId"]);
                    //model.prc_request_type = Session["requestype"].ToString();
                    model.cutStandardCode = cutspecList[1].pm_dim_parm_dwgvalue;
                    model.cutStandardName = cutspecList[0].pm_dim_parm_dwgvalue;
                    model.prc_request_id = cutspecList[0].prc_request_id;
                    model.prc_request_type = cutspecList[0].prc_request_type;
                    if (i >= 2)
                    {
                        model.prc_parameter_name = item.prc_parameter_name;
                        model.prc_parameter_value = item.prc_parameter_value;
                        model.pm_dim_parm_dwgvalue = item.pm_dim_parm_dwgvalue;
                        model.pm_dim_parm_scrnvalue = item.pm_dim_parm_scrnvalue;
                        objBAL.editCutspecssavebyrowActive(model);
                    }
                    i++;
                }
                return Json(new { success = true, url = @Url.Action("Index", "CutSpecmaster"), Message = "Custspec Data Approved" }, JsonRequestBehavior.AllowGet);
            }
            //objBAL.AddCutspecsEditLink(model.prc_request_id, model.prc_request_type);

            
        }
        public ActionResult Cutspecs(string Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            CutSpecsEntity cutspecModelObj = new CutSpecsEntity();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Cutspecs_master_details_bycode", con);
            SqlCommand cmd1 = new SqlCommand("sp_get_Cutspecs_values_details_bycode", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd1.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter paramtype = new SqlParameter("@type", type);
                SqlParameter param1 = new SqlParameter("@id", Id);
                SqlParameter paramtype1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param);
                cmd.Parameters.Add(paramtype);
                cmd.Parameters.Add(paramtype1);
                cmd1.Parameters.Add(param1);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                con.Close();
                da.Fill(dt);
                da1.Fill(dt1);
                cutspecModelObj.fcl_ftype_code = dt.Rows[0]["File Code"].ToString();
                cutspecModelObj.filesize = Convert.ToDecimal(dt.Rows[0]["Nominal Length in inches"].ToString());
                cutspecModelObj.cuttype = dt.Rows[0]["Cut Type"].ToString();
                cutspecModelObj.cutStandardName = dt.Rows[0]["Cut Standard"].ToString();
                cutspecModelObj.upcutrange = dt.Rows[0]["UPCUT"].ToString();
                cutspecModelObj.edcutrange = dt.Rows[0]["EDGE"].ToString();
                cutspecModelObj.ovcutrange = dt.Rows[0]["OVER"].ToString();
                cutspecModelObj.upcutvalue = Convert.ToDecimal(dt1.Rows[0]["UPCUT"].ToString());
                cutspecModelObj.edcutvalue = Convert.ToDecimal(dt1.Rows[0]["EDGE"].ToString());
                cutspecModelObj.ovcutvalue = Convert.ToDecimal(dt1.Rows[0]["OVER"].ToString());
                cutspecModelObj.prc_request_id = Convert.ToInt16(dt.Rows[0]["tpi_request_id"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_cutspecsDetails", cutspecModelObj);
        }
        #endregion
        #region By Yogesh Cut Spect ECN
        [HttpGet]
        public ActionResult getCutSpaceDetailsList(string sizecode, string filecode,string fileSubtypecode, string cuttype, string cutstandar)
        {
            string requesttype = "E";
            string flag1 = "checkECNCutSpectUpdatedstatus";
            DataTable dt1 = objBAL.CheckEcnCutspectRequestisUpdated(flag1, sizecode, filecode, fileSubtypecode, cuttype, cutstandar, requesttype);
            List<FileSubTypeEntity> checkrequestID = new List<FileSubTypeEntity>();

            if (dt1.Rows.Count > 0 && dt1 != null)
            {
                checkrequestID = dt1.AsEnumerable().Select(item => new FileSubTypeEntity
                {
                    RequestID = item.Field<string>("RequestID")
                }
                   ).ToList();
                var requestNo = checkrequestID[0].RequestID;
                return Json(new { success = true, Message = "CutSpect being edited in request id=", checkrequestID = requestNo }, JsonRequestBehavior.AllowGet);
            }
            DataTable dt = new DataTable();
            try
            {
                string flag = "getecncutspectview";
                dt = objBAL.getcutspectdetailvalue(flag, sizecode, filecode, fileSubtypecode, cuttype, cutstandar);
                List<TpiCutStandardsEntity> tpicutllist = new List<TpiCutStandardsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    tpicutllist = dt.AsEnumerable().Select(item => new TpiCutStandardsEntity
                    {
                        tpi_cut_application = item.Field<string>("tpi_cut_application"),
                        tpi_dim_name = item.Field<string>("tpi_dim_name"),
                        tpi_dim_code = item.Field<string>("tpi_dim_code"),
                        tpi_cut_std_name = item.Field<string>("tpi_cut_std_name"),
                        tpi_dim_scrnval = item.Field<string>("tpi_dim_scrnval"),
                        tpi_dim_dwgval = item.Field<string>("tpi_dim_dwgval")
                    }
                    ).ToList();
                }
                return Json(new { success = true, tpicutllist = tpicutllist, Message = "", sizecode = sizecode, filecode = filecode, fileSubtypecode= fileSubtypecode, cuttype = cuttype, cutstandar = cutstandar }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult ManageSKUList1(List<TpiCutStandardsModel> AllArrayValue)
        {
            var sizecode = AllArrayValue[0].tpi_fsize_code;
            var filecode = AllArrayValue[0].tpi_ftype_code;
            var subtypCode = AllArrayValue[0].fileSubTypeCode;
            var cutCode = AllArrayValue[0].cutCode;
            var cutStandardName = AllArrayValue[0].cutStandardName;

            TempData["CutSpectlist"] = AllArrayValue;
            return Json(new { url = @Url.Action("ManageCutSpectSKUList", "CutSpecmaster", new { sizecode = sizecode, filecode = filecode, subtypCode = subtypCode, cutCode= cutCode, cutStandardName= cutStandardName }) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageCutSpectSKUList(string sizecode, string filecode, string subtypCode,string cutCode,string cutStandardName)
        {
            TpiCutStandardsModel obj = new TpiCutStandardsModel();
            var tempP1 = "%";
            string PartNoId = tempP1 + sizecode + filecode+ subtypCode+ cutCode+ cutStandardName+ tempP1;
            ViewBag.sizecode = sizecode;
            ViewBag.filecode = filecode + subtypCode;
            ViewBag.cutCode = cutCode;
            ViewBag.cutStandardName = cutStandardName;
            ViewBag.TodayDate = DateTime.Now;
            
            obj.skulist = get_CutSpecPartNumList(PartNoId);
            TempData["skulist"] = obj.skulist;
            if ((obj.skulist != null) && (obj.skulist.Any()))
            {
                DateTime Stmax = obj.skulist.Min(x => x.pn_start_date);
                DateTime Endmin = obj.skulist.Min(x => x.pn_end_date);
                DateTime todaydate = DateTime.Now;
                int cmp = Stmax.CompareTo(todaydate);
                if (cmp > 0)
                {
                    ViewBag.StartMaxDate = Stmax.ToString("yyyy-MM-dd");
                    // date1 is greater means date1 is comes after date2
                }
                else if (cmp < 0)
                {
                    ViewBag.StartMaxDate = todaydate.ToString("yyyy-MM-dd");
                    // date2 is greater means date1 is comes after date1
                }
                else
                {
                    ViewBag.StartMaxDate = Stmax.ToString("yyyy-MM-dd");
                    // date1 is same as date2
                }
                ViewBag.EndMaxDate = Endmin.ToString("yyyy-MM-dd");
            }
            if (!String.IsNullOrEmpty(sizecode))
            {
                ViewBag.or_request_keydata = sizecode + filecode + subtypCode + cutCode + cutStandardName;
            }
            ViewBag.subtypCode = subtypCode;
            if (TempData["CutSpectlist"] != null)
            {
                ViewBag.FileSubTypelist = TempData["CutSpectlist"].ToString();
                TempData["ECNCutspecttvalue"] = TempData["CutSpectlist"];
            }
            return View(obj);
        }
        protected List<TpiCutStandardsModel> get_CutSpecPartNumList(string pn_part_no)
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "CutSpectpartnumSKUlist";
            DataTable dt = objBAL.get_CutSpecPartNumList(flag, pn_part_no);
            List<TpiCutStandardsModel> partnumlist = new List<TpiCutStandardsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                partnumlist = dt.AsEnumerable().Select(item => new TpiCutStandardsModel()
                {
                    pn_part_no = item.Field<string>("pn_part_no"),
                    pn_fsize_code = item.Field<string>("pn_fsize_code"),
                    pn_ftype_code = item.Field<string>("pn_ftype_code"),
                    pn_fstype_code = item.Field<string>("pn_fstype_code"),
                   // pn_version_no = item.Field<int>("pn_version_no"),
                    pn_start_date = item.Field<DateTime>("pn_start_date"),
                    pn_end_date = item.Field<DateTime>("pn_end_date"),
                    pn_request_id = item.Field<int>("pn_request_id")
                }).ToList();
            }

          
            return partnumlist;
        }
        [HttpPost]
        public ActionResult InsertOtherRequest(string or_request_keydata, string or_effective_dt)
        {
            if (ModelState.IsValid)
            {
                List<TpiCutStandardsModel> ecnlist = (List<TpiCutStandardsModel>)TempData["ECNCutspecttvalue"];
                List<TpiCutStandardsModel> skulist = (List<TpiCutStandardsModel>)TempData["skulist"];
                OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
                TpiCutStandardsEntity Tcsobj = new TpiCutStandardsEntity();
                //// ECN Other Requesed
                Rorobj.or_effective_dt = Convert.ToDateTime(or_effective_dt);
                Rorobj.flag = "insertOtherrequest";
                Rorobj.or_request_from = "CutSpect";
                Rorobj.or_request_keydata = or_request_keydata;
                Rorobj.or_createby = Session["User_Id"].ToString();
                Rorobj.approveby = Session["User_Id"].ToString();
                int i = objBAL.SaveOtherRequest(Rorobj);
                int RequestID = i;
                ImpactedSkQMasterEntity imsobj = new ImpactedSkQMasterEntity();

                //// ECN Impact SkU Data
                foreach (var item1 in skulist)
                {
                    imsobj.im_request_type = "E";
                    imsobj.im_request_id = i;
                    imsobj.im_sku = item1.pn_part_no;
                    imsobj.im_sku_verno = item1.pn_version_no;
                    imsobj.im_pnum_updated = "N";
                    imsobj.im_sku_start_date = item1.pn_start_date;
                    imsobj.im_sku_end_date = item1.pn_end_date;
                    imsobj.flag = "insertimpactedsku";
                    int k = objBAL.SaveImpactedSku(imsobj);
                }
                //// ECN IProfile Master
                foreach (var item in ecnlist)
                {
                    string temverNo = objBAL.MaxVernoCutSpect(item.tpi_fsize_code, item.tpi_ftype_code,item.filesubtype,item.cutCode, item.cutStandardName);
                    int verNo = 0;
                    if (!String.IsNullOrEmpty(temverNo))
                    {
                        verNo = Convert.ToInt32(temverNo);
                    }
                    else
                    {
                        verNo = 1;
                    }
                    if (verNo > 0)
                    {
                        verNo = verNo + 1;
                    }
                    else
                    {
                        verNo = 1;
                    }
                    Tcsobj.tpi_cut_application = item.tpi_cut_application; //
                    Tcsobj.tpi_dim_name = item.tpi_dim_name; //
                    Tcsobj.tpi_dim_code = item.tpi_dim_code; //
                    Tcsobj.tpi_cut_std_code = item.cutStandardName;//  
                    Tcsobj.tpi_cut_std_name =item.tpi_cut_std_name;//  
                    Tcsobj.tpi_dim_dwgval = item.tpi_dim_dwgval;//
                    Tcsobj.tpi_dim_scrnval = item.tpi_dim_scrnval; //
                    Tcsobj.tpi_request_id = RequestID;// 
                    Tcsobj.tpi_cut_code = item.cutCode;//1
                    Tcsobj.tpi_request_type = "E"; //
                    Tcsobj.tpi_verno = Convert.ToInt32(verNo);//
                    Tcsobj.tpi_fsize_code = item.tpi_fsize_code; //1
                    string filecode = item.tpi_ftype_code;//
                    Tcsobj.tpi_ftype_code = filecode.Substring(0, 2);//1
                    Tcsobj.tpi_fstype_code = item.filesubtype;//1
                    Tcsobj.flag = "inserTtpiemaster";
                    int j = objBAL.SaveTpiMaster(Tcsobj);
                }
                return Json(new { url = @Url.Action("Index", "CutSpecmaster") }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { url = @Url.Action("Index", "CutSpecmaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CutSpecECN(string partno,string keydata)
        {
            string sizecode = keydata.Substring(0, 2);
            string filecode = keydata.Substring(2, 3);
            string subtypCode = keydata.Substring(4, 1);
            string CutType = keydata.Substring(5, 1);
            string tpi_cut_std_code = keydata.Substring(6, 2);
            string flag = "getCutName";
            string cutname = objBAL.getStringName(CutType, flag);
            flag = "getCutstandard";
            string stdname = objBAL.getStringName(tpi_cut_std_code, flag);
            return RedirectToAction("ManageCutSpectRequestUpdatedforApproval", new
            {
                sizecode = sizecode,
                filecode = filecode,
                subtypCode = subtypCode,
                CutType = CutType,
                tpi_cut_std_code = tpi_cut_std_code,
                cutname = cutname,
                stdname = stdname
            });
        }
        public ActionResult ManageCutSpectRequestUpdated(string sizecode, string filecode, string subtypCode, string CutType, string tpi_cut_std_code, string cutname, string stdname)
        {
            TpiCutStandardsModel obj = new TpiCutStandardsModel();
            string tempCode = filecode.Substring(0, 2);
            obj.oldvaluelist = getoldvaluelist(sizecode, tempCode, subtypCode, CutType, tpi_cut_std_code);
            obj.newvaluelist = getnewvaluelist(sizecode, tempCode, subtypCode, CutType, tpi_cut_std_code);
            string tempP1 = "%";
            string PartNoId = tempP1 + sizecode + filecode + subtypCode + CutType + tpi_cut_std_code + tempP1;
            ViewBag.sizecode = sizecode;
           
            var requestID = 0;
            string EditDate = "";
            if ((obj.newvaluelist != null))
            {
                requestID = obj.newvaluelist[0].tpi_request_id;
                EditDate = objBAL.editSkudate(requestID);
                obj.SkuEditDAte = EditDate;
            }
            ViewBag.requestID = requestID;
            obj.skulist = get_CutSpecPartNumDetailList(requestID);
            obj.pn_fsize_code = sizecode;
            obj.pn_ftype_code = filecode;
            obj.tpi_cut_code = CutType;
            obj.cutname = cutname;
            obj.stdname = stdname;
            return View(obj);
        }



        public ActionResult ManageCutSpectRequestUpdatedforApproval(string sizecode, string filecode, string subtypCode, string CutType, string tpi_cut_std_code, string cutname, string stdname)
        {
            TpiCutStandardsModel obj = new TpiCutStandardsModel();
            string tempCode = filecode.Substring(0, 2);
            obj.oldvaluelist = getoldvaluelist(sizecode, tempCode, subtypCode, CutType, tpi_cut_std_code);
            obj.newvaluelist = getnewvaluelistforapproval(sizecode, tempCode, subtypCode, CutType, tpi_cut_std_code);
            string tempP1 = "%";
            string PartNoId = tempP1 + sizecode + filecode + subtypCode + CutType + tpi_cut_std_code + tempP1;
            ViewBag.sizecode = sizecode;

            var requestID = 0;
            string EditDate = "";
            if ((obj.newvaluelist != null))
            {
                requestID = obj.newvaluelist[0].tpi_request_id;
                EditDate = objBAL.editSkudate(requestID);
                obj.SkuEditDAte = EditDate;
            }
            ViewBag.requestID = requestID;
            obj.skulist = get_CutSpecPartNumDetailList(requestID);
            obj.pn_fsize_code = sizecode;
            obj.pn_ftype_code = filecode;
            obj.tpi_cut_code = CutType;
            obj.cutname = cutname;
            obj.stdname = stdname;
            return View(obj);
        }



        [HttpPost]
        public ActionResult ManageRequestUpdated1(string sizecode, string filecode, string subtypCode, string CutType,string tpi_cut_std_code,string cutname, string stdname)
        {
            return Json(new { url = @Url.Action("ManageCutSpectRequestUpdated", "CutSpecmaster", new { sizecode = sizecode, filecode = filecode, subtypCode = subtypCode, CutType = CutType, tpi_cut_std_code = tpi_cut_std_code, cutname= cutname, stdname = stdname }) }, JsonRequestBehavior.AllowGet);
        }
        protected List<TpiCutStandardsModel> getoldvaluelist(string sizecode, string filecode, string subtypCode, string CutType, string tpi_cut_std_code)
        {
          
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "getecncutspectview";
            DataTable dt = new DataTable();
            dt = objBAL.getcutspectdetailvalue(flag, sizecode, filecode, subtypCode, CutType, tpi_cut_std_code);
            List<TpiCutStandardsModel> oldvaluelist = new List<TpiCutStandardsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                oldvaluelist = dt.AsEnumerable().Select(item => new TpiCutStandardsModel()
                {
                    tpi_cut_application = item.Field<string>("tpi_cut_application"),
                    tpi_dim_name = item.Field<string>("tpi_dim_name"),
                    tpi_dim_code = item.Field<string>("tpi_dim_code"),
                    tpi_dim_scrnval = item.Field<string>("tpi_dim_scrnval"),
                    tpi_dim_dwgval = item.Field<string>("tpi_dim_dwgval")
                }
                ).ToList();
            }

            return oldvaluelist;

        }
        protected List<TpiCutStandardsModel> getnewvaluelist(string sizecode, string filecode, string subtypCode, string CutType, string tpi_cut_std_code)
        {
            FileSpec objfile = new FileSpec();
            string tpi_request_type = "E";
            string flag = "getecncutspectnewvalueview";
            DataTable dt = objBAL.getecnCutSpectnewValue(flag, sizecode, filecode, subtypCode, CutType, tpi_cut_std_code, tpi_request_type);
            List<TpiCutStandardsModel> newvaluelist = new List<TpiCutStandardsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                newvaluelist = dt.AsEnumerable().Select(item => new TpiCutStandardsModel()
                {
                    tpi_cut_application = item.Field<string>("tpi_cut_application"),
                    tpi_dim_name = item.Field<string>("tpi_dim_name"),
                    tpi_dim_code = item.Field<string>("tpi_dim_code"),
                    tpi_dim_scrnval = item.Field<string>("tpi_dim_scrnval"),
                    tpi_dim_dwgval = item.Field<string>("tpi_dim_dwgval"),
                    tpi_request_id = item.Field<int>("tpi_request_id")
                   
                }
                ).ToList();
            }
            return newvaluelist;
        }



        protected List<TpiCutStandardsModel> getnewvaluelistforapproval(string sizecode, string filecode, string subtypCode, string CutType, string tpi_cut_std_code)
        {
            FileSpec objfile = new FileSpec();
            string tpi_request_type = "E";
            string flag = "getecncutspectforapproval";
            DataTable dt = objBAL.getecnCutSpectnewValue(flag, sizecode, filecode, subtypCode, CutType, tpi_cut_std_code, tpi_request_type);
            List<TpiCutStandardsModel> newvaluelist = new List<TpiCutStandardsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                newvaluelist = dt.AsEnumerable().Select(item => new TpiCutStandardsModel()
                {
                    tpi_cut_application = item.Field<string>("tpi_cut_application"),
                    tpi_dim_name = item.Field<string>("tpi_dim_name"),
                    tpi_dim_code = item.Field<string>("tpi_dim_code"),
                    tpi_dim_scrnval = item.Field<string>("tpi_dim_scrnval"),
                    tpi_dim_dwgval = item.Field<string>("tpi_dim_dwgval"),
                    tpi_request_id = item.Field<int>("tpi_request_id")

                }
                ).ToList();
            }
            return newvaluelist;
        }


        public ActionResult RejectECNRequest(string RequestID, string Remarks)
        {
            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
            string requestformcheck = objBAL.requestform(Convert.ToInt32(RequestID));
            Rorobj.or_request_id = Convert.ToInt32(RequestID);
            Rorobj.flag = "RejectRequestNo";
            Rorobj.or_remarks = Remarks;
            Rorobj.or_request_type = "E";
            int i = objBAL.ChangeRejectRequest(Rorobj);
            FileSubTypeEntity Fstobj = new FileSubTypeEntity();
            TpiCutStandardsEntity tcsobj = new TpiCutStandardsEntity();
            if (requestformcheck == "FileSubType")
            {
                Fstobj.pm_request_id = Convert.ToInt32(RequestID);
                Fstobj.pm_request_type = "E";
                Fstobj.pm_dim_parm_isApproved = "R";
                Fstobj.flag = "RejectRequestNoprofilemaster";
                int j = objBAL.ChangeRejectRequestProfileMaster(Fstobj);

            }
            else if (requestformcheck == "CutSpect")
            {
                tcsobj.tpi_request_id = Convert.ToInt32(RequestID);
                tcsobj.tpi_request_type = "E";
                tcsobj.tpi_isApproved = "R";
                tcsobj.flag = "RejectRequestNoCutspectmaster";
                int j = objBAL.ChangeRejectRequestCutSpectMaster(tcsobj);

            }
            else if (requestformcheck == "PartNum")
            {

            }
            else
            {

            }
            return Json(new { url = @Url.Action("Index", "CutSpecmaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveECNRequest(string RequestID)
        {
            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();

            Rorobj.or_request_id = Convert.ToInt32(RequestID);
            Rorobj.flag = "ApproveRequestNo";
            Rorobj.or_request_from = "CutSpect";
            Rorobj.or_request_type = "E";
            int i = objBAL.ChangeRejectRequest(Rorobj);
            TpiCutStandardsEntity tcsobj = new TpiCutStandardsEntity();
            tcsobj.tpi_request_id = Convert.ToInt32(RequestID);
            tcsobj.flag = "ApproveRequestNoCutSpectemaster";
            tcsobj.tpi_request_type = "E";
            int j = objBAL.ChangeRejectRequestCutSpectMaster(tcsobj);



            //Fstobj.flag = "ApproveOldRequestNoprofilemaster";
            //int k = objBAL.ChangeRejectRequestProfileMaster(Fstobj);
            return Json(new { url = @Url.Action("Index", "CutSpecmaster") }, JsonRequestBehavior.AllowGet);
        }
        protected List<TpiCutStandardsModel> get_CutSpecPartNumDetailList(int tpi_request_id)
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "CutSpectpartnumDetailsSKUlist";
            DataTable dt = objBAL.get_CutSpecPartNumDetailList(flag, tpi_request_id);
            List<TpiCutStandardsModel> partnumlist = new List<TpiCutStandardsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                partnumlist = dt.AsEnumerable().Select(item => new TpiCutStandardsModel()
                {
                    im_sku = item.Field<string>("im_sku").ToString(),
                  //  im_sku_verno = item.Field<int>("im_sku_verno"),
                    im_sku_start_date = Convert.ToDateTime(item.Field<DateTime>("im_sku_start_date")),
                    im_sku_end_date = Convert.ToDateTime(item.Field<DateTime>("im_sku_end_date")
                   )

                }).ToList();
            }


            return partnumlist;
        }
        #endregion
    }
}