﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;

namespace JKFiles.WebApp.Controllers
{
    public class UserApprovalController : Controller
    {
        BAL objBAL = new BAL();
        // GET: UserApproval
        public ActionResult UserApprovalIndex()
        {
            UserLoginModel modelobj = new UserLoginModel();
            modelobj.listuser = get_userlist();
            var v = modelobj.listuser;
            return View("UserApprovalIndex",v);
        }

        public List<UserLoginModel> get_userlist()
        {
            string flag = "getalluserdetailspending";
            DataTable dt = objBAL.getUserlist(flag);
            UserRoleModel modelobj = new UserRoleModel();
            List<UserLoginModel> list = new List<UserLoginModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserLoginModel objRole = new UserLoginModel();
                    objRole.Login_Id = (dt.Rows[i]["Login_Id"].ToString());
                    objRole.User_Id = (dt.Rows[i]["User_Id"].ToString());
                    objRole.RoleName = dt.Rows[i]["RoleName"].ToString();
                    objRole.PlantName = dt.Rows[i]["PlantName"].ToString();
                    objRole.FirstName = dt.Rows[i]["FirstName"].ToString();
                    objRole.LastName = dt.Rows[i]["LastName"].ToString();
                    objRole.Main_Login_Flag = dt.Rows[i]["Main_Login_Flag"].ToString();
                    objRole.Report_Login_Flag = dt.Rows[i]["Report_Login_Flag"].ToString();
                    list.Add(objRole);
                }
            }
            return list.ToList();
        }


        public ActionResult Approve(string id, UserLoginModel vm)
        {
            UsermgmtEntity user = new UsermgmtEntity();

            user.Login_Id = id;

            user.flag = "updateapprove";

            objBAL.updateapprove(user);

            return RedirectToAction("UserApprovalIndex", "UserApproval");
        }


        [HttpGet]
        public ActionResult CallAprovalReject(string reqno)
        {
            UserLoginModel user = new UserLoginModel();
            user.User_Id = reqno;
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_RejectUser", user);

        }


        [HttpPost]
        public ActionResult Rejectuser(UserLoginModel obj)
        {

            UsermgmtEntity user = new UsermgmtEntity();
            user.Decline_Reason = obj.decline_reason;
            user.User_Id = obj.User_Id;
            user.flag = "insertrejection";

            objBAL.insertrejection(user);

           // return RedirectToAction("Index");
            return RedirectToAction("UserApprovalIndex", "UserApproval");
        }
    }
}