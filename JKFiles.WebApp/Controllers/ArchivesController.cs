﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class ArchivesController : Controller
    {
        BAL objBAL = new BAL();
        // GET: Archives
        public ActionResult ArchivesIndex()
        {
            return View();
        }        

        public ActionResult ArchivesItemMaster(string sku, string cust_sku, string custname, string brandname,
            string filesize, string filetype, string cuttype, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
           // pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<ChangeNoteModel> rmm = null;
            ChangeNoteModel modelobj = new ChangeNoteModel();
            // modelobj.completedreqlist = get_completed_req();
            List<ChangeNoteModel> objlist = new List<ChangeNoteModel>();
            objlist = GetHistory();

            if (!String.IsNullOrEmpty(sku))
            {
                ViewBag.sku = sku;
                objlist = objlist.Where(x => x.PartNo == sku).ToList();
            }

            if (!String.IsNullOrEmpty(cust_sku))
            {
                ViewBag.cust_sku = cust_sku;
                objlist = objlist.Where(x => x.CustpartNo == cust_sku).ToList();
            }

            if (!String.IsNullOrEmpty(custname))
            {
                ViewBag.custname = custname;
                objlist = objlist.Where(x => x.cust_name == custname).ToList();
            }

            if (!String.IsNullOrEmpty(brandname))
            {
                ViewBag.brandname = brandname;
                objlist = objlist.Where(x => x.brand_name == brandname).ToList();
            }

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                objlist = objlist.Where(x => x.size_inches == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                objlist = objlist.Where(x => x.file_type == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(cuttype))
            {
                ViewBag.cuttype = cuttype;
                objlist = objlist.Where(x => x.cut_type == cuttype).ToList();
            }

            modelobj.completedreqlist = objlist;
            Session["page"] = page;
            rmm = objlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_ItemMasterArchives", rmm);
            //return View();
        }

        public List<ChangeNoteModel> GetHistory()
        {
            string flag = "item_master";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.Archives(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.PartNo = dt.Rows[i]["PartNo"].ToString();
                    string partno = dt.Rows[i]["PartNo"].ToString();
                    objChangeNote.split1 = partno.Substring(0, 2);
                    objChangeNote.split2 = partno.Substring(2, 8);
                    objChangeNote.split3 = partno.Substring(10, 3);
                    objChangeNote.SAPNo = dt.Rows[i]["SapNo"].ToString();
                    objChangeNote.Description = dt.Rows[i]["Desc"].ToString();
                    objChangeNote.ApprovedDate = dt.Rows[i]["ApprovalDate"].ToString();
                    objChangeNote.VerNo = dt.Rows[i]["VerNo"].ToString();
                    objChangeNote.CustpartNo = dt.Rows[i]["CutPartNo"].ToString();
                    objChangeNote.cust_name = dt.Rows[i]["cust_name"].ToString();
                    objChangeNote.brand_name = dt.Rows[i]["brand_name"].ToString();
                    objChangeNote.size_inches = dt.Rows[i]["size_inches"].ToString();
                    objChangeNote.file_type = dt.Rows[i]["file_type"].ToString();
                    objChangeNote.cut_type = dt.Rows[i]["cut_type"].ToString();
                    objChangeNote.endDate = dt.Rows[i]["end_date"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }

        public ActionResult DrawingArchives()
        {
            DrawingLibraryModel objDrawingModel = new DrawingLibraryModel();
            objDrawingModel.listdrawing = getDrawing();
            return PartialView("_DrawingArchives",objDrawingModel);
        }

        protected List<DrawingLibraryModel> getDrawing()
        {
            string flag = "drawingArchives";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.Archives(flag, userid);
            List<DrawingLibraryModel> listdrawing = new List<DrawingLibraryModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DrawingLibraryModel objdraw = new DrawingLibraryModel();
                    objdraw.dwg_prod_sku = dt.Rows[i]["dwg_prod_sku"].ToString();
                    objdraw.dwg_dxfname = dt.Rows[i]["dwg_dxfname"].ToString();
                    objdraw.dwg_pdfname = dt.Rows[i]["dwg_pdfname"].ToString();
                    listdrawing.Add(objdraw);
                }
            }
            return listdrawing;
        }


        public ActionResult ItemMasterDDL()
        {
            ItemmasterModel objmodel = new ItemmasterModel();
            objmodel.SKUlist = get_SKU();
            objmodel.Cust_skulist = get_CustSKU();
            objmodel.custList = objBAL.getCustomerList();
            objmodel.brandList = objBAL.getBrandList();

            objmodel.filesizelist = getFileSize();
            objmodel.filetTypelist = getFileType();
            objmodel.CutTypelist = getFileCutType();
            return PartialView("_DDLView", objmodel);
        }


        public List<FileSizeDetailsEntity> getFileSize()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileSize";
                dt = objBAL.getMasters(flag);
                List<FileSizeDetailsEntity> listfilespec = new List<FileSizeDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilespec = dt.AsEnumerable().Select(item => new FileSizeDetailsEntity
                    {
                        fs_size_inches = item.Field<decimal>("FileSize"),
                        fs_size_code = item.Field<string>("fs_size_code")
                    }).ToList();
                }

                // ViewData["FileSize"] = listfilespec.ToList();
                return listfilespec;
                // TempData["FileSize"] = listfilespec.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get fileType
        public List<FileTypeDataEntity> getFileType()
        {
            DataTable dt = new DataTable();
            List<FileTypeDataEntity> fileTypelist = new List<FileTypeDataEntity>();
            try
            {
                string flag = "getFileType";
                dt = objBAL.getMasters(flag);
                //List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    fileTypelist = dt.AsEnumerable().Select(item => new FileTypeDataEntity
                    {
                        ft_ftype_desc = item.Field<string>("FileType"),
                        ft_ftype_code = item.Field<string>("ft_ftype_code")
                    }).ToList();
                }
                return fileTypelist;
                //ViewData["FileType"] = listfilespec.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get fileCutType
        public List<CutTypeDetailsEntity> getFileCutType()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileCutType";
                dt = objBAL.getMasters(flag);
                List<CutTypeDetailsEntity> CutTypelist = new List<CutTypeDetailsEntity>();
                // List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    CutTypelist = dt.AsEnumerable().Select(item => new CutTypeDetailsEntity
                    {
                        fc_cut_type = item.Field<string>("CutType"),
                        fc_cut_code = item.Field<string>("fc_cut_code")

                    }).ToList();
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.CutType = dt.Rows[i]["CutType"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                }
                //ViewBag.CutType = listfilespec.ToList();
                return CutTypelist;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ItemmasterModel> get_SKU()
        {
            string flag = "sku_list_archives";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            ItemmasterModel modelobj = new ItemmasterModel();
            List<ItemmasterModel> list = new List<ItemmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemmasterModel obj = new ItemmasterModel();
                    obj.SKU = (dt.Rows[i]["SKU"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<ItemmasterModel> get_CustSKU()
        {
            string flag = "cust_sku_archives";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            ItemmasterModel modelobj = new ItemmasterModel();
            List<ItemmasterModel> list = new List<ItemmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemmasterModel obj = new ItemmasterModel();
                    obj.Cust_sku = (dt.Rows[i]["cust_sku"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

    }
}