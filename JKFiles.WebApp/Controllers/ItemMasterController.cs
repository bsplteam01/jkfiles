﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [SessionTimeout]
    public class ItemMasterController : Controller
    {
        BAL objBAL = new BAL();
        // GET: ItemMaster
        #region by Dhanashree
        public ActionResult ItemMasterIndex()
        {
            //Session["Customer"] = null;
            //Session["Brand"] = null;
            //Session["Country"] = null;
            //ChangeNoteModel modelobj = new ChangeNoteModel();
            //modelobj.completedreqlist = get_completed_req();
            return View();
        }


        public ActionResult RequestIndex()
        {
            //Session["Customer"] = null;
            //Session["Brand"] = null;
            //Session["Country"] = null;
            //ChangeNoteModel modelobj = new ChangeNoteModel();
            //modelobj.completedreqlist = get_completed_req();
            return PartialView("_RequestIndex");
        }

        //Completed Requests-01.06.2018-by Dhanashree
        public List<ChangeNoteModel> get_completed_req()
        {
            string flag = "completed_req";
            string userid = Session["User_Id"].ToString();
            DataTable dt = objBAL.getChangeNote(flag, userid);
            ChangeNoteModel modelobj = new ChangeNoteModel();
            List<ChangeNoteModel> list = new List<ChangeNoteModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChangeNoteModel objChangeNote = new ChangeNoteModel();
                    objChangeNote.RequestNumber = dt.Rows[i]["ReqNo"].ToString();
                    string retype= dt.Rows[i]["ReqNo"].ToString();
                    objChangeNote.PartNo = dt.Rows[i]["PartNo"].ToString();
                    string partno = dt.Rows[i]["PartNo"].ToString();
                    objChangeNote.ReqType= retype.Substring(0, 1);
                    objChangeNote.split1 = partno.Substring(0, 2);
                    objChangeNote.split2 = partno.Substring(2, 8);
                    objChangeNote.split3 = partno.Substring(10, 3);
                    objChangeNote.SAPNo = dt.Rows[i]["SapNo"].ToString();
                    objChangeNote.Description = dt.Rows[i]["Desc"].ToString();
                    objChangeNote.ApprovedDate = dt.Rows[i]["ApprovalDate"].ToString();
                    objChangeNote.VerNo = dt.Rows[i]["VerNo"].ToString();
                    objChangeNote.CustpartNo = dt.Rows[i]["CutPartNo"].ToString();
                    objChangeNote.cust_name = dt.Rows[i]["cust_name"].ToString();
                    objChangeNote.brand_name = dt.Rows[i]["brand_name"].ToString();
                    objChangeNote.size_inches = dt.Rows[i]["size_inches"].ToString();
                    objChangeNote.file_type = dt.Rows[i]["file_type"].ToString();
                    objChangeNote.cut_type = dt.Rows[i]["cut_type"].ToString();
                    objChangeNote.endDate = dt.Rows[i]["end_date"].ToString();
                    list.Add(objChangeNote);
                }
            }
            return list.ToList();
        }


        public ActionResult ItemMaster(string sku, string cust_sku, string custname, string brandname,
            string filesize, string filetype, string cuttype, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<ChangeNoteModel> rmm = null;
            ChangeNoteModel modelobj = new ChangeNoteModel();
            // modelobj.completedreqlist = get_completed_req();
            List<ChangeNoteModel> objlist = new List<ChangeNoteModel>();
            objlist = get_completed_req();

            if (!String.IsNullOrEmpty(sku))
            {
                ViewBag.sku = sku;
                objlist = objlist.Where(x => x.PartNo == sku).ToList();
            }

            if (!String.IsNullOrEmpty(cust_sku))
            {
                ViewBag.cust_sku = cust_sku;
                objlist = objlist.Where(x => x.CustpartNo == cust_sku).ToList();
            }

            if (!String.IsNullOrEmpty(custname))
            {
                ViewBag.custname = custname;
                objlist = objlist.Where(x => x.cust_name == custname).ToList();
            }

            if (!String.IsNullOrEmpty(brandname))
            {
                ViewBag.brandname = brandname;
                objlist = objlist.Where(x => x.brand_name == brandname).ToList();
            }

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                objlist = objlist.Where(x => x.size_inches == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                objlist = objlist.Where(x => x.file_type == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(cuttype))
            {
                ViewBag.cuttype = cuttype;
                objlist = objlist.Where(x => x.cut_type == cuttype).ToList();
            }

            modelobj.completedreqlist = objlist;
            Session["page"] = page;
            rmm = objlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_ItemMaster", rmm);
        }


        //Bind DDL for filter
        public ActionResult ItemMasterDDL()
        {
            ItemmasterModel objmodel = new ItemmasterModel();
            objmodel.SKUlist = get_SKU();
            objmodel.Cust_skulist = get_CustSKU();
            objmodel.custList = objBAL.getCustomerList();
            objmodel.brandList = objBAL.getBrandList();

            objmodel.filesizelist = getFileSize();
            objmodel.filetTypelist = getFileType();
            objmodel.CutTypelist = getFileCutType();
            return PartialView("_ItemMasterDDL", objmodel);
        }

        
        public List<FileSizeDetailsEntity> getFileSize()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileSize";
                dt = objBAL.getMasters(flag);
                List<FileSizeDetailsEntity> listfilespec = new List<FileSizeDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilespec = dt.AsEnumerable().Select(item => new FileSizeDetailsEntity
                    {
                        fs_size_inches = item.Field<decimal>("FileSize"),
                        fs_size_code = item.Field<string>("fs_size_code")
                    }).ToList();
                }

                // ViewData["FileSize"] = listfilespec.ToList();
                return listfilespec;
                // TempData["FileSize"] = listfilespec.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get fileType
        public List<FileTypeDataEntity> getFileType()
        {
            DataTable dt = new DataTable();
            List<FileTypeDataEntity> fileTypelist = new List<FileTypeDataEntity>();
            try
            {
                string flag = "getFileType";
                dt = objBAL.getMasters(flag);
                //List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    fileTypelist = dt.AsEnumerable().Select(item => new FileTypeDataEntity
                    {
                        ft_ftype_desc = item.Field<string>("FileType"),
                        ft_ftype_code = item.Field<string>("ft_ftype_code")
                    }).ToList();
                }
                return fileTypelist;
                //ViewData["FileType"] = listfilespec.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get fileCutType
        public List<CutTypeDetailsEntity> getFileCutType()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileCutType";
                dt = objBAL.getMasters(flag);
                List<CutTypeDetailsEntity> CutTypelist = new List<CutTypeDetailsEntity>();
                // List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    CutTypelist = dt.AsEnumerable().Select(item => new CutTypeDetailsEntity
                    {
                        fc_cut_type = item.Field<string>("CutType"),
                        fc_cut_code = item.Field<string>("fc_cut_code")

                    }).ToList();
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    FileSpec objFileSpec = new FileSpec();
                    //    objFileSpec.CutType = dt.Rows[i]["CutType"].ToString();
                    //    listfilespec.Add(objFileSpec);
                    //}
                }
                //ViewBag.CutType = listfilespec.ToList();
                return CutTypelist;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ItemmasterModel> get_SKU()
        {
            string flag = "sku_list";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            ItemmasterModel modelobj = new ItemmasterModel();
            List<ItemmasterModel> list = new List<ItemmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemmasterModel obj = new ItemmasterModel();
                    obj.SKU = (dt.Rows[i]["SKU"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<ItemmasterModel> get_CustSKU()
        {
            string flag = "cust_sku";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            ItemmasterModel modelobj = new ItemmasterModel();
            List<ItemmasterModel> list = new List<ItemmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemmasterModel obj = new ItemmasterModel();
                    obj.Cust_sku = (dt.Rows[i]["cust_sku"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        #endregion


        #region Requests by Dhanashree
        public ActionResult RequestDDL()
        {
            RequestsModel objmodel = new RequestsModel();
            objmodel.RequestNoLists = get_ReqNo();
            objmodel.CreatebyList = getCreateby();
            objmodel.StatusList = getStatusList();
            return PartialView("_RequestDDL",objmodel);
        }

        public ActionResult Requests(string req_no, string Createdby,string Status,
            string date,int? page1)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page1.HasValue ? Convert.ToInt32(page1) : 1;
            IPagedList<RequestsModel> rmm = null;
            RequestsModel objRequestModel = new RequestsModel();
            List<RequestsModel> requestlist = new List<RequestsModel>();
            //objRequestModel.RequestLists = GetRequests();
            requestlist = GetRequests();
            if (!String.IsNullOrEmpty(req_no))
            {
                ViewBag.req_no = req_no;
                requestlist = requestlist.Where(x => x.req_no == req_no).ToList();
            }

            if (!String.IsNullOrEmpty(Createdby))
            {
                ViewBag.Createdby = Createdby;
                requestlist = requestlist.Where(x => x.Createdby == Createdby).ToList();
            }

            if (!String.IsNullOrEmpty(Status))
            {
                ViewBag.Status = Status;
                requestlist = requestlist.Where(x => x.Status == Status).ToList();
            }
            if(!String.IsNullOrEmpty(date))
            {
                ViewBag.Date = date;
                string[] datesplit = date.Split('-');
                DateTime date1 =Convert.ToDateTime(datesplit[0]);
                DateTime date2 = Convert.ToDateTime(datesplit[1]);
                
                requestlist = requestlist.Where(x => Convert.ToDateTime(x.CreationDate) >= date1 &&
                Convert.ToDateTime(x.CreationDate) <= date2).ToList();
            }
            objRequestModel.RequestLists = requestlist;
            Session["page"] = page1;
            rmm = requestlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_Requests", rmm);
        }


        //public ActionResult Requests()
        //{
        //    RequestsModel objRequestModel = new RequestsModel();
        //    List<RequestsModel> requestlist = new List<RequestsModel>();
        //    objRequestModel.RequestLists = GetRequests();
        //    //requestlist = GetRequests();
        //    //if (!String.IsNullOrEmpty(req_no))
        //    //{
        //    //    ViewBag.req_no = req_no;
        //    //    requestlist = requestlist.Where(x => x.req_no == req_no).ToList();
        //    //}

        //    //if (!String.IsNullOrEmpty(Createdby))
        //    //{
        //    //    ViewBag.cust_sku = Createdby;
        //    //    requestlist = requestlist.Where(x => x.Createdby == Createdby).ToList();
        //    //}


        //    return PartialView("_Requests", objRequestModel);
        //}

        public List<RequestsModel> GetRequests()
        {
            string flag = "getRequests";
            DataTable dt = objBAL.getRequests(flag,"","","");
            List<RequestsModel> RequestList = new List<RequestsModel>();
            if (dt.Rows.Count>0 && dt!=null)
            {
                RequestList = dt.AsEnumerable().Select(item => new RequestsModel
                {
                    req_no=item.Field<string>("ReqNo"),
                    Createdby=item.Field<string>("pr_createby"),
                    CreationDate=item.Field<string>("CreateDate"),
                    Status=item.Field<string>("Status")
                }).ToList();
            }
            return RequestList;
        }        

        public List<RequestsModel> get_ReqNo()
        {
            string flag = "RequestNo";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            RequestsModel modelobj = new RequestsModel();
            List<RequestsModel> list = new List<RequestsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RequestsModel obj = new RequestsModel();
                    obj.req_no = (dt.Rows[i]["ReqNo"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }


        public List<RequestsModel> getCreateby()
        {
            string flag = "CreatedBy";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            RequestsModel modelobj = new RequestsModel();
            List<RequestsModel> list = new List<RequestsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RequestsModel obj = new RequestsModel();
                    obj.Createdby = (dt.Rows[i]["Createby"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<RequestsModel> getStatusList()
        {
            string flag = "Status";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            RequestsModel modelobj = new RequestsModel();
            List<RequestsModel> list = new List<RequestsModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RequestsModel obj = new RequestsModel();
                    obj.Status = (dt.Rows[i]["Status"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        #endregion
    }

}