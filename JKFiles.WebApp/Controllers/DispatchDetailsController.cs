﻿using JKFiles.WebApp.ActionFilters;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class DispatchDetailsController : Controller
    {
        BAL objBAL = new BAL();

        #region The Dispatch by Archana Mahajan.
        // GET: DispatchDetails
        public ActionResult DispatchIndex()
        {
            BAL objBAL = new BAL();
            jkfiles_dbEntities db = new jkfiles_dbEntities();
            DispatchSKU dispatchSKUModel = new DispatchSKU();
            HandlePresenceMasterModel handlePresenceMasterObj = new HandlePresenceMasterModel();
            HandleMasterModel handleMasterObj = new HandleMasterModel();
            string fileTypeCode = "";
            string FileSubType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            dispatchSKUModel.handleMasterList = objBAL.getHandleMasterList(fileTypeCode, FileSubType);
            dispatchSKUModel.handlePresenceList = objBAL.getHandelPresenceList();

            List<MasterReqParEntity> newList = new List<MasterReqParEntity>();
            dispatchSKUModel.ReqParlistEntity = newList;

            return View(dispatchSKUModel);
        }

        [HttpPost]
        public ActionResult DispatchIndex(DispatchSKU dispatchObj)
        {
            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["HandelDetailsTab"] = dispatchObj;
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            // Session["barcodeFlag"] = dispatchObj.dp_barcode_flg;
            dispatchEntity.dp_stamp_type = dispatchObj.dp_stamp_type;
            // Session["lineNOFlag"] = dispatchObj.dp_lineno_flg;
            //  Session["hologramFlag"] = dispatchObj.dp_hologram_flg;
            // Session["makeInIndiaFlag"] = dispatchObj.dp_india_flg;
            dispatchEntity.dp_handle_presence = dispatchObj.dp_handle_presence;
            dispatchEntity.dp_handle_type = dispatchObj.dp_handle_type;
            dispatchEntity.dp_handle_chart = dispatchObj.dp_handle_chart;
            if (dispatchEntity.dp_handle_presence == "WITHOUT HANDLE")
            {
                dispatchEntity.dp_handle_chart = "NoChart";
            }
            else
            {
                dispatchEntity.dp_handle_chart = dispatchEntity.dp_handle_chart;
                dispatchEntity.handle_chart = dispatchEntity.dp_handle_chart;
            }
            Session["HandleChartNo"] = dispatchEntity.dp_handle_chart;
            dispatchEntity.dp_handle_subtype = dispatchObj.dp_handle_subtype;
            objBAL.saveDispatchDetailsToDispatchSKU(dispatchEntity);
            //return RedirectToAction("WrapperTab");
            return RedirectToAction("getWrapperTabDetails");
        }

        #region back save For Handle By Archana Mahajan
        public ActionResult getHandelTabDetails()
        {
            DispatchSKU dispatchSKUModel = new DispatchSKU();


            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "handle_details";
            dispatchSKUModel.modellist = getEditHandle(ReqNo, flag);
            Session["HandelDetailsTab"] = dispatchSKUModel.modellist;
            #endregion


            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["HandelDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
            }

            if (Session["HandelDetailsTab"] != null && sessionObj.Count > 0)
            {
                dispatchSKUModel.handleMasterList = objBAL.getHandleMasterList(fileTypeCode, FileSubType);
                dispatchSKUModel.handlePresenceList = objBAL.getHandelPresenceList();
                dispatchSKUModel.dp_handle_presence = sessionObj[0].dp_handle_presence;
                if (dispatchSKUModel.dp_handle_presence == "WITHOUT HANDLE")
                {
                    dispatchSKUModel.dp_handle_chart = "NoChart";
                }
                else
                {
                    dispatchSKUModel.dp_handle_chart = sessionObj[0].dp_handle_chart;
                    dispatchSKUModel.handle_chart = sessionObj[0].dp_handle_chart;
                }
                dispatchSKUModel.dp_handle_type = sessionObj[0].dp_handle_type;
                dispatchSKUModel.dp_handle_subtype = sessionObj[0].dp_handle_subtype;
                return View("DispatchIndex", dispatchSKUModel);
            }
            return RedirectToAction("DispatchIndex");
        }


        #region Edit code handle -by Dhanashree
        public List<DispatchSKU> getEditHandle(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_handle_presence = item.Field<string>("dp_handle_presence"),
                    dp_handle_type = item.Field<string>("dp_handle_type"),
                    dp_handle_subtype = item.Field<string>("dp_handle_subtype"),
                    dp_handle_chart = item.Field<string>("dp_handle_chart")

                }).ToList();
            }
            return modellist;
        }
        #endregion

        #endregion

        #region get Handle Subtype by Dhanashree
        [HttpGet]
        public JsonResult getHandlesSubType(string handleType)
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }

            HandleMasterEntity hndleMasterObj = new HandleMasterEntity();

            hndleMasterObj.handleSubTypeList = objBAL.getHandleSubType(handleType, fileTypeCode, FileSubType, fileSize);

            //Session["handletypeCode"]= hndleMasterObj.handleSubTypeList[0].hm_handle_type;
            return Json(hndleMasterObj.handleSubTypeList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpGet]
        public JsonResult getHandlesChartNums(string handleType)
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }

            HandleMasterEntity hndleMasterObj = new HandleMasterEntity();
            hndleMasterObj.handleMasterList = objBAL.getHandleMasterList(handleType, fileTypeCode, FileSubType, fileSize);


            return Json(hndleMasterObj.handleMasterList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ViewHandleDrawing(string handleType)
        {
            HandleMasterModel handleMasterObj = new HandleMasterModel();
            HandleDetailsEntity handleDetailsObj = new HandleDetailsEntity();
            handleDetailsObj.hd_handle_id = objBAL.getHandleId(handleType);
            // handleMasterObj.handleDetailsList = objBAL.getHandleDrawingDetails(handleDetailsObj.hd_handle_id);

            handleDetailsObj = objBAL.getHandleDrawingDetails(handleDetailsObj.hd_handle_id);
            handleDetailsObj.tempChartNum = handleType;
            handleDetailsObj.HandleImage = objBAL.getHandleImageName(handleDetailsObj.tempChartNum);
            // handleDetailsObj = objBAL.getHandleDrawingDetails(handleType);
            HandleDetailsModel handleObj = new HandleDetailsModel();
            handleObj.HandleDetailsEntityList = handleDetailsObj.HandleDetailsEntityList;
            handleObj.HandleImage = handleDetailsObj.HandleImage;
            return PartialView("_ViewHandleDrawing", handleObj);
        }

        //wrapping tab
        public ActionResult WrapperTab()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "WRAPPER";
            dispatchObj.packingMasterialList = objBAL.getWrappingTypeList(usedFor);
            return PartialView("_WrapperTab", dispatchObj);
        }

        [HttpPost]
        public ActionResult WrapperTab(DispatchSKU dispatchModelObj)
        {
            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["WrapperDetailsTab"] = dispatchModelObj;
            dispatchEntity.dp_wrapping_type = dispatchModelObj.dp_wrapping_type;
            dispatchEntity.dp_wrapping_chart = dispatchModelObj.dp_wrapping_chart;
            if (dispatchEntity.dp_handle_presence == "WITHOUT HANDLE")
            {
                dispatchEntity.dp_handle_chart = "NoChart";
            }
            else
            {
                dispatchEntity.dp_handle_chart = dispatchModelObj.dp_wrapping_chart;

            }

            Session["WrapperChartNo"] = dispatchEntity.dp_wrapping_chart;
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            objBAL.saveWrapperTabDetails(dispatchEntity);
            //   return RedirectToAction("InnerBox");
            return RedirectToAction("getInnerBoxTabDetails");
        }

        #region back save For Wrapper By Archana Mahajan
        public ActionResult getWrapperTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();


            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "wrapper_details";
            dispatchObj.modellist = getEditWrapper(ReqNo, flag);
            Session["WrapperDetailsTab"] = dispatchObj.modellist;
            #endregion


            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["WrapperDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["WrapperDetailsTab"] != null && sessionObj.Count > 0)
            {

                string usedFor = "WRAPPER";
                dispatchObj.packingMasterialList = objBAL.getWrappingTypeList(usedFor);

                dispatchObj.dp_wrapping_type = sessionObj[0].dp_wrapping_type;
                dispatchObj.dp_wrapping_chart = sessionObj[0].dp_wrapping_chart;
                dispatchObj.wrapping_chart = sessionObj[0].dp_wrapping_chart;
                dispatchObj.dp_request_id = sessionObj[0].dp_request_id;
                dispatchObj.dp_request_type = sessionObj[0].dp_request_type;
                return PartialView("_WrapperTab", dispatchObj);
            }
            return RedirectToAction("WrapperTab");
        }
        #endregion


        #region Edit code Wrapper -by Dhanashree
        public List<DispatchSKU> getEditWrapper(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_wrapping_chart = item.Field<string>("dp_wrapping_chart"),
                    dp_wrapping_type = item.Field<string>("dp_wrapping_type"),
                    dp_wrapping_recid = item.Field<string>("dp_wrapping_recid"),
                    dp_request_id = item.Field<int>("dp_request_id"),
                    dp_request_type = item.Field<string>("dp_request_type")
                }).ToList();
            }
            return modellist;
        }
        #endregion


        [HttpGet]
        public JsonResult getWrappingChartNums(string wrappingType)
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if (Session["FileSubTypeCode"] != null)
            {
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["FileSizeCode"] != null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["FileCutTypeCode"] != null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
           
            string brandId = Session["BrandId"].ToString();
            string custId = Session["CoustomerId"].ToString();

            DispatchSKU obj = new DispatchSKU();
            obj.dp_wrapping_chartList = objBAL.getWrappingChartNums(wrappingType, fileTypeCode, FileSubType, fileSize, custId, brandId);


            return Json(obj.dp_wrapping_chartList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ViewWrappersDetails(string wrapperChart)
        {
            // wrapperChart = "PROV-02A";
            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();

            WrapperDetailsModel wrapperMasterObj = new WrapperDetailsModel();
            WrapperDetailsEntity wraperEntity = new WrapperDetailsEntity();
            wraperEntity.wd_wrapper_id = objBAL.getWrapperId(wrapperChart);

            wraperEntity.wrappperDetailsEntityList = objBAL.getViewWrappingDetails(wraperEntity.wd_wrapper_id);

            //wrapperEntity = objBAL.getWrapperChartDetails(handleDetailsObj.hd_handle_id);
            wrapperMasterObj.wrappperDetailsEntityList = wraperEntity.wrappperDetailsEntityList;
            wrapperMasterObj.FileType = Session["FileTypeCode"].ToString();
            wrapperMasterObj.FileSize = Session["FileSizeCode"].ToString();
            return PartialView("_ViewWrappersDetails", wrapperMasterObj);
        }


        public ActionResult InnerBox()
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "INNER BOX";
            dispatchObj.innerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
            dispatchObj.qtyInnerBoxList = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            //dispatchObj.dp_innerbox_id = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            return PartialView("_InnerBox", dispatchObj);
        }

        [HttpPost]
        public ActionResult InnerBox(DispatchSKU dispObj)
        {
            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["InnerBoxDetailsTab"] = dispObj;
            dispatchEntity.dp_innerbox_type = dispObj.dp_innerbox_type;
            dispatchEntity.dp_innerbox_chart = dispObj.dp_innerbox_chart;
            Session["InnerBoxNo"] = dispatchEntity.dp_innerbox_chart;
            dispatchEntity.dp_innerbox_qty = dispObj.dp_innerbox_qty;
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            objBAL.saveInnerBoxDetails(dispatchEntity);
            return RedirectToAction("InnerLabel");
            //return RedirectToAction("InnerLabelDetails");
        }

        #region back save For InnerBox By Archana Mahajan
        public ActionResult getInnerBoxTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "inner_box";
            dispatchObj.modellist = getEditInnerBox(ReqNo, flag);
            Session["InnerBoxDetailsTab"] = dispatchObj.modellist;
            #endregion


            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["InnerBoxDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["InnerBoxDetailsTab"] != null && sessionObj.Count > 0)
            {

                string usedFor = "INNER BOX";
                dispatchObj.innerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
                dispatchObj.qtyInnerBoxList = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);

                dispatchObj.dp_innerbox_type = sessionObj[0].dp_innerbox_type;
                dispatchObj.dp_innerbox_chart = sessionObj[0].ib_chartnum;
                dispatchObj.innerbox_chart = sessionObj[0].ib_chartnum;
                dispatchObj.dp_innerbox_qty = sessionObj[0].dp_innerbox_qty;
                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_InnerBox", dispatchObj);
            }
            return RedirectToAction("InnerBox");
        }
        #endregion

        #region Edit code InnerBox -by Dhanashree
        public List<DispatchSKU> getEditInnerBox(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    ib_chartnum = item.Field<string>("dp_innerbox_chart"),
                    dp_innerbox_type = item.Field<string>("dp_innerbox_type"),
                    dp_innerbox_id = item.Field<string>("dp_innerbox_id")
                }).ToList();
            }
            return modellist;
        }
        #endregion
        [HttpGet]
        public ActionResult ViewInnerBoxDetails(string InnerBoxChart)
        {

            //InnerBoxChart = "CardBoard";
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }
            InnerBoxDetilsModel obj = new InnerBoxDetilsModel();
            InnerBoxDetailsEntity entityEntity = new InnerBoxDetailsEntity();
            entityEntity.ibd_box_recid = InnerBoxChart;

            entityEntity.InnerBoxDetailsEntityList = objBAL.getInnerBoxDetails(entityEntity.ibd_box_recid);
            if (InnerBoxChart != "" || InnerBoxChart != null)
            {
                obj.innerboxlist = objBAL.getinnerboxlist(InnerBoxChart, FileSubType, fileTypeCode, fileSize);
            }

            obj.InnerBoxDetailsEntityList = entityEntity.InnerBoxDetailsEntityList;
            obj.FileSize = fileSize;
            obj.FileType = fileTypeCode;
            //wrapperEntity = objBAL.getWrapperChartDetails(handleDetailsObj.hd_handle_id);
            //wrapperMasterObj.wrappperDetailsEntityList = wraperEntity.wrappperDetailsEntityList;
            //wrapperMasterObj.FileType = Session["FileTypeCode"].ToString();
            //wrapperMasterObj.FileSize = Session["FileSizeCode"].ToString();
            return PartialView("_ViewInnerBoxDetails", obj);
        }


        [HttpGet]
        public JsonResult getInnerBoxChartNums(string innerBoxMaterial, string innerQuntity)
        {
            //string fileTypeCode = Session["FileTypeCode"].ToString();
            //string FileSubType = Session["FileSubTypeCode"].ToString();
            //string fileSize = Session["FileSizeCode"].ToString();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if (Session["FileSubTypeCode"] != null)
            {
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["FileSizeCode"] != null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["FileCutTypeCode"] != null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            DispatchSKU obj = new DispatchSKU();
            obj.innerBoxChartNUmsList = objBAL.getInnerBoxChartNums(innerBoxMaterial, innerQuntity, fileTypeCode, FileSubType, fileSize);


            return Json(obj.innerBoxChartNUmsList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InnerLabel()
        {
            return PartialView("_InnerLabel");
        }

        [HttpGet]
        public ActionResult InnerLabelDetails()
        {
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            InnerLabelDetailsModel detailsModel = new InnerLabelDetailsModel();
            DispatchSKU disPackObj = new DispatchSKU();
            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null
                && Session["BrandId"] != null && Session["CoustomerId"] != null && Session["ShipToCountryCode"] != null &&
                Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
            Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
            if(detailsEntity.ilbd_chart_num =="" || detailsEntity.ilbd_chart_num == null)
            {
                detailsEntity.ilbd_chart_num = "No Chart";
            }
            
            detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);
            detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
            disPackObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;

            return PartialView("_InnerLabelDetails", disPackObj);
        }
        [HttpPost]
        public ActionResult InnerLabel(DispatchSKU disObj)
        {

            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["InnerLabelDetailsTab"] = disObj;
            dispatchEntity.dp_innerlabel_chart = Session["InnerLabelChartNum"].ToString();
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            objBAL.saveInnerLabel(dispatchEntity);
            //return RedirectToAction("Outer");
            return RedirectToAction("getOuterTabDetails");
        }

        #region back save For InnerLabel By Archana Mahajan
        public ActionResult getInnerLabelTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            DispatchSKU sessionObj = (DispatchSKU)Session["InnerLabelDetailsTab"];
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            InnerLabelDetailsModel detailsModel = new InnerLabelDetailsModel();

            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();

            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null
               && Session["BrandId"] != null && Session["CoustomerId"] != null && Session["ShipToCountryCode"] != null &&
               Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = Session["FileCutTypeCode"].ToString();
            }

            if (Session["InnerLabelDetailsTab"] != null)
            {


                detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
                Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
                detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);
                detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
                dispatchObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;


                dispatchObj.dp_innerlabel_chart = Session["InnerLabelChartNum"].ToString();
                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_InnerLabel", dispatchObj);
            }
            return RedirectToAction("InnerLabel");
        }
        #endregion

        public ActionResult Outer()
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "outer";
            dispatchObj.outerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
            //dispatchObj.qtyInnerBoxList = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            //dispatchObj.dp_innerbox_id = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            return PartialView("_Outer", dispatchObj);
        }
        [HttpGet]
        public JsonResult getOuterChartNums(string dp_outerbox_type)
        {

            DispatchSKU obj = new DispatchSKU();
            obj.outerChartNumslist = objBAL.getOuterBoxChartNums(dp_outerbox_type);


            return Json(obj.outerChartNumslist, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Outer(DispatchSKU disObj)
        {
            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["OuterDetailsTab"] = disObj;
            dispatchEntity.dp_outerbox_chart = disObj.dp_outerbox_chart;
            Session["OuterBoxChartNo"] = dispatchEntity.dp_outerbox_chart;

            dispatchEntity.dp_outerbox_type = disObj.dp_outerbox_type;

            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            objBAL.saveOuterDetails(dispatchEntity);
            //return RedirectToAction("Pallet");
            return RedirectToAction("getPalletTabDetails");
        }

        #region back save For Outer By Archana Mahajan
        public ActionResult getOuterTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();


            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "outer_box";
            dispatchObj.modellist = getEditOuterBox(ReqNo, flag);
            Session["OuterDetailsTab"] = dispatchObj.modellist;
            #endregion

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["OuterDetailsTab"];
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
            }

            if (Session["OuterDetailsTab"] != null && sessionObj.Count > 0)
            {

                string usedFor = "outer";
                dispatchObj.outerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);

                dispatchObj.dp_outerbox_chart = sessionObj[0].dp_outerbox_chart;
                dispatchObj.outerbox_chart = sessionObj[0].dp_outerbox_chart;
                dispatchObj.dp_outerbox_type = sessionObj[0].dp_outerbox_type;

                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_Outer", dispatchObj);
            }
            return RedirectToAction("Outer");
        }
        #endregion

        #region Edit code OuterBox -by Dhanashree
        public List<DispatchSKU> getEditOuterBox(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_outerbox_chart = item.Field<string>("dp_outerbox_chart"),
                    dp_outerbox_type = item.Field<string>("dp_outerbox_type")
                }).ToList();
            }
            return modellist;
        }
        #endregion

        [HttpGet]
        public ActionResult ViewOuterDetails(string OuterChart)
        {
            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();

            OuterDetailsModel obj = new OuterDetailsModel();
            OuterDetailsEntity entity = new OuterDetailsEntity();
            obj.obd_box_id = objBAL.getOuterBoxId(OuterChart);

            entity.outerDeatialsEntityList = objBAL.getOuterDetials(obj.obd_box_id);
            if (OuterChart != "" || OuterChart != null)
            {
                obj.OterChartList = objBAL.GetOuterChartlist(OuterChart);
            }

            obj.outerDeatialsEntityList = entity.outerDeatialsEntityList;
            obj.filesize = fileSize;
            obj.filetype = fileTypeCode;
            obj.filesubtype = FileSubType;

            return PartialView("_ViewOuterDetails", obj);
        }

        public ActionResult Pallet()
        {
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
            }
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "PALLET";
            dispatchObj.palletMaterialList = objBAL.getWrappingTypeList(usedFor);
            return PartialView("_Pallet", dispatchObj);
        }

        [HttpGet]
        public JsonResult getPalletChartNums(string dp_pallet_type)
        {
            DispatchSKU obj = new DispatchSKU();
            obj.palletChartNumslist = objBAL.getPalletChartNums(dp_pallet_type);
            return Json(obj.palletChartNumslist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Pallet(DispatchSKU disObj)
        {
            string dispatchSKU;
            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["PalletTab"] = disObj;
            dispatchEntity.dp_pallet_chart = disObj.dp_pallet_chart;
            Session["PalletChartNo"] = dispatchEntity.dp_pallet_chart;
            dispatchEntity.dp_pallet_type = disObj.dp_pallet_type;
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            objBAL.savePalletDetails(dispatchEntity);
            string itemNum;
            if (Session["BrandId"] != null && Session["FileSizeCode"] != null && Session["Filecode"] != null && Session["FileCutTypeCode"] != null && Session["CutSpecification"] != null && Session["CoustomerId"] != null)
            {
                itemNum = Session["BrandId"].ToString() + Session["FileSizeCode"].ToString() + Session["Filecode"].ToString() + Session["FileCutTypeCode"].ToString() + Session["CutSpecification"].ToString() + Session["CoustomerId"].ToString();
            }
            else
            {
                itemNum = "SomeSessionIsExpired";
            }

            if (Session["HandleChartNo"] != null && Session["WrapperChartNo"] != null &&
                Session["InnerBoxNo"] != null && Session["InnerLabelChartNum"] != null &&
                Session["OuterBoxChartNo"] != null && Session["PalletChartNo"] != null
                && Session["currentStampChartNum"]!=null)
            {
                dispatchSKU = Session["HandleChartNo"].ToString() + Session["WrapperChartNo"].ToString() +
                   Session["InnerBoxNo"].ToString() + Session["InnerLabelChartNum"].ToString() +
                   Session["OuterBoxChartNo"].ToString() + Session["PalletChartNo"].ToString() +
                    Session["currentStampChartNum"].ToString();
            }
            else
            {
                dispatchSKU = "SomeSessionIsExpiered";
            }

            ItemPartNumModel itemNumObj = new ItemPartNumModel();
            itemNumObj.requestId = Convert.ToInt32(Session["requestId"]);
            // Session["CurrentRequestId"] = reqEntity.requestId;
            itemNumObj.requestType = Session["requestype"].ToString();
            //Session["CurrentRequestType"] = reqEntity.requestType;
            // itemNumObj.itemNum = dispatchSKU;
            FileSpec objfile = new FileSpec();
            ItemPartNumEntity itemNumEntity = new ItemPartNumEntity();
            itemNumEntity.itemNum = itemNum;
            itemNumEntity.requestId = itemNumObj.requestId;
            itemNumEntity.requestType = itemNumObj.requestType;
            itemNumEntity.dispSKU = dispatchSKU;
            int result = objBAL.UpdatedispatchSKU(itemNumEntity);
            if (result == 0)
            {
                TempData["Message"] = "MyMessage";
                objBAL.DeactiveRequest(itemNumEntity);
                return RedirectToAction("getPalletTabDetails");
            }
            else
            {
                //string imagename = objfile.filesizecode + 
                //    objfile.filetypecode + objfile.filesubtypecode + 
                //    cuttypecode + cutstandard;
                string imagename = Session["FileSizeCode"].ToString() +
                    Session["FileTypeCode"] + Session["FileSubTypeCode"] +
                    Session["FileCutTypeCode"].ToString() + Session["CutSpecification"].ToString();
                objfile.dwg_prod_sku = imagename;
                objfile.dwg_imgname = imagename + ".jpeg";
                objfile.dwg_pdfname = imagename + ".pdf";
                objfile.dwg_dxfname = imagename + ".dxf";
                objfile.flag = "insert_drawing_library";
                objfile.ReqNo = Session["requestype"].ToString() + Convert.ToInt32(Session["requestId"]);
                objBAL.get_DrawingValues(objfile);
            }
            return RedirectToAction("PackingDetails");
        }

        #region back save For Pallet By Archana Mahajan
        public ActionResult getPalletTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            string Message = "";
            if (TempData["Message"] != null)
            {
                ViewBag.Message = "Success";
                Message = "Success";
                TempData.Remove("Message");
            }

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "pallet_details";
            dispatchObj.modellist = getEditPallet(ReqNo, flag);
            Session["PalletTab"] = dispatchObj.modellist;
            #endregion

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["PalletTab"];
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["PalletTab"] != null && sessionObj.Count > 0)
            {
                string usedFor = "PALLET";
                dispatchObj.palletMaterialList = objBAL.getWrappingTypeList(usedFor);

                dispatchObj.dp_pallet_chart = sessionObj[0].dp_pallet_chart;
                dispatchObj.pallet_chart = sessionObj[0].dp_pallet_chart;
                dispatchObj.dp_pallet_type = sessionObj[0].dp_pallet_type;

                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                dispatchObj.Message = Message;
                return PartialView("_Pallet", dispatchObj);

            }
            return RedirectToAction("Pallet");
        }
        #endregion


        #region Edit code Pallet -by Dhanashree
        public List<DispatchSKU> getEditPallet(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_pallet_chart = item.Field<string>("dp_pallet_chart"),
                    dp_pallet_type = item.Field<string>("dp_pallet_type")
                }).ToList();
            }
            return modellist;
        }
        #endregion

        public ActionResult PackingDetails()
        {
            //return View("_PackingDetails");
            DispatchSKU disObj = new DispatchSKU();

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "packing_details";
            disObj.modellist = getEditPackingDetails(ReqNo, flag);
            Session["PackingDetailsTab"] = disObj.modellist;
            #endregion

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["PackingDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["PackingDetailsTab"] != null && sessionObj.Count > 0)
            {
                disObj.temp_barcode_flg = sessionObj[0].dp_barcode_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_barcodeouter_flg = sessionObj[0].dp_barcodeouter_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_fumigation_flg = sessionObj[0].dp_fumigation_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_hologram_flg = sessionObj[0].dp_hologram_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_india_flg = sessionObj[0].dp_india_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_lineno_flg = sessionObj[0].dp_lineno_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_polybag_flg = sessionObj[0].dp_polybag_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_prcstkr_flg = sessionObj[0].dp_prcstkr_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_qltyseal_flg = sessionObj[0].dp_qltyseal_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_silicagel_flg = sessionObj[0].dp_silicagel_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_promotionalitem_flg = sessionObj[0].dp_promotionalitem_flg.ToString() == "True" ? "Yes" : "No";
                if (sessionObj[0].dp_strap != null)
                {
                    disObj.temp_strap = sessionObj[0].dp_strap.ToString();
                }
                else
                {
                    disObj.temp_strap = "Yes";
                }
                disObj.temp_cellotape_flg = sessionObj[0].dp_cellotape_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_shrinkwrap_flg = sessionObj[0].dp_shrinkwrap_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_decicant_flg = sessionObj[0].dp_decicant_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_paperwool_flg = sessionObj[0].dp_paperwool_flg.ToString() == "True" ? "Yes" : "No";
                if (disObj.dp_outer_mrking_type != null)
                {
                    disObj.dp_outer_mrking_type = sessionObj[0].dp_outer_mrking_type.ToString();
                }
                else
                {
                    disObj.dp_outer_mrking_type = "No";
                }
                disObj.temp_pono_flg = sessionObj[0].dp_pono_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_blko_flg = sessionObj[0].dp_blko_flg.ToString() == "True" ? "Yes" : "No";
                if(sessionObj[0].dp_tangcolor!=null)
                {
                    disObj.temp_tangcolor = sessionObj[0].dp_tangcolor.ToString();
                }
                if (sessionObj[0].dp_pkg_remarks != null)
                {
                    disObj.temp_pkg_remarks = sessionObj[0].dp_pkg_remarks.ToString();
                }
                return PartialView("_PackingDetails", disObj);

            }
            return RedirectToAction("PackingDetails");
        }

        [HttpPost]
        public ActionResult PackingDetails(DispatchSKU disObj)
        {

            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["PackingDetailsTab"] = disObj;
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();
            #region packing Flags
            if (disObj.temp_barcode_flg == "Yes")
            {
                dispatchEntity.dp_barcode_flg = true;
            }
            else dispatchEntity.dp_barcode_flg = false;
            if (disObj.temp_barcodeouter_flg == "Yes")
            {
                dispatchEntity.dp_barcodeouter_flg = true;
            }
            else dispatchEntity.dp_barcodeouter_flg = false;
            
            if (disObj.temp_hologram_flg == "Yes")
            {
                dispatchEntity.dp_hologram_flg = true;
            }
            else dispatchEntity.dp_hologram_flg = false;
            if (disObj.temp_fumigation_flg == "Yes")
            {
                dispatchEntity.dp_fumigation_flg = true;
            }
            else dispatchEntity.dp_fumigation_flg = false;
            if (disObj.temp_india_flg == "Yes")
            {
                dispatchEntity.dp_india_flg = true;
            }
            else dispatchEntity.dp_india_flg = false;
            if (disObj.temp_lineno_flg == "Yes")
            {
                dispatchEntity.dp_lineno_flg = true;
            }
            else dispatchEntity.dp_lineno_flg = false;
            if (disObj.temp_polybag_flg == "Yes")
            {
                dispatchEntity.dp_polybag_flg = true;
            }
            else dispatchEntity.dp_polybag_flg = false;
            if (disObj.temp_prcstkr_flg == "Yes")
            {
                dispatchEntity.dp_prcstkr_flg = true;
            }
            else dispatchEntity.dp_prcstkr_flg = false;
            if (disObj.temp_qltyseal_flg == "Yes")
            {
                dispatchEntity.dp_qltyseal_flg = true;
            }
            else dispatchEntity.dp_qltyseal_flg = false;
            if (disObj.temp_silicagel_flg == "Yes")
            {
                dispatchEntity.dp_silicagel_flg = true;
            }
            else dispatchEntity.dp_silicagel_flg = false;
            if (disObj.temp_promotionalitem_flg == "Yes")
                dispatchEntity.dp_promo_flg = true;
            else
                dispatchEntity.dp_promo_flg = false;
            if (disObj.temp_strap == "Yes")
                dispatchEntity.dp_strap_flg = disObj.temp_strap;
            else
                dispatchEntity.dp_strap_flg = disObj.temp_strap;
            if (disObj.temp_cellotape_flg == "Yes")
                dispatchEntity.dp_cellotape_flg = true;
            else
                dispatchEntity.dp_cellotape_flg = false;
            if (disObj.temp_shrinkwrap_flg == "Yes")
                dispatchEntity.dp_shrinkwrap_flg = true;
            else dispatchEntity.dp_shrinkwrap_flg = false;
            if (disObj.temp_decicant_flg == "Yes")
                dispatchEntity.dp_decicant_flg = true;
            else
                dispatchEntity.dp_decicant_flg = false;
            if (disObj.temp_paperwool_flg == "Yes")
                dispatchEntity.dp_paperwool_flg = true;
            else
                dispatchEntity.dp_paperwool_flg = false;

            #region code added for Outer/Pallet Marking by Sanket

            dispatchEntity.dp_outer_mrking_type = disObj.dp_outer_mrking_type;
            if (dispatchEntity.dp_outer_mrking_type == "No")
            {
                dispatchEntity.dp_outer_mrking_img = "NA.jpg";
            }
            else
            {
                HttpPostedFileBase photo = Request.Files["outer"];
                if (photo != null)
                {
                    dispatchEntity.dp_outer_mrking_img = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/PackingDetails/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        //   string file = filename.Replace(filename, stampObj.sm_chart_num);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename + ".jpg"));
                    }
                    // dispatchEntity.dp_outer_mrking_img = disObj.dp_outer_mrking_img;
                }
            }


            dispatchEntity.dp_pallet_mrking_type = disObj.dp_pallet_mrking_type;
            if (dispatchEntity.dp_pallet_mrking_type == "No")
            {
                dispatchEntity.dp_pallet_mrking_img = "NA.jpg";
            }
            else
            {
                HttpPostedFileBase photo = Request.Files["pallet"];
                if (photo != null)
                {
                    dispatchEntity.dp_pallet_mrking_img = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/PackingDetails/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        //   string file = filename.Replace(filename, stampObj.sm_chart_num);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename + ".jpg"));
                    }
                    // dispatchEntity.dp_outer_mrking_img = disObj.dp_outer_mrking_img;
                }
            }

            #endregion

            //}
            #endregion
            if (disObj.temp_blko_flg == "Yes")
                dispatchEntity.dp_blko_flg = true;
            else
                dispatchEntity.dp_blko_flg = false;
            if (disObj.temp_pono_flg == "Yes")
                dispatchEntity.dp_pono_flg = true;
            else
                dispatchEntity.dp_pono_flg = false;
          
            dispatchEntity.dp_pkg_remarks = disObj.temp_pkg_remarks;
            dispatchEntity.dp_tangcolor = disObj.temp_tangcolor;
            objBAL.savePackingDetailsToDispatchSKU(dispatchEntity);
            return RedirectToAction("LabelLayOut");
        }





        #region back save For PackingDetails By Archana Mahajan
        public ActionResult getPackingDetailsTab()
        {
            DispatchSKU disObj = new DispatchSKU();

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "packing_details";
            disObj.modellist = getEditPackingDetails(ReqNo, flag);
            Session["PackingDetailsTab"] = disObj.modellist;
            #endregion

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["PackingDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["PackingDetailsTab"] != null && sessionObj.Count > 0)
            {
                disObj.temp_barcode_flg = sessionObj[0].dp_barcode_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_barcodeouter_flg = sessionObj[0].dp_barcodeouter_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_fumigation_flg = sessionObj[0].dp_fumigation_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_hologram_flg = sessionObj[0].dp_hologram_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_india_flg = sessionObj[0].dp_india_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_lineno_flg = sessionObj[0].dp_lineno_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_polybag_flg = sessionObj[0].dp_polybag_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_prcstkr_flg = sessionObj[0].dp_prcstkr_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_qltyseal_flg = sessionObj[0].dp_qltyseal_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_silicagel_flg = sessionObj[0].dp_silicagel_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_promotionalitem_flg = sessionObj[0].dp_promotionalitem_flg.ToString() == "True" ? "Yes" : "No";
                if (sessionObj[0].dp_strap != null)
                {
                    disObj.temp_strap = sessionObj[0].dp_strap.ToString();
                }
                else
                {
                    disObj.temp_strap = "Yes";
                }
                disObj.temp_cellotape_flg = sessionObj[0].dp_cellotape_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_shrinkwrap_flg = sessionObj[0].dp_shrinkwrap_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_decicant_flg = sessionObj[0].dp_decicant_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_paperwool_flg = sessionObj[0].dp_paperwool_flg.ToString() == "True" ? "Yes" : "No";
                if (disObj.dp_outer_mrking_type != null)
                {
                    disObj.dp_outer_mrking_type = sessionObj[0].dp_outer_mrking_type.ToString();
                }
                else
                {
                    disObj.dp_outer_mrking_type = "No";
                }
                disObj.temp_pono_flg = sessionObj[0].dp_pono_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_blko_flg = sessionObj[0].dp_blko_flg.ToString() == "True" ? "Yes" : "No";
                if (sessionObj[0].dp_tangcolor != null)
                {
                    disObj.temp_tangcolor = sessionObj[0].dp_tangcolor.ToString();
                }
                if (sessionObj[0].dp_pkg_remarks != null)
                {
                    disObj.temp_pkg_remarks = sessionObj[0].dp_pkg_remarks.ToString();
                }
                return PartialView("_PackingDetails", disObj);

            }
            return RedirectToAction("PackingDetails");
        }
        #endregion

        #region Edit code Packing Details -by Dhanashree
        public List<DispatchSKU> getEditPackingDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_barcode_flg = item.Field<Boolean?>("dp_barcode_flg"),
                    dp_barcodeouter_flg= item.Field<Boolean?>("dp_barcodeouter_flg"),
                    dp_fumigation_flg = item.Field<Boolean?>("dp_fumigation_flg"),
                    dp_hologram_flg = item.Field<Boolean?>("dp_hologram_flg"),
                    dp_india_flg = item.Field<Boolean?>("dp_india_flg"),
                    dp_lineno_flg = item.Field<Boolean?>("dp_lineno_flg"),
                    dp_polybag_flg = item.Field<Boolean?>("dp_polybag_flg"),
                    dp_prcstkr_flg = item.Field<Boolean?>("dp_prcstkr_flg"),
                    dp_qltyseal_flg = item.Field<Boolean?>("dp_qltyseal_flg"),
                    dp_silicagel_flg = item.Field<Boolean?>("dp_silicagel_flg"),
                    dp_promotionalitem_flg = item.Field<Boolean?>("dp_promo_item_flg"),
                    dp_strap = item.Field<string>("dp_strap"),
                    dp_cellotape_flg = item.Field<Boolean?>("dp_cellotape_flg"),
                    dp_shrinkwrap_flg = item.Field<Boolean?>("dp_shrinkwrap_flg"),
                    dp_decicant_flg = item.Field<Boolean?>("dp_decicant_flg"),
                    dp_paperwool_flg = item.Field<Boolean?>("dp_paperwool_flg"),
                    dp_stamp_type = item.Field<string>("dp_stamp_type"),
                    dp_stamp_chart = item.Field<string>("dp_stamp_chart"),
                    tang_tempering = item.Field<string>("pr_tang_color"),
                    dp_outer_mrking_type = item.Field<string>("dp_outer_mrking_type"),
                    dp_pallet_mrking_type = item.Field<string>("dp_pallet_mrking_type"),

                    dp_pono_flg = item.Field<Boolean?>("dp_pono_flg"),
                    dp_tangcolor = item.Field<string>("dp_tang_color"),
                    dp_blko_flg = item.Field<Boolean?>("dp_blko_flg"),
                    dp_pkg_remarks = item.Field<string>("dp_pkg_remark")




                }).ToList();
            }
            return modellist;
        }
        #endregion

        public ActionResult LabelLayOut()
        {
            return PartialView("_LabelLayOut");
        }

        [HttpPost]
        public ActionResult LabelLayOut(DispatchSKU disObj)
        {
            return RedirectToAction("getOtherDetailsTab");
        }


        [HttpGet]
        public ActionResult LabelLayOutDetails()
        {
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            InnerLabelDetailsModel detailsModel = new InnerLabelDetailsModel();
            DispatchSKU disPackObj = new DispatchSKU();
            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            CustomerMasterModel sessionObj = (CustomerMasterModel)Session["CustomerDetailsTab"];
            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "packing_details";
            disPackObj.modellist = getEditPackingDetails(ReqNo, flag);
            Session["PackingDetailsTab"] = disPackObj.modellist;

            List<DispatchSKU> sessionObj1 = (List<DispatchSKU>)Session["PackingDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null
                && Session["BrandId"] != null && Session["CoustomerId"] != null && Session["ShipToCountryCode"] != null
                && Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = Session["FileCutTypeCode"].ToString();
            }

            detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
            Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;

            if(detailsEntity.ilbd_chart_num=="" || detailsEntity.ilbd_chart_num == null)
            {
                detailsEntity.ilbd_chart_num = "No Chart";
            }
            detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);



            detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
            disPackObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;
            disPackObj.CustomerName = sessionObj.costomerName;
            disPackObj.BrandName = sessionObj.brandName;
            disPackObj.temp_barcode_flg = sessionObj1[0].dp_barcode_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_barcodeouter_flg = sessionObj1[0].dp_barcodeouter_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_fumigation_flg = sessionObj1[0].dp_fumigation_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_hologram_flg = sessionObj1[0].dp_hologram_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_india_flg = sessionObj1[0].dp_india_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_lineno_flg = sessionObj1[0].dp_lineno_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_polybag_flg = sessionObj1[0].dp_polybag_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_prcstkr_flg = sessionObj1[0].dp_prcstkr_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_qltyseal_flg = sessionObj1[0].dp_qltyseal_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_silicagel_flg = sessionObj1[0].dp_silicagel_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_promotionalitem_flg = sessionObj1[0].dp_promotionalitem_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_strap = sessionObj1[0].dp_strap.ToString();
            disPackObj.temp_cellotape_flg = sessionObj1[0].dp_cellotape_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_shrinkwrap_flg = sessionObj1[0].dp_shrinkwrap_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_decicant_flg = sessionObj1[0].dp_decicant_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_paperwool_flg = sessionObj1[0].dp_paperwool_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.dp_stamp_type = sessionObj1[0].dp_stamp_type.ToString();
            disPackObj.dp_stamp_chart = sessionObj1[0].dp_stamp_chart.ToString();
            disPackObj.tang_tempering = sessionObj1[0].tang_tempering.ToString();
            disPackObj.dp_outer_mrking_type = sessionObj1[0].dp_outer_mrking_type.ToString();
            disPackObj.dp_pallet_mrking_type = sessionObj1[0].dp_pallet_mrking_type.ToString();
            //disPackObj.dp_outer_mrking_img = sessionObj1[0].dp_outer_mrking_img.ToString();
           // disPackObj.dp_pallet_mrking_img = sessionObj1[0].dp_pallet_mrking_img.ToString();
            #region All Images Added by Dhanashree 
            DataTable dt = new DataTable();
            if (Convert.ToInt32(Session["requestId"]) != 0)
            {
                dt = objBAL.getdispatchdetails(Convert.ToInt32(Session["requestId"]));
            }
            if (dt.Rows.Count > 0)
            {
                disPackObj.dp_handle_chart = dt.Rows[0]["dp_handle_chart"].ToString();
                disPackObj.dp_wrapping_chart = dt.Rows[0]["dp_wrapping_chart"].ToString();
                disPackObj.dp_innerbox_chart = dt.Rows[0]["dp_innerbox_chart"].ToString();
                disPackObj.dp_outerbox_chart = dt.Rows[0]["dp_outerbox_chart"].ToString();
                disPackObj.dp_pallet_chart = dt.Rows[0]["dp_pallet_chart"].ToString();

                disPackObj.dp_outer_mrking_img = dt.Rows[0]["dp_outer_mrking_img"].ToString();
                disPackObj.dp_pallet_mrking_img = dt.Rows[0]["dp_pallet_mrking_img"].ToString();

                if (disPackObj.dp_handle_chart != "" || disPackObj.dp_handle_chart != null)
                {
                    disPackObj.HandleDetailsEntityList = objBAL.gethandleimgpath(disPackObj.dp_handle_chart);
                }
                if (disPackObj.dp_wrapping_chart != "" || disPackObj.dp_wrapping_chart != null)
                {
                    disPackObj.WrapperdetailsList = objBAL.getwrappinglist(disPackObj.dp_wrapping_chart);
                }
                if (disPackObj.dp_innerbox_chart != "" || disPackObj.dp_innerbox_chart != null)
                {
                    disPackObj.innerboxlist = objBAL.getinnerboxlist(disPackObj.dp_innerbox_chart, FileSubType, fileTypeCode, fileSize);
                }
                if (disPackObj.dp_outerbox_chart != "" || disPackObj.dp_outerbox_chart != null)
                {
                    disPackObj.OterChartList = objBAL.GetOuterChartlist(disPackObj.dp_outerbox_chart);
                }
                if (disPackObj.dp_pallet_chart != "" || disPackObj.dp_pallet_chart != null)
                {
                    disPackObj.PalletList = objBAL.GetPalletList(disPackObj.dp_pallet_chart);
                }

                //if (disPackObj.dp_outer_mrking_img != "" || disPackObj.dp_outer_mrking_img != null)
                //{
                //    disPackObj.OuterList = objBAL.GetPalletList(disPackObj.dp_outer_mrking_img);
                //}
            }

            #endregion


            return PartialView("_LabelLayOutDetails", disPackObj);
        }


        [HttpGet]
        public ActionResult ViewPalletDetails(string pallet_chart)
        {
            PalletDetailsModel palletDetailsModelObj = new PalletDetailsModel();

            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();


            PalletDetailsEntity entity = new PalletDetailsEntity();

            entity.pd_pallet_id = objBAL.getPalletId(pallet_chart);
            entity.palletDetailsEntityList = objBAL.getPalletDetials(entity.pd_pallet_id);

            palletDetailsModelObj.pm_pallet_chartimg = objBAL.getPalletImageName(pallet_chart);

            palletDetailsModelObj.palletDetailsEntityList = entity.palletDetailsEntityList;
            palletDetailsModelObj.filesize = fileSize;
            palletDetailsModelObj.filetype = fileTypeCode;
            palletDetailsModelObj.filesubtype = FileSubType;

            return PartialView("_PalletDetailsView", palletDetailsModelObj);
        }



        public ActionResult OtherDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OtherDetails(PartNumOtherDetailsModel partObj, FormCollection form)
        {
            Session["OtherDetailsTab"] = partObj;
            string button = form["actionType"].ToString();
            Session["OtherDetailsTab"] = partObj;
            PartNumOtherDetailsEntity partEntity = new PartNumOtherDetailsEntity();
            partEntity.othdt_request_id = Convert.ToInt32(Session["requestId"]);
            partEntity.othdt_request_type = Session["requestype"].ToString();
            partEntity.othdt_prod_cost_onetm = partObj.othdt_prod_cost_onetm;
            partEntity.othdt_prod_cost_run = partObj.othdt_prod_cost_run;
            partEntity.othdt_dispatch_onetm = partObj.othdt_dispatch_onetm;
            partEntity.othdt_dispatch_run = partObj.othdt_dispatch_run;
            partEntity.othdt_req_startdt = partObj.othdt_req_startdt;
            partEntity.othdt_req_enddt = partObj.othdt_req_enddt;

            if (button == "Save")
            {

                if (form["selectradio"] != null)
                {
                    string radio = form["selectradio"].ToString();
                    if (radio == "NO")
                    {
                        objBAL.CreateCloneReq(null, Convert.ToInt32(Session["requestId"]), "clone_with_new");
                        partEntity.othdt_request_type = "N";
                        Session["requestype"] = "N";
                    }
                    else if (radio == "YES")
                    {
                        partEntity.othdt_req_enddt_current_sku = partObj.othdt_req_enddt_current_sku;
                    }
                }
                objBAL.saveOtherDetails(partEntity);
                return RedirectToAction("getOtherDetailsTab");

            }
            else if (button == "Sub")
            {
                //if (form["selectradio"] != null)
                //{
                //    string radio = form["selectradio"].ToString();
                //    if (radio == "NO")
                //    {
                //        objBAL.CreateCloneReq(null, Convert.ToInt32(Session["requestId"]), "clone_with_new");
                //        partEntity.othdt_request_type = "N";
                //        Session["requestype"] = "N";
                //    }
                //    else if (radio == "YES")
                //    {
                //        partEntity.othdt_req_enddt_current_sku = partObj.othdt_req_enddt_current_sku;
                //    }
                //}
                objBAL.saveOtherDetails(partEntity);
                objBAL.FinalSubmit(partEntity);
                Session["OtherDetailsTab"] = null;
                Session["PalletTab"] = null;
                Session["OuterDetailsTab"] = null;
                Session["InnerLabelDetailsTab"] = null;
                Session["InnerBoxDetailsTab"] = null;
                Session["WrapperDetailsTab"] = null;
                Session["HandelDetailsTab"] = null;
                Session["CustomerDetailsTab"] = null;
                Session["StampDetailsTab"] = null;
                Session["RawMaterialDetailsTab"] = null;
                Session["ProductionDetailsTab"] = null;
            }
            return RedirectToAction("ChangeNote", "ChangeNotes");
        }


        #region back save For Other details By Archana Mahajan
        public ActionResult getOtherDetailsTab()
        {
            PartNumOtherDetailsModel partObj = new PartNumOtherDetailsModel();

            #region code added for edit by Dhanashree

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "other_details";
            partObj.otherdetailList = getEditOtherDetails(ReqNo, flag);
            Session["OtherDetailsTab"] = partObj.otherdetailList;



            List<PartNumOtherDetailsModel> sessionObj = (List<PartNumOtherDetailsModel>)Session["OtherDetailsTab"];
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["OtherDetailsTab"] != null && partObj.otherdetailList.Count > 0)
            {
                partObj.othdt_request_id = Convert.ToInt32(Session["requestId"]);
                partObj.othdt_request_type = Session["requestype"].ToString();
                partObj.othdt_prod_cost_onetm = sessionObj[0].othdt_prod_cost_onetm;
                partObj.othdt_prod_cost_run = sessionObj[0].othdt_prod_cost_run;
                partObj.othdt_dispatch_onetm = sessionObj[0].othdt_dispatch_onetm;
                partObj.othdt_dispatch_run = sessionObj[0].othdt_dispatch_run;
                partObj.othdt_req_startdt = Convert.ToDateTime(sessionObj[0].othdt_req_startdt);
                partObj.othdt_req_enddt = Convert.ToDateTime(sessionObj[0].othdt_req_enddt);
                partObj.othdt_req_enddt_current_sku = sessionObj[0].othdt_req_enddt_current_sku;
                return View("OtherDetails", partObj);

            }
            #endregion
            return RedirectToAction("OtherDetails");
        }
        #endregion


        #region Other Details-Edit by Dhanashree
        public List<PartNumOtherDetailsModel> getEditOtherDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<PartNumOtherDetailsModel> modellist = new List<PartNumOtherDetailsModel>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new PartNumOtherDetailsModel()
                {
                    othdt_prod_cost_onetm = item.Field<Decimal?>("othdt_prod_cost_onetm"),
                    othdt_prod_cost_run = item.Field<Decimal?>("othdt_prod_cost_run"),
                    othdt_dispatch_onetm = item.Field<Decimal?>("othdt_dispatch_onetm"),
                    othdt_dispatch_run = item.Field<Decimal?>("othdt_dispatch_run"),
                    othdt_req_startdt = Convert.ToDateTime(item.Field<DateTime?>("othdt_req_startdt")),
                    othdt_req_enddt = Convert.ToDateTime(item.Field<DateTime?>("othdt_req_enddt")),
                    othdt_req_enddt_current_sku = item.Field<DateTime?>("othdt_req_enddt_curr_sku")
                }).ToList();
            }
            return modellist;
        }
        #endregion

        [HttpGet]
        public ActionResult OverwriteExisting()
        {
            PartNumOtherDetailsModel partObj = new PartNumOtherDetailsModel();
            partObj.othdt_request_id = Convert.ToInt32(Session["requestId"]);
            partObj.othdt_request_type = Session["requestype"].ToString();
            DataTable dt = objBAL.CreateCloneReq("", partObj.othdt_request_id, "get_SKU");
            if (dt.Rows.Count > 0 && dt != null)
            {
                partObj.CurrSKU = dt.Rows[0]["SKU"].ToString();
            }
            return PartialView("_ConfrimOverwrite", partObj);
        }



        [HttpGet]
        public ActionResult Confirm()
        {
            PartNumOtherDetailsModel partObj = new PartNumOtherDetailsModel();

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "other_details";
            partObj.otherdetailList = getEditOtherDetails(ReqNo, flag);
            Session["OtherDetailsTab"] = partObj.otherdetailList;



            List<PartNumOtherDetailsModel> sessionObj = (List<PartNumOtherDetailsModel>)Session["OtherDetailsTab"];
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null)
            {
                string fileTypeCode = Session["FileTypeCode"].ToString();
                string FileSubType = Session["FileSubTypeCode"].ToString();
                string fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["OtherDetailsTab"] != null && partObj.otherdetailList.Count > 0)
            {
                partObj.othdt_request_id = Convert.ToInt32(Session["requestId"]);
                partObj.othdt_request_type = Session["requestype"].ToString();
                partObj.othdt_prod_cost_onetm = sessionObj[0].othdt_prod_cost_onetm;
                partObj.othdt_prod_cost_run = sessionObj[0].othdt_prod_cost_run;
                partObj.othdt_dispatch_onetm = sessionObj[0].othdt_dispatch_onetm;
                partObj.othdt_dispatch_run = sessionObj[0].othdt_dispatch_run;
                partObj.othdt_req_startdt = partObj.otherdetailList[0].othdt_req_startdt;
                partObj.othdt_req_enddt = partObj.otherdetailList[0].othdt_req_enddt;
                partObj.othdt_req_enddt_current_sku = sessionObj[0].othdt_req_enddt_current_sku;
            }
            return PartialView("_ConfirmSubmit", partObj);
        }
        [HttpPost]
        public ActionResult ConfirmOverwrite(PartNumOtherDetailsModel partObj)
        {
            PartNumOtherDetailsEntity partEntity = new PartNumOtherDetailsEntity();
            UsermgmtEntity user = new UsermgmtEntity();
            user.Password = user.Encrypt(partObj.confirmpassword);
            user.User_Id = Session["UserId"].ToString();
            // user.flag = "getloginuser";

            if (objBAL.getlogin(user))
            {
                partEntity.othdt_request_id = Convert.ToInt32(Session["requestId"]);
                partEntity.othdt_request_type = Session["requestype"].ToString();
                partEntity.ap_createby = Session["UserId"].ToString();
                objBAL.FinalSubmit(partEntity);
                return RedirectToAction("ChangeNote", "ChangeNotes");
            }
            return RedirectToAction("getOtherDetailsTab");
        }


        [HttpPost]
        public ActionResult ConfirmSubmit(PartNumOtherDetailsModel partObj)
        {
            Session["OtherDetailsTab"] = partObj;
            PartNumOtherDetailsEntity partEntity = new PartNumOtherDetailsEntity();
            partEntity.othdt_request_id = Convert.ToInt32(Session["requestId"]);
            partEntity.othdt_request_type = Session["requestype"].ToString();
            partEntity.othdt_prod_cost_onetm = partObj.othdt_prod_cost_onetm;
            partEntity.othdt_prod_cost_run = partObj.othdt_prod_cost_run;
            partEntity.othdt_dispatch_onetm = partObj.othdt_dispatch_onetm;
            partEntity.othdt_dispatch_run = partObj.othdt_dispatch_run;
            partEntity.othdt_req_startdt = partObj.othdt_req_startdt;
            partEntity.othdt_req_enddt = partObj.othdt_req_enddt;
            partEntity.ap_createby = Session["UserId"].ToString();
            // user.flag = "getloginuser";

            if (partEntity.othdt_request_type == "S")
            {
                var date = partObj.othdt_req_startdt.ToString("yyyy/MM/dd");
                partEntity.othdt_req_startdt = partObj.othdt_req_startdt;
                partEntity.othdt_req_enddt = partObj.othdt_req_enddt;
                objBAL.saveSetOtherDetails(partEntity);
                objBAL.FinalSubmit(partEntity);
            }
            else
            {
                objBAL.saveOtherDetails(partEntity);
                objBAL.FinalSubmit(partEntity);
            }

            string flag = "getMailByRole";
            string subject = "";
            string body = "";
            subject = "Request No:  " + Session["requestype"].ToString()
                 + Session["requestId"].ToString() + " and Request Submission";
            body = "Request No: " + Session["requestype"].ToString()
                + Session["requestId"].ToString() + " By User ID " + Session["UserId"].ToString()
                + " Generated/Re-Submitted.Needs Attention.";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();             
                objBAL.sendingmail(email,body,subject);
            }

            DataTable dtEmail1 = objBAL.GetEmailId("EMT_Disp_User", "", flag);
            if (dtEmail1.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }

            Session["OtherDetailsTab"] = null;
            Session["PalletTab"] = null;
            Session["OuterDetailsTab"] = null;
            Session["InnerLabelDetailsTab"] = null;
            Session["InnerBoxDetailsTab"] = null;
            Session["WrapperDetailsTab"] = null;
            Session["HandelDetailsTab"] = null;
            Session["CustomerDetailsTab"] = null;
            Session["StampDetailsTab"] = null;
            Session["RawMaterialDetailsTab"] = null;
            Session["ProductionDetailsTab"] = null;
            return RedirectToAction("ChangeNote", "ChangeNotes");

        }
        #endregion

        #region Remarks written by yogesh

        public ActionResult SubmitRemarkDispatchDetails(string prm_remarks, string prm_remark_tab)
        {
            try
            {
                PartnumgenRemarksModel remarkObj = new PartnumgenRemarksModel();
                int i = 0;
                if (ModelState.IsValid)
                {
                    PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
                    remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
                    remarkEntityObj.prm_request_type = Session["requestype"].ToString();
                    remarkEntityObj.prm_remark_tab = prm_remark_tab;
                    remarkEntityObj.prm_createby = "ABC";
                    remarkEntityObj.prm_remarks = prm_remarks;
                    objBAL.SaveRemark(remarkEntityObj);
                    i = 1;
                }
                if (i == 0)
                {
                    return Json(new { success = false, responseText = "Remark not added" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, responseText = "Remark added successfully." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SelectRemarkCustomerDetails(string prm_remark_tab)
        {
            PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
            PartnumgenRemarksModel objmodel = new PartnumgenRemarksModel();
            remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
            remarkEntityObj.prm_request_type = Session["requestype"].ToString();
            PartnumgenRemarksModel remarkobj = new PartnumgenRemarksModel();
            remarkEntityObj.remarklist = objBAL.getremarkdetail(prm_remark_tab, remarkEntityObj.prm_request_id, remarkEntityObj.prm_request_type);
            var RList = remarkEntityObj.remarklist;
            if (remarkEntityObj.remarklist.Count == 0)
            {
                return Json(new { success = true, remarks = "", remarkdate = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, remarks = RList }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region By Swapnil New Flow
        [HttpGet]
        public JsonResult gettangcolorddl()
        {
            DataSet ds = new DataSet();
            ds=objBAL.gettangcolorddl();
            return Json(ds, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult checkhandletype()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "HANDLETYPE";
            DataTable dt = new DataTable();
            dt = objBAL.checkpkgnewmstr(id, type, fo);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["pkng_isApproved"].ToString() == "N")
                {
                    string pkng_material = "0Z";
                    return Json(pkng_material, JsonRequestBehavior.AllowGet);
                }
                else if (dt.Rows[0]["pkng_isApproved"].ToString() == "Y")
                {
                    string pkng_material = dt.Rows[0]["pkng_material"].ToString();
                    return Json(pkng_material, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string pkng_material = "Ok";
                    return Json(pkng_material, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string pkng_material = "Ok";
                return Json(pkng_material, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkhandle()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Handle";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkpallet()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Pallet";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkouter()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "OuterChart";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkoutertype()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "OuterChart";
            string validation = objBAL.checkoutertype(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return Json(validation, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkwrapper()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "WrapperChartNo";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult checkpallettype()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Pallet";
            string validation = objBAL.checkpallettype(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(validation, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkwrappertype()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "WrapperChartNo";
            string validation = objBAL.checkwrappertype(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(validation, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkinnerbox()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "InnerBox";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkinnerboxtype()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "InnerBox";
            string validation = objBAL.checkinnerboxtype(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return Json(validation, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult UploadinnerlabelFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/ProjectImages/InnerLabel/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        [HttpPost]
        public ActionResult UploadinnerBoxFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/ProjectImages/InnerBox/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        [HttpPost]
        public ActionResult InnerboxEdit(List<InnerBoxDetailsEntity> InnerBoxList)
        {

            InnerBoxDetailsEntity model = new InnerBoxDetailsEntity();
            model.ibd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ibd_request_type = Session["requestype"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ib_chartnum = InnerBoxList[0].ib_chartnum;
                model.ib_chartimg = InnerBoxList[0].ib_chartimg;
                model.ib_box_type = InnerBoxList[0].ib_box_type;
                int count = InnerBoxList.Count;
                // count = count / 2;
                for (int i = 0; i < count; i++)
                {
                    model.ibd_pkg_parm_name = InnerBoxList[i].ibd_pkg_parm_name;
                    model.ibd_pkg_parm_code = InnerBoxList[i].ibd_pkg_parm_code;
                    model.ibd_pkg_parm_dwgvalue = InnerBoxList[i].ibd_pkg_parm_dwgvalue;
                    model.ibd_pkg_parm_scrnvalue = InnerBoxList[i].ibd_pkg_parm_scrnvalue;
                    objBAL.EditInnerboxSinglesavebyrow(model);
                }
                objBAL.EditInnerboxMaster(model);
            }

            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("_InnerBox");
            }
            else
            {
                return RedirectToAction("SetInnerBox");
            }

        }
        [HttpPost]
        public ActionResult InnerboxSingleAdd(List<InnerBoxDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerBoxDetailsEntity model = new InnerBoxDetailsEntity();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if (Session["FileSubTypeCode"] != null)
            {
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["FileSizeCode"] != null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["FileCutTypeCode"] != null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            //string fileTypeCode = Session["FileTypeCode"].ToString();
            //string FileSubType = Session["FileSubTypeCode"].ToString();
            //string fileSize = Session["FileSizeCode"].ToString();
            //string fileCutType = Session["FileCutTypeCode"].ToString();
            model.ibd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ibd_request_type = Session["requestype"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ib_chartnum = InnerBoxList[0].ib_chartnum;
                model.ib_chartimg = InnerBoxList[0].ib_chartimg;
                model.ib_box_type = InnerBoxList[0].ib_box_type;
                model.ib_qty_inbox = InnerBoxList[0].ib_qty_inbox;
                foreach (var item in InnerBoxList)
                {
                    if (i >= 1)
                    {
                        model.ibd_pkg_parm_name = item.ibd_pkg_parm_name;
                        model.ibd_pkg_parm_code = item.ibd_pkg_parm_code;
                        model.ibd_pkg_parm_dwgvalue = item.ibd_pkg_parm_dwgvalue;
                        model.ibd_pkg_parm_scrnvalue = item.ibd_pkg_parm_scrnvalue;
                        objBAL.InnerboxSinglesavebyrow(model, fileTypeCode, FileSubType, fileSize, fileCutType);

                    }
                    i++;
                }
                objBAL.AddInnerboxMasterandLink(model, fileTypeCode, FileSubType, fileSize, fileCutType);
            }


            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("_InnerBox");
            }
            else
            {
                return RedirectToAction("SetInnerBox");
            }
        }
        [HttpPost]
        public ActionResult InnerLabelDetailsEntityList(List<InnerLabelDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerLabelDetailsEntity model = new InnerLabelDetailsEntity();
            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();
            string fileCutType = Session["FileCutTypeCode"].ToString();
            model.ilbd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ilbd_request_type = Session["requestype"].ToString();
            string country = Session["ShipToCountryCode"].ToString();
            string brandId = Session["BrandId"].ToString();
            string custId = Session["CoustomerId"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ilbd_chart_num = InnerBoxList[0].ilbd_chart_num;

                foreach (var item in InnerBoxList)
                {
                    if (i >= 1)
                    {
                        model.ilbd_parm_name = item.ilbd_parm_name;
                        model.ilbd_parm_scrnvalue = item.ilbd_parm_scrnvalue;
                        objBAL.Innerlabelavebyrow(model);

                    }
                    i++;
                }
                objBAL.AddInnerLabelMasterandLink(model, fileTypeCode, FileSubType, fileSize, fileCutType, country, brandId, custId);
            }


            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("_InnerBox");
            }
            else
            {
                return RedirectToAction("SetInnerBox");
            }
        }
        [HttpPost]
        public ActionResult InnerLabelEditDetailsEntityList(List<InnerLabelDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerLabelDetailsEntity model = new InnerLabelDetailsEntity();

            model.ilbd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ilbd_request_type = Session["requestype"].ToString();

            if (InnerBoxList.Count >= 1)
            {
                model.ilbd_chart_num = InnerBoxList[0].ilbd_chart_num;

                foreach (var item in InnerBoxList)
                {

                    model.ilbd_parm_name = item.ilbd_parm_name;

                    if (string.IsNullOrEmpty(item.ilbd_parm_scrnvalue))
                    {
                        model.ilbd_parm_scrnvalue = item.ilbd_parm_dwgvalue;
                    }
                    else
                    {
                        model.ilbd_parm_scrnvalue = item.ilbd_parm_scrnvalue;
                    }
                    objBAL.InnerlabelEditavebyrow(model);

                    i++;
                }
                objBAL.AddInnerLabelMasterEditandLink(model);
            }

            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("_InnerBox");
            }
            else
            {
                return RedirectToAction("SetInnerBox");
            }

        }
        [HttpGet]
        public JsonResult innerlableMasterAdd()
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt16(Session["requestId"]);
            string C = objBAL.Checkinnerboxqty(id, type);
            ReqParlist.ReqParlist = objBAL.getinnerlableList(C);
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult innerboxSinglenew()
        {
            CutSpecFiledModel ReqParlist = new CutSpecFiledModel();
            ReqParlist.ReqParlist = objBAL.getinnerboxSinglenewfieldList();
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult innerboxDoublenew()
        {
            CutSpecFiledModel ReqParlist = new CutSpecFiledModel();
            ReqParlist.ReqParlist = objBAL.getinnerboxDoublefieldList();
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckHandleTypeAvailability(string hm_handle_type)
        {
            int SeachData = objBAL.CheckHandleTypeAvailability(hm_handle_type);
            // System.Threading.Thread.Sleep(200);
            // var SeachData = db.StudentDetails.Where(x => x.StuName == userdata).SingleOrDefault();
            if (SeachData >= 1)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
        [HttpGet]
        public ActionResult gethandleParmMasterDetails(string selectedhandle_type)
        {
            DispatchSKU model = new DispatchSKU();
            if (selectedhandle_type != "" && selectedhandle_type != "Select" && selectedhandle_type != "#handle_subtype")
            {
                Session["HandleTypeName"] = selectedhandle_type;
            }
            else
            {
                Session["HandleTypeName"] = "";
            }
            model.ReqParlistEntity = objBAL.getHandlesubtypefieldList(selectedhandle_type);
            if (model.ReqParlistEntity.Count > 0)
            {
                model.ReqParlistEntity = model.ReqParlistEntity;
            }
            else
            {
                List<MasterReqParModel> newList = new List<MasterReqParModel>();
                model.ReqParlist = newList;
            }


            return PartialView("_HandleMasternewAdd", model);
        }
        [HttpPost]
        public ActionResult HandletypeAdd(HandleDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.hm_request_type = Session["requestype"].ToString();
                model.hm_request_id = Convert.ToInt16(Session["requestId"]);

                objBAL.AddHandletypeDetails(model);
            }
            return RedirectToAction("DispatchIndex");
        }
        [HttpPost]
        public ActionResult HandleChartAdd(HandleDetailsEntity model)
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string fileCutType = "";
            if (Session["FileTypeCode"]!= null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
            }
            if (Session["FileSubTypeCode"] != null)
            {
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            if (Session["FileSizeCode"]!= null)
            {
                fileSize = Session["FileSizeCode"].ToString();
            }
            if (Session["FileCutTypeCode"] != null)
            {
                fileCutType = Session["FileCutTypeCode"].ToString();
            }
            model.fileTypeCode = fileTypeCode;
            model.FileSubType = FileSubType;
            model.fileSize = fileSize;
            int flag = objBAL.GetHandlestatus(model.hm_handle_type);
            if (flag == 0)
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(model.hm_handle_type))
                    {
                        model.hm_handle_type = "";
                    }
                    model.hm_request_type = Session["requestype"].ToString();
                    model.hm_request_id = Convert.ToInt16(Session["requestId"]);
                    objBAL.AddHandlechartandpkgmasterDetails(model);
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(model.hm_handle_type))
                    {
                        model.hm_handle_type = "";
                    }
                    model.hm_request_type = Session["requestype"].ToString();
                    model.hm_request_id = Convert.ToInt16(Session["requestId"]);
                    objBAL.AddHandlechartDetails(model);
                }
            }
            return RedirectToAction("DispatchIndex");
        }

        [HttpPost]
        public ActionResult HandleChartUpload(HandleDetailsEntity model)
        {
            var file = Request.Files["3"];
            if (file != null)
            {
                model.HandleImage = file.FileName;
                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].ContentLength == 0) continue;
                    string pathToSave = Server.MapPath("~/ProjectImages/Handle/");
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                }
            }
            return RedirectToAction("DispatchIndex");
        }
        [HttpPost]
        public ActionResult EditHandleChartDetails(HandleDetailsEntity model)
        {
            var file = Request.Files["file1"];
            if (file.FileName != "")
            {
                model.hm_chartimg_name = file.FileName;
                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].ContentLength == 0) continue;
                    string pathToSave = Server.MapPath("~/ProjectImages/Handle/");
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                }
            }
            int flag = objBAL.GetHandlestatusApprove(model.hm_handle_type);
            if (flag == 0)
            {
                if (ModelState.IsValid)
                {
                    objBAL.EditHandlechartandpkgmasterDetails(model);
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    objBAL.EditHandlechartDetails(model);
                }
            }
            return RedirectToAction("DispatchIndex");
        }
        [HttpPost]
        public ActionResult EditOuterChartDetails(OuterDetailsEntity model)
        {
            var file = Request.Files["file"];
            if (file.FileName != "")
            {
                model.OUTERPACKIMG = file.FileName;
            }

            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Outer/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            if (ModelState.IsValid)
            {
                objBAL.EditOuterchartDetails(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Outer");
            }
            else
            {
                return RedirectToAction("SetOuter");
            }


        }
        [HttpPost]
        public ActionResult EditInnerLabelDetails(InnerLabelDetailsEntity model)
        {
            var file1 = Request.Files["file1"];
            var file2 = Request.Files["file2"];
            var file3 = Request.Files["file3"];
            var file4 = Request.Files["file4"];
            var file5 = Request.Files["file5"];
            var file6 = Request.Files["file6"];
            var file7 = Request.Files["file7"];
            var file8 = Request.Files["file8"];
            if (file1 != null)
            {
                model.INNERPACKINGTOPLABEL = file1.FileName;
            }
            if (file2 != null)
            {
                model.INNERPACKINGBOTTOMLABEL = file2.FileName;
            }
            if (file3 != null)
            {
                model.INNERPACKINGSIDE1LABEL = file3.FileName;
            }
            if (file4 != null)
            {
                model.INNERPACKINGSIDE2LABEL = file4.FileName;
            }
            if (file5 != null)
            {
                model.INNERPACKINGEND1LABEL = file5.FileName;
            }
            if (file6 != null)
            {
                model.INNERPACKINGEND2LABEL = file6.FileName;
            }
            if (file7 != null)
            {
                model.INNERPACKINGBOXSAMPLE = file7.FileName;
            }
            if (file8 != null)
            {
                model.INNERPACKINGLABELLOCATIONCHARTID = file8.FileName;
            }
            //model.INNERPACKINGTOPLABEL = Path.GetFileName(Request.Files[0].FileName);
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/InnerLabel/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            objBAL.EditInnerLabelDetails(model);
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("InnerLabel");
            }
            else
            {
                return RedirectToAction("SetInnerLabel");
            }

        }
        [HttpPost]
        public ActionResult innerlableUpload(InnerLabelDetailsEntity model)
        {
            // HttpPostedFileBase photo = Request.Files["FILE1"];
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/InnerLabel/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("InnerLabel");
            }
            else
            {
                return RedirectToAction("SetInnerLabel");
            }
        }
        [HttpPost]
        public ActionResult OuterChartUpload(OuterDetailsEntity model)
        {
            // HttpPostedFileBase photo = Request.Files["FILE1"];
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Outer/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }

            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Outer");
            }
            else
            {
                return RedirectToAction("SetOuter");
            }
        }
        [HttpPost]
        public ActionResult innerlablemasterAdd(InnerLabelDetailsEntity model)
        {
            string fileTypeCode = Session["FileTypeCode"].ToString();
            string FileSubType = Session["FileSubTypeCode"].ToString();
            string fileSize = Session["FileSizeCode"].ToString();

            model.ilbd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ilbd_request_type = Convert.ToString(Session["requestype"]);
            model.fs_size_code = fileSize;
            model.ft_ftype_desc = fileTypeCode;
            model.pm_fstype_desc = FileSubType;
            model.bm_brand_name = Session["BrandId"].ToString();
            model.cm_cust_name = Session["CoustomerId"].ToString();
            model.fc_cut_desc = Session["FileCutTypeCode"].ToString();
            model.ShipToCountryCode = Session["ShipToCountryCode"].ToString();
            objBAL.innerlablenewSave(model);

            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("InnerLabel");
            }
            else
            {
                return RedirectToAction("SetInnerLabel");
            }
        }
        [HttpPost]
        public ActionResult WrapperImgUpload()
        {
            //string directory = @"D:\Temp\";
            HttpPostedFileBase photo = Request.Files["FILE12"];
            //photo = Request.Files["FILE2"];

            if (photo != null && photo.ContentLength > 0)
            {
                string spath = Server.MapPath("~/ProjectImages/WrapperChart");
                var fileName = Path.GetFileName(photo.FileName);
                photo.SaveAs(Path.Combine(spath, fileName));

            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("WrapperTab");
            }
            else
            {
                return RedirectToAction("ManageNewSetDispatch");
            }

        }

        public PartialViewResult _HandleMasterAdd(HandleMasterEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getHandlefieldList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _HandleMasternewAdd(HandleMasterEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getHandlechartfieldList();
            return PartialView(ReqParlist);
        }
        //[HttpGet]
        //public ActionResult gethandleParmMasterDetails(string selectedhandle_type)
        //{
        //    DispatchSKU model = new DispatchSKU();
        //    model.ReqParlistEntity = objBAL.getHandlesubtypefieldList(selectedhandle_type);

        //    if (model.ReqParlistEntity.Count > 0)
        //    {
        //        model.ReqParlistEntity = model.ReqParlistEntity;
        //    }
        //    else
        //    {
        //        List<MasterReqParModel> newList = new List<MasterReqParModel>();
        //        model.ReqParlist = newList;
        //    }
        //    return PartialView("_HandleMasternewAdd", model);
        //}
        public PartialViewResult _wrappingchartMasterAdd(WrapChartModelEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getWrappingchartList();
            return PartialView(ReqParlist);
        }
        //public PartialViewResult innerlableMasterAdd(WrapChartModelEntity model)
        //{
        //    MasterReqParModel ReqParlist = new MasterReqParModel();
        //    ReqParlist.ReqParlist = objBAL.getinnerlableList();
        //    return PartialView(ReqParlist);
        //}
        public PartialViewResult _wrappingtypeMasterAdd(WrapperTypeEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL._wrappingtypeMasterAdd();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _innerboxTypeMasterAdd(InnerBoxTypeEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getinnerboxTypeList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _OuterTypeMasterAdd(OutertypeEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getOuterTypeList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult OuterchartMasterAdd(OuterDetailsEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getOuterchartList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _PalletTypeMasterAdd(PallettypeEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getPalletTypeList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _PalletChartMasterAdd(PalletDetailsEntity model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getPalletChartList();
            return PartialView(ReqParlist);
        }
        [HttpPost]
        public ActionResult PalletChartUpload(PalletDetailsEntity model)
        {
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Pallet/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Pallet");
            }
            else
            {
                return RedirectToAction("SetPallet");
            }

        }
        [HttpPost]
        public ActionResult PalletchartMasterAdd(PalletDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pm_request_type = Convert.ToString(Session["requestype"]);
                model.pm_request_id = Convert.ToInt16(Session["requestId"]);
                objBAL.PalletchartSave(model);
            }

            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Pallet");
            }
            else
            {
                return RedirectToAction("SetPallet");
            }
        }
        [HttpPost]
        public ActionResult EditPalletChartDetails(PalletDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pm_request_type = Convert.ToString(Session["requestype"]);
                model.pm_request_id = Convert.ToInt16(Session["requestId"]);
                HttpPostedFileBase photo = Request.Files["file1"];
                if (photo.FileName != "")
                {
                    model.pm_pallet_chartimg = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/Pallet/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                    }
                }
                objBAL.EditPalletchart(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Pallet");
            }
            else
            {
                return RedirectToAction("SetPallet");
            }
        }
        [HttpPost]
        public ActionResult WrapperChartAdd(WrapChartModelEntity model)
        {
            if (ModelState.IsValid)
            {
                model.wm_request_id = Convert.ToInt16(Session["requestId"]);
                model.wm_request_type = Convert.ToString(Session["requestype"]);
                string fileTypeCode = "";
                string FileSubType = "";
                string fileSize = "";
                string fileCutType = "";
                if (Session["FileTypeCode"] != null)
                {
                    fileTypeCode = Session["FileTypeCode"].ToString();
                }
                if (Session["FileSubTypeCode"] != null)
                {
                    FileSubType = Session["FileSubTypeCode"].ToString();
                }
                if (Session["FileSizeCode"] != null)
                {
                    fileSize = Session["FileSizeCode"].ToString();
                }
                if (Session["FileCutTypeCode"] != null)
                {
                    fileCutType = Session["FileCutTypeCode"].ToString();
                }
                model.wm_ftype_id = fileTypeCode;
                model.wm_fstype_id = FileSubType;
                model.wm_cust_id = Session["CoustomerId"].ToString();
                model.wm_brand_id = Session["BrandId"].ToString();
                model.wm_fsize_id = fileSize;

                objBAL.NewWrapperChartSave(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("WrapperTab");
            }
            else
            {
                return RedirectToAction("ManageNewSetDispatch");
            }

        }
        [HttpPost]
        public ActionResult EditWrapChartDetails(WrapChartModelEntity model)
        {
            HttpPostedFileBase photo = Request.Files["img1"];
            //photo = Request.Files["FILE2"];

            if (photo != null && photo.ContentLength > 0)
            {
                string spath = Server.MapPath("~/ProjectImages/WrapperChart");
                var fileName = Path.GetFileName(photo.FileName);
                photo.SaveAs(Path.Combine(spath, fileName));
                model.img = fileName;
            }
            if (ModelState.IsValid)
            {
                //model.wm_ftype_id = Session["FileTypeCode"].ToString();
                //model.wm_fstype_id = Session["FileSubTypeCode"].ToString();
                //model.wm_cust_id = Session["CoustomerId"].ToString();
                //model.wm_brand_id = Session["BrandId"].ToString();
                objBAL.EditWrapChartDetails(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("WrapperTab");
            }
            else
            {
                return RedirectToAction("ManageNewSetDispatch");
            }
        }
        [HttpPost]
        public ActionResult WrappertypeAdd(WrapperTypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.WrappertypeAdd(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("WrapperTab");
            }
            else
            {
                return RedirectToAction("ManageNewSetDispatch");
            }
        }
        [HttpPost]
        public ActionResult innerboxTypeMasterAdd(InnerBoxTypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.NewinnerboxtypeSave(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("InnerBox");
            }
            else
            {
                return RedirectToAction("ManageNewSetDispatch");
            }

        }
        [HttpPost]
        public ActionResult OutertypeAdd(OutertypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.NewOutertypeSave(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Outer");
            }
            else
            {
                return RedirectToAction("SetOuter");
            }

        }
        [HttpPost]
        public ActionResult OuterChartAdd(OuterDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.ob_request_id = Convert.ToInt16(Session["requestId"]);
                model.ob_request_type = Convert.ToString(Session["requestype"]);
                string fileSize = "";
                if (Session["FileSizeCode"] != null)
                {
                    fileSize = Session["FileSizeCode"].ToString();
                }
                //model.ob_box_type = "";
                model.ob_box_fsize_code = fileSize;
                objBAL.NewOuterchartSave(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Outer");
            }
            else
            {
                return RedirectToAction("SetOuter");
            }
        }
        public ActionResult PalletTypeMasterAdd(PallettypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.NewPallettypeSave(model);
            }
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("Pallet");
            }
            else
            {
                return RedirectToAction("SetPallet");
            }
        }
        #endregion

        #region Required New Set Writte By Yogesh

        #region NEw Flow Of Set by Swapnil
        [HttpPost]
        public ActionResult SetWrapperChartAdd(WrapChartModelEntity model)
        {
            if (ModelState.IsValid)
            {
                Session["SetWrapperDetailsTab"] = model;
                model.wm_request_id = Convert.ToInt16(Session["requestId"]);
                model.wm_request_type = Convert.ToString(Session["requestype"]);
                model.wm_ftype_id = "SE";
                model.wm_fstype_id = "T";
                model.wm_cust_id = Session["CoustomerId"].ToString();
                model.wm_brand_id = Session["BrandId"].ToString();
                string fileSize = Session["MaxFileSizeCode"].ToString();
                model.wm_fsize_id = fileSize;

                objBAL.NewWrapperChartSave(model);
            }
            return RedirectToAction("ManageNewSetDispatch");
        }
        [HttpPost]
        public ActionResult SetUploadinnerlabelFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/ProjectImages/InnerLabel/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        [HttpPost]
        public ActionResult SetUploadinnerBoxFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/ProjectImages/InnerBox/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        [HttpPost]
        public ActionResult SetInnerboxEdit(List<InnerBoxDetailsEntity> InnerBoxList)
        {

            InnerBoxDetailsEntity model = new InnerBoxDetailsEntity();
            model.ibd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ibd_request_type = Session["requestype"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ib_chartnum = InnerBoxList[0].ib_chartnum;
                model.ib_chartimg = InnerBoxList[0].ib_chartimg;
                model.ib_box_type = InnerBoxList[0].ib_box_type;
                int count = InnerBoxList.Count;
                // count = count / 2;
                for (int i = 0; i < count; i++)
                {
                    model.ibd_pkg_parm_name = InnerBoxList[i].ibd_pkg_parm_name;
                    model.ibd_pkg_parm_code = InnerBoxList[i].ibd_pkg_parm_code;
                    model.ibd_pkg_parm_dwgvalue = InnerBoxList[i].ibd_pkg_parm_dwgvalue;
                    model.ibd_pkg_parm_scrnvalue = InnerBoxList[i].ibd_pkg_parm_scrnvalue;
                    objBAL.EditInnerboxSinglesavebyrow(model);
                }
                objBAL.EditInnerboxMaster(model);
            }


            return RedirectToAction("SetInnerBox");
        }
        [HttpPost]
        public ActionResult SetInnerboxSingleAdd(List<InnerBoxDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerBoxDetailsEntity model = new InnerBoxDetailsEntity();
            string fileTypeCode = "SE";
            string FileSubType = "T";
            string fileSize = Session["MaxFileSizeCode"].ToString();
            string fileCutType = "N";
            model.ibd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ibd_request_type = Session["requestype"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ib_chartnum = InnerBoxList[0].ib_chartnum;
                model.ib_chartimg = InnerBoxList[0].ib_chartimg;
                model.ib_box_type = InnerBoxList[0].ib_box_type;
                model.ib_qty_inbox = InnerBoxList[0].ib_qty_inbox;
                foreach (var item in InnerBoxList)
                {
                    if (i >= 1)
                    {
                        model.ibd_pkg_parm_name = item.ibd_pkg_parm_name;
                        model.ibd_pkg_parm_code = item.ibd_pkg_parm_code;
                        model.ibd_pkg_parm_dwgvalue = item.ibd_pkg_parm_dwgvalue;
                        model.ibd_pkg_parm_scrnvalue = item.ibd_pkg_parm_scrnvalue;
                        objBAL.InnerboxSinglesavebyrow(model, fileTypeCode, FileSubType, fileSize, fileCutType);

                    }
                    i++;
                }
                objBAL.AddInnerboxMasterandLink(model, fileTypeCode, FileSubType, fileSize, fileCutType);
            }


            return RedirectToAction("getSetInnerBoxTabDetails");
        }
        [HttpPost]
        public ActionResult SetInnerLabelDetailsEntityList(List<InnerLabelDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerLabelDetailsEntity model = new InnerLabelDetailsEntity();
            string fileTypeCode = "SE";
            string FileSubType = "T";
            string fileSize = Session["MaxFileSizeCode"].ToString();
            string fileCutType = "N";
            model.ilbd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ilbd_request_type = Session["requestype"].ToString();
            string country = Session["ShipToCountryCode"].ToString();
            string brandId = Session["BrandId"].ToString();
            string custId = Session["CoustomerId"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ilbd_chart_num = InnerBoxList[0].ilbd_chart_num;

                foreach (var item in InnerBoxList)
                {
                    if (i >= 1)
                    {
                        model.ilbd_parm_name = item.ilbd_parm_name;
                        model.ilbd_parm_scrnvalue = item.ilbd_parm_scrnvalue;
                        objBAL.Innerlabelavebyrow(model);

                    }
                    i++;
                }
                objBAL.AddInnerLabelMasterandLink(model, fileTypeCode, FileSubType, fileSize, fileCutType, country, brandId, custId);
            }


            return RedirectToAction("SetInnerLabel");
        }
        [HttpPost]
        public ActionResult SetInnerLabelEditDetailsEntityList(List<InnerLabelDetailsEntity> InnerBoxList)
        {
            int i = 0;
            InnerLabelDetailsEntity model = new InnerLabelDetailsEntity();

            model.ilbd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ilbd_request_type = Session["requestype"].ToString();

            if (InnerBoxList.Count >= 1)
            {
                model.ilbd_chart_num = InnerBoxList[0].ilbd_chart_num;

                foreach (var item in InnerBoxList)
                {

                    model.ilbd_parm_name = item.ilbd_parm_name;

                    if (string.IsNullOrEmpty(item.ilbd_parm_scrnvalue))
                    {
                        model.ilbd_parm_scrnvalue = item.ilbd_parm_dwgvalue;
                    }
                    else
                    {
                        model.ilbd_parm_scrnvalue = item.ilbd_parm_scrnvalue;
                    }
                    objBAL.InnerlabelEditavebyrow(model);

                    i++;
                }
                objBAL.AddInnerLabelMasterEditandLink(model);
            }


            return RedirectToAction("SetInnerLabel");
        }

        [HttpGet]
        public JsonResult SetinnerlableMasterAdd()
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt16(Session["requestId"]);
            string C = objBAL.Checkinnerboxqty(id, type);
            ReqParlist.ReqParlist = objBAL.getinnerlableList(C);
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult SetinnerboxSinglenew()
        {
            CutSpecFiledModel ReqParlist = new CutSpecFiledModel();
            ReqParlist.ReqParlist = objBAL.getinnerboxSinglenewfieldList();
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult SetinnerboxDoublenew()
        {
            CutSpecFiledModel ReqParlist = new CutSpecFiledModel();
            ReqParlist.ReqParlist = objBAL.getinnerboxDoublefieldList();
            return Json(ReqParlist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SetEditOuterChartDetails(OuterDetailsEntity model)
        {
            var file = Request.Files["file"];
            if (file != null)
            {
                model.OUTERPACKIMG = file.FileName;
            }

            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Outer/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            if (ModelState.IsValid)
            {
                objBAL.EditOuterchartDetails(model);
            }
            return RedirectToAction("SetOuter");
        }
        [HttpPost]
        public ActionResult SetEditInnerLabelDetails(InnerLabelDetailsEntity model)
        {
            var file1 = Request.Files["file1"];
            var file2 = Request.Files["file2"];
            var file3 = Request.Files["file3"];
            var file4 = Request.Files["file4"];
            var file5 = Request.Files["file5"];
            var file6 = Request.Files["file6"];
            var file7 = Request.Files["file7"];
            var file8 = Request.Files["file8"];
            if (file1 != null)
            {
                model.INNERPACKINGTOPLABEL = file1.FileName;
            }
            if (file2 != null)
            {
                model.INNERPACKINGBOTTOMLABEL = file2.FileName;
            }
            if (file3 != null)
            {
                model.INNERPACKINGSIDE1LABEL = file3.FileName;
            }
            if (file4 != null)
            {
                model.INNERPACKINGSIDE2LABEL = file4.FileName;
            }
            if (file5 != null)
            {
                model.INNERPACKINGEND1LABEL = file5.FileName;
            }
            if (file6 != null)
            {
                model.INNERPACKINGEND2LABEL = file6.FileName;
            }
            if (file7 != null)
            {
                model.INNERPACKINGBOXSAMPLE = file7.FileName;
            }
            if (file8 != null)
            {
                model.INNERPACKINGLABELLOCATIONCHARTID = file8.FileName;
            }
            //model.INNERPACKINGTOPLABEL = Path.GetFileName(Request.Files[0].FileName);
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/InnerLabel/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            objBAL.EditInnerLabelDetails(model);

            return RedirectToAction("SetInnerLabel");
        }
        [HttpPost]
        public ActionResult SetinnerlableUpload(InnerLabelDetailsEntity model)
        {
            // HttpPostedFileBase photo = Request.Files["FILE1"];
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/InnerLabel/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            return RedirectToAction("SetInnerLabel");
        }
        [HttpPost]
        public ActionResult SetOuterChartUpload(OuterDetailsEntity model)
        {
            // HttpPostedFileBase photo = Request.Files["FILE1"];
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Outer/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            return RedirectToAction("Outer");
        }
        [HttpPost]
        public ActionResult SetinnerlablemasterAdd(InnerLabelDetailsEntity model)
        {
            string fileTypeCode = "SE";
            string FileSubType = "T";
            string fileSize = Session["MaxFileSizeCode"].ToString();

            model.ilbd_request_id = Convert.ToInt16(Session["requestId"]);
            model.ilbd_request_type = Convert.ToString(Session["requestype"]);
            model.fs_size_code = fileSize;
            model.ft_ftype_desc = fileTypeCode;
            model.pm_fstype_desc = FileSubType;
            model.bm_brand_name = Session["BrandId"].ToString();
            model.cm_cust_name = Session["CoustomerId"].ToString();
            model.fc_cut_desc = "N";
            model.ShipToCountryCode = Session["ShipToCountryCode"].ToString();
            objBAL.innerlablenewSave(model);
            return RedirectToAction("SetInnerLabel");
        }
        [HttpPost]
        public ActionResult SetWrapperImgUpload()
        {
            //string directory = @"D:\Temp\";
            HttpPostedFileBase photo = Request.Files["FILE12"];
            //photo = Request.Files["FILE2"];

            if (photo != null && photo.ContentLength > 0)
            {
                string spath = Server.MapPath("~/ProjectImages/WrapperChart/");
                var fileName = Path.GetFileName(photo.FileName);
                photo.SaveAs(Path.Combine(spath, fileName));

            }

            return RedirectToAction("ManageNewSetDispatch");
        }
        [HttpPost]
        public ActionResult SetPalletChartUpload(PalletDetailsEntity model)
        {
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Pallet/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            return RedirectToAction("SetPallet");
        }
        [HttpPost]
        public ActionResult SetPalletchartMasterAdd(PalletDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pm_request_type = Convert.ToString(Session["requestype"]);
                model.pm_request_id = Convert.ToInt16(Session["requestId"]);
                objBAL.PalletchartSave(model);
            }
            return RedirectToAction("SetPallet");
        }
        [HttpPost]
        public ActionResult SetEditPalletChartDetails(PalletDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pm_request_type = Convert.ToString(Session["requestype"]);
                model.pm_request_id = Convert.ToInt16(Session["requestId"]);
                HttpPostedFileBase photo = Request.Files["file1"];
                if (photo != null)
                {
                    model.pm_pallet_chartimg = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/Pallet/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                    }
                }

                objBAL.EditPalletchart(model);
            }
            return RedirectToAction("SetPallet");
        }

        [HttpPost]
        public ActionResult SetEditWrapChartDetails(WrapChartModelEntity model)
        {
            HttpPostedFileBase photo = Request.Files["img1"];
            //photo = Request.Files["FILE2"];

            if (photo != null && photo.ContentLength > 0)
            {
                string spath = Server.MapPath("~/ProjectImages/WrapperChart");
                var fileName = Path.GetFileName(photo.FileName);
                photo.SaveAs(Path.Combine(spath, fileName));

            }
            if (ModelState.IsValid)
            {
                //model.wm_ftype_id = "SE";
                //model.wm_fstype_id = "T";
                //model.wm_cust_id = Session["CoustomerId"].ToString();
                //model.wm_brand_id = Session["BrandId"].ToString();
                objBAL.EditWrapChartDetails(model);
            }
            return RedirectToAction("ManageNewSetDispatch");
        }
        [HttpPost]
        public ActionResult SetWrappertypeAdd(WrapperTypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.WrappertypeAdd(model);
            }
            return RedirectToAction("ManageNewSetDispatch");
        }
        [HttpPost]
        public ActionResult SetinnerboxTypeMasterAdd(InnerBoxTypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.NewinnerboxtypeSave(model);
            }
            return RedirectToAction("getSetInnerBoxTabDetails");
        }
        [HttpPost]
        public ActionResult SetOutertypeAdd(OutertypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.NewOutertypeSave(model);
            }
            return RedirectToAction("SetOuter");
        }
        [HttpPost]
        public ActionResult SetOuterChartAdd(OuterDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                model.ob_request_id = Convert.ToInt16(Session["requestId"]);
                model.ob_request_type = Convert.ToString(Session["requestype"]);
                //model.ob_box_type = "";
                model.ob_box_fsize_code = Session["MaxFileSizeCode"].ToString();
                objBAL.NewOuterchartSave(model);
            }
            return RedirectToAction("SetOuter");
        }
        public ActionResult SetPalletTypeMasterAdd(PallettypeEntity model)
        {
            if (ModelState.IsValid)
            {
                model.pkng_request_id = Convert.ToInt16(Session["requestId"]);
                model.pkng_request_type = Convert.ToString(Session["requestype"]);
                objBAL.NewPallettypeSave(model);
            }
            return RedirectToAction("SetPallet");
        }
        #endregion

        #region Set wrapper
        public ActionResult ManageNewSetDispatch()
        {
            string fileTypeCode = "";
            string FileSubType = "";
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
            }
            ViewBag.WrapperView = "New";
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "WRAPPER";
            dispatchObj.packingMasterialList = objBAL.getWrappingTypeList(usedFor);
            return View(dispatchObj);
        }

        [HttpPost]
        public ActionResult Wrappingskusection(string dp_wrapping_type, string dp_wrapping_chart)
        {
            DispatchSKUEntity dpobj = new DispatchSKUEntity();

            dpobj.dp_wrapping_type = dp_wrapping_type;
            dpobj.dp_wrapping_chart = dp_wrapping_chart;
            dpobj.dp_request_type = "S";
            dpobj.dp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            dpobj.flag = "updateskudispatchwrapper";
            Session["SetWrapperDetailsTab"] = dpobj;
            int j = objBAL.SaveupdateskudispatchMaster(dpobj);
            return Json(new { url = @Url.Action("getSetInnerBoxTabDetails", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult WrappingskusectionNoYes(string dp_wrapping_chart, string dp_wrapping_type)
        {
            SkuSelectionEntity sseobj = new SkuSelectionEntity();
            sseobj.rs_request_type = "S";
            sseobj.rs_wrapper_chart = "Use Set Wrapper";
            sseobj.rs_request_id = Convert.ToInt32(Session["requestId"].ToString());
            Session["SetWrapperDetailsTab"] = sseobj;
            sseobj.flag = "updatewrapperskuNY";
            int j = objBAL.SaveupdatewrapperskuMaster(sseobj);
            DispatchSKUEntity dpobj = new DispatchSKUEntity();
            dpobj.dp_request_type = "S";
            dpobj.dp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            dpobj.dp_wrapping_chart = dp_wrapping_chart;
            dpobj.dp_wrapping_type = dp_wrapping_type;
            dpobj.flag = "updateskudispatchwrapper";
            int k = objBAL.SaveupdateskudispatchMaster(dpobj);
            return Json(new { url = @Url.Action("getSetInnerBoxTabDetails", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult WrappingskusectionYesNo(string dp_wrapping_chart)
        {
            DispatchSKUEntity dpobj = new DispatchSKUEntity();


            dpobj.dp_wrapping_chart = "Use Individual wrapper";
            dpobj.dp_request_type = "S";
            dpobj.dp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            Session["SetWrapperDetailsTab"] = dpobj;
            dpobj.flag = "updateskudispatchwrapper";
            int j = objBAL.SaveupdateskudispatchMaster(dpobj);
            return Json(new { url = @Url.Action("getSetInnerBoxTabDetails", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult getSetWrappingChartNums(string wrappingType)
        {

            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
            }


            string fileTypeCode = "SE";
            string FileSubType = "T";
            string fileSize = Session["MaxFileSizeCode"].ToString();
            string brandId = Session["BrandId"].ToString();
            string custId = Session["CoustomerId"].ToString();

            DispatchSKU obj = new DispatchSKU();
            obj.dp_wrapping_chartList = objBAL.getWrappingChartNums(wrappingType, fileTypeCode, FileSubType, fileSize, custId, brandId);

            return Json(obj.dp_wrapping_chartList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getSetWrapperTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "wrapper_details";
            dispatchObj.modellist = getEditWrapper(ReqNo, flag);
            Session["SetWrapperDetailsTab"] = dispatchObj.modellist;
            if ((dispatchObj.modellist != null) && (dispatchObj.modellist.Any()))
            {
                ViewBag.LockScreen = "LockScreen";
            }

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["SetWrapperDetailsTab"];
            if (Session["SetWrapperDetailsTab"] != null && sessionObj.Count > 0)
            {
                string WrapperSet = objBAL.CheckWrapperSet(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());




                if (WrapperSet == "Use Individual wrapper")
                {
                    ViewBag.Selectedradiobtn = "3";
                    ViewBag.WrapperView = "Save";
                }
                else
                {
                    string SetSKUWrapperSet = objBAL.SetSKUWrapperSet(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
                    if (SetSKUWrapperSet == "Use Set Wrapper")
                    {
                        ViewBag.Selectedradiobtn = "2";
                        ViewBag.WrapperView = "Save";
                    }
                    else
                    {
                        if (WrapperSet != "")
                        {
                            ViewBag.WrapperView = "Save";
                            ViewBag.Selectedradiobtn = "1";
                        }
                        else
                        {
                            ViewBag.WrapperView = "NotSave";
                        }
                    }

                }
                string SetNewSKUWrapperSet = objBAL.SetNewSKUWrapperSet(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());


                string usedFor = "WRAPPER";
                dispatchObj.packingMasterialList = objBAL.getWrappingTypeList(usedFor);
                dispatchObj.dp_wrapping_type = sessionObj[0].dp_wrapping_type;
                if (SetNewSKUWrapperSet != "")
                {
                    dispatchObj.dp_wrapping_chart = "New";
                }
                else
                {
                    dispatchObj.dp_wrapping_chart = sessionObj[0].dp_wrapping_chart;
                }
                dispatchObj.dp_request_id = sessionObj[0].dp_request_id;
                dispatchObj.dp_request_type = sessionObj[0].dp_request_type;

                return PartialView("ManageNewSetDispatch", dispatchObj);
            }
            return RedirectToAction("ManageNewSetDispatch");
        }
        public ActionResult SetWrapperTab()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "WRAPPER";
            dispatchObj.packingMasterialList = objBAL.getWrappingTypeList(usedFor);
            return PartialView("_WrapperTab", dispatchObj);
        }

        public List<DispatchSKU> getSetEditWrapper(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_wrapping_chart = item.Field<string>("dp_wrapping_chart"),
                    dp_wrapping_type = item.Field<string>("dp_wrapping_type"),
                    dp_wrapping_recid = item.Field<string>("dp_wrapping_recid"),

                }).ToList();
            }
            return modellist;
        }
        #endregion
        //inner box
        #region Set InnerBox
        public ActionResult SetInnerBox()
        {
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
                fileTypeCode = "SE";
                FileSubType = "T";
            }
            fileSize = Session["MaxFileSizeCode"].ToString();
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "INNER BOX";
            dispatchObj.innerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
            dispatchObj.qtyInnerBoxList = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            //dispatchObj.dp_innerbox_id = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);
            return PartialView("_SetInnerBox", dispatchObj);
        }

        public ActionResult getSetInnerBoxTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "inner_box";
            dispatchObj.modellist = getEditInnerBox(ReqNo, flag);
            Session["SetInnerBoxDetailsTab"] = dispatchObj.modellist;
            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["SetInnerBoxDetailsTab"];
            if (Session["SetInnerBoxDetailsTab"] != null && sessionObj.Count > 0)
            {
                string usedFor = "INNER BOX";
                dispatchObj.innerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
                // dispatchObj.qtyInnerBoxList = objBAL.getQtyFOrInnerBox(fileTypeCode, FileSubType, fileSize);

                dispatchObj.dp_innerbox_type = sessionObj[0].dp_innerbox_type;
                dispatchObj.dp_innerbox_chart = sessionObj[0].ib_chartnum;
                dispatchObj.dp_innerbox_qty = sessionObj[0].dp_innerbox_qty;
                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_SetInnerBox", dispatchObj);
            }
            return RedirectToAction("SetInnerBox");
        }

        [HttpGet]
        public JsonResult getSetInnerBoxChartNums(string innerBoxMaterial)
        {
            string fileTypeCode = "SE";
            string FileSubType = "T";
            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
                fileTypeCode = "SE";
                FileSubType = "T";
            }
            string fileSize = Session["MaxFileSizeCode"].ToString();
            DispatchSKU obj = new DispatchSKU();
            obj.innerBoxChartNUmsList = objBAL.getInnerBoxChartNums(innerBoxMaterial, "", fileTypeCode, FileSubType, fileSize);


            return Json(obj.innerBoxChartNUmsList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSkuSetinnerbox(string dp_innerbox_type, string dp_innerbox_chart)
        {
            DispatchSKUEntity dpobj = new DispatchSKUEntity();

            dpobj.dp_innerbox_type = dp_innerbox_type;
            dpobj.dp_innerbox_chart = dp_innerbox_chart;
            dpobj.dp_request_type = "S";
            dpobj.dp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            dpobj.flag = "updateskuSetinnerbox";
            Session["SetInnerBoxDetailsTab"] = dpobj;
            int j = objBAL.Saveupdateskudispatchinnerbox(dpobj);
            return Json(new { url = @Url.Action("SetInnerLabel", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ViewSetInnerBoxDetails(string InnerBoxChart)
        {

            //InnerBoxChart = "CardBoard";
            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;

            }
            string fileTypeCode = "SE";
            string FileSubType = "T";
            string fileSize = Session["MaxFileSizeCode"].ToString();
            InnerBoxDetilsModel obj = new InnerBoxDetilsModel();
            InnerBoxDetailsEntity entityEntity = new InnerBoxDetailsEntity();
            entityEntity.ibd_box_recid = InnerBoxChart;

            entityEntity.InnerBoxDetailsEntityList = objBAL.getInnerBoxDetails(entityEntity.ibd_box_recid);
            if (InnerBoxChart != "" || InnerBoxChart != null)
            {
                obj.innerboxlist = objBAL.getinnerboxlist(InnerBoxChart, FileSubType, fileTypeCode, fileSize);
            }

            obj.InnerBoxDetailsEntityList = entityEntity.InnerBoxDetailsEntityList;
            obj.FileSize = fileSize;
            obj.FileType = fileTypeCode;

            return PartialView("_ViewInnerBoxDetails", obj);
        }
        #endregion

        #region Set Inner Label
        public ActionResult SetInnerLabel()
        {
            return PartialView("_SetInnerLabel");
        }

        [HttpPost]
        public ActionResult SetInnerLabel(DispatchSKU disObj)
        {

            //DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            //Session["SetInnerLabelDetailsTab"] = disObj;
            //dispatchEntity.dp_innerlabel_chart = Session["InnerLabelChartNum"].ToString();
            //dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            //dispatchEntity.dp_request_type = Session["requestype"].ToString();
            //objBAL.saveInnerLabel(dispatchEntity);
            Session["SetInnerLabelDetailsTab"] = disObj;
            DispatchSKU dispatchObj = new DispatchSKU();
            DispatchSKU sessionObj = (DispatchSKU)Session["SetInnerLabelDetailsTab"];
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();

            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = ""; string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
                fileTypeCode = "SE";
                FileSubType = "T";
                fileSize = Session["MaxFileSizeCode"].ToString();
            }

            if (Session["MaxFileSizeCode"] != null && Session["BrandId"] != null && Session["CoustomerId"] != null
                && Session["ShipToCountryCode"] != null)
            {
                fileTypeCode = "SE";
                FileSubType = "T";
                fileSize = Session["MaxFileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = "N";
                detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
                Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
                detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);
                detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
                dispatchObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;
                dispatchObj.dp_innerlabel_chart = Session["InnerLabelChartNum"].ToString();
                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
                dispatchEntity.dp_innerlabel_chart = Session["InnerLabelChartNum"].ToString();
                dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchEntity.dp_request_type = Session["requestype"].ToString();
                objBAL.saveInnerLabel(dispatchEntity);
                return Json(new { url = @Url.Action("getSetOuterTabDetails", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("_ViewOuterDetails");
        }

        public ActionResult getSetInnerLabelTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            DispatchSKU sessionObj = (DispatchSKU)Session["SetInnerLabelDetailsTab"];
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            InnerLabelDetailsModel detailsModel = new InnerLabelDetailsModel();

            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
                fileTypeCode = "SE";
                FileSubType = "T";
            }
            fileSize = Session["MaxFileSizeCode"].ToString();
            if (Session["MaxFileSizeCode"] != null && Session["BrandId"] != null && Session["CoustomerId"] != null
                && Session["ShipToCountryCode"] != null)
            {
                fileTypeCode = "SE";
                FileSubType = "T";
                fileSize = Session["MaxFileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = "N";
            }
            if (Session["SetInnerLabelDetailsTab"] != null)
            {
                detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
                Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
                detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);
                detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
                dispatchObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;

                dispatchObj.dp_innerlabel_chart = Session["InnerLabelChartNum"].ToString();
                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_SetInnerLabel", dispatchObj);
            }
            return RedirectToAction("SetInnerLabel");
        }
        [HttpGet]
        public ActionResult SetInnerLabelDetails()
        {
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            DispatchSKU disPackObj = new DispatchSKU();
            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
                fileTypeCode = "SE";
                FileSubType = "T";
            }
            fileSize = Session["MaxFileSizeCode"].ToString();
            if (Session["MaxFileSizeCode"] != null && Session["BrandId"] != null && Session["CoustomerId"] != null
              && Session["ShipToCountryCode"] != null)

            {
                fileTypeCode = "SE";
                FileSubType = "T";
                fileSize = Session["MaxFileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = "N";
            }
            detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
            Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
            detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);
            detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
            disPackObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;

            return PartialView("_InnerLabelDetails", disPackObj);
        }

        #endregion

        #region Set Outer

        public ActionResult SetOuter()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "outer";
            dispatchObj.outerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
            return PartialView("_SetOuter", dispatchObj);
        }

        [HttpPost]
        public ActionResult SaveSkuSetOuter(string dp_outerbox_type, string dp_outerbox_chart)
        {
            DispatchSKUEntity dpobj = new DispatchSKUEntity();

            dpobj.dp_outerbox_type = dp_outerbox_type;
            dpobj.dp_outerbox_chart = dp_outerbox_chart;
            dpobj.dp_request_type = "S";
            dpobj.dp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            dpobj.flag = "updateskuSetOuterbox";
            Session["SetOuterDetailsTab"] = dpobj;
            int j = objBAL.SaveSkuSetOuter(dpobj);
            return Json(new { url = @Url.Action("getSetPalletTabDetails", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSetOuterTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "outer_box";
            dispatchObj.modellist = getEditOuterBox(ReqNo, flag);
            Session["SetOuterDetailsTab"] = dispatchObj.modellist;

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["SetOuterDetailsTab"];


            if (Session["SetOuterDetailsTab"] != null && sessionObj.Count > 0)
            {

                string usedFor = "outer";
                dispatchObj.outerBoxMasterialList = objBAL.getWrappingTypeList(usedFor);
                dispatchObj.outerbox_chart = sessionObj[0].dp_outerbox_chart;
                dispatchObj.dp_outerbox_chart = sessionObj[0].dp_outerbox_chart;
                dispatchObj.dp_outerbox_type = sessionObj[0].dp_outerbox_type;

                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_SetOuter", dispatchObj);
            }
            return RedirectToAction("_SetOuter");
        }

        [HttpGet]
        public ActionResult ViewSetOuterDetails(string OuterChart)
        {
            string fileTypeCode = "SE";
            string FileSubType = "T";
            string maxsize = objBAL.CheckMaxSizeCode(Convert.ToInt32(Session["requestId"].ToString()), Session["requestype"].ToString());
            if (maxsize != "")
            {
                var maxcode = maxsize.Substring(2);
                var f = maxcode[0];
                var l = maxcode[1];
                string a = Convert.ToString(f);
                string b = Convert.ToString(l);
                string Mxcode = a + b;
                Session["MaxFileSizeCode"] = Mxcode;
                fileTypeCode = "SE";
                FileSubType = "T";
            }
            string fileSize = Session["MaxFileSizeCode"].ToString();

            OuterDetailsModel obj = new OuterDetailsModel();
            OuterDetailsEntity entity = new OuterDetailsEntity();
            obj.obd_box_id = objBAL.getOuterBoxId(OuterChart);

            entity.outerDeatialsEntityList = objBAL.getOuterDetials(obj.obd_box_id);
            if (OuterChart != "" || OuterChart != null)
            {
                obj.OterChartList = objBAL.GetOuterChartlist(OuterChart);
            }

            obj.outerDeatialsEntityList = entity.outerDeatialsEntityList;
            obj.filesize = fileSize;
            obj.filetype = fileTypeCode;
            obj.filesubtype = FileSubType;

            return PartialView("_ViewOuterDetails", obj);
        }
        #endregion

        #region Set Pallet
        public ActionResult SetPallet()
        {

            DispatchSKU dispatchObj = new DispatchSKU();
            string usedFor = "PALLET";
            dispatchObj.palletMaterialList = objBAL.getWrappingTypeList(usedFor);
            return PartialView("_SetPallet", dispatchObj);
        }

        [HttpPost]
        public ActionResult SaveSkuSetPallet(string dp_pallet_type, string dp_pallet_chart)
        {
            DispatchSKUEntity dpobj = new DispatchSKUEntity();

            dpobj.dp_pallet_type = dp_pallet_type;
            dpobj.dp_pallet_chart = dp_pallet_chart;
            dpobj.dp_request_type = "S";
            dpobj.dp_request_id = Convert.ToInt32(Session["requestId"].ToString());
            dpobj.flag = "updateskuSetPallet";
            Session["SetPalletTab"] = dpobj;
            int j = objBAL.SaveSkuSetPallet(dpobj);
            return Json(new { url = @Url.Action("getSetPackingDetailsTab", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSetPalletTabDetails()
        {
            DispatchSKU dispatchObj = new DispatchSKU();
            string msg = ViewBag.Message;

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "pallet_details";
            dispatchObj.modellist = getEditPallet(ReqNo, flag);
            Session["SetPalletTab"] = dispatchObj.modellist;

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["SetPalletTab"];

            if (Session["SetPalletTab"] != null && sessionObj.Count > 0)
            {
                string usedFor = "PALLET";
                dispatchObj.palletMaterialList = objBAL.getWrappingTypeList(usedFor);
                dispatchObj.dp_pallet_chart = sessionObj[0].dp_pallet_chart;
                dispatchObj.pallet_chart = sessionObj[0].dp_pallet_chart;
                dispatchObj.dp_pallet_type = sessionObj[0].dp_pallet_type;
                dispatchObj.dp_request_id = Convert.ToInt32(Session["requestId"]);
                dispatchObj.dp_request_type = Session["requestype"].ToString();
                return PartialView("_SetPallet", dispatchObj);
            }
            return RedirectToAction("SetPallet");
        }

        [HttpGet]
        public ActionResult ViewSetPalletDetails(string pallet_chart)
        {
            PalletDetailsModel palletDetailsModelObj = new PalletDetailsModel();

            string fileTypeCode = "SE";
            string FileSubType = "T";
            string fileSize = Session["MaxFileSizeCode"].ToString();


            PalletDetailsEntity entity = new PalletDetailsEntity();

            entity.pd_pallet_id = objBAL.getPalletId(pallet_chart);
            entity.palletDetailsEntityList = objBAL.getPalletDetials(entity.pd_pallet_id);

            palletDetailsModelObj.pm_pallet_chartimg = objBAL.getPalletImageName(pallet_chart);

            palletDetailsModelObj.palletDetailsEntityList = entity.palletDetailsEntityList;
            palletDetailsModelObj.filesize = fileSize;
            palletDetailsModelObj.filetype = fileTypeCode;
            palletDetailsModelObj.filesubtype = FileSubType;

            return PartialView("_PalletDetailsView", palletDetailsModelObj);
        }

        #endregion

        #region Set Packing Details
        public ActionResult SetPackingDetails()
        {
            return View("_SetPackingDetails");
        }
        [HttpPost]
        public ActionResult UpdatePackingDetails(DispatchSKU disObj)
        {

            DispatchSKUEntity dispatchEntity = new DispatchSKUEntity();
            Session["SetPackingDetailsTab"] = disObj;
            dispatchEntity.dp_request_id = Convert.ToInt32(Session["requestId"]);
            dispatchEntity.dp_request_type = Session["requestype"].ToString();

            if (disObj.temp_barcode_flg == "Yes")
            {
                dispatchEntity.dp_barcode_flg = true;
            }
            else dispatchEntity.dp_barcode_flg = false;
            if (disObj.temp_barcodeouter_flg == "Yes")
            {
                dispatchEntity.dp_barcodeouter_flg = true;
            }
            else dispatchEntity.dp_barcodeouter_flg = false;
            
            if (disObj.temp_hologram_flg == "Yes")
            {
                dispatchEntity.dp_hologram_flg = true;
            }
            else dispatchEntity.dp_hologram_flg = false;
            if (disObj.temp_fumigation_flg == "Yes")
            {
                dispatchEntity.dp_fumigation_flg = true;
            }
            else dispatchEntity.dp_fumigation_flg = false;
            if (disObj.temp_india_flg == "Yes")
            {
                dispatchEntity.dp_india_flg = true;
            }
            else dispatchEntity.dp_india_flg = false;
            if (disObj.temp_lineno_flg == "Yes")
            {
                dispatchEntity.dp_lineno_flg = true;
            }
            else dispatchEntity.dp_lineno_flg = false;
            if (disObj.temp_polybag_flg == "Yes")
            {
                dispatchEntity.dp_polybag_flg = true;
            }
            else dispatchEntity.dp_polybag_flg = false;
            if (disObj.temp_prcstkr_flg == "Yes")
            {
                dispatchEntity.dp_prcstkr_flg = true;
            }
            else dispatchEntity.dp_prcstkr_flg = false;
            if (disObj.temp_qltyseal_flg == "Yes")
            {
                dispatchEntity.dp_qltyseal_flg = true;
            }
            else dispatchEntity.dp_qltyseal_flg = false;
            if (disObj.temp_silicagel_flg == "Yes")
            {
                dispatchEntity.dp_silicagel_flg = true;
            }
            else dispatchEntity.dp_silicagel_flg = false;
            if (disObj.temp_promotionalitem_flg == "Yes")
                dispatchEntity.dp_promo_flg = true;
            else
                dispatchEntity.dp_promo_flg = false;
            if (disObj.temp_strap == "Yes")
                dispatchEntity.dp_strap_flg = disObj.temp_strap;
            else
                dispatchEntity.dp_strap_flg = disObj.temp_strap;
            if (disObj.temp_cellotape_flg == "Yes")
                dispatchEntity.dp_cellotape_flg = true;
            else
                dispatchEntity.dp_cellotape_flg = false;
            if (disObj.temp_shrinkwrap_flg == "Yes")
                dispatchEntity.dp_shrinkwrap_flg = true;
            else dispatchEntity.dp_shrinkwrap_flg = false;
            if (disObj.temp_decicant_flg == "Yes")
                dispatchEntity.dp_decicant_flg = true;
            else
                dispatchEntity.dp_decicant_flg = false;
            if (disObj.temp_paperwool_flg == "Yes")
                dispatchEntity.dp_paperwool_flg = true;
            else
                dispatchEntity.dp_paperwool_flg = false;

            #region code added for Outer/Pallet Marking

            dispatchEntity.dp_outer_mrking_type = disObj.dp_outer_mrking_type;
            if (dispatchEntity.dp_outer_mrking_type == "No")
            {
                dispatchEntity.dp_outer_mrking_img = "NA.jpg";
            }
            else
            {
                HttpPostedFileBase photo = Request.Files["outer"];
                if (photo != null)
                {
                    dispatchEntity.dp_outer_mrking_img = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/PackingDetails/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename + ".jpg"));
                    }
                }
            }


            dispatchEntity.dp_pallet_mrking_type = disObj.dp_pallet_mrking_type;
            if (dispatchEntity.dp_pallet_mrking_type == "No")
            {
                dispatchEntity.dp_pallet_mrking_img = "NA.jpg";
            }
            else
            {
                HttpPostedFileBase photo = Request.Files["pallet"];
                if (photo != null)
                {
                    dispatchEntity.dp_pallet_mrking_img = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/PackingDetails/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename + ".jpg"));
                    }
                    
                }
            }

            #endregion
            if (disObj.temp_blko_flg == "Yes")
                dispatchEntity.dp_blko_flg = true;
            else
                dispatchEntity.dp_blko_flg = false;
            if (disObj.temp_pono_flg == "Yes")
                dispatchEntity.dp_pono_flg = true;
            else
                dispatchEntity.dp_pono_flg = false;

            dispatchEntity.dp_pkg_remarks = disObj.temp_pkg_remarks;
            dispatchEntity.dp_tangcolor = disObj.temp_tangcolor;

            objBAL.savePackingDetailsToDispatchSKU(dispatchEntity);
            return RedirectToAction("SetLabelLayOut");
        }

        public ActionResult getSetPackingDetailsTab()
        {
            DispatchSKU disObj = new DispatchSKU();
            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "packing_details";
            disObj.modellist = getEditPackingDetails(ReqNo, flag);
            Session["SetPackingDetailsTab"] = disObj.modellist;

            List<DispatchSKU> sessionObj = (List<DispatchSKU>)Session["SetPackingDetailsTab"];

            if (Session["SetPackingDetailsTab"] != null && sessionObj.Count > 0)
            {
                disObj.temp_barcode_flg = sessionObj[0].dp_barcode_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_barcodeouter_flg = sessionObj[0].dp_barcodeouter_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_fumigation_flg = sessionObj[0].dp_fumigation_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_hologram_flg = sessionObj[0].dp_hologram_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_india_flg = sessionObj[0].dp_india_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_lineno_flg = sessionObj[0].dp_lineno_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_polybag_flg = sessionObj[0].dp_polybag_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_prcstkr_flg = sessionObj[0].dp_prcstkr_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_qltyseal_flg = sessionObj[0].dp_qltyseal_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_silicagel_flg = sessionObj[0].dp_silicagel_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_promotionalitem_flg = sessionObj[0].dp_promotionalitem_flg.ToString() == "True" ? "Yes" : "No";
                if (sessionObj[0].dp_strap == null)
                {
                    disObj.temp_strap = "Yes";
                }
                else
                {
                    if (sessionObj[0].dp_strap.ToString() == "Yes")
                    {
                        disObj.temp_strap = "Yes";
                    }
                    else
                    {
                        disObj.temp_strap = "No";
                    }

                }
                disObj.temp_cellotape_flg = sessionObj[0].dp_cellotape_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_shrinkwrap_flg = sessionObj[0].dp_shrinkwrap_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_decicant_flg = sessionObj[0].dp_decicant_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_paperwool_flg = sessionObj[0].dp_paperwool_flg.ToString() == "True" ? "Yes" : "No";
                if (sessionObj[0].dp_outer_mrking_type == null)
                {
                    disObj.dp_outer_mrking_type = "No";
                }
                else
                {
                    disObj.dp_outer_mrking_type = sessionObj[0].dp_outer_mrking_type.ToString();
                }
                if (sessionObj[0].dp_pallet_mrking_type ==null)
                {
                    disObj.dp_pallet_mrking_type = "No";
                }
                else
                {
                    disObj.dp_pallet_mrking_type = sessionObj[0].dp_pallet_mrking_type.ToString();
                }

                //disObj.dp_outer_mrking_type = sessionObj[0].dp_outer_mrking_type.ToString();
                //disObj.dp_pallet_mrking_type = sessionObj[0].dp_pallet_mrking_type.ToString();
                disObj.temp_pono_flg = sessionObj[0].dp_pono_flg.ToString() == "True" ? "Yes" : "No";
                disObj.temp_blko_flg = sessionObj[0].dp_blko_flg.ToString() == "True" ? "Yes" : "No";
                if (sessionObj[0].dp_tangcolor != null)
                {
                    disObj.temp_tangcolor = sessionObj[0].dp_tangcolor.ToString();
                }
                if (sessionObj[0].dp_pkg_remarks != null)
                {
                    disObj.temp_pkg_remarks = sessionObj[0].dp_pkg_remarks.ToString();
                }
                return PartialView("_SetPackingDetails", disObj);

            }
            return RedirectToAction("SetPackingDetails");
        }
        #endregion

        #region Set Label Layout
        public ActionResult SetLabelLayOut()
        {
            return PartialView("_SetLabelLayOut");
        }
        [HttpPost]
        public ActionResult SaveSkuSetlabellayout(DispatchSKU disObj)
        {

            return Json(new { url = @Url.Action("getSetOtherDetailsTab", "DispatchDetails") }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult SetLabelLayOutDetails()
        {
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            InnerLabelDetailsModel detailsModel = new InnerLabelDetailsModel();
            DispatchSKU disPackObj = new DispatchSKU();
            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            CustomerMasterModel sessionObj = (CustomerMasterModel)Session["SetCustomerDetailsTab"];
            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "packing_details";
            disPackObj.modellist = getEditPackingDetails(ReqNo, flag);
            Session["SetPackingDetailsTab"] = disPackObj.modellist;

            List<DispatchSKU> sessionObj1 = (List<DispatchSKU>)Session["SetPackingDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            if (Session["BrandId"] != null && Session["CoustomerId"] != null && Session["ShipToCountryCode"] != null
                && Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = "SE";
                FileSubType = "T";
                fileSize = Session["MaxFileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = "N";
            }

            detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
            Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
            detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);

            detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
            disPackObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;
            disPackObj.CustomerName = sessionObj.costomerName;
            disPackObj.BrandName = sessionObj.brandName;
            disPackObj.temp_barcode_flg = sessionObj1[0].dp_barcode_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_barcodeouter_flg = sessionObj1[0].dp_barcodeouter_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_fumigation_flg = sessionObj1[0].dp_fumigation_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_hologram_flg = sessionObj1[0].dp_hologram_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_india_flg = sessionObj1[0].dp_india_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_lineno_flg = sessionObj1[0].dp_lineno_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_polybag_flg = sessionObj1[0].dp_polybag_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_prcstkr_flg = sessionObj1[0].dp_prcstkr_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_qltyseal_flg = sessionObj1[0].dp_qltyseal_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_silicagel_flg = sessionObj1[0].dp_silicagel_flg.ToString() == "True" ? "Yes" : "No";

            DataTable dt = new DataTable();
            if (Convert.ToInt32(Session["requestId"]) != 0)
            {
                dt = objBAL.getdispatchdetails(Convert.ToInt32(Session["requestId"]));
            }
            if (dt.Rows.Count > 0)
            {
                disPackObj.dp_handle_chart = dt.Rows[0]["dp_handle_chart"].ToString();
                disPackObj.dp_wrapping_chart = dt.Rows[0]["dp_wrapping_chart"].ToString();
                disPackObj.dp_innerbox_chart = dt.Rows[0]["dp_innerbox_chart"].ToString();
                disPackObj.dp_outerbox_chart = dt.Rows[0]["dp_outerbox_chart"].ToString();
                disPackObj.dp_pallet_chart = dt.Rows[0]["dp_pallet_chart"].ToString();
                if (disPackObj.dp_handle_chart != "" || disPackObj.dp_handle_chart != null)
                {
                    disPackObj.HandleDetailsEntityList = objBAL.gethandleimgpath(disPackObj.dp_handle_chart);
                }
                if (disPackObj.dp_wrapping_chart != "" || disPackObj.dp_wrapping_chart != null)
                {
                    disPackObj.WrapperdetailsList = objBAL.getwrappinglist(disPackObj.dp_wrapping_chart);
                }
                if (disPackObj.dp_innerbox_chart != "" || disPackObj.dp_innerbox_chart != null)
                {
                    disPackObj.innerboxlist = objBAL.getinnerboxlist(disPackObj.dp_innerbox_chart, FileSubType, fileTypeCode, fileSize);
                }
                if (disPackObj.dp_outerbox_chart != "" || disPackObj.dp_outerbox_chart != null)
                {
                    disPackObj.OterChartList = objBAL.GetOuterChartlist(disPackObj.dp_outerbox_chart);
                }
                if (disPackObj.dp_pallet_chart != "" || disPackObj.dp_pallet_chart != null)
                {
                    disPackObj.PalletList = objBAL.GetPalletList(disPackObj.dp_pallet_chart);
                }
            }
            return PartialView("_SetLabelLayOutDetails", disPackObj);
        }

        #endregion

        #region Manage Submission

        public ActionResult getSetOtherDetailsTab()
        {
            PartNumOtherDetailsModel partObj = new PartNumOtherDetailsModel();

            string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string flag = "other_details";
            partObj.otherdetailList = getEditOtherDetails(ReqNo, flag);
            Session["SetOtherDetailsTab"] = partObj.otherdetailList;



            List<PartNumOtherDetailsModel> sessionObj = (List<PartNumOtherDetailsModel>)Session["SetOtherDetailsTab"];

            if (Session["SetOtherDetailsTab"] != null && partObj.otherdetailList.Count > 0)
            {
                partObj.othdt_request_id = Convert.ToInt32(Session["requestId"]);
                partObj.othdt_request_type = Session["requestype"].ToString();
                partObj.othdt_prod_cost_onetm = sessionObj[0].othdt_prod_cost_onetm;
                partObj.othdt_prod_cost_run = sessionObj[0].othdt_prod_cost_run;
                partObj.othdt_dispatch_onetm = sessionObj[0].othdt_dispatch_onetm;
                partObj.othdt_dispatch_run = sessionObj[0].othdt_dispatch_run;
                partObj.othdt_req_startdt = sessionObj[0].othdt_req_startdt;
                partObj.othdt_req_enddt = sessionObj[0].othdt_req_enddt;
                partObj.othdt_req_enddt_current_sku = sessionObj[0].othdt_req_enddt_current_sku;

                ViewBag.othdt_req_startdt = Convert.ToDateTime(sessionObj[0].othdt_req_startdt);
                ViewBag.othdt_req_enddt = Convert.ToDateTime(sessionObj[0].othdt_req_enddt);
                return View("ManageSubmission", partObj);
            }

            return RedirectToAction("ManageSubmission");
        }
        public ActionResult ManageSubmission()
        {
            SkuSelectionModel ssmpobj = new SkuSelectionModel();
            int requestId = Convert.ToInt32(Session["requestId"]);
            ssmpobj.ssmskulist = get_ssmskulist(requestId);

            if ((ssmpobj.ssmskulist != null) && (ssmpobj.ssmskulist.Any()))
            {
                DateTime Stmax = ssmpobj.ssmskulist.Min(x => x.rs_sku_start_date);
                DateTime Endmin = ssmpobj.ssmskulist.Min(x => x.rs_sku_end_date);
                DateTime todaydate = DateTime.Now;
                int cmp = Stmax.CompareTo(todaydate);
                if (cmp > 0)
                {
                    ViewBag.StartMaxDate = Stmax.ToString("yyyy-MM-dd");
                }
                else if (cmp < 0)
                {
                    ViewBag.StartMaxDate = todaydate.ToString("yyyy-MM-dd");
                }
                else
                {
                    ViewBag.StartMaxDate = Stmax.ToString("yyyy-MM-dd");
                }
                ViewBag.EndMaxDate = Endmin.ToString("yyyy-MM-dd");
            }
            return View();
        }

        protected List<SkuSelectionModel> get_ssmskulist(int requestId)
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            string flag = "ssmskulist";
            DataTable dt = objBAL.get_ssmskulist(flag, requestId);
            List<SkuSelectionModel> ssmskulist = new List<SkuSelectionModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                ssmskulist = dt.AsEnumerable().Select(item => new SkuSelectionModel()
                {
                    rs_sku_start_date = item.Field<DateTime>("rs_sku_start_date"),
                    rs_sku_end_date = item.Field<DateTime>("rs_sku_end_date"),
                    rs_request_id = item.Field<int>("rs_request_id")
                }).ToList();
            }


            return ssmskulist;
        }


        [HttpPost]
        public ActionResult SetSubmissionDetails(PartNumOtherDetailsModel partObj, FormCollection form)
        {
            Session["SetOtherDetailsTab"] = partObj;
            string button = form["actionType"].ToString();
            Session["SetOtherDetailsTab"] = partObj;
            PartNumOtherDetailsEntity partEntity = new PartNumOtherDetailsEntity();
            partEntity.othdt_request_id = Convert.ToInt32(Session["requestId"]);
            partEntity.othdt_request_type = Session["requestype"].ToString();
            partEntity.othdt_prod_cost_onetm = partObj.othdt_prod_cost_onetm;
            partEntity.othdt_prod_cost_run = partObj.othdt_prod_cost_run;
            partEntity.othdt_dispatch_onetm = partObj.othdt_dispatch_onetm;
            partEntity.othdt_dispatch_run = partObj.othdt_dispatch_run;
            partEntity.othdt_req_startdt = partObj.othdt_req_startdt;
            partEntity.othdt_req_enddt = partObj.othdt_req_enddt;

            TempData["sartdate"] = partObj.othdt_req_startdt;
            TempData["enddate"] = partObj.othdt_req_enddt;

            if (button == "Save")
            {

                if (form["selectradio"] != null)
                {
                    string radio = form["selectradio"].ToString();
                    if (radio == "NO")
                    {
                        objBAL.CreateCloneReq(null, Convert.ToInt32(Session["requestId"]), "clone_with_new");
                        partEntity.othdt_request_type = "S";
                        Session["requestype"] = "S";
                    }
                    else if (radio == "YES")
                    {
                        partEntity.othdt_req_enddt_current_sku = partObj.othdt_req_enddt_current_sku;
                    }
                }
                objBAL.saveSetOtherDetails(partEntity);
                return RedirectToAction("getSetOtherDetailsTab");

            }
            else if (button == "Sub")
            {

                objBAL.saveOtherDetails(partEntity);
                objBAL.FinalSubmit(partEntity);
                Session["SetOtherDetailsTab"] = null;
                Session["SetPalletTab"] = null;
                Session["SetOuterDetailsTab"] = null;
                Session["SetInnerLabelDetailsTab"] = null;
                Session["SetInnerBoxDetailsTab"] = null;
                Session["SetWrapperDetailsTab"] = null;
                Session["SetCustomerDetailsTab"] = null;
                Session["StampDetailsTab"] = null;
            }
            return RedirectToAction("ChangeNote", "ChangeNotes");
        }

        #endregion


        #endregion
       

    }
}