﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class DispatchMasterController : Controller
    {
        // GET: DispatchMaster
        public ActionResult Handle()
        {
            return View();
        }
      

        public ActionResult Stamp()
        {
            return View();
        }

        public ActionResult Wrapper()
        {
            return View();
        }

        public ActionResult InnerBox()
        {
            return View();
        }

        public ActionResult InnerLabel()
        {
            return View();
        }

        public ActionResult Outer()
        {
            return View();
        }

        public ActionResult Pallet()
        {
            return View();
        }

        public ActionResult OverviewDispatch()
        {
            return View();
        }
    }
}