﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFilesBAL;
using JKFIlesEntity;
using System.Data;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;


namespace JKFiles.WebApp.Controllers
{
    public class CutspectTestController : Controller
    {
        BAL objBAL = new BAL();
        #region by Swapnil
        // GET: CutSpecmaster
        public ActionResult Index()
        {
            //// string flag = "fulllist";
            //// DataTable dt = objBAL.getCutSpecMaster(flag);
            //// return View(dt);

            ////Swapnil code

            ////ProductionMasterModel objProdModel = new ProductionMasterModel();
            ////objProdModel.filesizelist = getFileSize();
            ////objProdModel.filetTypelist = getFileType();
            //// objcutspecs.cutSpecsNoActiveList = objBAL.getCutSpecNoActiveMaster();
            //// objcutspecs.cutSpecsNoApproveList = objBAL.getCutSpecNoApproveMaster();
            //int pageSize = 10;
            //int pageIndex = 1;
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //IPagedList<CutSpecsEntity> cst = null;
            //CutSpecsEntity objcutspecs = new CutSpecsEntity();
            //List<CutSpecsEntity> cstlist = new List<CutSpecsEntity>();
            //if ((string.IsNullOrEmpty(selectedCutStandardName)) && (string.IsNullOrWhiteSpace(ftypecode)) && (string.IsNullOrWhiteSpace(filetype)) && (string.IsNullOrWhiteSpace(cuttype)) && (string.IsNullOrWhiteSpace(cutstand)))
            //{

            //    //int pageSize = 10;
            //    //int pageIndex = 1;
            //    //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //    //IPagedList<CutSpecsEntity> cst = null;


            //    cstlist = objBAL.getCutSpecMaster();
            //    objcutspecs.cutSpecsEntityList = cstlist;
            //    cst = cstlist.ToPagedList(pageIndex, pageSize);
            //    // return View(cst);
            //}

            //else
            //{

            //    cstlist = objBAL.getCutSpecMastersearch(selectedCutStandardName);
            //    objcutspecs.cutSpecsEntityList = cstlist;
            //    cst = cstlist.ToPagedList(pageIndex, pageSize);
            //}




            ////  objcutspecs.cutSpecsEntityList = objBAL.getCutSpecMaster();

            //return View(cst);
            return View();
        }


        //Yogesh Code
        public ActionResult cutspectmastr(string selectedCutStandardName, string ftypecode, string filetype, string cuttype, string cutstand, int? page)
        {
            // string flag = "fulllist";
            // DataTable dt = objBAL.getCutSpecMaster(flag);
            // return View(dt);

            //Swapnil code

            //ProductionMasterModel objProdModel = new ProductionMasterModel();
            //objProdModel.filesizelist = getFileSize();
            //objProdModel.filetTypelist = getFileType();
            // objcutspecs.cutSpecsNoActiveList = objBAL.getCutSpecNoActiveMaster();
            // objcutspecs.cutSpecsNoApproveList = objBAL.getCutSpecNoApproveMaster();
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<CutSpecsEntity> cst = null;
            CutSpecsEntity objcutspecs = new CutSpecsEntity();
            List<CutSpecsEntity> cstlist = new List<CutSpecsEntity>();
           
            cstlist = objBAL.getCutSpecMaster();
            
            if (!String.IsNullOrEmpty(selectedCutStandardName))
            {
                ViewBag.selectedCutStandardName = selectedCutStandardName;
                cstlist = cstlist.Where(x => x.filesizecode == selectedCutStandardName).ToList();
            }

            if (!String.IsNullOrEmpty(ftypecode))
            {
                ViewBag.ftypecode = ftypecode;
                cstlist = cstlist.Where(x => x.fcl_ftype_code == ftypecode).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                cstlist = cstlist.Where(x => x.filetype == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(cuttype))
            {
                ViewBag.cuttype = cuttype;
                cstlist = cstlist.Where(x => x.cuttype == cuttype).ToList();
            }

            if (!String.IsNullOrEmpty(cutstand))
            {
                ViewBag.cutstand = cutstand;
                cstlist = cstlist.Where(x => x.cutStandardName == cutstand).ToList();
            }



            // cstlist = objBAL.getCutSpecMastersearch(selectedCutStandardName);
            objcutspecs.cutSpecsEntityList = cstlist;
                cst = cstlist.ToPagedList(pageIndex, pageSize);
               
           
            
            return PartialView("_cutspectmastr", cst);

        }

     
        public ActionResult NewRequest()
        {

            //Swapnil code
            CutSpecsEntity objcutspecs = new CutSpecsEntity();

            objcutspecs.cutSpecsNoActiveList = objBAL.getCutSpecNoActiveMaster();

            return PartialView("_NewRequest", objcutspecs);

        }

        public ActionResult ApprovedRequest()
        {

            //Swapnil code
            CutSpecsEntity objcutspecs = new CutSpecsEntity();

            objcutspecs.cutSpecsNoApproveList = objBAL.getCutSpecNoApproveMaster();

            return PartialView("_ApprovedRequest", objcutspecs);

        }

        public ActionResult Cutspectmaster()
        {

            //Swapnil code
            CutSpecsEntity objcutspecs = new CutSpecsEntity();

           // objcutspecs.cutSpecsEntityList = objBAL.getCutSpecMaster();


            objcutspecs.listsizecode = get_size();
            objcutspecs.listtype = get_type();
            objcutspecs.listsubtype = get_subtype();
            objcutspecs.listfiletypecode = get_filetypecode();
            objcutspecs.listcuttype = get_cuttype();
            objcutspecs.listcutstandard = get_cutstandrd();
            return PartialView("_Cutspectmaster", objcutspecs);

        }

        public List<CutSpecsEntity> get_cutstandrd()
        {
            string flag = "getcutstandard";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.cut_standard = (dt.Rows[i]["Cut_Standard"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<CutSpecsEntity> get_cuttype()
        {
            string flag = "getcuttype";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.cut_type = (dt.Rows[i]["Cut_Type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<CutSpecsEntity> get_filetypecode()
        {
            string flag = "getfiletypecode";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.filetypecode = (dt.Rows[i]["File_Type_Code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<CutSpecsEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            CutSpecsEntity modelobj = new CutSpecsEntity();
            List<CutSpecsEntity> list = new List<CutSpecsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CutSpecsEntity obj = new CutSpecsEntity();
                    obj.fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }


        [HttpPost]
        public ActionResult CutSpecsDetails(CutSpecsEntity model)
        {
            objBAL.CutSpecssaveactive(model);
            //custModelObj.custList = objBAL.getCustomerList();
            //custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
            //custModelObj.custNoApproveList = objBAL.getCustListNoApprove();
            return RedirectToAction("Index");
        }
        public ActionResult UpdatecustspecApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdatecutspecApprove(ID, type);
            }
            return RedirectToAction("Index");
        }
        public ActionResult Cutspecs(string Id)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            CutSpecsEntity cutspecModelObj = new CutSpecsEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Cutspecs_master_details_bycode", con);
            SqlCommand cmd1 = new SqlCommand("sp_get_Cutspecs_values_details_bycode", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd1.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@id", Id);
                cmd.Parameters.Add(param);
                cmd1.Parameters.Add(param1);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                con.Close();
                da.Fill(dt);
                da1.Fill(dt1);
                cutspecModelObj.fcl_ftype_code = dt.Rows[0]["File Code"].ToString();
                cutspecModelObj.filesize = Convert.ToDecimal(dt.Rows[0]["Nominal Length in inches"].ToString());
                cutspecModelObj.cuttype = dt.Rows[0]["Cut Type"].ToString();
                cutspecModelObj.cutStandardName = dt.Rows[0]["Cut Standard"].ToString();
                cutspecModelObj.upcutrange = dt.Rows[0]["UPCUT"].ToString();
                cutspecModelObj.edcutrange = dt.Rows[0]["EDGE"].ToString();
                cutspecModelObj.ovcutrange = dt.Rows[0]["OVER"].ToString();
                cutspecModelObj.upcutvalue = Convert.ToDecimal(dt1.Rows[0]["UPCUT"].ToString());
                cutspecModelObj.edcutvalue = Convert.ToDecimal(dt1.Rows[0]["EDGE"].ToString());
                cutspecModelObj.ovcutvalue = Convert.ToDecimal(dt1.Rows[0]["OVER"].ToString());
                cutspecModelObj.prc_request_id = Convert.ToInt16(dt.Rows[0]["tpi_request_id"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_cutspecsDetails", cutspecModelObj);
        }
        #endregion
    }
}