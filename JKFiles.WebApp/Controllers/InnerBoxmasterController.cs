﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class InnerBoxmasterController : Controller
    {
        BAL objBAL = new BAL();
        InnerBoxMasterEntity ib = new InnerBoxMasterEntity();
        [HttpPost]
        public ActionResult InnerboxEditactive(List<InnerBoxDetailsEntity> InnerBoxList)
        {

            InnerBoxDetailsEntity model = new InnerBoxDetailsEntity();
            //model.ibd_request_id = Convert.ToInt16(Session["requestId"]);
            //model.ibd_request_type = Session["requestype"].ToString();
            if (InnerBoxList.Count >= 1)
            {
                model.ib_chartnum = InnerBoxList[0].ib_chartnum;
                model.ib_chartimg = InnerBoxList[0].ib_chartimg;
                model.ib_box_type = InnerBoxList[0].ib_box_type;
                int count = InnerBoxList.Count;
                // count = count / 2;
                for (int i = 0; i < count; i++)
                {
                    model.ibd_pkg_parm_name = InnerBoxList[i].ibd_pkg_parm_name;
                    model.ibd_pkg_parm_code = InnerBoxList[i].ibd_pkg_parm_code;
                    model.ibd_pkg_parm_dwgvalue = InnerBoxList[i].ibd_pkg_parm_dwgvalue;
                    model.ibd_pkg_parm_scrnvalue = InnerBoxList[i].ibd_pkg_parm_scrnvalue;
                    model.ibd_request_id = InnerBoxList[i].ibd_request_id;
                    model.ibd_request_type = InnerBoxList[i].ibd_request_type;
                    objBAL.EditInnerboxSinglesavebyrow(model);
                }
                objBAL.InnerboxEditactive(model);


                string subject = "";
                string body = "";

                subject = "Request No:  " + model.ibd_request_type + model.ibd_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.ibd_request_type + model.ibd_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",InnerBox Master updated.Needs Approval.";

                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                     objBAL.sendingmail(email,body,subject);
                }
            }


            return RedirectToAction("_InnerBox");
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "InnerBox";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "InnerBoxMaster") }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateInnerBoxApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                if(ID!='E')
                {
                    objBAL.UpdateInnerBoxApprove(ID, type);

                    string subject = "";
                    string body = "";
                    subject = "Request No:  " + type + ID + " and " +
                           "Dispatch Master Approval";
                    body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                        + ",InnerBox Master Approved.";


                    string flag = "getMailByRole";
                    DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                    if (dtEmail.Rows.Count > 0 && dtEmail != null)
                    {
                        string email = dtEmail.Rows[0]["User_Id"].ToString();
                        objBAL.sendingmail(email, body, subject);
                    }
                }


              
            }
            return Json(new { url = @Url.Action("Index", "InnerBoxMaster") }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        // GET: InnerBoxmaster
        public ActionResult Index()
        {
            ib.innerMasterEntityList = getinnerboxmstr();
            return View("Index", ib);
        }

        public ActionResult NewRequest()
        {
            ib.innerMasterNoActiveEntityList = getinnerboxnoactive();
            return PartialView("_NewRequest", ib);

        }

        public ActionResult ApprovedRequest()
        {
            ib.innerMasterNoApproveEntityList = getinnerboxnoapprove();
            return PartialView("_ApprovedRequest", ib);

        }
        [HttpGet]
        public JsonResult REQInnerBoxDetails(int Id, string reqtype)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            //InnerBoxDetailsEntity cutspecModelObj = new InnerBoxDetailsEntity();
            InnerBoxDetailsEntity obj = new InnerBoxDetailsEntity();

            obj.InnerBoxDetailsEntityList = objBAL.getInnerBoxlistforEdit(Id, reqtype);
            return Json(obj.InnerBoxDetailsEntityList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Innerboxdetailsmastr(string fsize,string filetype, string subtype, string material, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<InnerBoxMasterEntity> ib = null;
            InnerBoxMasterEntity inner = new InnerBoxMasterEntity();
            List<InnerBoxMasterEntity> innerlist = new List<InnerBoxMasterEntity>();

            innerlist = getinnerboxmstr();

            if (!String.IsNullOrEmpty(fsize))
            {
                ViewBag.fsize = fsize;
                innerlist = innerlist.Where(x => x.ib_fsize_id == fsize).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                innerlist = innerlist.Where(x => x.ft_ftype_desc == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(subtype))
            {
                ViewBag.subtype = subtype;
                innerlist = innerlist.Where(x => x.pm_fstype_desc == subtype).ToList();
            }

            if (!String.IsNullOrEmpty(material))
            {
                ViewBag.cuttype = material;
                innerlist = innerlist.Where(x => x.ib_box_type == material).ToList();
            }

            inner.innerMasterEntityList = innerlist;
            ib = innerlist.ToPagedList(pageIndex, pageSize);



            return PartialView("_Innerboxdetailsmastr", ib);
        }     
        public ActionResult Innerboxddlmaster()
        {

            //Swapnil code
            InnerBoxMasterEntity objinner = new InnerBoxMasterEntity();

            // objcutspecs.cutSpecsEntityList = objBAL.getCutSpecMaster();


            objinner.listsizecode = get_size();
            objinner.listtype = get_type();
            objinner.listsubtype = get_subtype();
            objinner.listinnerbmaterial = get_material();
           
            return PartialView("_Innerboxddlmaster", objinner);

        }
        public List<InnerBoxMasterEntity> get_material()
        {
            string flag = "getinnerboxmstrmaterila";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerBoxMasterEntity modelobj = new InnerBoxMasterEntity();
            List<InnerBoxMasterEntity> list = new List<InnerBoxMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerBoxMasterEntity obj = new InnerBoxMasterEntity();
                    obj.ib_box_type = (dt.Rows[i]["ib_box_type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerBoxMasterEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerBoxMasterEntity modelobj = new InnerBoxMasterEntity();
            List<InnerBoxMasterEntity> list = new List<InnerBoxMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerBoxMasterEntity obj = new InnerBoxMasterEntity();
                    obj.pm_fstype_desc = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerBoxMasterEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerBoxMasterEntity modelobj = new InnerBoxMasterEntity();
            List<InnerBoxMasterEntity> list = new List<InnerBoxMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerBoxMasterEntity obj = new InnerBoxMasterEntity();
                    obj.ft_ftype_desc = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<InnerBoxMasterEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            InnerBoxMasterEntity modelobj = new InnerBoxMasterEntity();
            List<InnerBoxMasterEntity> list = new List<InnerBoxMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InnerBoxMasterEntity obj = new InnerBoxMasterEntity();
                    obj.ib_fsize_id = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        [HttpGet]
        public ActionResult editInnerBoxmaster(string reqno)
        {
            InnerBoxMasterEntity ib = new InnerBoxMasterEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string chartno = values[0];
            string filetype = values[1];
            ib.ft_ftype_desc = filetype;
            ib.ib_chartnum = chartno;
            ib.innerboxlist = objBAL.getinnerboxlistview(ib.ib_chartnum, ib.ft_ftype_desc);
            //model.innerboxlist = objBAL.getinnerboxlist(model.dp_innerbox_chart, model.pr_fstype_code, model.pr_ftype_code, model.pr_fsize_code);
            //string flag = "editrawmaterials";
            //DataTable dt = objBAL.getrawmaterialedit(flag, reqno);
            //rm.rm_fsize_id = dt.Rows[0]["fs_size_code"].ToString();
            //rm.filesubtype = (dt.Rows[0]["pm_fstype_desc"].ToString());
            //rm.rm_code = (dt.Rows[0]["rm_code"].ToString());
            //rm.filetype = dt.Rows[0]["ft_ftype_desc"].ToString();
            //rm.rm_desc = dt.Rows[0]["rm_desc"].ToString();
            //rm.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[0]["rm_wtinkg_perthsnd"].ToString());
            //rm.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[0]["rm_netwtingm_persku"].ToString());
            //rm.Remark = dt.Rows[0]["rm_remarks"].ToString();
            //// return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_EditInnerBox", ib);

        }

        [HttpGet]
        public ActionResult Getshowinnerbox(string ib_chartnum,string ft_ftype_desc)
        {
            InnerBoxMasterEntity ib = new InnerBoxMasterEntity();
           
            ib.innerboxlist = objBAL.Getshowinnerbox(ib_chartnum,ft_ftype_desc);
            return PartialView("_EditInnerBox", ib);

        }
        [HttpPost]
        public ActionResult UpdateInnerbox(InnerBoxMasterEntity obj)
        {
            //UsermgmtEntity user = new UsermgmtEntity();
            //user.Decline_Reason = obj.decline_reason;
            //user.User_Id = obj.User_Id;
            //user.flag = "insertrejection";

            //objBAL.insertrejection(user);

            // return RedirectToAction("Index");
            return RedirectToAction("RawMaterial", "MastersDetails");
        }

        public List<InnerBoxMasterEntity> getinnerboxmstr()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getinnerboxmstr";
                dt = objBAL.getinnerboxMasters(flag);
                List<InnerBoxMasterEntity> listfilesize = new List<InnerBoxMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new InnerBoxMasterEntity
                    {
                        ib_box_type = item.Field<string>("ib_box_type"),
                        ib_fsize_id = item.Field<string>("ib_fsize_id"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        ib_chartnum = item.Field<string>("ib_chartnum"),

                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<InnerBoxMasterEntity> getinnerboxnoactive()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getinnerboxnoactive";
                dt = objBAL.getinnerboxMasters(flag);
                List<InnerBoxMasterEntity> listfilesize = new List<InnerBoxMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new InnerBoxMasterEntity
                    {
                        ib_box_type = item.Field<string>("ib_box_type"),
                        ib_fsize_id = item.Field<string>("ib_fsize_id"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        ib_chartnum = item.Field<string>("ib_chartnum"),
                        ib_request_id = item.Field<int>("ib_request_id"),
                        ib_request_type = item.Field<string>("ib_request_type"),
                        ReqNo=item.Field<string>("ib_request_type")+item.Field<int>("ib_request_id")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<InnerBoxMasterEntity> getinnerboxnoapprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getinnerboxnoapprove";
                dt = objBAL.getinnerboxMasters(flag);
                List<InnerBoxMasterEntity> listfilesize = new List<InnerBoxMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new InnerBoxMasterEntity
                    {
                        ib_box_type = item.Field<string>("ib_box_type"),
                        ib_fsize_id = item.Field<string>("ib_fsize_id"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        ib_chartnum = item.Field<string>("ib_chartnum"),
                        ib_request_id = item.Field<int>("ib_request_id"),
                        ib_request_type = item.Field<string>("ib_request_type"),
                        ReqNo = item.Field<string>("ib_request_type") + item.Field<int>("ib_request_id")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}