﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFilesDAL;
using System.Data;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;

namespace JKFiles.WebApp.Controllers
{
    public class LoginPageController : Controller
    {
        BAL objBAL = new BAL();
        // GET: LoginPage
        public ActionResult LoginIndex()
        {
            Session["UserId"] = null;
            return View();
        }

        public ActionResult LoginIndex1()
        {
            UsermgmtEntity user = new UsermgmtEntity();
            user.flag = "getmenulist";
            user.menulist = objBAL.getmainmenu(user);
            return PartialView("_MenuMaster", user);
        }

        [HttpPost]
        public ActionResult LoginIndex(UserLoginModel custObj)
        {
            if (ModelState.IsValid)
            {
                UsermgmtEntity user = new UsermgmtEntity();
                if (custObj.User_Id != "" && custObj.Password != ""
                    && custObj.User_Id != null && custObj.Password != null)
                {
                    user.User_Id = custObj.User_Id;
                    user.Password = user.Encrypt(custObj.Password);
                    Session["UserId"] = custObj.User_Id;
                    // user.flag = "getloginuser";

                    if (objBAL.getlogin(user))
                    {
                        Session["Login_Id"] = user.Login_Id;
                        Session["Role"] = user.RoleName;
                        //string role = user.RoleName;
                        user.RoleName = user.RoleName;
                        Session["OEMId"] = user.OEMId;
                        Session["OEMName"] = user.OEMName;
                        // Session["Customer_Id"] = user.Customer_Id;
                        Session["FirstName"] = user.FirstName;
                        Session["LastName"] = user.LastName;
                        Session["Main_Login_Flag"] = user.Main_Login_Flag;
                        Session["Report_Login_Flag"] = user.Report_Login_Flag;
                        // Session["UserRegion"] = user.Region;
                        // Session["UserGroup"] = user.Group;
                        Session["User_Id"] = user.User_Id;

                        user.flag = "getmenulist";
                        user.menulist = objBAL.getmainmenu(user);
                        Session["MenuMaster"] = user.menulist;
                        
                        user.Login_Id = Session["UserId"].ToString();
                        user.flag = "updatemainloginflagtrue";

                        objBAL.updateuserlogin(user);

                        return RedirectToAction("ItemMasterIndex", "ItemMaster");
                    }

                    else
                    {
                        TempData["Credentials"] = "Please enter valid credentials";
                        if (TempData["Credentials"] != null)
                        {
                            ViewBag.Credentials = "Please enter valid credentials";
                          //  Message = "Success";
                            TempData.Remove("Message");
                        }
                    }
                }
                else
                {
                    TempData["Credentials"] = "Please enter valid credentials";
                    if (TempData["Credentials"] != null)
                    {
                        ViewBag.Credentials = "Please enter valid credentials";
                        //  Message = "Success";
                        TempData.Remove("Message");
                    }
                }

            }
            return View();
        }


        public ActionResult Logout()
        {
            UsermgmtEntity modelobj = new UsermgmtEntity();
            modelobj.Login_Id = Session["UserId"].ToString();
            modelobj.flag = "updatemainloginflag";

            objBAL.updateuserlogin(modelobj);
            Session["UserId"] = null;
            return RedirectToAction("LoginIndex");

        }

        //public List<UserLoginModel> getmainmenu(UserLoginModel u)
        //{
        //    DataSet ds = new DataSet();
        //    ds = dbObj.menulist(u);
        //    List<UserLoginModel> menulist = new List<UserLoginModel>();

        //    menulist = ds.Tables[0].AsEnumerable().Select(item => new UserLoginModel()
        //    {
        //        SubMenu_Id = item.Field<int>("SubMenu_Id"),
        //        SubMenu = item.Field<string>("SubMenu"),
        //        Controller = item.Field<string>("Controller"),
        //        Action = item.Field<string>("Action"),
        //        RoleName = item.Field<string>("Role_Name"),
        //        MainMenuID = item.Field<int>("MainMenuID"),
        //        RoleId = item.Field<int>("Role_ID"),
        //        MainMenu = item.Field<string>("MainMenu")

        //    }).ToList();
        //    return menulist;
        //}
    }
}