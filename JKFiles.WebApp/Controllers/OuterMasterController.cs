﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using System.IO;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class OuterMasterController : Controller
    {
        BAL objBAL = new BAL();
        OuterBoxMasterEntity obm = new OuterBoxMasterEntity();
        // GET: OuterMaster
        public ActionResult Index()
        {
             //obm.outerboxMasterEntityList = getouterboxmstr();
             //return View("Index", obm);
             return View();
        }
        public ActionResult NewRequest()
        {

            obm.outerboxNOActiveList = getouterboxNOActive();
            return PartialView("_NewRequest", obm);

        }
        public ActionResult ApprovedRequest()
        {
            obm.outerboxNOApproveList = getouterboxNOApprove();
            return PartialView("_ApprovedRequest", obm);

        }
        public List<OuterBoxMasterEntity> getouterboxNOActive()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getouterboxNOActive";
                dt = objBAL.getouterboxMasters(flag);
                List<OuterBoxMasterEntity> listfilesize = new List<OuterBoxMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new OuterBoxMasterEntity
                    {
                        ob_box_type = item.Field<string>("ob_box_type"),
                        fs_size_code = item.Field<string>("fs_size_code"),
                        ob_box_chartnum = item.Field<string>("ob_box_chartnum"),
                        ob_request_type = item.Field<string>("ob_request_type"),
                        ob_request_id = item.Field<int>("ob_request_id"),
                        ReqNo = item.Field<string>("ob_request_type") + item.Field<int>("ob_request_id")
                        // pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        // ib_chartnum = item.Field<string>("ib_chartnum"),

                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<OuterBoxMasterEntity> getouterboxNOApprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getouterboxNOApprove";
                dt = objBAL.getouterboxMasters(flag);
                List<OuterBoxMasterEntity> listfilesize = new List<OuterBoxMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new OuterBoxMasterEntity
                    {
                        ob_box_type = item.Field<string>("ob_box_type"),
                        fs_size_code = item.Field<string>("fs_size_code"),
                        ob_box_chartnum = item.Field<string>("ob_box_chartnum"),
                        ob_request_type = item.Field<string>("ob_request_type"),
                        ob_request_id = item.Field<int>("ob_request_id"),
                        ReqNo = item.Field<string>("ob_request_type") + item.Field<int>("ob_request_id")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult OuterdetailsMaster(string filesize, string outertype, int? page)
        {
           // obm.outerboxMasterEntityList = getouterboxmstr();
            //return View("Index", obm);
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<OuterBoxMasterEntity> om = null;
            OuterBoxMasterEntity omm = new OuterBoxMasterEntity();
            List<OuterBoxMasterEntity> otlist = new List<OuterBoxMasterEntity>();

            otlist = getouterboxmstr();

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                otlist = otlist.Where(x => x.fs_size_code == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(outertype))
            {
                ViewBag.outertype = outertype;
                otlist = otlist.Where(x => x.ob_box_type == outertype).ToList();
            }

            omm.outerboxMasterEntityList = otlist;
            om = otlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_OuterdetailsMaster", om);
        }
        public ActionResult Outerddlmaster()
        {


            OuterBoxMasterEntity obj = new OuterBoxMasterEntity();

            obj.listsizecode = get_size();
            obj.listoutertype = get_outertype();


            return PartialView("_Outerddlmaster", obj);

        }
        public List<OuterBoxMasterEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            OuterBoxMasterEntity modelobj = new OuterBoxMasterEntity();
            List<OuterBoxMasterEntity> list = new List<OuterBoxMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OuterBoxMasterEntity obj = new OuterBoxMasterEntity();
                    obj.fs_size_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<OuterBoxMasterEntity> get_outertype()
        {
            string flag = "getoutertype";
            DataTable dt = objBAL.getrawmateriallist(flag);
            OuterBoxMasterEntity modelobj = new OuterBoxMasterEntity();
            List<OuterBoxMasterEntity> list = new List<OuterBoxMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OuterBoxMasterEntity obj = new OuterBoxMasterEntity();
                    obj.ob_box_type = (dt.Rows[i]["ob_box_type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<OuterBoxMasterEntity> getouterboxmstr()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getouterboxmstr";
                dt = objBAL.getouterboxMasters(flag);
                List<OuterBoxMasterEntity> listfilesize = new List<OuterBoxMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new OuterBoxMasterEntity
                    {
                        ob_box_type = item.Field<string>("ob_box_type"),
                        fs_size_code = item.Field<string>("fs_size_code"),
                        ob_box_chartnum = item.Field<string>("ob_box_chartnum"),
                       // pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                       // ib_chartnum = item.Field<string>("ib_chartnum"),

                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult EditOuterChartDetails(OuterDetailsEntity model)
        {
            var file = Request.Files["file"];
            if (file != null)
            {
                model.OUTERPACKIMG = file.FileName;
            }

            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].ContentLength == 0) continue;
                string pathToSave = Server.MapPath("~/ProjectImages/Outer/");
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
            }
            if (ModelState.IsValid)
            {
                objBAL.EditOuterchartDetailsactive(model);



                #region Mail Sendin by Dhanashree 

                string subject = "";
                string body = "";

                subject = "Request No:  " + model.ob_request_type + model.ob_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.ob_request_type + model.ob_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",Outer Master updated.Needs Approval.";

                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email,body,subject);
                }
                #endregion
            }
            return RedirectToAction("Index");
        }        
        public ActionResult UpdateOuterApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdateOuterApprove(ID, type);


                string subject = "";
                string body = "";
                subject = "Request No:  " + type + ID + " and " +
                       "Dispatch Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",Outer Master Approved.";


                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return Json(new { url = @Url.Action("Index", "OuterMaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "OuterBox";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "OuterMaster") }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult OuterChartDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            OuterDetailsEntity OuterModelObj = new OuterDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Outer_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                OuterModelObj.obd_boxdtl_recid = dt.Rows[0]["ob_box_chartnum"].ToString();
                OuterModelObj.OUTERPACKMATERIAL = dt.Rows[0]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKDESCRIPTION = dt.Rows[1]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKFINISH = dt.Rows[2]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKAPPLICATION = dt.Rows[3]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKIMG = dt.Rows[0]["ob_box_imgname"].ToString();
                OuterModelObj.OUTERPACKLENGTH = dt.Rows[4]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKWIDTH = dt.Rows[5]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKHEIGHT = dt.Rows[6]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKBATTENPOSITION = dt.Rows[7]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER1 = dt.Rows[8]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER2 = dt.Rows[9]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER3 = dt.Rows[10]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER4 = dt.Rows[11]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OTHER5 = dt.Rows[12]["obd_parm_scrnvalue"].ToString();
                OuterModelObj.OUTERPACKLENGTHrange = dt.Rows[4]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OUTERPACKWIDTHrange = dt.Rows[5]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OUTERPACKHEIGHTrange = dt.Rows[6]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OUTERPACKBATTENPOSITIONrange = dt.Rows[7]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER1range = dt.Rows[8]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER2range = dt.Rows[9]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER3range = dt.Rows[10]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER4range = dt.Rows[11]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.OTHER5range = dt.Rows[12]["obd_parm_dwgvalue"].ToString();
                OuterModelObj.ob_request_type = dt.Rows[12]["ob_request_type"].ToString();
                OuterModelObj.ob_request_id = Convert.ToInt16(dt.Rows[12]["ob_request_id"].ToString());
                OuterModelObj.ob_box_type = dt.Rows[12]["ob_box_type"].ToString();
                OuterModelObj.ob_box_fsize_code = dt.Rows[12]["ob_box_fsize_code"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("OuterChartDetails", OuterModelObj);
        }
        [HttpGet]
        public ActionResult editOutermaster(string reqno)
        {
            OuterBoxMasterEntity ob = new OuterBoxMasterEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string chartno = values[0];
            string boxtype = values[1];
            ob.ob_box_type = boxtype;
            ob.ob_box_chartnum = chartno;
            // ob.outerboxMasterList = objBAL.GetinnerLabelListview(ib.ilbd_chart_num, ib.ft_ftype_desc);
            ob.outerboxMasterList = objBAL.GetOuterChartlistview(ob.ob_box_chartnum,ob.ob_box_type);
            return PartialView("_EditOuterView", ob);

        }

        [HttpGet]
        public ActionResult showoutermaster(string ob_box_chartnum,string ob_box_type)
        {
            OuterBoxMasterEntity ob = new OuterBoxMasterEntity();
            ob.outerboxMasterList = objBAL.Getshowoutermaster(ob_box_chartnum, ob_box_type);
            return PartialView("_EditOuterView", ob);

        }
    }
}