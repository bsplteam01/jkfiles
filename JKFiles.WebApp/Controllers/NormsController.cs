﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class NormsController : Controller
    {
        BAL objBAL = new BAL();
        // GET: Norms
        public ActionResult Index()
        {
            NormsMasterModel obj = new NormsMasterModel();
            obj.Normslist = objBAL.getnewnormlist();
            obj.ApprovalNormslist = objBAL.getApprovalNormslist();
            obj.unitdropdownlist = objBAL.getunitDropdownList();
            return View(obj);
        }
        public ActionResult Showaddprocess(string process_code, string requestID, string requesttype)
        {
            List<ProcessmasterModel> processlist = new List<ProcessmasterModel>();
            var requestID1 = requestID.Substring(1);
            processlist = Getprocesstypewiselist(process_code, requestID1, requesttype);
            return Json(new { success = true, processlist = processlist }, JsonRequestBehavior.AllowGet);

        }
        public List<ProcessmasterModel> Getprocesstypewiselist(string process_code, string requestID, string requesttype)
        {
            string flag = "Normsprocesslist";
            int reqno = Convert.ToInt32(requestID);
            DataTable dt = objBAL.processtypewiselist(flag, process_code, reqno, requesttype);
            ProcessmasterModel modelobj = new ProcessmasterModel();
            List<ProcessmasterModel> list = new List<ProcessmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ProcessmasterModel objProc = new ProcessmasterModel();
                    objProc.ppm_process_code = dt.Rows[i]["ppm_process_code"].ToString();
                    objProc.ppm_process_seqno = Convert.ToInt32(dt.Rows[i]["ppm_process_seqno"].ToString());
                    objProc.ProcessName = dt.Rows[i]["opr_operation_name"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }
        [HttpGet]
        public JsonResult NormsProcesslist(string RequestId, string Requesrtype, string UnitCode, string valuestrem)
        {
            NormsMasterModel nmobj = new NormsMasterModel();
            var reqest = RequestId.Substring(1);
            var VsCode = valuestrem.Substring(1);
            nmobj.NormsProcesslist = getNormsProcesslist(reqest, Requesrtype, UnitCode, VsCode);
            if (nmobj.NormsProcesslist.Count == 0)
            {
                return Json(new { success = false, NormsPElist = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, NormsPElist = nmobj.NormsProcesslist, req_id = reqest }, JsonRequestBehavior.AllowGet);
            }

        }


        public List<NormsMasterModel> getNormsProcesslist(string RequestId, string Requesrtype, string UnitCode, string VsCode)
        {
            string flag = "NormsGrid1ProcessEqu";

            DataTable dt = objBAL.getNormsProcesslist(flag, RequestId, Requesrtype, UnitCode, VsCode);
            NormsMasterModel modelobj = new NormsMasterModel();
            List<NormsMasterModel> list = new List<NormsMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    NormsMasterModel objProc = new NormsMasterModel();
                    objProc.aa_unit_code = dt.Rows[i]["aa_unit_code"].ToString();
                    objProc.aa_main_valstream = dt.Rows[i]["aa_main_valstream"].ToString();
                    objProc.opr_operation_seq = Convert.ToInt32(dt.Rows[i]["opr_operation_seq"].ToString());
                    objProc.opr_operation_name = dt.Rows[i]["opr_operation_name"].ToString();
                    objProc.equ_name = dt.Rows[i]["equ_name"].ToString();
                    objProc.equ_type = dt.Rows[i]["equ_type"].ToString();
                    objProc.pd_flg = dt.Rows[i]["pd_flg"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }
        [HttpPost]
        public ActionResult SaveNorms(List<NormsMasterModel> AllArrayValue)
        {
            NormsMasterEntity nmobj = new NormsMasterEntity();

            foreach (var item in AllArrayValue)
            {
                if (item.pd_flg == "P")
                {
                    var sku = item.nm_prod_sku.Substring(2, 8);
                    nmobj.nm_prod_sku = sku;
                    nmobj.nm_valuestream = item.nm_valuestream;
                    nmobj.nm_unit_code = item.nm_unit_code;
                    nmobj.nm_opern_name = item.nm_opern_name;
                    nmobj.nm_equ_name = item.nm_equ_name;
                    nmobj.nm_equ_type = item.nm_equ_type;
                    nmobj.nm_norms_value = item.nm_norms_value;
                    nmobj.nm_request_type = item.nm_request_type;
                    nmobj.nm_request_id = item.nm_request_id;
                    nmobj.nm_createby = Session["User_Id"].ToString();
                    nmobj.flag = "InsertPnormsvalue ";
                    objBAL.SavePtypenormsvlaue(nmobj);
                }
                else
                {
                    nmobj.dnm_item_sku = item.nm_prod_sku;
                    nmobj.dnm_valuestream = item.nm_valuestream;
                    nmobj.dnm_unit_code = item.nm_unit_code;
                    nmobj.dnm_opern_name = item.nm_opern_name;
                    nmobj.dnm_equ_name = item.nm_equ_name;
                    nmobj.dnm_equ_type = item.nm_equ_type;
                    nmobj.dnm_dnorms_value = item.nm_norms_value;
                    nmobj.dnm_request_type = item.nm_request_type;
                    nmobj.dnm_request_id = item.nm_request_id;
                    nmobj.dnm_createby = Session["User_Id"].ToString();
                    nmobj.dflag = "InsertDnormsvalue ";
                    objBAL.SaveDtypenormsvlaue(nmobj);
                }

            }
            return Json(new { success = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CloseFirstgridNorms(string RequestId, string Requesrtype)
        {
            NormsMasterEntity nmobj = new NormsMasterEntity();
            var reqest = RequestId.Substring(1);
            nmobj.nm_request_type = Requesrtype;
            nmobj.nm_request_id = Convert.ToInt32(reqest);
            nmobj.nm_createby = Session["User_Id"].ToString();
            nmobj.flag = "CloseNorms";
            objBAL.CloseNormsDetails(nmobj);

            #region Mail Sendin by Dhanashree 

            string subject = "";
            string body = "";

            subject = "Request No:  " + Requesrtype + RequestId
                + " and " + "Production Master Update";
            body = "Request No: " + Requesrtype + RequestId +
                " By User ID " + Session["UserId"].ToString()
                + ",Norms Master updated.Needs Approval.";
            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }
            #endregion

            return Json(new { success = true, message = "Close succesfully." }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveNorms(string RequestId, string Requesrtype)
        {
            try
            {
                NormsMasterEntity nmobj = new NormsMasterEntity();
                var reqest = RequestId.Substring(1);
                nmobj.nm_request_type = Requesrtype;
                nmobj.nm_request_id = Convert.ToInt32(reqest);
                nmobj.flag = "ApproveNorms";
                nmobj.nm_createby = Session["User_Id"].ToString();
                objBAL.CloseNormsDetails(nmobj);

                string subject = "";
                string body = "";

                subject = "Request No:  " + Requesrtype + RequestId + " and " +
                       "Production Master Approval";
                body = "Request No: " + Requesrtype + RequestId + " By User ID " + Session["UserId"].ToString()
                    + ",Norms Master Approved.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }

                return Json(new { success = true, message = "Approve succesfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult RejectrequestNorms(string RequestId, string Requesrtype, string ma_remarks)
        {
            try
            {
                NormsMasterEntity nmobj = new NormsMasterEntity();
                var reqest = RequestId.Substring(1);
                nmobj.nm_request_type = Requesrtype;
                nmobj.ma_remarks = ma_remarks;
                nmobj.nm_request_id = Convert.ToInt32(reqest);
                nmobj.nm_createby = Session["User_Id"].ToString();
                nmobj.flag = "RejectNorms";
                objBAL.RejectNormsDetails(nmobj);

                string subject = "";
                string body = "";
                subject = "Request No:  " + Requesrtype + RequestId + " and " +
                      "Production Master Reject";
                body = "Request No: " + Requesrtype + RequestId + " By User ID " + Session["UserId"].ToString()
                    + ",Norms Master Rejected.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
                return Json(new { success = true, message = "Reject succesfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult NormsProcessViewlist(string RequestId, string Requesrtype, string UnitCode)
        {
            NormsMasterModel nmobj = new NormsMasterModel();
            var reqest = RequestId.Substring(1);
            nmobj.NormsProcesslist = getNormsProcessViewlist(reqest, Requesrtype, UnitCode);
            if (nmobj.NormsProcesslist.Count == 0)
            {
                return Json(new { success = false, NormsPElist = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, NormsPElist = nmobj.NormsProcesslist, req_id = reqest }, JsonRequestBehavior.AllowGet);
            }

        }
        public List<NormsMasterModel> getNormsProcessViewlist(string RequestId, string Requesrtype, string UnitCode)
        {
            string flag = "NormsGrid1ProcessEquView";

            DataTable dt = objBAL.getNormsProcessViewlist(flag, RequestId, Requesrtype, UnitCode);
            NormsMasterModel modelobj = new NormsMasterModel();
            List<NormsMasterModel> list = new List<NormsMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    NormsMasterModel objProc = new NormsMasterModel();
                    objProc.nm_opern_name = dt.Rows[i]["nm_opern_name"].ToString();
                    objProc.nm_equ_name = dt.Rows[i]["nm_equ_name"].ToString();
                    objProc.nm_equ_type = dt.Rows[i]["nm_equ_type"].ToString();
                    objProc.nm_norms_value = dt.Rows[i]["nm_norms_value"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }
    }
}