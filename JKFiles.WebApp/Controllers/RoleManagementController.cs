﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
//using JKFiles.WebApp.Usermgmt;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;
//using JKFiles.WebApp.Asset;

namespace JKFiles.WebApp.Controllers
{
    public class RoleManagementController : Controller
    {
        BAL objBAL = new BAL();

        // GET: RoleManagement
        public ActionResult AddRole()
        {
            return View();
        }

        //inserting role
        [HttpPost]
        public ActionResult AddRole(UserRoleModel custObj)
        {
            if (ModelState.IsValid)
            {
                UsermgmtEntity user = new UsermgmtEntity();
                user.RoleName = custObj.RoleName;
                user.Description = custObj.Description;
                user.flag = "insertrole";

                objBAL.saveroleDetails(user);
                return RedirectToAction("AddRole", "RoleManagement");
            }
            return RedirectToAction("AddRole");
        }

        


        //list of role grid view
        public ActionResult RoleList()
        {
            UserRoleModel modelobj = new UserRoleModel();
            modelobj.listrole = get_rolelist();
            var v = modelobj.listrole;
            return PartialView("_RoleList", v.ToList());
        }

        public List<UserRoleModel> get_rolelist()
        {
            string flag = "getrolelist";
            DataTable dt = objBAL.getRolelist(flag);
            UserRoleModel modelobj = new UserRoleModel();
            List<UserRoleModel> list = new List<UserRoleModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserRoleModel objRole = new UserRoleModel();
                    objRole.Role_Id = Convert.ToInt32(dt.Rows[i]["Role_Id"].ToString());
                    objRole.RoleName = dt.Rows[i]["RoleName"].ToString();
                    objRole.Description = dt.Rows[i]["Description"].ToString();
                    list.Add(objRole);
                }
            }
            return list.ToList();
        }

        public ActionResult Edit(int id)
        {
            UserRoleModel modelobj = new UserRoleModel();
            var v = get_rolelist().Find(p => p.Role_Id == id);
            return View(v);

        }


        //update role
        [HttpPost]
        public ActionResult Edit(UserRoleModel custObj, int id)
        {
            if (ModelState.IsValid)
            {
                UsermgmtEntity user = new UsermgmtEntity();
                user.RoleName = custObj.RoleName;
                user.Description = custObj.Description;
                //user.Role_Id =Convert.ToInt32(custObj.Role_Id).ToString();
                user.Role_Id = Convert.ToInt32(id).ToString();
                user.flag = "updaterole";

                objBAL.updateroleDetails(user);
                return RedirectToAction("AddRole", "RoleManagement");
            }
            return RedirectToAction("AddRole");
        }

        //Delete role
        public ActionResult Delete(int id)
        {
            UsermgmtEntity modelobj = new UsermgmtEntity();
            modelobj.Role_Id = Convert.ToInt32(id).ToString();
            modelobj.flag = "deleterole";

            objBAL.deleteroleDetails(modelobj);
            return RedirectToAction("AddRole", "RoleManagement");
           

        }


    }
}