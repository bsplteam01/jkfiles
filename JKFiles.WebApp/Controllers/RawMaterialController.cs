﻿using JKFiles.WebApp.Entity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using System.IO;

namespace JKFiles.WebApp.Controllers1
{
    public class RawMaterialController : Controller
    {
        BAL objBAL = new BAL();
        public ActionResult RawMaterialMasterGrid()
        {

            //Initialization.    
            DataTables result = new DataTables();
            try
            {
                //Initialization.    
                string search = Request.Params["search[value]"];
                string draw = Request.Params["draw"];
                string order = Request.Params["order[0][column]"];
                string orderDir = Request.Params["order[0][dir]"];
                int PageNo = Convert.ToInt32(Request.Params["start"]);
                int pageSize = Convert.ToInt32(Request.Params["length"]);
                //Loading.    
                List<RawMaterialMastrEntity> data = LoadData();
                // Total record count.    
                int totalRecords = data.Count;
                //Verification.    
                if (!string.IsNullOrEmpty(search) &&
                  !string.IsNullOrWhiteSpace(search))
                {
                    //Apply search    
                    data = data.Where(p =>
                           p.rm_fsize_id.ToString().Contains(search.ToLower()) ||
                           p.rm_fstype_id.ToString().Contains(search.ToLower()) ||
                           p.rm_code.ToString().Contains(search.ToLower()) ||
                           p.rm_wtinkg_perthsnd.ToString().Contains(search.ToLower()) ||
                           p.rm_netwtingm_persku.ToString().Contains(search.ToLower()) ||
                           p.rm_remarks.ToString().Contains(search.ToLower()) ||
                           p.rm_ftype_id.ToString().ToLower().Contains(search.ToLower())

                           ).ToList();
                }
                //Sorting.  error in sorting   
              //  data = SortByColumnWithOrder(order, orderDir, data);
                //Filter record count.    
                int recFilter = data.Count;
                // Applypagination.    
                data = data.Skip(PageNo).Take(pageSize).ToList();
                //Loading drop down lists.    
                result.draw = Convert.ToInt32(draw);
                result.recordsTotal = totalRecords;
                result.recordsFiltered = recFilter;
                result.data = data;
            }
            catch (Exception ex)
            {
                //Info    
                Console.Write(ex);
            }
            //Return info.    
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private List<RawMaterialMastrEntity> LoadData()
        {
            List<RawMaterialMastrEntity> Data = new List<RawMaterialMastrEntity>();
            Data = objBAL.getrawmateriallistnoapprove();
            return Data;
        }
        //raw material 14 june 2018
        //public ActionResult RawMaterial()
        //{
        //    RawMaterialMasterModel rm = new RawMaterialMasterModel();

        //    rm.listrawm = get_rawlist();
        //    rm.listrmcode = get_code();
        //    rm.listsizecode = get_size();
        //    rm.listtype = get_type();
        //    rm.listsubtype = get_subtype();
        //    return View("RawMaterial", rm);
        //}
        public ActionResult RawMaterial()
        {

            //int pageSize = 10;
            //int pageIndex = 1;
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //IPagedList<RawMaterialMasterModel> rmm = null;
            //RawMaterialMasterModel rm = new RawMaterialMasterModel();
            //List<RawMaterialMasterModel> rmlist = new List<RawMaterialMasterModel>();

            //rmlist = get_rawlist();
            //rm.listrawm = rmlist;
            //rmm = rmlist.ToPagedList(pageIndex, pageSize);
            //  rmcodelist = get_code();
            //  rm.listrmcode = rmcodelist;
            //  rmm = rmcodelist.ToPagedList(pageIndex, pageSize);
            //  rmm = rmlist.ToList();
            //rm.listrawm = get_rawlist();
            //rm.listrmcode = get_code();
            //rm.listsizecode = get_size();
            //rm.listtype = get_type();
            //rm.listsubtype = get_subtype();
            // return View("RawMaterial", rmm);
            // return View(rmm);

            return View();
        }


        #region by swapnil
        public List<RawMaterialMasterModel> rawmateriallistactive1()
        {

            string flag = "getrawmaterialsactivelist";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.rm_request_id = Convert.ToInt16(dt.Rows[i]["rm_request_id"].ToString());
                    obj.rm_request_type = (dt.Rows[i]["rm_request_type"].ToString());
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    obj.rm_code = (dt.Rows[i]["rm_code"].ToString());
                    obj.rm_fsize_id = dt.Rows[i]["fs_size_code"].ToString();
                    obj.filetype = dt.Rows[i]["ft_ftype_desc"].ToString();
                    obj.rm_desc = dt.Rows[i]["rm_desc"].ToString();
                    obj.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[i]["rm_wtinkg_perthsnd"].ToString());
                    obj.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[i]["rm_netwtingm_persku"].ToString());
                    obj.Remark = dt.Rows[i]["rm_remarks"].ToString();
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<RawMaterialMasterModel> rawmaterialNoApprove()
        {

            string flag = "getrawmaterialNoApprovelist";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.rm_request_id = Convert.ToInt16(dt.Rows[i]["rm_request_id"].ToString());
                    obj.rm_request_type = (dt.Rows[i]["rm_request_type"].ToString());
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    obj.rm_code = (dt.Rows[i]["rm_code"].ToString());
                    obj.rm_fsize_id = dt.Rows[i]["fs_size_code"].ToString();
                    obj.filetype = dt.Rows[i]["ft_ftype_desc"].ToString();
                    obj.rm_desc = dt.Rows[i]["rm_desc"].ToString();
                    obj.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[i]["rm_wtinkg_perthsnd"].ToString());
                    obj.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[i]["rm_netwtingm_persku"].ToString());
                    obj.Remark = dt.Rows[i]["rm_remarks"].ToString();
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public ActionResult NewRequest()
        {
            RawMaterialMasterModel rm = new RawMaterialMasterModel();
           rm.rawmaterialnoactive = rawmateriallistactive1();
            return PartialView("_NewRequest", rm);
        }
        public ActionResult ApprovedRequest()
        {

            RawMaterialMasterModel rm = new RawMaterialMasterModel();
            rm.rawmaterialnoapprove = rawmaterialNoApprove();
            return PartialView("_ApprovedRequest", rm);

        }
        public ActionResult RawmaterialDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            RawMaterialMastrEntity RawmaterialModelObj = new RawMaterialMastrEntity();
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_RawMaterial_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                RawmaterialModelObj.rm_fsize_id = dt.Rows[0]["rm_fsize_id"].ToString();
                RawmaterialModelObj.rm_ftype_id = dt.Rows[0]["rm_ftype_id"].ToString();
                RawmaterialModelObj.rm_fstype_id = dt.Rows[0]["rm_fstype_id"].ToString();
                RawmaterialModelObj.rm_code = dt.Rows[0]["rm_code"].ToString();
                RawmaterialModelObj.rm_desc = dt.Rows[0]["rm_desc"].ToString();
                RawmaterialModelObj.rm_material = dt.Rows[0]["rm_material"].ToString();
                RawmaterialModelObj.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[0]["rm_wtinkg_perthsnd"].ToString());
                RawmaterialModelObj.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[0]["rm_netwtingm_persku"].ToString());
                RawmaterialModelObj.rm_height = Convert.ToDecimal(dt.Rows[0]["rm_height"].ToString());
                RawmaterialModelObj.rm_width = Convert.ToDecimal(dt.Rows[0]["rm_width"].ToString());
                RawmaterialModelObj.rm_request_type = dt.Rows[0]["rm_request_type"].ToString();
                RawmaterialModelObj.rm_request_id = Convert.ToInt16(dt.Rows[0]["rm_request_id"].ToString());
                RawmaterialModelObj.rm_remarks = dt.Rows[0]["rm_remarks"].ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("RawMaterialDetails", RawmaterialModelObj);

        }
        [HttpPost]
        public ActionResult EditRawMaterialDetails(RawMaterialMastrEntity model)
        {
            if (ModelState.IsValid)
            {
                objBAL.RawMaterialUpdatenoactive(model);
            }
            return RedirectToAction("RawMaterial");
        }

        public ActionResult UpdateRawMaterialApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdateRawMaterialApprove(ID, type);
            }
            return RedirectToAction("RawMaterial");
        }  
    
        #endregion

        public ActionResult RawMaterialmaster(string rawmaterial, string filesize, string fsubtype, string filetype, int? page)
        {

            int pageSize = 5;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<RawMaterialMasterModel> rmm = null;
            RawMaterialMasterModel rm = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> rmlist = new List<RawMaterialMasterModel>();

            rmlist = get_rawlist();


            if (!String.IsNullOrEmpty(rawmaterial))
            {
                ViewBag.rawmaterial = rawmaterial;
                rmlist = rmlist.Where(x => x.rm_code == rawmaterial).ToList();
            }

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                rmlist = rmlist.Where(x => x.rm_fsize_id == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(fsubtype))
            {
                ViewBag.ftypecode = fsubtype;
                rmlist = rmlist.Where(x => x.filesubtype == fsubtype).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                rmlist = rmlist.Where(x => x.filetype == filetype).ToList();
            }




            rm.listrawm = rmlist;
            rmm = rmlist.ToPagedList(pageIndex, pageSize);

            return PartialView("_RawMaterialmaster", rmm);

        }
        public ActionResult RawMaterialddl()
        {
            RawMaterialMasterModel rm = new RawMaterialMasterModel();

            // rm.listrawm = get_rawlist();
            rm.listrawm = get_rawlist();
            rm.listrmcode = get_code();
            rm.listsizecode = get_size();
            rm.listtype = get_type();
            rm.listsubtype = get_subtype();
            return PartialView("_RawMaterialDLL", rm);
        }
        public List<RawMaterialMasterModel> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<RawMaterialMasterModel> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<RawMaterialMasterModel> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.rm_fsize_id = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<RawMaterialMasterModel> get_code()
        {
            string flag = "getrawmaterialcodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.rm_code = (dt.Rows[i]["rm_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<RawMaterialMasterModel> get_rawlist()
        {
            string flag = "getrawmaterials";
            DataTable dt = objBAL.getrawmateriallist(flag);
            RawMaterialMasterModel modelobj = new RawMaterialMasterModel();
            List<RawMaterialMasterModel> list = new List<RawMaterialMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RawMaterialMasterModel obj = new RawMaterialMasterModel();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    obj.rm_code = (dt.Rows[i]["rm_code"].ToString());
                    obj.rm_fsize_id = dt.Rows[i]["fs_size_code"].ToString();
                    obj.filetype = dt.Rows[i]["ft_ftype_desc"].ToString();
                    obj.rm_desc = dt.Rows[i]["rm_desc"].ToString();
                    obj.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[i]["rm_wtinkg_perthsnd"].ToString());
                    obj.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[i]["rm_netwtingm_persku"].ToString());
                    obj.Remark = dt.Rows[i]["rm_remarks"].ToString();
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        [HttpGet]
        public ActionResult editrawmaterial(string reqno)
        {
            RawMaterialMasterModel rm = new RawMaterialMasterModel();
            rm.rm_code = reqno;
            string flag = "editrawmaterials";
            DataTable dt = objBAL.getrawmaterialedit(flag, reqno);
            rm.rm_fsize_id = dt.Rows[0]["fs_size_code"].ToString();
            rm.filesubtype = (dt.Rows[0]["pm_fstype_desc"].ToString());
            rm.rm_code = (dt.Rows[0]["rm_code"].ToString());
            rm.filetype = dt.Rows[0]["ft_ftype_desc"].ToString();
            rm.rm_desc = dt.Rows[0]["rm_desc"].ToString();
            rm.rm_wtinkg_perthsnd = Convert.ToDecimal(dt.Rows[0]["rm_wtinkg_perthsnd"].ToString());
            rm.rm_netwtingm_persku = Convert.ToDecimal(dt.Rows[0]["rm_netwtingm_persku"].ToString());
            rm.Remark = dt.Rows[0]["rm_remarks"].ToString();
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_EditRawMaterial", rm);

        }
        [HttpPost]
        public ActionResult Updaterawmaterial(RawMaterialMasterModel obj)
        {
            //UsermgmtEntity user = new UsermgmtEntity();
            //user.Decline_Reason = obj.decline_reason;
            //user.User_Id = obj.User_Id;
            //user.flag = "insertrejection";

            //objBAL.insertrejection(user);

            // return RedirectToAction("Index");
            return RedirectToAction("RawMaterial", "MastersDetails");
        }
    }
}