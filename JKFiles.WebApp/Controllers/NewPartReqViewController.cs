﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using JKFiles.WebApp.ActionFilters;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class NewPartReqViewController : Controller
    {
        // GET: NewPartReqView
        BAL objBAL = new BAL();
       
        public ActionResult Index(string partno)
       {
            DispachProductSkuEntity model = new DispachProductSkuEntity();
            try
            {                
                model.partno = partno;
                Session["partno"] = partno;
                //partno = partno.Remove(partno.Length - 1);
                string type = partno.Substring(0,1);
                partno = partno.Substring(1);
                Session["type"] = type;
                DataTable dt = new DataTable();
                Session["requestId1"] = partno;
                //int ReqId = objBAL.getReqId(partno);
                int ReqId = Convert.ToInt32(partno);
               
                string completepartno = objBAL.getReqId(model.partno);
                if (completepartno != "")
                {
                    model.complete_partno = completepartno;
                    string complete_partno = model.complete_partno;
                    model.split1 = complete_partno.Substring(0, 2);
                    model.split2 = complete_partno.Substring(2, 8);
                    model.split3 = complete_partno.Substring(10, 3);
                }
                if (ReqId != 0)
                {
                    dt = objBAL.getdispatchdetails(ReqId);
                }
                model.pr_request_type = dt.Rows[0]["pr_request_type"].ToString();
                model.pr_request_id = Convert.ToInt16(dt.Rows[0]["pr_request_id"].ToString());
                // model.partno = partno;
                model.pr_cust_id = dt.Rows[0]["pr_cust_id"].ToString();
                Session["CoustomerId"] = dt.Rows[0]["pr_cust_id"].ToString();
                model.pr_cust_name = dt.Rows[0]["pr_cust_name"].ToString();
                model.pr_brand_id = dt.Rows[0]["pr_brand_id"].ToString();
                Session["BrandId"] = dt.Rows[0]["pr_brand_id"].ToString();
                model.pr_brand_desc = dt.Rows[0]["pr_brand_desc"].ToString();
                model.pr_ship2country_id = dt.Rows[0]["pr_ship2country_id"].ToString();
                model.pr_ship2country_name = dt.Rows[0]["pr_ship2country_name"].ToString();
                model.pr_sap_no = dt.Rows[0]["pr_sap_no"].ToString();
                model.pr_customer_part_no = dt.Rows[0]["pr_customer_part_no"].ToString();
                model.pr_fsize_code = dt.Rows[0]["pr_fsize_code"].ToString();
                Session["FileSizeCode"] = dt.Rows[0]["pr_fsize_code"].ToString();
                Session["filesizecode"] = model.pr_fsize_code;
                model.fs_size_inches = dt.Rows[0]["fs_size_inches"].ToString();
                model.pr_ftype_code = dt.Rows[0]["pr_ftype_code"].ToString();
                model.pr_ftype_desc = dt.Rows[0]["pr_ftype_desc"].ToString();
                model.pr_fstype_code = dt.Rows[0]["pr_fstype_code"].ToString();
                model.pr_fstype_desc = dt.Rows[0]["pr_fstype_desc"].ToString();
                Session["filecode"] = model.pr_ftype_code + model.pr_fstype_code;
                Session["Filecode"] = model.pr_ftype_code + model.pr_fstype_code;
                model.pr_filecode = model.pr_ftype_code + model.pr_fstype_code;
                model.pr_hardness = dt.Rows[0]["pr_hardness"].ToString();
                model.pr_tang_color = dt.Rows[0]["pr_tang_color"].ToString();

                if (dt.Rows[0]["pr_uncutattang"].ToString() != null && dt.Rows[0]["pr_uncutattang"].ToString() != "")
                {

                    model.pr_uncutattang = Convert.ToDecimal(dt.Rows[0]["pr_uncutattang"].ToString());
                }
                else
                {
                    model.pr_uncutattang = 0;
                }
                //model.pr_uncutatpoint = Convert.ToDecimal(dt.Rows[0]["pr_uncutatpoint"].ToString());
                if (dt.Rows[0]["pr_uncutatedge"].ToString() != "" && dt.Rows[0]["pr_uncutatedge"].ToString() != null)
                {
                    model.pr_uncutatedge = Convert.ToDecimal(dt.Rows[0]["pr_uncutatedge"].ToString());
                }
                else
                {
                    model.pr_uncutatedge = 0;
                }
                if (dt.Rows[0]["pr_upcutncline"].ToString() != "" && dt.Rows[0]["pr_upcutncline"].ToString() != null)
                {
                    model.pr_upcutncline = Convert.ToDecimal(dt.Rows[0]["pr_upcutncline"].ToString());
                }
                else
                {
                    model.pr_upcutncline = 0;
                }
                if (dt.Rows[0]["pr_overcutincline"].ToString() != null && dt.Rows[0]["pr_overcutincline"].ToString() != "")
                {
                    model.pr_overcutincline = Convert.ToDecimal(dt.Rows[0]["pr_overcutincline"].ToString());
                }
                if (dt.Rows[0]["pr_edgecutincline"].ToString() != null && dt.Rows[0]["pr_edgecutincline"].ToString() != "")
                {
                    model.pr_edgecutincline = Convert.ToDecimal(dt.Rows[0]["pr_edgecutincline"].ToString());
                }
                else
                {
                    model.pr_overcutincline = 0;
                }
                model.pr_fcut_code = dt.Rows[0]["pr_fcut_code"].ToString();
                Session["FileCutTypeCode"] = dt.Rows[0]["pr_fcut_code"].ToString();
                model.pr_fcut_desc = dt.Rows[0]["pr_fcut_desc"].ToString();
                model.pr_cut_standard = dt.Rows[0]["pr_cut_standard"].ToString();
                Session["CutSpecification"] = dt.Rows[0]["pr_cut_standard"].ToString();
                #region Session for Drawing by Dhanashree
                //Session["FileSizeCode"] = dt.Rows[0]["pr_fsize_code"].ToString();
                //Session["FileTypeCode"] = dt.Rows[0]["pr_ftype_code"].ToString();
                //Session["FileSubTypeCode"] = dt.Rows[0]["pr_fstype_code"].ToString();
                //Session["FileCutTypeCode"] = dt.Rows[0]["pr_fcut_code"].ToString();
                #endregion
                if (dt.Rows[0]["pr_rawmtrl_height"].ToString() != "")
                {
                    model.pr_rawmtrl_height = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_height"].ToString());
                }
                if (dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString() != "")
                {
                    model.pr_rawmtrl_width = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString());
                }
                if (dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString() != "")
                {
                    model.pr_rawmtrl_kgperthsnd = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString());
                }
                if (dt.Rows[0]["pr_rawmtrl_netwt"].ToString() != "")
                {
                    model.pr_rawmtrl_netwt = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_netwt"].ToString());
                }
                model.pr_rawmtrl_cd = dt.Rows[0]["pr_rawmtrl_cd"].ToString();
                model.pr_isApproved = dt.Rows[0]["pr_isApproved"].ToString();
                //model.pr_isActive = dt.Rows[0][""].ToString();
                model.pr_createby = dt.Rows[0]["pr_createby"].ToString();
                model.pr_approveby = dt.Rows[0]["pr_approveby"].ToString();
                model.pr_uniq_id = dt.Rows[0]["pr_uniq_id"].ToString();
                model.pr_remarks = dt.Rows[0]["pr_remarks"].ToString();
                model.dp_id = Convert.ToInt16(dt.Rows[0]["dp_id"].ToString());
                model.dp_request_type = dt.Rows[0]["dp_request_type"].ToString();
                model.dp_dispatch_digit = dt.Rows[0]["dp_dispatch_digit"].ToString();
                model.dp_stamp_type = dt.Rows[0]["dp_stamp_type"].ToString();
                model.dp_stamp_chart = dt.Rows[0]["dp_stamp_chart"].ToString();
                //model.dp_barcode_flg = dt.Rows[0]["dp_barcode_flg"].ToString();
                //model.dp_lineno_flg = dt.Rows[0]["dp_lineno_flg"].ToString();
                //model.dp_hologram_flg = dt.Rows[0]["dp_hologram_flg"].ToString();
                //model.dp_india_flg = dt.Rows[0]["dp_india_flg"].ToString();
                model.dp_qltyseal_flg = Convert.ToBoolean(dt.Rows[0]["dp_qltyseal_flg"].ToString());
                model.dp_barcode_flg = Convert.ToBoolean(dt.Rows[0]["dp_barcode_flg"].ToString());
                model.dp_lineno_flg = Convert.ToBoolean(dt.Rows[0]["dp_lineno_flg"].ToString());
                model.dp_hologram_flg = Convert.ToBoolean(dt.Rows[0]["dp_hologram_flg"].ToString());
                model.dp_prcstkr_flg = Convert.ToBoolean(dt.Rows[0]["dp_prcstkr_flg"].ToString());
                model.dp_india_flg = Convert.ToBoolean(dt.Rows[0]["dp_india_flg"].ToString());
                model.dp_polybag_flg = Convert.ToBoolean(dt.Rows[0]["dp_polybag_flg"].ToString());
                model.dp_silicagel_flg = Convert.ToBoolean(dt.Rows[0]["dp_silicagel_flg"].ToString());
                model.dp_fumigation_flg = Convert.ToBoolean(dt.Rows[0]["dp_fumigation_flg"].ToString());

                model.dp_promotionalitem_flg = Convert.ToBoolean(dt.Rows[0]["dp_promo_item_flg"].ToString());
                model.dp_strap = dt.Rows[0]["dp_strap"].ToString();
                model.dp_cellotape_flg = Convert.ToBoolean(dt.Rows[0]["dp_cellotape_flg"].ToString());
                model.dp_shrinkwrap_flg = Convert.ToBoolean(dt.Rows[0]["dp_shrinkwrap_flg"].ToString());
                model.dp_decicant_flg = Convert.ToBoolean(dt.Rows[0]["dp_decicant_flg"].ToString());
                model.dp_paperwool_flg = Convert.ToBoolean(dt.Rows[0]["dp_paperwool_flg"].ToString());

                model.dp_outer_mrking_type = (dt.Rows[0]["dp_outer_mrking_type"].ToString());
                model.dp_outer_mrking_img = (dt.Rows[0]["dp_outer_mrking_img"].ToString());
                model.dp_pallet_mrking_type = (dt.Rows[0]["dp_pallet_mrking_type"].ToString());
                model.dp_pallet_mrking_img = (dt.Rows[0]["dp_pallet_mrking_img"].ToString());

                model.temp_pono_flg = Convert.ToBoolean(dt.Rows[0]["dp_pono_flg"].ToString());
                model.temp_tangcolor=(dt.Rows[0]["dp_tang_color"].ToString());
                model.temp_blko_flg = Convert.ToBoolean(dt.Rows[0]["dp_blko_flg"].ToString());
                model.temp_barcodeouter_flg = Convert.ToBoolean(dt.Rows[0]["dp_barcodeouter_flg"].ToString());
                model.temp_pkg_remarks=(dt.Rows[0]["dp_pkg_remark"].ToString());

                model.dp_stamp_type = dt.Rows[0]["dp_stamp_type"].ToString();
                if (dt.Rows[0]["dp_stamp_chart"].ToString() != "" && dt.Rows[0]["dp_stamp_chart"].ToString() != null)
                {
                    model.dp_stamp_chart = dt.Rows[0]["dp_stamp_chart"].ToString();

                }else
                {
                    Session["currentStampChartNum"] = dt.Rows[0]["dp_stamp_chart"].ToString();
                }
                model.pr_tang_color = dt.Rows[0]["pr_tang_color"].ToString();
                model.dp_handle_presence = dt.Rows[0]["dp_handle_presence"].ToString();
                model.dp_handle_type = dt.Rows[0]["dp_handle_type"].ToString();
                model.dp_handle_chart = dt.Rows[0]["dp_handle_chart"].ToString();
                if (dt.Rows[0]["dp_handle_chart"].ToString() == "" || dt.Rows[0]["dp_handle_chart"].ToString() == null)
                {
                    Session["HandleChartNo"] = "NoChart";
                }
                else
                {
                    Session["HandleChartNo"] = dt.Rows[0]["dp_handle_chart"].ToString();
                }
                model.dp_handle_subtype = dt.Rows[0]["dp_handle_subtype"].ToString();
                //model.dp_handle_recid = dt.Rows[0]["dp_handle_recid"].ToString();
                model.dp_wrapping_type = dt.Rows[0]["dp_wrapping_type"].ToString();
                model.dp_wrapping_chart = dt.Rows[0]["dp_wrapping_chart"].ToString();
                Session["WrapperChartNo"] = dt.Rows[0]["dp_wrapping_chart"].ToString();

                model.dp_wrapping_recid = dt.Rows[0]["dp_wrapping_recid"].ToString();
                model.dp_innerbox_type = dt.Rows[0]["dp_innerbox_type"].ToString();
                model.dp_innerbox_qty = dt.Rows[0]["dp_innerbox_qty"].ToString();
                model.dp_innerbox_chart = dt.Rows[0]["dp_innerbox_chart"].ToString();
                Session["InnerBoxNo"] = dt.Rows[0]["dp_innerbox_chart"].ToString();

                model.dp_innerbox_id = dt.Rows[0]["dp_innerbox_id"].ToString();
                model.dp_innerlabel_chart = dt.Rows[0]["dp_innerlabel_chart"].ToString();
                if (dt.Rows[0]["dp_innerlabel_chart"].ToString() == ""
                    || dt.Rows[0]["dp_innerlabel_chart"].ToString() == null)
                {
                    Session["InnerLabelChartNum"] = "No Label found";
                }
                else
                {
                    Session["InnerLabelChartNum"] = dt.Rows[0]["dp_innerlabel_chart"].ToString();
                }
                model.dp_outerbox_type = dt.Rows[0]["dp_outerbox_type"].ToString();
                model.dp_outerbox_chart = dt.Rows[0]["dp_outerbox_chart"].ToString();

                Session["OuterBoxChartNo"] = dt.Rows[0]["dp_outerbox_chart"].ToString();
                model.dp_outerbox_recid = dt.Rows[0]["dp_outerbox_recid"].ToString();
                model.dp_pallet_type = dt.Rows[0]["dp_pallet_type"].ToString();
                model.dp_pallet_chart = dt.Rows[0]["dp_pallet_chart"].ToString();
                Session["PalletChartNo"] = dt.Rows[0]["dp_pallet_chart"].ToString();
                model.dp_palletdtl_recid = dt.Rows[0]["dp_palletdtl_recid"].ToString();
                model.filetype = dt.Rows[0]["pr_ftype_code"].ToString() + dt.Rows[0]["pr_fstype_code"].ToString();
                model.imagename = dt.Rows[0]["dwg_imgname"].ToString();
                if (ReqId != 0)
                {

                    model.Custspeclist = objBAL.getcustspeclist(model.partno);
                    #region Session for Drawing by Dhanashree
                    string cutspec = dt.Rows[0]["pr_cut_standard"].ToString();
                    Session["CutSpecification"] = cutspec;
                    #endregion
                }
                if (model.dp_handle_chart != "" || model.dp_handle_chart != null)
                {
                    model.HandleDetailsEntityList = objBAL.gethandleimgpath(model.dp_handle_chart);
                }
                if (model.dp_wrapping_chart != "" || model.dp_wrapping_chart != null)
                {
                    model.WrapperdetailsList = objBAL.getwrappinglist(model.dp_wrapping_chart);
                }
                if (model.dp_innerbox_chart != "" || model.dp_innerbox_chart != null)
                {
                    model.innerboxlist = objBAL.getinnerboxlist(model.dp_innerbox_chart, model.pr_fstype_code, model.pr_ftype_code, model.pr_fsize_code);
                }
                if (model.dp_outerbox_chart != "" || model.dp_outerbox_chart != null)
                {
                    model.OterChartList = objBAL.GetOuterChartlist(model.dp_outerbox_chart);
                }
                if (model.dp_pallet_chart != "" || model.dp_pallet_chart != null)
                {
                    model.PalletList = objBAL.GetPalletList(model.dp_pallet_chart);
                }
                if (ReqId != 0)
                {
                    model.obj = objBAL.GetCostDetails(model.partno);
                }
                model.innerLabelList = objBAL.GetinnerLabelList(model.dp_innerlabel_chart, model.pr_ftype_code, model.pr_fstype_code, model.pr_fsize_code, model.pr_cust_id, model.pr_brand_id, model.pr_fcut_code, model.pr_ship2country_id);
                model.HistoryDetailsList = objBAL.GetHistoryDetails(model.pr_request_id, model.pr_request_type);
                model.ApprovalDetailsList = objBAL.GetApprovalDetails(model.pr_request_id, model.pr_request_type);
                model.unitdropdownlist = objBAL.getunitDropdownList();
                return View(model);
            }
            catch(Exception e)
            {
                throw e;
            }
        }


        public ActionResult ViewStamps(string Id)
        {
            //string chartNum1 = Session["currentStampChartNum"].ToString();
            StampDetailsModel stampDetailsObj = new StampDetailsModel();
            StampDetailsEntity stampsEntity = new StampDetailsEntity();
            //  stampsEntity = objBAL.getStampDetailsForModal(chartNum); 
            stampDetailsObj.StampDetailsEntityList = objBAL.getStampDetailsForModal(Id);
            // return PartialView("_ViewStamps", stampDetailsObj);
            ViewBag.stampDetailsList = stampDetailsObj.StampDetailsEntityList;
            return View("_ViewStamps", stampDetailsObj);
            // return View(stampDetailsObj.StampDetailsEntityList);
        }
        #region View Drawing as PDF by Dhanashree
        public void ViePDF()
        {
            Document doc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 0f, 0f, 0f, 0f);

            try
            {
                //if (sheets != null && sheets.Count > 0)
                //{ 
                string imagename = "";
                if (Session["FileSizeCode"] != null && Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileCutTypeCode"] != null &&
                    Session["CutSpecification"] != null)
                {
                    imagename = Session["FileSizeCode"].ToString() + Session["FileTypeCode"].ToString()
                        + Session["FileSubTypeCode"].ToString() + Session["FileCutTypeCode"].ToString() + Session["CutSpecification"].ToString();
                }
                string pdfFilePath = Server.MapPath("~/pdf/dwg/" + imagename + ".pdf");
                FileStream pdfFile = new FileStream(pdfFilePath, FileMode.Create);
                PdfWriter writer = PdfWriter.GetInstance(doc, pdfFile);
                doc.Open();
                //foreach (Sheet sheetObj in sheets)
                //{
                Paragraph paragraph = new Paragraph("Drawing");
                string imageURL = Server.MapPath("~/pdf/dwg/" + imagename + ".jpeg");
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                //Resize image depend upon your need
                //jpg.SetAbsolutePosition();

                jpg.ScaleToFit(850f, 1500f);
                //jpg.ScaleToFit(PageSize.A4.Width , PageSize.A4.Height * 2f);
                //Give space before image
                jpg.SpacingBefore = 90f;
                //Give some space after the image
                //jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.ALIGN_JUSTIFIED_ALL;
                //doc.Add(paragraph);
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(" "));
                doc.Add(jpg);
                doc.NewPage();
                // }

                doc.Close();

                Response.Clear();

                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=drawing.pdf");
                // Response.TransmitFile(Server.MapPath(".") + "/Default.pdf");
                string path = Server.MapPath("~/pdf/dwg/" + imagename + ".pdf");
                Response.TransmitFile(path);
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(path);
                if (buffer != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.BinaryWrite(buffer);
                }
                Response.End();
                //}
            }
            finally
            {
                doc.Close();
            }
        }


        #region Clone of Part No by Dhanashree
        public ActionResult Clone(string requestid)
        {
            string ReqNo = requestid;
            var pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");
            int req_id =Convert.ToInt32(requestid.Substring(1, requestid.Length - 1));
            Session["requestIdFinder"] = pr_uniq_id;
            string flag = "insert_clone_request";
            objBAL.CreateCloneReq(pr_uniq_id, req_id, flag);
            int clonereqid = objBAL.GetNewReq(pr_uniq_id);
            Session["requestId"] = clonereqid;
            string clonereqtype = objBAL.GetNewReqtype(pr_uniq_id);
            Session["requestype"] = clonereqtype;
            string clonerequest =  Session["requestype"].ToString()+ Session["requestId"].ToString();
            
            return RedirectToAction("EditCustomerDetailsIndex", "CustomerDetails",new { ReqNo = clonerequest });
        }
        #endregion
        #endregion




        [HttpGet]
        public ActionResult getfilesubtypedetails(string sizecode, string filecode)
        {
            FileSubTypeEntity rm = new FileSubTypeEntity();
            //string s = reqno;
            //string[] values = s.Split(',');
            //string sizecode = values[0].Trim().ToString();
            //string filecode = values[1].Trim().ToString();
            ////string sizecode = "04";
            ////string filecode = "FLF";
            //rm.pm_fsize_code = reqno;
            //string flag = "getfilesubtypes";
            rm.fileSubTypedetaillist = getdetailssubfile(sizecode, filecode);

            return PartialView("_FileSubTypeView", rm);

        }


        public List<FileSubTypeEntity> getdetailssubfile(string sizecode, string filecode)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getfilesubtypesview";
                dt = objBAL.getfilesubtype(flag, sizecode, filecode);
                List<FileSubTypeEntity> listfiletype = new List<FileSubTypeEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileSubTypeEntity
                    {
                        // pm_request_id = item.Field<int>("pm_request_id"),
                        // pm_request_type = item.Field<string>("pm_request_type"),
                        pm_dim_parm_catg = item.Field<string>("pm_dim_parm_catg"),
                        pm_dim_parm_name = item.Field<string>("pm_dim_parm_name"),
                        pm_dim_parm_code = item.Field<string>("pm_dim_parm_code"),
                        pm_dim_parm_scrnvalue = item.Field<string>("pm_dim_parm_scrnvalue"),
                        pm_dim_parm_dwgvalue = item.Field<decimal>("pm_dim_parm_dwgvalue")
                    }
                    ).ToList();
                }
                return listfiletype;
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpGet]
        public ActionResult getdetails()
        {
            string sizecode = Session["filesizecode"].ToString();
            string filecode = Session["filecode"].ToString();
            FileSubTypeEntity rm = new FileSubTypeEntity();
            //string s = reqno;
            //string[] values = s.Split(',');
            //string sizecode = values[0].Trim().ToString();
            //string filecode = values[1].Trim().ToString();
            ////string sizecode = "04";
            ////string filecode = "FLF";
            //rm.pm_fsize_code = reqno;
            //string flag = "getfilesubtypes";
            rm.fileSubTypedetaillist = getdetailssubfile(sizecode, filecode);

            return PartialView("_FileSubTypeView", rm);
        }


        #region Manage View Set
        public ActionResult ManageSetView(string partno)
        {
          
            DispachProductSkuEntity model = new DispachProductSkuEntity();
            model.partno = partno;
            string type = partno.Substring(0, 1);
            Session["type"] = type;
            Session["partno"] = partno;
            //partno = partno.Remove(partno.Length - 1);
            partno = partno.Substring(1);
            Session["requestId"] = partno;
            DataTable dt = new DataTable();
            //int ReqId = objBAL.getReqId(partno);
            int ReqId = Convert.ToInt32(partno);
            string completepartno = objBAL.getReqId(model.partno);
            if (completepartno != "")
            {
                model.complete_partno = completepartno;
                string complete_partno = model.complete_partno;
                model.split1 = complete_partno.Substring(0, 2);
                model.split2 = complete_partno.Substring(2, 8);
                model.split3 = complete_partno.Substring(10, 3);
            }
            if (ReqId != 0)
            {
                dt = objBAL.getdispatchdetails(ReqId);
            }
            model.pr_request_type = dt.Rows[0]["pr_request_type"].ToString();
            model.pr_request_id = Convert.ToInt16(dt.Rows[0]["pr_request_id"].ToString());
            // model.partno = partno;
            model.pr_cust_id = dt.Rows[0]["pr_cust_id"].ToString();
            model.pr_cust_name = dt.Rows[0]["pr_cust_name"].ToString();
            model.pr_brand_id = dt.Rows[0]["pr_brand_id"].ToString();
            model.pr_brand_desc = dt.Rows[0]["pr_brand_desc"].ToString();
            model.pr_ship2country_id = dt.Rows[0]["pr_ship2country_id"].ToString();
            model.pr_ship2country_name = dt.Rows[0]["pr_ship2country_name"].ToString();
            model.pr_sap_no = dt.Rows[0]["pr_sap_no"].ToString();
            model.pr_customer_part_no = dt.Rows[0]["pr_customer_part_no"].ToString();
            model.pr_fsize_code = dt.Rows[0]["pr_fsize_code"].ToString();

            Session["filesizecode"] = model.pr_fsize_code;
            model.fs_size_inches = dt.Rows[0]["fs_size_inches"].ToString();
            model.pr_ftype_code = dt.Rows[0]["pr_ftype_code"].ToString();
            model.pr_ftype_desc = dt.Rows[0]["pr_ftype_desc"].ToString();
            model.pr_fstype_code = dt.Rows[0]["pr_fstype_code"].ToString();
            model.pr_fstype_desc = dt.Rows[0]["pr_fstype_desc"].ToString();
            Session["filecode"] = model.pr_ftype_code + model.pr_fstype_code;
            model.pr_filecode = model.pr_ftype_code + model.pr_fstype_code;
            model.pr_hardness = dt.Rows[0]["pr_hardness"].ToString();
            model.pr_tang_color = dt.Rows[0]["pr_tang_color"].ToString();

            if (dt.Rows[0]["pr_uncutattang"].ToString() != null && dt.Rows[0]["pr_uncutattang"].ToString() != "")
            {

                model.pr_uncutattang = Convert.ToDecimal(dt.Rows[0]["pr_uncutattang"].ToString());
            }
            else
            {
                model.pr_uncutattang = 0;
            }
            //model.pr_uncutatpoint = Convert.ToDecimal(dt.Rows[0]["pr_uncutatpoint"].ToString());
            if (dt.Rows[0]["pr_uncutatedge"].ToString() != "" && dt.Rows[0]["pr_uncutatedge"].ToString() != null)
            {
                model.pr_uncutatedge = Convert.ToDecimal(dt.Rows[0]["pr_uncutatedge"].ToString());
            }
            else
            {
                model.pr_uncutatedge = 0;
            }
            if (dt.Rows[0]["pr_upcutncline"].ToString() != "" && dt.Rows[0]["pr_upcutncline"].ToString() != null)
            {
                model.pr_upcutncline = Convert.ToDecimal(dt.Rows[0]["pr_upcutncline"].ToString());
            }
            else
            {
                model.pr_upcutncline = 0;
            }
            if (dt.Rows[0]["pr_overcutincline"].ToString() != null && dt.Rows[0]["pr_overcutincline"].ToString() != "")
            {
                model.pr_overcutincline = Convert.ToDecimal(dt.Rows[0]["pr_overcutincline"].ToString());
            }
            if (dt.Rows[0]["pr_edgecutincline"].ToString() != null && dt.Rows[0]["pr_edgecutincline"].ToString() != "")
            {
                model.pr_edgecutincline = Convert.ToDecimal(dt.Rows[0]["pr_edgecutincline"].ToString());
            }
            else
            {
                model.pr_overcutincline = 0;
            }


            model.pr_fcut_code = dt.Rows[0]["pr_fcut_code"].ToString();
            model.pr_fcut_desc = dt.Rows[0]["pr_fcut_desc"].ToString();
            #region Session for Drawing by Dhanashree
            Session["FileSizeCode"] = dt.Rows[0]["pr_fsize_code"].ToString();
            Session["FileTypeCode"] = dt.Rows[0]["pr_ftype_code"].ToString();
            Session["FileSubTypeCode"] = dt.Rows[0]["pr_fstype_code"].ToString();
            Session["FileCutTypeCode"] = dt.Rows[0]["pr_fcut_code"].ToString();
            #endregion
            if (dt.Rows[0]["pr_rawmtrl_height"].ToString() != "")
            {
                model.pr_rawmtrl_height = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_height"].ToString());
            }
            if (dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString() != "")
            {
                model.pr_rawmtrl_width = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString());
            }
            if (dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString() != "")
            {
                model.pr_rawmtrl_kgperthsnd = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_kgperthsnd"].ToString());
            }
            if (dt.Rows[0]["pr_rawmtrl_netwt"].ToString() != "")
            {
                model.pr_rawmtrl_netwt = Convert.ToDecimal(dt.Rows[0]["pr_rawmtrl_netwt"].ToString());
            }
            model.pr_rawmtrl_cd = dt.Rows[0]["pr_rawmtrl_cd"].ToString();
            model.pr_isApproved = dt.Rows[0]["pr_isApproved"].ToString();
            //model.pr_isActive = dt.Rows[0][""].ToString();
            model.pr_createby = dt.Rows[0]["pr_createby"].ToString();
            model.pr_approveby = dt.Rows[0]["pr_approveby"].ToString();
            model.pr_uniq_id = dt.Rows[0]["pr_uniq_id"].ToString();
            model.pr_remarks = dt.Rows[0]["pr_remarks"].ToString();
            model.dp_id = Convert.ToInt16(dt.Rows[0]["dp_id"].ToString());
            model.dp_request_type = dt.Rows[0]["dp_request_type"].ToString();
            model.dp_dispatch_digit = dt.Rows[0]["dp_dispatch_digit"].ToString();
            model.dp_stamp_type = dt.Rows[0]["dp_stamp_type"].ToString();
            model.dp_stamp_chart = dt.Rows[0]["dp_stamp_chart"].ToString();
            //model.dp_barcode_flg = dt.Rows[0]["dp_barcode_flg"].ToString();
            //model.dp_lineno_flg = dt.Rows[0]["dp_lineno_flg"].ToString();
            //model.dp_hologram_flg = dt.Rows[0]["dp_hologram_flg"].ToString();
            //model.dp_india_flg = dt.Rows[0]["dp_india_flg"].ToString();
            model.dp_qltyseal_flg = Convert.ToBoolean(dt.Rows[0]["dp_qltyseal_flg"].ToString());
            model.dp_barcode_flg = Convert.ToBoolean(dt.Rows[0]["dp_barcode_flg"].ToString());
            model.dp_lineno_flg = Convert.ToBoolean(dt.Rows[0]["dp_lineno_flg"].ToString());
            model.dp_hologram_flg = Convert.ToBoolean(dt.Rows[0]["dp_hologram_flg"].ToString());
            model.dp_prcstkr_flg = Convert.ToBoolean(dt.Rows[0]["dp_prcstkr_flg"].ToString());
            model.dp_india_flg = Convert.ToBoolean(dt.Rows[0]["dp_india_flg"].ToString());
            model.dp_polybag_flg = Convert.ToBoolean(dt.Rows[0]["dp_polybag_flg"].ToString());
            model.dp_silicagel_flg = Convert.ToBoolean(dt.Rows[0]["dp_silicagel_flg"].ToString());
            model.dp_fumigation_flg = Convert.ToBoolean(dt.Rows[0]["dp_fumigation_flg"].ToString());

            model.dp_promotionalitem_flg = Convert.ToBoolean(dt.Rows[0]["dp_promo_item_flg"].ToString());
            model.dp_strap = dt.Rows[0]["dp_strap"].ToString();
            model.dp_cellotape_flg = Convert.ToBoolean(dt.Rows[0]["dp_cellotape_flg"].ToString());
            model.dp_shrinkwrap_flg = Convert.ToBoolean(dt.Rows[0]["dp_shrinkwrap_flg"].ToString());
            model.dp_decicant_flg = Convert.ToBoolean(dt.Rows[0]["dp_decicant_flg"].ToString());
            model.dp_paperwool_flg = Convert.ToBoolean(dt.Rows[0]["dp_paperwool_flg"].ToString());

            model.dp_outer_mrking_type = (dt.Rows[0]["dp_outer_mrking_type"].ToString());
            model.dp_pallet_mrking_type = (dt.Rows[0]["dp_pallet_mrking_type"].ToString());


            model.dp_handle_presence = dt.Rows[0]["dp_handle_presence"].ToString();
            model.dp_handle_type = dt.Rows[0]["dp_handle_type"].ToString();
            model.dp_handle_chart = dt.Rows[0]["dp_handle_chart"].ToString();
            model.dp_handle_subtype = dt.Rows[0]["dp_handle_subtype"].ToString();
            //model.dp_handle_recid = dt.Rows[0]["dp_handle_recid"].ToString();
            model.dp_wrapping_type = dt.Rows[0]["dp_wrapping_type"].ToString();
            model.dp_wrapping_chart = dt.Rows[0]["dp_wrapping_chart"].ToString();
            model.dp_wrapping_recid = dt.Rows[0]["dp_wrapping_recid"].ToString();
            model.dp_innerbox_type = dt.Rows[0]["dp_innerbox_type"].ToString();
            model.dp_innerbox_qty = dt.Rows[0]["dp_innerbox_qty"].ToString();
            model.dp_innerbox_chart = dt.Rows[0]["dp_innerbox_chart"].ToString();
            model.dp_innerbox_id = dt.Rows[0]["dp_innerbox_id"].ToString();
            model.dp_innerlabel_chart = dt.Rows[0]["dp_innerlabel_chart"].ToString();
            model.dp_outerbox_type = dt.Rows[0]["dp_outerbox_type"].ToString();
            model.dp_outerbox_chart = dt.Rows[0]["dp_outerbox_chart"].ToString();
            model.dp_outerbox_recid = dt.Rows[0]["dp_outerbox_recid"].ToString();
            model.dp_pallet_type = dt.Rows[0]["dp_pallet_type"].ToString();
            model.dp_pallet_chart = dt.Rows[0]["dp_pallet_chart"].ToString();
            model.dp_palletdtl_recid = dt.Rows[0]["dp_palletdtl_recid"].ToString();
            model.filetype = dt.Rows[0]["pr_ftype_code"].ToString() + dt.Rows[0]["pr_fstype_code"].ToString();
            model.imagename = dt.Rows[0]["dwg_imgname"].ToString();
            if (ReqId != 0)
            {

                model.Custspeclist = objBAL.getcustspeclist(model.partno);
                
                //string cutspec = model.Custspeclist[0].prc_parameter_value;
                //Session["CutSpecification"] = cutspec;
             
            }
            if (model.dp_handle_chart != "" || model.dp_handle_chart != null)
            {
                model.HandleDetailsEntityList = objBAL.gethandleimgpath(model.dp_handle_chart);
            }
            if (model.dp_wrapping_chart != "" || model.dp_wrapping_chart != null)
            {
                model.WrapperdetailsList = objBAL.getwrappinglist(model.dp_wrapping_chart);
            }
            if (model.dp_innerbox_chart != "" || model.dp_innerbox_chart != null)
            {
                model.innerboxlist = objBAL.getinnerboxlist(model.dp_innerbox_chart, model.pr_fstype_code, model.pr_ftype_code, model.pr_fsize_code);
            }
            if (model.dp_outerbox_chart != "" || model.dp_outerbox_chart != null)
            {
                model.OterChartList = objBAL.GetOuterChartlist(model.dp_outerbox_chart);
            }
            if (model.dp_pallet_chart != "" || model.dp_pallet_chart != null)
            {
                model.PalletList = objBAL.GetPalletList(model.dp_pallet_chart);
            }

            if (ReqId != 0)
            {
                model.obj = objBAL.GetCostDetails(model.partno);
            }
            model.innerLabelList = objBAL.GetinnerLabelList(model.dp_innerlabel_chart, model.pr_ftype_code, model.pr_fstype_code, model.pr_fsize_code, model.pr_cust_id, model.pr_brand_id, model.pr_fcut_code, model.pr_ship2country_id);
            model.HistoryDetailsList = objBAL.GetHistoryDetails(model.pr_request_id, model.pr_request_type);
            model.ApprovalDetailsList = objBAL.GetApprovalDetails(model.pr_request_id, model.pr_request_type);
            model.skudetailslist = objBAL.GetSkuDetails(model.pr_request_id, model.pr_request_type);
            return View(model);
        }
        #endregion

        #region Norms View
        [HttpGet]
        public JsonResult NormsProcessViewlist(string UnitCode)
        {
            NormsMasterModel nmobj = new NormsMasterModel();
            string Requesrtype = Session["type"].ToString();
            string RequestId = Session["partno"].ToString();
            var reqest = RequestId.Substring(1);
            nmobj.NormsProcesslist = getNormsProcessViewlist(reqest, Requesrtype, UnitCode);
            if (nmobj.NormsProcesslist.Count == 0)
            {
                return Json(new { success = false, NormsPElist = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, NormsPElist = nmobj.NormsProcesslist, req_id = reqest }, JsonRequestBehavior.AllowGet);
            }

        }
        public List<NormsMasterModel> getNormsProcessViewlist(string RequestId, string Requesrtype, string UnitCode)
        {
            string flag = "NormsItemProcessEquView";

            DataTable dt = objBAL.getNormsProcessViewlist(flag, RequestId, Requesrtype, UnitCode);
            NormsMasterModel modelobj = new NormsMasterModel();
            List<NormsMasterModel> list = new List<NormsMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    NormsMasterModel objProc = new NormsMasterModel();
                    objProc.nm_opern_name = dt.Rows[i]["nm_opern_name"].ToString();
                    objProc.nm_equ_name = dt.Rows[i]["nm_equ_name"].ToString();
                    objProc.nm_equ_type = dt.Rows[i]["nm_equ_type"].ToString();
                    objProc.nm_norms_value = dt.Rows[i]["nm_norms_value"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }
        #endregion
    }
}