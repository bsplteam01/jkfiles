﻿using JKFiles.WebApp.ActionFilters;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class EquipmentController : Controller
    {
        BAL objBAL = new BAL();
        // GET: Equipment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EquipmentRegister()
        {
            EquipmentMasterModel obj = new EquipmentMasterModel();
            obj.equipmentlist = getEquipmentDropdownList();
            obj.equipmenttypelist = EquipmentTypeDropdownList();
            obj.UnitDropdownlist = get_unitlist();
            obj.oprationmasterlist = getoprationMaster();

            return PartialView("_EquipmentRegister", obj);
        }

        public List<EquipmentMasterModel> getEquipmentDropdownList()
        {
            string flag = "EquipmentDropdownList";
            DataTable dt = objBAL.getEquipmentDropdownList(flag);
            EquipmentMasterModel modelobj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objeqi = new EquipmentMasterModel();
                    objeqi.equ_name = dt.Rows[i]["equ_name"].ToString();
                    list.Add(objeqi);
                }
            }
            return list.ToList();
        }
        public List<EquipmentMasterModel> EquipmentTypeDropdownList()
        {
            string flag = "EquipmentTypeDropdownList";
            DataTable dt = objBAL.getEquipmentDropdownList(flag);
            EquipmentMasterModel modelobj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objeqi = new EquipmentMasterModel();
                    objeqi.equ_type = dt.Rows[i]["equ_type"].ToString();
                    list.Add(objeqi);
                }
            }
            return list.ToList();
        }
        [HttpGet]
        public JsonResult getUnitValuesteram(string uni_unit_code)
        {
            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.unitvaluestreamlist = getunitvaluestreamlist(uni_unit_code);
            return Json(obj.unitvaluestreamlist, JsonRequestBehavior.AllowGet);
        }
        public List<EquipmentMasterEntity> getunitvaluestreamlist(string uni_unit_code)
        {
            DataTable dt = new DataTable();
            string flag = "unitvaluestream";
            dt = objBAL.getunitvaluestreamlist(flag, uni_unit_code);
            List<EquipmentMasterEntity> unitVSlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                unitVSlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    val_valuestream_code = item.Field<string>("val_valuestream_code"),
                    val_valuestream_name = item.Field<string>("val_valuestream_name")
                }).ToList();
            }
            return unitVSlist;
        }
        
        [HttpGet]
        public JsonResult getUnitEquipment(string uni_unit_code)
        {

            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.unitequipmentlist = getUnitEquipmentlist(uni_unit_code);

            return Json(obj.unitequipmentlist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getUnitEquipmentType(string uni_unit_code, string equ_name)
        {
            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.unitequipmenttypelist = getunitequipmenttypelist(uni_unit_code, equ_name);

            return Json(obj.unitequipmenttypelist, JsonRequestBehavior.AllowGet);
        }
        public List<EquipmentMasterEntity> getUnitEquipmentlist(string uni_unit_code)
        {
            DataTable dt = new DataTable();
            string flag = "unitequipment";
            dt = objBAL.getUnitEquipmentlist(flag, uni_unit_code);
            List<EquipmentMasterEntity> unitEQlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                unitEQlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    equ_name = item.Field<string>("equ_name"),
                   // equ_type = item.Field<string>("equ_type")

                }).ToList();
            }
            return unitEQlist;
        }
        public List<EquipmentMasterEntity> getunitequipmenttypelist(string uni_unit_code, string equ_name)
        {
            DataTable dt = new DataTable();
            string flag = "unitequipmenttype";
            dt = objBAL.getunitequipmenttypelist(flag, uni_unit_code, equ_name);
            List<EquipmentMasterEntity> unitEQlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                unitEQlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    equ_type = item.Field<string>("equ_type")

                }).ToList();
            }
            return unitEQlist;
        }

        [HttpGet]
        public JsonResult getUnitEquipmentAsset(string uni_unit_code, string equ_name, string equ_type)
        {

            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.EquipmentAssetlist = getEquipmentAssetlist(uni_unit_code, equ_name, equ_type);

            return Json(obj.EquipmentAssetlist, JsonRequestBehavior.AllowGet);
        }
        public List<EquipmentMasterEntity> getEquipmentAssetlist(string uni_unit_code, string equ_name, string equ_type)
        {
            DataTable dt = new DataTable();
            string flag = "unitequipmentTypeAsset";
            dt = objBAL.getUnitEquipmentAssetlist(flag, uni_unit_code, equ_name, equ_type);
            List<EquipmentMasterEntity> Assetlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                Assetlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    equ_assetno = item.Field<Int32>("equ_assetno")

                }).ToList();
            }
            return Assetlist;
        }

        [HttpPost]
        public ActionResult UpdateAssetAllocation(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                EquipmentMasterEntity emobj = new EquipmentMasterEntity();
                string Asset = form["Asset[]"].ToString();
                string[] assetno = Asset.Split(',');
                for (int i = 0; i < assetno.Length; i++)
                {
                    emobj.aa_approveby = Session["User_Id"].ToString();
                    emobj.aa_unit_code = form["uni_unit_code"];
                    emobj.aa_main_valstream = form["val_valuestream_code"];
                    if (form["val_alternate_valuestream_code"] == "Select")
                    {
                        emobj.aa_alt_valstream = "";
                    }
                    else
                    {
                        emobj.aa_alt_valstream = form["val_alternate_valuestream_code"];
                    }

                    emobj.aa_operseq_no = Convert.ToInt32(form["opr_operation_seq"]);
                    emobj.aa_assetno = Convert.ToInt32(assetno[i].ToString());
                    emobj.flag = "updateAssetallcation";
                    objBAL.saveAssetValueStream(emobj);
                }

                return Json(new { success = true, message = "Record insert succesfully." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getValuesteramProcess(string val_valuestream_code)
        {
            EquipmentMasterModel obj = new EquipmentMasterModel();
            obj.oprationmasterlist = ValuesteramProcess(val_valuestream_code);
            return Json(obj.oprationmasterlist, JsonRequestBehavior.AllowGet);
        }

        public List<EquipmentMasterModel> ValuesteramProcess(string val_valuestream_code)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getvaluestreamprocesslist";
                dt = objBAL.getValuesteramProcess(flag, val_valuestream_code);

                List<EquipmentMasterModel> listopretionlist = new List<EquipmentMasterModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listopretionlist = dt.AsEnumerable().Select(item => new EquipmentMasterModel
                    {
                        ppm_process_seqno = item.Field<Int16>("ppm_process_seqno"),
                        ppm_operation_name =  item.Field<string>("ppm_operation_name"),

                    }).ToList();
                }
                return listopretionlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EquipmentMasterModel> getoprationMaster()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getOpretionmasterlist";
                dt = objBAL.getmasterList(flag);


                List<EquipmentMasterModel> listopretionlist = new List<EquipmentMasterModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listopretionlist = dt.AsEnumerable().Select(item => new EquipmentMasterModel
                    {
                        opr_operation_seq = item.Field<Int16>("opr_operation_seq"),
                        opr_operation_name = item.Field<Int16>("opr_operation_seq") + "-" + item.Field<string>("opr_operation_name"),

                    }).ToList();
                }
                return listopretionlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<EquipmentMasterModel> get_unitlist()
        {
            string flag = "unitlist";
            DataTable dt = objBAL.getUnitlist(flag);
            EquipmentMasterModel modelobj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objunit = new EquipmentMasterModel();
                    objunit.uni_unit_code = dt.Rows[i]["uni_unit_code"].ToString();
                    objunit.uni_unit_name = dt.Rows[i]["uni_unit_name"].ToString();
                    objunit.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();
                    list.Add(objunit);
                }
            }
            return list.ToList();
        }

        [HttpPost]
        public ActionResult AddEquipment(EquipmentMasterModel objmodel)
        {
            if (ModelState.IsValid)
            {
                EquipmentMasterEntity opobj = new EquipmentMasterEntity();
                int quantity = objmodel.quantity;
                for (int i = 0; i < quantity; i++)
                {
                    opobj.equ_name = objmodel.equ_name;
                    if (objmodel.equ_type == null)
                    {
                        opobj.equ_type = "NA";
                    }
                    else
                    {
                        opobj.equ_type = objmodel.equ_type;
                    }

                    opobj.equ_approveby = Session["User_Id"].ToString();
                    opobj.equ_createby = Session["User_Id"].ToString();
                    opobj.flag = "insertequipment";
                    int j = objBAL.saveequipmentDetails(opobj);
                    opobj.aa_assetno = j;
                    opobj.aa_unit_code = objmodel.uni_unit_code;
                    opobj.aa_startdate = objmodel.aa_startdate;
                    opobj.aa_enddate = objmodel.aa_enddate;
                    opobj.aa_approveby = Session["User_Id"].ToString();
                    opobj.aa_createby = Session["User_Id"].ToString();
                    objBAL.SaveAssetAllocation(opobj);
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
        public ActionResult EquipmentNo()
        {
            EquipmentMasterModel obj = new EquipmentMasterModel();
            obj.equipmentlist = getEquipmentDropdownList();
            obj.equipmenttypelist = EquipmentTypeDropdownList();
            obj.UnitDropdownlist = get_unitlist();
            obj.UnitTypeDropdownlist = get_unitlist();
            obj.oprationmasterlist = getoprationMaster();
            return PartialView("_EquipmentNo", obj);
        }

        public ActionResult EquipmentList(string UnitType, string uni_unit_code, string equ_name, string equ_type)
        {
            EquipmentMasterModel obj = new EquipmentMasterModel();
            obj.equipmentlist = getEquipmentList();
            var list = obj.equipmentlist;
            if (!String.IsNullOrEmpty(UnitType))
            {
                list = list.Where(x => x.uni_unit_type == UnitType).ToList();
            }
            if (!String.IsNullOrEmpty(uni_unit_code))
            {
                list = list.Where(x => x.aa_unit_code == uni_unit_code).ToList();
            }
           
            if (!String.IsNullOrEmpty(equ_name))
            {
                list = list.Where(x => x.equ_name == equ_name).ToList();
            }
            if (!String.IsNullOrEmpty(equ_type))
            {
                list = list.Where(x => x.equ_type == equ_type).ToList();
            }

              list.Take(100);

            return Json(new { success = true, equiplist = list }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                IList<EquipmentMasterModel> Data = getEquipmentList();
                var customerData = (from tempcustomer in Data
                                    select tempcustomer);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.uni_unit_type.ToUpper().Contains(searchValue.ToUpper())
                        || m.uni_unit_name.ToUpper().Contains(searchValue.ToUpper()) || m.equ_name.ToString().ToUpper().Contains(searchValue.ToUpper()) ||m.equ_type.ToString().ToUpper().Contains(searchValue.ToUpper()));
                }
                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EquipmentMasterModel> getEquipmentList()
        {
            string flag = "equipmentList";
            DataTable dt = objBAL.getequipmentlist(flag);
            EquipmentMasterModel modelobj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objeqi = new EquipmentMasterModel();
                    objeqi.aa_id = Convert.ToInt32(dt.Rows[i]["aa_id"].ToString());
                    objeqi.equ_name = dt.Rows[i]["equ_name"].ToString();
                    objeqi.equ_type = dt.Rows[i]["equ_type"].ToString();
                    objeqi.aa_unit_code = dt.Rows[i]["aa_unit_code"].ToString();
                    objeqi.aa_assetno = Convert.ToInt32(dt.Rows[i]["aa_assetno"].ToString());
                    objeqi.stringaa_startdate = dt.Rows[i]["aa_startdate"].ToString();
                    objeqi.stringaa_enddate = dt.Rows[i]["aa_enddate"].ToString();
                    objeqi.uni_unit_name = dt.Rows[i]["uni_unit_name"].ToString();
                    objeqi.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();

                    list.Add(objeqi);
                }
            }
            return list.ToList();
        }

        public ActionResult Equipmentallocationlist(string UnitType,string uni_unit_code,string opr_operation_seq,string equ_name,string equ_type)
        {
            string flag = "Equipmentallocationlist";
            DataTable dt = objBAL.getEquipmentvalueStream(flag);
            EquipmentMasterModel modelobj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objunit = new EquipmentMasterModel();
                  //  objunit.aa_assetno = Convert.ToInt32(dt.Rows[i]["aa_assetno"].ToString());
                    objunit.aa_operseq_no = Convert.ToInt32(dt.Rows[i]["aa_operseq_no"].ToString());
                    objunit.VSCode = dt.Rows[i]["aa_unit_code"].ToString() + "_" + dt.Rows[i]["uni_unit_attachTo"].ToString() + "_" + dt.Rows[i]["aa_main_valstream"].ToString();
                    objunit.opr_operation_name = dt.Rows[i]["aa_operseq_no"].ToString() + "-" + dt.Rows[i]["opr_operation_name"].ToString();
                    objunit.aa_main_valstream = dt.Rows[i]["aa_main_valstream"].ToString();
                    objunit.aa_unit_code = dt.Rows[i]["aa_unit_code"].ToString();
                    objunit.uni_unit_attachTo = dt.Rows[i]["uni_unit_attachTo"].ToString();
                    objunit.equ_name = dt.Rows[i]["equ_name"].ToString();
                    objunit.equ_type = dt.Rows[i]["equ_type"].ToString();
                    objunit.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();
                    objunit.aa_main_valstream = dt.Rows[i]["aa_main_valstream"].ToString();
                    string tempqt = objBAL.getAllocateequipmentqty(objunit.aa_operseq_no, dt.Rows[i]["aa_unit_code"].ToString(), objunit.aa_main_valstream);
                    objunit.quantity =Convert.ToInt32(tempqt);
                    list.Add(objunit); 
                }
                if (!String.IsNullOrEmpty(UnitType))
                {
                    list = list.Where(x => x.uni_unit_type == UnitType).ToList();
                }
                if (!String.IsNullOrEmpty(uni_unit_code))
                {
                    list = list.Where(x => x.aa_unit_code == uni_unit_code).ToList();
                }
                if (!String.IsNullOrEmpty(opr_operation_seq))
                {
                    list = list.Where(x => x.aa_operseq_no == Convert.ToInt32(opr_operation_seq)).ToList();
                }
                if (!String.IsNullOrEmpty(equ_name))
                {
                    list = list.Where(x => x.equ_name == equ_name).ToList();
                }
                if (!String.IsNullOrEmpty(equ_type))
                {
                    list = list.Where(x => x.equ_type == equ_type).ToList();
                }
            }
            return Json(new { success = true, equivaluelist = list }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchEquipment(string UnitType, string UnitName, int EqiId, string Equitype)
        {
            EquipmentMasterModel obj = new EquipmentMasterModel();
            obj.equipmentlist = getEquipmentList();

            if (!String.IsNullOrEmpty(UnitType))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.uni_unit_type == UnitType).ToList();
            }
            if (!String.IsNullOrEmpty(UnitName))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.unitname == UnitName).ToList();
            }
            if (!String.IsNullOrEmpty(EqiId.ToString()))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.equ_id == EqiId).ToList();
            }
            if (!String.IsNullOrEmpty(Equitype))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.equ_type == Equitype).ToList();
            }
            return PartialView("_EquipmentRegister", obj);
        }
        public ActionResult SearchEquipmentAsset(string UnitType, string UnitName, int EqiId, string Equitype)
        {
            EquipmentMasterModel obj = new EquipmentMasterModel();
            obj.equipmentlist = getEquipmentList();

            if (!String.IsNullOrEmpty(UnitType))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.uni_unit_type == UnitType).ToList();
            }
            if (!String.IsNullOrEmpty(UnitName))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.unitname == UnitName).ToList();
            }
            if (!String.IsNullOrEmpty(EqiId.ToString()))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.equ_id == EqiId).ToList();
            }
            if (!String.IsNullOrEmpty(Equitype))
            {
                obj.equipmentlist = obj.equipmentlist.Where(x => x.equ_type == Equitype).ToList();
            }
            return PartialView("_EquipmentNo", obj);
        }

        public ActionResult EquipmentAssetNo(int EquipmentID)
        {
            string flag = "equipmentAssetList";
            DataTable dt = objBAL.getEquipmentAssset(flag, EquipmentID);
            EquipmentMasterModel obj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objeqi = new EquipmentMasterModel();
                    objeqi.equ_id = Convert.ToInt32(dt.Rows[i]["equ_id"].ToString());
                    objeqi.equ_code = dt.Rows[i]["equ_code"].ToString();
                    objeqi.equ_name = dt.Rows[i]["equ_name"].ToString();
                    objeqi.equ_type = dt.Rows[i]["equ_type"].ToString();
                    objeqi.equ_assetno = Convert.ToInt32(dt.Rows[i]["equ_assetno"].ToString());
                    objeqi.equ_equipments_per_cluster = Convert.ToInt32(dt.Rows[i]["equ_equipments_per_cluster"]);
                    objeqi.equ_clusters = Convert.ToInt32(dt.Rows[i]["equ_clusters"]);
                    objeqi.equ_shifts_per_day = Convert.ToInt32(dt.Rows[i]["equ_shifts_per_day"].ToString());
                    objeqi.unitname = dt.Rows[i]["unitname"].ToString();
                    objeqi.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();
                    list.Add(objeqi);
                }
            }
            return Json(new { success = true, equivaluelist = list }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAssetNoTotal(string uni_unit_code,string equ_name,string equ_type,string val_valuestream_code,string opr_operation_seq)
        {
            string TotalAsset = objBAL.getAssetNoTotal(uni_unit_code, equ_name, equ_type, val_valuestream_code,opr_operation_seq);
            string AllocateAsset = objBAL.getAllocateAsset(uni_unit_code, equ_name, equ_type, val_valuestream_code, opr_operation_seq);
            string AvailbleAsset = objBAL.getAvailbleAsset(uni_unit_code, equ_name, equ_type, val_valuestream_code, opr_operation_seq);
            return Json(new { success = true, TotalAsset= TotalAsset, AllocateAsset= AllocateAsset, AvailbleAsset= AvailbleAsset }, JsonRequestBehavior.AllowGet);
        }
       
    }
}