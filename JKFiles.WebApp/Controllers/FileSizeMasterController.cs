﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.ActionFilters;
using System.Linq.Dynamic;
namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class FileSizeMasterController : Controller
    {
        BAL objBAL = new BAL();
        // GET: FileSizeMater
        FileSizeDetailsEntity fs = new FileSizeDetailsEntity();
        #region FileSize Master modify by Swapnil
        public ActionResult Index()
        {
            // fs.filelist = getFileSize();



            // return View(fs);
            return View();
        }
        public ActionResult NewRequest()
        {
            fs.FileSizeNoActiveList = objBAL.FileSizeListinactive();
            return PartialView("_NewRequest", fs);
        }
        public ActionResult ApprovedRequest()
        {
            fs.FileSizeNoApproveList = objBAL.FileSizeListNoApprove();
            return PartialView("_ApprovedRequest", fs);
        }
        public ActionResult FilesizemstrDetails(string fsizecode, int? page)
        {
           // fs.filelist = getFileSize();
           // return PartialView("_FilesizemstrDetails", fs);


            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<FileSizeDetailsEntity> fz = null;
            FileSizeDetailsEntity fs = new FileSizeDetailsEntity();
            List<FileSizeDetailsEntity> fzlist = new List<FileSizeDetailsEntity>();
            fzlist = getFileSize();

            if (!String.IsNullOrEmpty(fsizecode))
            {
                ViewBag.selectedCutStandardName = fsizecode;
                fzlist = fzlist.Where(x => x.fs_size_code == fsizecode).ToList();
            }

            fs.filelist = fzlist;
            fz = fzlist.ToPagedList(pageIndex, pageSize);



            return PartialView("_FilesizemstrDetails", fz);
        }
        public ActionResult Filesizemstr()
        {
            FileSizeDetailsEntity fz = new FileSizeDetailsEntity();
            fz.listsizecode = get_size();
            return PartialView("_Filesizemstr", fz);
        }
        public List<FileSizeDetailsEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            FileSizeDetailsEntity modelobj = new FileSizeDetailsEntity();
            List<FileSizeDetailsEntity> list = new List<FileSizeDetailsEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileSizeDetailsEntity obj = new FileSizeDetailsEntity();
                    obj.fs_size_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public ActionResult filesizeDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            FileSizeDetailsEntity fsizeModelObj = new FileSizeDetailsEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_filesize_master_details_byid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@fs_request_id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                fsizeModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                fsizeModelObj.fs_size_inches = Convert.ToDecimal(dt.Rows[0]["fs_size_inches"].ToString());
                fsizeModelObj.fs_size_code = dt.Rows[0]["fs_size_code"].ToString();
                fsizeModelObj.fs_size_remarks = dt.Rows[0]["fs_size_remarks"].ToString();
                fsizeModelObj.fs_request_id = Convert.ToInt16(dt.Rows[0]["fs_request_id"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("filesizeDetails", fsizeModelObj);
        }
        [HttpPost]
        public ActionResult SavefilesizeDetails(FileSizeDetailsEntity model)
        {
            string flg = "FileSize";
            int count = objBAL.checkiduniqness(model.fs_size_code, flg);
            if (count >= 1)
            {
                TempData["msg"] = "This Code Already Exist";
            }
            else
            {
                objBAL.filesizesaveactive(model);
                #region Mail Sendin by Dhanashree 

                string subject = "";
                string body = "";

                subject = "Request No:  " + model.fs_request_type + model.fs_request_id
                    + " and " + "Production Master Update";
                body = "Request No: " + model.fs_request_type + model.fs_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",File Sub Type Master updated.Needs Approval.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
                #endregion
            }

            //custModelObj.custList = objBAL.getCustomerList();
            //custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
            //custModelObj.custNoApproveList = objBAL.getCustListNoApprove();




            return RedirectToAction("Index");
        }
        public ActionResult UpdatesizeApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdatesizeApprove(ID, type);             
                string subject = "";
                string body = "";
                subject = "Request No:  " + type + ID + " and " +
                       "Prodution Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",File Size Master Approved.";


                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return Json(new { url = @Url.Action("Index", "FileSizeMaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "FileSize";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "FileSizeMaster") }, JsonRequestBehavior.AllowGet);
           
        }
        public List<FileSizeDetailsEntity> getFileSize()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileSize";
                dt = objBAL.getMasters(flag);
                List<FileSizeDetailsEntity> listfilesize = new List<FileSizeDetailsEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new FileSizeDetailsEntity
                    {
                        fs_size_inches = item.Field<decimal>("FileSize"),
                        fs_size_code = item.Field<string>("fs_size_code"),
                        fs_size_mm = item.Field<decimal>("fs_size_mm"),
                        fs_size_remarks = item.Field<string>("fs_size_remarks"),
                        _id = item.Field<int>("_id"),
                        fs_request_id = item.Field<int>("fs_request_id"),
                        fs_request_type = item.Field<string>("fs_request_type")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Customer data  
                    IList<FileSizeDetailsEntity> Data = getFileSize();
                    var customerData = (from tempcustomer in Data
                                        select tempcustomer);
                    //Sorting  
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                    }
                    //Search  
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                    customerData = customerData.Where(m => m.fs_size_code.ToUpper().Contains(searchValue.ToUpper())
                        || m.fs_size_mm.ToString().Contains(searchValue) || m.fs_size_inches.ToString().Contains(searchValue));
                }
                    //total number of rows count   
                    recordsTotal = customerData.Count();
                    //Paging   
                    var data = customerData.Skip(skip).Take(pageSize).ToList();
                    //Returning Json Data  
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion



        #region ADD new File Size

        [HttpGet]
        public ActionResult getfilesize(FileSizeDetailsEntity reqno)
        {
            FileSizeDetailsEntity modelobj = new FileSizeDetailsEntity();
            return PartialView("_AddFileSize", modelobj);
        }

        //insert nw file size
        [HttpPost]
        public ActionResult InsertNewfilesize(FileSizeDetailsEntity Obj)
        {
            //if (ModelState.IsValid)
            //{
            //int configid = Obj.Config_Id;

            OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
            Rorobj.or_effective_dt = DateTime.Now.Date;
            Rorobj.flag = "insertOtherrequestMaters";
            Rorobj.or_request_from = "FileSize";
            Rorobj.or_request_keydata = Obj.fs_size_code;
            Rorobj.or_createby = Session["User_Id"].ToString();
            Rorobj.approveby = Session["User_Id"].ToString();
            int i = objBAL.SaveOtherRequest(Rorobj);
            int RequestID = i;

            if(RequestID !=0)
            {
                int sizemm = 25;
                FileSizeDetailsEntity fls = new FileSizeDetailsEntity();
                fls.fs_size_inches = Obj.fs_size_inches;
                fls.fs_size_mm = sizemm * fls.fs_size_inches;
                fls.fs_size_code = Obj.fs_size_code;
                fls.fs_size_remarks = Obj.fs_size_remarks;
                fls.fs_request_id = RequestID;
                fls.fs_size_approveby = Session["User_Id"].ToString();
                fls.fs_size_createby = Session["User_Id"].ToString();
                fls.flag = "insertFileSize";

                objBAL.insertfilesize(fls);

            
            }
            return RedirectToAction("Index", "FileSizeMaster");


        }
        #endregion


        public ActionResult CheckFileSizeCode(string fs_size_code)
        {
            string SqNo = objBAL.CheckFileSizeCode(fs_size_code);
            if (SqNo == "")
            {
                return Json(new { success = false, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, Message = "Please Check the SQ_No repeat not Allowd" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}