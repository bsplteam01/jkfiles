﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class CustomerApprovalController : Controller
    {
        #region Customer Approval-by Dhanashree 27.05.2018
        BAL objBAL = new BAL();

        // GET: CustomerApproval
        public ActionResult CustomerApproval(string reqno)
        {
            string req = reqno;
            Session["ReqNo"] = req;
            return View();
        }
        [HttpPost]
        public ActionResult CustomerApproval(string Production, string Dispatch)
        {
            // string Reqno = Session["requestno"].ToString();
            string Reqno = Session["ReqNo"].ToString();
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in Reqno)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }

            }
            if (Production != "")
            {
               // objBAL.UpdateCustomerApproval(letters, Convert.ToInt32(numbers), "production", Production);
            }
            if (Dispatch != "")
            {
               // objBAL.UpdateCustomerApproval(letters, Convert.ToInt32(numbers), "dispatch", Dispatch);
            }
            return View();
        }
        #endregion

   
    }
}