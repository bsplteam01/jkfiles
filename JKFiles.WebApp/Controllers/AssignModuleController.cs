﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Models;

namespace JKFiles.WebApp.Controllers
{
    public class AssignModuleController : Controller
    {
        // GET: AssignModule
        MenuMaster mm = new MenuMaster();
        BAL objBAL = new BAL();

        public ActionResult Assign_Module()
        {
            mm.listrolename = get_rolelist();
            mm.listMainmenu = get_mainmenulist();
            return View("Assign_Module", mm);
        }


        public ActionResult SubMenu(string mainmenu)
        {
            List<MenuMaster> allsubmenu = new List<MenuMaster>();
            MenuMaster menu = new MenuMaster();
            allsubmenu = get_submenulist();
            mm.listSubMainmenu = allsubmenu;

            // allsubmenu = allsubmenu.OrderBy(a => a.SubMenu_Id).ToList();


            if (!String.IsNullOrEmpty(mainmenu))
            {
                ViewBag.mainmenu = mainmenu;
                allsubmenu = allsubmenu.Where(x => Convert.ToInt32(x.MainMenuID).ToString() == mainmenu).ToList();
            }
            return PartialView("_SubMenu", allsubmenu);
        }



        public ActionResult SubList(string mainmenu)
        {
            List<MenuMaster> allsubmenu = new List<MenuMaster>();
            MenuMaster menu = new MenuMaster();
            allsubmenu = get_submenulist();
            mm.listSubMainmenu = allsubmenu;

           // allsubmenu = allsubmenu.OrderBy(a => a.SubMenu_Id).ToList();


            if (!String.IsNullOrEmpty(mainmenu))
            {
                ViewBag.mainmenu = mainmenu;
                allsubmenu = allsubmenu.Where(x => Convert.ToInt32(x.MainMenuID).ToString() == mainmenu).ToList();
            }
            return PartialView("_Submenulist", allsubmenu);
        }

        [HttpPost]
        public ActionResult SubList1(string[] ids)
        {
            if (ids != null)
            {
                TempData["Message"] = string.Join(",", ids);
                ViewBag.Message += string.Format("{0}\\n", ids[0] );
            }
            //  return RedirectToAction("Assign_Module");
            return PartialView("_Submenulist", ViewBag.Message);


            //ViewBag.Message = "\\n";
            //foreach (SelectListItem item in items)
            //{
            //    if (item.Selected)
            //    {
            //        ViewBag.Message += string.Format("{0}\\n", item.Text + " --- " + item.Value);
            //        //  ViewBag.Message += string.Format("{0}\\n", item.Value);
            //    }
            //}
            //return View(items);
        }

        [HttpPost]
        public ActionResult SubList(int[] ids, MenuMaster mm)
        {
            List<MenuMaster> allsubmenu = new List<MenuMaster>();
            MenuMasterEntity menu = new MenuMasterEntity();
            allsubmenu = get_submenulist();
            mm.listSubMainmenu = allsubmenu;
            allsubmenu = allsubmenu.Where(x => Convert.ToBoolean(x.MainMenuID)).ToList();


            foreach (object o in ids)
            {
                int subid = (int)o;
                menu.RoleName = mm.RoleName;
                menu.SubMenu_Id = subid;
               // menu.MainMenuID = mm.MainMenuID;

                menu.flag = "insertpageid";

               // objBAL.savepageDetails(menu);

            }

           
            //if (ids != null)
            //{
            //    ViewBag.Message = "You have selected following Customer ID(s):" + string.Join(", ", ids);
            //}
            //else
            //{
            //    ViewBag.Message = "No record selected";
            //}

            return RedirectToAction("Assign_Module");
            //  return PartialView("_Submenulist", allsubmenu);
            //  return View("Assign_Module", allsubmenu);
            // return PartialView(allsubmenu);
        }


        //[HttpPost]
        //public ActionResult Save(List<MenuMaster> hobbies)
        //{
        //    MenuMaster entities = new MenuMaster();
        //    foreach (MenuMaster hobby in hobbies)
        //    {
        //        MenuMaster updatedHobby = entities.MenuMaster.ToList().Find(p => p.HobbyId == hobby.HobbyId);
        //        updatedHobby.IsSelected = hobby.IsSelected;
        //    }

        //    entities.SaveChanges();

        //    return RedirectToAction("Index");
        //}









        //public ActionResult Submenulist()
        //{


        //    List<MenuMaster> allsubmenu = new List<MenuMaster>();
        //    MenuMaster menu = new MenuMaster();
        //    allsubmenu = get_submenulist();
        //    mm.listSubMainmenu = allsubmenu;

        //    allsubmenu = allsubmenu.OrderBy(a => a.SubMenu_Id).ToList();

        //    //using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    //{
        //    //    allCust = dc.CustomerInfoes.OrderBy(a => a.CustomerID).ToList();
        //    //}
        //    return PartialView("_Submenulist", allsubmenu);
        //}

        public List<MenuMaster> get_submenulist()
        {
            string flag = "getsubmenuelist";
            DataTable dt = objBAL.getrolelist(flag);
            MenuMaster modelobj = new MenuMaster();
            List<MenuMaster> list = new List<MenuMaster>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MenuMaster obj = new MenuMaster();
                    obj.SubMenu_Id = Convert.ToInt32(dt.Rows[i]["SubMenu_Id"].ToString());
                    obj.SubMenu = (dt.Rows[i]["SubMenu"].ToString());
                    obj.Controller = (dt.Rows[i]["Controller"].ToString());
                    obj.Action = (dt.Rows[i]["Action"].ToString());
                    obj.MainMenuID = Convert.ToInt32(dt.Rows[i]["MainMenuID"].ToString());

                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        [HttpPost]
        public ActionResult Submenulist(string[] ids)
        {
            List<MenuMaster> allsubmenu = new List<MenuMaster>();
            MenuMaster menu = new MenuMaster();
            allsubmenu = get_submenulist();
            mm.listSubMainmenu = allsubmenu;

            allsubmenu = allsubmenu.OrderBy(a => a.SubMenu_Id).ToList();

            // In the real application you can ids 

            if (ids != null)
            {
                ViewBag.Message = "You have selected following Customer ID(s):" + string.Join(", ", ids);
            }
            else
            {
                ViewBag.Message = "No record selected";
            }

            return PartialView(allsubmenu);
        }
        public List<MenuMaster> get_rolelist()
        {
            string flag = "getrolenamelist";
            DataTable dt = objBAL.getrolelist(flag);
            MenuMaster modelobj = new MenuMaster();
            List<MenuMaster> list = new List<MenuMaster>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MenuMaster obj = new MenuMaster();
                    obj.RoleName = (dt.Rows[i]["RoleName"].ToString());
                    obj.RoleId = (dt.Rows[i]["Role_Id"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<MenuMaster> get_mainmenulist()
        {
            string flag = "getmainmenulist";
            DataTable dt = objBAL.getrolelist(flag);
            MenuMaster modelobj = new MenuMaster();
            List<MenuMaster> list = new List<MenuMaster>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MenuMaster obj = new MenuMaster();
                    obj.MainMenuID = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    obj.MainMenu = (dt.Rows[i]["MainMenu"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }



    }
}