﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;
namespace JKFiles.WebApp.Controllers
{
    public class RequestFromUserController : Controller
    {
        BAL objBAL = new BAL();
        // GET: RequestFromUser
        public ActionResult RequestFromUserIndex()
        {
            ComplaintmgntModel modelobj = new ComplaintmgntModel();
            modelobj.listrequest = get_request();
            var v = modelobj.listrequest;
            return View("RequestFromUserIndex", v.ToList());
        }

        public List<ComplaintmgntModel> get_request()
        {
            string flag = "getrequestAdmin";
            DataTable dt = objBAL.getrequestlist(flag);
            ComplaintmgntModel modelobj = new ComplaintmgntModel();
            List<ComplaintmgntModel> list = new List<ComplaintmgntModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ComplaintmgntModel objRole = new ComplaintmgntModel();
                    objRole.Request_Id = Convert.ToInt32(dt.Rows[i]["Request_Id"].ToString());
                    objRole.OEMId = dt.Rows[i]["OEMId"].ToString();
                    objRole.OEMName = dt.Rows[i]["OEMName"].ToString();
                    objRole.RequestType = dt.Rows[i]["RequestType"].ToString();
                    objRole.RequestDesc = dt.Rows[i]["RequestDesc"].ToString();
                    objRole.RequestStatus = dt.Rows[i]["RequestStatus"].ToString();
                    objRole.RequestDate = dt.Rows[i]["RequestDate"].ToString();
                    objRole.ModifiedDate = dt.Rows[i]["ModifiedDate"].ToString();
                    objRole.RequestBetween = dt.Rows[i]["RequestBetween"].ToString();
                    list.Add(objRole);
                }
            }
            return list.ToList();
        }


        [HttpGet]
        public ActionResult RequestStatus(string reqno)
        {
            ComplaintmgntModel req = new ComplaintmgntModel();
            req.Request_Id =Convert.ToInt32(reqno);
            // return Json(custId, JsonRequestBehavior.AllowGet);
            return PartialView("_Requeststatus", req);

        }


        //update admin setting
        [HttpPost]
        public ActionResult SaveRequest(ComplaintmgntModel Obj)
        {
            //if (ModelState.IsValid)
            //{
            int reqid = Obj.Request_Id;

            RequestMgmtEntity req = new RequestMgmtEntity();
            req.RequestStatus = Obj.RequestStatus;
            req.Comments = Obj.Comments;
            req.Request_Id = reqid;

            req.flag = "updaterequstmgnt";

            objBAL.updaterequstmgnt(req);

            req.flag = "insertrequestdetails";

            objBAL.saverequesttypeDetails(req);


            return RedirectToAction("RequestFromUserIndex", "RequestFromUser");
            //}
            //return RedirectToAction("UserManagementIndex", "UserManagement");
        }
    }
}