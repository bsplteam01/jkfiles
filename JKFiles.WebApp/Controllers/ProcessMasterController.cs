﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using JKFiles.WebApp.Models;
using JKFiles.WebApp.ActionFilters;
using System.Linq.Dynamic;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class ProcessMasterController : Controller
    {
        BAL objBAL = new BAL();
        // GET: ProcessMaster
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProcessIndex()
        {
            return View();
        }

        public ActionResult Opreationmasterlist()
        {
            OperationMasterEntity PM = new OperationMasterEntity();
            PM.oprationmasterlist = getoprationMaster();

            return PartialView("_ProcessOperationlist", PM);
        }

        public ActionResult FileTypewiseOperations()
        {
            ProcessmasterModel obj = new ProcessmasterModel();
            obj.processfilelist = objBAL.getnewdispatchprocess();
            obj.Approveprocessfilelist = objBAL.getApprovedispatchprocess();
            obj.SKUlist = get_SKU();
            obj.filetTypelist = getFileType();
            return PartialView("_FileTypewiseOperations", obj);
        }

        public List<ItemmasterModel> get_SKU()
        {
            string flag = "sku_list";
            DataTable dt = objBAL.Itemmasterfilter(flag);
            ItemmasterModel modelobj = new ItemmasterModel();
            List<ItemmasterModel> list = new List<ItemmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemmasterModel obj = new ItemmasterModel();
                    obj.SKU = (dt.Rows[i]["SKU"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public JsonResult getDispatchoprationList(string requestId, string requesttype)
        {
            var reqest = requestId.Substring(1);
            ProcessmasterModel obj = new ProcessmasterModel();
            obj.processaprovelist = objBAL.DispatchoprationList(reqest, requesttype);
            return Json(obj.processaprovelist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getFileTypeUnitList(string FileTypeCode)
        {

            ProcessmasterModel obj = new ProcessmasterModel();
            obj.processaprovelist = objBAL.getFileTypeUnitList(FileTypeCode);
            return Json(obj.processaprovelist, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ProcessMasterlist(string SKU, string FileType)
        //{
        //    ProcessmasterModel obj = new ProcessmasterModel();
        //    obj.Processmasterlist = getProcessMasterlist();
        //    var prolist = obj.Processmasterlist;
        //    if (!String.IsNullOrEmpty(SKU))
        //    {
        //        prolist = prolist.Where(x => x.pn_part_no == SKU).ToList();
        //    }

        //    if (!String.IsNullOrEmpty(FileType))
        //    {
        //        prolist = prolist.Where(x => x.pn_ftype_desc == FileType).ToList();
        //    }
        //    return Json(new { success = true, prolist = prolist }, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult ProcessMasterlist()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                IList<ProcessmasterModel> Data = getProcessMasterlist();
                var customerData = (from tempcustomer in Data
                                    select tempcustomer);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.pn_part_no.ToUpper().Contains(searchValue.ToUpper())
                        || m.pn_ftype_desc.ToUpper().Contains(searchValue.ToUpper()) || m.pn_ftype_code.ToUpper().Contains(searchValue.ToUpper()) || m.ft_process_code.ToUpper().Contains(searchValue.ToUpper()));
                }
                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProcessmasterModel> getProcessMasterlist()
        {
            string flag = "ProcessMasterlist";

            DataTable dt = objBAL.ProcessMasterlist(flag);
            ProcessmasterModel modelobj = new ProcessmasterModel();
            List<ProcessmasterModel> list = new List<ProcessmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    ProcessmasterModel objProc = new ProcessmasterModel();
                    objProc.pn_part_no = dt.Rows[i]["pn_part_no"].ToString();
                    objProc.pn_ftype_code = dt.Rows[i]["pn_ftype_code"].ToString();
                    objProc.pn_ftype_desc = dt.Rows[i]["pn_ftype_desc"].ToString();
                    objProc.ft_process_code = dt.Rows[i]["ft_process_code"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }
        [HttpGet]
        public JsonResult getUnitEquipment(string uni_unit_code)
        {

            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.unitequipmentlist = getUnitEquipmentlist(uni_unit_code);

            return Json(obj.unitequipmentlist, JsonRequestBehavior.AllowGet);
        }

        public List<EquipmentMasterEntity> getUnitEquipmentlist(string uni_unit_code)
        {
            DataTable dt = new DataTable();
            string flag = "unitequipment";
            dt = objBAL.getUnitEquipmentlist(flag, uni_unit_code);
            List<EquipmentMasterEntity> unitEQlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                unitEQlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    equ_name = item.Field<string>("equ_name"),
                    // equ_type = item.Field<string>("equ_type")

                }).ToList();
            }
            return unitEQlist;
        }
        [HttpGet]
        public JsonResult getUnitEquipmentType(string uni_unit_code, string equ_name)
        {
            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.unitequipmenttypelist = getunitequipmenttypelist(uni_unit_code, equ_name);

            return Json(obj.unitequipmenttypelist, JsonRequestBehavior.AllowGet);
        }

        public List<EquipmentMasterEntity> getunitequipmenttypelist(string uni_unit_code, string equ_name)
        {
            DataTable dt = new DataTable();
            string flag = "unitequipmenttype";
            dt = objBAL.getunitequipmenttypelist(flag, uni_unit_code, equ_name);
            List<EquipmentMasterEntity> unitEQlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                unitEQlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    equ_type = item.Field<string>("equ_type")

                }).ToList();
            }
            return unitEQlist;
        }

        [HttpGet]
        public JsonResult getUnitEquipmentAsset(string uni_unit_code, string equ_name, string equ_type)
        {

            EquipmentMasterEntity obj = new EquipmentMasterEntity();
            obj.EquipmentAssetlist = getEquipmentAssetlist(uni_unit_code, equ_name, equ_type);

            return Json(obj.EquipmentAssetlist, JsonRequestBehavior.AllowGet);
        }

        public List<EquipmentMasterEntity> getEquipmentAssetlist(string uni_unit_code, string equ_name, string equ_type)
        {
            DataTable dt = new DataTable();
            string flag = "processunitequipmentTypeAsset";
            dt = objBAL.getUnitEquipmentAssetlist(flag, uni_unit_code, equ_name, equ_type);
            List<EquipmentMasterEntity> Assetlist = new List<EquipmentMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                Assetlist = dt.AsEnumerable().Select(item => new EquipmentMasterEntity
                {
                    equ_assetno = item.Field<Int32>("equ_assetno")

                }).ToList();
            }
            return Assetlist;
        }

        public List<OperationMasterEntity> getoprationMaster()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getOpretionmasterlist";
                dt = objBAL.getmasterList(flag);
                List<OperationMasterEntity> listopretionlist = new List<OperationMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listopretionlist = dt.AsEnumerable().Select(item => new OperationMasterEntity
                    {
                        opr_operation_seq = item.Field<Int16>("opr_operation_seq"),
                        opr_operation_name = item.Field<string>("opr_operation_name"),
                        opr_operation_restricted = item.Field<string>("opr_operation_restricted"),

                    }).ToList();
                }
                return listopretionlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult AddProcessAssetAllocation(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                DispatchAssetAllocationEntity daobj = new DispatchAssetAllocationEntity();
                string Asset = form["Asset[]"].ToString();
                string[] assetno = Asset.Split(',');
                var requestid = 0;
                for (int i = 0; i < assetno.Length; i++)
                {
                    daobj.daa_createby = Session["User_Id"].ToString();
                    daobj.daa_approveby = Session["User_Id"].ToString();
                    daobj.daa_unit_code = form["uni_unit_code"];
                    daobj.daa_main_valstream = form["val_valuestream_code"];
                    daobj.daa_request_type = form["daa_request_type"];
                    var daa_request_id = form["daa_request_id"].Substring(1);
                    daobj.daa_request_id = Convert.ToInt32(daa_request_id);
                    daobj.daa_operseq_no = Convert.ToInt32(form["daa_operseq_no"]);
                    daobj.daa_assetno = Convert.ToInt32(assetno[i].ToString());
                    daobj.flag = "ProcessAssetallcation";
                    objBAL.saveProcessAssetAllocation(daobj);
                    requestid = Convert.ToInt32(form["daa_request_id"].Substring(1));
                }
                if (requestid > 0)
                {
                    daobj.selectedprocesslist = selectedprocesslist(requestid);
                }

                return Json(new { success = true, prolist = daobj.selectedprocesslist, message = "Record insert succesfully." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }


        public List<DispatchAssetAllocationEntity> selectedprocesslist(int requestid)
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getSelectedprocesslist";
                dt = objBAL.getSelectedprocesslist(flag, requestid);
                List<DispatchAssetAllocationEntity> listopretionlist = new List<DispatchAssetAllocationEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listopretionlist = dt.AsEnumerable().Select(item => new DispatchAssetAllocationEntity
                    {
                        daa_assetno = item.Field<int>("daa_assetno"),
                        daa_unit_name = item.Field<string>("daa_unit_name"),
                        daa_operseq_name = item.Field<string>("daa_operseq_name"),
                        equ_name = item.Field<string>("equ_name"),
                        equ_type = item.Field<string>("equ_type"),
                    }).ToList();
                }
                return listopretionlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CloseProcessAllocation(string daa_request_id, string daa_request_type)
        {
            ProcessMasterDetailsEntity opobj = new ProcessMasterDetailsEntity();
            opobj.ft_request_type = daa_request_type;
            var daa_request_id1 = daa_request_id.Substring(1);
            opobj.ft_request_id = Convert.ToInt32(daa_request_id1);
            opobj.flag = "CloseProcessAllocation";
            objBAL.ColseProcessDetails(opobj);


            #region Mail Sendin by Dhanashree 

            string subject = "";
            string body = "";

            subject = "Request No:  " + daa_request_type + daa_request_id
                + " and " + "Production Master Update";
            body = "Request No: " + daa_request_type + daa_request_id +
                " By User ID " + Session["UserId"].ToString()
                + ",Process Master updated.Needs Approval.";
            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }
            #endregion
            return Json(new { success = true, message = "Close succesfully." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProcessApprove(string process_code, string requestID, string requesttype)
        {
            if (ModelState.IsValid)
            {
                OperationMasterEntity opobj = new OperationMasterEntity();
                opobj.ma_createby = Session["User_Id"].ToString();
                opobj.dpm_process_code = process_code;
                opobj.dpm_request_type = requesttype;
                var daa_request_id1 = requestID.Substring(1);
                opobj.dpm_request_id = Convert.ToInt32(daa_request_id1);
                opobj.flag = "ApproveProcessStatus";
                objBAL.ApproveProcessStatus(opobj);

                string subject = "";
                string body = "";

                subject = "Request No:  " + requesttype + requestID + " and " +
                       "Production Master Approval";
                body = "Request No: " + requesttype + requestID + " By User ID " + Session["UserId"].ToString()
                    + ",Process Master Approved.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }


                return Json(new { success = true, message = "Process Approve succesfully." }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("ProcessIndex");
        }

        [HttpPost]
        public ActionResult ProcessRejected(string ma_remarks, string requestID, string requesttype)
        {
            if (ModelState.IsValid)
            {
                OperationMasterEntity opobj = new OperationMasterEntity();

                opobj.ma_createby = Session["User_Id"].ToString();
                opobj.dpm_request_type = requesttype;
                opobj.ma_remarks = ma_remarks;
                var daa_request_id1 = requestID.Substring(1);
                opobj.dpm_request_id = Convert.ToInt32(daa_request_id1);
                opobj.flag = "RejectProcessStatus";
                objBAL.RejectProcessStatus(opobj);

                string subject = "";
                string body = "";
                subject = "Request No:  " + requesttype + requestID + " and " +
                      "Production Master Reject";
                body = "Request No: " + requesttype+ requestID + " By User ID " + Session["UserId"].ToString()
                    + ",Drawing Master Rejected.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }

                return Json(new { success = true, message = "Process Reject succesfully." }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("ProcessIndex");
        }


        public ActionResult ShowGridprocess(string partno)
        {
            List<ProcessmasterModel> processlist = new List<ProcessmasterModel>();

            processlist = GetShowGridprocess(partno);
            return Json(new { success = true, processlist = processlist }, JsonRequestBehavior.AllowGet);

        }

        public List<ProcessmasterModel> GetShowGridprocess(string partno)
        {
            string flag = "processgrid3list";

            DataTable dt = objBAL.GetShowGridprocess(flag, partno);
            ProcessmasterModel modelobj = new ProcessmasterModel();
            List<ProcessmasterModel> list = new List<ProcessmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ProcessmasterModel objProc = new ProcessmasterModel();
                    objProc.ppm_process_code = dt.Rows[i]["ppm_process_code"].ToString();
                    objProc.ppm_process_seqno = Convert.ToInt32(dt.Rows[i]["ppm_process_seqno"].ToString());
                    objProc.ProcessName = dt.Rows[i]["opr_operation_name"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }



        public List<EquipmentMasterModel> getEquipmentList()
        {
            string flag = "equipmentList";
            DataTable dt = objBAL.getequipmentlist(flag);
            EquipmentMasterModel modelobj = new EquipmentMasterModel();
            List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    EquipmentMasterModel objeqi = new EquipmentMasterModel();
                    objeqi.aa_id = Convert.ToInt32(dt.Rows[i]["aa_id"].ToString());
                    objeqi.equ_name = dt.Rows[i]["equ_name"].ToString();
                    objeqi.equ_type = dt.Rows[i]["equ_type"].ToString();
                    objeqi.aa_assetno = Convert.ToInt32(dt.Rows[i]["aa_assetno"].ToString());
                    objeqi.stringaa_startdate = dt.Rows[i]["aa_startdate"].ToString();
                    objeqi.stringaa_enddate = dt.Rows[i]["aa_enddate"].ToString();
                    objeqi.uni_unit_name = dt.Rows[i]["uni_unit_name"].ToString();
                    objeqi.uni_unit_type = dt.Rows[i]["uni_unit_type"].ToString();

                    list.Add(objeqi);
                }
            }
            return list.ToList();
        }
        public ActionResult CheckOprationSqNo(string opr_operation_seq)
        {
            string SqNo = objBAL.CheckOprationSqNo(opr_operation_seq);
            if (SqNo == "")
            {
                return Json(new { success = false, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, Message = "Please Check the SQ_No repeat not Allowd" }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<FileTypeDataEntity> getFileType()
        {
            DataTable dt = new DataTable();
            List<FileTypeDataEntity> fileTypelist = new List<FileTypeDataEntity>();
            try
            {
                string flag = "getFileType";
                dt = objBAL.getMasters(flag);
                if (dt.Rows.Count > 0 && dt != null)
                {
                    fileTypelist = dt.AsEnumerable().Select(item => new FileTypeDataEntity
                    {
                        ft_ftype_desc = item.Field<string>("FileType"),
                        ft_ftype_code = item.Field<string>("ft_ftype_code")
                    }).ToList();
                }
                return fileTypelist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindProcess()
        {
            DataTable dt = new DataTable();
            string flag = "getOpretionmasterlist";
            dt = objBAL.getmasterList(flag);

            List<OperationMasterEntity> listopretionlist = new List<OperationMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listopretionlist = dt.AsEnumerable().Select(item => new OperationMasterEntity
                    {
                        opr_operation_seq = item.Field<Int16>("opr_operation_seq"),
                        opr_operation_name = item.Field<string>("opr_operation_name"),
                        opr_operation_restricted = item.Field<string>("opr_operation_restricted"),

                    }).ToList();
                }
            ViewData["Process"] = listopretionlist.ToList();
        }


        [HttpPost]
        public ActionResult Addopreation(OprationMasterModel objmodel)
        {
            if (ModelState.IsValid)
            {
                OperationMasterEntity opobj = new OperationMasterEntity();
                opobj.opr_operation_seq = objmodel.opr_operation_seq;
                opobj.opr_operation_name = objmodel.opr_operation_name;
                opobj.opr_operation_restricted = objmodel.opr_operation_restricted;
                opobj.flag = "insertopration";
                objBAL.saveoprationDetails(opobj);
                return RedirectToAction("ProcessIndex");
            }
            return RedirectToAction("ProcessIndex");
        }

        [HttpPost]
        public ActionResult AddProcess(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                ProcessMasterDetailsEntity opobj = new ProcessMasterDetailsEntity();
                string processes = form["Process[]"].ToString();
                string[] process = processes.Split(',');
                for (int i = 0; i < process.Length; i++)
                {
                    opobj.ppm_createby = Session["User_Id"].ToString();
                    opobj.ppm_process_code = form["ppm_process_code"];
                    opobj.ppm_request_type = form["ppm_request_type"];
                    opobj.ppm_request_id = Convert.ToInt32(form["ppm_request_id"]);
                    opobj.ppm_ftype_code = form["ppm_ftype_code"];
                    opobj.ppm_fstype_code = form["ppm_fstype_code"];
                    opobj.ppm_process_seqno = Convert.ToInt32(process[i].ToString());
                    opobj.flag = "insertProcess";
                    objBAL.saveprocessDetails(opobj);
                }

                return Json(new { success = true, message = "Record insert succesfully." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Showaddprocess(string process_code, string requestID, string requesttype)
        {
            List<ProcessmasterModel> processlist = new List<ProcessmasterModel>();
            var requestID1 = requestID.Substring(1);
            processlist = Getprocesstypewiselist(process_code, requestID1, requesttype);
            return Json(new { success = true, processlist = processlist }, JsonRequestBehavior.AllowGet);

        }

        public List<ProcessmasterModel> Getprocesstypewiselist(string process_code, string requestID, string requesttype)
        {
            string flag = "processlist";
            int reqno = Convert.ToInt32(requestID);
            DataTable dt = objBAL.processtypewiselist(flag, process_code, reqno, requesttype);
            ProcessmasterModel modelobj = new ProcessmasterModel();
            List<ProcessmasterModel> list = new List<ProcessmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ProcessmasterModel objProc = new ProcessmasterModel();
                    objProc.ppm_process_code = dt.Rows[i]["ppm_process_code"].ToString();
                    objProc.ppm_process_seqno = Convert.ToInt32(dt.Rows[i]["ppm_process_seqno"].ToString());
                    objProc.ProcessName = dt.Rows[i]["opr_operation_name"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }

        public ActionResult ShowAddApproveProcess(string process_code, string requestID, string requesttype)
        {
            List<ProcessmasterModel> processlist = new List<ProcessmasterModel>();

            processlist = GetAddApproveProcess(process_code);
            return Json(new { success = true, processlist = processlist }, JsonRequestBehavior.AllowGet);
        }

        public List<ProcessmasterModel> GetAddApproveProcess(string process_code)
        {
            string flag = "ApproveProcesslist";

            DataTable dt = objBAL.GetAddApproveProcess(flag, process_code);
            ProcessmasterModel modelobj = new ProcessmasterModel();
            List<ProcessmasterModel> list = new List<ProcessmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ProcessmasterModel objProc = new ProcessmasterModel();
                    objProc.ppm_process_code = dt.Rows[i]["ppm_process_code"].ToString();
                    objProc.ppm_process_seqno = Convert.ToInt32(dt.Rows[i]["ppm_process_seqno"].ToString());
                    objProc.ProcessName = dt.Rows[i]["ProcessName"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }


        public ActionResult ChangeProcessApproveStatus(string ppm_request_id, string ppm_request_type, string ppm_remarks)
        {
            if (ModelState.IsValid)
            {
                ProcessMasterDetailsEntity opobj = new ProcessMasterDetailsEntity();
                opobj.ppm_request_id = Convert.ToInt32(ppm_request_id);
                opobj.ppm_request_type = ppm_request_type;
                opobj.ppm_isApproved = "Y";
                opobj.ppm_isActive = true;
                opobj.ppm_remarks = ppm_remarks;
                opobj.ppm_approveby = Session["User_Id"].ToString();
                opobj.flag = "changeprocessStatus";
                objBAL.ChangeProcessStatus(opobj);
                return Json(new { url = @Url.Action("ProcessIndex", "ProcessMaster") }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { url = @Url.Action("ProcessIndex", "ProcessMaster") }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ChangeProcessRejectStatus(string ppm_request_id, string ppm_request_type, string ppm_remarks)
        {
            if (ModelState.IsValid)
            {
                ProcessMasterDetailsEntity opobj = new ProcessMasterDetailsEntity();
                opobj.ppm_request_id = Convert.ToInt32(ppm_request_id);
                opobj.ppm_request_type = ppm_request_type;
                opobj.ppm_isApproved = "R";
                opobj.ppm_isActive = false;
                opobj.ppm_remarks = ppm_remarks;
                opobj.ppm_approveby = Session["User_Id"].ToString();
                opobj.flag = "changeprocessStatus";
                objBAL.ChangeProcessStatus(opobj);
                return Json(new { url = @Url.Action("ProcessIndex", "ProcessMaster") }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { url = @Url.Action("ProcessIndex", "ProcessMaster") }, JsonRequestBehavior.AllowGet);
        }


    }
}