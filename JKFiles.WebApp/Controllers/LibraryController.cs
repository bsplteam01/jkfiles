﻿using JKFiles.WebApp.Models;
using JKFilesBAL;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKFiles.WebApp.Controllers
{
    public class LibraryController : Controller
    {
        BAL objBAL = new BAL();
        // GET: Library
        public ActionResult Stamp_Library()
        {
            return View();
        }


        protected List<DrawingLibraryModel> getDrawing()
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            objfile.flag = "drawing_library";
            DataTable dt = objBAL.get_DrawingValues(objfile);
            List<DrawingLibraryModel> listdrawing = new List<DrawingLibraryModel>();
            if(dt.Rows.Count>0 && dt!=null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DrawingLibraryModel objdraw = new DrawingLibraryModel();
                    objdraw.dwg_prod_sku = dt.Rows[i]["dwg_prod_sku"].ToString();
                    objdraw.dwg_dxfname = dt.Rows[i]["dwg_dxfname"].ToString();
                    objdraw.dwg_pdfname = dt.Rows[i]["dwg_pdfname"].ToString();
                    listdrawing.Add(objdraw);
                }
            }
            return listdrawing;
        }

        public ActionResult Drawing_Library()
        {

            DrawingLibraryModel objDrawingModel = new DrawingLibraryModel();
            objDrawingModel.listdrawing = getDrawing();
            return View(objDrawingModel);
        }

        public ActionResult Label_Library()
        {
            LabelLibraryModel objlistlibrary = new LabelLibraryModel();
            objlistlibrary.label_list = getLabelLibrary();
            return View(objlistlibrary);
        }

        protected List<LabelLibraryModel> getLabelLibrary()
        {
            FileSpec objfile = new FileSpec();
            BAL objBAL = new BAL();
            DataTable dt = objBAL.get_LabelLibrary();
            List<LabelLibraryModel> listlibrary = new List<LabelLibraryModel>();
            listlibrary = dt.AsEnumerable().Select(item => new LabelLibraryModel()
            {
                pn_part_no= item.Field<string>("partno"),
                ReqNo= item.Field<string>("ReqNo"),
                pn_request_id= item.Field<int>("pn_request_id")
            }).ToList();
            return listlibrary;
        }


        [HttpGet]
        public ActionResult LabelLayOutDetails(string ReqNo)
        {
            InnerLabelDetailsEntity detailsEntity = new InnerLabelDetailsEntity();
            InnerLabelDetailsModel detailsModel = new InnerLabelDetailsModel();
            DispatchSKU disPackObj = new DispatchSKU();
            InnerLabelDetailsModel detailObj = new InnerLabelDetailsModel();
            //string ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString();
            string id = ReqNo.Substring(1, ReqNo.Length - 1);
            int ReqId = Convert.ToInt32(id);
            string flag = "packing_details";
            disPackObj.modellist = getEditPackingDetails(ReqNo, flag);
            Session["PackingDetailsTab"] = disPackObj.modellist;

            List<DispatchSKU> sessionObj1 = (List<DispatchSKU>)Session["PackingDetailsTab"];
            string fileTypeCode = "";
            string FileSubType = "";
            string fileSize = "";
            string brandId = "";
            string custId = "";
            string country = "";
            string fileCutType = "";
            



            #region All Images Added by Dhanashree 
            DataTable dt = new DataTable();
            if (ReqId != 0)
            {
                dt = objBAL.getdispatchdetails(ReqId);
            }
            if (dt.Rows.Count > 0)
            {
                disPackObj.CustomerName = dt.Rows[0]["pr_cust_name"].ToString();
                Session["CoustomerId"] = dt.Rows[0]["pr_cust_id"].ToString();
                disPackObj.BrandName = dt.Rows[0]["pr_brand_desc"].ToString();
                Session["BrandId"] = dt.Rows[0]["pr_brand_id"].ToString();
                Session["FileTypeCode"]= dt.Rows[0]["pr_ftype_code"].ToString();
                Session["FileSubTypeCode"]= dt.Rows[0]["pr_fstype_code"].ToString();
                Session["FileSizeCode"]= dt.Rows[0]["pr_fsize_code"].ToString();
                Session["ShipToCountryCode"]= dt.Rows[0]["pr_ship2country_id"].ToString();
                Session["FileCutTypeCode"]= dt.Rows[0]["pr_fcut_code"].ToString();
                disPackObj.dp_handle_chart = dt.Rows[0]["dp_handle_chart"].ToString();
                disPackObj.dp_wrapping_chart = dt.Rows[0]["dp_wrapping_chart"].ToString();
                disPackObj.dp_innerbox_chart = dt.Rows[0]["dp_innerbox_chart"].ToString();
                disPackObj.dp_outerbox_chart = dt.Rows[0]["dp_outerbox_chart"].ToString();
                disPackObj.dp_pallet_chart = dt.Rows[0]["dp_pallet_chart"].ToString();
                if (disPackObj.dp_handle_chart != "" || disPackObj.dp_handle_chart != null)
                {
                    disPackObj.HandleDetailsEntityList = objBAL.gethandleimgpath(disPackObj.dp_handle_chart);
                }
                if (disPackObj.dp_wrapping_chart != "" || disPackObj.dp_wrapping_chart != null)
                {
                    disPackObj.WrapperdetailsList = objBAL.getwrappinglist(disPackObj.dp_wrapping_chart);
                }
                if (disPackObj.dp_innerbox_chart != "" || disPackObj.dp_innerbox_chart != null)
                {
                    disPackObj.innerboxlist = objBAL.getinnerboxlist(disPackObj.dp_innerbox_chart, FileSubType, fileTypeCode, fileSize);
                }
                if (disPackObj.dp_outerbox_chart != "" || disPackObj.dp_outerbox_chart != null)
                {
                    disPackObj.OterChartList = objBAL.GetOuterChartlist(disPackObj.dp_outerbox_chart);
                }
                if (disPackObj.dp_pallet_chart != "" || disPackObj.dp_pallet_chart != null)
                {
                    disPackObj.PalletList = objBAL.GetPalletList(disPackObj.dp_pallet_chart);
                }
            }
            if (Session["FileTypeCode"] != null && Session["FileSubTypeCode"] != null && Session["FileSizeCode"] != null
                && Session["BrandId"] != null && Session["CoustomerId"] != null && Session["ShipToCountryCode"] != null
                && Session["FileCutTypeCode"] != null)
            {
                fileTypeCode = Session["FileTypeCode"].ToString();
                FileSubType = Session["FileSubTypeCode"].ToString();
                fileSize = Session["FileSizeCode"].ToString();
                brandId = Session["BrandId"].ToString();
                custId = Session["CoustomerId"].ToString();
                country = Session["ShipToCountryCode"].ToString();
                fileCutType = Session["FileCutTypeCode"].ToString();
            }

            detailsEntity.ilbd_chart_num = objBAL.getChartNumForInnerLabel(fileTypeCode, FileSubType, fileSize, brandId, custId, country, fileCutType);
            Session["InnerLabelChartNum"] = detailsEntity.ilbd_chart_num;
            detailsEntity.InnerLabelDetailsEntityList = objBAL.getInnerLabelDetails(detailsEntity.ilbd_chart_num);



            detailObj.innerLabelDetailsEntoityList = detailsEntity.InnerLabelDetailsEntityList;
            disPackObj.innerLabelDetilasList = detailObj.innerLabelDetailsEntoityList;

            disPackObj.temp_barcode_flg = sessionObj1[0].dp_barcode_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_fumigation_flg = sessionObj1[0].dp_fumigation_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_hologram_flg = sessionObj1[0].dp_hologram_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_india_flg = sessionObj1[0].dp_india_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_lineno_flg = sessionObj1[0].dp_lineno_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_polybag_flg = sessionObj1[0].dp_polybag_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_prcstkr_flg = sessionObj1[0].dp_prcstkr_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_qltyseal_flg = sessionObj1[0].dp_qltyseal_flg.ToString() == "True" ? "Yes" : "No";
            disPackObj.temp_silicagel_flg = sessionObj1[0].dp_silicagel_flg.ToString() == "True" ? "Yes" : "No";


            #endregion


            return PartialView("_LabelLayOutDetails", disPackObj);
        }

        #region Edit code Packing Details -by Dhanashree
        public List<DispatchSKU> getEditPackingDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            ds = objBAL.getEditFileSpecDetails(ReqNo, flag);
            List<DispatchSKU> modellist = new List<DispatchSKU>();
            if (ds.Tables[0] != null)
            {
                modellist = ds.Tables[0].AsEnumerable().Select(item => new DispatchSKU()
                {
                    dp_barcode_flg = item.Field<Boolean?>("dp_barcode_flg"),
                    dp_fumigation_flg = item.Field<Boolean?>("dp_fumigation_flg"),
                    dp_hologram_flg = item.Field<Boolean?>("dp_hologram_flg"),
                    dp_india_flg = item.Field<Boolean?>("dp_india_flg"),
                    dp_lineno_flg = item.Field<Boolean?>("dp_lineno_flg"),
                    dp_polybag_flg = item.Field<Boolean?>("dp_polybag_flg"),
                    dp_prcstkr_flg = item.Field<Boolean?>("dp_prcstkr_flg"),
                    dp_qltyseal_flg = item.Field<Boolean?>("dp_qltyseal_flg"),
                    dp_silicagel_flg = item.Field<Boolean?>("dp_silicagel_flg"),
                }).ToList();
            }
            return modellist;
        }
        #endregion
    }
}