﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;
using JKFIlesEntity;
using JKFiles.WebApp.Entity;
using System.IO;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    public class CustomerDetailsController : Controller
    {
        BAL objBAL = new BAL();
        jkfiles_dbEntities db = new jkfiles_dbEntities();


        #region Customer Details Index Methods by-Archana Mahajan
        // GET: CustomerDetails
        //Customer Details : Index Methods by-Archana Mahajan
        public ActionResult Index()
        {
            CustomerMasterModel custModelObj = new CustomerMasterModel();
            custModelObj.custList = objBAL.getCustomerList();
            custModelObj.brandList = objBAL.getBrandList();
            custModelObj.countryList = objBAL.getShipTOCountryList();
            return View(custModelObj);
        }

        [HttpPost]
        public ActionResult Index(CustomerMasterModel custObj)
        {
            //if (ModelState.IsValid)
            //{
                CustomerDetailsEntity custEntityObj = new CustomerDetailsEntity();
                Session["CustomerDetailsTab"] = custObj;
                if (custObj.costomerId == "0Z")
                {
                    custEntityObj.Cm_Cust_Id = "0Z";
                    Session["CoustomerId"] = custEntityObj.Cm_Cust_Id;
                    custEntityObj.Cm_Cust_Name = "0Z";
                    Session["Customer"] = "0Z";
                }
                else
                {
                    custEntityObj.Cm_Cust_Id = custObj.costomerId;
                    Session["CoustomerId"] = custEntityObj.Cm_Cust_Id;
                    custEntityObj.Cm_Cust_Name = custObj.costomerName;
                    Session["Customer"] = custObj.costomerName;
                }
                if (custObj.brandId == "0Z")
                {
                    custEntityObj.brandId = "0Z";
                    Session["BrandId"] = custEntityObj.brandId;
                    custEntityObj.brandName = "0Z";
                    Session["Brand"] = "0Z";
                }
                else
                {
                    custEntityObj.brandId = custObj.brandId;
                    Session["BrandId"] = custObj.brandId;
                    custEntityObj.brandName = custObj.brandName;
                    Session["Brand"] = custObj.brandName;
                }

                custEntityObj.countryName = custObj.countryName;
                Session["Country"] = custObj.countryName;
                if (custEntityObj.sapNo == null)
                {
                    custEntityObj.sapNo = "NA";
                }
                else
                    custEntityObj.sapNo = custObj.sapNo;
                if (custEntityObj.customerPartNO == null)
                {
                    custEntityObj.customerPartNO = "NA";
                }
                else
                    custEntityObj.customerPartNO = custObj.customerPartNO;
                custEntityObj.countryId = custObj.countryId;
                custEntityObj.pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");
                Session["requestIdFinder"] = custEntityObj.pr_uniq_id;
                custEntityObj.cm_request_type = Session["requestype"].ToString();
                custEntityObj.cm_request_id = Convert.ToInt32(Session["requestId"]);
                objBAL.saveCustomerDetails(custEntityObj);
                return RedirectToAction("getFileSpecsDetails", "ProductionDetails");
            //}

            //return RedirectToAction("Index");
        }


        #region  back save  customer details - Archana Mahajan
        public ActionResult getCustomerDetailsIndex()
        {
            CustomerMasterModel custObj = new CustomerMasterModel();
            CustomerMasterModel sessionObj = (CustomerMasterModel)Session["CustomerDetailsTab"];
            if (Session["CustomerDetailsTab"] != null)
            {
                custObj.custList = objBAL.getCustomerList();
                custObj.brandList = objBAL.getBrandList();
                custObj.countryList = objBAL.getShipTOCountryList();
                if (sessionObj.costomerName == "#Customernew")
                {
                    custObj.costomerId = "OZ";
                    custObj.costomerName = "#Customernew";
                }
                else
                {
                    custObj.costomerId = sessionObj.costomerId;
                    custObj.costomerName = sessionObj.costomerName;
                }
                if (sessionObj.brandName == "#Brandnew")
                {
                    custObj.brandId = "OZ";
                    custObj.brandName = "#Brandnew";
                }
                else
                {
                    custObj.brandId = sessionObj.brandId;
                    custObj.brandName = sessionObj.brandName;
                }
                custObj.countryName = sessionObj.countryName;
                custObj.countryId = sessionObj.countryId;
                custObj.sapNo = sessionObj.sapNo;
                custObj.customerPartNO = sessionObj.customerPartNO;


                Session["BrandId"] = custObj.brandId;
                Session["CoustomerId"] = custObj.costomerId;
                Session["ShipToCountryCode"] = custObj.countryId;
                return View("Index", custObj);
            }
            return RedirectToAction("Index");
        }
        #endregion

        [HttpGet]
        public JsonResult getCustomerId(string slectedName)
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Customer";
            string validation = objBAL.checknewmstr(id, type,fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = objBAL.getCustomerID(slectedName);
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "OZ";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult getBrandId(string slectedbrandName)
        {
            string brandId = objBAL.getBrandId(slectedbrandName);

            return Json(brandId, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getCountryId(string slectedCOuntryName)
        {
            string countryId = objBAL.getCountryId(slectedCOuntryName);
            Session["ShipToCountryCode"] = countryId;
            return Json(countryId, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region New Customer by Swapnil
        [HttpGet]
        public JsonResult checkCustomerId()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Customer";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult checkBrandId()
        {
            string type = Session["requestype"].ToString();
            int id = Convert.ToInt32(Session["requestId"]);
            string fo = "Brand";
            string validation = objBAL.checknewmstr(id, type, fo);
            if (string.IsNullOrEmpty(validation))
            {
                string custId = "Ok";
                return Json(custId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string custId1 = "0Z";
                return Json(custId1, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult CreateNewReq()
        {
            var pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");
            Session["requestIdFinder"] = pr_uniq_id;
            string user_id = Session["User_Id"].ToString();
            objBAL.CreateNewReq(pr_uniq_id, user_id);
            int newreqid = objBAL.GetNewReq(pr_uniq_id);
            Session["requestId"] = newreqid;
            string newreqtype = objBAL.GetNewReqtype(pr_uniq_id);
            Session["requestype"] = newreqtype;
            return RedirectToAction("Index");
        }
        //public ActionResult CreateNewReq()
        //{
        //    var pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");

        //    Session["requestIdFinder"] = pr_uniq_id;
        //    string user_id = Session["User_Id"].ToString();
        //    objBAL.CreateNewReq(pr_uniq_id, user_id);
        //    int newreqid = objBAL.GetNewReq(pr_uniq_id);
        //    Session["requestId"] = newreqid;
        //    string newreqtype = objBAL.GetNewReqtype(pr_uniq_id);
        //    Session["requestype"] = newreqtype;
        //    return RedirectToAction("Index");
        //}
        public PartialViewResult _MasterAdd(masterModule model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getCustomerfieldList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult _BrandMasterAdd(BrandModel model)
        {
            MasterReqParModel ReqParlist = new MasterReqParModel();
            ReqParlist.ReqParlist = objBAL.getBrandfieldList();
            return PartialView(ReqParlist);
        }
        public PartialViewResult NewReqList(NewMstrReq model)
        {
            NewMstrReq ReqParlist = new NewMstrReq();
            ReqParlist.ReqParlist = objBAL.getNewMstrReqList(Convert.ToInt16(Session["requestId"]));
            return PartialView(ReqParlist);

        }
        [HttpPost]
        public ActionResult EditCustDetails(CustomerMasterModelForActive model)
        {
            objBAL.CustomerUpdate(model);
            return RedirectToAction("getCustomerDetailsIndex");
        }
        [HttpPost]
        public ActionResult EditBrandDetails(BrandDetailsEntity model)
        {

            objBAL.BrandUpdate(model);

           
            if (Session["requestype"].ToString() == "N")
            {
                return RedirectToAction("getCustomerDetailsIndex");
            }
            else
            {
                return RedirectToAction("getSetCustomerDetailsIndex");
            }
        }
       
        [HttpPost]
        public ActionResult MasterAdd(masterModule model)
        {
            // string abc =Request["1"].ToString();

            if (ModelState.IsValid)
            {
                masterModule5 model1 = new masterModule5();
                model1.cm_cust_id = model.Cm_Cust_Id;
                model1.cm_cust_name = model.Cm_Cust_Name;
                model1.cm_cust_remarks = model.Cm_Cust_Remarks;
                model1.cm_request_id = Convert.ToInt16(Session["requestId"]);
                model1.cm_request_type = Convert.ToString(Session["requestype"]);
                objBAL.newCustomersave(model1);
            }

            return RedirectToAction("NewReqList");
        }
        [HttpPost]
        public ActionResult BrandAdd(BrandModel model)
        {
            // string abc =Request["1"].ToString();

            if (ModelState.IsValid)
            {
                BrandDetailsEntity model1 = new BrandDetailsEntity();
                model1.bm_brand_id = model.bm_brand_id;
                model1.bm_brand_name = model.bm_brand_name;
                model1.bm_brand_remarks = model.bm_brand_remarks;
                model1.bm_request_id = Convert.ToInt16(Session["requestId"]);
                model1.bm_request_type = Convert.ToString(Session["requestype"]);
                objBAL.newBrandsave(model1);
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region  Edit customer details for ReqNo - Dhanashree Kolpe
        public ActionResult EditCustomerDetailsIndex(string ReqNo)
        {
            CustomerMasterModel custObj = new CustomerMasterModel();
            Session["ReqNo"] = ReqNo;
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in ReqNo)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            Session["requestype"] = letters;
            Session["requestId"] = numbers;
            CustomerMasterModel EditObj = new CustomerMasterModel();
            string flag = "cust_details";
            EditObj.custList = objBAL.getEditCustDetails(ReqNo, flag);

            flag = "brand_details";
            EditObj.brandList = objBAL.getEditBrandDetails(ReqNo, flag);

            flag = "country_details";
            EditObj.countryList = objBAL.getEditCountryDetails(ReqNo, flag);

            custObj.custList = objBAL.getCustomerList();
            custObj.brandList = objBAL.getBrandList();
            custObj.countryList = objBAL.getShipTOCountryList();
            if (EditObj.custList != null && EditObj.brandList != null && EditObj.countryList != null)
            {
                custObj.costomerId = EditObj.custList[0].Cm_Cust_Id;
                custObj.costomerName = EditObj.custList[0].Cm_Cust_Name;
                Session["Customer"] = EditObj.custList[0].Cm_Cust_Name;

                custObj.sapNo = EditObj.custList[0].sapNo;
                custObj.customerPartNO = EditObj.custList[0].customerPartNO;

                custObj.brandId = EditObj.brandList[0].bm_brand_id;
                custObj.brandName = EditObj.brandList[0].bm_brand_name;
                Session["Brand"] = EditObj.brandList[0].bm_brand_name;

                custObj.countryId = EditObj.countryList[0].cm_country_cd;
                custObj.countryName = EditObj.countryList[0].cm_country_name;
                Session["Country"] = EditObj.countryList[0].cm_country_name;
                Session["ShipToCountryCode"] = EditObj.countryList[0].cm_country_cd;

                Session["BrandId"] = EditObj.brandList[0].bm_brand_id;
                Session["CoustomerId"] = EditObj.custList[0].Cm_Cust_Id;

                Session["CustomerDetailsTab"] = custObj;

                DataTable dt = new DataTable();
                if (Convert.ToInt32(Session["requestId"]) != 0)
                {
                    dt = objBAL.getdispatchdetails(Convert.ToInt32(Session["requestId"]));
                }

                Session["CoustomerId"] = dt.Rows[0]["pr_cust_id"].ToString();
                Session["BrandId"] = dt.Rows[0]["pr_brand_id"].ToString();
                Session["FileSizeCode"] = dt.Rows[0]["pr_fsize_code"].ToString();
                Session["filesizecode"] = dt.Rows[0]["pr_fsize_code"].ToString();
                Session["Filecode"] = dt.Rows[0]["pr_ftype_code"].ToString() + dt.Rows[0]["pr_fstype_code"].ToString();
                Session["FileCutTypeCode"] = dt.Rows[0]["pr_fcut_code"].ToString();
                Session["CutSpecification"] = dt.Rows[0]["pr_cut_standard"].ToString();
                if (dt.Rows[0]["dp_handle_chart"].ToString() == "" || dt.Rows[0]["dp_handle_chart"].ToString() == null)
                {
                    Session["HandleChartNo"] = "NoChart";
                }
                else
                {
                    Session["HandleChartNo"] = dt.Rows[0]["dp_handle_chart"].ToString();
                }
                Session["WrapperChartNo"] = dt.Rows[0]["dp_wrapping_chart"].ToString();
                Session["InnerBoxNo"] = dt.Rows[0]["dp_innerbox_chart"].ToString();
                if (dt.Rows[0]["dp_innerlabel_chart"].ToString() == ""
                        || dt.Rows[0]["dp_innerlabel_chart"].ToString() == null)
                {
                    Session["InnerLabelChartNum"] = "No Label found";
                }
                else
                {
                    Session["InnerLabelChartNum"] = dt.Rows[0]["dp_innerlabel_chart"].ToString();
                }
                Session["OuterBoxChartNo"] = dt.Rows[0]["dp_outerbox_chart"].ToString();
                Session["PalletChartNo"] = dt.Rows[0]["dp_pallet_chart"].ToString();




                return View("Index", custObj);


            }
            return RedirectToAction("Index");
            //if (Session["CustomerDetailsTab"] != null)
            //{
            //    custObj.custList = objBAL.getCustomerList();
            //    custObj.brandList = objBAL.getBrandList();
            //    custObj.countryList = objBAL.getShipTOCountryList();
            //    if (sessionObj.costomerName == "#Customernew")
            //    {
            //        custObj.costomerId = "OZ";
            //        custObj.costomerName = "#Customernew";
            //    }
            //    else
            //    {
            //        custObj.costomerId = sessionObj.costomerId;
            //        custObj.costomerName = sessionObj.costomerName;
            //    }
            //    if (sessionObj.brandName == "#Brandnew")
            //    {
            //        custObj.brandId = "OZ";
            //        custObj.brandName = "#Brandnew";
            //    }
            //    else
            //    {
            //        custObj.brandId = sessionObj.brandId;
            //        custObj.brandName = sessionObj.brandName;
            //    }
            //    custObj.countryName = sessionObj.countryName;
            //    custObj.countryId = sessionObj.countryId;

            //    return View("Index", custObj);
            //}
        }




        public ActionResult CopyRequest(string ReqNo)
        {
            var pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");
            Session["requestIdFinder"] = pr_uniq_id;
            string user_id = Session["User_Id"].ToString();
            string flag = "copyRequest";
            objBAL.getRequests(flag, user_id, ReqNo, pr_uniq_id);
            int newreqid = objBAL.GetNewReq(pr_uniq_id);
            Session["requestId"] = newreqid;
            string newreqtype = objBAL.GetNewReqtype(pr_uniq_id);
            Session["requestype"] = newreqtype;
            DataTable dt=new DataTable();
            if (newreqid != 0)
            {
                dt = objBAL.getdispatchdetails(newreqid);
            }

            Session["CoustomerId"] = dt.Rows[0]["pr_cust_id"].ToString();
            Session["BrandId"] = dt.Rows[0]["pr_brand_id"].ToString();
            Session["FileSizeCode"] = dt.Rows[0]["pr_fsize_code"].ToString();
            Session["filesizecode"] = dt.Rows[0]["pr_fsize_code"].ToString();
            Session["Filecode"] = dt.Rows[0]["pr_ftype_code"].ToString() + dt.Rows[0]["pr_fstype_code"].ToString();
            Session["FileCutTypeCode"] = dt.Rows[0]["pr_fcut_code"].ToString();
            Session["CutSpecification"] = dt.Rows[0]["pr_cut_standard"].ToString();
            if (dt.Rows[0]["dp_handle_chart"].ToString() == "" || dt.Rows[0]["dp_handle_chart"].ToString() == null)
            {
                Session["HandleChartNo"] = "NoChart";
            }
            else
            {
                Session["HandleChartNo"] = dt.Rows[0]["dp_handle_chart"].ToString();
            }
            Session["WrapperChartNo"] = dt.Rows[0]["dp_wrapping_chart"].ToString();
            Session["InnerBoxNo"] = dt.Rows[0]["dp_innerbox_chart"].ToString();
            if (dt.Rows[0]["dp_innerlabel_chart"].ToString() == ""
                    || dt.Rows[0]["dp_innerlabel_chart"].ToString() == null)
            {
                Session["InnerLabelChartNum"] = "No Label found";
            }
            else
            {
                Session["InnerLabelChartNum"] = dt.Rows[0]["dp_innerlabel_chart"].ToString();
            }
            Session["OuterBoxChartNo"] = dt.Rows[0]["dp_outerbox_chart"].ToString();
            Session["PalletChartNo"] = dt.Rows[0]["dp_pallet_chart"].ToString();
            return RedirectToAction("EditCustomerDetailsIndex", new { ReqNo = Session["requestype"].ToString() + Session["requestId"].ToString() });
        }

        #endregion

        #region Remarks written by yogesh

        public ActionResult SubmitRemarkCustomerDetails(string prm_remarks, string prm_remark_tab)
        {
            try
            {
                PartnumgenRemarksModel remarkObj = new PartnumgenRemarksModel();
                int i = 0;
                if (ModelState.IsValid)
                {
                    PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
                    remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
                    remarkEntityObj.prm_request_type = Session["requestype"].ToString();
                    remarkEntityObj.prm_remark_tab = prm_remark_tab;
                    remarkEntityObj.prm_createby = Session["UserId"].ToString();
                    remarkEntityObj.prm_remarks = prm_remarks;
                    objBAL.SaveRemark(remarkEntityObj);
                    i = 1;
                }
                if (i == 0)
                {
                    return Json(new { success = false, responseText = "Remark not added" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, responseText = "Remark added successfully." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SelectRemarkCustomerDetails(string prm_remark_tab)
        {
            PartnumgenRemarksEntity remarkEntityObj = new PartnumgenRemarksEntity();
            PartnumgenRemarksModel objmodel = new PartnumgenRemarksModel();
            remarkEntityObj.prm_request_id = Convert.ToInt32(Session["requestId"]);
            remarkEntityObj.prm_request_type = Session["requestype"].ToString();
            PartnumgenRemarksModel remarkobj = new PartnumgenRemarksModel();
            remarkEntityObj.remarklist = objBAL.getremarkdetail(prm_remark_tab, remarkEntityObj.prm_request_id, remarkEntityObj.prm_request_type);
            var RList = remarkEntityObj.remarklist;
            if (remarkEntityObj.remarklist.Count == 0)
            {
                return Json(new { success = true, remarks = "", remarkdate = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, remarks = RList }, JsonRequestBehavior.AllowGet);
            }



        }
        #endregion


        #region Customer Request New Set Written by Yogesh

        public ActionResult ManageRequestSet()
        {
            if (Session["User_Id"].ToString()!=null)
            {
              CustomerMasterModel custModelObj = new CustomerMasterModel();
            custModelObj.custList = objBAL.getCustomerList();
            custModelObj.brandList = objBAL.getBrandList();
            custModelObj.countryList = objBAL.getShipTOCountryList();
            return View(custModelObj);
            }
            else
            {
                return RedirectToAction("Index");
            }
          
        }

        public ActionResult CreateNewReqSet()
        {
            var pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");

            Session["requestIdFinder"] = pr_uniq_id;
            string UserId = Session["User_Id"].ToString();
            objBAL.CreateNewReqSet(pr_uniq_id, UserId);
            int newsetreqid = objBAL.GetNewReq(pr_uniq_id);
            Session["requestId"] = newsetreqid;
            string newsetreqtype = objBAL.GetNewReqtype(pr_uniq_id);
            Session["requestype"] = newsetreqtype;
            return RedirectToAction("ManageRequestSet");
        }


        [HttpPost]
        public ActionResult UpdateCustomer(CustomerMasterModel custObj)
        {
           
                CustomerDetailsEntity custEntityObj = new CustomerDetailsEntity();
                Session["SetCustomerDetailsTab"] = custObj;
                if (custObj.costomerName == "#Customernew")
                {
                    custEntityObj.Cm_Cust_Id = "0Z";
                    Session["CoustomerId"] = custEntityObj.Cm_Cust_Id;
                    custEntityObj.Cm_Cust_Name = "0Z";
                    Session["Customer"] = "0Z";
                }
                else
                {
                    custEntityObj.Cm_Cust_Id = custObj.costomerId;
                    Session["CoustomerId"] = custEntityObj.Cm_Cust_Id;
                    custEntityObj.Cm_Cust_Name = custObj.costomerName;
                    Session["Customer"] = custObj.costomerName;
                }
                if (custObj.brandName == "#Brandnew")
                {
                    custEntityObj.brandId = "0Z";
                    Session["BrandId"] = custEntityObj.brandId;
                    custEntityObj.brandName = "0Z";
                    Session["Brand"] = "0Z";
                }
                else
                {
                    custEntityObj.brandId = custObj.brandId;
                    Session["BrandId"] = custObj.brandId;
                    custEntityObj.brandName = custObj.brandName;
                    Session["Brand"] = custObj.brandName;
                }

                custEntityObj.countryName = custObj.countryName;
                Session["Country"] = custObj.countryName;
                if (custEntityObj.sapNo == null)
                {
                    custEntityObj.sapNo = "NA";
                }
                else
                    custEntityObj.sapNo = custObj.sapNo;
                if (custEntityObj.customerPartNO == null)
                {
                    custEntityObj.customerPartNO = "NA";
                }
                else
                    custEntityObj.customerPartNO = custObj.customerPartNO;
                custEntityObj.countryId = custObj.countryId;
                custEntityObj.pr_uniq_id = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff");
                Session["requestIdFinder"] = custEntityObj.pr_uniq_id;
                custEntityObj.cm_request_type = Session["requestype"].ToString();
                custEntityObj.cm_request_id = Convert.ToInt32(Session["requestId"]);
                objBAL.saveCustomerDetails(custEntityObj);
                return RedirectToAction("getSKUSelection", "SKUSelection");
            

            //return RedirectToAction("Index");
        }

        public ActionResult getSetCustomerDetailsIndex()
        {
            CustomerMasterModel custObj = new CustomerMasterModel();
            CustomerMasterModel sessionObj = (CustomerMasterModel)Session["SetCustomerDetailsTab"];
            if (Session["SetCustomerDetailsTab"] != null)
            {
                custObj.custList = objBAL.getCustomerList();
                custObj.brandList = objBAL.getBrandList();
                custObj.countryList = objBAL.getShipTOCountryList();
                if (sessionObj.costomerName == "#Customernew")
                {
                    custObj.costomerId = "OZ";
                    custObj.costomerName = "#Customernew";
                }
                else
                {
                    custObj.costomerId = sessionObj.costomerId;
                    custObj.costomerName = sessionObj.costomerName;
                }
                if (sessionObj.brandName == "#Brandnew")
                {
                    custObj.brandId = "OZ";
                    custObj.brandName = "#Brandnew";
                }
                else
                {
                    custObj.brandId = sessionObj.brandId;
                    custObj.brandName = sessionObj.brandName;
                }
                custObj.countryName = sessionObj.countryName;
                custObj.countryId = sessionObj.countryId;
                custObj.sapNo = sessionObj.sapNo;
                custObj.customerPartNO = sessionObj.customerPartNO;


                Session["BrandId"] = custObj.brandId;
                Session["CoustomerId"] = custObj.costomerId;
                Session["ShipToCountryCode"] = custObj.countryId;
                return View("ManageRequestSet", custObj);
            }
            return RedirectToAction("ManageRequestSet");
        }

        public ActionResult EditSetCustomerDetailsIndex(string ReqNo)
        {
            CustomerMasterModel custObj = new CustomerMasterModel();
            Session["ReqNo"] = ReqNo;
            string letters = string.Empty;
            string numbers = string.Empty;
            foreach (char c in ReqNo)
            {
                if (Char.IsLetter(c))
                {
                    letters += c;
                }
                if (Char.IsNumber(c))
                {
                    numbers += c;
                }
            }
            Session["requestype"] = letters;
            Session["requestId"] = numbers;
            CustomerMasterModel EditObj = new CustomerMasterModel();
            string flag = "cust_details";
            EditObj.custList = objBAL.getEditCustDetails(ReqNo, flag);

            flag = "brand_details";
            EditObj.brandList = objBAL.getEditBrandDetails(ReqNo, flag);

            flag = "country_details";
            EditObj.countryList = objBAL.getEditCountryDetails(ReqNo, flag);

            custObj.custList = objBAL.getCustomerList();
            custObj.brandList = objBAL.getBrandList();
            custObj.countryList = objBAL.getShipTOCountryList();
            if (EditObj.custList != null && EditObj.brandList != null && EditObj.countryList != null)
            {
                custObj.costomerId = EditObj.custList[0].Cm_Cust_Id;
                custObj.costomerName = EditObj.custList[0].Cm_Cust_Name;
                Session["Customer"] = EditObj.custList[0].Cm_Cust_Name;

                custObj.sapNo = EditObj.custList[0].sapNo;
                custObj.customerPartNO = EditObj.custList[0].customerPartNO;

                custObj.brandId = EditObj.brandList[0].bm_brand_id;
                custObj.brandName = EditObj.brandList[0].bm_brand_name;
                Session["Brand"] = EditObj.brandList[0].bm_brand_name;

                custObj.countryId = EditObj.countryList[0].cm_country_cd;
                custObj.countryName = EditObj.countryList[0].cm_country_name;
                Session["Country"] = EditObj.countryList[0].cm_country_name;
                Session["ShipToCountryCode"] = EditObj.countryList[0].cm_country_cd;

                Session["BrandId"] = EditObj.brandList[0].bm_brand_id;
                Session["CoustomerId"] = EditObj.custList[0].Cm_Cust_Id;

                Session["SetCustomerDetailsTab"] = custObj;
                return View("ManageRequestSet", custObj);
            }
            return RedirectToAction("ManageRequestSet");
            
        }

        #endregion

    }
}