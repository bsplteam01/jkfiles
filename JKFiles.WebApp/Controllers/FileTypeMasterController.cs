﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using JKFiles.WebApp.Models;
using JKFiles.WebApp.ActionFilters;
using System.Linq.Dynamic;
namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class FileTypeMasterController : Controller
    {
        // GET: FileTypeMaster
        #region FileType Master By Swapnil
        BAL objBAL = new BAL();
        FileTypeDataEntity ft = new FileTypeDataEntity();
        public ActionResult Index()
        {
            //var v = ft.fileTypelist = getFiletype();
            //ft.fileTypelist = getFiletype();
            // return View(ft);
            return View();
        }


        public ActionResult NewRequest()
        {
            ft.fileTypeNoActiveList = objBAL.fileTypeListinactive();
            return PartialView("_NewRequest", ft);
        }
        public ActionResult ApprovedRequest()
        {
            ft.fileTypeNoApproveList = objBAL.fileTypeListNoApprove();
            return PartialView("_ApprovedRequest", ft);
        }
        public ActionResult FileTypemstrDetails(string filetype, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<FileTypeDataEntity> ftype = null;
            FileTypeDataEntity ft = new FileTypeDataEntity();
            List<FileTypeDataEntity> ftlist = new List<FileTypeDataEntity>();
            ftlist = getFiletype();
            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                ftlist = ftlist.Where(x => x.ft_ftype_desc == filetype).ToList();
            }
            ft.fileTypelist = ftlist;
            ftype = ftlist.ToPagedList(pageIndex, pageSize);
            return PartialView("_FileTypemstrDetails", ftype);
        }


        [HttpGet]
        public ActionResult getfiletypeimage(string reqno)
        {
            FileTypeDataEntity hm = new FileTypeDataEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string fty_des = values[0];
            //string boxtype = values[1];
            // pl.pm_pallet_type = boxtype;
            hm.ft_ftype_desc = fty_des;
            hm.listtype = objBAL.getftimageview(hm.ft_ftype_desc);
            //objBAL.GetinnerLabelListview(pl.pm_pallet_chartnum, pl.pm_pallet_type);
            // ob.outerboxMasterList = objBAL.GetOuterChartlistview(ob.ob_box_chartnum, ob.ob_box_type);
            return PartialView("_FileTypeImage", hm);

        }


        public ActionResult Filetypeddlmaster()
        {

            //Swapnil code
            FileTypeDataEntity ftyp = new FileTypeDataEntity();

            ftyp.listtype = get_type();
          
            return PartialView("_Filetypeddlmaster", ftyp);

        }

        public List<FileTypeDataEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            FileTypeDataEntity modelobj = new FileTypeDataEntity();
            List<FileTypeDataEntity> list = new List<FileTypeDataEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileTypeDataEntity obj = new FileTypeDataEntity();
                    obj.ft_ftype_desc = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public ActionResult filetypeDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            FileTypeDataEntity ftypeModelObj = new FileTypeDataEntity();
            // custModelObj = CustomerMasterModelForActive.Find(Id);
            // custModelObj.custModel = objBAL.getCustomerListbyid(Id);
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_fileType_master_details_byid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@ft_request_id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                ftypeModelObj._id = Convert.ToInt16(dt.Rows[0]["_id"].ToString());
                ftypeModelObj.ft_ftype_code = dt.Rows[0]["ft_ftype_code"].ToString();
                ftypeModelObj.ft_ftype_desc = dt.Rows[0]["ft_ftype_desc"].ToString();
                ftypeModelObj.ft_ftype_remarks = dt.Rows[0]["ft_ftype_remarks"].ToString();
                ftypeModelObj.ft_request_id = Convert.ToInt16(dt.Rows[0]["ft_request_id"].ToString());
                ftypeModelObj.ft_request_type = dt.Rows[0]["ft_request_type"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("_filetypeDetails", ftypeModelObj);
        }
        [HttpPost]
        public ActionResult SavefiletypeDetails(FileTypeDataEntity model)
        {
            //model.ft_request_id = Convert.ToInt16(Session["requestId"]);
            //model.ft_request_type = Convert.ToString(Session["requestype"]);
            objBAL.filetypesaveactive(model);
            //custModelObj.custList = objBAL.getCustomerList();
            //custModelObj.custNoActiveList = objBAL.getCustomerListinactive();
            //custModelObj.custNoApproveList = objBAL.getCustListNoApprove();


            #region Mail Sending by Dhanashree
            string subject = "";
            string body = "";

            subject = "Request No:  " + model.ft_request_type + model.ft_request_id
                + " and " + "Production Master Update";
            body = "Request No: " + model.ft_request_type + model.ft_request_id +
                " By User ID " + Session["UserId"].ToString()
                + ",File Type Master updated.Needs Approval.";

            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_MGR", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
               objBAL.sendingmail(email,body,subject);
            }
            #endregion

            return RedirectToAction("Index");
        }
        public ActionResult UpdatefiletypeApprove(int? ID)
        {
            if (ID.HasValue)
            {
                string type = "N";
                objBAL.UpdatefiletypeApprove(ID, type);                

                string subject = "";
                string body = "";
                subject = "Request No:  " + type + ID + " and " +
                       "Production Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",File Type Master Approved.";


                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Prod_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }

            }
            return RedirectToAction("Index");
        }
        public ActionResult UpdateStampApprove(int? ID,string type)
        {
            if (ID.HasValue)
            {             
                objBAL.UpdateStampApprove(ID, type);
            }
            return RedirectToAction("Index");
        }
        
        public List<FileTypeDataEntity> getFiletype()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileType";
                dt = objBAL.getMasters(flag);
                List<FileTypeDataEntity> listfiletype = new List<FileTypeDataEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfiletype = dt.AsEnumerable().Select(item => new FileTypeDataEntity
                    {
                        ft_ftype_desc = item.Field<string>("FileType"),
                        ft_ftype_remarks = item.Field<string>("ft_ftype_remarks"),
                        ft_ftype_code = item.Field<string>("ft_ftype_code"),
                        _id = item.Field<int>("_id"),
                        ft_request_type = item.Field<string>("ft_request_type"),
                        ft_process_code = item.Field<string>("ft_process_code"),
                        ft_ftype_profile_img = item.Field<string>("ft_ftype_profile_img"),
                        ft_request_id = item.Field<int>("ft_request_id"),
                    }
                    ).ToList();
                }
                return listfiletype;
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion
        [HttpGet]
        public string getProfileImage(string url)
        {
            return url;
        }

        public ActionResult Showaddprocess(string process_code)
        {
            List<ProcessmasterModel> processlist = new List<ProcessmasterModel>();

            processlist = GetFileTypeprocesslist(process_code);
            return Json(new { success = true, processlist = processlist }, JsonRequestBehavior.AllowGet);
        }

        public List<ProcessmasterModel> GetFileTypeprocesslist(string process_code)
        {
            string flag = "processprofilelist";
            DataTable dt = objBAL.FileTypeprocesslist(flag, process_code);
            ProcessmasterModel modelobj = new ProcessmasterModel();
            List<ProcessmasterModel> list = new List<ProcessmasterModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ProcessmasterModel objProc = new ProcessmasterModel();
                    objProc.ppm_process_code = dt.Rows[i]["ppm_process_code"].ToString();
                    objProc.ppm_process_seqno = Convert.ToInt32(dt.Rows[i]["ppm_process_seqno"].ToString());
                    objProc.ProcessName = dt.Rows[i]["opr_operation_name"].ToString();
                    list.Add(objProc);
                }
            }
            return list.ToList();
        }
        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                //using (DatabaseContext _context = new DatabaseContext())
                //{
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                IList<FileTypeDataEntity> Data = getFiletype();
                var customerData = (from tempcustomer in Data
                                    select tempcustomer);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    customerData = customerData.OrderBy(sortColumn + " " + sortColumnDir);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.ft_ftype_desc.ToUpper().Contains(searchValue.ToUpper())
                        || m.ft_ftype_code.ToString().ToUpper().Contains(searchValue.ToUpper()) || m.ft_process_code.ToString().ToUpper().Contains(searchValue));
                }
                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
