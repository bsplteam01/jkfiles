﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;
using JKFIlesEntity;
using System.Text;
using System.Net.Mail;

namespace JKFiles.WebApp.Controllers
{
    public class AddUserController : Controller
    {
        BAL objBAL = new BAL();
        byte[] logo, profile;
        // GET: AddUser
        public ActionResult Index()
        {
            UserLoginModel um = new UserLoginModel();
            um.listrole = getrolelist();
            um.listplant = getplantlist();
            return View("Index", um);
        }


        //get rolelist
        public List<UserRoleModel> getrolelist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getrolelist";
                dt = objBAL.getRolelist(flag);
                List<UserRoleModel> rl = new List<UserRoleModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new UserRoleModel
                    {
                        Role_Id = item.Field<int>("Role_Id"),
                        RoleName = item.Field<string>("RoleName")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //get plantlist
        public List<UserRoleModel> getplantlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getplantlist";
                dt = objBAL.getPlantlist(flag);
                List<UserRoleModel> rl = new List<UserRoleModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new UserRoleModel
                    {
                        Plant_Id = item.Field<int>("Plant_Id"),
                        PlantName = item.Field<string>("PlantName")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //inserting user
        [HttpPost]
        public ActionResult Index(UserLoginModel custObj)
        {
            if (ModelState.IsValid)
            {
                UsermgmtEntity user = new UsermgmtEntity();
                user.PlantName= custObj.PlantName;
                user.RoleName = custObj.RoleName;
                user.FirstName = custObj.FirstName;
                Session["firstname"] = user.FirstName;
                user.LastName = custObj.LastName;
                Session["lastname"] = user.LastName;
                user.Employee_Id = custObj.Employee_Id;
                user.Department = custObj.Department;
                user.User_Id = custObj.User_Id;
                Session["userid"]= user.User_Id;
                user.ApproveFlag = "No";
                Random random = new Random();
                int no = random.Next(100000, 999999);
                custObj.Password =Convert.ToInt32(no).ToString();
                user.Password = custObj.Password;
                Session["password"] = user.Password;

                #region getcstdetails
                DataTable d= objBAL.getcstdetails("getcstdetails");
                user.OEMId= d.Rows[0]["OEMId"].ToString();
                user.OEMName = d.Rows[0]["OEMName"].ToString();
                Session["oemname"]= user.OEMName;
                user.Registration_Key= d.Rows[0]["Registration_Key"].ToString();
                #endregion

                #region DefaultLogo and Pic
                user.flag = "getimage";
                DataTable dt = objBAL.getImage("getimage");
                string pic= dt.Rows[0]["ProfilePic"].ToString();
                profile = Encoding.ASCII.GetBytes(pic);
                user.ProfilePic = profile;
                #endregion

                user.flag = "insertloginuser";

                objBAL.saveuserDetails(user);
                sendingmail();
                return RedirectToAction("UserManagementIndex", "UserManagement");
            }
            return RedirectToAction("Index");
        }

        public void sendingmail()
        {
            try
            {
                string Email = Session["userid"].ToString();
                string pwd= Session["password"].ToString();
                string fname = Session["firstname"].ToString();
                string lname = Session["lastname"].ToString();
                string name = Session["oemname"].ToString();

                if (Email != "")
                {
                    MailMessage mail = new MailMessage();
                    mail.To.Add(Email);
                   // mail.From = new MailAddress("dashboard@nichrome.com");
                    mail.From = new MailAddress("noreply@decintell.in");
                    // mail.CC.Add("");

                    mail.Subject = "Your account at " + name + "";
                    //string Body = "Dear " + txt_oemname.Text + " " + "," + "<br/><br/>";
                    string Body = "<br/>Welcome to " + name + " <br/><br/> ";
                    Body += "Dear " + fname + " " + " " + lname + "<br/><br/>";
                    Body += "<br/>You can access your account using details as below <br/><br/> ";
                    // Body += "<br/> http://www.brainlines.in/DID_Premise/OEMLoginPage.aspx  <br/><br/>";
                    Body += "<br/> http://192.10.10.133:8067/OEMLoginPage.aspx  <br/><br/>";
                    Body += "UserName  :" + Email + "<br/><br/>";
                    Body += "Password  :" + pwd + "<br/><br/>";

                    Body += "Warm Regards,<br/><br/> ";
                    Body += "DI Team" + "<br/><br/>";

                 
                    SmtpClient smtp = new SmtpClient();
                  
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    smtp.Host = "mail.decintell.in";   //-- Donot change.
                    smtp.Port = 25;
                    smtp.EnableSsl = false;//--- Donot change
                    smtp.UseDefaultCredentials = true;
                    //smtp.Credentials = new System.Net.NetworkCredential("dashboard-nichrome", "DashBoard_1");
                    smtp.Credentials = new System.Net.NetworkCredential("noreply@decintell.in", "fKx56f$3");
                    smtp.Send(mail);
                }
                else
                {
                }
            }
            catch (Exception)
            {
            }
        }
    }
}