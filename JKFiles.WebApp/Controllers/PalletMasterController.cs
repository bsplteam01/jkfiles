﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using System.IO;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class PalletMasterController : Controller
    {
        BAL objBAL = new BAL();
        PalletMasterEntity pl = new PalletMasterEntity();
        // GET: PalletMaster
        public ActionResult Index()
        {
            //pl.palletmsterEntityList = getpalletmstr();
            //return View("Index", pl);
            return View();
        }
        public ActionResult NewRequest()
        {
            pl.palletmsterNoactiveList = getpalletnoactive();
            return PartialView("_NewRequest", pl);
        }
        public ActionResult ApprovedRequest()
        {
            pl.palletmsterNoApproveList = getpalletnoApprove();
            return PartialView("_ApprovedRequest", pl);
        }
        [HttpPost]
        public ActionResult EditPalletChartDetails(PalletDetailsEntity model)
        {
            if (ModelState.IsValid)
            {
                //model.pm_request_type = Convert.ToString(Session["requestype"]);
                //model.pm_request_id = Convert.ToInt16(Session["requestId"]);
                HttpPostedFileBase photo = Request.Files["file1"];
                if (photo != null)
                {
                    model.pm_pallet_chartimg = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/Pallet/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                    }
                }
                #region Mail Sendin by Dhanashree 

                string subject = "";
                string body = "";

                subject = "Request No:  " + model.pm_request_type + model.pm_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.pm_request_type + model.pm_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",Pallet Master updated.Needs Approval.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email,body,subject);
                }
                #endregion
                objBAL.EditPalletchartactive(model);
            }
            return RedirectToAction("Index");
        }
        public ActionResult PalletChartDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            PalletDetailsEntity PalletModelObj = new PalletDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Pallet_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                PalletModelObj.pm_pallet_chartnum = dt.Rows[0]["pm_pallet_chartnum"].ToString();
                PalletModelObj.pm_pallet_type = dt.Rows[0]["pm_pallet_type"].ToString();
                PalletModelObj.pm_pallet_chartimg = dt.Rows[0]["pm_pallet_chartimg"].ToString();
                PalletModelObj.PALLETMATERIALRANGE = dt.Rows[0]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETDESCRIPTIONRANGE = dt.Rows[1]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETTYPERANGE = dt.Rows[2]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETLENGTHRANGE = dt.Rows[3]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETWIDTHRANGE = dt.Rows[4]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHEIGHTRANGE = dt.Rows[5]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERRANGE = dt.Rows[6]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERLENGTHRANGE = dt.Rows[7]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERWIDTHRANGE = dt.Rows[8]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERHEIGHTRANGE = dt.Rows[9]["pd_parm_dwgvalue"].ToString();
                PalletModelObj.PALLETMATERIAL = dt.Rows[0]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETDESCRIPTION = dt.Rows[1]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETTYPE = dt.Rows[2]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETLENGTH = dt.Rows[3]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETWIDTH = dt.Rows[4]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHEIGHT = dt.Rows[5]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVER = dt.Rows[6]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERLENGTH = dt.Rows[7]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERWIDTH = dt.Rows[8]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.PALLETHDPECOVERHEIGHT = dt.Rows[9]["pd_parm_scrnvalue"].ToString();
                PalletModelObj.pm_request_type = dt.Rows[0]["pm_request_type"].ToString();
                PalletModelObj.pm_request_id = Convert.ToInt16(dt.Rows[0]["pm_request_id"].ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("PalletChartDetails", PalletModelObj);
        }
        public List<PalletMasterEntity> getpalletnoactive()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getpalletnoactive";
                dt = objBAL.getpalletMasters(flag);
                List<PalletMasterEntity> listfilesize = new List<PalletMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new PalletMasterEntity
                    {
                        pm_pallet_type = item.Field<string>("pm_pallet_type"),
                        pm_pallet_chartnum = item.Field<string>("pm_pallet_chartnum"),
                        pm_request_type = item.Field<string>("pm_request_type"),
                        pm_request_id = item.Field<int>("pm_request_id"),
                        ReqNo = item.Field<string>("pm_request_type")+item.Field<int>("pm_request_id")
                    }).ToList();
                }
                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PalletMasterEntity> getpalletnoApprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getpalletnoApprove";
                dt = objBAL.getpalletMasters(flag);
                List<PalletMasterEntity> listfilesize = new List<PalletMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new PalletMasterEntity
                    {
                        pm_pallet_type = item.Field<string>("pm_pallet_type"),
                        pm_pallet_chartnum = item.Field<string>("pm_pallet_chartnum"),
                        pm_request_type = item.Field<string>("pm_request_type"),
                        pm_request_id = item.Field<int>("pm_request_id"),
                        ReqNo = item.Field<string>("pm_request_type") + item.Field<int>("pm_request_id")
                    }).ToList();
                }
                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult PalletdetailsMaster(string pallettype, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<PalletMasterEntity> pm = null;
            PalletMasterEntity pmm = new PalletMasterEntity();
            List<PalletMasterEntity> pllist = new List<PalletMasterEntity>();
            pllist = getpalletmstr();
            if (!String.IsNullOrEmpty(pallettype))
            {
                ViewBag.pallettype = pallettype;
                pllist = pllist.Where(x => x.pm_pallet_type == pallettype).ToList();
            }
            pmm.palletmsterEntityList = pllist;
            pm = pllist.ToPagedList(pageIndex, pageSize);

            return PartialView("_PalletdetailsMaster", pm);
        }
        public ActionResult Palletddlmaster()
        {


            PalletMasterEntity obj = new PalletMasterEntity();
            obj.pallettypemsterList = get_pallettype();


            return PartialView("_Palletddlmaster", obj);

        }
        public ActionResult UpdatePalletApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
                objBAL.UpdatePalletApprove(ID, type);

                string subject = "";
                string body = "";
                subject = "Request No:  " + type + ID + " and " +
                       "Dispatch Master Approval";
                body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                    + ",Pallet Master Approved.";


                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                    objBAL.sendingmail(email, body, subject);
                }
            }
            return Json(new { url = @Url.Action("Index", "PalletMaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "Pallet";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "PalletMaster") }, JsonRequestBehavior.AllowGet);

        }
        public List<PalletMasterEntity> get_pallettype()
        {
            string flag = "getpallettypemstr";
            DataTable dt = objBAL.getrawmateriallist(flag);
            PalletMasterEntity modelobj = new PalletMasterEntity();
            List<PalletMasterEntity> list = new List<PalletMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PalletMasterEntity obj = new PalletMasterEntity();
                    obj.pm_pallet_type = (dt.Rows[i]["pm_pallet_type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }

        public List<PalletMasterEntity> getpalletmstr()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getpalletmstr";
                dt = objBAL.getpalletMasters(flag);
                List<PalletMasterEntity> listfilesize = new List<PalletMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new PalletMasterEntity
                    {
                        pm_pallet_type = item.Field<string>("pm_pallet_type"),
                        pm_pallet_chartnum = item.Field<string>("pm_pallet_chartnum")
                    }).ToList();
                }
                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult editPalletmaster(string pm_pallet_chartnum, string pm_pallet_type)
        {
            PalletMasterEntity pl = new PalletMasterEntity();
           
            pl.palletmsterList = objBAL.GetPalletListview(pm_pallet_chartnum,pm_pallet_type);
            return PartialView("_EditPaleetMaster", pl);
        }

        [HttpGet]
        public ActionResult getApprovepalletmstrview(string pm_pallet_chartnum,string pm_pallet_type)
        {
            PalletMasterEntity pl = new PalletMasterEntity();
           
            pl.palletmsterList = objBAL.getApprovepalletmstrview(pm_pallet_chartnum,pm_pallet_type);
          
            return PartialView("_EditPaleetMaster", pl);
        }
    }
}