﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using JKFiles.WebApp.Models;
using JKFilesBAL;
using System.Data;

namespace JKFiles.WebApp.Controllers
{
    public class RequestToAdminController : Controller
    {
        BAL objBAL = new BAL();
        // GET: RequestToAdmin
        public ActionResult RequestToAdminIndex()
        {
            ComplaintmgntModel cm = new ComplaintmgntModel();
            cm.listrequest = getrqquestuserlist();
            return View("RequestToAdminIndex", cm);
           
        }

        //get Request type
        public List<ComplaintmgntModel> getrqquestuserlist()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getrequestusertype";
                dt = objBAL.getReqtypeuser(flag);
                List<ComplaintmgntModel> rl = new List<ComplaintmgntModel>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    rl = dt.AsEnumerable().Select(item => new ComplaintmgntModel
                    {
                        Request_Type_Id = item.Field<int>("Request_Type_Id"),
                        RequestType = item.Field<string>("Request_Type")
                    }).ToList();
                }
                return rl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //inserting request
        [HttpPost]
        public ActionResult RequestToAdminIndex(ComplaintmgntModel custObj)
        {
            if (ModelState.IsValid)
            {
                RequestMgmtEntity req = new RequestMgmtEntity();
                //  req.Request_Type_Id = custObj.Request_Type_Id;
                req.RequestType = custObj.RequestType;
                req.RequestDesc = custObj.RequestDesc;
                //req.OEMId = Session["OEMId"].ToString();
                //req.OEMName = Session["OEMName"].ToString();
                //req.Customer_Id = Session["Customer_Id"].ToString();
                req.OEMId = "5555";
                req.OEMName = "JK";
                req.Customer_Id = "0";
                req.flag = "insertoemdirequestadmin";

                objBAL.saverequestDetails(req);
                return RedirectToAction("RequestToAdminIndex", "RequestToAdmin");
            }
            return RedirectToAction("RequestToAdminIndex");
        }

        //list of request grid view
        public ActionResult RequestGrid()
        {
            ComplaintmgntModel modelobj = new ComplaintmgntModel();
            modelobj.listrequest = get_request();
            var v = modelobj.listrequest;
            return PartialView("_RequestToAdmin", v.ToList());
        }

        public List<ComplaintmgntModel> get_request()
        {
            string flag = "getrequestAdmin";
            DataTable dt = objBAL.getrequestlist(flag);
            ComplaintmgntModel modelobj = new ComplaintmgntModel();
            List<ComplaintmgntModel> list = new List<ComplaintmgntModel>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ComplaintmgntModel objRole = new ComplaintmgntModel();
                    objRole.Request_Id = Convert.ToInt32(dt.Rows[i]["Request_Id"].ToString());
                    objRole.OEMId = dt.Rows[i]["OEMId"].ToString();
                    objRole.OEMName = dt.Rows[i]["OEMName"].ToString();
                    objRole.RequestType = dt.Rows[i]["RequestType"].ToString();
                    objRole.RequestDesc = dt.Rows[i]["RequestDesc"].ToString();
                    objRole.RequestStatus = dt.Rows[i]["RequestStatus"].ToString();
                    objRole.RequestDate = dt.Rows[i]["RequestDate"].ToString();
                    objRole.ModifiedDate = dt.Rows[i]["ModifiedDate"].ToString();
                    objRole.RequestBetween = dt.Rows[i]["RequestBetween"].ToString();
                    list.Add(objRole);
                }
            }
            return list.ToList();
        }
    }
}