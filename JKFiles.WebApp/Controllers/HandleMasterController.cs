﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFIlesEntity;
using System.Data;
using JKFilesBAL;
using System.Data.SqlClient;
using PagedList;
using PagedList.Mvc;
using System.IO;
using JKFiles.WebApp.ActionFilters;

namespace JKFiles.WebApp.Controllers
{
    [HandleError]
    [SessionTimeout]
    public class HandleMasterController : Controller
    {
        BAL objBAL = new BAL();
        HandleMasterEntity hm = new HandleMasterEntity();
        // GET: HandleMaster
        public ActionResult Index()
        {
           // hm.handleMasterList = gethandlemstr();
          //  return View("Index", hm);

            return View();
        }
        public ActionResult UpdateHandleApprove(int? ID, string type)
        {
            if (ID.HasValue)
            {
             objBAL.UpdateHandleApprove(ID, type);
            }
            string subject = "";
            string body = "";
            subject = "Request No:  " + type + ID + " and " +
                   "Dispatch Master Approval";
            body = "Request No: " + type + ID + " By User ID " + Session["UserId"].ToString()
                + ",Handle Master Approved.";


            string flag = "getMailByRole";
            DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_User", "", flag);
            if (dtEmail.Rows.Count > 0 && dtEmail != null)
            {
                string email = dtEmail.Rows[0]["User_Id"].ToString();
                objBAL.sendingmail(email, body, subject);
            }

            return Json(new { url = @Url.Action("Index", "HandleMaster") }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectApprove(int? ID, string type)
        {
            string Flag = "Handle";
            objBAL.RejectApprove(ID, type, Flag);
            return Json(new { url = @Url.Action("Index", "HandleMaster") }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult EditHandleChartDetailsactive(HandleDetailsEntity model)
        {
            var file = Request.Files["file1"];
            if (file.FileName != "")
            {
                model.hm_chartimg_name = file.FileName;
                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].ContentLength == 0) continue;
                    string pathToSave = Server.MapPath("~/ProjectImages/Handle/");
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(pathToSave, filename));
                }
            }
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.hm_handle_type))
                {
                    model.hm_handle_type = "";
                }
                objBAL.EditHandlechartDetailsactive(model);

                string subject = "";
                string body = "";

                subject = "Request No:  " + model.hm_request_type + model.hm_request_id
                    + " and " + "Dispatch Master Update";
                body = "Request No: " + model.hm_request_type + model.hm_request_id +
                    " By User ID " + Session["UserId"].ToString()
                    + ",Handle Master updated.Needs Approval.";
                string flag = "getMailByRole";
                DataTable dtEmail = objBAL.GetEmailId("EMT_Disp_MGR", "", flag);
                if (dtEmail.Rows.Count > 0 && dtEmail != null)
                {
                    string email = dtEmail.Rows[0]["User_Id"].ToString();
                   objBAL.sendingmail(email,body,subject);
                }
            }

            return RedirectToAction("Index");
        }
        public ActionResult HandleDetails(int Id,string type)
        {
            Utils objUtils = new Utils();
            string connectionstring;
            connectionstring = objUtils.getConnString();
            HandleDetailsEntity HandleModelObj = new HandleDetailsEntity();

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_handle_master_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlParameter param = new SqlParameter("@id", Id);
                SqlParameter param1 = new SqlParameter("@type", type);
                cmd.Parameters.Add(param1);
                cmd.Parameters.Add(param);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                HandleModelObj.fileTypeCode = dt.Rows[0]["hm_ftype_id"].ToString();
                HandleModelObj.FileSubType = dt.Rows[0]["hm_fstype_id"].ToString();
                HandleModelObj.fileSize = dt.Rows[0]["hm_fsize_id"].ToString();
                HandleModelObj.hm_request_type = dt.Rows[0]["hm_request_type"].ToString();
                HandleModelObj.hm_request_id = Convert.ToInt16(dt.Rows[0]["hm_request_id"].ToString());
                HandleModelObj.hm_handle_type = dt.Rows[0]["hm_handle_type"].ToString();
                HandleModelObj.tempChartNum = dt.Rows[0]["hm_chart_num"].ToString();
                HandleModelObj.hm_chartimg_name = dt.Rows[0]["hm_chartimg_name"].ToString();
                HandleModelObj.remark = dt.Rows[0]["hm_chart_remarks"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }

            return PartialView("REQHandleChartDetails", HandleModelObj);
        }
        public ActionResult NewRequest()
        {

            HandleMasterEntity obj = new HandleMasterEntity();

            obj.handlenoactive = gethandlemstrnoactive();

            return PartialView("_NewRequest", obj);

        }
        public ActionResult ApprovedRequest()
        {
            HandleMasterEntity objj = new HandleMasterEntity();
            objj.handlenoapprove = gethandlemstrnoapprove();

            return PartialView("_ApprovedRequest", objj);

        }
        public ActionResult Handledetailmaster(string filesize, string filetype, string filesubtype,string handletype, string handlesubtype, string chartno, int? page)
        {
           // hm.handleMasterList = gethandlemstr();
           // return View("Index", hm);


            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<HandleMasterEntity> hm = null;
            HandleMasterEntity hmmm = new HandleMasterEntity();
            List<HandleMasterEntity> hndlelist = new List<HandleMasterEntity>();

            hndlelist = gethandlemstr();

            if (!String.IsNullOrEmpty(filesize))
            {
                ViewBag.filesize = filesize;
                hndlelist = hndlelist.Where(x => x.hm_fsize_id == filesize).ToList();
            }

            if (!String.IsNullOrEmpty(filesubtype))
            {
                ViewBag.fsubtypecode = filesubtype;
                hndlelist = hndlelist.Where(x => x.pm_fstype_desc == filesubtype).ToList();
            }

            if (!String.IsNullOrEmpty(filetype))
            {
                ViewBag.filetype = filetype;
                hndlelist = hndlelist.Where(x => x.ft_ftype_desc == filetype).ToList();
            }

            if (!String.IsNullOrEmpty(handletype))
            {
                ViewBag.handletype = handletype;
                hndlelist = hndlelist.Where(x => x.hm_handle_type == handletype).ToList();
            }

            if (!String.IsNullOrEmpty(handlesubtype))
            {
                ViewBag.handlesubtype = handlesubtype;
                hndlelist = hndlelist.Where(x => x.hm_handle_subtype == handlesubtype).ToList();
            }

            if (!String.IsNullOrEmpty(chartno))
            {
                ViewBag.chartno = chartno;
                hndlelist = hndlelist.Where(x => x.hm_chart_num == chartno).ToList();
            }

            // cstlist = objBAL.getCutSpecMastersearch(selectedCutStandardName);
            hmmm.handleMasterList = hndlelist;
            hm = hndlelist.ToPagedList(pageIndex, pageSize);

            return PartialView("_Handledetailmaster", hm);
        }   
        public ActionResult Handleddlmaster()
        {


            HandleMasterEntity obj = new HandleMasterEntity();

            obj.listsizecode = get_size();
            obj.listtype = get_type();
            obj.listsubtype = get_subtype();
            obj.listfiletypecode = get_filetypecode();
            obj.listhandletype = get_handletype();
            obj.listhandlesubtype = get_handlesubtype();
            obj.listchartno = get_handlechartno();
            return PartialView("_Handleddlmaster", obj);

        }
        public List<HandleMasterEntity> get_filetypecode()
        {
            string flag = "getfiletypecode";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.filetypecode = (dt.Rows[i]["File_Type_Code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> get_subtype()
        {
            string flag = "getfilesubtypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.filesubtype = (dt.Rows[i]["pm_fstype_desc"].ToString());
                    obj.pm_fstype_code = (dt.Rows[i]["pm_fstype_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> get_type()
        {
            string flag = "getfiletypes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.filetype = (dt.Rows[i]["ft_ftype_desc"].ToString());
                    obj.ft_ftype_code = (dt.Rows[i]["ft_ftype_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> get_size()
        {
            string flag = "getfilesizecodes";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.fsize_code = (dt.Rows[i]["fs_size_code"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> get_handletype()
        {
            string flag = "gethandletypmstr";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.handletype = (dt.Rows[i]["hm_handle_type"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> get_handlesubtype()
        {
            string flag = "gethandlesubtypmstr";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.handlesubtype = (dt.Rows[i]["hm_handle_subtype"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> get_handlechartno()
        {
            string flag = "gethandlechartno";
            DataTable dt = objBAL.getrawmateriallist(flag);
            HandleMasterEntity modelobj = new HandleMasterEntity();
            List<HandleMasterEntity> list = new List<HandleMasterEntity>();
            if (dt.Rows.Count > 0 && dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    HandleMasterEntity obj = new HandleMasterEntity();
                    obj.hm_chart_num = (dt.Rows[i]["hm_chart_num"].ToString());
                    list.Add(obj);
                }
            }
            return list.ToList();
        }
        public List<HandleMasterEntity> gethandlemstr()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "gethandlemstr";
                dt = objBAL.gethandleMasters(flag);
                List<HandleMasterEntity> listfilesize = new List<HandleMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new HandleMasterEntity
                    {
                        hm_handle_id = item.Field<int>("hm_handle_id"),
                        hm_fsize_id = item.Field<string>("hm_fsize_id"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        hm_handle_type = item.Field<string>("hm_handle_type"),
                        hm_handle_subtype = item.Field<string>("hm_handle_subtype"),
                        hm_chart_num = item.Field<string>("hm_chart_num")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HandleMasterEntity> gethandlemstrnoactive()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "gethandlemstrnoactive";
                dt = objBAL.gethandleMasters(flag);
                List<HandleMasterEntity> listfilesize = new List<HandleMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new HandleMasterEntity
                    {
                        hm_handle_id        = item.Field<int>("hm_handle_id"),
                        hm_fsize_id         = item.Field<string>("hm_fsize_id"),
                        ft_ftype_desc       = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc       = item.Field<string>("pm_fstype_desc"),
                        hm_handle_type       = item.Field<string>("hm_handle_type"),
                        hm_handle_subtype = item.Field<string>("hm_handle_subtype"),
                        hm_chart_num        = item.Field<string>("hm_chart_num"),
                        hm_request_type = item.Field<string>("hm_request_type"),
                        hm_request_id = item.Field<int>("hm_request_id"),
                        ReqNo=item.Field<string>("hm_request_type")+item.Field<int>("hm_request_id")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HandleMasterEntity> gethandlemstrnoapprove()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "gethandlemstrnoapprove";
                dt = objBAL.gethandleMasters(flag);
                List<HandleMasterEntity> listfilesize = new List<HandleMasterEntity>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    listfilesize = dt.AsEnumerable().Select(item => new HandleMasterEntity
                    {
                        hm_handle_id = item.Field<int>("hm_handle_id"),
                        hm_fsize_id = item.Field<string>("hm_fsize_id"),
                        ft_ftype_desc = item.Field<string>("ft_ftype_desc"),
                        pm_fstype_desc = item.Field<string>("pm_fstype_desc"),
                        hm_handle_type = item.Field<string>("hm_handle_type"),
                        hm_handle_subtype = item.Field<string>("hm_handle_subtype"),
                        hm_chart_num = item.Field<string>("hm_chart_num"),
                        hm_request_type = item.Field<string>("hm_request_type"),
                        hm_request_id = item.Field<int>("hm_request_id"),
                        ReqNo = item.Field<string>("hm_request_type") + item.Field<int>("hm_request_id")
                    }).ToList();



                }


                return listfilesize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult edithandlermaster(string reqno)
        {
            HandleMasterEntity hm = new HandleMasterEntity();
            string s = reqno;
            string[] values = s.Split(',');
            string chartno = values[0];
            //string boxtype = values[1];
            // pl.pm_pallet_type = boxtype;
            hm.hm_chart_num = chartno;
            hm.handlemstrList = objBAL.gethandleimgpathview(hm.hm_chart_num);
            //objBAL.GetinnerLabelListview(pl.pm_pallet_chartnum, pl.pm_pallet_type);
            // ob.outerboxMasterList = objBAL.GetOuterChartlistview(ob.ob_box_chartnum, ob.ob_box_type);
            return PartialView("_EditHandleMaster", hm);
        }

        [HttpGet]
        public ActionResult GetShowHandelChart(string hm_chart_num)
        {
            HandleMasterEntity hm = new HandleMasterEntity();
            hm.handlemstrList = objBAL.GetShowHandelChart(hm_chart_num);
            return PartialView("_EditHandleMaster", hm);
        }



        #region Add/Edit Stamp-Dhanashree 08.08.2018
        public ActionResult AddNew()
        {
            HandleMasterEntity obj = new HandleMasterEntity();

            obj.listsizecode = get_size();
            obj.listtype = get_type();
            obj.listsubtype = get_subtype();
            obj.listfiletypecode = get_filetypecode();
            obj.listhandletype = get_handletype();
            obj.listhandlesubtype = get_handlesubtype();

            return PartialView("_AddEditNew", obj);
        }

        [HttpGet]
        public JsonResult getHandlesChartNums(HandleMasterEntity objHandle)
        {
            string fileTypeCode = objHandle.hm_ftype_id;
            string FileSubType = objHandle.hm_fstype_id;
            string fileSize = objHandle.hm_fsize_id;
            string handleType = objHandle.hm_handle_type;
            //stampEntity.sm_fcut_id = stampObj.fcut_id;

            objHandle.handleMasterList = objBAL.getHandleMasterList(handleType, fileTypeCode, FileSubType, fileSize);
            if (objHandle.handleMasterList.Count > 0)
            {
                objHandle.hm_chart_num = objHandle.handleMasterList[0].hm_chart_num;
            }else
            {
                objHandle.hm_chart_num = "";
            }

            return Json(objHandle.hm_chart_num, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveNewStamp(HandleMasterEntity objHandle)
        {
            if (ModelState.IsValid)
            {
                OtherRequestMasterEntity Rorobj = new OtherRequestMasterEntity();
                Rorobj.or_effective_dt = DateTime.Now.Date;
                Rorobj.flag = "insertOtherrequestMaters";
                Rorobj.or_request_from = "Handle";
                Rorobj.or_request_keydata = "";
                Rorobj.or_createby = Session["User_Id"].ToString();
                Rorobj.approveby = Session["User_Id"].ToString();
                int i = objBAL.SaveOtherRequest(Rorobj);
                int RequestID = i;



                string flag = "";
                string file = "";
                HttpPostedFileBase photo = Request.Files["imagefield1"];
                if (photo != null)
                {
                    objHandle.hm_chartimg_name = photo.FileName;
                    foreach (string upload in Request.Files)
                    {
                        if (Request.Files[upload].ContentLength == 0) continue;
                        string pathToSave = Server.MapPath("~/ProjectImages/Handle/");
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        file = filename.Replace(filename, objHandle.hm_chart_num);
                        Request.Files[upload].SaveAs(Path.Combine(pathToSave, file + ".jpg"));
                    }

                    objHandle.hm_ftype_id = objHandle.filetype;
                    objHandle.hm_handle_subtype = objHandle.filesubtype;
                    objHandle.hm_fsize_id = objHandle.fsize_code;
                    objHandle.hm_handle_type = objHandle.handletype;
                    objHandle.hm_fstype_id = objHandle.filesubtype;
                    objHandle.hm_chartimg_name = file + ".jpg";

                    objHandle.hm_request_type = "M";
                    objHandle.hm_request_id = RequestID;
                    if (objHandle.sm_new_overwrite == "new")
                    {
                        objHandle.flag = "new";
                        objBAL.AddNewHandle(objHandle);
                    }
                    else if (objHandle.sm_new_overwrite == "exist")
                    {
                        objHandle.flag = "overwrite";
                        objBAL.AddNewHandle(objHandle);
                    }
                    //StampMasterEntity entityobj = new StampMasterEntity();
                    //entityobj.sm_fsize_id = stampObj.fsize_code;
                    //entityobj.sm_ftype_id = stampObj.filetype;
                    //entityobj.sm_fstype_id = stampObj.filesubtype;
                    //entityobj.sm_cust_id = stampObj.costomerName;
                    //entityobj.sm_brand_id = stampObj.bm_brand_name;
                    //entityobj.sm_chart_num = stampObj.sm_chart_num;
                    //entityobj.sm_chartdoc_name = stampObj.sm_chart_num + ".jpg";
                    //entityobj.sm_stamp_mode = stampObj.sm_stamp_mode;
                    //entityobj.sm_chart_remarks = "test";
                    //entityobj.sm_ver_no = 0;
                    //entityobj.sm_createby = Session["UserId"].ToString();
                    //if (stampObj.sm_new_overwrite == "new")
                    //{
                    //    entityobj.sm_flag = "new";
                    //    objBAL.AddNewStamp(entityobj);
                    //}
                    //else
                    //{
                    //    entityobj.sm_flag = "overwrite";
                    //    objBAL.AddNewStamp(entityobj);
                    //}
                }
            }
            return RedirectToAction("Index");
        }

        #endregion
    }
}