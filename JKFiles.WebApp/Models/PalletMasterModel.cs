﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class PalletMasterModel
    {
        public string pm_pallet_chartnum { get; set; }
        public string pm_pallet_type { get; set; }
        public string pm_pallet_chartimg { get; set; }
        public string pm_isApproved { get; set; }
        public string pm_isActive { get; set; }
        public string pm_createby { get; set; }

        public string pm_createdate { get; set; }
        public string pm_approveby { get; set; }
        public string pm_approvedate { get; set; }
        public string pm_verno { get; set; }
        public string pm_request_type { get; set; }
        public string pm_request_id { get; set; }

        public List<PalletDetailsEntity> palletDetailsEntityList { get; set; }

    }
}


      