﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
using System.ComponentModel.DataAnnotations;

namespace JKFiles.WebApp.Models
{
    public class DispatchSKU
    {
        public string dp_request_type { get; set; }
        public int dp_request_id { get; set; }
        public string dp_dispatch_digit { get; set; }
        public string dp_stamp_type { get; set; }
        public string dp_stamp_chart { get; set; }
        [Required(ErrorMessage = "Please Select Handle Presence")]
        public string dp_handle_presence { get; set; }
      
         public string hp_presence_desc { get; set; }
        public string dp_handle_type { get; set; }
        public string dp_handle_chart { get; set; }
        public string dp_handle_subtype { get; set; }
        [Required(ErrorMessage = "Please Select Wrapper Type")]
        public string dp_wrapping_type { get; set; }
        [Required(ErrorMessage = "Please Select Wrapper Chart")]
        public string dp_wrapping_chart { get; set; }
        public string dp_wrapping_recid { get; set; }
        [Required(ErrorMessage = "Please Select Innerbox Type")]
        public string dp_innerbox_type { get; set; }
        [Required(ErrorMessage = "Please Select Innerbox Chart")]
        public string dp_innerbox_chart { get; set; }
        public string dp_innerbox_id { get; set; }
        public string dp_innerlabel_chart { get; set; }
        [Required(ErrorMessage = "Please Select Outer Type")]
        public string dp_outerbox_type { get; set; }
        [Required(ErrorMessage = "Please Select Outer Chart")]
        public string dp_outerbox_chart { get; set; }
        public string dp_outerbox_recid { get; set; }
        [Required(ErrorMessage = "Please Select Pallet Type")]
        public string dp_pallet_type { get; set; }
        [Required(ErrorMessage = "Please Select Pallet Chart")]
        public string dp_pallet_chart { get; set; }

        public string dp_palletdtl_recid { get; set; }
        public int dp_version_no { get; set; }
        public DateTime dp_isApproved { get; set; }
        public Boolean dp_isActive { get; set; }
        public DateTime dp_createdate { get; set; }
        public string dp_createby { get; set; }
        public DateTime dp_approvedate { get; set; }
        public string dp_approveby { get; set; }
        public string dp_innerbox_qty { get; set; }



        public string outerbox_chart { get; set; }
        public string innerbox_chart { get; set; }
        public string pallet_chart { get; set; }
        public string wrapping_chart { get; set; }

        public List<HandlePresenceMasterEntity> handlePresenceList { get; set; }
        public List<HandleMasterEntity> handleMasterList { get; set; }
        public List<HandleMasterEntity> handleSubTypeList { get; set; }

        public List<PackingMaterialMAsterEntity> packingMasterialList { get; set; }
        public List<WrapperMasterEntity> dp_wrapping_chartList { get; set; }
        public List<PackingMaterialMAsterEntity> innerBoxMasterialList { get; set; }
        public List<InnerBoxMasterEntity> qtyInnerBoxList { get; set; }
        public List<InnerBoxMasterEntity> innerBoxChartNUmsList { get; set; }
        public List<OuterBoxMasterEntity> outerChartNumslist { get; set; }
        public List<PackingMaterialMAsterEntity> outerBoxMasterialList { get; set; }
        public List<PackingMaterialMAsterEntity> palletMaterialList { get; set; }
        public List<PalletMasterEntity> palletChartNumslist { get; set; }
        public List<InnerLabelDetailsEntity> innerLabelDetilasList { get; set; }

        public List<MasterReqParModel> ReqParlist { get; set; }

        public List<MasterReqParEntity> ReqParlistEntity { get; set; }

        #region packingDetails Flags by Archana

        public Boolean? dp_qltyseal_flg { get; set; }
        public Boolean? dp_barcode_flg { get; set; }
        public Boolean? dp_barcodeouter_flg { get; set; }
        public Boolean? dp_lineno_flg { get; set; }
        public Boolean? dp_hologram_flg { get; set; }
        public Boolean? dp_prcstkr_flg { get; set; }
        public Boolean? dp_india_flg { get; set; }
        public Boolean? dp_polybag_flg { get; set; }
        public Boolean? dp_silicagel_flg { get; set; }
        public Boolean? dp_fumigation_flg { get; set; }

        public Boolean? dp_promotionalitem_flg { get; set; }
        public string dp_strap { get; set; }
        public Boolean? dp_cellotape_flg { get; set; }
        public Boolean? dp_shrinkwrap_flg { get; set; }
        public Boolean? dp_decicant_flg { get; set; }
        public Boolean? dp_paperwool_flg { get; set; }

        public string temp_qltyseal_flg { get; set; }
        public string temp_barcode_flg { get; set; }
        public string temp_barcodeouter_flg { get; set; }
        public string temp_lineno_flg { get; set; }
        public string temp_hologram_flg { get; set; }
        public string temp_prcstkr_flg { get; set; }
        public string temp_india_flg { get; set; }
        public string temp_polybag_flg { get; set; }
        public string temp_silicagel_flg { get; set; }
        public string temp_fumigation_flg { get; set; }
        public string temp_promotionalitem_flg { get; set; }
        public string temp_strap { get; set; }
        public string temp_cellotape_flg { get; set; }
        public string temp_shrinkwrap_flg { get; set; }
        public string temp_decicant_flg { get; set; }
        public string temp_paperwool_flg { get; set; }

        public string CustomerName { get; set; }
        public string BrandName { get; set; }
        #endregion

       

        #region added by Dhanashree
        public List<HandleDetailsEntity> HandleDetailsEntityList { get; set; }
        public List<WrapperDetailsEntity> WrapperdetailsList { get; set; }
        public List<PalletDetailsEntity> PalletList { get; set; }
        public List<DispatchSKU> modellist { get; set; }
        public List<InnerBoxDetailsEntity> innerboxlist { get; set; }
        public List<OuterChartMasterEntity> OterChartList { get; set; }
        public string ib_chartnum { get; set; }

        public List<DispatchSKU> OuterList { get; set; }
        public List<DispatchSKU> PalletList1 { get; set; }
        #endregion

        //public string wrapping_chart { get; set; }
        //public string innerbox_chart { get; set; }
        //public string outerbox_chart { get; set; }
        //public string pallet_chart { get; set; }
        public string handle_chart { get; set; }
        public string Message { get; set; }

        public string tang_tempering { get; set; }

        public string temp_outer_mrking_flg { get; set; }
        public string dp_outer_mrking_type { get; set; }
        public string dp_outer_mrking_img { get; set; }
        public string temp_pallet_mrking_flg { get; set; }
        public string dp_pallet_mrking_type { get; set; }
        public string dp_pallet_mrking_img { get; set; }
        public Boolean? dp_pono_flg { get; set; }
       
        public string dp_tangcolor { get; set; }
        public Boolean? dp_blko_flg { get; set; }
        public string dp_pkg_remarks { get; set; }
        public string temp_pono_flg { get; set; }
        public string temp_tangcolor { get; set; }
        public string temp_blko_flg { get; set; }
        public string temp_pkg_remarks { get; set; }
    }
}









