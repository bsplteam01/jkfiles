﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class InnerBoxDetilsModel
    {
        public string ibd_box_recid { get; set; }
        public string ibd_pkg_parm_name { get; set; }
        public string ibd_pkg_parm_code { get; set; }
        public string ibd_pkg_parm_dwgvalue { get; set; }
        public string ibd_pkg_parm_scrnvalue { get; set; }
        public string ibd_pkg_isApproved { get; set; }
        public Boolean ibd_pkg_isActive { get; set; }
        public string ibd_pkg_createby { get; set; }
        public DateTime ibd_pkg_createdate { get; set; }
        public string ibd_pkg_approveby { get; set; }
        public DateTime ibd_pkg_approvedate { get; set; }


        public string FileSize { get; set; }
        public string FileType { get; set; }
        public string INNERPACKINGMATERIAL { get; set; }
        public string INNERPACKINGBOXDESIGN { get; set; }
        public string INNERPACKINGNOTE { get; set; }
        public string INNERPACKINGBOXWIDTH { get; set; }
        public string INNERPACKINGBOXHEIGHT { get; set; }
        public string INNERPACKINGBOXLENGTH { get; set; }
        public string INNERPACKINGUNITSTOBEPACKED { get; set; }
        public List<InnerBoxDetailsEntity> InnerBoxDetailsEntityList { get; set; }
        public List<InnerBoxDetailsEntity> innerboxlist { get; set; }
    }
}

