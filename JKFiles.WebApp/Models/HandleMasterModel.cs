﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class HandleMasterModel
    {
        public string hm_chart_num { get; set; }
        public string hm_handle_type { get; set; }
        public string hm_ftype_id { get; set; }
        public string hm_fstype_id { get; set; }
        public string hm_fsize_id { get; set; }
        public string hm_chartimg_name { get; set; }
        public string hm_chart_remarks { get; set; }
        public string hm_isApproved { get; set; }
        public Boolean hm_isActive { get; set; }
        public DateTime hm_createdate { get; set; }
        public string hm_createby { get; set; }
        public DateTime hm_approvedate { get; set; }
        public Boolean hm_approveby { get; set; }
        public int hm_verno { get; set; }
        public string hm_request_type { get; set; }
        public int hm_request_id { get; set; }

        public List<HandleMasterEntity> handleMasterList { get; set; }

        public List<HandleDetailsEntity>handleDetailsList { get; set; }
    }
}




      
     