﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class RawMaterialMasterModel
    {
        public string rm_fsize_id { get; set; }
        public string rm_ftype_id { get; set; }
        public string rm_fstype_id { get; set; }
        public string rm_code { get; set; }
        public string rm_desc { get; set; }
        public string rm_material { get; set; }
        public Decimal rm_wtinkg_perthsnd { get; set; }
        public Decimal rm_netwtingm_persku { get; set; }

        public string filesize { get; set; }
        public string filetype { get; set; }
        public string filesubtype { get; set; }

        public string thickness { get; set; }
        public string width { get; set; }
        public Decimal rm_height { get; set; }
        public Decimal rm_width { get; set; }

        public List<RawMaterialMasterModel> rawmaterialnoapprove { get; set; }
        public List<RawMaterialMasterModel> rawmaterialnoactive { get; set; }
        public List<RawMaterialMasterModel> listtest { get; set; }

        public IEnumerable<RawMaterialMasterModel> listraw { get; set; }
        public List<RawMaterialMasterModel> listrawm { get; set; }
        public List<RawMaterialMasterModel> listrmcode { get; set; }
        public List<RawMaterialMasterModel> listsizecode { get; set; }
        public List<RawMaterialMasterModel> listtype { get; set; }
        public List<RawMaterialMasterModel> listsubtype { get; set; }
        public string Remark { get; set; }
        public int rm_request_id  { get; set; }
        public string rm_request_type { get; set; }
    }
}

