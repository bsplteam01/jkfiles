﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class InnerBoxMasterModel
    {
        public string ib_qty_inbox { get; set; }
        public int ib_box_id { get; set; }
        public string ib_chartnum { get; set; }
    }
}

