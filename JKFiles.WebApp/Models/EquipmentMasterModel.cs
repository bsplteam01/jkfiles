﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class EquipmentMasterModel
    {
        public int equ_id { get; set; }
        public string equ_code { get; set; }
        public string equ_name { get; set; }
        public string equ_type { get; set; }
        public int equ_equipments_per_cluster { get; set; }
        public int equ_clusters { get; set; }
        public int equ_shifts_per_day { get; set; }
        public int equ_downtime { get; set; }
        public string isApproved_flg { get; set; }
        public Boolean isActive_flag { get; set; }
        public DateTime CreateDate { get; set; }
        public string Createby { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Approveby { get; set; }
        public string Remarks { get; set; }
        public string unitname { get; set; }
        public string uni_unit_type { get; set; }
        public string uni_unit_code { get; set; }
        public string uni_unit_name { get; set; }
        public int equ_assetno { get; set; }
        public string ddlequ_type { get; set; }
        public List <EquipmentMasterModel> equipmentlist { get; set; }
        public List <EquipmentMasterModel> equipmenttypelist { get; set; }
        public List <EquipmentMasterModel> UnitDropdownlist { get; set; }
        public List <EquipmentMasterModel> UnitTypeDropdownlist { get; set; }
        public List <EquipmentMasterModel> EquDropdownlist { get; set; }
        public List <EquipmentMasterModel> oprationmasterlist { get; set; }
      

        //Equipment Value Stream

        public int val_equ_id { get; set; }
        public string val_valuestream_code { get; set; }
        public string val_alternate_valuestream_code { get; set; }
        public int val_quantity { get; set; }
        public string val_valuestream_name { get; set; }
        public string equnametype { get; set; }
        public string unitcode { get; set; }
        public int opr_operation_seq { get; set; }
        public string opr_operation_name { get; set; }
        public string equ_unitcode { get; set; }
        public int equ_oprseqno { get; set; }
        public DateTime equ_startdt { get; set; }
        public DateTime equ_enddt { get; set; }
        public string equ_approveby { get; set; }
        public string equ_createby { get; set; }
        public int quantity { get; set; }


        /// tb_asset_allocation

        public int aa_id { get; set; }
        public int aa_assetno { get; set; }
        public string aa_unit_code { get; set; }
        public DateTime aa_startdate { get; set; }
        public DateTime aa_enddate { get; set; }
        public string aa_isApproved { get; set; }
        public bool aa_isActive { get; set; }
        public DateTime aa_createdate { get; set; }
        public string aa_createby { get; set; }
        public DateTime aa_approvedate { get; set; }
        public string aa_approveby { get; set; }
        public string aa_remarks { get; set; }
        public string EquipmentName { get; set; }
        public int aa_operseq_no { get; set; }
        public string aa_main_valstream { get; set; }
        public string stringaa_startdate { get; set; }
        public string stringaa_enddate { get; set; }
        public string VSCode { get; set; }
        public string uni_unit_attachTo { get; set; }
        public int ppm_process_seqno { get; set; }
        public string ppm_operation_name { get; set; }
    }
}

      
      
      
      
      
      
      