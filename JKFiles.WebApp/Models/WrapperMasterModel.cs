﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class WrapperMasterModel
    {
        public int wm_wrapper_id { get; set; }
        public string wm_chart_num { get; set; }
        public string wm_cust_id { get; set; }
        public string wm_brand_id { get; set; }
        public string wm_wrappertype { get; set; }
        public string wm_ftype_id { get; set; }
        public string wm_fstype_id { get; set; }
        public string wm_fsize_id { get; set; }

        public List<WrapperMasterEntity> wrapperMasterEntityList { get; set; }
    }
}

