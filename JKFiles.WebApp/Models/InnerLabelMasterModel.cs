﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class InnerLabelMasterModel
    {
        public string ilb_chart_num { get; set; }

        public List<InnerLabelDetailsEntity> innerLabelDetailsEntoityList { get; set; }
    }

}