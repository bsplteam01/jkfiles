﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
using System.ComponentModel.DataAnnotations;

namespace JKFiles.WebApp.Models
{
    public class StampMasterModel
    {
        public string chart_num { get; set; }
        public string cust_id { get; set; }
        public string brand_id { get; set; }
        public string ftype_id { get; set; }
        public string fstype_id { get; set; }
        public string fsize_id { get; set; }
        public string fcut_id { get; set; }
        [Required(ErrorMessage = "Please Select Stamp Type")]
        public string stamp_mode { get; set; }
        public string chartdoc_name { get; set; }
        public string chart_remarks { get; set; }
        public string isApproved { get; set; }
        public Boolean isActive { get; set; }
        public Byte ver_no { get; set; }
        public DateTime createdate { get; set; }
        public string createby { get; set; }
        public DateTime approvedate { get; set; }
        public string approveby { get; set; }
        public StampDetailsEntity stampDetailsEntity { get; set; }
        public List<StampMasterEntity> stmpModeList { get; set; }

        public List<StampDetailsEntity> stampsDetailsEntityList { get; set; }


        public List<StampMasterModel> stampsModelList { get; set; }
    }
}

    
     
    