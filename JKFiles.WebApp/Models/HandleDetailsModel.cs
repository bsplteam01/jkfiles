﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class HandleDetailsModel
    {
        public int hd_handle_id { get; set; }
        public string hd_handle_recno { get; set; }
        public string hd_parm_name { get; set; }
        public string hd_parm_code { get; set; }
        public string hd_parm_dwgvalue { get; set; }
        public string hd_parm_scrnvalue { get; set; }
        public Boolean hd_parm_isApproved { get; set; }
        public Boolean hd_parm_isActive { get; set; }
        public string hd_parm_createby { get; set; }
        public DateTime hd_parm_createdate { get; set; }
        public string hd_parm_approveby { get; set; }
        public DateTime hd_parm_approvedate { get; set; }

        public string H { get; set; }
        public string H1 { get; set; }
        public string H2 { get; set; }
        public string HoleDIN { get; set; }
        public string LENGTHOFHANDLE { get; set; }
        public string R { get; set; }
        public string R1 { get; set; }
        public string R2 { get; set; }
        public string R3 { get; set; }
        public string SIDER4 { get; set; }
        public string SIDER5 { get; set; }
        public string SideW { get; set; }
        public string SideW1 { get; set; }
        public string SideW2 { get; set; }
        public string SideW3 { get; set; }

        public string HandleImage { get; set; }
        public List<HandleDetailsEntity> HandleDetailsEntityList { get; set; }

        public string tempChartNum { get; set; }
    }
}


     