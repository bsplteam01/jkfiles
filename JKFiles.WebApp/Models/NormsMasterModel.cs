﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class NormsMasterModel
    {
        public string nor_prod_sku { get; set; }
        public int nor_operation_id { get; set; }
        public string nor_unit_id { get; set; }
        public int nor_equ_id { get; set; }
        public int nor_output_norms { get; set; }
        public string isApproved_flg { get; set; }
        public Boolean isActive_flg { get; set; }
        public DateTime CreateDate { get; set; }
        public string Createby { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Approveby { get; set; }
        public string Remarks { get; set; }

        public IEnumerable<NormsMasterEntity> Normslist { get; set; }
        public IEnumerable<NormsMasterEntity> ApprovalNormslist { get; set; }
        public List<NormsMasterModel> NormsProcesslist { get; set; }
        public List<UnitMasterEntity> unitdropdownlist { get; set; }
        public string uni_unit_code { get; set; }
        public string uni_unit_name { get; set; }
        public string uni_unit_type { get; set; }

        public string partno { get; set; }
        public int dp_id { get; set; }
        public string Skupartno { get; set; }

        public int opr_operation_seq { get; set; }
        public string opr_operation_name { get; set; }

        public int equ_id { get; set; }
        public string equ_name { get; set; }
        public string equname { get; set; }
        public string unitname { get; set; }
        public string oprationname { get; set; }
        public string equ_code { get; set; }
        public string equ_type { get; set; }


        public string pr_RequestNo { get; set; }
        public string pr_ftype_code { get; set; }
        public string pr_process_code { get; set; }
        public string pd_flg { get; set; }
        public string aa_unit_code { get; set; }
        public string aa_main_valstream { get; set; }


        /// tb_norms_values

        public string SKU { get; set; }
        public string nm_prod_sku { get; set; }
        public string nm_valuestream { get; set; }
        public string nm_unit_code { get; set; }
        public string nm_opern_name { get; set; }
        public string nm_equ_name { get; set; }
        public string nm_equ_type { get; set; }
        public string nm_norms_value { get; set; }
        public string nm_request_type { get; set; }
        public int nm_request_id { get; set; }

        //tb_dispatch_norms_values
        public string dnm_item_sku { get; set; }
        public string dnm_valuestream { get; set; }
        public string dnm_unit_code { get; set; }
        public string dnm_opern_name { get; set; }
        public string dnm_equ_name { get; set; }
        public string dnm_equ_type { get; set; }
        public string dnm_dnorms_value { get; set; }
        public string dnm_request_type { get; set; }
        public int dnm_request_id { get; set; }
       

    }
}












