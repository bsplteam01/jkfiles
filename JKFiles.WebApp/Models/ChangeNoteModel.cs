﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class ChangeNoteModel
    {      
        public string RequestNumber { get; set; }
        public string CreatedDate { get; set; }
        public string ApprovedDate { get; set; }
        public string Status { get; set; }
        public string PartNo { get; set; }
        public string IsApproved { get; set; }
        public string dis_IsApproved { get; set; }
        public string mstr_IsApproved { get; set; }
        public string disp_mstr_IsApproved { get; set; }
        public string norms_IsApproved { get; set; }

        public string Appr_flag1 { get; set; }
        public string Appr_flag2 { get; set; }
        public string Appr_flag3 { get; set; }
        public string Appr_flag4 { get; set; }
        public string Appr_flag5 { get; set; }

        public string ap_remarks { get; set; }
        public string  ap_createdDate { get; set; }
        
        public string enter_remark { get; set; }
        public string approve_flag { get; set; }
        public string Appr_from { get; set; }
        public string ap_remarks_for { get; set; }
        public string ap_status { get; set; }
        public string ap_createby { get; set; }
        public string ap_User { get; set; }

        public string Description { get; set; }
        public string SAPNo { get; set; }
        public string VerNo { get; set; }
        public string CustpartNo { get; set; }

        public string Name { get; set; }
        public string ApprovalDate { get; set; }
        public string flag { get; set; }

        //added
        public string approve_from { get; set; }

        public List<ChangeNoteModel> listchangenote { get; set; }
        public List<ChangeNoteModel> listchangenoteEngg { get; set; }
        public List<ChangeNoteModel> listinprocess { get; set; }
        public List<ChangeNoteModel> itemgen_approval { get; set; }

        public List<ChangeNoteModel> cust_approvalremarks_sales { get; set; }
        public List<ChangeNoteModel> cust_approvalremarks_engg { get; set; }

        public List<ChangeNoteModel> completedreqlist { get; set; }
        public List<ChangeNoteModel> approvaldateslist { get; set; }
        public List<ApprovalPreparationEntity> approval_list { get; set; }

        #region  part no color by Dhanashree
        public string split1 { get; set; }
        public string split2 { get; set; }
        public string split3 { get; set; }
        #endregion

        public string cust_name { get; set; }
        public string brand_name { get; set; }
        public string size_inches { get; set; }
        public string file_type { get; set; }
        public string cut_type { get; set; }
        public string endDate { get; set; }
        public string ReqType { get; set; }
        public string key_data { get; set; }

    }
}