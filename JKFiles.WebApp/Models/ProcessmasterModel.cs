﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class ProcessmasterModel
    {
        public string pro_process_code { get; set; }
        public int pro_process_seqno { get; set; }
        public string pro_cut_setup { get; set; }
        public string pro_length_setup { get; set; }
        public string pro_type_setup { get; set; }
        public string pro_isApproved { get; set; }
        public Boolean pro_isActive { get; set; }
        public DateTime pro_createdate { get; set; }
        public string pro_createby { get; set; }
        public DateTime pro_approvedate { get; set; }
        public string pro_approveby { get; set; }
        public string FileType { get; set; }
        public string ft_ftype_code { get; set; }
        public int opr_operation_seq { get; set; }
        public string opr_operation_name { get; set; }
        public string flag { get; set; }
        public string ProcessName { get; set; }
        public string pro_filetype { get; set; }
        public string filetype { get; set; }

        public IEnumerable<FileTypeDataEntity> filetTypelist { get; set; }
        public IEnumerable<OperationMasterEntity> opretionlist { get; set; }
        public IEnumerable<FileSubTypeEntity> Processlist { get; set; }
        public IEnumerable<ProductionSKUCutSpecsEntity> processfilelist { get; set; }
        public IEnumerable<ProductionSKUCutSpecsEntity> Approveprocessfilelist { get; set; }
        public IEnumerable<ProcessmasterModel> processtypewiselist { get; set; }
        public IEnumerable<ProcessMasterDetailsEntity> processaprovelist { get; set; }
        public IEnumerable<ProcessMasterDetailsEntity> processCompletelist { get; set; }
        public IEnumerable<ProcessmasterModel> Processmasterlist { get; set; }
        public List<ItemmasterModel> SKUlist { get; set; }
        /// New Profile Process Master

        public int ppm_id { get; set; }
        public string ppm_process_code { get; set; }
        public int ppm_process_seqno { get; set; }
        public string ppm_request_type { get; set; }
        public int ppm_request_id { get; set; }
        public string ppm_ftype_code { get; set; }
        public string ppm_fstype_code { get; set; }
        public string ppm_remarks { get; set; }
        public string ppm_isApproved { get; set; }
        public bool ppm_isActive { get; set; }
        public string ppm_createby { get; set; }
        public DateTime ppm_createdate { get; set; }
        public string ppm_approveby { get; set; }
        public DateTime ppm_approvedate { get; set; }
        public IEnumerable<UnitMasterEntity> unitdropdownlist { get; set; }
        public IEnumerable<EquipmentMasterModel> equipmentlist { get; set; }
        public string uni_unit_code { get; set; }
        public string uni_unit_name { get; set; }
        public int pm_process_seqno { get; set; }
        public string val_unit_code { get; set; }
        public string equ_name { get; set; }
        public string equ_type { get; set; }
        public int equ_assetno { get; set; }

        public string pn_part_no { get; set; }
        public string pn_ftype_code { get; set; }
        public string pn_ftype_desc { get; set; }
        public string ft_process_code { get; set; }
        public string SKU { get; set; }
        public string filetypecode { get; set; }
    }
}

      
      
      
      
      
      
      
      
      
      