﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class UnitMasterModel
    {
        public string uni_unit_code { get; set; }
        public string uni_unit_name { get; set; }
        public string uni_unit_type { get; set; }
        public string uni_unit_location { get; set; }
        public string uni_unit_attachTo { get; set; }
        public int uni_unit_absentisum { get; set; }
        public int uni_unit_material_downtime { get; set; }
        public DateTime uni_createdate { get; set; }
        public string uni_createby { get; set; }
        public string uni_approveby { get; set; }
        public string uni_isApproved { get; set; }
        public Boolean uni_isActive { get; set; }
        public string uni_remarks { get; set; }
        public DateTime uni_approvedate { get; set; }
        public List<UnitMasterModel> unitlist { get; set; }
        public List<UnitMasterModel> unitNormlist { get; set; }
        public List<UnitMasterEntity> unitdropdownlist { get; set; }
        public List<PlantMasterEntity> plantdropdownlist { get; set; }
        public List<UnitMasterEntity> unitvaluestreamlist { get; set; }
        public string val_valuestream_code { get; set; }
        public string val_alternate_valuestream_code { get; set; }
        public string val_unit_code { get; set; }
        public string valueStreamname { get; set; }
        public string valueStramcode { get; set; }
        public string pla_plant_code { get; set; }
        public string pla_plant_name { get; set; }
        public string flag { get; set; }
        public string ft_ftype_code { get; set; }
        public string ft_ftype_desc { get; set; }
    }
}
      
      
      
      
      
      
      
      
      
      
      
      
      
      