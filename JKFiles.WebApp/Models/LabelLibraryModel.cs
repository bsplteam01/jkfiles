﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class LabelLibraryModel
    {
        public string pn_part_no { get; set; }
        public string ReqNo { get; set; }
        public string pn_request_type { get; set; }
        public int pn_request_id { get; set; }
        public string pn_isApproved { get; set; }

        public List<LabelLibraryModel> label_list { get; set; }

        public string pn_fsize_code { get; set; }
        public string pn_ftype_code { get; set; }
        public DateTime pn_start_date { get; set; }
        public DateTime pn_end_date { get; set; }
    }
} 