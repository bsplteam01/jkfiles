﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class OtherRequestMasterModel
    {
        public string or_request_type { get; set; }
        public int or_request_id { get; set; }
        public string or_request_from { get; set; }
        public string or_request_keydata { get; set; }
        public bool or_approver1_flg { get; set; }
        public bool or_approver2_flg { get; set; }
        public bool or_approver3_flg { get; set; }
        public bool or_approver4_flg { get; set; }
        public bool or_approver5_flg { get; set; }
        public DateTime or_approver1_dt { get; set; }
        public DateTime or_approver2_dt { get; set; }
        public DateTime or_approver3_dt { get; set; }
        public DateTime or_approver4_dt { get; set; }
        public DateTime or_approver5_dt { get; set; }
        public string or_remarks { get; set; }
        public bool or_isActive { get; set; }
        public int or_isApproved { get; set; }
        public int or_approveby { get; set; }
        public DateTime or_approvedate { get; set; }
        public int or_createby { get; set; }
        public DateTime or_createdate { get; set; }
    }
}

     
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      