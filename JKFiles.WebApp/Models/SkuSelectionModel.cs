﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class SkuSelectionModel
    {

        [Required(ErrorMessage = "Please select the Costomer Name.")]
        public string costomerName { get; set; }
       
        public string costomerId { get; set; }
        [Required(ErrorMessage = "Please select the Brand Name ")]
        public string brandName { get; set; }
        public string brandId { get; set; }
        [Required(ErrorMessage = "Please select the Country Name ")]
        public string countryName { get; set; }


        public string dp_stamp_chart { get; set; }
        public string dp_wrapping_chart { get; set; }
        public int cm_request_id { get; set; }
        public string pr_uniq_id { get; set; }
        public string dp_handle_chart { get; set; }


        public string rs_sku_code { get; set; }
        public int rs_quantity { get; set; }
      
        public string  pn_request_type { get; set; }
       
        public string pn_part_no { get; set; }
        public DateTime rs_sku_start_date { get; set; }
        public DateTime rs_sku_end_date { get; set; }
        public int rs_request_id { get; set; }


        public List<SkuSelectionModel> ssmskulist { get; set; }
        public List<PartNumMasterEntity> partlist { get; set; }
        public List<PartNumMasterEntity> partlistDetaillist { get; set; }
    }
}