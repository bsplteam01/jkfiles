﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class RequestsModel
    {
        public string req_no { get; set; }
        public string Createdby { get; set; }
        public string CreationDate { get; set; }

        public DateTime date { get; set; }
        public string Status { get; set; }

        public string pr_unique_id { get; set; }

        public List<RequestsModel> RequestLists { get; set; }

        public List<RequestsModel> RequestNoLists { get; set; }
        public List<RequestsModel> CreatebyList { get; set; }
        public List<RequestsModel> StatusList { get; set; }
    }
}