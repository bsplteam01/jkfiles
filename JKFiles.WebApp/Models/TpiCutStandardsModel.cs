﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class TpiCutStandardsModel
    {
        public string fileSizeCode { get; set; }
        public string fileTypeCode { get; set; }
        public string fileSubTypeCode { get; set; }
        public string cutCode { get; set; }
        public string cutStandardCode { get; set; }
        public string cutStandardName { get; set; }
        public string dimensionName { get; set; }
        public string dimensionCode { get; set; }
        public string dimensionDrawingVal { get; set; }
        public Decimal dimensionScreeningVal { get; set; }
        public bool isActive { get; set; }
        public byte versionNo { get; set; }
        public DateTime createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime approvedDate { get; set; }
        public string approvedBy { get; set; }

        public string tpi_cut_application { get; set; }
        public string tpi_dim_name { get; set; }
        public string tpi_dim_code { get; set; }
        public string tpi_dim_scrnval { get; set; }
        public string tpi_dim_dwgval { get; set; }
        public string tpi_fsize_code { get; set; }
        public string tpi_ftype_code { get; set; }
        public string filesubtype { get; set; }
        public int tpi_request_id { get; set; }
        public string tpi_cut_code { get; set; }
        public string tpi_cut_std_name { get; set; }



        public IEnumerable<TpiCutStandardsModel> skulist { get; set; }

        public List<TpiCutStandardsModel> oldvaluelist { get; set; }
        public List<TpiCutStandardsModel> newvaluelist { get; set; }
        public string pn_part_no { get; set; }
        public string pn_fsize_code { get; set; }
        public string pn_ftype_code { get; set; }
        public string pn_fstype_code { get; set; }
        public DateTime pn_start_date { get; set; }
        public DateTime pn_end_date { get; set; }
        public int pn_request_id { get; set; }
        public int pn_version_no { get; set; }
        public string pm_file_code { get; set; }
        public string pm_fsize_code { get; set; }
        public string SkuEditDAte { get; set; }


        public int im_sku_verno { get; set; }
        public string im_sku { get; set; }
        public DateTime im_sku_start_date { get; set; }
        public DateTime im_sku_end_date { get; set; }
        public int im_request_id { get; set; }
        public string cutname { get; set; }
        public string stdname { get; set; }

    }
}

