﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class UserLoginModel
    {
        
        public string FirstName { get; set; }
       
        public string LastName { get; set; }
       
        public string Employee_Id { get; set; }
       
        public string Department { get; set; }
       
        public string User_Id { get; set; }
        public string Credentials { get; set; }
        public string PlantName { get; set; }
        public string RoleName { get; set; }
        public int Plant_Id { get; set; }
        public string Login_Id { get; set; }
        public string OEMId { get; set; }
        public string OEMName { get; set; }
        public string Password { get; set; }
        public string Main_Login_Flag { get; set; }
        public string Report_Login_Flag { get; set; }
        public string decline_reason { get; set; }
        public string Fullname { get { return string.Format("{0} {1}", FirstName, LastName); } }
        public string flag { get; set; }
        public List<UserRoleModel> listrole { get; set; }
        public List<UserRoleModel> listplant { get; set; }

        public List<UserLoginModel> listuser { get; set; }
        public int totaluser { get; set; }
        public int totalactiveuser { get; set; }
        public List<UserLoginModel> menulist { get; set; }

        public List<UsermgmtEntity> menulist1 { get; set; }
        public string MainMenu { get; set; }
        public int SubMenu_Id { get; set; }
        public string SubMenu { get; set; }
        public string Controller { get; set; }
        public string sd_chart_num { get; set; }
        public int MainMenuID { get; set; }
        public string Action { get; set; }
        public int RoleId { get; set; }

    }
}