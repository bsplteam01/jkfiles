﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class ProductionMasterModel
    {
         [Required(ErrorMessage = "Please select the File Size.")]
        public string FileSize { get; set; }
        [Required(ErrorMessage = "Please select the File Type.")]
        public string FileType { get; set; }
        [Required(ErrorMessage = "Please select the Cut Type.")]
        public string CutType { get; set; }
        [Required(ErrorMessage = "Please select the File Sub type.")]
        public string FileSubType { get; set; }
        [Required(ErrorMessage = "Please select the Tang Color.")]       
        public string TangColor { get; set; }
        public string subtype { get; set; }
        public string USL { get; set; }
        public string UEL { get; set; }
        public string UPL { get; set; }
        public string IUP { get; set; }
        public string IOV { get; set; }
        public string IEG { get; set; }
        public string HRC { get; set; }
        public string Filecode { get; set; }
        public string filesizecode { get; set; }
        public string filetypecode { get; set; }
        public string filesubtypecode { get; set; }
        public string cuttypecode { get; set; }
        public string ReqNo { get; set; }

        public List<FileSizeDetailsEntity> filesizelist { get; set; }
        public List<FileTypeDataEntity> filetTypelist { get; set; }
        public List<CutTypeDetailsEntity> CutTypelist { get; set; }
        public List<TangColorDetailsEntity> TangColorlist { get; set; }
        public List<ProfileDetailsEntity> filesubtypelist { get; set; }
        public List<FileSubTypeEntity> filetypesublist { get; set; }

        public List<ProductionMasterModel> modellist { get; set; }
        public decimal? file_size_inches { get; set; }
       
    }
}