﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class CutLocationTpiMasterModel
    {
        public string vtpi_ftype_code { get; set; }
        public string vtpi_fstype_code { get; set; }
        public string vtpi_fsize_code { get; set; }
        public string vtpi_cut_code { get; set; }
        public Byte vtpi_parm_seq_no { get; set; }
        public string vtpi_parm_code { get; set; }
        public string vtpi_parm_value { get; set; }
        public string vtpi_cut_std_code { get; set; }
        public string vtpi_cut_std_name { get; set; }
        public Boolean vtpi_upcut_flg { get; set; }
        public Boolean vtpi_overcut_flg { get; set; }
        public Boolean vtpi_edgecut_flg { get; set; }
        public Boolean vtpi_stamphere_flg { get; set; }
        public string vtpi_cut_application { get; set; }
        public string UPCUT { get; set; }
        public string EDGE { get; set; }
        public string OVERCUT { get; set; }

        public List<CutLocationTpiMasterModel> cutLocationMasterTPIList { get; set; }
    }
}