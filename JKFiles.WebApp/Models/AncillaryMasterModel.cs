﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class AncillaryMasterModel
    {
        public int AncillaryID { get; set; }
        public string Ancillary_Name { get; set; }
        public string Ancillary_Code { get; set; }
        public string Ancillary_Location { get; set; }
        public int PlantID { get; set; }
        public DateTime Ancillary_createdate { get; set; }
        public string Ancillary_createby { get; set; }
        public string Ancillary_approveby { get; set; }
        public Boolean Ancillary_isApproved { get; set; }
        public Boolean Ancillary_isActive { get; set; }
        public string Ancillary_remarks { get; set; }
        public DateTime Ancillary_approvedate { get; set; }
        public string PlantName { get; set; }
        public string plantname { get; set; }
        public string plantcode { get; set; }
        public string ValueStremCode { get; set; }
        public List<AncillaryMasterModel> ancilllarylist { get; set; }
        public List<PlantMasterEntity> plantdropdownlist { get; set; }
    }
}

      
      
      
      
      
      
      
      
      
      
      