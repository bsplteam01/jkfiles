﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
using System.ComponentModel.DataAnnotations;

namespace JKFiles.WebApp.Models
{
    public class CustomerMasterModel
    {
        [Required(ErrorMessage = "Please select the Costomer Name.")]
        public string costomerName { get; set; }
        //  [Required(ErrorMessage = "Please enter the Costomer Name ")]
        public string costomerId { get; set; }
        [Required(ErrorMessage = "Please select the Brand Name ")]
        public string brandName { get; set; }
        public string brandId { get; set; }
        [Required(ErrorMessage = "Please select the Country Name ")]
        public string countryName { get; set; }
        // [Required(ErrorMessage = "Please enter the SAP No. ")]
        public string sapNo { get; set; }
        // [Required(ErrorMessage = "Please enter the Costomer part No. ")]
        public string customerPartNO { get; set; }
        public string countryId { get; set; }
        public int cm_request_id { get; set; }
        public string pr_uniq_id { get; set; }
        public List<CustomerMasterNoApprove> custNoApproveList { get; set; }
        public List<Customersinactive> custNoActiveList { get; set; }
        public List<CustomerDetailsEntity> custList { get; set; }
        public List<BrandDetailsEntity> brandList { get; set; }
        public List<BrandDetailsEntity> brandNoApproveList { get; set; }
        public List<BrandDetailsEntity> brandListNoActiveList { get; set; }
        public List<ShipToCountryEntity> countryList { get; set; }
    }
}