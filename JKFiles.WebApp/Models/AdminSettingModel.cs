﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class AdminSettingModel
    {
        public string flag { get; set; }
        public int Config_Id { get; set; }
        public string OEMId { get; set; }
        public string OEMName { get; set; }
        public string Registration_Key { get; set; }
        public string Parameter_Code { get; set; }
        public string Parameter_Desc { get; set; }
        [Required(ErrorMessage = "Please Add value.")]
        public string Parameter_Value { get; set; }
        [Required(ErrorMessage = "Please Select Operator.")]
        public string Operator { get; set; }
        [Required(ErrorMessage = "Please Select Operation.")]
        public string Operation { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public OperationFactor Operations { get; set; }

        public List<AdminSettingModel> listset { get; set; }
    }


    public enum OperationFactor
    {
        Divide,
        Multiply,
        Addition,
        Subtraction,
        NA
    }

}