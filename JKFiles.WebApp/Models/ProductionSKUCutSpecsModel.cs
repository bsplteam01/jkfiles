﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class ProductionSKUCutSpecsModel
    {
        public string prc_request_type { get; set; }
        public string prc_request_id { get; set; }
        public string prc_parameter_name { get; set; }
        public string prc_parameter_value { get; set; }
        public Boolean prc_upcut_flg { get; set; }
        public Boolean prc_overcut_flg { get; set; }
        public Boolean prc_edgecut_flg { get; set; }
        public DateTime prc_createdate { get; set; }
        public string prc_createby { get; set; }

        public string vtpi_parm_code { get; set; }
        public string vtpi_parm_value { get; set; }
        public string vtpi_cut_std_code { get; set; }
        public string vtpi_cut_std_name { get; set; }
        public Boolean vtpi_upcut_flg { get; set; }
        public Boolean vtpi_overcut_flg { get; set; }
        public Boolean vtpi_edgecut_flg { get; set; }
        public Boolean vtpi_stamphere_flg { get; set; }
        public string vtpi_cut_application { get; set; }
        public string UPCUT { get; set; }
        public string EDGE { get; set; }
        public string OVERCUT { get; set; }

    }
}