﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class StampDetailsModel
    {
        public string sd_chart_num { get; set; }
        public int sd_stampimg_id { get; set; }
        public string sd_parm_name { get; set; }
        public string sd_parm_code { get; set; }
        public string sd_parm_dwgvalue { get; set; }
        public string sd_parm_scrnvalue { get; set; }
        public string sd_is_Approved { get; set; }
        public Boolean sd_is_Active { get; set; }
        public string sd_createby { get; set; }
        public DateTime sd_createdate { get; set; }
        public string sd_approveby { get; set; }
        public DateTime sd_approvedate { get; set; }

        public string Size { get; set; }
        public string Letter_Height_in_mm { get; set; }
        public string letterHeight { get; set; }
        public string orientation { get; set; }
        public string place { get; set; }
        public string side { get; set; }

        public List<StampDetailsEntity> StampDetailsEntityList { get; set; }
        // public List<StampDetailsEntity> stampsDetailsEntityList { get; set; }
        public IEnumerable<StampDetailsModel> stmaplistactive { get; set; }
        public IEnumerable<StampDetailsModel> stmaplistNoApprove { get; set; }

        public IEnumerable<StampDetailsModel> stampsDetailsList { get; set; }
        public IEnumerable<StampDetailsModel> listcst { get; set; }
        public IEnumerable<StampDetailsModel> listbrand { get; set; }
        public List<StampDetailsModel> listchartno { get; set; }
        public string costomerName { get; set; }
        public string costomerId { get; set; }
        public string bm_brand_id { get; set; }
        public string bm_brand_name { get; set; }
        public string ft_ftype_code { get; set; }
        public string pm_fstype_code { get; set; }


        public string File_code { get; set; }
        public string Cut_Type { get; set; }
        public string StampMode { get; set; }
        public string File_type { get; set; }
        public string File_subtype { get; set; }

        public string sd_stampimg { get; set; }





        public List<StampDetailsModel> listsizecode { get; set; }
        public List<StampDetailsModel> listtype { get; set; }
        public List<StampDetailsModel> listsubtype { get; set; }
        public List<StampDetailsModel> listcuttype { get; set; }
        public List<StampDetailsModel> listcstmstr { get; set; }
        public List<StampDetailsModel> listbrandmstr { get; set; }
        public string filesubtype { get; set; }
        public string filetype { get; set; }
        public string fsize_code { get; set; }
        public string filetypecode { get; set; }
        public string Customer { get; set; }
        public string Brand { get; set; }
        public int sm_request_id { get; set; }
        public string sm_request_type { get; set; }
        public string sm_chartdoc_name { get; set; }

        public List<StampDetailsModel> listchartname { get; set; }
        //Added by DhanashreestmpModeList
        public string sm_stamp_mode { get; set; }
        public List<StampDetailsModel> stmpModeList { get; set; }
        public string sm_chart_num { get; set; }
        public string sm_new_overwrite { get; set; }
        public string sm_flag { get; set; }

        public string stamp_size { get; set; }
        public string stamp_location { get; set; }
        public string fontSize { get; set; }
        public string NumofSides { get; set; }
        public string side1img { get; set; }
        public string side2img { get; set; }
        public string side3img { get; set; }
        public string side4img { get; set; }

        public string stamp_id { get; set; }
        public string sm_chart_remarks { get; set; }
        public string ReqNo { get; set; }
    }
}
