﻿using System;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class FileSizeModel
    {
        public int _id { get; set; }
        public decimal fs_size_inches { get; set; }
        public decimal fs_size_mm { get; set; }
        public string fs_size_code { get; set; }
        public string fs_size_remarks { get; set; }
        public string fs_size_isApproved { get; set; }
        public int fs_size_isActive { get; set; }
        public int fs_size_verno { get; set; }
        public string fs_size_createby { get; set; }
        public string fs_size_approveby { get; set; }
        public string fs_request_type { get; set; }
        public int fs_request_id { get; set; }
    }
}
