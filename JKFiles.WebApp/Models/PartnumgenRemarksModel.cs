﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class PartnumgenRemarksModel
    {
        public int prm_id { get; set; }
        public string prm_request_type { get; set; }
        public int prm_request_id { get; set; }
        public string prm_remark_tab { get; set; }
        public string prm_remarks { get; set; }
        public DateTime prm_createdate { get; set; }
        public string prm_createby { get; set; }
        public string Stringcreatedate { get; set; }
        public List<PartnumgenRemarksEntity> remarklist { get; set; }
    }
}
