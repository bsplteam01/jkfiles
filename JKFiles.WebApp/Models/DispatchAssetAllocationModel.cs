﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class DispatchAssetAllocationModel
    {
        public int daa_id { get; set; }
        public int daa_assetno { get; set; }
        public string daa_unit_code { get; set; }
        public int daa_operseq_no { get; set; }
        public DateTime daa_startdate { get; set; }
        public DateTime daa_enddate { get; set; }
        public string daa_main_valstream { get; set; }
        public string daa_alt_valstream { get; set; }
        public string daa_request_type { get; set; }
        public int daa_request_id { get; set; }
        public string daa_isApproved { get; set; }
        public bool daa_isActive { get; set; }
        public DateTime daa_createdate { get; set; }
        public string daa_createby { get; set; }
        public DateTime daa_approvedate { get; set; }
        public string daa_approveby { get; set; }
        public string daa_remarks { get; set; }

    }
}