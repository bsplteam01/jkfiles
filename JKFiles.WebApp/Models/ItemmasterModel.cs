﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class ItemmasterModel
    {
        public string SKU { get; set; }
        public string Cust_sku { get; set; }
        public string costomerName { get; set; }
        public string costomerId { get; set; }
        public string brandName { get; set; }
        public string brandId { get; set; }
        public string FileSize { get; set; }
        public string filesizecode { get; set; }
        public string FileType { get; set; }
        public string filetypecode { get; set; }
        public string CutType { get; set; }
        public string cuttypecode { get; set; }
        public List<ItemmasterModel> SKUlist { get; set; }
        public List<ItemmasterModel> Cust_skulist { get; set; }
        public List<CustomerDetailsEntity> custList { get; set; }
        public List<BrandDetailsEntity> brandList { get; set; }
        public List<FileSizeDetailsEntity> filesizelist { get; set; }
        public List<FileTypeDataEntity> filetTypelist { get; set; }
        public List<CutTypeDetailsEntity> CutTypelist { get; set; }
    }
}