﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class InnerLabelDetailsModel
    {
        public string ilbd_chart_num { get; set; }
        public string ilbd_parm_name { get; set; }
        public string ilbd_parm_scrnvalue { get; set; }

        public string INNERPACKINGTOPLABEL { get; set; }
        public string INNERPACKINGBOTTOMLABEL { get; set; }
        public string INNERPACKINGSIDE1LABEL { get; set; }
        public string INNERPACKINGSIDE2LABEL { get; set; }
        public string INNERPACKINGEND1LABEL { get; set; }
        public string INNERPACKINGEND2LABEL { get; set; }
        public string INNERPACKINGQUALITYSEAL { get; set; }
        public string INNERPACKINGBOXSAMPLE { get; set; }
        public string INNERPACKINGLABELLOCATIONCHARTID { get; set; }
        public string INNERPACKINGBOARDSUPPLIER { get; set; }

        public List<InnerLabelDetailsEntity> innerLabelDetailsEntoityList { get; set; }
    }

}