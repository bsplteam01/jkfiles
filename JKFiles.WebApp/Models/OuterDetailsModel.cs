﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
namespace JKFiles.WebApp.Models
{
    public class OuterDetailsModel
    {
        public int obd_box_id { get; set; }
        public string obd_boxdtl_recid { get; set; }
        public string obd_parm_name { get; set; }
        public string obd_parm_code { get; set; }
        public string obd_parm_dwgvalue { get; set; }
        public string obd_parm_scrnvalue { get; set; }
        public string obd_box_isApproved { get; set; }
        public Boolean obd_box_isActive { get; set; }
        public string obd_box_createby { get; set; }
        public DateTime obd_box_createdate { get; set; }
        public string obd_box_approveby { get; set; }
        public DateTime obd_box_approvedate { get; set; }

        public string filesize { get; set; }
        public string filetype { get; set; }
        public string filesubtype { get; set; }

        public string OUTERPACKMATERIAL { get; set; }
        public string OUTERPACKDESCRIPTION { get; set; }
        public string OUTERPACKFINISH { get; set; }
        public string OUTERPACKAPPLICATION { get; set; }
        public string OUTERPACKLENGTH { get; set; }
        public string OUTERPACKWIDTH { get; set; }
        public string OUTERPACKHEIGHT { get; set; }
        public string OUTERPACKBATTENPOSITION { get; set; }
       

        public List<OuterDetailsEntity> outerDeatialsEntityList { get; set; }
        public List<OuterChartMasterEntity> OterChartList { get; set; }
    }
}

                    