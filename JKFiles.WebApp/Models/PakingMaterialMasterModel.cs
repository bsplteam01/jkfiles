﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class PakingMaterialMasterModel
    {
        public string pkng_usedfor { get; set; }
      
        public string pkng_material { get; set; }
    }
}