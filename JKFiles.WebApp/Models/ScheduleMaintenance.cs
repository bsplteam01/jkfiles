﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
using System.ComponentModel.DataAnnotations;
namespace JKFiles.WebApp.Models
{
    public class ScheduleMaintenance
    {

        public int id { get; set; }
        public string OEMId { get; set; }
        public string OEMName { get; set; }
        public string Start_Date { get; set; }
        public string CurrentDate { get; set; }
        public string CurrentTime { get; set; }
        //public DateTime Start_Date { get; set; }
        public string End_Date { get; set; }
        public string Start_Time { get; set; }
        public string End_Time { get; set; }
        public string Warn_Time { get; set; }
        public string Warn_Date { get; set; }
        public string RoleName { get; set; }
        // public DateTime Warn_Date { get; set; } 
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsSuspend { get; set; }
        public String CreatedDate { get; set; }
        public string sdt { get; set; }
        public string edt { get; set; }
        public string wdt { get; set; }
        public string Maintenance { get; set; }

        public string yearend { get; set; }
        public string SCMCondition { get; set; }
    }
}