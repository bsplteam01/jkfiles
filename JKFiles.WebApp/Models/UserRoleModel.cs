﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using JKFIlesEntity;

namespace JKFiles.WebApp.Models
{
    public class UserRoleModel
    {
        [Required(ErrorMessage = "Please Insert the Role Name.")]
        public string RoleName { get; set; }
        [Required(ErrorMessage = "Please Insert the Role Description.")]
        public string Description { get; set; }
        public int Role_Id { get; set; }

       // [Required(ErrorMessage = "Please Insert the PlantName.")]
        public string PlantName { get; set; }
        public int Plant_Id { get; set; }

        //[Required(ErrorMessage = "Please Insert the First Name.")]
        //public string FirstName { get; set; }
        //[Required(ErrorMessage = "Please Insert the Last Name.")]
        //public string LastName { get; set; }
        //[Required(ErrorMessage = "Please Insert the Employee_Id.")]
        //public string Employee_Id { get; set; }
        //[Required(ErrorMessage = "Please Insert the Department Name.")]
        //public string Department { get; set; }
        //[Required(ErrorMessage = "Please Insert the User_Id.")]
        //public string User_Id { get; set; }
       
        //public string Login_Id { get; set; }
        //public string OEMId { get; set; }
        //public string OEMName { get; set; }
        //public string Password { get; set; }


        public List<UserRoleModel> listrole { get; set; }
        public List<UserRoleModel> listplant { get; set; }
    }
}