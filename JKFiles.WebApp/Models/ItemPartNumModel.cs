﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class ItemPartNumModel
    {
        public int requestId { get; set; }
        public string requestType { get; set; }
        public string itemNum { get; set; }
    }
}