﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class MasterReqParModel
    {
        public List<MasterReqParEntity> ReqParlist { get; set; }
        public List<WrapChartModelEntity> listcstmstr { get; set; }
        public int nrm_id { get; set; }
        public string nrm_for { get; set; }
        public int nrm_seqno { get; set; }
        public string nrm_parameter { get; set; }
        public string nrm_input_type { get; set; }
        public int nrm_numof_inputs { get; set; }
        public string nrm_createby { get; set; }
        public string nrm_createdate { get; set; }

    }
}
