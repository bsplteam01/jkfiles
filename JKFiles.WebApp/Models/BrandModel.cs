﻿using System;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class BrandModel
    {
        public int _id { get; set; }
        public string bm_brand_id { get; set; }
        public string bm_brand_name { get; set; }
        public string bm_brand_remarks { get; set; }
        public string bm_brand_isApproved { get; set; }
        public int bm_brand_isActive { get; set; }
        public int bm_brand_verno { get; set; }
        public string bm_brand_createby { get; set; }
        public string bm_brand_approveby { get; set; }
        public string bm_request_type { get; set; }
        public int bm_request_id { get; set; }
    }
}
