﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class DrawingLibraryModel
    {
        public string dwg_prod_sku { get; set; }
        public string dwg_imgname { get; set; }
        public string dwg_pdfname { get; set; }
        public string dwg_dxfname { get; set; }
        public int dwg_ver_no { get; set; }
        public string dwg_isApproved { get; set; }
        public Boolean dwg_isActive { get; set; }
        public string dwg_remarks { get; set; }
        public DateTime dwg_createdate { get; set; }
        public string dwg_createby { get; set; }
        public DateTime dwg_approvedate { get; set; }
        public string dwg_approveby { get; set; }

        public List<DrawingLibraryModel> listdrawing { get; set; }


        public string req_no { get; set; }
        public string prod_sku { get; set; }
        public List<DrawingLibraryModel> new_request_list { get; set; }
        public List<DrawingLibraryModel> approved_records_list { get; set; }
    }
}