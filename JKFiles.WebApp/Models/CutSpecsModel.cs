﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
using System.ComponentModel.DataAnnotations;
namespace JKFiles.WebApp.Models
{
    public class CutSpecsModel
    {
        public string prc_request_type { get; set; }
        public int prc_request_id { get; set; }
        public string prc_parameter_name { get; set; }
        public string prc_parameter_value { get; set; }
        public Boolean prc_upcut_flg { get; set; }
        public Boolean prc_overcut_flg { get; set; }
        public Boolean prc_edgecut_flg { get; set; }
        public DateTime prc_createdate { get; set; }
        public string prc_createby { get; set; }
        public string vtpi_parm_value { get; set; }
        public string cutStandardCode { get; set; }

        public string pm_dim_parm_dwgvalue { get; set; }
        public decimal pm_dim_parm_scrnvalue { get; set; }

        public string vtpi_cut_std_code { get; set; }
        [Required(ErrorMessage = "Please Select Cut Standard")]
        public string vtpi_cut_std_name { get; set; }

        public List<CutSpecsEntity> CutSpecsList{ get; set; }
        public List<CutLocationTpiMasterModel> CutLocationTpiMasterModelListModel { get; set; }
        public List<CutLocationTpiMasterEntity> CutLocationTpiMasterModelList { get; set; }

        public List<CutLocationTpiMasterEntity> tpiCutStandardList { get; set; }
        //public List<TpiCutStandardsEntity> tpiCutStandardList { get; set; }
        public List< ProductionSKUCutSpecsEntity> prodSkuEntity { get; set; }

        public string UPCUT { get; set; }
        public string EDGE { get; set; }
        public string OVERCUT { get; set; }

        #region added by Dhanashree-for edit 
        public List<CutSpecsModel> cutspeceditlist { get; set; }
        #endregion
    }
}