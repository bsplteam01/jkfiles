﻿using JKFilesBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JKFilesDAL;
using System.Data;
using JKFIlesEntity;

namespace JKFiles.WebApp.Controllers
{
    public class ProductionDetailsController : Controller
    {
        BAL objBAL = new BAL();
        // GET: ProductionDetails
        [ChildActionOnly]
        public ActionResult Index()
        {
            //  getFileSize();
            getFileCutType();
            getTangColor();
            return PartialView("_Index");
        }

        public void getFileSize()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileSize";
                dt = objBAL.getMasters(flag);
                List<FileSpec> listfilespec = new List<FileSpec>();
                if(dt.Rows.Count>0 && dt!=null)
                {
                    for(int i=0;i<dt.Rows.Count;i++)
                    {
                        FileSpec objFileSpec = new FileSpec();
                        objFileSpec.FileSize = dt.Rows[i]["FileSize"].ToString();
                        listfilespec.Add(objFileSpec);
                    }
                }
                ViewBag.FileSize= listfilespec.ToList();
               // TempData["FileSize"] = listfilespec.ToList();

            }catch(Exception ex)
            {
                throw ex;
            }
        }


        public void getFileCutType()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getFileCutType";
                dt = objBAL.getMasters(flag);
                List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FileSpec objFileSpec = new FileSpec();
                        objFileSpec.CutType = dt.Rows[i]["CutType"].ToString();
                        listfilespec.Add(objFileSpec);
                    }
                }
                //ViewBag.CutType = listfilespec.ToList();
                ViewData["CutType"] = listfilespec.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void getTangColor()
        {
            DataTable dt = new DataTable();
            try
            {
                string flag = "getTangColor";
                dt = objBAL.getMasters(flag);
                List<FileSpec> listfilespec = new List<FileSpec>();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FileSpec objFileSpec = new FileSpec();
                        objFileSpec.TangColor = dt.Rows[i]["TangColor"].ToString();
                        listfilespec.Add(objFileSpec);
                    }
                }
                //ViewBag.CutType = listfilespec.ToList();
                ViewData["TangColor"] = listfilespec.ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}