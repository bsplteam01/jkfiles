﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFiles.WebApp.Models
{
    public class CustomerMasterModel
    {
        
        public decimal Cm_Cust_Payer_Code { get; set; }
        public string Cm_Cust_Id { get; set; }
        public string Cm_Cust_Name { get; set; }
        public string Cm_Cust_Remarks { get; set; }
        public bool Cm_Cust_IsApproved { get; set; }
        public bool Cm_Cust_IsActive { get; set; }
        public System.DateTime Cm_Cust_Createdate { get; set; }
        public string Cm_Cust_Createby { get; set; }
        public System.DateTime Cm_Cust_Approvedate { get; set; }
        public string Cm_Cust_Approvedby { get; set; }

        public IEnumerable<CustomerMasterModel> custList { get; set; }
    }
}