//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JKFiles.WebApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class View_Ancillary_Plant_Master
    {
        public string Ancillary_Plant_Name { get; set; }
        public string Ancillary_Plant_Code { get; set; }
        public string Ancillary_Name_Code { get; set; }
        public string MainPlantId { get; set; }
        public System.Guid Id { get; set; }
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
    }
}
