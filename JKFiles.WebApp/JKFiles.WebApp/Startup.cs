﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JKFiles.WebApp.Startup))]
namespace JKFiles.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
