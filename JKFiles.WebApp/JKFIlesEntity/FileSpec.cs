﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class FileSpec
    {
        public string FileSize { get; set; }
        public string FileType { get; set; }
        public string CutType { get; set; }
        public string FileSubType { get; set; }
        public string TangColor { get; set; }
    }
}