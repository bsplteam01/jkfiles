﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class NewMstrReq
    {
        public int ma_id { get; set; }
        public string ma_request_type { get; set; }
        public int ma_request_id { get; set; }
        public string ma_mstrtbl_name { get; set; }
        public string ma_mstrtbl_resp_grp { get; set; }
        public string ma_isApproved_flg { get; set; }
        public string ma_createby { get; set; }
        public string ma_createdate { get; set; }
        public string ma_approveby { get; set; }
        public string ma_approvedate { get; set; }
        public IEnumerable<NewMstrReq> ReqParlist { get; set; }
    }
}
