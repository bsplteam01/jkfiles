﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Coordinate
/// </summary>




public class Coordinate {
    //class for vertex coordinates , used to strore coordinates of profile vertex

    public double V1_x = 40.0;  // Vertex X coordinate
    public double V1_y = 40.0;  // Vertex Y coordinate
    public double V2_x = 40.0;  // Vertex X coordinate
    public double V2_y = 40.0;  // Vertex Y coordinate
    public double V3_x = 40.0;  // Vertex X coordinate
    public double V3_y = 40.0;  // Vertex Y coordinate
    public double V4_x = 40.0;  // Vertex X coordinate
    public double V4_y = 40.0;  // Vertex Y coordinate
    public double V5_x = 40.0;  // Vertex X coordinate
    public double V5_y = 40.0;  // Vertex Y coordinate
    public double V6_x = 40.0;  // Vertex X coordinate
    public double V6_y = 40.0;  // Vertex Y coordinate
    public double V7_x = 40.0;  // Vertex X coordinate
    public double V7_y = 40.0;  // Vertex Y coordinate
    public double V8_x = 40.0;  // Vertex X coordinate
    public double V8_y = 40.0;  // Vertex Y coordinate
    public double V9_x = 40.0;  // Vertex X coordinate
    public double V9_y = 40.0;  // Vertex Y coordinate
    public double V10_x = 40.0;  // Vertex X coordinate
    public double V10_y = 40.0;  // Vertex Y coordinate
    public double V11_x = 40.0;  // Vertex X coordinate
    public double V11_y = 40.0;  // Vertex Y coordinate
    public double V12_x = 40.0;  // Vertex X coordinate
    public double V12_y = 40.0;  // Vertex Y coordinate
    public double V13_x = 40.0;  // Vertex X coordinate
    public double V13_y = 40.0;  // Vertex Y coordinate
    public double V14_x = 40.0;  // Vertex X coordinate
    public double V14_y = 40.0;  // Vertex Y coordinate
    public double V15_x = 40.0;  // Vertex X coordinate
    public double V15_y = 40.0;  // Vertex Y coordinate
    public double V16_x = 40.0;  // Vertex X coordinate
    public double V16_y = 40.0;  // Vertex Y coordinate
    //vertex for horizontal centre line
    public double VL_x = 40.0;  // Vertex X coordinate left side
    public double VL_y = 40.0;  // Vertex Y coordinate left side
    public double VR_x = 40.0;  // Vertex X coordinate right side
    public double VR_y = 40.0;  // Vertex Y coordinate right side
    // SIDE VIEW LEFT TOP VERTEX
    public double VS_x = 40.0;  // Vertex X coordinate 
    public double VS_y = 40.0;  // Vertex y coordinate 
    //-------------------- declear cord A4 templete --------------------------
    public double RMW_dispvalloc_x = 30;
    public double RMW_dispvalloc_y = 173;
    public double RMT_dispvalloc_x = 45;
    public double RMT_dispvalloc_y = 173;
    public double CML_dispvalloc_x = 62;
    public double CML_dispvalloc_y = 173;
    public double BL_dispvalloc_x = 81;
    public double BL_dispvalloc_y = 173;
    public double TL_dispvalloc_x = 104;
    public double TL_dispvalloc_y= 173;
    public double TWS_dispvalloc_x = 120;
    public double TWS_dispvalloc_y = 173;
    public double TTS_dispvalloc_x = 135;
    public double TTS_dispvalloc_y = 173;
    public double TWT_dispvalloc_x = 148;
    public double TWT_dispvalloc_y = 173;
    public double TTT_dimval_x = 158;
    public double TTT_dimval_y = 173;
    public double TPL_dispvalloc_x = 245;
    public double TPL_dispvalloc_y = 173;
    public double FPL_dispvalloc_x = 167;
    public double FPL_dispvalloc_y = 173;
    public double W_dispvalloc_x = 187;
    public double W_dispvalloc_y = 173;
    public double T_dispvalloc_x = 203;
    public double T_dispvalloc_y = 173;
    public double GPL_dispvalloc_x = 218;
    public double GPL_dispvalloc_y = 173;
    public double TTT_dispvalloc_x = 158;
    public double TTT_dispvalloc_y = 173;
    public double L_dispvalloc_x = 270;
    public double L_dispvalloc_y= 173;

    public double IUP_dispvalloc_x = 15;
    public double IUP_dispvalloc_y = 31;
    public double IOV_dispvalloc_x =35;
    public double IOV_dispvalloc_y =31;
    public double IEG_dispvalloc_x = 53;
    public double IEG_dispvalloc_y=31;
    public double HRC_dispvalloc_x =72;
    public double HRC_dispvalloc_y = 69;

    public double CT_dispvalloc_x = 49;
    public double CT_dispvalloc_y = 52;
    public double CSP_dispvalloc_x = 15;
    public double CSP_dispvalloc_y = 52;
    public double UCT_dispvalloc_x= 72;
    public double UCT_dispvalloc_y = 52;
    public double ECT_dispvalloc_x = 97;
    public double ECT_dispvalloc_y = 52;
    public double OCT_dispvalloc_x= 113;
    public double OCT_dispvalloc_y= 52;
    public double FSZ_dispvalloc_x = 15;
    public double FSZ_dispvalloc_y = 173;

    public double PTN_dispvalloc_x = 263;
    public double PTN_dispvalloc_y = 10;
    public double FNM_dispvalloc_x = 220;
    public double FNM_dispvalloc_y= 10;
    public double USL_dispvalloc_x = 75;
    public double USL_dispvalloc_y =31;
    public double UEL_dispvalloc_x=90;
    public double UEL_dispvalloc_y = 31;
    public double RMW1_dispvalloc_x =73;
    public double RMW1_dispvalloc_y= 74;
    public double RMT1_dispvalloc_x =89;
    public double RMT1_dispvalloc_y =74;

    //cordinates for text location for A3 template

    public double RMW_dispvalA3_x = 43;
    public double RMW_dispvalA3_y = 245;
    public double RMT_dispvalA3_x= 63;
    public double RMT_dispvalA3_y= 245;
    public double CML_dispvalA3_x =91;
    public double CML_dispvalA3_y= 245;
    public double BL_dispvalA3_x = 118;
    public double BL_dispvalA3_y = 245;
    public double TL_dispvalA3_x =148;
    public double TL_dispvalA3_y = 245;
    public double TWS_dispvalA3_x= 170;
    public double TWS_dispvalA3_y = 245;
    public double TTS_dispvalA3_x = 187;
    public double TTS_dispvalA3_y = 245;
    public double TWT_dispvalA3_x = 208;
    public double TWT_dispvalA3_y = 245;
    public double TTT_dispvalA3_x = 220;
    public double TTT_dispvalA3_y= 245;
    public double FPL_dispvalA3_x = 240;
    public double FPL_dispvalA3_y = 245;
    public double W_dispvalA3_x = 265;
    public double W_dispvalA3_y = 245;
    public double T_dispvalA3_x = 285;
    public double T_dispvalA3_y = 245;
    public double GPL_dispvalA3_x =305;
    public double GPL_dispvalA3_y =245;
    public double TPL_dispvalA3_x = 340;
    public double TPL_dispvalA3_y = 245;

    public double L_dispvalA3_x = 375;
    public double L_dispvalA3_y= 245;
    public double IUP_dispvalA3_x = 21;
    public double IUP_dispvalA3_y= 44;
    public double IOV_dispvalA3_x =50;
    public double IOV_dispvalA3_y = 44;
    public double IEG_dispvalA3_x = 78;
    public double IEG_dispvalA3_y =44;
    public double HRC_dispvalA3_x = 105;
    public double HRC_dispvalA3_y =97;

    public double CT_dispvalA3_x = 73;
    public double CT_dispvalA3_y = 74;
    public double CSP_dispvalA3_x = 32;
    public double CSP_dispvalA3_y = 74;
    public double UCT_dispvalA3_x = 105;
    public double UCT_dispvalA3_y = 74;
    public double ECT_dispvalA3_x = 134;
    public double ECT_dispvalA3_y = 74;
    public double OCT_dispvalA3_x = 165;
    public double OCT_dispvalA3_y = 74;
    public double FSZ_dispvalA3_x = 20;
    public double FSZ_dispvalA3_y= 245;

    public double PTN_dispvalA3_x = 380;
    public double PTN_dispvalA3_y = 15;
    public double FNM_dispvalA3_x = 310;
    public double FNM_dispvalA3_y = 15;
    public double USL_dispvalA3_x = 110;
    public double USL_dispvalA3_y = 44;
    public double UEL_dispvalA3_x = 145;
    public double UEL_dispvalA3_y = 44;
    public double RMW1_dispvalA3_x = 110;
    public double RMW1_dispvalA3_y = 104;
    public double RMT1_dispvalA3_x = 130;
    public double RMT1_dispvalA3_y = 104;
    
}