﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class StampDetailsEntity
    {
        public string sd_chart_num { get; set; }
        public int sd_stampimg_id { get; set; }
     
        public string stampsize  { get; set; }
        public string stamplocation { get; set; }
        public string Orientation { get; set; }
        public string fontsize    { get; set; }
        public string noofsides   { get; set; }
        public string side1image  { get; set; }
        public string side2image  { get; set; }
        public string side3image { get; set; }
        public string side4image { get; set; }      
        public string sd_parm_name { get; set; }
        public string sd_parm_code { get; set; }
        public string sd_parm_dwgvalue { get; set; }
        public string sd_parm_scrnvalue { get; set; }
        public string sd_is_Approved { get; set; }
        public Boolean sd_is_Active { get; set; }
        public string sd_createby { get; set; }
        public DateTime sd_createdate { get; set; }
        public string sd_approveby { get; set; }
        public DateTime sd_approvedate { get; set; }


        public string Size { get; set; }
        public string Letter_Height_in_mm { get; set; }
        public string letterHeight { get; set; }
        //public string orientation { get; set; }
        public string place { get; set; }
        public string side { get; set; }

        
        public string sm_cust_id    { get; set; }
        public string sm_brand_id   { get; set; }
        public string sm_ftype_id   { get; set; }
        public string sm_fstype_id  { get; set; }
        public string sm_fsize_id   { get; set; }
        public string sm_fcut_id    { get; set; }
        public string sm_stamp_mode { get; set; }
        public string ImageName { get; set; }
        public string request_type { get; set; }
        public int request_id { get; set; }
        public List<StampDetailsEntity> StampDetailsEntityList { get; set; }

        public string sm_chartdoc_name { get; set; }
        public List<StampDetailsEntity> listchartname { get; set; }

        public string Customer { get; set; }
        public string Brand { get; set; }
        public string Cut_Type { get; set; }
        public string StampMode { get; set; }
        public string File_type { get; set; }
        public string File_subtype { get; set; }
        public string sd_stampimg { get; set; }
    }
}