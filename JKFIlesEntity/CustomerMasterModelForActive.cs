﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
namespace JKFiles.WebApp.Entity
{
    public class CustomerMasterModelForActive
    {
        public int _id { get; set; }
        public string Cm_Cust_Id { get; set; }
        public string Cm_Cust_Name { get; set; }
        public string Cm_Cust_Remarks { get; set; }
        public int cm_request_id { get; set; }
        public string cm_request_type { get; set; }
        public IEnumerable<CustomerMasterModelForActive> custModel { get; set; }

    }
}
