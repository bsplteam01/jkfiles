﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class HandlePresenceMasterEntity
    {
        public int hp_presence_code { get; set; }
        public string hp_presence_desc { get; set; }
        public Boolean hp_isApproved { get; set; }
        public Boolean hp_isActive { get; set; }
        public string hp_approveby { get; set; }
        public DateTime hp_approvedate { get; set; }
        public string hp_createby { get; set; }
        public DateTime hp_createdate { get; set; }

        public List<HandlePresenceMasterEntity> handlePresenceList { get; set; }
    }
}