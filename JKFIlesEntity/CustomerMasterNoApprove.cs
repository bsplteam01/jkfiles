﻿using System;
using System.Web;

namespace JKFIlesEntity
{

    public class CustomerMasterNoApprove
    {
        public int _id { get; set; }
        public string Cm_Cust_Id { get; set; }
        public string Cm_Cust_Name { get; set; }
        public string Cm_Cust_Remarks { get; set; }
        public bool Cm_Cust_IsApproved { get; set; }
        public bool Cm_Cust_IsActive { get; set; }
        public System.DateTime Cm_Cust_Createdate { get; set; }
        public string Cm_Cust_Createby { get; set; }
        public System.DateTime Cm_Cust_Approvedate { get; set; }
        public string Cm_Cust_Approvedby { get; set; }
        public int cm_request_id { get; set; }
        public string cm_request_type { get; set; }
    }

}
