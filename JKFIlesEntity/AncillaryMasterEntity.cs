﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class AncillaryMasterEntity
    {
        public int AncillaryID { get; set; }
        public string Ancillary_Name { get; set; }
        public string Ancillary_Code { get; set; }
        public string Ancillary_Location { get; set; }
        public int PlantID { get; set; }
        public DateTime Ancillary_createdate { get; set; }
        public string Ancillary_createby { get; set; }
        public string Ancillary_approveby { get; set; }
        public Boolean Ancillary_isApproved { get; set; }
        public Boolean Ancillary_isActive { get; set; }
        public string Ancillary_remarks { get; set; }
        public string flag { get; set; }
        public DateTime Ancillary_approvedate { get; set; }
    }
}