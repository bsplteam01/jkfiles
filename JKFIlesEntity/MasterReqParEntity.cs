﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class MasterReqParEntity
    {
        public int nrm_id { get; set; }
        public string nrm_for { get; set; }
        public int nrm_seqno { get; set; }
        public string nrm_parameter { get; set; }
        public string nrm_input_type { get; set; }
        public int nrm_numof_inputs { get; set; }
        public string nrm_createby { get; set; }
        public string nrm_createdate { get; set; }
        public string Cm_Cust_Name { get; set; }
        public string Cm_Cust_Id { get; set; }
        public string Cm_Cust_Remarks { get; set; }
        public int nrm_max_length { get; set; }
        public IEnumerable<MasterReqParEntity> ReqParList { get; set; }
    }
}
