﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class FileTypeDataEntity
    {
        public int _id { get; set; }
        public string ft_ftype_code { get; set; }
        public string ft_ftype_desc { get; set; }
        public string ft_ftype_remarks { get; set; }
        public bool ft_ftype_isApproved { get; set; }
        public bool ft_ftype_isActive { get; set; }
        public int ft_ftype_verno { get; set; }
        public DateTime ft_ftype_createddate { get; set; }
        public string ft_ftype_createby { get; set; }
        public DateTime ft_ftype_approvedate { get; set; }
        public string ft_ftype_approveby { get; set; }
        public string ft_ftype_profile_img { get; set; }
        public int ft_request_id { get; set; }
        public string ft_request_type { get; set; }
        public IEnumerable<FileTypeDataEntity> fileTypelist { get; set; }
        public IEnumerable<FileTypeDataEntity> fileTypeNoActiveList { get; set; }
        public IEnumerable<FileTypeDataEntity> fileTypeNoApproveList { get; set; }

        public List<FileTypeDataEntity> listtype { get; set; }

        public string ft_process_code { get; set; }
    }
}