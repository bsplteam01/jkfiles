﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class ProfileDetailsEntity
    {
        public string pm_ftype_code { get; set; }
        public string pm_fstype_code { get; set; }
        public string pm_fstype_desc { get; set; }
        public string pm_fsize_code { get; set; }
        public string pm_dim_parm_catg { get; set; }
        public string pm_dim_parm_name { get; set; }
        public string pm_dim_parm_code { get; set; }
        public string pm_dim_parm_dwgvalue { get; set; }
        public decimal pm_dim_parm_scrnvalue { get; set; }
        public bool pm_dim_parm_isApproved { get; set; }
        public bool pm_dim_parm_isActive { get; set; }
        public int pm_dim_parm_verno { get; set; }
        public DateTime pm_dim_parm_createddate { get; set; }
        public string pm_dim_parm_createby { get; set; }
        public DateTime pm_dim_parm_approvedate { get; set; }
        public string pm_dim_parm_approveby { get; set; }
    }
}