﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class StampMasterEntity
    {
        public string sm_chart_num { get; set; }
        public string sm_cust_id { get; set; }
        public string sm_brand_id { get; set; }
        public string sm_ftype_id { get; set; }
        public string sm_fstype_id { get; set; }
        public string sm_fsize_id { get; set; }
        public string sm_fcut_id { get; set; }
        public string sm_stamp_mode { get; set; }
        public string sm_chartdoc_name { get; set; }
        public string sm_chart_remarks { get; set; }
        public string sm_isApproved { get; set; }
        public Boolean sm_isActive { get; set; }
        public Byte sm_ver_no { get; set; }
        public DateTime sm_createdate { get; set; }
        public string sm_createby { get; set; }
        public DateTime sm_approvedate { get; set; }
        public string sm_approveby { get; set; }

        public List<StampMasterEntity> stmpModeList { get; set; }
        public string sm_new_overwrite { get; set; }
        public string sm_flag { get; set; }
        public string sm_request_type { get; set; }
        public int sm_request_id { get; set; }
        public string sm_stamp_id { get; set; }
    }
}