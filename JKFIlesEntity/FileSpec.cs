﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class FileSpec
    {
        public string FileSize { get; set; }
        public string FileType { get; set; }
        public string CutType { get; set; }
        public string FileSubType { get; set; }
        public string TangColor { get; set; }
        public string filesizecode { get; set; }
        public string filetypecode { get; set; }
        public string flag { get; set; }

        public string Filecode { get; set; }
        public string filesubtypecode { get; set; }
        public string cuttypecode { get; set; }
        public string HRC { get; set; }
        public string USL { get; set; }
        public string UEL { get; set; }
        public string UPL { get; set; }
        public string IUP { get; set; }
        public string IOV { get; set; }
        public string IEG { get; set; }
        public string CustName { get; set; }

        public string ReqType { get; set; }
        public int req_id { get; set; }

        #region Drawing Insertion by Dhanashree
        //For Insertion in Drawing Library
        public string dwg_prod_sku { get; set; }
        public string dwg_imgname { get; set; }
        public string dwg_pdfname { get; set; }
        public string dwg_dxfname { get; set; }
        #endregion


        public string ReqNo { get; set; }
        public string cutstandard { get; set; }
    }
}