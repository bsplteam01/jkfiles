﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class DispatchSKUEntity
    {
        public string dp_request_type { get; set; }
        public int dp_request_id { get; set; }
        public string dp_dispatch_digit { get; set; }
        public string dp_stamp_type { get; set; }
        public string dp_stamp_chart { get; set; }
        public Boolean dp_qltyseal_flg { get; set; }
        public Boolean dp_barcode_flg { get; set; }
        public Boolean dp_barcodeouter_flg { get; set; }
        public Boolean dp_lineno_flg { get; set; }
        public Boolean dp_hologram_flg { get; set; }
        public Boolean dp_prcstkr_flg { get; set; }
        public Boolean dp_india_flg { get; set; }
        public Boolean dp_polybag_flg { get; set; }
        public Boolean dp_silicagel_flg { get; set; }
        public Boolean dp_fumigation_flg { get; set; }

        public Boolean dp_promo_flg { get; set; }
        public string dp_strap_flg { get; set; }
        public Boolean dp_cellotape_flg { get; set; }
        public Boolean dp_shrinkwrap_flg { get; set; }
        public Boolean dp_decicant_flg { get; set; }
        public Boolean dp_paperwool_flg { get; set; }

        public string dp_handle_presence { get; set; }
        public string dp_handle_type { get; set; }
        public string dp_handle_chart { get; set; }
        public string dp_handle_subtype { get; set; }
        public string dp_wrapping_type { get; set; }
        public string dp_wrapping_chart { get; set; }
        public string dp_wrapping_recid { get; set; }
        public string dp_innerbox_type { get; set; }
        public string dp_innerbox_chart { get; set; }

       
        public string dp_innerbox_id { get; set; }
        public string dp_innerlabel_chart { get; set; }
        public string dp_outerbox_type { get; set; }
        public string dp_outerbox_chart { get; set; }
        public string dp_outerbox_recid { get; set; }
        public string dp_pallet_type { get; set; }
        public string dp_pallet_chart { get; set; }
        public string dp_palletdtl_recid { get; set; }
        public int dp_version_no { get; set; }
        public DateTime dp_isApproved { get; set; }
        public Boolean dp_isActive { get; set; }
        public DateTime dp_createdate { get; set; }
        public string dp_createby { get; set; }
        public DateTime dp_approvedate { get; set; }
        public string dp_approveby { get; set; }
        public string dp_innerbox_qty { get; set; }
        public string flag { get; set; }
        public string ib_box_type { get; set; }

        public string handle_chart { get; set; }

        public string dp_outer_mrking_type { get; set; }
        public string dp_outer_mrking_img { get; set; }
        public string dp_pallet_mrking_type { get; set; }
        public string dp_pallet_mrking_img { get; set; }
        public Boolean dp_pono_flg { get; set; }
        public string dp_tangcolor { get; set; }
        public Boolean dp_blko_flg { get; set; }
        public string dp_pkg_remarks { get; set; }
    }
}