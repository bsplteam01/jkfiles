﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class EquipmentMasterEntity
    {
        public int equ_id { get; set; }
        public string equ_code { get; set; }
        public string equ_name { get; set; }
        public string equ_type { get; set; }
        public int equ_equipments_per_cluster { get; set; }
        public int equ_clusters { get; set; }
        public int equ_shifts_per_day { get; set; }
        public int equ_downtime { get; set; }
        public string isApproved_flg { get; set; }
        public Boolean isActive_flag { get; set; }
        public DateTime CreateDate { get; set; }
        public string Createby { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Approveby { get; set; }
        public string Remarks { get; set; }
        public int equ_assetno { get; set; }
        public string flag { get; set; }
        public string equ_unitcode { get; set; }
        public int equ_oprseqno { get; set; }
        public DateTime equ_startdt { get; set; }
        public DateTime equ_enddt { get; set; }
        public string equ_approveby { get; set; }
        public string equ_createby { get; set; }
        public List<EquipmentMasterEntity> unitvaluestreamlist { get; set; }
        public List<EquipmentMasterEntity> unitequipmentlist { get; set; }
        public List<EquipmentMasterEntity> unitequipmenttypelist { get; set; }
        public List<EquipmentMasterEntity> EquipmentAssetlist { get; set; }

        /// tb_asset_allocation

        public int aa_id { get; set; }
        public int aa_assetno { get; set; }
        public string aa_unit_code { get; set; }
        public DateTime aa_startdate { get; set; }
        public DateTime aa_enddate { get; set; }
        public string aa_isApproved { get; set; }
        public bool aa_isActive { get; set; }
        public DateTime aa_createdate { get; set; }
        public string aa_createby { get; set; }
        public DateTime aa_approvedate { get; set; }
        public string aa_approveby { get; set; }
        public string aa_remarks { get; set; }
        public string val_valuestream_code { get; set; }
        public string val_valuestream_name { get; set; }
        public string val_alternate_valuestream_code { get; set; }
        public int aa_operseq_no { get; set; }
        public string aa_main_valstream { get; set; }
        public string aa_alt_valstream { get; set; }
    }
}