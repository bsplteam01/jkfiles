﻿using System;
using System.Web;

namespace JKFIlesEntity
{
    public class CostDetailsModel
    {
      public int  othdt_request_id{get;set;}
      public string othdt_prod_cost_onetm{get;set;}
      public string othdt_prod_cost_run{get;set;}
      public string othdt_dispatch_onetm{get;set;}
      public string othdt_dispatch_run{get;set;}
      public string othdt_req_startdt{get;set;}
      public string othdt_req_enddt{get;set;}
      public string othdt_req_enddt_current_sku { get; set; }
    }
}
