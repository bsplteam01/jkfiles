﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

/// <summary>
/// Summary description for Profile_Manager
/// </summary>
public class Profile_Manager
{



    public static void Flat_profile(Graphics g, Component comp, Profile obj)
    {

        commonfun.Template(g, comp, obj);
        Coordinate cord = new Coordinate();
        // Profile obj = new Profile();
        obj.VST_x = 90;
        obj.VST_y = 80;



        obj.V1_x = obj.VST_x;
        obj.V1_y = obj.VST_y + obj.TWT_dmval / 2;
        obj.V2_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V2_y = obj.VST_y + obj.TWS_dmval / 2;
        obj.V3_x = obj.VST_x + obj.TL_dmval;
        obj.V3_y = obj.VST_y + obj.W_dmval / 2;
        obj.V4_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V4_y = obj.VST_y + obj.W_dmval / 2;
        obj.V5_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V5_y = obj.VST_y + obj.FPL_dmval / 2;
        obj.V6_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V6_y = obj.VST_y - obj.FPL_dmval / 2;
        obj.V7_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V7_y = obj.VST_y - obj.W_dmval / 2;
        obj.V8_x = obj.VST_x + obj.TL_dmval;
        obj.V8_y = obj.VST_y - obj.W_dmval / 2;

        obj.V9_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V9_y = obj.VST_y - obj.TWS_dmval / 2;

        obj.V10_x = obj.VST_x;
        obj.V10_y = obj.VST_y - obj.TWT_dmval / 2;
        obj.VC1_x = obj.VST_x - 20;
        obj.VC1_y = obj.VST_y;
        obj.VC2_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 80;
        obj.VC2_y = obj.VST_y;
        obj.V11_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V11_y = obj.VST_y - obj.W_dmval / 2;
        obj.V12_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V12_y = obj.VST_y - obj.W_dmval / 2;
        obj.V13_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V13_y = obj.VST_y + obj.W_dmval / 2;
        obj.V14_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V14_y = obj.VST_y + obj.W_dmval / 2;

        obj.Vuc1_x = obj.V3_x + 13;
        obj.Vuc1_y = obj.V3_y;
        obj.Vuc2_x = obj.V3_x + 13 - (13 / 2);
        obj.Vuc2_y = obj.V3_y - obj.W_dmval;
        obj.Vuc3_x = obj.V3_x + 13 + (13 / 2);
        obj.Vuc3_y = obj.V3_y - obj.W_dmval;


        //calculate vertex       
        Point pst = new Point((int)(obj.VST_x * gconst.dsc), (int)(obj.VST_y * gconst.dsc));
        Point p1 = new Point((int)(obj.V1_x * gconst.dsc), (int)(obj.V1_y * gconst.dsc));
        Point p2 = new Point((int)(obj.V2_x * gconst.dsc), (int)(obj.V2_y * gconst.dsc));
        Point p3 = new Point((int)(obj.V3_x * gconst.dsc), (int)(obj.V3_y * gconst.dsc));
        Point p4 = new Point((int)(obj.V4_x * gconst.dsc), (int)(obj.V4_y * gconst.dsc));
        Point p5 = new Point((int)(obj.V5_x * gconst.dsc), (int)(obj.V5_y * gconst.dsc));
        Point p6 = new Point((int)(obj.V6_x * gconst.dsc), (int)(obj.V6_y * gconst.dsc));
        Point p7 = new Point((int)(obj.V7_x * gconst.dsc), (int)(obj.V7_y * gconst.dsc));
        Point p8 = new Point((int)(obj.V8_x * gconst.dsc), (int)(obj.V8_y * gconst.dsc));
        Point p9 = new Point((int)(obj.V9_x * gconst.dsc), (int)(obj.V9_y * gconst.dsc));
        Point p10 = new Point((int)(obj.V10_x * gconst.dsc), (int)(obj.V10_y * gconst.dsc));
        Point VC1 = new Point((int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc));
        Point VC2 = new Point((int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
        Point V11 = new Point((int)(obj.V11_x * gconst.dsc), (int)(obj.V11_y * gconst.dsc));
        Point V12 = new Point((int)(obj.V12_x * gconst.dsc), (int)(obj.V12_y * gconst.dsc));
        Point V13 = new Point((int)(obj.V13_x * gconst.dsc), (int)(obj.V13_y * gconst.dsc));
        Point V14 = new Point((int)(obj.V14_x * gconst.dsc), (int)(obj.V14_y * gconst.dsc));
        Point Vuc1 = new Point((int)(obj.Vuc1_x * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
        Point Vuc2 = new Point((int)(obj.Vuc2_x * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));
        Point Vuc3 = new Point((int)(obj.Vuc3_x * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));





        // Create points that define line.
        //Point p1 = new Point((int)(x * gconst.dsc), (int)(y * gconst.dsc));
        //Point p2 = new Point((int)(cord.V1_x * gconst.dsc), (int)(cord.V1_y * gconst.dsc));
        //Draw lines joining above 10 vertices for basic profile
        g.DrawLine(gconst.BPEN, pst, p1);
        g.DrawLine(gconst.BPEN, p1, p2);
        g.DrawLine(gconst.BPEN, p2, p3);
        g.DrawLine(gconst.BPEN, p3, p4);
        g.DrawLine(gconst.BPEN, p4, p5);
        g.DrawLine(gconst.BPEN, p5, p6);
        g.DrawLine(gconst.BPEN, p6, p7);
        g.DrawLine(gconst.BPEN, p7, p8);
        g.DrawLine(gconst.BPEN, p8, p9);
        g.DrawLine(gconst.BPEN, p9, p10);
        g.DrawLine(gconst.BPEN, p10, p1);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc2);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc3);
        g.DrawLine(gconst.BPEN, p3, p8);
        // side view
        g.DrawLine(gconst.BPEN, V11, V12);
        g.DrawLine(gconst.BPEN, V12, V14);
        g.DrawLine(gconst.BPEN, V14, V13);
        g.DrawLine(gconst.BPEN, V11, V13);

        // draw dimensions
        commonfun.hordimtop(g, (int)(obj.V10_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.TL_dimval);
        commonfun.hordimtop(g, (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V6_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.BL_dimval);
        commonfun.hordimtop(g, (int)(obj.V4_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), (int)(obj.V5_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), obj.TPL_dimval);
        commonfun.vertdim(g, (int)((obj.V5_x + 8) * gconst.dsc), (int)((obj.V5_y) * gconst.dsc), (int)((obj.V6_x + 8) * gconst.dsc), (int)((obj.V6_y) * gconst.dsc), obj.FPL_dimval);
        commonfun.vertdim(g, (int)((obj.V1_x - 8) * gconst.dsc), (int)((obj.V1_y) * gconst.dsc), (int)((obj.V10_x - 8) * gconst.dsc), (int)((obj.V10_y) * gconst.dsc), obj.TTS_dimval);
        commonfun.vertdim(g, (int)((obj.V9_x + 3) * gconst.dsc), (int)((obj.V9_y) * gconst.dsc), (int)((obj.V2_x + 3) * gconst.dsc), (int)((obj.V2_y) * gconst.dsc), obj.TWS_dimval);
        // side view dim.

        commonfun.hordimtop(g, (int)(obj.V11_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), (int)(obj.V12_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), obj.T_dimval);
        commonfun.vertdim_withouttext(g, (int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y) * gconst.dsc), (int)((obj.V14_x + 8) * gconst.dsc), (int)((obj.V14_y) * gconst.dsc), obj.W_dimval);
        Point tm = new Point((int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y + obj.W_dimval / 2) * gconst.dsc));
        string st = System.Convert.ToString(obj.W_dimval);
        g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, tm);   // draw text 

        //uncut dim
        commonfun.hordimwithouttext(g, (int)(obj.V3_x * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc), (int)(obj.Vuc1_x * gconst.dsc), (int)((obj.Vuc1_y + 7) * gconst.dsc), 0);
        Point tm1 = new Point((int)((obj.V3_x + 17) * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc));
        g.DrawString("UNCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm1);   // draw text UNCUT 
                                                                       //text upcut
        Point tm2 = new Point((int)(150 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("UPCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm2);   // draw text UPCUT 
        Point tm4 = new Point((int)(155 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm5 = new Point((int)(140 * gconst.dsc), (int)(80 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm4, tm5);
        //text overcut
        Point tm3 = new Point((int)(175 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("OVERCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm3);   // draw text OVERCUT 
        Point tm6 = new Point((int)(180 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm7 = new Point((int)(155 * gconst.dsc), (int)(82 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm6, tm7);
        //hatch pattern for cut
        int i = 1;
        for (i = 1; i < 20; i++)
        {
            Point tmp1 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp2 = new Point((int)((obj.Vuc2_x + 1 * i) * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));

            g.DrawLine(gconst.BPEN, tmp1, tmp2);
            Point tmp3 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp4 = new Point((int)((obj.Vuc3_x + 1 * i) * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
            g.DrawLine(gconst.BPEN, tmp3, tmp4);

        }

        commonfun.cenline(g, (int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc), (int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
    }



    public static void Mil_profile(Graphics g, Component comp, Profile obj)
    {

        commonfun.MilTemplate(g, comp, obj);
        Coordinate cord = new Coordinate();
        //Profile obj = new Profile();
        obj.VST_x = 90;
        obj.VST_y = 80;



        obj.V1_x = obj.VST_x;
        obj.V1_y = obj.VST_y + obj.TWT_dmval / 2;
        obj.V2_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V2_y = obj.VST_y + obj.TWS_dmval / 2;
        obj.V3_x = obj.VST_x + obj.TL_dmval;
        obj.V3_y = obj.VST_y + obj.W_dmval / 2;
        obj.V4_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V4_y = obj.VST_y + obj.W_dmval / 2;
        obj.V5_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V5_y = obj.VST_y + obj.FPL_dmval / 2;
        obj.V6_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V6_y = obj.VST_y - obj.FPL_dmval / 2;
        obj.V7_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V7_y = obj.VST_y - obj.W_dmval / 2;
        obj.V8_x = obj.VST_x + obj.TL_dmval;
        obj.V8_y = obj.VST_y - obj.W_dmval / 2;

        obj.V9_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V9_y = obj.VST_y - obj.TWS_dmval / 2;

        obj.V10_x = obj.VST_x;
        obj.V10_y = obj.VST_y - obj.TWT_dmval / 2;
        obj.VC1_x = obj.VST_x - 20;
        obj.VC1_y = obj.VST_y;
        obj.VC2_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 80;
        obj.VC2_y = obj.VST_y;
        obj.V11_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V11_y = obj.VST_y - obj.W_dmval / 2;
        obj.V12_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V12_y = obj.VST_y - obj.W_dmval / 2;
        obj.V13_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V13_y = obj.VST_y + obj.W_dmval / 2;
        obj.V14_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V14_y = obj.VST_y + obj.W_dmval / 2;

        obj.Vuc1_x = obj.V3_x + 13;
        obj.Vuc1_y = obj.V3_y;
        obj.Vuc2_x = obj.V3_x + 13 - (13 / 2);
        obj.Vuc2_y = obj.V3_y - obj.W_dmval;
        obj.Vuc3_x = obj.V3_x + 13 + (13 / 2);
        obj.Vuc3_y = obj.V3_y - obj.W_dmval;


        //calculate vertex       
        Point pst = new Point((int)(obj.VST_x * gconst.dsc), (int)(obj.VST_y * gconst.dsc));
        Point p1 = new Point((int)(obj.V1_x * gconst.dsc), (int)(obj.V1_y * gconst.dsc));
        Point p2 = new Point((int)(obj.V2_x * gconst.dsc), (int)(obj.V2_y * gconst.dsc));
        Point p3 = new Point((int)(obj.V3_x * gconst.dsc), (int)(obj.V3_y * gconst.dsc));
        Point p4 = new Point((int)(obj.V4_x * gconst.dsc), (int)(obj.V4_y * gconst.dsc));
        Point p5 = new Point((int)(obj.V5_x * gconst.dsc), (int)(obj.V5_y * gconst.dsc));
        Point p6 = new Point((int)(obj.V6_x * gconst.dsc), (int)(obj.V6_y * gconst.dsc));
        Point p7 = new Point((int)(obj.V7_x * gconst.dsc), (int)(obj.V7_y * gconst.dsc));
        Point p8 = new Point((int)(obj.V8_x * gconst.dsc), (int)(obj.V8_y * gconst.dsc));
        Point p9 = new Point((int)(obj.V9_x * gconst.dsc), (int)(obj.V9_y * gconst.dsc));
        Point p10 = new Point((int)(obj.V10_x * gconst.dsc), (int)(obj.V10_y * gconst.dsc));
        Point VC1 = new Point((int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc));
        Point VC2 = new Point((int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
        Point V11 = new Point((int)(obj.V11_x * gconst.dsc), (int)(obj.V11_y * gconst.dsc));
        Point V12 = new Point((int)(obj.V12_x * gconst.dsc), (int)(obj.V12_y * gconst.dsc));
        Point V13 = new Point((int)(obj.V13_x * gconst.dsc), (int)(obj.V13_y * gconst.dsc));
        Point V14 = new Point((int)(obj.V14_x * gconst.dsc), (int)(obj.V14_y * gconst.dsc));
        Point Vuc1 = new Point((int)(obj.Vuc1_x * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
        Point Vuc2 = new Point((int)(obj.Vuc2_x * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));
        Point Vuc3 = new Point((int)(obj.Vuc3_x * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));





        // Create points that define line.
        //Point p1 = new Point((int)(x * gconst.dsc), (int)(y * gconst.dsc));
        //Point p2 = new Point((int)(cord.V1_x * gconst.dsc), (int)(cord.V1_y * gconst.dsc));
        //Draw lines joining above 10 vertices for basic profile
        g.DrawLine(gconst.BPEN, pst, p1);
        g.DrawLine(gconst.BPEN, p1, p2);
        g.DrawLine(gconst.BPEN, p2, p3);
        g.DrawLine(gconst.BPEN, p3, p4);
        g.DrawLine(gconst.BPEN, p4, p5);
        g.DrawLine(gconst.BPEN, p5, p6);
        g.DrawLine(gconst.BPEN, p6, p7);
        g.DrawLine(gconst.BPEN, p7, p8);
        g.DrawLine(gconst.BPEN, p8, p9);
        g.DrawLine(gconst.BPEN, p9, p10);
        g.DrawLine(gconst.BPEN, p10, p1);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc2);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc3);
        g.DrawLine(gconst.BPEN, p3, p8);
        // side view
        g.DrawLine(gconst.BPEN, V11, V12);
        g.DrawLine(gconst.BPEN, V12, V14);
        g.DrawLine(gconst.BPEN, V14, V13);
        g.DrawLine(gconst.BPEN, V11, V13);

        // draw dimensions
        commonfun.hordimtop(g, (int)(obj.V10_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.TL_dimval);
        commonfun.hordimtop(g, (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V6_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.BL_dimval);
        commonfun.hordimtop(g, (int)(obj.V4_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), (int)(obj.V5_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), obj.TPL_dimval);
        commonfun.vertdim(g, (int)((obj.V5_x + 8) * gconst.dsc), (int)((obj.V5_y) * gconst.dsc), (int)((obj.V6_x + 8) * gconst.dsc), (int)((obj.V6_y) * gconst.dsc), obj.FPL_dimval);
        commonfun.vertdim(g, (int)((obj.V1_x - 8) * gconst.dsc), (int)((obj.V1_y) * gconst.dsc), (int)((obj.V10_x - 8) * gconst.dsc), (int)((obj.V10_y) * gconst.dsc), obj.TTS_dimval);
        commonfun.vertdim(g, (int)((obj.V9_x + 3) * gconst.dsc), (int)((obj.V9_y) * gconst.dsc), (int)((obj.V2_x + 3) * gconst.dsc), (int)((obj.V2_y) * gconst.dsc), obj.TWS_dimval);
        // side view dim.

        commonfun.hordimtop(g, (int)(obj.V11_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), (int)(obj.V12_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), obj.T_dimval);
        commonfun.vertdim_withouttext(g, (int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y) * gconst.dsc), (int)((obj.V14_x + 8) * gconst.dsc), (int)((obj.V14_y) * gconst.dsc), obj.W_dimval);
        Point tm = new Point((int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y + obj.W_dimval / 2) * gconst.dsc));
        string st = System.Convert.ToString(obj.W_dimval);
        g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, tm);   // draw text 

        //uncut dim
        commonfun.hordimwithouttext(g, (int)(obj.V3_x * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc), (int)(obj.Vuc1_x * gconst.dsc), (int)((obj.Vuc1_y + 7) * gconst.dsc), 0);
        Point tm1 = new Point((int)((obj.V3_x + 17) * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc));
        g.DrawString("UNCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm1);   // draw text UNCUT 
                                                                       //text upcut
        Point tm2 = new Point((int)(150 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("UPCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm2);   // draw text UPCUT 
        Point tm4 = new Point((int)(155 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm5 = new Point((int)(140 * gconst.dsc), (int)(80 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm4, tm5);
        //text overcut
        Point tm3 = new Point((int)(175 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("OVERCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm3);   // draw text OVERCUT 
        Point tm6 = new Point((int)(180 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm7 = new Point((int)(155 * gconst.dsc), (int)(82 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm6, tm7);
        //hatch pattern for cut
        int i = 1;
        for (i = 1; i < 20; i++)
        {
            Point tmp1 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp2 = new Point((int)((obj.Vuc2_x + 1 * i) * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));

            g.DrawLine(gconst.BPEN, tmp1, tmp2);
            Point tmp3 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp4 = new Point((int)((obj.Vuc3_x + 1 * i) * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
            g.DrawLine(gconst.BPEN, tmp3, tmp4);

        }

        commonfun.cenline(g, (int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc), (int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
    }


    public static void HalfRound_profile(Graphics g, Component comp, Profile obj)
    {

        commonfun.HalfRoundTemplate(g, comp, obj);
        Coordinate cord = new Coordinate();

        obj.VST_x = 90;
        obj.VST_y = 80;



        obj.V1_x = obj.VST_x;
        obj.V1_y = obj.VST_y + obj.TWT_dmval / 2;
        obj.V2_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V2_y = obj.VST_y + obj.TWS_dmval / 2;
        obj.V3_x = obj.VST_x + obj.TL_dmval;
        obj.V3_y = obj.VST_y + obj.W_dmval / 2;
        obj.V4_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V4_y = obj.VST_y + obj.W_dmval / 2;
        obj.V5_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V5_y = obj.VST_y + obj.FPL_dmval / 2;
        obj.V6_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V6_y = obj.VST_y - obj.FPL_dmval / 2;
        obj.V7_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V7_y = obj.VST_y - obj.W_dmval / 2;
        obj.V8_x = obj.VST_x + obj.TL_dmval;
        obj.V8_y = obj.VST_y - obj.W_dmval / 2;

        obj.V9_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V9_y = obj.VST_y - obj.TWS_dmval / 2;

        obj.V10_x = obj.VST_x;
        obj.V10_y = obj.VST_y - obj.TWT_dmval / 2;
        obj.VC1_x = obj.VST_x - 20;
        obj.VC1_y = obj.VST_y;
        obj.VC2_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 80;
        obj.VC2_y = obj.VST_y;
        obj.V11_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V11_y = obj.VST_y - obj.W_dmval / 2;
        obj.V12_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V12_y = obj.VST_y - obj.W_dmval / 2;
        obj.V13_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V13_y = obj.VST_y + obj.W_dmval / 2;
        obj.V14_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V14_y = obj.VST_y + obj.W_dmval / 2;

        obj.Vuc1_x = obj.V3_x + 13;
        obj.Vuc1_y = obj.V3_y;
        obj.Vuc2_x = obj.V3_x + 13 - (13 / 2);
        obj.Vuc2_y = obj.V3_y - obj.W_dmval;
        obj.Vuc3_x = obj.V3_x + 13 + (13 / 2);
        obj.Vuc3_y = obj.V3_y - obj.W_dmval;


        //calculate vertex       
        Point pst = new Point((int)(obj.VST_x * gconst.dsc), (int)(obj.VST_y * gconst.dsc));
        Point p1 = new Point((int)(obj.V1_x * gconst.dsc), (int)(obj.V1_y * gconst.dsc));
        Point p2 = new Point((int)(obj.V2_x * gconst.dsc), (int)(obj.V2_y * gconst.dsc));
        Point p3 = new Point((int)(obj.V3_x * gconst.dsc), (int)(obj.V3_y * gconst.dsc));
        Point p4 = new Point((int)(obj.V4_x * gconst.dsc), (int)(obj.V4_y * gconst.dsc));
        Point p5 = new Point((int)(obj.V5_x * gconst.dsc), (int)(obj.V5_y * gconst.dsc));
        Point p6 = new Point((int)(obj.V6_x * gconst.dsc), (int)(obj.V6_y * gconst.dsc));
        Point p7 = new Point((int)(obj.V7_x * gconst.dsc), (int)(obj.V7_y * gconst.dsc));
        Point p8 = new Point((int)(obj.V8_x * gconst.dsc), (int)(obj.V8_y * gconst.dsc));
        Point p9 = new Point((int)(obj.V9_x * gconst.dsc), (int)(obj.V9_y * gconst.dsc));
        Point p10 = new Point((int)(obj.V10_x * gconst.dsc), (int)(obj.V10_y * gconst.dsc));
        Point VC1 = new Point((int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc));
        Point VC2 = new Point((int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
        Point V11 = new Point((int)(obj.V11_x * gconst.dsc), (int)(obj.V11_y * gconst.dsc));
        Point V12 = new Point((int)(obj.V12_x * gconst.dsc), (int)(obj.V12_y * gconst.dsc));
        Point V13 = new Point((int)(obj.V13_x * gconst.dsc), (int)(obj.V13_y * gconst.dsc));
        Point V14 = new Point((int)(obj.V14_x * gconst.dsc), (int)(obj.V14_y * gconst.dsc));
        Point Vuc1 = new Point((int)(obj.Vuc1_x * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
        Point Vuc2 = new Point((int)(obj.Vuc2_x * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));
        Point Vuc3 = new Point((int)(obj.Vuc3_x * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));





        // Create points that define line.
        //Point p1 = new Point((int)(x * gconst.dsc), (int)(y * gconst.dsc));
        //Point p2 = new Point((int)(cord.V1_x * gconst.dsc), (int)(cord.V1_y * gconst.dsc));
        //Draw lines joining above 10 vertices for basic profile
        g.DrawLine(gconst.BPEN, pst, p1);
        g.DrawLine(gconst.BPEN, p1, p2);
        g.DrawLine(gconst.BPEN, p2, p3);
        g.DrawLine(gconst.BPEN, p3, p4);
        g.DrawLine(gconst.BPEN, p4, p5);
        g.DrawLine(gconst.BPEN, p5, p6);
        g.DrawLine(gconst.BPEN, p6, p7);
        g.DrawLine(gconst.BPEN, p7, p8);
        g.DrawLine(gconst.BPEN, p8, p9);
        g.DrawLine(gconst.BPEN, p9, p10);
        g.DrawLine(gconst.BPEN, p10, p1);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc2);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc3);
        g.DrawLine(gconst.BPEN, p3, p8);
        // side view
        g.DrawLine(gconst.BPEN, V11, V13);
        Rectangle rect = new Rectangle((int)((obj.V11_x - 3) * gconst.dsc), (int)(obj.V11_y * gconst.dsc), 30, 50);
        g.DrawArc(gconst.BPEN, rect, 270, 180);

        // draw dimensions
        commonfun.hordimtop(g, (int)(obj.V10_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.TL_dimval);
        commonfun.hordimtop(g, (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V6_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.BL_dimval);
        commonfun.hordimtop(g, (int)(obj.V4_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), (int)(obj.V5_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), obj.TPL_dimval);
        commonfun.vertdim(g, (int)((obj.V5_x + 8) * gconst.dsc), (int)((obj.V5_y) * gconst.dsc), (int)((obj.V6_x + 8) * gconst.dsc), (int)((obj.V6_y) * gconst.dsc), obj.FPL_dimval);
        commonfun.vertdim(g, (int)((obj.V1_x - 8) * gconst.dsc), (int)((obj.V1_y) * gconst.dsc), (int)((obj.V10_x - 8) * gconst.dsc), (int)((obj.V10_y) * gconst.dsc), obj.TTS_dimval);
        commonfun.vertdim(g, (int)((obj.V9_x + 3) * gconst.dsc), (int)((obj.V9_y) * gconst.dsc), (int)((obj.V2_x + 3) * gconst.dsc), (int)((obj.V2_y) * gconst.dsc), obj.TWS_dimval);
        // side view dim.

        commonfun.hordimtop(g, (int)(obj.V11_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), (int)(obj.V12_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), obj.T_dimval);
        commonfun.vertdim_withouttext(g, (int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y) * gconst.dsc), (int)((obj.V14_x + 8) * gconst.dsc), (int)((obj.V14_y) * gconst.dsc), obj.W_dimval);
        Point tm = new Point((int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y + obj.W_dimval / 2) * gconst.dsc));
        string st = System.Convert.ToString(obj.W_dimval);
        g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, tm);   // draw text 

        //uncut dim
        commonfun.hordimwithouttext(g, (int)(obj.V3_x * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc), (int)(obj.Vuc1_x * gconst.dsc), (int)((obj.Vuc1_y + 7) * gconst.dsc), 0);
        Point tm1 = new Point((int)((obj.V3_x + 17) * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc));
        g.DrawString("UNCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm1);   // draw text UNCUT 
                                                                       //text upcut
        Point tm2 = new Point((int)(150 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("UPCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm2);   // draw text UPCUT 
        Point tm4 = new Point((int)(155 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm5 = new Point((int)(140 * gconst.dsc), (int)(80 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm4, tm5);
        //text overcut
        Point tm3 = new Point((int)(175 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("OVERCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm3);   // draw text OVERCUT 
        Point tm6 = new Point((int)(180 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm7 = new Point((int)(155 * gconst.dsc), (int)(82 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm6, tm7);
        //hatch pattern for cut
        int i = 1;
        for (i = 1; i < 20; i++)
        {
            Point tmp1 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp2 = new Point((int)((obj.Vuc2_x + 1 * i) * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));

            g.DrawLine(gconst.BPEN, tmp1, tmp2);
            Point tmp3 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp4 = new Point((int)((obj.Vuc3_x + 1 * i) * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
            g.DrawLine(gconst.BPEN, tmp3, tmp4);

        }

        commonfun.cenline(g, (int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc), (int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
    }
    
    public static void Round_profile(Graphics g, Component comp, Profile obj)
    {

        commonfun.RoundTemplate(g, comp, obj);
        Coordinate cord = new Coordinate();

        obj.VST_x = 70;
        obj.VST_y = 80;



        obj.V1_x = obj.VST_x;
        obj.V1_y = obj.VST_y + obj.TWT_dmval / 2;
        obj.V2_x = obj.VST_x + obj.TL_dmval - (obj.D_dimval_R / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V2_y = obj.VST_y + obj.TWS_dmval / 2;
        obj.V3_x = obj.VST_x + obj.TL_dmval;
        obj.V3_y = obj.VST_y + obj.D_dimval_R / 2;
        obj.V4_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V4_y = obj.VST_y + obj.D_dimval_R / 2;
        obj.V5_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V5_y = obj.VST_y + obj.GPL_dmval / 2;
        obj.V6_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V6_y = obj.VST_y - obj.GPL_dmval / 2;
        obj.V7_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V7_y = obj.VST_y - obj.D_dimval_R / 2;
        obj.V8_x = obj.VST_x + obj.TL_dmval;
        obj.V8_y = obj.VST_y - obj.D_dimval_R / 2;

        obj.V9_x = obj.VST_x + obj.TL_dmval - (obj.D_dimval_R / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V9_y = obj.VST_y - obj.TWS_dmval / 2;

        obj.V10_x = obj.VST_x;
        obj.V10_y = obj.VST_y - obj.TWT_dmval / 2;
        obj.VC1_x = obj.VST_x - 20;
        obj.VC1_y = obj.VST_y;
        obj.VC2_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 80;
        obj.VC2_y = obj.VST_y;
        obj.V11_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.D_dimval_R / 2;
        obj.V11_y = obj.VST_y;
        obj.V12_x = obj.V11_x - obj.D_dimval_R;
        obj.V12_y = obj.VST_y;
        obj.V13_x = obj.V11_x + obj.D_dimval_R;
        obj.V13_y = obj.VST_y;
        obj.V14_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.T_dmval;
        obj.V14_y = obj.VST_y + obj.D_dimval_R;

        obj.Vuc1_x = obj.V3_x + 13;
        obj.Vuc1_y = obj.V3_y;
        obj.Vuc2_x = obj.V3_x + 13 - (13 / 2);
        obj.Vuc2_y = obj.V3_y - obj.D_dimval_R;
        obj.Vuc3_x = obj.V3_x + 13 + (13 / 2);
        obj.Vuc3_y = obj.V3_y - obj.D_dimval_R;


        //calculate vertex       
        Point pst = new Point((int)(obj.VST_x * gconst.dsc), (int)(obj.VST_y * gconst.dsc));
        Point p1 = new Point((int)(obj.V1_x * gconst.dsc), (int)(obj.V1_y * gconst.dsc));
        Point p2 = new Point((int)(obj.V2_x * gconst.dsc), (int)(obj.V2_y * gconst.dsc));
        Point p3 = new Point((int)(obj.V3_x * gconst.dsc), (int)(obj.V3_y * gconst.dsc));
        Point p4 = new Point((int)(obj.V4_x * gconst.dsc), (int)(obj.V4_y * gconst.dsc));
        Point p5 = new Point((int)(obj.V5_x * gconst.dsc), (int)(obj.V5_y * gconst.dsc));
        Point p6 = new Point((int)(obj.V6_x * gconst.dsc), (int)(obj.V6_y * gconst.dsc));
        Point p7 = new Point((int)(obj.V7_x * gconst.dsc), (int)(obj.V7_y * gconst.dsc));
        Point p8 = new Point((int)(obj.V8_x * gconst.dsc), (int)(obj.V8_y * gconst.dsc));
        Point p9 = new Point((int)(obj.V9_x * gconst.dsc), (int)(obj.V9_y * gconst.dsc));
        Point p10 = new Point((int)(obj.V10_x * gconst.dsc), (int)(obj.V10_y * gconst.dsc));
        Point VC1 = new Point((int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc));
        Point VC2 = new Point((int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
        Point V11 = new Point((int)(obj.V11_x * gconst.dsc), (int)(obj.V11_y * gconst.dsc));
        Point V12 = new Point((int)(obj.V12_x * gconst.dsc), (int)(obj.V12_y * gconst.dsc));
        Point V13 = new Point((int)(obj.V13_x * gconst.dsc), (int)(obj.V13_y * gconst.dsc));
        Point V14 = new Point((int)(obj.V14_x * gconst.dsc), (int)(obj.V14_y * gconst.dsc));
        Point Vuc1 = new Point((int)(obj.Vuc1_x * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
        Point Vuc2 = new Point((int)(obj.Vuc2_x * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));
        Point Vuc3 = new Point((int)(obj.Vuc3_x * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));





        // Create points that define line.
        //Point p1 = new Point((int)(x * gconst.dsc), (int)(y * gconst.dsc));
        //Point p2 = new Point((int)(cord.V1_x * gconst.dsc), (int)(cord.V1_y * gconst.dsc));
        //Draw lines joining above 10 vertices for basic profile
        g.DrawLine(gconst.BPEN, pst, p1);
        g.DrawLine(gconst.BPEN, p1, p2);
        g.DrawLine(gconst.BPEN, p2, p3);
        g.DrawLine(gconst.BPEN, p3, p4);
        g.DrawLine(gconst.BPEN, p4, p5);
        g.DrawLine(gconst.BPEN, p5, p6);
        g.DrawLine(gconst.BPEN, p6, p7);
        g.DrawLine(gconst.BPEN, p7, p8);
        g.DrawLine(gconst.BPEN, p8, p9);
        g.DrawLine(gconst.BPEN, p9, p10);
        g.DrawLine(gconst.BPEN, p10, p1);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc2);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc3);
        g.DrawLine(gconst.BPEN, p3, p8);
        // side view

        g.DrawEllipse(gconst.BPEN, (int)((obj.V11_x - obj.D_dimval_R) * gconst.dsc), (int)((obj.V11_y - obj.D_dimval_R / 2) * gconst.dsc), (int)(obj.D_dimval_R * gconst.dsc), (int)(obj.D_dimval_R * gconst.dsc));
        // draw dimensions
        commonfun.hordimtop(g, (int)(obj.V10_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.TL_dimval);
        commonfun.hordimtop(g, (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V6_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.BL_dimval);
        commonfun.hordimtop(g, (int)(obj.V4_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), (int)(obj.V5_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), obj.TPL_dimval);
        commonfun.vertdim(g, (int)((obj.V5_x + 8) * gconst.dsc), (int)((obj.V5_y) * gconst.dsc), (int)((obj.V6_x + 8) * gconst.dsc), (int)((obj.V6_y) * gconst.dsc), obj.GPL_dimval);
        commonfun.vertdim(g, (int)((obj.V1_x - 8) * gconst.dsc), (int)((obj.V1_y) * gconst.dsc), (int)((obj.V10_x - 8) * gconst.dsc), (int)((obj.V10_y) * gconst.dsc), obj.TTS_dimval);
        commonfun.vertdim(g, (int)((obj.V9_x + 3) * gconst.dsc), (int)((obj.V9_y) * gconst.dsc), (int)((obj.V2_x + 3) * gconst.dsc), (int)((obj.V2_y) * gconst.dsc), obj.TWS_dimval);
        // side view dim.

        commonfun.hordimtop(g, (int)(obj.V11_x * gconst.dsc), (int)((obj.V12_y + 5) * gconst.dsc), (int)(obj.V12_x * gconst.dsc), (int)((obj.V12_y + 5) * gconst.dsc), obj.D_dimval_R);


        //uncut dim
        commonfun.hordimwithouttext(g, (int)(obj.V3_x * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc), (int)(obj.Vuc1_x * gconst.dsc), (int)((obj.Vuc1_y + 7) * gconst.dsc), 0);
        Point tm1 = new Point((int)((obj.V3_x + 17) * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc));
        g.DrawString("UNCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm1);   // draw text UNCUT 
        //text upcut
        Point tm2 = new Point((int)(150 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("UPCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm2);   // draw text UPCUT 
        Point tm4 = new Point((int)(155 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm5 = new Point((int)(140 * gconst.dsc), (int)(80 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm4, tm5);
        //text overcut
        Point tm3 = new Point((int)(175 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("OVERCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm3);   // draw text OVERCUT 
        Point tm6 = new Point((int)(180 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm7 = new Point((int)(155 * gconst.dsc), (int)(82 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm6, tm7);
        //hatch pattern for cut
        int i = 1;
        for (i = 1; i < 40; i++)
        {
            Point tmp1 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp2 = new Point((int)((obj.Vuc2_x + 1 * i) * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));

            g.DrawLine(gconst.BPEN, tmp1, tmp2);
            Point tmp3 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp4 = new Point((int)((obj.Vuc3_x + 1 * i) * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
            g.DrawLine(gconst.BPEN, tmp3, tmp4);

        }

        commonfun.cenline(g, (int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc), (int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
    }

    public static void SQUARE_profile(Graphics g, Component comp, Profile obj)
    {

        commonfun.SquareTemplate(g, comp, obj);
        Coordinate cord = new Coordinate();
        obj.VST_x = 90;
        obj.VST_y = 80;



        obj.V1_x = obj.VST_x;
        obj.V1_y = obj.VST_y + obj.TWT_dimval / 2;
        obj.V2_x = obj.VST_x + obj.TL_dimval - (obj.RMD_dimval / 2 - obj.TWS_dimval / 2) / 1.14;
        obj.V2_y = obj.VST_y + obj.TWS_dimval / 2;
        obj.V3_x = obj.VST_x + obj.TL_dimval;
        obj.V3_y = obj.VST_y + obj.RMD_dimval / 2;
        obj.V4_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval - obj.TPL_dimval;
        obj.V4_y = obj.VST_y + obj.RMD_dimval / 2;
        obj.V5_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval;
        obj.V5_y = obj.VST_y + obj.GPL_dimval / 2;
        obj.V6_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval;
        obj.V6_y = obj.VST_y - obj.GPL_dimval / 2;
        obj.V7_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval - obj.TPL_dimval;
        obj.V7_y = obj.VST_y - obj.RMD_dimval / 2;
        obj.V8_x = obj.VST_x + obj.TL_dimval;
        obj.V8_y = obj.VST_y - obj.RMD_dimval / 2;

        obj.V9_x = obj.VST_x + obj.TL_dimval - (obj.RMD_dimval / 2 - obj.TWS_dimval / 2) / 1.14;
        obj.V9_y = obj.VST_y - obj.TWS_dimval / 2;

        obj.V10_x = obj.VST_x;
        obj.V10_y = obj.VST_y - obj.TWT_dimval / 2;
        obj.VC1_x = obj.VST_x - 20;
        obj.VC1_y = obj.VST_y;
        obj.VC2_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval + 80;
        obj.VC2_y = obj.VST_y;
        obj.V11_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval + 40;
        obj.V11_y = obj.VST_y - obj.RMD_dimval / 2;
        obj.V12_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval + 40 + obj.RMD_dimval;
        obj.V12_y = obj.VST_y - obj.RMD_dimval / 2;
        obj.V13_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval + 40;
        obj.V13_y = obj.VST_y + obj.RMD_dimval / 2;
        obj.V14_x = obj.VST_x + obj.TL_dimval + obj.BL_dimval + 40 + obj.RMD_dimval;
        obj.V14_y = obj.VST_y + obj.RMD_dimval / 2;

        obj.Vuc1_x = obj.V3_x + 13;
        obj.Vuc1_y = obj.V3_y;
        obj.Vuc2_x = obj.V3_x + 13 - (13 / 2);
        obj.Vuc2_y = obj.V3_y - obj.RMD_dimval;
        obj.Vuc3_x = obj.V3_x + 13 + (13 / 2);
        obj.Vuc3_y = obj.V3_y - obj.RMD_dimval;


        //calculate vertex       
        Point pst = new Point((int)(obj.VST_x * gconst.dsc), (int)(obj.VST_y * gconst.dsc));
        Point p1 = new Point((int)(obj.V1_x * gconst.dsc), (int)(obj.V1_y * gconst.dsc));
        Point p2 = new Point((int)(obj.V2_x * gconst.dsc), (int)(obj.V2_y * gconst.dsc));
        Point p3 = new Point((int)(obj.V3_x * gconst.dsc), (int)(obj.V3_y * gconst.dsc));
        Point p4 = new Point((int)(obj.V4_x * gconst.dsc), (int)(obj.V4_y * gconst.dsc));
        Point p5 = new Point((int)(obj.V5_x * gconst.dsc), (int)(obj.V5_y * gconst.dsc));
        Point p6 = new Point((int)(obj.V6_x * gconst.dsc), (int)(obj.V6_y * gconst.dsc));
        Point p7 = new Point((int)(obj.V7_x * gconst.dsc), (int)(obj.V7_y * gconst.dsc));
        Point p8 = new Point((int)(obj.V8_x * gconst.dsc), (int)(obj.V8_y * gconst.dsc));
        Point p9 = new Point((int)(obj.V9_x * gconst.dsc), (int)(obj.V9_y * gconst.dsc));
        Point p10 = new Point((int)(obj.V10_x * gconst.dsc), (int)(obj.V10_y * gconst.dsc));
        Point VC1 = new Point((int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc));
        Point VC2 = new Point((int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
        Point V11 = new Point((int)(obj.V11_x * gconst.dsc), (int)(obj.V11_y * gconst.dsc));
        Point V12 = new Point((int)(obj.V12_x * gconst.dsc), (int)(obj.V12_y * gconst.dsc));
        Point V13 = new Point((int)(obj.V13_x * gconst.dsc), (int)(obj.V13_y * gconst.dsc));
        Point V14 = new Point((int)(obj.V14_x * gconst.dsc), (int)(obj.V14_y * gconst.dsc));
        Point Vuc1 = new Point((int)(obj.Vuc1_x * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
        Point Vuc2 = new Point((int)(obj.Vuc2_x * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));
        Point Vuc3 = new Point((int)(obj.Vuc3_x * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));





        // Create points that define line.
        //Point p1 = new Point((int)(x * gconst.dsc), (int)(y * gconst.dsc));
        //Point p2 = new Point((int)(cord.V1_x * gconst.dsc), (int)(cord.V1_y * gconst.dsc));
        //Draw lines joining above 10 vertices for basic profile
        g.DrawLine(gconst.BPEN, pst, p1);
        g.DrawLine(gconst.BPEN, p1, p2);
        g.DrawLine(gconst.BPEN, p2, p3);
        g.DrawLine(gconst.BPEN, p3, p4);
        g.DrawLine(gconst.BPEN, p4, p5);
        g.DrawLine(gconst.BPEN, p5, p6);
        g.DrawLine(gconst.BPEN, p6, p7);
        g.DrawLine(gconst.BPEN, p7, p8);
        g.DrawLine(gconst.BPEN, p8, p9);
        g.DrawLine(gconst.BPEN, p9, p10);
        g.DrawLine(gconst.BPEN, p10, p1);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc2);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc3);
        g.DrawLine(gconst.BPEN, p3, p8);
        // side view
        g.DrawLine(gconst.BPEN, V11, V12);
        g.DrawLine(gconst.BPEN, V12, V14);
        g.DrawLine(gconst.BPEN, V14, V13);
        g.DrawLine(gconst.BPEN, V11, V13);

        // draw dimensions
        commonfun.hordimtop(g, (int)(obj.V10_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.TL_dimval);
        commonfun.hordimtop(g, (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V6_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.BL_dimval);
        commonfun.hordimtop(g, (int)(obj.V4_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), (int)(obj.V5_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), obj.TPL_dimval);
        commonfun.vertdim(g, (int)((obj.V5_x + 8) * gconst.dsc), (int)((obj.V5_y) * gconst.dsc), (int)((obj.V6_x + 8) * gconst.dsc), (int)((obj.V6_y) * gconst.dsc), obj.GPL_dimval);
        commonfun.vertdim(g, (int)((obj.V1_x - 8) * gconst.dsc), (int)((obj.V1_y) * gconst.dsc), (int)((obj.V10_x - 8) * gconst.dsc), (int)((obj.V10_y) * gconst.dsc), obj.TTS_dimval);
        commonfun.vertdim(g, (int)((obj.V9_x + 3) * gconst.dsc), (int)((obj.V9_y) * gconst.dsc), (int)((obj.V2_x + 3) * gconst.dsc), (int)((obj.V2_y) * gconst.dsc), obj.TWS_dimval);
        // side view dim.

        commonfun.hordimtop(g, (int)(obj.V11_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), (int)(obj.V12_x * gconst.dsc), (int)((obj.V12_y - 5) * gconst.dsc), obj.RMD_dimval);
        //commonfun.vertdim_withouttext(g, (int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y) * gconst.dsc), (int)((obj.V14_x + 8) * gconst.dsc), (int)((obj.V14_y) * gconst.dsc), obj.RMD_dimval);
        // Point tm = new Point((int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y + obj.RMD_dimval / 2) * gconst.dsc));
        // string st = System.Convert.ToString(obj.RMD_dimval;
        // g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, tm);   // draw text 

        //uncut dim
        commonfun.hordimwithouttext(g, (int)(obj.V3_x * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc), (int)(obj.Vuc1_x * gconst.dsc), (int)((obj.Vuc1_y + 7) * gconst.dsc), 0);
        Point tm1 = new Point((int)((obj.V3_x + 17) * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc));
        g.DrawString("UNCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm1);   // draw text UNCUT 
        //text upcut
        Point tm2 = new Point((int)(150 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("UPCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm2);   // draw text UPCUT 
        Point tm4 = new Point((int)(155 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm5 = new Point((int)(140 * gconst.dsc), (int)(80 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm4, tm5);
        //text overcut
        Point tm3 = new Point((int)(175 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("OVERCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm3);   // draw text OVERCUT 
        Point tm6 = new Point((int)(180 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm7 = new Point((int)(155 * gconst.dsc), (int)(82 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm6, tm7);
        //hatch pattern for cut
        int i = 1;
        for (i = 1; i < 20; i++)
        {
            Point tmp1 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp2 = new Point((int)((obj.Vuc2_x + 1 * i) * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));

            g.DrawLine(gconst.BPEN, tmp1, tmp2);
            Point tmp3 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp4 = new Point((int)((obj.Vuc3_x + 1 * i) * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
            g.DrawLine(gconst.BPEN, tmp3, tmp4);

        }

        commonfun.cenline(g, (int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc), (int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
    }

    public static void Taper_profile(Graphics g, Component comp, Profile obj)
    {

        commonfun.TaperTemplate(g, comp, obj);
        Coordinate cord = new Coordinate();
        obj.VST_x = 90;
        obj.VST_y = 80;



        obj.V1_x = obj.VST_x;
        obj.V1_y = obj.VST_y + obj.TWT_dmval / 2;
        obj.V2_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V2_y = obj.VST_y + obj.TWS_dmval / 2;
        obj.V3_x = obj.VST_x + obj.TL_dmval;
        obj.V3_y = obj.VST_y + obj.W_dmval / 2;
        obj.V4_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V4_y = obj.VST_y + obj.W_dmval / 2;
        obj.V5_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V5_y = obj.VST_y + obj.FPL_dmval / 2;
        obj.V6_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval;
        obj.V6_y = obj.VST_y - obj.FPL_dmval / 2;
        obj.V7_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval - obj.TPL_dmval;
        obj.V7_y = obj.VST_y - obj.W_dmval / 2;
        obj.V8_x = obj.VST_x + obj.TL_dmval;
        obj.V8_y = obj.VST_y - obj.W_dmval / 2;

        obj.V9_x = obj.VST_x + obj.TL_dmval - (obj.W_dmval / 2 - obj.TWS_dmval / 2) / 1.14;
        obj.V9_y = obj.VST_y - obj.TWS_dmval / 2;

        obj.V10_x = obj.VST_x;
        obj.V10_y = obj.VST_y - obj.TWT_dmval / 2;
        obj.VC1_x = obj.VST_x - 20;
        obj.VC1_y = obj.VST_y;
        obj.VC2_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 80;
        obj.VC2_y = obj.VST_y;
        obj.V11_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V11_y = obj.VST_y - obj.W_dmval / 2;
        obj.V12_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.W_dmval;
        obj.V12_y = obj.VST_y - obj.W_dmval / 2;
        obj.V13_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40;
        obj.V13_y = obj.VST_y + obj.W_dmval / 2;
        obj.V14_x = obj.VST_x + obj.TL_dmval + obj.BL_dmval + 40 + obj.W_dmval;
        obj.V14_y = obj.VST_y + obj.W_dmval / 2;
        obj.Vm_x = obj.V11_x + obj.W_dmval / 2;
        obj.Vm_y = obj.VST_y - obj.W_dmval / 2;
        obj.Vuc1_x = obj.V3_x + 13;
        obj.Vuc1_y = obj.V3_y;
        obj.Vuc2_x = obj.V3_x + 13 - (13 / 2);
        obj.Vuc2_y = obj.V3_y - obj.W_dmval;
        obj.Vuc3_x = obj.V3_x + 13 + (13 / 2);
        obj.Vuc3_y = obj.V3_y - obj.W_dmval;


        //calculate vertex       
        Point pst = new Point((int)(obj.VST_x * gconst.dsc), (int)(obj.VST_y * gconst.dsc));
        Point p1 = new Point((int)(obj.V1_x * gconst.dsc), (int)(obj.V1_y * gconst.dsc));
        Point p2 = new Point((int)(obj.V2_x * gconst.dsc), (int)(obj.V2_y * gconst.dsc));
        Point p3 = new Point((int)(obj.V3_x * gconst.dsc), (int)(obj.V3_y * gconst.dsc));
        Point p4 = new Point((int)(obj.V4_x * gconst.dsc), (int)(obj.V4_y * gconst.dsc));
        Point p5 = new Point((int)(obj.V5_x * gconst.dsc), (int)(obj.V5_y * gconst.dsc));
        Point p6 = new Point((int)(obj.V6_x * gconst.dsc), (int)(obj.V6_y * gconst.dsc));
        Point p7 = new Point((int)(obj.V7_x * gconst.dsc), (int)(obj.V7_y * gconst.dsc));
        Point p8 = new Point((int)(obj.V8_x * gconst.dsc), (int)(obj.V8_y * gconst.dsc));
        Point p9 = new Point((int)(obj.V9_x * gconst.dsc), (int)(obj.V9_y * gconst.dsc));
        Point p10 = new Point((int)(obj.V10_x * gconst.dsc), (int)(obj.V10_y * gconst.dsc));
        Point VC1 = new Point((int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc));
        Point VC2 = new Point((int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
        Point V11 = new Point((int)(obj.V11_x * gconst.dsc), (int)(obj.V11_y * gconst.dsc));
        Point V12 = new Point((int)(obj.V12_x * gconst.dsc), (int)(obj.V12_y * gconst.dsc));
        Point V13 = new Point((int)(obj.V13_x * gconst.dsc), (int)(obj.V13_y * gconst.dsc));
        Point V14 = new Point((int)(obj.V14_x * gconst.dsc), (int)(obj.V14_y * gconst.dsc));
        Point Vuc1 = new Point((int)(obj.Vuc1_x * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
        Point Vuc2 = new Point((int)(obj.Vuc2_x * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));
        Point Vuc3 = new Point((int)(obj.Vuc3_x * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
        Point Vm = new Point((int)(obj.Vm_x * gconst.dsc), (int)(obj.Vm_y * gconst.dsc));




        // Create points that define line.
        //Point p1 = new Point((int)(x * gconst.dsc), (int)(y * gconst.dsc));
        //Point p2 = new Point((int)(cord.V1_x * gconst.dsc), (int)(cord.V1_y * gconst.dsc));
        //Draw lines joining above 10 vertices for basic profile
        g.DrawLine(gconst.BPEN, pst, p1);
        g.DrawLine(gconst.BPEN, p1, p2);
        g.DrawLine(gconst.BPEN, p2, p3);
        g.DrawLine(gconst.BPEN, p3, p4);
        g.DrawLine(gconst.BPEN, p4, p5);
        g.DrawLine(gconst.BPEN, p5, p6);
        g.DrawLine(gconst.BPEN, p6, p7);
        g.DrawLine(gconst.BPEN, p7, p8);
        g.DrawLine(gconst.BPEN, p8, p9);
        g.DrawLine(gconst.BPEN, p9, p10);
        g.DrawLine(gconst.BPEN, p10, p1);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc2);
        g.DrawLine(gconst.BPEN, Vuc1, Vuc3);
        g.DrawLine(gconst.BPEN, p3, p8);
        // side view
        g.DrawLine(gconst.BPEN, Vm, V13);
        g.DrawLine(gconst.BPEN, Vm, V14);
        g.DrawLine(gconst.BPEN, V14, V13);


        // draw dimensions
        commonfun.hordimtop(g, (int)(obj.V10_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.TL_dimval);
        commonfun.hordimtop(g, (int)(obj.V8_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), (int)(obj.V6_x * gconst.dsc), (int)((obj.V8_y - 5) * gconst.dsc), obj.BL_dimval);
        commonfun.hordimtop(g, (int)(obj.V4_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), (int)(obj.V5_x * gconst.dsc), (int)((obj.V4_y + 8) * gconst.dsc), obj.TPL_dimval);
        commonfun.vertdim(g, (int)((obj.V5_x + 8) * gconst.dsc), (int)((obj.V5_y) * gconst.dsc), (int)((obj.V6_x + 8) * gconst.dsc), (int)((obj.V6_y) * gconst.dsc), obj.GPG_dimval);
        commonfun.vertdim(g, (int)((obj.V1_x - 8) * gconst.dsc), (int)((obj.V1_y) * gconst.dsc), (int)((obj.V10_x - 8) * gconst.dsc), (int)((obj.V10_y) * gconst.dsc), obj.TTS_dimval);
        commonfun.vertdim(g, (int)((obj.V9_x + 3) * gconst.dsc), (int)((obj.V9_y) * gconst.dsc), (int)((obj.V2_x + 3) * gconst.dsc), (int)((obj.V2_y) * gconst.dsc), obj.TWS_dimval);
        // side view dim.

        commonfun.hordimwithouttext(g, (int)(obj.V13_x * gconst.dsc), (int)((obj.V14_y + 10) * gconst.dsc), (int)(obj.V14_x * gconst.dsc), (int)((obj.V14_y + 10) * gconst.dsc), obj.W_dimval);
        // commonfun.vertdim_withouttext(g, (int)((obj.V12_x + 8) * gconst.dsc), (int)((obj.V12_y) * gconst.dsc), (int)((obj.V14_x + 8) * gconst.dsc), (int)((obj.V14_y) * gconst.dsc), obj.W_dimval);
        Point tm = new Point((int)((obj.V13_x + 2) * gconst.dsc), (int)((obj.V14_y + 4) * gconst.dsc));
        string st = System.Convert.ToString(obj.W_dimval);
        g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, tm);   // draw text 

        //uncut dim
        commonfun.hordimwithouttext(g, (int)(obj.V3_x * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc), (int)(obj.Vuc1_x * gconst.dsc), (int)((obj.Vuc1_y + 7) * gconst.dsc), 0);
        Point tm1 = new Point((int)((obj.V3_x + 17) * gconst.dsc), (int)((obj.V3_y + 7) * gconst.dsc));
        g.DrawString("UNCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm1);   // draw text UNCUT 
        //text upcut
        Point tm2 = new Point((int)(150 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("UPCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm2);   // draw text UPCUT 
        Point tm4 = new Point((int)(155 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm5 = new Point((int)(140 * gconst.dsc), (int)(80 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm4, tm5);
        //text overcut
        Point tm3 = new Point((int)(175 * gconst.dsc), (int)(55 * gconst.dsc));
        g.DrawString("OVERCUT", gconst.TSIZE10, gconst.TXTBRUSH, tm3);   // draw text OVERCUT 
        Point tm6 = new Point((int)(180 * gconst.dsc), (int)(62 * gconst.dsc));
        Point tm7 = new Point((int)(155 * gconst.dsc), (int)(82 * gconst.dsc));
        g.DrawLine(gconst.BPEN, tm6, tm7);
        //hatch pattern for cut
        int i = 1;
        for (i = 1; i < 20; i++)
        {
            Point tmp1 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp2 = new Point((int)((obj.Vuc2_x + 1 * i) * gconst.dsc), (int)(obj.Vuc2_y * gconst.dsc));

            g.DrawLine(gconst.BPEN, tmp1, tmp2);
            Point tmp3 = new Point((int)((obj.Vuc1_x + 1 * i) * gconst.dsc), (int)(obj.Vuc1_y * gconst.dsc));
            Point tmp4 = new Point((int)((obj.Vuc3_x + 1 * i) * gconst.dsc), (int)(obj.Vuc3_y * gconst.dsc));
            g.DrawLine(gconst.BPEN, tmp3, tmp4);

        }

        commonfun.cenline(g, (int)(obj.VC1_x * gconst.dsc), (int)(obj.VC1_y * gconst.dsc), (int)(obj.VC2_x * gconst.dsc), (int)(obj.VC2_y * gconst.dsc));
    }
}
