﻿using System;
using System.Web;

namespace JKFIlesEntity
{
    public class OuterChartMasterEntity 
    {
     public string ob_box_chartnum;
     public int obd_detail_id{get;set;}
     public int obd_box_id {get;set;}
     public string obd_boxdtl_recid{get;set;}
     public string obd_parm_name{get;set;}
     public string obd_parm_code{get;set;}
     public string obd_parm_dwgvalue{get;set;}
     public string obd_parm_scrnvalue{get;set;}
     public string ob_box_imgname { get; set;}
}
}
