﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Profile
/// </summary>


public class Profile
{
    public double A = 45.0;  // Tang length
    public double B = 100.0; // Body length
    public double C = 7.9;    // At shoulder
    public double D = 5.0;    // At Tang TIP
    public double E = 40.0;    // Taper length
    public double F = 11.4;    // Forging 
    public double W = 15.55;    // width
    public double T = 5.6;    // thk  
    public double Ang = 40.0;  // Shoulder ang
    public double Rad = 3.0;  // Shoulder rad`
    public double Uncut = 15.0;  // Uncut length
    public double Upcutinclination = 45.0;  // Uncut length
    public double Overcutinclination = 65.0;  // Uncut length


    //public double A { get; set; }  // Tang length
    //public double B { get; set; }  // Body length
    //public double C { get; set; }   // At shoulder
    //public double D { get; set; }   // At Tang TIP
    //public double E { get; set; }     // Taper length
    //public double F { get; set; }  // Forging 
    //public double W { get; set; }   // width
    //public double T { get; set; }    // thk  
    //public double Ang { get; set; }   // Shoulder ang
    //public double Rad { get; set; }   // Shoulder rad`
    //public double Uncut { get; set; }   // Uncut length
    //public double Upcutinclination { get; set; }  // Uncut length
    //public double Overcutinclination { get; set; }  // Uncut length

    //-------------flat----------------------------

    public double RMW_dimval = 12.1;   //  RAW MATERIAL WIDTH
    public double RMT_dimval = 1.4;   //  RAW MATERIAL THICKNESS
    public double CML_dimval = 150;   //  CROPPING MOOD LENGTH (+1 / -0 in mm)
    public double BL_dimval = 100;   //  BODY LENGTH (in mm)
    public double TL_dimval = 40;   //  TANG LENGTH (%%P 1mm)
    public double TWS_dimval = 4.82;   //  TANG WIDTH AT SHOULDER (%%P 0.5 in mm)
    public double TTS_dimval = 3;   //  TANG THICKNESS AT SHOULDER (%%P 0.5 in mm)
    public double TWT_dimval = 3.0;   //  TANG WIDTH AT TIP (%%P 0.3 in mm)
    public double TTT_dimval = 3;   //  TANG THICKNESS AT TIP (%%P 0.3 in mm)
    public double FPL_dimval = 8.4;    //(Forging)         Point size (Punching) at exact length in mm
    public double W_dimval = 11.7;   //  (GRINDING) BODY DIMENSION WIDTH (%%P 0.5 in mm)
    public double T_dimval = 3;   //  (GRINDING) BODY DIMENSION THICKNESS (%%P 0.5 in mm)
    public double GPL_dimval = 4.85;   //  (GRINDING) BODY DIMENSION POINT SIZE AT EXACT LENGTH (in mm)
    public double TPL_dimval = 39;   //  TAPER LENGTH (in mm) (%%P 1 mm)
    public double L_dimval = 100;   //  BODY LENGTH AFTER FLAT SIDE CUTTING BEFORE HARDNENING (in mm)
    public double IUP_dimval = 65;   //  UPCUT INCLINATION (%%P 3)
    public double IOV_dimval = 50;   //  OVERCUT INCLINATION (%%P 3)
    public double IEG_dimval = 90;   //  EDGECUT INCLINATION (%%P 3)
    public double HRC_dimval = 60 - 62;   //  HARDNESS (in HRC)
    public double ASH_dimval = 40;   //  SHOULDER ANGLE (in deg)
    public double RSH_dimval = 3;   //  SHOULDER RADIUS (in mm)
    public double USL_dimval =13;        //UNCUT AT SHOULDER(%%P 2 mm)
    public double UEL_dimval = 9;         // UPCUT at edge                       

    public double D_dimval { get; set; }

    public string RMW_dispval { get; set; }   //  RAW MATERIAL WIDTH
    public string RMT_dispval { get; set; }   //  RAW MATERIAL THICKNESS
    public string CML_dispval { get; set; }   //  CROPPING MOOD LENGTH (+1 / -0 in mm)
    public string BL_dispval { get; set; }   //  BODY LENGTH (in mm)
    public string TL_dispval { get; set; }  //  TANG LENGTH (%%P 1mm)
    public string TWS_dispval { get; set; }   //  TANG WIDTH AT SHOULDER (%%P 0.5 in mm)
    public string TTS_dispval { get; set; }   //  TANG THICKNESS AT SHOULDER (%%P 0.5 in mm)
    public string TWT_dispval { get; set; }   //  TANG WIDTH AT TIP (%%P 0.3 in mm)
    public string TTT_dispval { get; set; }   //  TANG THICKNESS AT TIP (%%P 0.3 in mm)
    public string FPL_dispval { get; set; }   //(Forging)         Point size (Punching) at exact length in mm
    public string W_dispval { get; set; }  //  (GRINDING) BODY DIMENSION WIDTH (%%P 0.5 in mm)
    public string T_dispval { get; set; } //  (GRINDING) BODY DIMENSION THICKNESS (%%P 0.5 in mm)
    public string GPL_dispval { get; set; }   //  (GRINDING) BODY DIMENSION POINT SIZE AT EXACT LENGTH (in mm)
    public string TPL_dispval { get; set; }   //  TAPER LENGTH (in mm) (%%P 1 mm)
    public string L_dispval { get; set; }   //  BODY LENGTH AFTER FLAT SIDE CUTTING BEFORE HARDNENING (in mm)
    public string IUP_dispval { get; set; }  //  UPCUT INCLINATION (%%P 3)
    public string IOV_dispval { get; set; }//  OVERCUT INCLINATION (%%P 3)
    public string IEG_dispval { get; set; }   //  EDGECUT INCLINATION (%%P 3)
    public string HRC_dispval { get; set; }   //  HARDNESS (in HRC)
    public string ASH_dispval { get; set; }   //  SHOULDER ANGLE (in deg)
    public string RSH_dispval { get; set; }   //  SHOULDER RADIUS (in mm)
    public string CT_dispval { get; set; }  //  CUT TYPE
    public string CSP_dispval { get; set; }  //  CUT SPECIFICATION
    public string UCT_dispval { get; set; }  //  UP CUT
    public string ECT_dispval { get; set; }   //  EDGE CUT
    public string OCT_dispval { get; set; }   //  OVER CUT
    public string FTY_dispval { get; set; }//  FILE TYPE
    public string FSB_dispval { get; set; }  //  FYLE SUBTYPE
    public string FSZ_dispval { get; set; }  //  FILE SIZE
    public string PTN_dispval { get; set; }  //  PARTNUMBER
    public string FNM_dispval { get; set; }   //  FILE NAME
    public string USL_dispval { get; set; }        //UNCUT AT SHOULDER(%%P 2 mm)
    public string UEL_dispval { get; set; }   // UPCUT at edge



    // Half Round

    public string USLF_dispval_HR = "";        //UNCUT AT F/S
    public string USLR_dispval_HR = "";        //UNCUT AT R/S
    public string IUPF_dispval_HR = "";   //  UPCUT INCLINATION FOR F/S(%%P 3)
    public string IUPR_dispval_HR = "";   //  UPCUT INCLINATION FOR R/S(%%P 3)
    public string IOVF_dispval_HR = "";   //  OVERCUT INCLINATION FOR F/S(%%P 3)
    public string IOVR_dispval_HR = "";   //  OVERCUT INCLINATION FOR R/S(%%P 3)
    public string UP1GN_dispval_HR = "";  // GENERAL SIDE CUT AND ROUND CUT
    public string OV1GN_dispval_HR = "";  // GENERAL SIDE AND ROUND OVER CUT


    // for round 
    public string ATN_dispval_R = "";   //  TANG ANGLE IN DEG
    public string RSH_dispval_R = "";   //  TANG RADIUS IN MM
    public string D_dispval_R = "";   //  (GRINDING) BODY DIMENSION  (%%P 0.5 in mm) 

    public double D_dimval_R = 0;   // (GRINDING) BODY DIMENSION  (%%P 0.5 in mm) 
    public double RMD_dimval_R = 0;   //  RAW MATERIAL DIMENSION in mm(%%P0.2)

    public string RMD_dispval_R = "";
    public string OCR_dispval = "";//NO OF ROWS FOR ROUND FILE OVER CUT ROWS
    public string UCR_dispval = "";//NO OF ROWS FOR ROUND FILE UP CUT ROWS


    // square--

    public string ATN_dispval_SQ = "";   //  TANG ANGLE IN DEG
    public string RSH_dispval_SQ = "";   //  TANG RADIUS IN MM
    public string D_dispval_SQ = "";   //  (GRINDING) BODY DIMENSION  (%%P 0.5 in mm) 
    public string RMD_dispval_SQ = "";

    public double RMD_dimval { get; set; }

    //Coordinate defination
    public double VST_x, VST_y, V1_x, V1_y, V2_x, V2_y, V3_x, V3_y, V4_x, V4_y, V5_x, V5_y, V6_x,
        V6_y, V7_x, V7_y, V8_x, V8_y, V9_x, V9_y, V10_x, V10_y, VC1_x, VC1_y, VC2_x, VC2_y, V11_x, V11_y, Vh1_x, Vh1_y, Vh2_x, Vh2_y;

    public double V12_x, V12_y, V13_x, V13_y, V14_x, V14_y, Vuc1_x, Vuc1_y, Vuc2_x, Vuc2_y, Vuc3_x, Vuc3_y, Vm_x, Vm_y;

    //parameters for drawing static profile
    public double RMW_dmval = 12.1;   //  RAW MATERIAL WIDTH
    public double RMT_dmval = 1.4;   //  RAW MATERIAL THICKNESS
    public double CML_dmval = 150;   //  CROPPING MOOD LENGTH (+1 / -0 in mm)
    public double BL_dmval = 100;   //  BODY LENGTH (in mm)
    public double TL_dmval = 40;   //  TANG LENGTH (%%P 1mm)
    public double TWS_dmval = 4.82;   //  TANG WIDTH AT SHOULDER (%%P 0.5 in mm)
    public double TTS_dmval = 3;   //  TANG THICKNESS AT SHOULDER (%%P 0.5 in mm)
    public double TWT_dmval = 3.0;   //  TANG WIDTH AT TIP (%%P 0.3 in mm)
    public double TTT_dmval = 3;   //  TANG THICKNESS AT TIP (%%P 0.3 in mm)
    public double FPL_dmval = 8.4;    //(Forging)         Point size (Punching) at exact length in mm
    public double W_dmval = 11.7;   //  (GRINDING) BODY DIMENSION WIDTH (%%P 0.5 in mm)
    public double T_dmval = 3;   //  (GRINDING) BODY DIMENSION THICKNESS (%%P 0.5 in mm)
    public double GPL_dmval = 4.85;   //  (GRINDING) BODY DIMENSION POINT SIZE AT EXACT LENGTH (in mm)
    public double TPL_dmval = 39;   //  TAPER LENGTH (in mm) (%%P 1 mm)
    public double L_dmval = 100;   //  BODY LENGTH AFTER FLAT SIDE CUTTING BEFORE HARDNENING (in mm)




    //-------------------- declear cord A4 templete --------------------------
    public double RMW_dispvalloc_x = 30;
    public double RMW_dispvalloc_y = 173;
    public double RMT_dispvalloc_x = 45;
    public double RMT_dispvalloc_y = 173;
    public double CML_dispvalloc_x = 62;
    public double CML_dispvalloc_y = 173;
    public double BL_dispvalloc_x = 81;
    public double BL_dispvalloc_y = 173;
    public double TL_dispvalloc_x = 104;
    public double TL_dispvalloc_y = 173;
    public double TWS_dispvalloc_x = 120;
    public double TWS_dispvalloc_y = 173;
    public double TTS_dispvalloc_x = 135;
    public double TTS_dispvalloc_y = 173;
    public double TWT_dispvalloc_x = 148;
    public double TWT_dispvalloc_y = 173;
    public double TTT_dimval_x = 158;
    public double TTT_dimval_y = 173;
    public double TPL_dispvalloc_x = 245;
    public double TPL_dispvalloc_y = 173;
    public double FPL_dispvalloc_x = 167;
    public double FPL_dispvalloc_y = 173;
    public double W_dispvalloc_x = 187;
    public double W_dispvalloc_y = 173;
    public double T_dispvalloc_x = 203;
    public double T_dispvalloc_y = 173;
    public double GPL_dispvalloc_x = 218;
    public double GPL_dispvalloc_y = 173;
    public double TTT_dispvalloc_x = 158;
    public double TTT_dispvalloc_y = 173;
    public double L_dispvalloc_x = 270;
    public double L_dispvalloc_y = 173;

    public double IUP_dispvalloc_x = 15;
    public double IUP_dispvalloc_y = 31;
    public double IOV_dispvalloc_x = 35;
    public double IOV_dispvalloc_y = 31;
    public double IEG_dispvalloc_x = 53;
    public double IEG_dispvalloc_y = 31;
    public double HRC_dispvalloc_x = 72;
    public double HRC_dispvalloc_y = 69;

    public double CT_dispvalloc_x = 49;
    public double CT_dispvalloc_y = 52;
    public double CSP_dispvalloc_x = 15;
    public double CSP_dispvalloc_y = 52;
    public double UCT_dispvalloc_x = 72;
    public double UCT_dispvalloc_y = 52;
    public double ECT_dispvalloc_x = 97;
    public double ECT_dispvalloc_y = 52;
    public double OCT_dispvalloc_x = 113;
    public double OCT_dispvalloc_y = 52;
    public double FSZ_dispvalloc_x = 15;
    public double FSZ_dispvalloc_y = 173;

    public double PTN_dispvalloc_x = 263;
    public double PTN_dispvalloc_y = 11;
    public double FNM_dispvalloc_x = 220;
    public double FNM_dispvalloc_y = 6;
    public double USL_dispvalloc_x = 75;
    public double USL_dispvalloc_y = 31;
    public double UEL_dispvalloc_x = 100;
    public double UEL_dispvalloc_y = 31;
    public double RMW1_dispvalloc_x = 73;
    public double RMW1_dispvalloc_y = 74;
    public double RMT1_dispvalloc_x = 89;
    public double RMT1_dispvalloc_y = 74;

    //cordinates for text location for A3 template

    public double RMW_dispvalA3_x = 43;
    public double RMW_dispvalA3_y = 245;
    public double RMT_dispvalA3_x = 63;
    public double RMT_dispvalA3_y = 245;
    public double CML_dispvalA3_x = 91;
    public double CML_dispvalA3_y = 245;
    public double BL_dispvalA3_x = 118;
    public double BL_dispvalA3_y = 245;
    public double TL_dispvalA3_x = 148;
    public double TL_dispvalA3_y = 245;
    public double TWS_dispvalA3_x = 170;
    public double TWS_dispvalA3_y = 245;
    public double TTS_dispvalA3_x = 187;
    public double TTS_dispvalA3_y = 245;
    public double TWT_dispvalA3_x = 208;
    public double TWT_dispvalA3_y = 245;
    public double TTT_dispvalA3_x = 220;
    public double TTT_dispvalA3_y = 245;
    public double FPL_dispvalA3_x = 240;
    public double FPL_dispvalA3_y = 245;
    public double W_dispvalA3_x = 265;
    public double W_dispvalA3_y = 245;
    public double T_dispvalA3_x = 285;
    public double T_dispvalA3_y = 245;
    public double GPL_dispvalA3_x = 305;
    public double GPL_dispvalA3_y = 245;
    public double TPL_dispvalA3_x = 340;
    public double TPL_dispvalA3_y = 245;

    public double L_dispvalA3_x = 375;
    public double L_dispvalA3_y = 245;
    public double IUP_dispvalA3_x = 21;
    public double IUP_dispvalA3_y = 44;
    public double IOV_dispvalA3_x = 50;
    public double IOV_dispvalA3_y = 44;
    public double IEG_dispvalA3_x = 78;
    public double IEG_dispvalA3_y = 44;
    public double HRC_dispvalA3_x = 105;
    public double HRC_dispvalA3_y = 97;

    public double CT_dispvalA3_x = 73;
    public double CT_dispvalA3_y = 74;
    public double CSP_dispvalA3_x = 32;
    public double CSP_dispvalA3_y = 74;
    public double UCT_dispvalA3_x = 107;
    public double UCT_dispvalA3_y = 74;
    public double ECT_dispvalA3_x = 146;
    public double ECT_dispvalA3_y = 74;
    public double OCT_dispvalA3_x = 165;
    public double OCT_dispvalA3_y = 74;
    public double FSZ_dispvalA3_x = 20;
    public double FSZ_dispvalA3_y = 245;

    public double PTN_dispvalA3_x = 380;
    public double PTN_dispvalA3_y = 15;
    public double FNM_dispvalA3_x = 310;
    public double FNM_dispvalA3_y = 15;
    public double USL_dispvalA3_x = 110;
    public double USL_dispvalA3_y = 44;
    public double UEL_dispvalA3_x = 145;
    public double UEL_dispvalA3_y = 44;
    public double RMW1_dispvalA3_x = 110;
    public double RMW1_dispvalA3_y = 104;
    public double RMT1_dispvalA3_x = 130;
    public double RMT1_dispvalA3_y = 104;

    public String note1_dispval = "DEPTH OF CUT IS CALCLUATED BASED ON"; // NOTE FOR TEMPLETE
    public String note2_dispval = "TEETH PER INCH AND IS MIN. 52% & PITCH FOR SINGLE";
    public String note3_dispval = "CUT FILES AND MIN.55% FOR DOUBLE CUT FILE.";



    #region halfround
    public double RMW_dispvalloc_HR_x = 30;
    public double RMW_dispvalloc_HR_y = 173;
    public double RMT_dispvalloc_HR_x = 43;
    public double RMT_dispvalloc_HR_y = 173;
    public double CML_dispvalloc_HR_x = 61;
    public double CML_dispvalloc_HR_y = 173;
    public double BL_dispvalloc_HR_x = 80;
    public double BL_dispvalloc_HR_y = 173;
    public double TL_dispvalloc_HR_x = 103;
    public double TL_dispvalloc_HR_y = 173;
    public double TWS_dispvalloc_HR_x = 121;
    public double TWS_dispvalloc_HR_y = 173;
    public double TTS_dispvalloc_HR_x = 134;
    public double TTS_dispvalloc_HR_y = 173;
    public double TWT_dispvalloc_HR_x = 148;
    public double TWT_dispvalloc_HR_y = 173;
    public double TTT_dimval_HR_x = 158;
    public double TTT_dimval_HR_y = 173;
    public double TPL_dispvalloc_HR_x = 246;
    public double TPL_dispvalloc_HR_y = 173;
    public double FPL_dispvalloc_HR_x = 170;
    public double FPL_dispvalloc_HR_y = 173;
    public double W_dispvalloc_HR_x = 190;
    public double W_dispvalloc_HR_y = 173;
    public double T_dispvalloc_HR_x = 204;
    public double T_dispvalloc_HR_y = 173;
    public double GPL_dispvalloc_HR_x = 217;
    public double GPL_dispvalloc_HR_y = 173;
    public double TTT_dispvalloc_HR_x = 158;
    public double TTT_dispvalloc_HR_y = 173;
    public double L_dispvalloc_HR_x = 272;
    public double L_dispvalloc_HR_y = 173;
    public double CT_dispval_HR_x = 51;
    public double CT_dispval_HR_y = 60;
    public double CSP_dispval_HR_x = 25;
    public double CSP_dispval_HR_y = 60;

    public double IUPF_dispvalloc_HR_x = 22;
    public double IUPF_dispvalloc_HR_y = 33;
    public double IUPR_dispvalloc_HR_x = 90;
    public double IUPR_dispvalloc_HR_y = 33;
    public double IOVF_dispvalloc_HR_x = 54;
    public double IOVF_dispvalloc_HR_y = 33;
    public double IOVR_dispvalloc_HR_x = 123;
    public double IOVR_dispvalloc_HR_y = 33;
    public double HRC_dispvalloc_HR_x = 57;
    public double HRC_dispvalloc_HR_y = 78;

    public double USLF_dispvalloc_HR_x = 194;
    public double USLF_dispvalloc_HR_y = 33;
    public double USLR_dispvalloc_HR_x = 161;
    public double USLR_dispvalloc_HR_y = 33;
    public double RMW1_dispvalloc_HR_x = 65;
    public double RMW1_dispvalloc_HR_y = 83;
    public double RMT1_dispvalloc_HR_x = 78;
    public double RMT1_dispvalloc_HR_y = 83;

    public double UP1GNF_dispvalloc_HR_x = 78;
    public double UP1GNF_dispvalloc_HR_y = 60;
    public double UP1GNR_dispvalloc_HR_x = 117;
    public double UP1GNR_dispvalloc_HR_y = 60;
    public double OV1GNF_dispvalloc_HR_x = 156;
    public double OV1GNF_dispvalloc_HR_y = 60;
    public double OV1GNR_dispvalloc_HR_x = 194;
    public double OV1GNR_dispvalloc_HR_y = 60;

    public double V15_x, V15_y, V16_x, V16_y;
    #endregion


    //for Square
    public string D_dispval = "";   //  (GRINDING) BODY DIMENSION  (%%P 0.5 in mm) 
    public string RMD_dispval = "";

    //taper
    public String GBD_dispval = "9.40";//GRAINDING BODY IN DIM
    public String GPG_dispval = "6.40";//GRADING POINT GRADING 
    public String GAG_dispval = "5.40";//GRADING AFTER GRADING  
    public Double GPG_dimval = 6.40;//GRADING POINT GRADING 

    #region FOR SQUARE A4 TEMPLETE

    public double RMD_dispvalloc_SQ_x = 30;
    public double RMD_dispvalloc_SQ_y = 173;
    public double CML_dispvalloc_SQ__x = 60;
    public double CML_dispvalloc_SQ_y = 173;
    public double BL_dispvalloc_SQ_x = 82;
    public double BL_dispvalloc_SQ_y = 173;
    public double TL_dispvalloc_SQ_x = 104;
    public double TL_dispvalloc_SQ_y = 173;
    public double TWS_dispvalloc_SQ_x = 122;
    public double TWS_dispvalloc_SQ_y = 173;
    public double TTS_dispvalloc_SQ_x = 132;
    public double TTS_dispvalloc_SQ_y = 173;
    public double TWT_dispvallo_SQ_x = 148;
    public double TWT_dispvalloc_SQ_y = 173;
    public double TTT_dimval_SQ_x = 156;
    public double TTT_dimval_SQ_y = 173;
    public double TAD_dispvalloc_SQ_x = 173;
    public double TAD_dispvalloc_SQ_y = 173;
    public double TRM_dispvalloc_SQ_x = 189;
    public double TRM_dispvalloc_SQ_y = 173;
    public double D_dispvalloc_SQ_x = 211;
    public double D_dispvalloc_SQ_y = 173;
    public double GPL_dispvalloc_SQ_x = 223;
    public double GPL_dispvalloc_SQ_y = 173;
    public double TPL_dispvalloc_SQ_x = 249;
    public double TPL_dispvalloc_SQ_y = 173;
    public double L_dispvalloc_SQ_x = 271;
    public double L_dispvalloc_SQ_y = 173;

    public double IUP_dispvalloc_SQ_x = 15;
    public double IUP_dispvalloc_SQ_y = 31;
    public double IOV_dispvalloc_SQ_x = 35;
    public double IOV_dispvalloc_SQ_y = 31;
    public double HRC_dispvalloc_SQ_x = 72;
    public double HRC_dispvalloc_SQ_y = 69;


    public double UCT_dispvalloc_SQ_x = 78;
    public double UCT_dispvalloc_SQ_y = 52;
    public double OCT_dispvalloc_SQ_x = 98;
    public double OCT_dispvalloc_SQ_y = 52;
    public double USL_dispvalloc_SQ_x = 75;
    public double USL_dispvalloc_SQ_y = 31;
    public double RMT1_dispvalloc_SQ_x = 75;
    public double RMT1_dispvalloc_SQ_y = 74;

    public double nt1_x_SQ = 200;
    public double nt1_y_SQ = 50;
    public double nt2_x_SQ = 200;
    public double nt2_y_SQ = 46;
    public double nt3_x_SQ = 200;
    public double nt3_y_SQ = 42;
    #endregion

    #region ROUND FOR A3 TEMPLETE
    public double RMD_dispvalloc_x_R = 36;
    public double RMD_dispvalloc_y_R = 173;
    public double CML_dispvalloc_x_R = 61;
    public double CML_dispvalloc_y_R = 173;
    public double BL_dispvalloc_x_R = 80;
    public double BL_dispvalloc_y_R = 173;
    public double TL_dispvalloc_x_R = 108;
    public double TL_dispvalloc_y_R = 173;
    public double TWS_dispvalloc_x_R = 120;
    public double TWS_dispvalloc_y_R = 173;
    public double TTS_dispvalloc_x_R = 136;
    public double TTS_dispvalloc_y_R = 173;
    public double TWT_dispvalloc_x_R = 147;
    public double TWT_dispvalloc_y_R = 173;
    public double TTT_dimval_x_R = 156;
    public double TTT_dimval_y_R = 173;
    public double ATN_dispvalloc_x_R = 172;
    public double ATN_dispvalloc_y_R = 173;
    public double RSH_dispvalloc_x_R = 188;
    public double RSH_dispvalloc_y_R = 173;
    public double D_dispvalloc_x_R = 205;
    public double D_dispvalloc_y_R = 173;
    public double GPL_dispvalloc_x_R = 221;
    public double GPL_dispvalloc_y_R = 173;
    public double TPL_dispvalloc_x_R = 247;
    public double TPL_dispvalloc_y_R = 173;
    public double L_dispvalloc_x_R = 270;
    public double L_dispvalloc_y_R = 173;

    public double IUP_dispvalloc_x_R = 22;
    public double IUP_dispvalloc_y_R = 33;
    public double IOV_dispvalloc_x_R = 57;
    public double IOV_dispvalloc_y_R = 33;
    public double HRC_dispvalloc_x_R = 55;
    public double HRC_dispvalloc_y_R = 78;

    public double CT_dispvalloc_x_R = 53;
    public double CT_dispvalloc_y_R = 61;
    public double CSP_dispvalloc_x_R = 23;
    public double CSP_dispvalloc_y_R = 61;
    public double UCT_dispvalloc_x_R = 82;
    public double UCT_dispvalloc_y_R = 61;
    public double OCT_dispvalloc_x_R = 119;
    public double OCT_dispvalloc_y_R = 61;
    public double FSZ_dispvalloc_x_R = 16;
    public double FSZ_dispvalloc_y_R = 173;

    public double PTN_dispvalloc_x_R = 265;
    public double PTN_dispvalloc_y_R = 11;
    public double FNM_dispvalloc_x_R = 228;
    public double FNM_dispvalloc_y_R = 11;
    public double USL_dispvalloc_x_R = 97;
    public double USL_dispvalloc_y_R = 33;
    public double RMD1_dispvalloc_x_R = 64;
    public double RMD1_dispvalloc_y_R = 83;
    public double OCR_dispvalloc_x_R = 158;
    public double OCR_dispvalloc_y_R = 60;
    public double UCR_dispvalloc_x_R = 200;
    public double UCR_dispvalloc_y_R = 60;

    public double nt1_x_R = 200;
    public double nt1_y_R = 50;
    public double nt2_x_R = 200;
    public double nt2_y_R = 46;
    public double nt3_x_R = 200;
    public double nt3_y_R = 42;
    #endregion

    #region taper FOR A4 TEMPLETE
    public double RMW_dispvalloc_x_T = 36;
    public double RMW_dispvalloc_y_T = 173;
    public double CML_dispvalloc_x_T = 62;
    public double CML_dispvalloc_y_T = 173;
    public double TL_dispvalloc_x_T = 96;
    public double TL_dispvalloc_y_T = 173;
    public double TWS_dispvalloc_x_T = 125;
    public double TWS_dispvalloc_y_T = 173;
    public double TWT_dispvalloc_x_T = 148;
    public double TWT_dispvalloc_y_T = 173;
    public double GBD_dimval_x = 174;
    public double GBD_dimval_y = 173;
    public double GPG_dispvalloc_x = 209;
    public double GPG_dispvalloc_y = 173;
    public double GAG_dispvalloc_x = 241;
    public double GAG_dispvalloc_y = 173;
    public double TPL_dispvalloc_x_T = 270;
    public double TPL_dispvalloc_y_T = 173;

    public double IUP_dispvalloc_x_T = 15;
    public double IUP_dispvalloc_y_T = 32;
    public double IOV_dispvalloc_x_T = 33;
    public double IOV_dispvalloc_y_T = 32;
    public double IEG_dispvalloc_x_T = 50;
    public double IEG_dispvalloc_y_T = 32;
    public double HRC_dispvalloc_x_T = 55;
    public double HRC_dispvalloc_y_T = 69;

    public double CT_dispvalloc_x_T = 56;
    public double CT_dispvalloc_y_T = 53;
    public double CSP_dispvalloc_x_T = 24;
    public double CSP_dispvalloc_y_T = 53;
    public double UCT_dispvalloc_x_T = 78;
    public double UCT_dispvalloc_y_T = 53;
    public double ECT_dispvalloc_x_T = 98;
    public double ECT_dispvalloc_y_T = 53;
    public double OCT_dispvalloc_x_T = 116;
    public double OCT_dispvalloc_y_T = 53;
    public double FSZ_dispvalloc_x_T = 15;
    public double FSZ_dispvalloc_y_T = 173;

    public double PTN_dispvalloc_x_T = 263;
    public double PTN_dispvalloc_y_T = 11;
    public double FNM_dispvalloc_x_T = 220;
    public double FNM_dispvalloc_y_T = 12;
    public double UGL_dispvalloc_x_T = 100;
    public double UGL_dispvalloc_y_T = 32;
    public double RMW1_dispvalloc_x_T = 65;
    public double RMW1_dispvalloc_y_T = 74;
    public double USL_dispvalloc_x_T = 75;
    public double USL_dispvalloc_y_T = 32;
    public double UEL_dispvalloc_x_T = 100;
    public double UEL_dispvalloc_y_T = 32;

    public double nt1_x_T = 200;
    public double nt1_y_T = 50;
    public double nt2_x_T = 200;
    public double nt2_y_T = 46;
    public double nt3_x_T = 200;
    public double nt3_y_T = 42;

    #endregion
}