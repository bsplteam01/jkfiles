﻿using System;
using System.Web;

namespace JKFIlesEntity
{
    public class PallettypeEntity 
    {
        public string Pallettype { get; set; }
        public string remark { get; set; }
        public string pkng_request_type { get; set; }
        public int pkng_request_id { get; set; }
    }
}
