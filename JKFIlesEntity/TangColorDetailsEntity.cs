﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class TangColorDetailsEntity
    {
        public string tc_color_name { get; set; }
        public bool tc_isApproved { get; set; }
        public bool tc_isActive { get; set; }
        public int tc_verno { get; set; }
        public DateTime tc_createdate { get; set; }
        public string tc_createby { get; set; }
        public DateTime tc_approvedate { get; set; }
        public string tc_approveby { get; set; }

        public IEnumerable<TangColorDetailsEntity> TangColorlist { get; set; }
    }
}