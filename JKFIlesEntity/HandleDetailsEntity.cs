﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class HandleDetailsEntity
    {
        public int hd_handle_id { get; set; }
        public string hd_handle_recno { get; set; }
        public string hd_parm_name { get; set; }
        public string hd_parm_code { get; set; }
        public string hd_parm_dwgvalue { get; set; }
        public string hd_parm_scrnvalue { get; set; }
        public Boolean hd_parm_isApproved { get; set; }
        public Boolean hd_parm_isActive { get; set; }
        public string hd_parm_createby { get; set; }
        public DateTime hd_parm_createdate { get; set; }
        public string hd_parm_approveby { get; set; }
        public DateTime hd_parm_approvedate { get; set; }
        //by swapnil
        public string hm_chartimg_name { get; set; }
        public string hm_handle_type { get; set; }
        public string hm_handlesub_type { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string L { get; set; }
        public string FINISH { get; set; }
        public string Dummy1 { get; set; }
        public string Dummy2 { get; set; }
        public string Dummy3 { get; set; }
        public string Dummy4 { get; set; }
        public string Arange { get; set; }
        public string Brange { get; set; }
        public string Crange { get; set; }
        public string Drange { get; set; }
        public string Erange { get; set; }
        public string Frange { get; set; }
        public string Grange { get; set; }
        public string Hrange { get; set; }
        public string Lrange { get; set; }
        public string tempChartNumlbl { get; set; }
        public string hm_chartimg_namelbl { get; set; }
        public string Albl { get; set; }
        public string Blbl { get; set; }
        public string Clbl { get; set; }
        public string Dlbl { get; set; }
        public string Elbl { get; set; }
        public string Flbl { get; set; }
        public string Glbl { get; set; }
        public string Hlbl { get; set; }
        public string Llbl { get; set; }
        public string FINISHlbl { get; set; }
        public string Dummy1lbl { get; set; }
        public string Dummy2lbl { get; set; }
        public string Dummy3lbl { get; set; }
        public string Dummy4lbl { get; set; }
        public string hm_request_type{ get; set; }
        public int    hm_request_id  { get; set; }
        public string fileTypeCode   { get; set; }
        public string FileSubType    { get; set; }
        public string fileSize       { get; set; }
        public string remark { get; set; }
        //end

        //public string H { get; set; }
        public string H1 { get; set; }
        public string H2 { get; set; }
        public string HoleDIN { get; set; }
        public string LENGTHOFHANDLE { get; set; }
        public string R { get; set; }
        public string R1 { get; set; }
        public string R2 { get; set; }
        public string R3 { get; set; }
        public string SIDER4 { get; set; }
        public string SIDER5 { get; set; }
        public string SideW { get; set; }
        public string SideW1 { get; set; }
        public string SideW2 { get; set; }
        public string SideW3 { get; set; }
        public string HandleImage { get; set; }

       


        public List<HandleDetailsEntity> HandleDetailsEntityList { get; set; }

        public string tempChartNum { get; set; }
    }
}