﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class BrandDetailsEntity
    {
        public int _id { get; set; }
        public string bm_brand_id { get; set; }
        public string bm_brand_name { get; set; }
        public string bm_brand_remarks { get; set; }
        public string bm_brand_isApproved { get; set; }
        public bool bm_brand_isActive { get; set; }
        public int bm_brand_verno { get; set; }
        public DateTime bm_brand_createdate { get; set; }
        public string bm_brand_createby { get; set; }
        public DateTime bm_brand_approvedate { get; set; }
        public string bm_brand_approveby { get; set; }

        public int bm_request_id { get; set; }
        public string bm_request_type { get; set; }

        public IEnumerable<BrandDetailsEntity> brandList { get; set; }

        public string flag { get; set; } 

    }
}