﻿using System;
using System.Web;

namespace JKFIlesEntity
{
    public class ApprovalEntityModel 
    {
            public string Name{get;set;}
            public DateTime? Date {get;set;}
            public string Status { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
    }
}
