﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class OuterDetailsEntity
    {
        public int      obd_box_id { get; set; }
        public string   obd_boxdtl_recid { get; set; }
        public string   obd_parm_name { get; set; }
        public string   obd_parm_code { get; set; }
        public string   obd_parm_dwgvalue { get; set; }
        public string   obd_parm_scrnvalue { get; set; }
        public string   obd_box_isApproved { get; set; }
        public Boolean  obd_box_isActive { get; set; }
        public string   obd_box_createby { get; set; }
        public DateTime obd_box_createdate { get; set; }
        public string   obd_box_approveby { get; set; }
        public DateTime obd_box_approvedate { get; set; }


        public string OUTERPACKMATERIAL { get; set; }
        public string OUTERPACKDESCRIPTION { get; set; }
        public string OUTERPACKFINISH { get; set; }
        public string OUTERPACKAPPLICATION { get; set; }
        public string OUTERPACKLENGTH { get; set; }
        public string OUTERPACKWIDTH { get; set; }
        public string OUTERPACKHEIGHT { get; set; }
        public string OUTERPACKBATTENPOSITION { get; set; }
        public string OUTERPACKIMG { get; set; }
        public string OTHER1 { get; set; }
        public string OTHER2 { get; set; }
        public string OTHER3 { get; set; }
        public string OTHER4 { get; set; }
        public string OTHER5 { get; set; }

        public string OUTERPACKLENGTHrange { get; set; }
        public string OUTERPACKWIDTHrange { get; set; }
        public string OUTERPACKHEIGHTrange { get; set; }
        public string OUTERPACKBATTENPOSITIONrange { get; set; }
        public string OTHER1range { get; set; }
        public string OTHER2range { get; set; }
        public string OTHER3range { get; set; }
        public string OTHER4range { get; set; }
        public string OTHER5range { get; set; }

        public string ob_request_type { get; set; }
        public int ob_request_id { get; set; }
        public string ob_box_type { get; set; }
        public string ob_box_fsize_code { get; set; }
        public List<OuterDetailsEntity> outerDeatialsEntityList { get; set; }
    }
}