﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class ApprovalPreparationEntity
    {
        public string req_type { get; set; }
        public int req_id { get; set; }
        public string ap_remarks { get; set; }
        public string ap_createdDate { get; set; }
        public string enter_remark { get; set; }
        public string approve_flag { get; set; }
        public string Appr_from { get; set; }
        public string ap_remarks_for { get; set; }
        public string ap_status { get; set; }
        public string ap_createby { get; set; }

    }
}