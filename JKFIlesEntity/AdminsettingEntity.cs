﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class AdminsettingEntity
    {
        
        public int Config_Id { get; set; }
        public string OEMId { get; set; }
        public string OEMName { get; set; }
        public string Registration_Key { get; set; }
        public string Parameter_Code { get; set; }
        public string Parameter_Desc { get; set; }
        public string Parameter_Value { get; set; }
        public string Operator { get; set; }
        public string Operation { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }

        public string server { get; set; }
        public string data_base { get; set; }
        public string userid { get; set; }
        public string password { get; set; }

        public string flag { get; set; }
    }
}