﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class Constants
    {
        public string dp_createby { get; set; }
        public string dp_approveby { get; set; }
        public string pr_approveby { get; set; }
        public string pr_createby { get; set; }
        public string prc_createby { get; set; }
        public string othdt_createby { get; set; }
        public string othdt_approveby { get; set; }
        public string pm_dim_parm_verno { get; set; }
        public string fcl_verno { get; set; }
        public string bm_brand_verno { get; set; }
        public string ft_ftype_verno { get; set; }
        public string fs_size_verno { get; set; }


        
    }
}