﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class ProcessMasterDetailsEntity
    {
        public string pro_process_code { get; set; }
        public int pro_process_seqno { get; set; }
        public string pro_cut_setup { get; set; }
        public string pro_length_setup { get; set; }
        public string pro_type_setup { get; set; }
        public string pro_isApproved { get; set; }
        public Boolean pro_isActive { get; set; }
        public DateTime pro_createdate { get; set; }
        public string pro_createby { get; set; }
        public DateTime pro_approvedate { get; set; }
        public string pro_approveby { get; set; }
        public string ft_ftype_code { get; set; }
        public string flag { get; set; }
        public string pro_filetype { get; set; }

        public IEnumerable<ProcessMasterDetailsEntity> processmasterlist { get; set; }
        public IEnumerable<FileTypeDataEntity> filetTypelist { get; set; }
        public IEnumerable<ProcessMasterDetailsEntity> Processlist { get; set; }


        /// New Profile Process Master

        public int ppm_id { get; set; }
        public string ppm_process_code { get; set; }
        public int ppm_process_seqno { get; set; }
        public string ppm_request_type { get; set; }
        public int ppm_request_id { get; set; }
        public string ppm_ftype_code { get; set; }
        public string ppm_fstype_code { get; set; }
        public string ppm_remarks { get; set; }
        public string ppm_isApproved { get; set; }
        public bool ppm_isActive { get; set; }
        public string ppm_createby { get; set; }
        public DateTime ppm_createdate { get; set; }
        public string ppm_approveby { get; set; }
        public DateTime ppm_approvedate { get; set; }
        public string ppm_fstype_desc { get; set; }
        public string ppm_ftype_desc { get; set; }
        public int pm_process_seqno { get; set; }
        public string opr_operation_name { get; set; }
        public string uni_unit_name { get; set; }
        public string val_unit_code { get; set; }
        public string ft_request_type { get; set; }
        public int ft_request_id { get; set; }
        
    }
}

