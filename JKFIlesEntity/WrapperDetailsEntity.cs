﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class WrapperDetailsEntity
    {
        public int wd_wrapper_id { get; set; }
        public string wd_wrapper_recid { get; set; }
        public string wd_parm_name { get; set; }
        public string wd_parm_code { get; set; }
        public string wd_parm_dwgvalue { get; set; }
        public string wd_parm_scrnvalue { get; set; }
        public string wd_isApproved { get; set; }
        public Boolean wd_isActive { get; set; }
        public string wd_createby { get; set; }
        public DateTime wd_createdate { get; set; }
        public string wd_approveby { get; set; }
        public DateTime wd_approvedate { get; set; }

        public string WRAPPERCOLOR { get; set; }
        public string WRAPPERCOLORPANTOMESHADE1 { get; set; }
        public string WRAPPERCOLORPANTOMESHADE2 { get; set; }
        public string WRAPPERCOLORPANTOMESHADE3 { get; set; }
        public string WRAPPERNOTE { get; set; }
        public string WRAPPERWIDTH { get; set; }
        public string WRAPPERLENGTH { get; set; }
        public string WRAPPERUNITSTOBEPACKED { get; set; }
        public string WRAPPINGTHICKNESS { get; set; }
        public string WRAPPINGPRINTED { get; set; }
        public string WRAPPINGIMAGE { get; set; }

        public List<WrapperDetailsEntity> wrappperDetailsEntityList { get; set; }
    }
}