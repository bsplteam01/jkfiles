﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class CutSpecsrowwiseEntity 
    {
        public string cutStandardCode { get; set; }
        public string cutStandardName { get; set; }
        public string upcutrange { get; set; }
        public decimal upcutvalue { get; set; }
        public string ovcutrange { get; set; }
        public decimal ovcutvalue { get; set; }
        public string edcutrange { get; set; }
        public decimal edcutvalue { get; set; }
        public string cuttype { get; set; }
        public decimal filesize { get; set; }
        public string prc_request_type { get; set; }
        public int prc_request_id { get; set; }

        public string fcl_ftype_code { get; set; }
        public int fcl_parm_seq_no { get; set; }
        public string fcl_parm_code { get; set; }
        //  public string fcl_parm_value { get; set; }
        public string prc_parameter_value { get; set; }
        public Boolean fcl_upcut_flg { get; set; }
        public Boolean fcl_overcut_flg { get; set; }
        public Boolean fcl_edgecut_flg { get; set; }
        public Boolean fcl_stamphere_flg { get; set; }
        public string fcl_isApproved { get; set; }
        public Boolean fcl_isActive { get; set; }
        public int fcl_verno { get; set; }
        public DateTime fcl_createddate { get; set; }
        public string fcl_createby { get; set; }
        public DateTime fcl_approvedate { get; set; }
        public string fcl_approveby { get; set; }
        public string prc_parameter_name { get; set; }
        public Boolean prc_upcut_flg { get; set; }
        public Boolean prc_overcut_flg { get; set; }
        public Boolean prc_edgecut_flg { get; set; }
        public string prc_overcut_value { get; set; }
        public string prc_upcut_value { get; set; }
        public string prc_edgecut_value { get; set; }
        public string fs_size_code { get; set; }

        public string pm_dim_parm_dwgvalue { get; set; }
        public decimal pm_dim_parm_scrnvalue { get; set; }

        public string vtpi_cut_std_code { get; set; }
        public string vtpi_cut_std_name { get; set; }

        public List<CutSpecsEntity> cutSpecsEntityList { get; set; }
        //public List<CutLocationTpiMasterEntity> CutLocationTpiMasterEntityList { get; set; }

        public List<CutLocationTpiMasterEntity> CutLocationTpiMasterModelList { get; set; }


        //public List<TpiCutStandardsEntity> tpiCutStandardList { get; set; }
        public List<CutLocationTpiMasterEntity> tpiCutStandardList { get; set; }
        public List<ProductionSKUCutSpecsEntity> prodSkuEntity { get; set; }
        public List<CutSpecsEntity> cutSpecsNoApproveList { get; set; }
        public List<CutSpecsEntity> cutSpecsNoActiveList { get; set; }


        public string UPCUT { get; set; }
        public string EDGE { get; set; }
        public string OVERCUT { get; set; }
        public string filesizecode { get; set; }


        public List<CutSpecsEntity> listfiletypecode { get; set; }
        public List<CutSpecsEntity> listsizecode { get; set; }
        public List<CutSpecsEntity> listtype { get; set; }
        public List<CutSpecsEntity> listsubtype { get; set; }
        public List<CutSpecsEntity> listcuttype { get; set; }
        public List<CutSpecsEntity> listcutstandard { get; set; }

        public string filesubtype { get; set; }
        public string filetype { get; set; }
        public string fsize_code { get; set; }
        public string filetypecode { get; set; }
        public string cut_type { get; set; }
        public string cut_standard { get; set; }
    }
}
