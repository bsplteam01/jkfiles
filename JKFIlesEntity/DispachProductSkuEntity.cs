﻿using System;
using System.Collections.Generic;
using System.Web;
//using JKFiles.WebApp.Models;
namespace JKFIlesEntity
{
    public class DispachProductSkuEntity
    {
        public string partno { get; set; }
        public string filetype { get; set; }
        public string pr_request_type { get; set; }
        public int pr_request_id { get; set; }
        public string pr_cust_id { get; set; }
        public string pr_cust_name { get; set; }
        public string pr_brand_id { get; set; }
        public string pr_brand_desc { get; set; }
        public string pr_ship2country_id { get; set; }
        public string pr_ship2country_name { get; set; }
        public string pr_sap_no { get; set; }
        public string pr_customer_part_no { get; set; }
        public string pr_fsize_code { get; set; }
        public string pr_ftype_code { get; set; }
        public string pr_ftype_desc { get; set; }
        public string pr_fstype_code { get; set; }
        public string pr_fstype_desc { get; set; }
        public string pr_filecode { get; set; }
        public string pr_hardness { get; set; }
        public string pr_tang_color { get; set; }
        public decimal pr_uncutattang { get; set; }
        public decimal pr_uncutatpoint { get; set; }
        public decimal pr_uncutatedge { get; set; }
        public decimal pr_upcutncline { get; set; }
        public decimal pr_overcutincline { get; set; }
        public decimal pr_edgecutincline { get; set; }
        public string pr_fcut_code { get; set; }
        public string pr_fcut_desc { get; set; }
        public string pr_cut_standard { get; set; }
        public string pr_rawmtrl_cd { get; set; }
        public decimal pr_rawmtrl_height { get; set; }
        public decimal pr_rawmtrl_width { get; set; }
        public decimal pr_rawmtrl_kgperthsnd { get; set; }
        public decimal pr_rawmtrl_netwt { get; set; }
        public string pr_isApproved { get; set; }
        public byte pr_isActive { get; set; }
        //public string pr_createdate{ get; set; }
        public string pr_createby { get; set; }
        // public string pr_approvedate{ get; set; }
        public string pr_approveby { get; set; }
        public string pr_uniq_id { get; set; }
        public string pr_remarks { get; set; }
        public string fs_size_inches { get; set; }
        public string complete_partno { get; set; }

        //dispatch sku
        public int dp_id { get; set; }
        public string dp_request_type { get; set; }
        public int dp_request_id { get; set; }
        public string dp_dispatch_digit { get; set; }
        public string dp_stamp_type { get; set; }
        public string dp_stamp_chart { get; set; }
        
        public string dp_handle_presence { get; set; }
        public string dp_handle_type { get; set; }
        public string dp_handle_chart { get; set; }
        public string dp_handle_subtype { get; set; }
        public string dp_wrapping_type { get; set; }
        public string dp_wrapping_chart { get; set; }
        public string dp_wrapping_recid { get; set; }
        public string dp_innerbox_type { get; set; }
        public string dp_innerbox_qty { get; set; }
        public string dp_innerbox_chart { get; set; }
        public string dp_innerbox_id { get; set; }
        public string dp_innerlabel_chart { get; set; }
        public string dp_outerbox_type { get; set; }
        public string dp_outerbox_chart { get; set; }
        public string dp_outerbox_recid { get; set; }
        public string dp_pallet_type { get; set; }
        public string dp_pallet_chart { get; set; }
        public string dp_palletdtl_recid { get; set; }

        public Boolean dp_barcode_flg { get; set; }
        public Boolean dp_lineno_flg { get; set; }
        public Boolean dp_hologram_flg { get; set; }
        public Boolean dp_india_flg { get; set; }
        public Boolean dp_qltyseal_flg { get; set; }
        public Boolean dp_prcstkr_flg { get; set; }
        public Boolean dp_polybag_flg { get; set; }
        public Boolean dp_silicagel_flg { get; set; }
        public Boolean dp_fumigation_flg { get; set; }
        public string Skupartno { get; set; }
        public Boolean dp_promotionalitem_flg { get; set; }
        public string dp_strap { get; set; }
        public Boolean dp_cellotape_flg { get; set; }
        public Boolean dp_shrinkwrap_flg { get; set; }
        public Boolean dp_decicant_flg { get; set; }
        public Boolean dp_paperwool_flg { get; set; }
        public Boolean temp_pono_flg { get; set; }
        public string temp_tangcolor { get; set; }
        public Boolean temp_blko_flg { get; set; }
        public Boolean temp_barcodeouter_flg { get; set; }
        public string temp_pkg_remarks { get; set; }
        //public Boolean? dp_promotionalitem_flg { get; set; }
        //public string dp_strap { get; set; }
        //public Boolean? dp_cellotape_flg { get; set; }
        //public Boolean? dp_shrinkwrap_flg { get; set; }
        //public Boolean? dp_decicant_flg { get; set; }
        //public Boolean? dp_paperwool_flg { get; set; }

        //public string dp_version_no{get;set;}
        //public string dp_isApproved{get;set;}
        //public string dp_isActive{get;set;}
        //public string dp_createdate{get;set;}
        //public string dp_createby{get;set;}
        //public string dp_approvedate{get;set;}
        //public string dp_approveby{get;set;}
        public CostDetailsModel obj { get; set; }
        //public List<StampDetailsModel>StampEntityList { get; set; }
        public List<StampDetailsEntity> StampDetailsEntityList { get; set; }
        public List<StampDetailsEntity> stampsDetailsEntityList { get; set; }
        public List<DispachProductSkuEntity> ProduSKUList { get; set; }


        public string handleimgpath { get; set; }
        public List<CutSpecsEntity> Custspeclist { get; set; }
        public List<InnerBoxDetailsEntity> innerboxlist { get; set; }
        public List<OuterChartMasterEntity> OterChartList { get; set; }
        public List<HandleDetailsEntity> HandleDetailsEntityList { get; set; }
        public List<WrapperDetailsEntity> WrapperdetailsList { get; set; }
        public List<PalletDetailsEntity> PalletList { get; set; }
        public List<InnerLabelDetailsEntity> innerLabelList { get; set; }
        public List<HistoryDetailEntity> HistoryDetailsList { get; set; }
        public List<ApprovalEntityModel> ApprovalDetailsList { get; set; }
        public List<SkuSelectionEntity> skudetailslist { get; set; }
        public List<UnitMasterEntity> unitdropdownlist { get; set; }

        public string aa_unit_code { get; set; }

        #region  part no color by Dhanashree
        public string split1 { get; set; }
        public string split2 { get; set; }
        public string split3 { get; set; }
        #endregion

        public string imagename { get; set; }



        public string temp_outer_mrking_flg { get; set; }
        public string dp_outer_mrking_type { get; set; }
        public string dp_outer_mrking_img { get; set; }
        public string temp_pallet_mrking_flg { get; set; }
        public string dp_pallet_mrking_type { get; set; }
        public string dp_pallet_mrking_img { get; set; }
    }
}
