﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class OuterBoxMasterEntity
    {
        public string ob_box_chartnum { get; set; }
        public string ob_box_type { get; set; }
        public string fs_size_code { get; set; }
        public string ob_request_type{ get; set; }
        public int ob_request_id { get; set; }

        public List<OuterBoxMasterEntity> outerboxNOActiveList { get; set; }
        public List<OuterBoxMasterEntity> outerboxNOApproveList { get; set; }

        public List<OuterBoxMasterEntity> outerboxMasterEntityList { get; set; }

        public List<OuterBoxMasterEntity> outerboxMasterList { get; set; }

        public string ob_box_imgname { get; set; }
        public string obd_parm_name { get; set; }
        public string obd_parm_scrnvalue { get; set; }
        public string obd_parm_dwgvalue { get; set; }


        public List<OuterBoxMasterEntity> listsizecode { get; set; }
        public List<OuterBoxMasterEntity> listoutertype { get; set; }
        public string ReqNo { get; set; }
    }
}