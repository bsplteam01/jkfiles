﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class FileSubTypeEntity
    {
        public string subtypename { get; set; }
        public string subtypecode { get; set; }
        public string gmssku { get; set; }
        public decimal width { get; set; }
        public decimal thikness { get; set; }
        public decimal mood_length { get; set; }
        public decimal lengthmm { get; set; }
        public decimal lengthP1mm { get; set; }
        public decimal widthatShoulder { get; set; }
        public decimal thiknessatshoulder { get; set; }
        public decimal widthattip { get; set; }
        public decimal thiknessattip { get; set; }
        public decimal pointsizeatexactlength { get; set; }
        public decimal bodydimentionwidth { get; set; }
        public decimal bodydimentionthikness { get; set; }
        public decimal bodypointsize { get; set; }
        public decimal tapperlengthmm { get; set; }
        public decimal lengthaftercuttingbefore { get; set; }
        public decimal atshoulder { get; set; }
        public decimal onedge { get; set; }
        public decimal upcut { get; set; }
        public decimal overcut { get; set; }
        public decimal edgecut { get; set; }
        public decimal hardness { get; set; }
        public decimal angel { get; set; }
        public decimal radious { get; set; }
        public string Remarks { get; set; }
        public string rmcode { get; set; }
        public string wtinkgthousand { get; set; }
        public string description { get; set; }
        public string widthrange { get; set; }
        public string thiknessrange { get; set; }
        public string mood_lengthrange { get; set; }
        public string lengthmmrange { get; set; }
        public string lengthP1mmrange { get; set; }
        public string widthatShoulderrange { get; set; }

        public string thiknessatshoulderrange { get; set; }
        public string widthattiptrange { get; set; }

        public string thiknessattiprange { get; set; }
        public string pointsizeatexactlengthrange { get; set; }
        public string bodydimentionwidthrange { get; set; }
        public string bodydimentionthiknessrange { get; set; }
        public string bodypointsizerange { get; set; }
        public string tapperlengthmmrange { get; set; }
        public string lengthaftercuttingbeforerange { get; set; }
        public string atshoulderrange { get; set; }
        public string onedgerange { get; set; }
        public string upcutrange { get; set; }
        public string overcutrange { get; set; }
        public string edgecutrange { get; set; }
        public string hardnessrange { get; set; }
        public string angelrange { get; set; }
        public string radiousrange { get; set; }
        public string pm_dim_parm_isApproved { get; set; }

        public int pm_request_id { get; set; }
        public string pm_request_type { get; set; }
        public string filetypecode { get; set; }
        public string filesizecode { get; set; }
        public string reqNo1 { get; set; }

        public string fieldsubtypename { get; set; }
        public string fieldsubtypecode { get; set; }
        public string fieldrmcode { get; set; }
        public string fieldwtinkgthousand { get; set; }
        public string fielddescription { get; set; }
        public string fieldwidth { get; set; }
        public string fieldthikness { get; set; }
        public string fieldgmssku { get; set; }
        public string fieldmood_length { get; set; }
        public string fieldlengthmm { get; set; }
        public string fieldlengthP1mm { get; set; }
        public string fieldwidthatShoulder { get; set; }
        public string fieldthiknessatshoulder { get; set; }
        public string fieldwidthattip { get; set; }
        public string fieldthiknessattip { get; set; }
        public string fieldpointsizeatexactlength { get; set; }
        public string fieldbodydimentionwidth { get; set; }
        public string fieldbodydimentionthikness { get; set; }
        public string fieldbodypointsize { get; set; }
        public string fieldtapperlengthmm { get; set; }
        public string fieldlengthaftercuttingbefore { get; set; }
        public string fieldatshoulder { get; set; }
        public string fieldonedge { get; set; }
        public string fieldupcut { get; set; }
        public string fieldovercut { get; set; }
        public string fieldedgecut { get; set; }
        public string fieldhardness { get; set; }
        public string fieldangel { get; set; }
        public string fieldradious { get; set; }
        public string fieldRemarks { get; set; }
        public string widthattiprange { get; set; }
        public string rmcoderange { get; set; }
        public string wtinkgthousandrange { get; set; }
        public string descriptionrange { get; set; }
        public string gmsskurange { get; set; }
        public string ft_ftype_desc { get; set; }
        public string pm_fstype_desc { get; set; }
        public decimal fs_size_inches { get; set; }
        public IEnumerable<FileSubTypeEntity> fileSubTypelist { get; set; }
        public IEnumerable<FileSubTypeEntity> filetypesublist { get; set; }
        public IEnumerable<FileSubTypeEntity> fileSubTypeNoActiveList { get; set; }
        public IEnumerable<FileSubTypeEntity> fileSubTypeNoApproveList { get; set; }
     
       
        public IEnumerable<FileSubTypeEntity> fileSubTypedetaillist { get; set; }
        public string pm_dim_parm_catg { get; set; }
        public string pm_dim_parm_name { get; set; }
        public string pm_dim_parm_code { get; set; }
        public string pm_dim_parm_scrnvalue { get; set; }
        public decimal pm_dim_parm_dwgvalue { get; set; }
        public string pm_dim_parm_dwgvalue1 { get; set; }
        public string pm_file_code { get; set; }
        public string pm_fsize_code { get; set; }
        public string pm_ftype_code { get; set; }
        public decimal pm_fsize_desc { get; set; }
        public string pm_ftype_desc { get; set; }
        public string pm_fstype_code { get; set; }
        public string flag { get; set; }
        public int pm_dim_parm_verno { get; set; }



        public List<FileSubTypeEntity> listfiletypecode { get; set; }
        public List<FileSubTypeEntity> listsizecode { get; set; }
        public List<FileSubTypeEntity> listtype { get; set; }
        public List<FileSubTypeEntity> listsubtype { get; set; }
        public string filesubtype { get; set; }
        public string filetype { get; set; }
        public List<FileSubTypeEntity> FileSubTypeEntityList { get; set; }
        public IEnumerable<FileSubTypeEntity> skulist { get; set; }
        public int opr_operation_seq { get; set; }
        public string opr_operation_name { get; set; }
        public string postData { get; set; }
        public int pn_version_no { get; set; }


        //partnum
        public string pn_part_no { get; set; }
        public string ReqNo { get; set; }
        public string pn_request_type { get; set; }
        public int pn_request_id { get; set; }
        public string pn_isApproved { get; set; }
        public string pn_fsize_code { get; set; }
        public string pn_ftype_code { get; set; }
        public DateTime pn_start_date { get; set; }
        public DateTime pn_end_date { get; set; }
        public string pn_fstype_code { get; set; }
       // public int pn_version_no { get; set; }


        // tb_impacted_skus table
        public int im_id { get; set; }
        public int im_request_type { get; set; }
        public int im_request_id { get; set; }
        public int im_sku { get; set; }
        public int im_pnum_updated { get; set; }
        public int im_sku_verno { get; set; }
        public int im_sku_start_date { get; set; }
        public int im_sku_end_date { get; set; }
       
        public IEnumerable<FileSubTypeEntity> oldvaluelist { get; set; }
        public IEnumerable<FileSubTypeEntity> newvaluelist { get; set; }

        public string RequestID { get; set; }
    }
}
