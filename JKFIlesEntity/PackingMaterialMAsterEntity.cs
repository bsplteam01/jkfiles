﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class PackingMaterialMAsterEntity
    {
        public string pkng_usedfor { get; set; }

        public string pkng_material { get; set; }
    }
}