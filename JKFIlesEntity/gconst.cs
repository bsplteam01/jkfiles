﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

/// <summary>
/// Summary description for ShapeConstants
/// </summary>
public class gconst
{
    public gconst()
    {

    }
    public static int MAXX = 1500;
    public static int MAXY = 800;
    public static double dsc = (800.0 / 180.0);
    public static SolidBrush TXTBRUSH = new SolidBrush(Color.Black);
    public static SolidBrush fillcirclecolor = new SolidBrush(Color.Black);
    public static Pen BPEN = new Pen(Color.Black);
 //   public static Pen BColor = new Pen(Color.Red);
    public static System.Drawing.Font TSIZE8 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 8);
    public static System.Drawing.Font TSIZE6 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 6);
    public static System.Drawing.Font TSIZE5 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 5);
    public static System.Drawing.Font TSIZE10 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 10);
    public static System.Drawing.Font TSIZE7 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 7);
    public static System.Drawing.Font TSIZE9 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 9);
    public static System.Drawing.Font TSIZE12 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 12);
    public static System.Drawing.Font TSIZE40 = new System.Drawing.Font(System.Drawing.FontFamily.GenericSansSerif, 40);
}