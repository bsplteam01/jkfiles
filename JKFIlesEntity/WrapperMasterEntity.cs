﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class WrapperMasterEntity
    {
        public int wm_wrapper_id { get; set; }
        public string wm_chart_num { get; set; }
        public string wm_cust_id { get; set; }
        public string wm_brand_id { get; set; }
        public string wm_wrappertype { get; set; }
        public string wm_ftype_id { get; set; }
        public string wm_fstype_id { get; set; }
        public string wm_fsize_id { get; set; }
         public string   cm_cust_name       { get; set; }
         public string   bm_brand_name      { get; set; }
         public string   FileTypeCode       { get; set; }
         public decimal   fs_size_inches     { get; set; }
         public string   wm_chart_remarks   { get; set; }
         public string   wm_request_type    { get; set; }
         public int      wm_request_id      { get; set; }
        public List<WrapperMasterEntity> wrapperNoActiveList { get; set; }
        public List<WrapperMasterEntity> wrapperNoApprove { get; set; }
        public List<WrapperMasterEntity> wrapperMasterEntityList { get; set; }

        public List<WrapperMasterEntity> wrapperMasterdetailsList { get; set; }
        public string wd_parm_name { get; set; }
        public string wd_parm_dwgvalue { get; set; }
        public string wd_parm_scrnvalue { get; set; }

        public List<WrapperMasterEntity> listsizecode { get; set; }
        public List<WrapperMasterEntity> listwrappertype { get; set; }
        public string ReqNo { get; set; }

    }
}