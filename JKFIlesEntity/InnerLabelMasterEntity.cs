﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class InnerLabelMasterEntity
    {
        public string ilb_chart_num { get; set; }
        public string ilb_label_id { get; set; }
        public string ilb_ftype_code { get; set; }
        public string ilb_fstype_code { get; set; }
        public string ilb_fsize_code { get; set; }
        public string ilb_cust_id { get; set; }
        public string ilb_brand_id { get; set; }
        public string ilb_fcut_code { get; set; }
        public string ibl_shiptocountry_cd { get; set; }    
        public string ilb_isApproved { get; set; }
        public string ilb_isActive { get; set; }
        public string ilb_createby { get; set; }
        public string ilb_createdate { get; set; }
        public string ilb_approveby { get; set; }
        public string ilb_approvedate { get; set; }
        public string ilb_verno { get; set; }
        public string ilb_request_type { get; set; }
        public string ilb_request_id { get; set; }


        public List<InnerLabelDetailsEntity> innerLabelDetailsEntoityList { get; set; }
    }
}