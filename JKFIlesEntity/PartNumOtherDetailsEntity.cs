﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class PartNumOtherDetailsEntity
    {
        public string othdt_request_type { get; set; }
        public int othdt_request_id { get; set; }
        public Decimal? othdt_prod_cost_onetm { get; set; }
        public Decimal? othdt_prod_cost_run { get; set; }
        public Decimal? othdt_dispatch_onetm { get; set; }
        public Decimal? othdt_dispatch_run { get; set; }
        //public string othdt_req_startdt { get; set; }
        //public string othdt_req_enddt { get; set; }

        public DateTime? othdt_req_startdt { get; set; }
        public DateTime? othdt_req_enddt { get; set; }
        public DateTime othdt_req_startdt1 { get; set; }
        public DateTime othdt_req_enddt1 { get; set; }

        public string othdt_approved { get; set; }
        public Boolean othdt_active { get; set; }
        public DateTime othdt_createdate { get; set; }
        public string othdt_createby { get; set; }
        public DateTime othdt_approvedate { get; set; }
        public string othdt_approveby { get; set; }
        public int othdt_ref_request_id { get; set; }
        public DateTime? othdt_req_enddt_current_sku { get; set; }
        public string ap_createby { get; set; }
    }
}