﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class CutTypeDetailsEntity
    {
        public string fc_cut_code { get; set; }
        public string fc_cut_desc { get; set; }
        public string fc_cut_remarks { get; set; }
        public bool fc_cut_isApproved { get; set; }
        public bool fc_cut_isActive { get; set; }
        public int fc_cut_verno { get; set; }
        public DateTime fc_cut_createddate { get; set; }
        public string fc_cut_createby { get; set; }
        public DateTime fc_cut_approvedate { get; set; }
        public string fc_cut_approveby { get; set; }

        public string fc_cut_type { get; set; }

        public IEnumerable<CutTypeDetailsEntity> CutTypeList { get; set; }
    }
}