﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class OperationMasterEntity
    {
        public int opr_operation_seq { get; set; }
        public string opr_operation_name { get; set; }
        public string opr_operation_image { get; set; }
        public string opr_isApproved { get; set; }
        public Boolean opr_isActive { get; set; }
        public DateTime opr_createdate { get; set; }
        public string opr_createby { get; set; }
        public string opr_approvedate { get; set; }
        public string opr_approveby { get; set; }
        public string flag { get; set; }
        public IEnumerable<OperationMasterEntity> oprationmasterlist { get; set; }
        public string opr_operation_restricted { get; set; }

        public string dpm_request_type { get; set; }
        public int dpm_request_id { get; set; }
        public string dpm_process_code { get; set; }
        public string ma_createby { get; set; }
        public string ma_remarks { get; set; }
    }
}