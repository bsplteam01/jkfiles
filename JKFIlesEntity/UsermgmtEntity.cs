﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace JKFIlesEntity
{
    public class UsermgmtEntity
    {
        public string flag { get; set; }
        public string User_Id { get; set; }
        public string Login_Id { get; set; }
        public string OEMId { get; set; }
        public string OEMName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Employee_Id { get; set; }
        public string Department { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public string Role_Id { get; set; }
        public string RandomNo { get; set; }
        public string PlantName { get; set; }
        public string Plant_Id { get; set; }
        public string Decline_Reason { get; set; }
        public string Comments { get; set; }
        public string Password { get; set; }
        public string CreatedDate { get; set; }
        public byte[] ProfilePic { get; set; }
        public byte[] LogoImg { get; set; }
        public string Main_Login_Flag { get; set; }
        public string Report_Login_Flag { get; set; }
        public Boolean FirstTime { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean IsSuspend { get; set; }
        public string IsFirstLogin { get; set; }
        public string Email_Sending { get; set; }
        public string Email_SetDate { get; set; }
        public string ApproveFlag { get; set; }
        public Boolean ETLFlag { get; set; }
        public string Registration_Key { get; set; }



        public List<UsermgmtEntity> menulist { get; set; }
        public string MainMenu { get; set; }
        public int SubMenu_Id { get; set; }
        public string SubMenu { get; set; }
        public string Controller { get; set; }
        public string sd_chart_num { get; set; }
        public int MainMenuID { get; set; }
        public string Action { get; set; }
        public int RoleId { get; set; }


        public int mstr_id { get; set; }
        public string Menu { get; set; }
        public int Menu_Level { get; set; }
        public int RefId { get; set; }
        public string IsSubMenuPresent { get; set; }




        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}