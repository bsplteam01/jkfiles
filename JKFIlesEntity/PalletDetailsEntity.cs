﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class PalletDetailsEntity
    {
        public int pd_detail_id { get; set; }
        public int pd_pallet_id { get; set; }
        public string pd_palletdtl_recid { get; set; }
        public string pd_parm_name { get; set; }
        public string pd_parm_code { get; set; }
        public string pd_parm_dwgvalue { get; set; }
        public string pd_parm_scrnvalue { get; set; }
        public string pm_pallet_chartimg { get; set; }
        public string pd_isApproved { get; set; }
        public string pd_isActive { get; set; }
        public string pd_createby { get; set; }
        public string pd_createdate { get; set; }
        public string pd_approveby { get; set; }
        public string pd_approvedate { get; set; }

        public List<PalletDetailsEntity> palletDetailsEntityList { get; set; }

        public string pm_pallet_chartnum { get; set; }
        public string pm_pallet_type { get; set; }
        public string PALLETMATERIALRANGE { get; set; }
        public string PALLETDESCRIPTIONRANGE { get; set; }
        public string PALLETTYPERANGE { get; set; }
        public string PALLETLENGTHRANGE { get; set; }
        public string PALLETWIDTHRANGE { get; set; }
        public string PALLETHEIGHTRANGE { get; set; }
        public string PALLETHDPECOVERRANGE { get; set; }
        public string PALLETHDPECOVERLENGTHRANGE { get; set; }
      
        public string PALLETHDPECOVERWIDTHRANGE { get; set; }
        public string PALLETHDPECOVERHEIGHTRANGE { get; set; }
        public string PALLETMATERIAL { get; set; }
        public string PALLETDESCRIPTION { get; set; }
        public string PALLETTYPE { get; set; }
        public string PALLETLENGTH { get; set; }
        public string PALLETWIDTH { get; set; }
        public string PALLETHEIGHT { get; set; }
        public string PALLETHDPECOVER { get; set; }
        public string PALLETHDPECOVERLENGTH { get; set; }
        public string PALLETHDPECOVERWIDTH { get; set; }
        public string PALLETHDPECOVERHEIGHT { get; set; }
        public string pm_request_type{ get; set; }
        public int    pm_request_id  { get; set; }
    }
}
