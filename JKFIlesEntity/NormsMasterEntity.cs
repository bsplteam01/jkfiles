﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class NormsMasterEntity
    {
        public string nor_prod_sku { get; set; }
        public int nor_operation_id { get; set; }
        public string nor_unit_id { get; set; }
        public int nor_equ_id { get; set; }
        public int nor_output_norms { get; set; }
        public string isApproved_flg { get; set; }
        public Boolean isActive_flg { get; set; }
        public DateTime CreateDate { get; set; }
        public string Createby { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Approveby { get; set; }
        public string Remarks { get; set; }
        public string flag { get; set; }


        /// tb_norms_values
        public string pr_RequestNo { get; set; }
        public string pr_ftype_code { get; set; }
        public string pr_process_code { get; set; }
        public string SKU { get; set; }
        public string nm_prod_sku { get; set; }
        public string nm_valuestream { get; set; }
        public string nm_unit_code { get; set; }
        public string nm_opern_name { get; set; }
        public string nm_equ_name { get; set; }
        public string nm_equ_type { get; set; }
        public string nm_norms_value { get; set; }
        public string nm_request_type { get; set; }
        public int nm_request_id { get; set; }


        //tb_dispatch_norms_values
        public string dnm_item_sku { get; set; }
        public string dnm_valuestream { get; set; }
        public string dnm_unit_code { get; set; }
        public string dnm_opern_name { get; set; }
        public string dnm_equ_name { get; set; }
        public string dnm_equ_type { get; set; }
        public string dnm_dnorms_value { get; set; }
        public string dnm_request_type { get; set; }
        public int dnm_request_id { get; set; }
        public string dflag { get; set; }
        public string nm_createby { get; set; }
        public string dnm_createby { get; set; }

        public string ma_remarks { get; set; }
    }
}

