﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class Customersinactive
    {
        public int _id { get; set; }
        public decimal Cm_Cust_Payer_Code { get; set; }
        public string Cm_Cust_Id { get; set; }
        public string Cm_Cust_Name { get; set; }
        public string Cm_Cust_Remarks { get; set; }
        public bool Cm_Cust_IsApproved { get; set; }
        public bool Cm_Cust_IsActive { get; set; }
        public System.DateTime Cm_Cust_Createdate { get; set; }
        public string Cm_Cust_Createby { get; set; }
        public System.DateTime Cm_Cust_Approvedate { get; set; }
        public string Cm_Cust_Approvedby { get; set; }

        public string brandName { get; set; }
        public string brandId { get; set; }
        public string countryName { get; set; }
        public string sapNo { get; set; }
        public string customerPartNO { get; set; }
        public string countryId { get; set; }
        public int cm_request_id { get; set; }
        public string cm_request_type { get; set; }

        public string pr_uniq_id { get; set; }

        public IEnumerable<Customersinactive> custList { get; set; }

    }
}