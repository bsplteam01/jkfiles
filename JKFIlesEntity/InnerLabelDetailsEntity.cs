﻿using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class InnerLabelDetailsEntity 
    {
        public string ilbd_chart_num { get; set; }
        public string ilbd_parm_name { get; set; }
        public string ilbd_parm_code { get; set; }
        public string ilbd_parm_dwgvalue { get; set; }
        public string ilbd_parm_scrnvalue { get; set; }

        public int ilbd_request_id{ get; set; }
        public string ilbd_request_type { get; set; }
        public string ShipToCountryCode { get; set; }

        public string INNERPACKINGTOPLABEL { get; set; }
        public string INNERPACKINGBOTTOMLABEL { get; set; }
        public string INNERPACKINGSIDE1LABEL { get; set; }
        public string INNERPACKINGSIDE2LABEL { get; set; }
        public string INNERPACKINGEND1LABEL { get; set; }
        public string INNERPACKINGEND2LABEL { get; set; }
        public string INNERPACKINGQUALITYSEAL { get; set; }
        public string INNERPACKINGBOXSAMPLE { get; set; }
        public string INNERPACKINGLABELLOCATIONCHARTID { get; set; }
        public string INNERPACKINGBOARDSUPPLIER { get; set; }


        public List<InnerLabelDetailsEntity> innerlableMasterNoApproveList { get; set; }
        public List<InnerLabelDetailsEntity> innerlableMasterNoActiveList { get; set; }
        public List<InnerLabelDetailsEntity> InnerLabelDetailsEntityList { get; set; }
        public List<InnerLabelDetailsEntity> innerlableMasterEntityList { get; set; }

        public string fs_size_code { get; set; }
        public string ft_ftype_desc { get; set; }
        public string cm_cust_name { get; set; }
        public string bm_brand_name { get; set; }
        public string fc_cut_desc { get; set; }
        public string pm_fstype_desc { get; set; }

        public List<InnerLabelDetailsEntity> innerlabellist { get; set; }

        public List<MasterReqParEntity> ReqParlist { get; set; }

        public List<InnerLabelDetailsEntity> listsizecode { get; set; }
        public List<InnerLabelDetailsEntity> listtype { get; set; }
        public List<InnerLabelDetailsEntity> listsubtype { get; set; }
        public List<InnerLabelDetailsEntity> listcuttype { get; set; }
        public List<InnerLabelDetailsEntity> listcstmstr { get; set; }
        public List<InnerLabelDetailsEntity> listbrandmstr { get; set; }
        public string filesubtype { get; set; }
        public string filetype { get; set; }
        public string fsize_code { get; set; }
        public string filetypecode { get; set; }
        public string Customer { get; set; }
        public string Brand { get; set; }

        public string CustomerName { get; set; }
        public string Cut_Type { get; set; }
        public string ReqNo { get; set; }
    }
}

