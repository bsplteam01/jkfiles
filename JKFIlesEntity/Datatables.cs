﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class DataTables
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<RawMaterialMastrEntity> data { get; set; }
    }
}
