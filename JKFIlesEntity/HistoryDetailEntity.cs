﻿using System;
using System.Web;

namespace JKFIlesEntity
{
    public class HistoryDetailEntity 
    {
      public int ap_id{get;set;}
      public string ap_request_type{get;set;}
      public int ap_request_id{get;set;}
      public string ap_remarks_from{get;set;}
      public string ap_remarks_for{get;set;}
      public string ap_remarks{get;set;}
      public string ap_status{get;set;}
      public string ap_createby{get;set;}
      public DateTime ap_createdate{get;set;}
    }
}
