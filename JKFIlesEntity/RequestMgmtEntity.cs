﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class RequestMgmtEntity
    {
        public string flag { get; set; }
        public int Request_Id { get; set; }
        public int Request_Type_Id { get; set; }
        public string OEMId { get; set; }
        public string OEMName { get; set; }
        public string RequestType { get; set; }
        public string RequestDesc { get; set; }
        public string RequestDate { get; set; }
        public string RequestStatus { get; set; }
        public string Comment_Id { get; set; }
        public string Comments { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean IsSuspend { get; set; }
        public string ModifiedDate { get; set; }
        public string RequestBetween { get; set; }
        public string Customer_Id { get; set; }
        public string CustomerName { get; set; }
    }
}