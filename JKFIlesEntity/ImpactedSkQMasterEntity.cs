﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class ImpactedSkQMasterEntity
    {
        public int im_id { get; set; }
        public string im_request_type { get; set; }
        public int im_request_id { get; set; }
        public string im_sku { get; set; }
        public string im_pnum_updated { get; set; }
        public int im_sku_verno { get; set; }
        public DateTime im_sku_start_date { get; set; }
        public DateTime im_sku_end_date { get; set; }
        public string flag { get; set; }
        public int pn_version_no { get; set; }
    }
}