﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

/// <summary>
/// Summary description for commonfun
/// </summary>
public class commonfun
{
      
       

    //function to draw concentric circle
    public static void DrawCircle(Graphics g, Pen pen, float centerX, float centerY, float radius)
    {        
        g.DrawEllipse(pen, centerX - radius, centerY - radius,
                      radius + radius, radius + radius);
    }

    public static void Template(Graphics g, Component comp,Profile prf)
    {
        Double t = 2.5;
        int stx = 20;
        int sty = 20;
        int outw = 1460;
        int outht = 760;
        int i;
        // define template text locations
        Point L1 = new Point(350, 30);
        Point L2 = new Point(35, 60);
        Point L3 = new Point(35, 85);
        Point L4 = new Point(35, 110);
        Point L5 = new Point(35, 135);
        Point L6 = new Point(35, 170);
        Point L7 = new Point(35,175);
        Point L8 = new Point(35,460);
        Point L9 = new Point(35,480);
        Point L10 = new Point(35,525);
        Point L11 = new Point(35,545);
        Point L12 = new Point(35, 590);
        Point L13 = new Point(35, 610);
        Point L14 = new Point(35,635);
        Point L15 = new Point(35,593);
        Point L16 = new Point(35,139);
        Point L17 = new Point(35,114);
        Point L18 = new Point(35,680);
        Point L19 = new Point(35,700);
        Point L20 = new Point(35,725);
        Point L21 = new Point(35,740);
        Point L22 = new Point(35,745);
        Point L23 = new Point(35, 748);
        Point L24 = new Point(35,747);
        g.DrawRectangle(gconst.BPEN,stx,sty,outw,outht );
        stx = stx + 5;
        sty = sty + 5;
        int rht = 22;
        g.DrawRectangle(gconst.BPEN, stx, sty, outw-10, outht-10);
        for (i = 1; i < 7; i++)
        {
            g.DrawLine(gconst.BPEN, stx, (sty + outht - rht*i), stx + outw - 430, (sty + outht - rht*i));
        }
       
        // cut
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 12, 700, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 11, 700, rht);
       
        // Raw meatrial and hardness
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 15, 550, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht*14 ,550, rht);
       
        //CUT inclnation
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 9, 300, rht * 3);
        g.DrawRectangle(gconst.BPEN, stx + 320, sty + outht - rht * 9, 300, rht * 3);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht, stx + 300, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht * 2, stx + 300, sty + outht - rht * 9 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 320, sty + outht - rht * 9 + rht, stx + 620, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx + 320, sty + outht - rht * 9 + rht * 2, stx + 620, sty + outht - rht * 9 + rht * 2);
                    // vertical lines for inclution
        g.DrawLine(gconst.BPEN, stx + 150, sty + 497, stx + 150, sty +540);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 497, stx + 250, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 380, sty + 497, stx + 380, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 480, sty + 497, stx + 480, sty + 540);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 584, stx + 80, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 584, stx + 200, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 480, sty + 584, stx + 480, sty + 628);

        //FILE SPECEFICATION 
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 8, 416, rht);
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 7, 416, rht);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 6 + rht * 2, stx + 1450, sty + outht - rht * 6+ rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 4 + rht * 2, stx + 1450, sty + outht - rht * 4 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 3 + rht * 2, stx + 1450, sty + outht - rht * 3 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 5 + rht * 2, stx + 1450, sty + outht - rht * 5 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 7 + rht * 2, stx + 1450, sty + outht - rht * 7 + rht * 2);

        //verical lines for file specification

        g.DrawLine(gconst.BPEN, stx + 1031, sty + 628, stx + 1031, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + 585, stx + 1220, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1090, sty + 585, stx + 1090, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1300, sty + 585, stx + 1300, sty + 715);
        g.DrawLine(gconst.BPEN, stx + 1350, sty + 585, stx + 1350, sty + 628);



        g.DrawLine(gconst.BPEN, stx + 1000, sty + outht - rht  , stx + 400, sty + outht - rht );
       
        //top horizontal lines
        g.DrawLine(gconst.BPEN, stx, sty+30, stx + outw - 10, sty + 30 );
        g.DrawLine(gconst.BPEN, stx, sty + 140, stx + outw - 10, sty + 140);
        g.DrawLine(gconst.BPEN, stx, sty + 170, stx + outw - 10, sty + 170);

        // top vertical lines
        g.DrawLine(gconst.BPEN, stx+95, sty + 30, stx + 95, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 30, stx + 200, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 330, sty + 30, stx + 330, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 430, sty , stx + 430, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 30, stx + 520, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 650, sty + 30, stx + 650, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 800, sty , stx + 800, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 900, sty , stx + 900, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 980, sty + 30, stx + 980, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1050, sty + 30, stx + 1050, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1190, sty , stx + 1190, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1280, sty + 30, stx + 1280, sty + 170);

        // write template text
        g.DrawString("                                    TANG DIMENSIONS                                                                                                                               GRINDING", gconst.TSIZE9, gconst.TXTBRUSH, L1);   // draw text
        g.DrawString("NORMAL            RM   DIM                CROPPING                    BODY                   TANG                  AT  SHOULDER               AT TIP (5MM                     (FORGING)              W                     T                   GRINDING                        TAPER              BODY  LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, L2);
        g.DrawString("LENGTH            IN MM (TOL.            MOOD LENGTH           LENGTH               LENGTH              (A/F)                                  FROM SH.)                       PT. SIZE                                                                 POINT                            LENGTH             AFTER FLAT SIDE", gconst.TSIZE9, gconst.TXTBRUSH, L3);
        g.DrawString("IN INCHES         W + 0.15,                  IN MM TOL.                  IN MM                   IN MM                   (TOL. + 0.5)                      (A/F)                                  PUNCHING)             + 0.5)               (+ 0.5)             SIZE                                IN MM                CUTTING BEFORE ", gconst.TSIZE9, gconst.TXTBRUSH, L4);
        g.DrawString("                                 -                                                                                                                                          -                                                                                                            -                         -", gconst.TSIZE9, gconst.TXTBRUSH, L17);
        g.DrawString("                           T +0.1)                     (+1/-0.0)                                                        (TOL. +1)             AT EXACT                                                                   IN MM                                                                                                                                         HARDINING (MM)", gconst.TSIZE9, gconst.TXTBRUSH, L5);
        g.DrawString("                              -                               -                                                                                - ", gconst.TSIZE9, gconst.TXTBRUSH, L16);
        g.DrawString("                                        X                                                                                                                                      X                                       X                                                                                                                                                                                                           ", gconst.TSIZE9, gconst.TXTBRUSH, L6);
        ////g.DrawString("4                            12.1        1.4                 100                               100-112                   45                     4.9              3                        3            3                      8.4 - 9.0                     11.7                  3                4.85  -  5.45                        39 - 44                  100 - 102", gconst.TSIZE9, gconst.TXTBRUSH, L6);
        g.DrawString("RAW MATERAL SECTION  (mm) :               x", gconst.TSIZE9, gconst.TXTBRUSH, L8);
        g.DrawString("HARDNESS  (HRC) :  ", gconst.TSIZE9, gconst.TXTBRUSH, L9);
        g.DrawString("CUT STANDRAD                    CUT TYPE                           U/C                           E/C                         O/C", gconst.TSIZE9, gconst.TXTBRUSH, L10);
        //g.DrawString("GENRAL                                    BAST", gconst.TSIZE9, gconst.TXTBRUSH, L11);
        g.DrawString("CUT INCLINATION (DEG.) + 3                                                             UNCUT (mm) + 2", gconst.TSIZE9, gconst.TXTBRUSH, L12);
        g.DrawString("                                              -                                                                                          -", gconst.TSIZE9, gconst.TXTBRUSH, L15);
        g.DrawString("1", gconst.TSIZE9, gconst.TXTBRUSH,1060,610);
        g.DrawString("       FILE SPACIFICATION                 -               1               -", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 610);
        g.DrawString("ITEM", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 635);
        g.DrawString("        DESCRPITION                 MATERIAL      DIFF       REMARKS", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 635);
        g.DrawString("U/C                           O/C                          E/C                         AT SHOULDER                               ON EDGE", gconst.TSIZE9, gconst.TXTBRUSH, L13);
        //g.DrawString("65                               50                           90                                  19                                                    9", gconst.TSIZE9, gconst.TXTBRUSH, L14);
        g.DrawString("S . NO .                         ALTERATIONS                                                                                                                                                        CHANGED  BY                                         DATE", gconst.TSIZE9, gconst.TXTBRUSH, L18);
        g.DrawString("                                  SURFACE FINISH                                                         GENERAL  TOLERANCE WHERE NOT SPECIFIED ", gconst.TSIZE9, gconst.TXTBRUSH, L19);
        g.DrawString("                               △                        △△                       △△△                                   0.10                     10.30                  30.100                  100.300                  300.100                       1000.2000", gconst.TSIZE9, gconst.TXTBRUSH, L20);
        g.DrawString(" ∼ ", gconst.TSIZE40, gconst.TXTBRUSH, 35,705);
        g.DrawString("ROUGH              ROUGH                 MEDIUM                    FINE  ", gconst.TSIZE9, gconst.TXTBRUSH, L21);
        g.DrawString("                                                                                                                                     +0.10                      +0.2                    +0.3                     +0.5                        +0.8                              +1.2", gconst.TSIZE9, gconst.TXTBRUSH, L22);
        g.DrawString("GROUND           MACHINE              MACHINE                  MACHINE ", gconst.TSIZE9, gconst.TXTBRUSH, L23);
        g.DrawString("                                                                                                                                     -                               -                          -                           -                              -                                     -", gconst.TSIZE9, gconst.TXTBRUSH, L24);
        g.DrawString("JK  FILES  (India)", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 660);
        g.DrawString("LIMITED     THANE", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 680);
        g.DrawString("DATE", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 660);
        g.DrawString("DRAWN", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 680);
        g.DrawString("REVIEWED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 700);
        g.DrawString("APPROVED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 720);
        g.DrawString("DRAWING NO .", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 740);
        g.DrawString("PROJECTION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 709);
        g.DrawString("DESIGNATION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 740);
       
        //------------------------values-------------------------------------------
      //  Profile prf = new Profile();
        g.DrawString(prf.FSZ_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35,170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 130,170);
        g.DrawString(prf.RMT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 185,170);
        g.DrawString(prf.CML_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250,170);
        g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 365,170);
        g.DrawString(prf.TL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 500,170);
        g.DrawString(prf.TWS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 570, 170);
        g.DrawString(prf.TTS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 625,170);
        g.DrawString(prf.TWT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 710, 170);
        g.DrawString(prf.TTT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 760, 170);
        g.DrawString(prf.FPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 850, 170);
        g.DrawString(prf.W_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 950, 170);
        g.DrawString(prf.T_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1050, 170);
        g.DrawString(prf.GPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1090, 170);
        g.DrawString(prf.TPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1220, 170);
        g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1350, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 230, 460);
        g.DrawString(prf.RMT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 290, 460);
        g.DrawString(prf.HRC_dispval, gconst.TSIZE10, gconst.TXTBRUSH,200,480 );
        g.DrawString(prf.UCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 330,545);
        g.DrawString(prf.ECT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 440, 545);
        g.DrawString(prf.OCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 540, 545);
        g.DrawString(prf.IUP_dispval, gconst.TSIZE10, gconst.TXTBRUSH,35,635 );
        g.DrawString(prf.IOV_dispval, gconst.TSIZE10, gconst.TXTBRUSH,140,635);
        g.DrawString(prf.IEG_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250,635);
        g.DrawString(prf.USL_dispval, gconst.TSIZE10, gconst.TXTBRUSH,380,635);
        g.DrawString(prf.UEL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 550,635);
        g.DrawString(prf.FNM_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1095, 750);
        g.DrawString(prf.PTN_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1250, 750);
        g.DrawString(prf.CSP_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 45, 548);
        g.DrawString(prf.CT_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 200, 548);
    }

    //Template for Mil Profile
    public static void MilTemplate(Graphics g, Component comp,Profile prf)
    {
        Double t = 2.5;
        int stx = 20;
        int sty = 20;
        int outw = 1460;
        int outht = 760;
        int i;
        // define template text locations
        Point L1 = new Point(350, 30);
        Point L2 = new Point(35, 60);
        Point L3 = new Point(35, 85);
        Point L4 = new Point(35, 110);
        Point L5 = new Point(35, 135);
        Point L6 = new Point(35, 170);
        Point L7 = new Point(35, 175);
        Point L8 = new Point(35, 460);
        Point L9 = new Point(35, 480);
        Point L10 = new Point(35, 525);
        Point L11 = new Point(35, 545);
        Point L12 = new Point(35, 590);
        Point L13 = new Point(35, 610);
        Point L14 = new Point(35, 635);
        Point L15 = new Point(35, 593);
        Point L16 = new Point(35, 139);
        Point L17 = new Point(35, 114);
        Point L18 = new Point(35, 680);
        Point L19 = new Point(35, 700);
        Point L20 = new Point(35, 725);
        Point L21 = new Point(35, 740);
        Point L22 = new Point(35, 745);
        Point L23 = new Point(35, 748);
        Point L24 = new Point(35, 747);
        g.DrawRectangle(gconst.BPEN, stx, sty, outw, outht);
        stx = stx + 5;
        sty = sty + 5;
        int rht = 22;
        g.DrawRectangle(gconst.BPEN, stx, sty, outw - 10, outht - 10);
        for (i = 1; i < 7; i++)
        {
            g.DrawLine(gconst.BPEN, stx, (sty + outht - rht * i), stx + outw - 430, (sty + outht - rht * i));
        }

        // cut
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 12, 700, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 11, 700, rht);

        // Raw meatrial and hardness
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 15, 550, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 14, 550, rht);

        //CUT inclnation
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 9, 300, rht * 3);
        g.DrawRectangle(gconst.BPEN, stx + 320, sty + outht - rht * 9, 300, rht * 3);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht, stx + 300, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht * 2, stx + 300, sty + outht - rht * 9 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 320, sty + outht - rht * 9 + rht, stx + 620, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx + 320, sty + outht - rht * 9 + rht * 2, stx + 620, sty + outht - rht * 9 + rht * 2);
        // vertical lines for inclution
        g.DrawLine(gconst.BPEN, stx + 150, sty + 497, stx + 150, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 497, stx + 250, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 380, sty + 497, stx + 380, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 480, sty + 497, stx + 480, sty + 540);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 584, stx + 80, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 584, stx + 200, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 480, sty + 584, stx + 480, sty + 628);



        g.DrawLine(gconst.BPEN, stx + 100, sty + 629, stx + 100, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 680, sty + 629, stx + 680, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 629, stx + 850, sty + 672);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 738, stx + 80, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 180, sty + 738, stx + 180, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 280, sty + 738, stx + 280, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 400, sty + 738, stx + 400, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 738, stx + 520, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 610, sty + 738, stx + 610, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 700, sty + 738, stx + 700, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 800, sty + 738, stx + 800, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 900, sty + 738, stx + 900, sty + 694);

        //FILE SPECEFICATION 
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 8, 416, rht);
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 7, 416, rht);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 6 + rht * 2, stx + 1450, sty + outht - rht * 6 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 4 + rht * 2, stx + 1450, sty + outht - rht * 4 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 3 + rht * 2, stx + 1450, sty + outht - rht * 3 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 5 + rht * 2, stx + 1450, sty + outht - rht * 5 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 7 + rht * 2, stx + 1450, sty + outht - rht * 7 + rht * 2);

        //verical lines for file specification

        g.DrawLine(gconst.BPEN, stx + 1031, sty + 628, stx + 1031, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + 585, stx + 1220, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1090, sty + 585, stx + 1090, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1300, sty + 585, stx + 1300, sty + 715);
        g.DrawLine(gconst.BPEN, stx + 1350, sty + 585, stx + 1350, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1450, sty + 585, stx + 1450, sty + 628);


        g.DrawLine(gconst.BPEN, stx + 1000, sty + outht - rht, stx + 400, sty + outht - rht);

        //top horizontal lines
        g.DrawLine(gconst.BPEN, stx, sty + 30, stx + outw - 10, sty + 30);
        g.DrawLine(gconst.BPEN, stx, sty + 140, stx + outw - 10, sty + 140);
        g.DrawLine(gconst.BPEN, stx, sty + 170, stx + outw - 10, sty + 170);

        // top vertical lines
        g.DrawLine(gconst.BPEN, stx + 95, sty + 30, stx + 95, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 30, stx + 200, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 330, sty + 30, stx + 330, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 430, sty, stx + 430, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 30, stx + 520, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 650, sty + 30, stx + 650, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 800, sty, stx + 800, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 900, sty, stx + 900, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 980, sty + 30, stx + 980, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1050, sty + 30, stx + 1050, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1190, sty, stx + 1190, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1280, sty + 30, stx + 1280, sty + 170);

        // write template text
        g.DrawString("                                    TANG DIMENSIONS                                                                                                                               GRINDING", gconst.TSIZE9, gconst.TXTBRUSH, L1);   // draw text
        g.DrawString("NORMAL            RM   DIM                CROPPING                    BODY                   TANG                  AT  SHOULDER               AT TIP (5MM                     (FORGING)              W                     T                   GRINDING                        TAPER              ", gconst.TSIZE9, gconst.TXTBRUSH, L2);
        g.DrawString("LENGTH            IN MM (TOL.            MOOD LENGTH           LENGTH               LENGTH              (A/F)                                  FROM SH.)                       PT. SIZE                                                                 POINT                            LENGTH             ", gconst.TSIZE9, gconst.TXTBRUSH, L3);
        g.DrawString("IN INCHES         W + 0.15,                  IN MM TOL.                  IN MM                   IN MM                   (TOL. + 0.5)                      (A/F)                                  PUNCHING)             + 0.5)               (+ 0.5)             SIZE                                IN MM                 ", gconst.TSIZE9, gconst.TXTBRUSH, L4);
        g.DrawString("                                 -                                                                                                                                          -                                                                                                            -                         -", gconst.TSIZE9, gconst.TXTBRUSH, L17);
        g.DrawString("                           T +0.1)                     (+1/-0.0)                                                        (TOL. +1)             AT EXACT                                                                   IN MM                                                                                                                                         ", gconst.TSIZE9, gconst.TXTBRUSH, L5);
        g.DrawString("                              -                               -                                                                                - ", gconst.TSIZE9, gconst.TXTBRUSH, L16);
        g.DrawString("                                        X                                                                                                                                      X                                       X                                                                                                                                                                                                           ", gconst.TSIZE9, gconst.TXTBRUSH, L6);
        ////g.DrawString("4                            12.1        1.4                 100                               100-112                   45                     4.9              3                        3            3                      8.4 - 9.0                     11.7                  3                4.85  -  5.45                        39 - 44                  100 - 102", gconst.TSIZE9, gconst.TXTBRUSH, L6);
        g.DrawString("RAW MATERAL SECTION  (mm) :               x", gconst.TSIZE9, gconst.TXTBRUSH, L8);
        g.DrawString("HARDNESS  (HRC) :  ", gconst.TSIZE9, gconst.TXTBRUSH, L9);
        g.DrawString("CUT STANDRAD                    CUT TYPE                           U/C                           E/C                         O/C", gconst.TSIZE9, gconst.TXTBRUSH, L10);
        //g.DrawString("GENRAL                                    BAST", gconst.TSIZE9, gconst.TXTBRUSH, L11);
        g.DrawString("CUT INCLINATION (DEG.) + 3                                                             UNCUT (mm) + 2", gconst.TSIZE9, gconst.TXTBRUSH, L12);
        g.DrawString("                                              -                                                                                          -", gconst.TSIZE9, gconst.TXTBRUSH, L15);
        g.DrawString("1", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 610);
        g.DrawString("       FILE SPACIFICATION                 -               1               -", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 610);
        g.DrawString("ITEM", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 635);
        g.DrawString("        DESCRPITION                 MATERIAL      DIFF       REMARKS", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 635);
        g.DrawString("U/C                           O/C                          E/C                         AT SHOULDER                               ON EDGE", gconst.TSIZE9, gconst.TXTBRUSH, L13);
        //g.DrawString("65                               50                           90                                  19                                                    9", gconst.TSIZE9, gconst.TXTBRUSH, L14);
        g.DrawString("S . NO .                         ALTERATIONS                                                                                                                                                        CHANGED  BY                                         DATE", gconst.TSIZE9, gconst.TXTBRUSH, L18);
        g.DrawString("                                  SURFACE FINISH                                                         GENERAL  TOLERANCE WHERE NOT SPECIFIED ", gconst.TSIZE9, gconst.TXTBRUSH, L19);
        g.DrawString("                               △                        △△                       △△△                                   0.10                     10.30                  30.100                  100.300                  300.100                       1000.2000", gconst.TSIZE9, gconst.TXTBRUSH, L20);
        g.DrawString(" ∼ ", gconst.TSIZE40, gconst.TXTBRUSH, 35, 705);
        g.DrawString("ROUGH              ROUGH                 MEDIUM                    FINE  ", gconst.TSIZE9, gconst.TXTBRUSH, L21);
        g.DrawString("                                                                                                                                     +0.10                      +0.2                    +0.3                     +0.5                        +0.8                              +1.2", gconst.TSIZE9, gconst.TXTBRUSH, L22);
        g.DrawString("GROUND           MACHINE              MACHINE                  MACHINE ", gconst.TSIZE9, gconst.TXTBRUSH, L23);
        g.DrawString("                                                                                                                                     -                               -                          -                           -                              -                                     -", gconst.TSIZE9, gconst.TXTBRUSH, L24);
        g.DrawString("JK  FILES  (India)", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 660);
        g.DrawString("LIMITED     THANE", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 680);
        g.DrawString("DATE", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 660);
        g.DrawString("DRAWN", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 680);
        g.DrawString("REVIEWED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 700);
        g.DrawString("APPROVED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 720);
        g.DrawString("DRAWING NO .", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 740);
        g.DrawString("PROJECTION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 709);
        g.DrawString("DESIGNATION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 740);

        //------------------------values-------------------------------------------
       // Profile prf = new Profile();
        g.DrawString(prf.FSZ_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 130, 170);
        g.DrawString(prf.RMT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 185, 170);
        g.DrawString(prf.CML_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250, 170);
        g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 365, 170);
        g.DrawString(prf.TL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 500, 170);
        g.DrawString(prf.TWS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 570, 170);
        g.DrawString(prf.TTS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 625, 170);
        g.DrawString(prf.TWT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 710, 170);
        g.DrawString(prf.TTT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 760, 170);
        g.DrawString(prf.FPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 850, 170);
        g.DrawString(prf.W_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 950, 170);
        g.DrawString(prf.T_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1050, 170);
        g.DrawString(prf.GPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1090, 170);
        g.DrawString(prf.TPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1220, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 230, 460);
        g.DrawString(prf.RMT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 290, 460);
        g.DrawString(prf.HRC_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 480);
        g.DrawString(prf.UCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 330, 545);
        g.DrawString(prf.ECT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 440, 545);
        g.DrawString(prf.OCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 540, 545);
        g.DrawString(prf.IUP_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35, 635);
        g.DrawString(prf.IOV_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 140, 635);
        g.DrawString(prf.IEG_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250, 635);
        g.DrawString(prf.USL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 380, 635);
        g.DrawString(prf.UEL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 550, 635);
        g.DrawString(prf.FNM_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1095, 750);
        g.DrawString(prf.PTN_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1250, 750);
        g.DrawString(prf.CSP_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 45, 548);
        g.DrawString(prf.CT_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 200, 548);
    }


    //Template for Half round
    public static void HalfRoundTemplate(Graphics g, Component comp, Profile prf)
    {
        Double t = 2.5;
        int stx = 20;
        int sty = 20;
        int outw = 1460;
        int outht = 760;
        int i;
        // define template text locations
        Point L1 = new Point(350, 30);
        Point L2 = new Point(35, 60);
        Point L3 = new Point(35, 85);
        Point L4 = new Point(35, 110);
        Point L5 = new Point(35, 135);
        Point L6 = new Point(35, 170);
        Point L7 = new Point(35, 175);
        Point L8 = new Point(35, 460);
        Point L9 = new Point(35, 480);
        Point L10 = new Point(35, 525);
        Point L11 = new Point(35, 545);
        Point L12 = new Point(35, 590);
        Point L13 = new Point(35, 610);
        Point L14 = new Point(35, 635);
        Point L15 = new Point(35, 593);
        Point L16 = new Point(35, 139);
        Point L17 = new Point(35, 114);
        Point L18 = new Point(35, 680);
        Point L19 = new Point(35, 700);
        Point L20 = new Point(35, 725);
        Point L21 = new Point(35, 740);
        Point L22 = new Point(35, 745);
        Point L23 = new Point(35, 748);
        Point L24 = new Point(35, 747);
        g.DrawRectangle(gconst.BPEN, stx, sty, outw, outht);
        stx = stx + 5;
        sty = sty + 5;
        int rht = 22;
        g.DrawRectangle(gconst.BPEN, stx, sty, outw - 10, outht - 10);
        for (i = 1; i < 7; i++)
        {
            g.DrawLine(gconst.BPEN, stx, (sty + outht - rht * i), stx + outw - 430, (sty + outht - rht * i));
        }

        // cut
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 12, 950, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 11, 950, rht);

        // Raw meatrial and hardness
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 15, 550, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 14, 550, rht);

        //CUT inclnation
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 9, 650, rht * 3);
        g.DrawRectangle(gconst.BPEN, stx + 730, sty + outht - rht * 9, 300, rht * 3);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht, stx + 650, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht * 2, stx + 650, sty + outht - rht * 9 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 730, sty + outht - rht * 9 + rht, stx + 1032, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx + 730, sty + outht - rht * 9 + rht * 2, stx + 1032, sty + outht - rht * 9 + rht * 2);
        // vertical lines for inclution
        g.DrawLine(gconst.BPEN, stx + 150, sty + 497, stx + 150, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 497, stx + 250, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 430, sty + 497, stx + 430, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 600, sty + 497, stx + 600, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 780, sty + 497, stx + 780, sty + 540);

        g.DrawLine(gconst.BPEN, stx + 140, sty + 584, stx + 140, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 300, sty + 584, stx + 300, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 480, sty + 584, stx + 480, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 880, sty + 584, stx + 880, sty + 628);



        g.DrawLine(gconst.BPEN, stx + 100, sty + 629, stx + 100, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 680, sty + 629, stx + 680, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 629, stx + 850, sty + 672);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 738, stx + 80, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 180, sty + 738, stx + 180, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 280, sty + 738, stx + 280, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 400, sty + 738, stx + 400, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 738, stx + 520, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 610, sty + 738, stx + 610, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 700, sty + 738, stx + 700, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 800, sty + 738, stx + 800, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 900, sty + 738, stx + 900, sty + 694);

        //FILE SPECEFICATION 
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 8, 416, rht);
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 7, 416, rht);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 6 + rht * 2, stx + 1450, sty + outht - rht * 6 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 4 + rht * 2, stx + 1450, sty + outht - rht * 4 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 3 + rht * 2, stx + 1450, sty + outht - rht * 3 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 5 + rht * 2, stx + 1450, sty + outht - rht * 5 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 7 + rht * 2, stx + 1450, sty + outht - rht * 7 + rht * 2);

        //verical lines for file specification

        g.DrawLine(gconst.BPEN, stx + 1031, sty + 628, stx + 1031, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + 585, stx + 1220, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1090, sty + 585, stx + 1090, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1300, sty + 585, stx + 1300, sty + 715);
        g.DrawLine(gconst.BPEN, stx + 1350, sty + 585, stx + 1350, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1450, sty + 585, stx + 1450, sty + 628);


        g.DrawLine(gconst.BPEN, stx + 1000, sty + outht - rht, stx + 400, sty + outht - rht);

        //top horizontal lines
        g.DrawLine(gconst.BPEN, stx, sty + 30, stx + outw - 10, sty + 30);
        g.DrawLine(gconst.BPEN, stx, sty + 140, stx + outw - 10, sty + 140);
        g.DrawLine(gconst.BPEN, stx, sty + 170, stx + outw - 10, sty + 170);

        // top vertical lines
        g.DrawLine(gconst.BPEN, stx + 95, sty + 30, stx + 95, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 30, stx + 200, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 330, sty + 30, stx + 330, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 430, sty, stx + 430, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 30, stx + 520, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 650, sty + 30, stx + 650, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 800, sty, stx + 800, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 900, sty, stx + 900, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 980, sty + 30, stx + 980, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1050, sty + 30, stx + 1050, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1190, sty, stx + 1190, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1280, sty + 30, stx + 1280, sty + 170);

        // write template text
        g.DrawString("                                    TANG DIMENSIONS                                                                                                                               GRINDING", gconst.TSIZE9, gconst.TXTBRUSH, L1);   // draw text
        g.DrawString("NORMAL            RM   DIM                CROPPING                    BODY                   TANG                  AT  SHOULDER               AT TIP (5MM                     (FORGING)              W                     T                   GRINDING                        TAPER              BODY  LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, L2);
        g.DrawString("LENGTH            IN MM (TOL.            MOOD LENGTH           LENGTH               LENGTH              (A/F)                                  FROM SH.)                       PT. SIZE                                                                 POINT                            LENGTH             AFTER FLAT SIDE", gconst.TSIZE9, gconst.TXTBRUSH, L3);
        g.DrawString("IN INCHES         W + 0.15,                  IN MM TOL.                  IN MM                   IN MM                   (TOL. + 0.5)                      (A/F)                                  PUNCHING)             + 0.5)               (+ 0.5)             SIZE                                IN MM                CUTTING BEFORE ", gconst.TSIZE9, gconst.TXTBRUSH, L4);
        g.DrawString("                                 -                                                                                                                                          -                                                                                                            -                         -", gconst.TSIZE9, gconst.TXTBRUSH, L17);
        g.DrawString("                           T +0.1)                     (+1/-0.0)                                                        (TOL. +1)             AT EXACT                                                                   IN MM                                                                                                                                         HARDINING (MM)", gconst.TSIZE9, gconst.TXTBRUSH, L5);
        g.DrawString("                              -                               -                                                                                - ", gconst.TSIZE9, gconst.TXTBRUSH, L16);
        g.DrawString("                                        X                                                                                                                                      X                                       X                                                                                                                                                                                                           ", gconst.TSIZE9, gconst.TXTBRUSH, L6);
        ////g.DrawString("4                            12.1        1.4                 100                               100-112                   45                     4.9              3                        3            3                      8.4 - 9.0                     11.7                  3                4.85  -  5.45                        39 - 44                  100 - 102", gconst.TSIZE9, gconst.TXTBRUSH, L6);
        g.DrawString("RAW MATERAL SECTION  (mm) :               x", gconst.TSIZE9, gconst.TXTBRUSH, L8);
        g.DrawString("HARDNESS  (HRC) :  ", gconst.TSIZE9, gconst.TXTBRUSH, L9);
        g.DrawString("CUT STANDRAD                    CUT TYPE", gconst.TSIZE9, gconst.TXTBRUSH, L10);
       // g.DrawString("GENRAL                                    BAST", gconst.TSIZE9, gconst.TXTBRUSH, L11);
        //g.DrawString("CUT INCLINATION (DEG.) + 3                                                             ", gconst.TSIZE9, gconst.TXTBRUSH, L12);
        //g.DrawString("                                              -                                                                                          -", gconst.TSIZE9, gconst.TXTBRUSH, L15);
        g.DrawString("1", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 610);
        g.DrawString("       FILE SPACIFICATION                 -               1               -", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 610);
        g.DrawString("ITEM", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 635);
        g.DrawString("        DESCRPITION                 MATERIAL      DIFF       REMARKS", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 635);
        //g.DrawString("U/C                           O/C                          E/C                         AT SHOULDER                               ON EDGE", gconst.TSIZE9, gconst.TXTBRUSH, L13);
        //g.DrawString("65                               50                           90                                  19                                                    9", gconst.TSIZE9, gconst.TXTBRUSH, L14);
        g.DrawString("S . NO .                         ALTERATIONS                                                                                                                                                        CHANGED  BY                                         DATE", gconst.TSIZE9, gconst.TXTBRUSH, L18);
        g.DrawString("                                  SURFACE FINISH                                                         GENERAL  TOLERANCE WHERE NOT SPECIFIED ", gconst.TSIZE9, gconst.TXTBRUSH, L19);
        g.DrawString("                               △                        △△                       △△△                                   0.10                     10.30                  30.100                  100.300                  300.100                       1000.2000", gconst.TSIZE9, gconst.TXTBRUSH, L20);
        g.DrawString(" ∼ ", gconst.TSIZE40, gconst.TXTBRUSH, 35, 705);
        g.DrawString("ROUGH              ROUGH                 MEDIUM                    FINE  ", gconst.TSIZE9, gconst.TXTBRUSH, L21);
        g.DrawString("                                                                                                                                     +0.10                      +0.2                    +0.3                     +0.5                        +0.8                              +1.2", gconst.TSIZE9, gconst.TXTBRUSH, L22);
        g.DrawString("GROUND           MACHINE              MACHINE                  MACHINE ", gconst.TSIZE9, gconst.TXTBRUSH, L23);
        g.DrawString("                                                                                                                                     -                               -                          -                           -                              -                                     -", gconst.TSIZE9, gconst.TXTBRUSH, L24);
        g.DrawString("JK  FILES  (India)", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 660);
        g.DrawString("LIMITED     THANE", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 680);
        g.DrawString("DATE", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 660);
        g.DrawString("DRAWN", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 680);
        g.DrawString("REVIEWED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 700);
        g.DrawString("APPROVED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 720);
        g.DrawString("DRAWING NO .", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 740);
        g.DrawString("PROJECTION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 709);
        g.DrawString("DESIGNATION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 740);

        //------------------------values-------------------------------------------
        g.DrawString(prf.FSZ_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 130, 170);
        g.DrawString(prf.RMT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 185, 170);
        g.DrawString(prf.CML_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250, 170);
        g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 365, 170);
        g.DrawString(prf.TL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 500, 170);
        g.DrawString(prf.TWS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 570, 170);
        g.DrawString(prf.TTS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 625, 170);
        g.DrawString(prf.TWT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 710, 170);
        g.DrawString(prf.TTT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 760, 170);
        g.DrawString(prf.FPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 850, 170);
        g.DrawString(prf.W_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 950, 170);
        g.DrawString(prf.T_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1050, 170);
        g.DrawString(prf.GPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1090, 170);
        g.DrawString(prf.TPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1220, 170);
        g.DrawString(prf.L_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1350, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 230, 460);
        g.DrawString(prf.RMT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 290, 460);
        g.DrawString(prf.HRC_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 480);
        g.DrawString(prf.UP1GN_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 350, 545);
        g.DrawString(prf.UP1GN_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 520, 545);
        g.DrawString(prf.OV1GN_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 680, 545);
        g.DrawString(prf.OV1GN_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 850, 545);
        g.DrawString(prf.IUPF_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 75, 635);
        g.DrawString(prf.IOVF_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 220, 635);
        g.DrawString(prf.IUPR_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 400, 635);
        g.DrawString(prf.IOVR_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 580, 635);
        g.DrawString(prf.USLR_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 800, 635);
        g.DrawString(prf.USLF_dispval_HR, gconst.TSIZE10, gconst.TXTBRUSH, 960, 635);
        g.DrawString(prf.FNM_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1095, 750);
        g.DrawString(prf.PTN_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1250, 750);
        g.DrawString(prf.CSP_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 45, 548);
        g.DrawString(prf.CT_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 200, 548);

        // text CUT INCLINATION AND UNCUT
        g.DrawString("CUT INCLINATION", gconst.TSIZE9, gconst.TXTBRUSH, 250, 590);
        g.DrawString("F/S UP CUT", gconst.TSIZE9, gconst.TXTBRUSH, 50, 610);
        g.DrawString("F/S OVER CUT", gconst.TSIZE9, gconst.TXTBRUSH, 200, 610);
        g.DrawString("R/S UP CUT", gconst.TSIZE9, gconst.TXTBRUSH, 380, 610);
        g.DrawString("R/S OVER CUT", gconst.TSIZE9, gconst.TXTBRUSH, 550, 610);
        g.DrawString("UNCUT", gconst.TSIZE9, gconst.TXTBRUSH, 880, 590);
        g.DrawString("R/S + 2 MM", gconst.TSIZE9, gconst.TXTBRUSH, 790, 610);
        g.DrawString("-", gconst.TSIZE9, gconst.TXTBRUSH, 815, 614);
        g.DrawString("F/S + 2 MM", gconst.TSIZE9, gconst.TXTBRUSH, 930, 610);
        g.DrawString("-", gconst.TSIZE9, gconst.TXTBRUSH, 953, 614);

        // TEXT FOR CUT 
        g.DrawString("F/S UPCUT/INCH", gconst.TSIZE9, gconst.TXTBRUSH, 320, 525);
        g.DrawString("R/S UPCUT/INCH", gconst.TSIZE9, gconst.TXTBRUSH, 500, 525);
        g.DrawString("F/S OVERCUT/INCH", gconst.TSIZE9, gconst.TXTBRUSH, 660, 525);
        g.DrawString("R/S OVERCUT/INCH", gconst.TSIZE9, gconst.TXTBRUSH, 830, 525);
    }

    public static void RoundTemplate(Graphics g, Component comp, Profile prf)
    {
        Double t = 2.5;
        int stx = 20;
        int sty = 20;
        int outw = 1460;
        int outht = 760;
        int i;
        // define template text locations
        Point L1 = new Point(350, 30);
        Point L2 = new Point(35, 60);
        Point L3 = new Point(35, 85);
        Point L4 = new Point(35, 110);
        Point L5 = new Point(35, 135);
        Point L6 = new Point(35, 170);
        Point L7 = new Point(35, 175);
        Point L8 = new Point(35, 460);
        Point L9 = new Point(35, 480);
        Point L10 = new Point(35, 525);
        Point L11 = new Point(35, 545);
        Point L12 = new Point(35, 590);
        Point L13 = new Point(35, 610);
        Point L14 = new Point(35, 635);
        Point L15 = new Point(35, 593);
        Point L16 = new Point(35, 139);
        Point L17 = new Point(35, 114);
        Point L18 = new Point(35, 680);
        Point L19 = new Point(35, 700);
        Point L20 = new Point(35, 725);
        Point L21 = new Point(35, 740);
        Point L22 = new Point(35, 745);
        Point L23 = new Point(35, 748);
        Point L24 = new Point(35, 747);
        g.DrawRectangle(gconst.BPEN, stx, sty, outw, outht);
        stx = stx + 5;
        sty = sty + 5;
        int rht = 22;
        g.DrawRectangle(gconst.BPEN, stx, sty, outw - 10, outht - 10);
        for (i = 1; i < 7; i++)
        {
            g.DrawLine(gconst.BPEN, stx, (sty + outht - rht * i), stx + outw - 430, (sty + outht - rht * i));
        }

        // cut
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 12, 600, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 11, 600, rht);

        g.DrawRectangle(gconst.BPEN, stx + 610, sty + outht - rht * 13, 300, rht);
        g.DrawRectangle(gconst.BPEN, stx + 610, sty + outht - rht * 12, 300, rht);
        g.DrawRectangle(gconst.BPEN, stx + 610, sty + outht - rht * 11, 300, rht);

        // Raw meatrial and hardness
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 15, 450, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 14, 450, rht);

        //CUT inclnation
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 9, 300, rht * 3);
        g.DrawRectangle(gconst.BPEN, stx + 350, sty + outht - rht * 9, 200, rht * 3);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht, stx + 300, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht * 2, stx + 300, sty + outht - rht * 9 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 350, sty + outht - rht * 9 + rht, stx + 550, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx + 350, sty + outht - rht * 9 + rht * 2, stx + 550, sty + outht - rht * 9 + rht * 2);

        // vertical lines for inclution
        g.DrawLine(gconst.BPEN, stx + 150, sty + 497, stx + 150, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 497, stx + 250, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 430, sty + 497, stx + 430, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 750, sty + 497, stx + 750, sty + 540);
        //g.DrawLine(gconst.BPEN, stx + 600, sty + 497, stx + 600, sty + 540);
        //g.DrawLine(gconst.BPEN, stx + 780, sty + 497, stx + 780, sty + 540);

        g.DrawLine(gconst.BPEN, stx + 140, sty + 584, stx + 140, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 300, sty + 584, stx + 300, sty + 628);
        //g.DrawLine(gconst.BPEN, stx + 500, sty + 584, stx + 500, sty + 628);



        g.DrawLine(gconst.BPEN, stx + 100, sty + 629, stx + 100, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 680, sty + 629, stx + 680, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 629, stx + 850, sty + 672);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 738, stx + 80, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 180, sty + 738, stx + 180, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 280, sty + 738, stx + 280, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 400, sty + 738, stx + 400, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 738, stx + 520, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 610, sty + 738, stx + 610, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 700, sty + 738, stx + 700, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 800, sty + 738, stx + 800, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 900, sty + 738, stx + 900, sty + 694);

        //FILE SPECEFICATION 
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 8, 416, rht);
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 7, 416, rht);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 6 + rht * 2, stx + 1450, sty + outht - rht * 6 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 4 + rht * 2, stx + 1450, sty + outht - rht * 4 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 3 + rht * 2, stx + 1450, sty + outht - rht * 3 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 5 + rht * 2, stx + 1450, sty + outht - rht * 5 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 7 + rht * 2, stx + 1450, sty + outht - rht * 7 + rht * 2);

        //verical lines for file specification

        g.DrawLine(gconst.BPEN, stx + 1031, sty + 628, stx + 1031, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + 585, stx + 1220, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1090, sty + 585, stx + 1090, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1300, sty + 585, stx + 1300, sty + 715);
        g.DrawLine(gconst.BPEN, stx + 1350, sty + 585, stx + 1350, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1450, sty + 585, stx + 1450, sty + 628);


        g.DrawLine(gconst.BPEN, stx + 1000, sty + outht - rht, stx + 400, sty + outht - rht);

        //top horizontal lines
        g.DrawLine(gconst.BPEN, stx, sty + 30, stx + outw - 10, sty + 30);
        g.DrawLine(gconst.BPEN, stx, sty + 140, stx + outw - 10, sty + 140);
        g.DrawLine(gconst.BPEN, stx, sty + 170, stx + outw - 10, sty + 170);

        // top vertical lines
        g.DrawLine(gconst.BPEN, stx + 95, sty + 30, stx + 95, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 30, stx + 200, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 330, sty + 30, stx + 330, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 430, sty, stx + 430, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 30, stx + 520, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 630, sty + 30, stx + 630, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 750, sty + 30, stx + 750, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 30, stx + 850, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 950, sty, stx + 950, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1050, sty + 30, stx + 1050, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1190, sty, stx + 1190, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1280, sty + 30, stx + 1280, sty + 170);

        g.DrawString("RAW MATERAL SECTION  (mm) :               ", gconst.TSIZE9, gconst.TXTBRUSH, L8);
        g.DrawString("HARDNESS  (HRC) :  ", gconst.TSIZE9, gconst.TXTBRUSH, L9);
        g.DrawString("CUT STANDRAD                    CUT TYPE", gconst.TSIZE9, gconst.TXTBRUSH, L10);
        //g.DrawString("GENRAL                                    BAST", gconst.TSIZE9, gconst.TXTBRUSH, L11);
        g.DrawString("1", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 610);
        g.DrawString("       FILE SPACIFICATION                 -               1               -", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 610);
        g.DrawString("ITEM", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 635);
        g.DrawString("        DESCRPITION                 MATERIAL      DIFF       REMARKS", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 635);
        g.DrawString("S . NO .                         ALTERATIONS                                                                                                                                                        CHANGED  BY                                         DATE", gconst.TSIZE9, gconst.TXTBRUSH, L18);
        g.DrawString("                                  SURFACE FINISH                                                         GENERAL  TOLERANCE WHERE NOT SPECIFIED ", gconst.TSIZE9, gconst.TXTBRUSH, L19);
        g.DrawString("                               △                        △△                       △△△                                   0.10                     10.30                  30.100                  100.300                  300.100                       1000.2000", gconst.TSIZE9, gconst.TXTBRUSH, L20);
        g.DrawString(" ∼ ", gconst.TSIZE40, gconst.TXTBRUSH, 35, 705);
        g.DrawString("ROUGH              ROUGH                 MEDIUM                    FINE  ", gconst.TSIZE9, gconst.TXTBRUSH, L21);
        g.DrawString("                                                                                                                                     +0.10                      +0.2                    +0.3                     +0.5                        +0.8                              +1.2", gconst.TSIZE9, gconst.TXTBRUSH, L22);
        g.DrawString("GROUND           MACHINE              MACHINE                  MACHINE ", gconst.TSIZE9, gconst.TXTBRUSH, L23);
        g.DrawString("                                                                                                                                     -                               -                          -                           -                              -                                     -", gconst.TSIZE9, gconst.TXTBRUSH, L24);
        g.DrawString("JK  FILES  (India)", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 660);
        g.DrawString("LIMITED     THANE", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 680);
        g.DrawString("DATE", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 660);
        g.DrawString("DRAWN", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 680);
        g.DrawString("REVIEWED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 700);
        g.DrawString("APPROVED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 720);
        g.DrawString("DRAWING NO .", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 740);
        g.DrawString("PROJECTION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 709);
        g.DrawString("DESIGNATION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 740);

        //------------------------values-------------------------------------------

        g.DrawString(prf.FSZ_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35, 170);
        g.DrawString(prf.RMD_dispval_R, gconst.TSIZE10, gconst.TXTBRUSH, 150, 170);
        g.DrawString(prf.CML_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250, 170);
        g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 365, 170);
        g.DrawString(prf.TL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 500, 170);
        g.DrawString(prf.TWS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 560, 170);
        g.DrawString("X", gconst.TSIZE9, gconst.TXTBRUSH, 590, 170);
        g.DrawString(prf.TTS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 610, 170);
        g.DrawString(prf.TWT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 680, 170);
        g.DrawString("X", gconst.TSIZE9, gconst.TXTBRUSH, 700, 170);
        g.DrawString(prf.TTT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 720, 170);
        g.DrawString(prf.ATN_dispval_R, gconst.TSIZE10, gconst.TXTBRUSH, 800, 170);
        g.DrawString(prf.RSH_dispval_R, gconst.TSIZE10, gconst.TXTBRUSH, 900, 170);
        g.DrawString(prf.D_dispval_R, gconst.TSIZE10, gconst.TXTBRUSH, 1030, 170);
        g.DrawString(prf.GPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1100, 170);
        g.DrawString(prf.TPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1240, 170);
        g.DrawString(prf.L_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1350, 170);
        g.DrawString(prf.RMD_dispval_R, gconst.TSIZE10, gconst.TXTBRUSH, 230, 460);
        g.DrawString(prf.HRC_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 480);
        g.DrawString(prf.UCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 350, 545);
        g.DrawString(prf.OCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 520, 545);
        g.DrawString(prf.OCR_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 700, 545);
        g.DrawString(prf.UCR_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 830, 545);
        g.DrawString(prf.IUP_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 55, 635);
        g.DrawString(prf.IOV_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 635);
        g.DrawString(prf.USL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 430, 635);
        g.DrawString(prf.FNM_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1095, 750);
        g.DrawString(prf.PTN_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1250, 750);
        g.DrawString(prf.CSP_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 45, 548);
        g.DrawString(prf.CT_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 200, 548);

        // text for top 
        g.DrawString("TANG DIMENSIONS", gconst.TSIZE9, gconst.TXTBRUSH, 680, 30);
        g.DrawString("GRINDING", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 30);

        g.DrawString("NORMAL", gconst.TSIZE9, gconst.TXTBRUSH, 35, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 35, 85);
        g.DrawString("IN INCHES", gconst.TSIZE9, gconst.TXTBRUSH, 35, 110);

        g.DrawString("RM   DIM", gconst.TSIZE9, gconst.TXTBRUSH, 140, 60);
        g.DrawString("IN MM (TOL.", gconst.TSIZE9, gconst.TXTBRUSH, 140, 85);
        g.DrawString("W + 0.15,", gconst.TSIZE9, gconst.TXTBRUSH, 140, 110);
        g.DrawString("T +0.1)", gconst.TSIZE9, gconst.TXTBRUSH, 140, 135);

        g.DrawString("CROPPING", gconst.TSIZE9, gconst.TXTBRUSH, 250, 60);
        g.DrawString("MOOD LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 250, 85);
        g.DrawString("IN MM TOL.", gconst.TSIZE9, gconst.TXTBRUSH, 250, 110);
        g.DrawString("(+1/-0.0)", gconst.TSIZE9, gconst.TXTBRUSH, 250, 135);

        g.DrawString("BODY", gconst.TSIZE9, gconst.TXTBRUSH, 380, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 380, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 380, 110);

        g.DrawString("TANG", gconst.TSIZE9, gconst.TXTBRUSH, 480, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 480, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 480, 110);
        g.DrawString("(TOL. +1)", gconst.TSIZE9, gconst.TXTBRUSH, 480, 135);

        g.DrawString("AT  SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 550, 60);
        g.DrawString("(A/F)", gconst.TSIZE9, gconst.TXTBRUSH, 550, 85);
        g.DrawString("(TOL. + 0.5)", gconst.TSIZE9, gconst.TXTBRUSH, 550, 110);

        g.DrawString("AT TIP (5MM", gconst.TSIZE9, gconst.TXTBRUSH, 670, 60);
        g.DrawString("FROM SH.)", gconst.TSIZE9, gconst.TXTBRUSH, 670, 85);
        g.DrawString("(A/F)", gconst.TSIZE9, gconst.TXTBRUSH, 670, 110);
        g.DrawString("(TOL. +3)", gconst.TSIZE9, gconst.TXTBRUSH, 670, 135);

        g.DrawString("TANG ANG.", gconst.TSIZE9, gconst.TXTBRUSH, 780, 60);
        g.DrawString("IN DEG.", gconst.TSIZE9, gconst.TXTBRUSH, 780, 85);
        g.DrawString("RADIUS", gconst.TSIZE9, gconst.TXTBRUSH, 890, 60);
        g.DrawString("AT", gconst.TSIZE9, gconst.TXTBRUSH, 890, 85);
        g.DrawString("SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 890, 110);
        g.DrawString("IN INCH", gconst.TSIZE9, gconst.TXTBRUSH, 890, 135);

        g.DrawString("'D'", gconst.TSIZE9, gconst.TXTBRUSH, 1020, 60);
        g.DrawString("BODY", gconst.TSIZE9, gconst.TXTBRUSH, 1020, 85);
        g.DrawString("(+ 0.5) ", gconst.TSIZE9, gconst.TXTBRUSH, 1020, 110);

        g.DrawString("GRINDING PT.", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 60);
        g.DrawString("SIZE AT EXACT", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 85);
        g.DrawString("LENGTH IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 110);
        g.DrawString("(TOL. +3)", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 135);

        g.DrawString("TAPER", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 110);
        g.DrawString("(TOL. +1)", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 135);

        g.DrawString("BODY  LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 60);
        g.DrawString("AFTER FLAT SIDE", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 85);
        g.DrawString("CUTTING BEFORE", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 110);
        g.DrawString("HARDINING (MM)", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 135);

        // text CUT INCLINATION AND UNCUT
        g.DrawString("CUT INCLINATION (mm)+2", gconst.TSIZE9, gconst.TXTBRUSH, 90, 590);
        g.DrawString("UPCUT", gconst.TSIZE9, gconst.TXTBRUSH, 50, 610);
        g.DrawString("OVERCUT", gconst.TSIZE9, gconst.TXTBRUSH, 200, 610);

        g.DrawString("UNCUT (mm) +2", gconst.TSIZE9, gconst.TXTBRUSH, 400, 590);
        g.DrawString("ON SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 400, 610);
        g.DrawString("-", gconst.TSIZE9, gconst.TXTBRUSH, 478, 594);

        // TEXT FOR CUT 
        g.DrawString("UPCUT/INCH", gconst.TSIZE9, gconst.TXTBRUSH, 360, 525);
        g.DrawString("OVERCUT/INCH", gconst.TSIZE9, gconst.TXTBRUSH, 520, 525);
        g.DrawString("OVER CUT ROWS", gconst.TSIZE9, gconst.TXTBRUSH, 660, 525);
        g.DrawString("UP CUT ROWS", gconst.TSIZE9, gconst.TXTBRUSH, 780, 525);
        g.DrawString("NO.OF ROWS FOR ROUND FILE", gconst.TSIZE9, gconst.TXTBRUSH, 700, 503);

        // Note 
        g.DrawString("DEPTH OF CUT IS CALCLUATED BASED ON", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 560);
        g.DrawString("TEETH PER INCH AND IS MIN. 52% & PITCH FOR SINGLE", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 575);
        g.DrawString("CUT FILES AND MIN.55% FOR DOUBLE CUT FILE.", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 593);
    }

    public static void SquareTemplate(Graphics g, Component comp, Profile prf)
    {
        Double t = 2.5;
        int stx = 20;
        int sty = 20;
        int outw = 1460;
        int outht = 760;
        int i;
        // define template text locations
        Point L1 = new Point(350, 30);
        Point L2 = new Point(35, 60);
        Point L3 = new Point(35, 85);
        Point L4 = new Point(35, 110);
        Point L5 = new Point(35, 135);
        Point L6 = new Point(35, 170);
        Point L7 = new Point(35, 175);
        Point L8 = new Point(35, 460);
        Point L9 = new Point(35, 480);
        Point L10 = new Point(35, 525);
        Point L11 = new Point(35, 545);
        Point L12 = new Point(35, 590);
        Point L13 = new Point(35, 610);
        Point L14 = new Point(35, 635);
        Point L15 = new Point(35, 593);
        Point L16 = new Point(35, 139);
        Point L17 = new Point(35, 114);
        Point L18 = new Point(35, 680);
        Point L19 = new Point(35, 700);
        Point L20 = new Point(35, 725);
        Point L21 = new Point(35, 740);
        Point L22 = new Point(35, 745);
        Point L23 = new Point(35, 748);
        Point L24 = new Point(35, 747);
        g.DrawRectangle(gconst.BPEN, stx, sty, outw, outht);
        stx = stx + 5;
        sty = sty + 5;
        int rht = 22;
        g.DrawRectangle(gconst.BPEN, stx, sty, outw - 10, outht - 10);
        for (i = 1; i < 7; i++)
        {
            g.DrawLine(gconst.BPEN, stx, (sty + outht - rht * i), stx + outw - 430, (sty + outht - rht * i));
        }

        // cut
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 12, 600, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 11, 600, rht);

        // Raw meatrial and hardness
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 15, 450, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 14, 450, rht);

        //CUT inclnation
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 9, 300, rht * 3);
        g.DrawRectangle(gconst.BPEN, stx + 350, sty + outht - rht * 9, 200, rht * 3);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht, stx + 300, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht * 2, stx + 300, sty + outht - rht * 9 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 350, sty + outht - rht * 9 + rht, stx + 550, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx + 350, sty + outht - rht * 9 + rht * 2, stx + 550, sty + outht - rht * 9 + rht * 2);

        // vertical lines for inclution
        g.DrawLine(gconst.BPEN, stx + 150, sty + 497, stx + 150, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 497, stx + 250, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 430, sty + 497, stx + 430, sty + 540);
        //g.DrawLine(gconst.BPEN, stx + 600, sty + 497, stx + 600, sty + 540);
        //g.DrawLine(gconst.BPEN, stx + 780, sty + 497, stx + 780, sty + 540);

        g.DrawLine(gconst.BPEN, stx + 140, sty + 584, stx + 140, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 300, sty + 584, stx + 300, sty + 628);
        //g.DrawLine(gconst.BPEN, stx + 500, sty + 584, stx + 500, sty + 628);



        g.DrawLine(gconst.BPEN, stx + 100, sty + 629, stx + 100, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 680, sty + 629, stx + 680, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 629, stx + 850, sty + 672);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 738, stx + 80, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 180, sty + 738, stx + 180, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 280, sty + 738, stx + 280, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 400, sty + 738, stx + 400, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 738, stx + 520, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 610, sty + 738, stx + 610, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 700, sty + 738, stx + 700, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 800, sty + 738, stx + 800, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 900, sty + 738, stx + 900, sty + 694);

        //FILE SPECEFICATION 
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 8, 416, rht);
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 7, 416, rht);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 6 + rht * 2, stx + 1450, sty + outht - rht * 6 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 4 + rht * 2, stx + 1450, sty + outht - rht * 4 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 3 + rht * 2, stx + 1450, sty + outht - rht * 3 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 5 + rht * 2, stx + 1450, sty + outht - rht * 5 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 7 + rht * 2, stx + 1450, sty + outht - rht * 7 + rht * 2);

        //verical lines for file specification

        g.DrawLine(gconst.BPEN, stx + 1031, sty + 628, stx + 1031, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + 585, stx + 1220, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1090, sty + 585, stx + 1090, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1300, sty + 585, stx + 1300, sty + 715);
        g.DrawLine(gconst.BPEN, stx + 1350, sty + 585, stx + 1350, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1450, sty + 585, stx + 1450, sty + 628);


        g.DrawLine(gconst.BPEN, stx + 1000, sty + outht - rht, stx + 400, sty + outht - rht);

        //top horizontal lines
        g.DrawLine(gconst.BPEN, stx, sty + 30, stx + outw - 10, sty + 30);
        g.DrawLine(gconst.BPEN, stx, sty + 140, stx + outw - 10, sty + 140);
        g.DrawLine(gconst.BPEN, stx, sty + 170, stx + outw - 10, sty + 170);

        // top vertical lines
        g.DrawLine(gconst.BPEN, stx + 95, sty + 30, stx + 95, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 200, sty + 30, stx + 200, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 330, sty + 30, stx + 330, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 430, sty, stx + 430, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 30, stx + 520, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 630, sty + 30, stx + 630, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 750, sty + 30, stx + 750, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 30, stx + 850, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 950, sty, stx + 950, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1050, sty + 30, stx + 1050, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1190, sty, stx + 1190, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1280, sty + 30, stx + 1280, sty + 170);

        g.DrawString("RAW MATERAL SECTION  (mm) :               ", gconst.TSIZE9, gconst.TXTBRUSH, L8);
        g.DrawString("HARDNESS  (HRC) :  ", gconst.TSIZE9, gconst.TXTBRUSH, L9);
        g.DrawString("CUT STANDRAD                    CUT TYPE", gconst.TSIZE9, gconst.TXTBRUSH, L10);
        //g.DrawString("GENRAL                                    BAST", gconst.TSIZE9, gconst.TXTBRUSH, L11);
        g.DrawString("1", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 610);
        g.DrawString("       FILE SPACIFICATION                 -               1               -", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 610);
        g.DrawString("ITEM", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 635);
        g.DrawString("        DESCRPITION                 MATERIAL      DIFF       REMARKS", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 635);
        g.DrawString("S . NO .                         ALTERATIONS                                                                                                                                                        CHANGED  BY                                         DATE", gconst.TSIZE9, gconst.TXTBRUSH, L18);
        g.DrawString("                                  SURFACE FINISH                                                         GENERAL  TOLERANCE WHERE NOT SPECIFIED ", gconst.TSIZE9, gconst.TXTBRUSH, L19);
        g.DrawString("                               △                        △△                       △△△                                   0.10                     10.30                  30.100                  100.300                  300.100                       1000.2000", gconst.TSIZE9, gconst.TXTBRUSH, L20);
        g.DrawString(" ∼ ", gconst.TSIZE40, gconst.TXTBRUSH, 35, 705);
        g.DrawString("ROUGH              ROUGH                 MEDIUM                    FINE  ", gconst.TSIZE9, gconst.TXTBRUSH, L21);
        g.DrawString("                                                                                                                                     +0.10                      +0.2                    +0.3                     +0.5                        +0.8                              +1.2", gconst.TSIZE9, gconst.TXTBRUSH, L22);
        g.DrawString("GROUND           MACHINE              MACHINE                  MACHINE ", gconst.TSIZE9, gconst.TXTBRUSH, L23);
        g.DrawString("                                                                                                                                     -                               -                          -                           -                              -                                     -", gconst.TSIZE9, gconst.TXTBRUSH, L24);
        g.DrawString("JK  FILES  (India)", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 660);
        g.DrawString("LIMITED     THANE", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 680);
        g.DrawString("DATE", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 660);
        g.DrawString("DRAWN", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 680);
        g.DrawString("REVIEWED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 700);
        g.DrawString("APPROVED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 720);
        g.DrawString("DRAWING NO .", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 740);
        g.DrawString("PROJECTION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 709);
        g.DrawString("DESIGNATION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 740);

        //------------------------values-------------------------------------------
      
        g.DrawString(prf.FSZ_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 150, 170);
        g.DrawString(prf.CML_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 250, 170);
        g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 365, 170);
        g.DrawString(prf.TL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 500, 170);
        g.DrawString(prf.TWS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 560, 170);
        g.DrawString("X", gconst.TSIZE9, gconst.TXTBRUSH, 590, 170);
        g.DrawString(prf.TTS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 610, 170);
        g.DrawString(prf.TWT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 680, 170);
        g.DrawString("X", gconst.TSIZE9, gconst.TXTBRUSH, 700, 170);
        g.DrawString(prf.TTT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 720, 170);
        g.DrawString(prf.ASH_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 800, 170);
        g.DrawString(prf.RSH_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 900, 170);
        g.DrawString(prf.D_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1030, 170);
        g.DrawString(prf.GPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1100, 170);
        g.DrawString(prf.TPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1240, 170);
        g.DrawString(prf.L_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1350, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 230, 460);
        g.DrawString(prf.HRC_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 480);
        g.DrawString(prf.UCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 350, 545);
        g.DrawString(prf.OCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 520, 545);
        g.DrawString(prf.CSP_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 50, 545);
        g.DrawString(prf.CT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 545);
        g.DrawString(prf.IUP_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 55, 635);
        g.DrawString(prf.IOV_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 635);
        g.DrawString(prf.USL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 430, 635);
        g.DrawString(prf.FNM_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1095, 750);
        g.DrawString(prf.PTN_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1250, 750);
       


        // text for top 
        g.DrawString("TANG DIMENSIONS", gconst.TSIZE9, gconst.TXTBRUSH, 680, 30);
        g.DrawString("GRINDING", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 30);
        g.DrawString("NORMAL", gconst.TSIZE9, gconst.TXTBRUSH, 35, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 35, 85);
        g.DrawString("IN INCHES", gconst.TSIZE9, gconst.TXTBRUSH, 35, 110);
        g.DrawString("RM   DIM", gconst.TSIZE9, gconst.TXTBRUSH, 140, 60);
        g.DrawString("IN MM (TOL.", gconst.TSIZE9, gconst.TXTBRUSH, 140, 85);
        g.DrawString("W + 0.15,", gconst.TSIZE9, gconst.TXTBRUSH, 140, 110);
        g.DrawString("T +0.1)", gconst.TSIZE9, gconst.TXTBRUSH, 140, 135);
        g.DrawString("CROPPING", gconst.TSIZE9, gconst.TXTBRUSH, 250, 60);
        g.DrawString("MOOD LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 250, 85);
        g.DrawString("IN MM TOL.", gconst.TSIZE9, gconst.TXTBRUSH, 250, 110);
        g.DrawString("(+1/-0.0)", gconst.TSIZE9, gconst.TXTBRUSH, 250, 135);
        g.DrawString("BODY", gconst.TSIZE9, gconst.TXTBRUSH, 380, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 380, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 380, 110);
        g.DrawString("TANG", gconst.TSIZE9, gconst.TXTBRUSH, 480, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 480, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 480, 110);
        g.DrawString("(TOL. +1)", gconst.TSIZE9, gconst.TXTBRUSH, 480, 135);
        g.DrawString("AT  SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 550, 60);
        g.DrawString("(A/F)", gconst.TSIZE9, gconst.TXTBRUSH, 550, 85);
        g.DrawString("(TOL. + 0.5)", gconst.TSIZE9, gconst.TXTBRUSH, 550, 110);
        g.DrawString("AT TIP (5MM", gconst.TSIZE9, gconst.TXTBRUSH, 670, 60);
        g.DrawString("FROM SH.)", gconst.TSIZE9, gconst.TXTBRUSH, 670, 85);
        g.DrawString("(A/F)", gconst.TSIZE9, gconst.TXTBRUSH, 670, 110);
        g.DrawString("(TOL. +3)", gconst.TSIZE9, gconst.TXTBRUSH, 670, 135);
        g.DrawString("TANG ANG.", gconst.TSIZE9, gconst.TXTBRUSH, 780, 60);
        g.DrawString("IN DEG.", gconst.TSIZE9, gconst.TXTBRUSH, 780, 85);
        g.DrawString("RADIUS", gconst.TSIZE9, gconst.TXTBRUSH, 890, 60);
        g.DrawString("AT", gconst.TSIZE9, gconst.TXTBRUSH, 890, 85);
        g.DrawString("SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 890, 110);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 890, 135);
        g.DrawString("'D'", gconst.TSIZE9, gconst.TXTBRUSH, 1020, 60);
        g.DrawString("BODY", gconst.TSIZE9, gconst.TXTBRUSH, 1020, 85);
        g.DrawString("(+ 0.5) ", gconst.TSIZE9, gconst.TXTBRUSH, 1020, 110);
        g.DrawString("GRINDING PT.", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 60);
        g.DrawString("SIZE AT EXACT", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 85);
        g.DrawString("LENGTH IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 110);
        g.DrawString("(TOL. +3)", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 135);
        g.DrawString("TAPER", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 110);
        g.DrawString("(TOL. +1)", gconst.TSIZE9, gconst.TXTBRUSH, 1240, 135);
        g.DrawString("BODY  LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 60);
        g.DrawString("AFTER FLAT SIDE", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 85);
        g.DrawString("CUTTING BEFORE", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 110);
        g.DrawString("HARDINING (MM)", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 135);
        // text CUT INCLINATION AND UNCUT
        g.DrawString("CUT INCLINATION (mm)+2", gconst.TSIZE9, gconst.TXTBRUSH, 90, 590);
        g.DrawString("U/C", gconst.TSIZE9, gconst.TXTBRUSH, 50, 610);
        g.DrawString("O/C", gconst.TSIZE9, gconst.TXTBRUSH, 200, 610);

        g.DrawString("UNCUT (mm) +2", gconst.TSIZE9, gconst.TXTBRUSH, 400, 590);
        g.DrawString("AT SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 400, 610);
        g.DrawString("-", gconst.TSIZE9, gconst.TXTBRUSH, 478, 594);

        // TEXT FOR CUT 
        g.DrawString("U/C", gconst.TSIZE9, gconst.TXTBRUSH, 360, 525);
        g.DrawString("O/C", gconst.TSIZE9, gconst.TXTBRUSH, 520, 525);

        // Note 
        g.DrawString("DEPTH OF CUT IS CALCLUATED BASED ON", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 560);
        g.DrawString("TEETH PER INCH AND IS MIN. 52% & PITCH FOR SINGLE", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 575);
        g.DrawString("CUT FILES AND MIN.55% FOR DOUBLE CUT FILE.", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 593);

    }

    public static void TaperTemplate(Graphics g, Component comp, Profile prf)
    {
        Double t = 2.5;
        int stx = 20;
        int sty = 20;
        int outw = 1460;
        int outht = 760;
        int i;
        // define template text locations
        Point L1 = new Point(350, 30);
        Point L2 = new Point(35, 60);
        Point L3 = new Point(35, 85);
        Point L4 = new Point(35, 110);
        Point L5 = new Point(35, 135);
        Point L6 = new Point(35, 170);
        Point L7 = new Point(35, 175);
        Point L8 = new Point(35, 460);
        Point L9 = new Point(35, 480);
        Point L10 = new Point(35, 525);
        Point L11 = new Point(35, 545);
        Point L12 = new Point(35, 590);
        Point L13 = new Point(35, 610);
        Point L14 = new Point(35, 635);
        Point L15 = new Point(35, 593);
        Point L16 = new Point(35, 139);
        Point L17 = new Point(35, 114);
        Point L18 = new Point(35, 680);
        Point L19 = new Point(35, 700);
        Point L20 = new Point(35, 725);
        Point L21 = new Point(35, 740);
        Point L22 = new Point(35, 745);
        Point L23 = new Point(35, 748);
        Point L24 = new Point(35, 747);
        g.DrawRectangle(gconst.BPEN, stx, sty, outw, outht);
        stx = stx + 5;
        sty = sty + 5;
        int rht = 22;
        g.DrawRectangle(gconst.BPEN, stx, sty, outw - 10, outht - 10);
        for (i = 1; i < 7; i++)
        {
            g.DrawLine(gconst.BPEN, stx, (sty + outht - rht * i), stx + outw - 430, (sty + outht - rht * i));
        }

        // cut
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 12, 700, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 11, 700, rht);

        // Raw meatrial and hardness
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 15, 450, rht);
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 14, 450, rht);

        //CUT inclnation
        g.DrawRectangle(gconst.BPEN, stx, sty + outht - rht * 9, 400, rht * 3);
        g.DrawRectangle(gconst.BPEN, stx + 440, sty + outht - rht * 9, 300, rht * 3);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht, stx + 400, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx, sty + outht - rht * 9 + rht * 2, stx + 400, sty + outht - rht * 9 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 440, sty + outht - rht * 9 + rht, stx + 740, sty + outht - rht * 9 + rht);
        g.DrawLine(gconst.BPEN, stx + 440, sty + outht - rht * 9 + rht * 2, stx + 740, sty + outht - rht * 9 + rht * 2);

        // vertical lines for inclution
        g.DrawLine(gconst.BPEN, stx + 150, sty + 497, stx + 150, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 497, stx + 250, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 430, sty + 497, stx + 430, sty + 540);
        g.DrawLine(gconst.BPEN, stx + 560, sty + 497, stx + 560, sty + 540);
        //g.DrawLine(gconst.BPEN, stx + 600, sty + 497, stx + 600, sty + 540);
        //g.DrawLine(gconst.BPEN, stx + 780, sty + 497, stx + 780, sty + 540);

        g.DrawLine(gconst.BPEN, stx + 140, sty + 584, stx + 140, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 300, sty + 584, stx + 300, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 600, sty + 584, stx + 600, sty + 628);
        //g.DrawLine(gconst.BPEN, stx + 500, sty + 584, stx + 500, sty + 628);



        g.DrawLine(gconst.BPEN, stx + 100, sty + 629, stx + 100, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 680, sty + 629, stx + 680, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 850, sty + 629, stx + 850, sty + 672);

        g.DrawLine(gconst.BPEN, stx + 80, sty + 738, stx + 80, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 180, sty + 738, stx + 180, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 280, sty + 738, stx + 280, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 400, sty + 738, stx + 400, sty + 672);
        g.DrawLine(gconst.BPEN, stx + 520, sty + 738, stx + 520, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 610, sty + 738, stx + 610, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 700, sty + 738, stx + 700, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 800, sty + 738, stx + 800, sty + 694);
        g.DrawLine(gconst.BPEN, stx + 900, sty + 738, stx + 900, sty + 694);

        //FILE SPECEFICATION 
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 8, 416, rht);
        g.DrawRectangle(gconst.BPEN, stx + 1032, sty + outht - rht * 7, 416, rht);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 6 + rht * 2, stx + 1450, sty + outht - rht * 6 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 4 + rht * 2, stx + 1450, sty + outht - rht * 4 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1032, sty + outht - rht * 3 + rht * 2, stx + 1450, sty + outht - rht * 3 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 5 + rht * 2, stx + 1450, sty + outht - rht * 5 + rht * 2);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + outht - rht * 7 + rht * 2, stx + 1450, sty + outht - rht * 7 + rht * 2);

        //verical lines for file specification

        g.DrawLine(gconst.BPEN, stx + 1031, sty + 628, stx + 1031, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1220, sty + 585, stx + 1220, sty + 737);
        g.DrawLine(gconst.BPEN, stx + 1090, sty + 585, stx + 1090, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1300, sty + 585, stx + 1300, sty + 715);
        g.DrawLine(gconst.BPEN, stx + 1350, sty + 585, stx + 1350, sty + 628);
        g.DrawLine(gconst.BPEN, stx + 1450, sty + 585, stx + 1450, sty + 628);


        g.DrawLine(gconst.BPEN, stx + 1000, sty + outht - rht, stx + 400, sty + outht - rht);

        //top horizontal lines
        g.DrawLine(gconst.BPEN, stx, sty + 30, stx + outw - 10, sty + 30);
        g.DrawLine(gconst.BPEN, stx, sty + 140, stx + outw - 10, sty + 140);
        g.DrawLine(gconst.BPEN, stx, sty + 170, stx + outw - 10, sty + 170);

        // top vertical lines
        g.DrawLine(gconst.BPEN, stx + 95, sty + 30, stx + 95, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 250, sty + 30, stx + 250, sty + 170);
        //g.DrawLine(gconst.BPEN, stx + 330, sty + 30, stx + 330, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 430, sty, stx + 430, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 540, sty + 30, stx + 540, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 680, sty + 30, stx + 680, sty + 170);
        //g.DrawLine(gconst.BPEN, stx + 750, sty , stx + 750, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 850, sty, stx + 850, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 990, sty, stx + 990, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1105, sty + 30, stx + 1105, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1220, sty, stx + 1220, sty + 170);
        g.DrawLine(gconst.BPEN, stx + 1290, sty, stx + 1290, sty + 170);

        g.DrawString("RAW MATERAL SECTION  (mm) :               ", gconst.TSIZE9, gconst.TXTBRUSH, L8);
        g.DrawString("HARDNESS  (HRC) :  ", gconst.TSIZE9, gconst.TXTBRUSH, L9);
        g.DrawString("CUT STANDRAD                    CUT TYPE", gconst.TSIZE9, gconst.TXTBRUSH, L10);
       // g.DrawString("GENRAL                                    BAST", gconst.TSIZE9, gconst.TXTBRUSH, L11);
        g.DrawString("1", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 610);
        g.DrawString("       FILE SPACIFICATION                 -               1               -", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 610);
        g.DrawString("ITEM", gconst.TSIZE9, gconst.TXTBRUSH, 1060, 635);
        g.DrawString("        DESCRPITION                 MATERIAL      DIFF       REMARKS", gconst.TSIZE9, gconst.TXTBRUSH, 1099, 635);
        g.DrawString("S . NO .                         ALTERATIONS                                                                                                                                                        CHANGED  BY                                         DATE", gconst.TSIZE9, gconst.TXTBRUSH, L18);
        g.DrawString("                                  SURFACE FINISH                                                         GENERAL  TOLERANCE WHERE NOT SPECIFIED ", gconst.TSIZE9, gconst.TXTBRUSH, L19);
        g.DrawString("                               △                        △△                       △△△                                   0.10                     10.30                  30.100                  100.300                  300.100                       1000.2000", gconst.TSIZE9, gconst.TXTBRUSH, L20);
        g.DrawString(" ∼ ", gconst.TSIZE40, gconst.TXTBRUSH, 35, 705);
        g.DrawString("ROUGH              ROUGH                 MEDIUM                    FINE  ", gconst.TSIZE9, gconst.TXTBRUSH, L21);
        g.DrawString("                                                                                                                                     +0.10                      +0.2                    +0.3                     +0.5                        +0.8                              +1.2", gconst.TSIZE9, gconst.TXTBRUSH, L22);
        g.DrawString("GROUND           MACHINE              MACHINE                  MACHINE ", gconst.TSIZE9, gconst.TXTBRUSH, L23);
        g.DrawString("                                                                                                                                     -                               -                          -                           -                              -                                     -", gconst.TSIZE9, gconst.TXTBRUSH, L24);
        g.DrawString("JK  FILES  (India)", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 660);
        g.DrawString("LIMITED     THANE", gconst.TSIZE12, gconst.TXTBRUSH, 1060, 680);
        g.DrawString("DATE", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 660);
        g.DrawString("DRAWN", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 680);
        g.DrawString("REVIEWED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 700);
        g.DrawString("APPROVED", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 720);
        g.DrawString("DRAWING NO .", gconst.TSIZE7, gconst.TXTBRUSH, 1250, 740);
        g.DrawString("PROJECTION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 709);
        g.DrawString("DESIGNATION", gconst.TSIZE7, gconst.TXTBRUSH, 1060, 740);

        //------------------------values-------------------------------------------
        
        g.DrawString(prf.FSZ_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 35, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 180, 170);
        g.DrawString(prf.CML_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 300, 170);
        //g.DrawString(prf.BL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 365, 170);
        g.DrawString(prf.TL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 500, 170);
        g.DrawString(prf.TWS_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 600, 170);
        g.DrawString(prf.TWT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 780, 170);
        g.DrawString(prf.GBD_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 920, 170);
        g.DrawString(prf.GPG_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1040, 170);
        g.DrawString(prf.GAG_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1160, 170);
        g.DrawString(prf.TPL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1250, 170);
        g.DrawString(prf.L_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 1350, 170);
        g.DrawString(prf.RMW_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 230, 460);
        g.DrawString(prf.HRC_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 480);
        g.DrawString(prf.UCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 350, 545);
        g.DrawString(prf.OCT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 520, 545);
        g.DrawString(prf.IUP_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 55, 635);
        g.DrawString(prf.IOV_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 200, 635);
        g.DrawString(prf.IEG_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 350, 635);
        g.DrawString(prf.USL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 520, 635);
        g.DrawString(prf.UEL_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 680, 635);
        g.DrawString(prf.ECT_dispval, gconst.TSIZE10, gconst.TXTBRUSH, 620, 545);
        g.DrawString(prf.FNM_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1095, 750);
        g.DrawString(prf.PTN_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1250, 750);
        g.DrawString(prf.CSP_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 45, 548);
        g.DrawString(prf.CT_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 200, 548);
        //g.DrawString(prf.L_dispval, gconst.TSIZE8, gconst.TXTBRUSH, 1100, 170);
        // text for top 
        g.DrawString("TANG DIMENSIONS", gconst.TSIZE9, gconst.TXTBRUSH, 600, 30);
        g.DrawString("(GRINDING)", gconst.TSIZE9, gconst.TXTBRUSH, 880, 30);
        g.DrawString("(GRINDING) POINT SIZE IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 1030, 30);

        g.DrawString("NORMAL", gconst.TSIZE9, gconst.TXTBRUSH, 35, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 35, 85);
        g.DrawString("IN INCHES", gconst.TSIZE9, gconst.TXTBRUSH, 35, 110);

        g.DrawString("RM   DIM", gconst.TSIZE9, gconst.TXTBRUSH, 170, 60);
        g.DrawString("IN MM (TOL.", gconst.TSIZE9, gconst.TXTBRUSH, 170, 85);
        g.DrawString("W + 0.15,", gconst.TSIZE9, gconst.TXTBRUSH, 170, 110);
        g.DrawString("T +0.1)", gconst.TSIZE9, gconst.TXTBRUSH, 170, 135);

        g.DrawString("CROPPING", gconst.TSIZE9, gconst.TXTBRUSH, 300, 60);
        g.DrawString("MOOD LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 300, 85);
        g.DrawString("IN MM TOL.", gconst.TSIZE9, gconst.TXTBRUSH, 300, 110);
        g.DrawString("(+1/-0.0)", gconst.TSIZE9, gconst.TXTBRUSH, 300, 135);

        //g.DrawString("BODY", gconst.TSIZE9, gconst.TXTBRUSH, 380, 60);
        //g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 380, 85);
        //g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 380, 110);

        g.DrawString("TANG", gconst.TSIZE9, gconst.TXTBRUSH, 480, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 480, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 480, 110);
        g.DrawString("(TOL. +1)", gconst.TSIZE9, gconst.TXTBRUSH, 480, 135);

        g.DrawString("AT  SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 580, 60);
        g.DrawString("(A/F)", gconst.TSIZE9, gconst.TXTBRUSH, 580, 85);
        g.DrawString("(TOL. + 0.5)", gconst.TSIZE9, gconst.TXTBRUSH, 580, 110);

        g.DrawString("AT TIP (5MM", gconst.TSIZE9, gconst.TXTBRUSH, 740, 60);
        g.DrawString("FROM SH.)", gconst.TSIZE9, gconst.TXTBRUSH, 740, 85);
        g.DrawString("(A/F)", gconst.TSIZE9, gconst.TXTBRUSH, 740, 110);
        g.DrawString("(TOL. +3)", gconst.TSIZE9, gconst.TXTBRUSH, 740, 135);

        g.DrawString("BODY DIM.", gconst.TSIZE9, gconst.TXTBRUSH, 890, 60);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 890, 85);
        g.DrawString("(TOL. +35)", gconst.TSIZE9, gconst.TXTBRUSH, 890, 110);

        g.DrawString("POINT GRINDING", gconst.TSIZE9, gconst.TXTBRUSH, 1025, 60);
        g.DrawString("(TOL.+2)", gconst.TSIZE9, gconst.TXTBRUSH, 1025, 85);

        g.DrawString("AFTER F/S", gconst.TSIZE9, gconst.TXTBRUSH, 1150, 60);
        g.DrawString("GRIDING", gconst.TSIZE9, gconst.TXTBRUSH, 1150, 85);
        g.DrawString("(TOL.+2)", gconst.TSIZE9, gconst.TXTBRUSH, 1150, 110);

        g.DrawString("TAPER", gconst.TSIZE9, gconst.TXTBRUSH, 1250, 60);
        g.DrawString("LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 1250, 85);
        g.DrawString("IN MM", gconst.TSIZE9, gconst.TXTBRUSH, 1250, 110);

        g.DrawString("BODY LENGTH", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 60);
        g.DrawString("AFTER FLAT SIDE", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 85);
        g.DrawString("CUTTING BEFORE", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 110);
        g.DrawString("HARDING (MM)", gconst.TSIZE9, gconst.TXTBRUSH, 1330, 135);
        //g.DrawString("HARDINING (MM)", gconst.TSIZE9, gconst.TXTBRUSH, 1100, 135);


        // text CUT INCLINATION AND UNCUT
        g.DrawString("CUT INCLINATION (mm)+2", gconst.TSIZE9, gconst.TXTBRUSH, 90, 590);
        g.DrawString("U/C", gconst.TSIZE9, gconst.TXTBRUSH, 50, 610);
        g.DrawString("O/C", gconst.TSIZE9, gconst.TXTBRUSH, 200, 610);
        g.DrawString("E/C", gconst.TSIZE9, gconst.TXTBRUSH, 350, 610);


        g.DrawString("UNCUT (mm) +2", gconst.TSIZE9, gconst.TXTBRUSH, 550, 590);
        g.DrawString("AT SHOULDER", gconst.TSIZE9, gconst.TXTBRUSH, 480, 610);
        g.DrawString("-", gconst.TSIZE9, gconst.TXTBRUSH, 627, 594);
        g.DrawString("ON EDGE", gconst.TSIZE9, gconst.TXTBRUSH, 650, 610);

        // TEXT FOR CUT 
        g.DrawString("U/C", gconst.TSIZE9, gconst.TXTBRUSH, 360, 525);
        g.DrawString("E/C", gconst.TSIZE9, gconst.TXTBRUSH, 520, 525);
        g.DrawString("O/C", gconst.TSIZE9, gconst.TXTBRUSH, 620, 525);

        // Note 
        g.DrawString("DEPTH OF CUT IS CALCLUATED BASED ON", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 560);
        g.DrawString("TEETH PER INCH AND IS MIN. 52% & PITCH FOR SINGLE", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 575);
        g.DrawString("CUT FILES AND MIN.55% FOR DOUBLE CUT FILE.", gconst.TSIZE9, gconst.TXTBRUSH, 1070, 593);
    }

    //function to draw horizontal dim
    public static void hordimtop(Graphics g, int dm1x,int dm1y,int dm2x,int dm2y, double dmtxt)
    {
        Point dmp1 = new Point(dm1x, dm2y);
        Point dmp2 = new Point(dm2x, dm2y);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // hor line
        dmp1 = new Point(dm1x, dm1y - 20);
        dmp2 = new Point(dm1x, dm2y + 20);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // first vert
        dmp1 = new Point(dm2x, dm2y - 20);
        dmp2 = new Point(dm2x, dm2y + 20);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // second vert


        dmp1 = new Point(dm2x, dm2y);
        dmp2 = new Point(dm2x - 8, dm2y - 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l1 
        dmp1 = new Point(dm2x, dm2y);
        dmp2 = new Point(dm2x - 8, dm2y + 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l2 

        dmp1 = new Point(dm1x, dm1y);
        dmp2 = new Point(dm1x + 5, dm1y - 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l1 
        dmp1 = new Point(dm1x, dm1y);
        dmp2 = new Point(dm1x + 5, dm2y + 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l2 
        dmp1 = new Point((int)(dm1x + dm2x + 10) / 2, dm1y - 20);
        string st = System.Convert.ToString(dmtxt);
        g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, dmp1);   // draw text


    }
    //function to draw horizontal dim without text
    public static void hordimwithouttext(Graphics g, int dm1x, int dm1y, int dm2x, int dm2y, double dmtxt)
    {
        Point dmp1 = new Point(dm1x, dm2y);
        Point dmp2 = new Point(dm2x, dm2y);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // hor line
        dmp1 = new Point(dm1x, dm1y - 20);
        dmp2 = new Point(dm1x, dm2y + 20);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // first vert
        dmp1 = new Point(dm2x, dm2y - 20);
        dmp2 = new Point(dm2x, dm2y + 20);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // second vert


        dmp1 = new Point(dm2x, dm2y);
        dmp2 = new Point(dm2x - 8, dm2y - 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l1 
        dmp1 = new Point(dm2x, dm2y);
        dmp2 = new Point(dm2x - 8, dm2y + 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l2 

        dmp1 = new Point(dm1x, dm1y);
        dmp2 = new Point(dm1x + 5, dm1y - 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l1 
        dmp1 = new Point(dm1x, dm1y);
        dmp2 = new Point(dm1x + 5, dm2y + 5);
        g.DrawLine(gconst.BPEN, dmp1, dmp2);         // arrow l2 
        //dmp1 = new Point((int)(dm1x + dm2x + 2) / 2, dm1y - 20);
        //string st = System.Convert.ToString(dmtxt);
        //g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, dmp1);   // draw text

    }
    // draw vertical dimension
    public static void vertdim(Graphics g, int dm1x, int dm1y, int dm2x, int dm2y, double dmtxt)
    {
        Point dm1 = new Point(dm1x, dm1y);
        Point dm2 = new Point(dm2x, dm2y);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // vertical line
        dm1 = new Point(dm1x - 20, dm1y);
        dm2 = new Point(dm1x + 20, dm1y);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // top hor 
        dm1 = new Point(dm2x - 20, dm2y);
        dm2 = new Point(dm2x + 20, dm2y);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // bot hor

        dm1 = new Point(dm1x, dm1y);
        dm2 = new Point(dm1x - 5, dm1y + 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // top arrow L 
        dm1 = new Point(dm1x, dm1y);
        dm2 = new Point(dm1x + 5, dm1y + 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // top arrow R

        dm1 = new Point(dm2x, dm2y);
        dm2 = new Point(dm2x - 5, dm2y - 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // bot arrow L 
        dm1 = new Point(dm2x, dm2y);
        dm2 = new Point(dm2x + 5, dm2y - 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // bot arrow R
        //extension line
        dm1 = new Point(dm1x, dm1y);
        dm2 = new Point(dm1x, dm1y + 20);
        g.DrawLine(gconst.BPEN, dm1, dm2);
        dm1 = new Point(dm2x, dm2y);
        dm2 = new Point(dm2x, dm2y - 20);
        g.DrawLine(gconst.BPEN, dm1, dm2);

        dm1 = new Point(dm1x + 5, dm2y);
        string st = System.Convert.ToString(dmtxt);
        g.DrawString(st, gconst.TSIZE10, gconst.TXTBRUSH, dm1);   // draw text

    }
    public static void vertdim_withouttext(Graphics g, int dm1x, int dm1y, int dm2x, int dm2y, double dmtxt)
    {
        Point dm1 = new Point(dm1x, dm1y);
        Point dm2 = new Point(dm2x, dm2y);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // vertical line
        dm1 = new Point(dm1x - 20, dm1y);
        dm2 = new Point(dm1x + 20, dm1y);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // top hor 
        dm1 = new Point(dm2x - 20, dm2y);
        dm2 = new Point(dm2x + 20, dm2y);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // bot hor

        dm1 = new Point(dm1x, dm1y);
        dm2 = new Point(dm1x - 5, dm1y + 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // top arrow L 
        dm1 = new Point(dm1x, dm1y);
        dm2 = new Point(dm1x + 5, dm1y + 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // top arrow R

        dm1 = new Point(dm2x, dm2y);
        dm2 = new Point(dm2x - 5, dm2y - 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // bot arrow L 
        dm1 = new Point(dm2x, dm2y);
        dm2 = new Point(dm2x + 5, dm2y - 8);
        g.DrawLine(gconst.BPEN, dm1, dm2);         // bot arrow R
        //extension line
        dm1 = new Point(dm1x, dm1y);
        dm2 = new Point(dm1x, dm1y + 20);
        g.DrawLine(gconst.BPEN, dm1, dm2);
        dm1 = new Point(dm2x, dm2y);
        dm2 = new Point(dm2x, dm2y - 20);
        g.DrawLine(gconst.BPEN, dm1, dm2);

    }
    public static void cenline(Graphics g, int cl1x, int cl1y, int cl2x, int cl2y)
    {
        int i = 1;
        int length = cl2x - cl1x;


        for (i = 1; i < 25; i++)
        {
            Point cl1 = new Point(cl1x, cl1y);
            cl2x = cl1x + 30;
            Point cl2 = new Point(cl2x, cl2y);

            g.DrawLine(gconst.BPEN, cl1, cl2);
            cl1x = cl2x + 5;
            cl2x = cl1x + 5;

            g.DrawLine(gconst.BPEN, cl1, cl2);
            cl1x = cl1x + 5;

        }
    }
}