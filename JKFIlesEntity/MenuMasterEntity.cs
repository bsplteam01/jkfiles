﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKFIlesEntity;
//using JKFiles.WebApp.Models;

namespace JKFIlesEntity
{
    public class MenuMasterEntity
    {
        public string MainMenu { get; set; }
        public int SubMenu_Id { get; set; }
        public string SubMenu { get; set; }
        public string Controller { get; set; }
        public string sd_chart_num { get; set; }
        public int MainMenuID { get; set; }
        public string Action { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public string RoleName { get; set; }
        public string RoleId { get; set; }

        //public List<MenuMaster> listrolename { get; set; }
        //public List<MenuMaster> listMainmenu { get; set; }
        //public List<MenuMaster> listSubMainmenu { get; set; }
        public bool IsSelected { get; set; }

        public string flag { get; set; }
    }
}