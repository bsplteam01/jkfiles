﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class FileSizeDetailsEntity
    {
        public int _id { get; set; }
        public decimal fs_size_inches { get; set; }
        public decimal fs_size_mm { get; set; }
        public string fs_size_code { get; set; }
        public string fs_size_remarks { get; set; }
        public bool fs_size_isApproved { get; set; }
        public bool fs_size_isActive { get; set; }
        public int fs_size_verno { get; set; }
        public DateTime fs_size_createddate { get; set; }
        public string fs_size_createby { get; set; }
        public DateTime fs_size_approvedate { get; set; }
        public string fs_size_approveby { get; set; }
        public string fs_request_type { get; set; }
        public int fs_request_id { get; set; }
        public IEnumerable<FileSizeDetailsEntity> filelist { get; set; }
        public IEnumerable<FileSizeDetailsEntity> FileSizeNoApproveList { get; set; }
        public IEnumerable<FileSizeDetailsEntity> FileSizeNoActiveList { get; set; }

        public List<FileSizeDetailsEntity> listsizecode { get; set; }

        public string flag { get; set; }

    }
}