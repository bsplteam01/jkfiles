﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class ItemPartNumEntity
    {
        public int requestId { get; set; }
        public string requestType { get; set; }
        public string itemNum { get; set; }
        public string dispSKU { get; set; }
    }
}