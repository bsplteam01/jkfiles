﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class InnerBoxMasterEntity
    {
        public string ib_qty_inbox { get; set; }
        public int ib_box_id { get; set; }
        public string ib_chartnum { get; set; }
        public string ib_box_type { get; set; }

        public string ib_request_type { get; set; }
        public int ib_request_id { get; set; }


        //public string ib_box_type { get; set; }
        public string ib_fsize_id { get; set; }
        public string ft_ftype_desc { get; set; }
        public string pm_fstype_desc { get; set; }

        public List<InnerBoxMasterEntity> innerMasterEntityList { get; set; }
        public List<InnerBoxMasterEntity> innerMasterNoActiveEntityList { get; set; }
        public List<InnerBoxMasterEntity> innerMasterNoApproveEntityList { get; set; }

        public List<InnerBoxDetailsEntity> innerboxlist { get; set; }


        public List<InnerBoxMasterEntity> listinnerbmaterial { get; set; }
        public List<InnerBoxMasterEntity> listsizecode { get; set; }
        public List<InnerBoxMasterEntity> listtype { get; set; }
        public List<InnerBoxMasterEntity> listsubtype { get; set; }

        public string ReqNo { get; set; }

    }
}