﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class PartNumMasterEntity
    {
        public string  pn_part_no { get; set; }
        public string costomerName { get; set; }

        public string costomerId { get; set; }
        
        public string brandName { get; set; }
        public string brandId { get; set; }

        public int rs_quantity { get; set; }
        public string dp_stamp_chart { get; set; }
        public string dp_wrapping_chart { get; set; }
        public int cm_request_id { get; set; }
        public string pr_uniq_id { get; set; }
        public string dp_handle_chart { get; set; }

        public string pn_request_type { get; set; }
        public int pn_request_id { get; set; }
        public DateTime rs_sku_start_date { get; set; }
        public DateTime rs_sku_end_date { get; set; }
        public DateTime pn_start_date { get; set; }
        public DateTime pn_end_date { get; set; }
        public string pr_tang_color { get; set; }

        // Temp part No details
        public int _id { get; set; }
        public string pn_tmp_reqtype { get; set; }
        public int pn_tmp_request_id { get; set; }
        public string pn_tmp_itemno { get; set; }
        public string pn_tmp_dispatchno { get; set; }
        public string pn_tmp_dispatchdigit { get; set; }
        public string flag { get; set; }
    }
}
