﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Component
/// </summary>
public class Component
{
    public double height = 180;
    public double width = 320;
    public double startX = 10;
    public double startY = 10;

    public double round_rect_width = 425;
    public double round_rect_height = 450;
    public string component_name = "IC ACB1";
    public string KA = "50 KA";
    public string amps = "800 Amps";
    public string handle = "Y";
    public string indicator_lamp = "RYB+ONOFFTRIP+RTC-07";
    public string start_push_button = "Illuminated";
    public string stop_push_button = "Illuminated";

    public string mains = "y";

    public string handle_pos = "Right";
    public string lock_pos = "Right";

    public string external_MTR = "A+V";
    public string TNC_switch = "No";
    public string ASd_ind_lamps = "ON";
  //  public string indicatinglamp { get; set; }

}