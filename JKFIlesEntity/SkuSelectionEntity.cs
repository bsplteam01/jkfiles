﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class SkuSelectionEntity
    {

        public string rs_request_type { get; set; }
        public int rs_request_id { get; set; }
        public string rs_sku_code { get; set; }
        public int rs_quantity { get; set; }
        public string rs_stamp_chart { get; set; }
        public string rs_wrapper_chart { get; set; }
        public string rs_handle_chart { get; set; }
        public string rs_tang_tempr { get; set; }
        public string rs_remarks { get; set; }
        public DateTime rs_createdate { get; set; }
        public string rs_createby { get; set; }
        public string flag { get; set; }
        public DateTime rs_sku_start_date { get; set; }
        public DateTime rs_sku_end_date { get; set; }
        public string SetSKUID { get; set; }
        public string MaxFileSizeCode { get; set; }
        public int pn_request_id { get; set; }
        public string pn_request_type { get; set; }

        public List<string[]> postData { get; set; }
        public List<string[]> TempPartNo { get; set; }
        public List<SkuSelectionEntity> skudetailslist { get; set; }
    }
}