﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class RawMaterialMastrEntity
    {


        public string rm_fsize_id { get; set; }
        public string rm_ftype_id { get; set; }
        public string rm_fstype_id { get; set; }
        public string rm_code { get; set; }
        public string rm_desc { get; set; }
        public string rm_material { get; set; }
        public Decimal rm_wtinkg_perthsnd { get; set; }
        public Decimal rm_netwtingm_persku { get; set; }

        public string filesize { get; set; }
        public string filetype { get; set; }
        public string filesubtype { get; set; }

        public string thickness { get; set; }
        public string width { get; set; }
        public Decimal rm_height { get; set; }
        public Decimal rm_width { get; set; }

        public string rm_request_type { get; set; }
        public int rm_request_id { get; set; }
        public string rm_remarks { get; set; }

        //stmp new 

        public string stampsize { get; set; }
        public string stamplocation { get; set; }
        public string Orientation { get; set; }
        public string fontsize { get; set; }
        public string noofsides { get; set; }
        public string side1image { get; set; }
        public string side2image { get; set; }
        public string side3image { get; set; }
        public string side4image { get; set; }
        public string sm_stamp_mode { get; set; }
        public string ImageName { get; set; }
        public string sd_chart_num { get; set; }
        public string sm_cust_id { get; set; }
        public string sm_brand_id { get; set; }
        public string sm_ftype_id { get; set; }
        public string sm_fstype_id { get; set; }
        public string sm_fsize_id { get; set; }
        public string sm_fcut_id { get; set; }
        public string request_type { get; set; }
        public int request_id { get; set; }
    }
}