﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class valueStreammasterEntity
    {
        public string val_valuestream_code { get; set; }
        public string val_valuestream_name { get; set; }
        public string flag { get; set; }
        public string ft_ftype_code { get; set; }
        public string ft_ftype_desc { get; set; }
        public List<valueStreammasterEntity> valueStreamDropdownList { get; set; }
    }
}