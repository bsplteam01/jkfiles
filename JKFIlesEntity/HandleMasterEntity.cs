﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class HandleMasterEntity
    {
        public string  hm_chart_num { get; set; }        
        public string  hm_handle_type { get; set; }
        public string  hm_ftype_id { get; set; }
        public string  hm_fstype_id { get; set; }
        public string  hm_fsize_id { get; set; }
        public string  hm_chartimg_name { get; set; }
        public string  hm_chart_remarks { get; set; }
        public string  hm_isApproved { get; set; }
        public Boolean hm_isActive { get; set; }
        public DateTime hm_createdate { get; set; }
        public string hm_createby { get; set; }
        public DateTime hm_approvedate { get; set; }
        public Boolean hm_approveby { get; set; }
        public int hm_verno { get; set; }
        public string hm_request_type { get; set; }
        public int hm_request_id { get; set; }

        public List<HandleMasterEntity> handleMasterList { get; set; }
        public List<HandleMasterEntity> handleSubTypeList { get; set; }

        public string ft_ftype_desc { get; set; }
        public string pm_fstype_desc { get; set; }
        public string hm_handle_subtype { get; set; }

        public int hm_handle_id { get; set; }

        public List<HandleMasterEntity> handlenoapprove { get; set; }
        public List<HandleMasterEntity> handlenoactive { get; set; }

        public List<HandleMasterEntity> handlemstrList { get; set; }
        public string hd_parm_name { get; set; }
        public string hd_parm_code { get; set; }
        public string hd_parm_dwgvalue { get; set; }
        public string hd_parm_scrnvalue { get; set; }

        public List<HandleMasterEntity> listfiletypecode { get; set; }
        public List<HandleMasterEntity> listsizecode { get; set; }
        public List<HandleMasterEntity> listtype { get; set; }
        public List<HandleMasterEntity> listsubtype { get; set; }
        public List<HandleMasterEntity> listhandletype { get; set; }
        public List<HandleMasterEntity> listhandlesubtype { get; set; }
        public List<HandleMasterEntity> listchartno { get; set; }
        public string filesubtype { get; set; }
        public string filetype { get; set; }
        public string fsize_code { get; set; }
        public string filetypecode { get; set; }
        public string handletype { get; set; }
        public string handlesubtype { get; set; }

        public string ft_ftype_code { get; set; }
        public string pm_fstype_code { get; set; }
        public string sm_new_overwrite { get; set; }

        public string flag { get; set; }
        public string ReqNo { get; set; }
    }
}