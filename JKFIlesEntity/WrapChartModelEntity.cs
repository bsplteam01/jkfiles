﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class WrapChartModelEntity
    {



        public string color { get; set; }
        public string colorrange { get; set; }
        public string colorphantomeshade1 { get; set; }
        public string colorphantomeshade1range { get; set; }
        public string colorphantomeshade2 { get; set; }
        public string colorphantomeshade2range { get; set; }
        public string colorphantomeshade3 { get; set; }
        public string colorphantomeshade3range { get; set; }
        public string wrapperNote { get; set; }
        public string wrapperNoterange { get; set; }
        public string wrapperwidth { get; set; }
        public string wrapperwidthrange { get; set; }
        public string wrapperlength { get; set; }
        public string wrapperlengthrange { get; set; }
        public string UnitBePack { get; set; }
        public string UnitBePackrange { get; set; }
        public string wrappingThiknes { get; set; }
        public string wrappingThiknesrange { get; set; }
        public string printed { get; set; }
        public string printedrange { get; set; }
        public string Remarks { get; set; }
        public string image { get; set; }
        public string chartnum { get; set; }
        public string wm_wrappertype { get; set; }

        public string wm_ftype_id { get; set; }
        public string wm_fstype_id { get; set; }
        public string wm_fsize_id { get; set; }
        public string wm_cust_id { get; set; }
        public string wd_wrapper_recid { get; set; }
        public string wm_brand_id { get; set; }
        
        public string wm_request_type { get; set; }
        public int wm_request_id { get; set; }


        public string img { get; set; }
        //  public HttpFileCollectionBase img1 { get; set; }

        public string filesubtype { get; set; }
        public string filetype { get; set; }
        public string fsize_code { get; set; }
        public string Brand { get; set; }
        public string bm_brand_name { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public List<WrapChartModelEntity> listsizecode { get; set; }
        public List<WrapChartModelEntity> listtype { get; set; }
        public List<WrapChartModelEntity> listsubtype { get; set; }
        public List<WrapChartModelEntity> listcstmstr { get; set; }
        public List<WrapChartModelEntity> listbrandmstr { get; set; }
    }
}
