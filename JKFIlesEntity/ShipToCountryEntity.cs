﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class ShipToCountryEntity
    {
        public string cm_country_cd { get; set; }
        public string cm_country_name { get; set; }
        public string cm_isApproved { get; set; }
        public bool cm_isActive { get; set; }
        public int cm_verno { get; set; }
        public DateTime cm_createdate { get; set; }
        public string cm_createby { get; set; }
        public DateTime cm_approvedate { get; set; }
        public string cm_approveby { get; set; }
    }
}