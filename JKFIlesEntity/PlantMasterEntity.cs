﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class PlantMasterEntity
    {
        public string pla_plant_code { get; set; }
        public string pla_plant_name { get; set; }
        public string pla_plant_location { get; set; }
        public string pla_createdate { get; set; }
        public DateTime pla_createby { get; set; }
        public string pla_approveby { get; set; }
        public string plant_approveby { get; set; }
        public Boolean pla_isApproved { get; set; }
        public Boolean pla_isActive { get; set; }
        public string pla_remarks { get; set; }
        public DateTime pla_approvedate { get; set; }

        public List<PlantMasterEntity> plantlist { get; set; }
    }
}