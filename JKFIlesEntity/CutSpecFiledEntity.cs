﻿using System;
using System.Collections.Generic;
using System.Web;

namespace JKFIlesEntity
{
    public class CutSpecFiledEntity 
    {
        public int tpi_newrm_id { get; set; }
        public string tpi_newrm_ftype { get; set; }
        public int tpi_newrm_seqno { get; set; }
        public string tpi_newrm_parm1 { get; set; }
        public string tpi_newrm_parm2 { get; set; }
        public int tpi_newrm_numinputs { get; set; }
        public string tpi_newrm_isApproved { get; set; }
        public string tpi_newrm_isActive { get; set; }
        public string tpi_newrm_createby { get; set; }
        public string tpi_newrm_createdate { get; set; }
        public string tpi_newrm_approveby { get; set; }
        public string tpi_newrm_approvedate { get; set; }
        public int tpi_newrm_maxlength { get; set; }
        public string nrm_input_type { get; set;}
        public IEnumerable<CutSpecFiledEntity> ReqParList { get; set; }
    }
}
