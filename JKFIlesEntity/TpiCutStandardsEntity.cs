﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKFIlesEntity
{
    public class TpiCutStandardsEntity
    {
        public string fileSizeCode { get; set; }
        public string fileTypeCode { get; set; }
        public string fileSubTypeCode { get; set; }
        public string cutCode { get; set; }
        public string cutStandardCode { get; set; }
        public string cutStandardName { get; set; }
        public string dimensionName { get; set; }
        public string dimensionCode { get; set; }
        public string dimensionDrawingVal { get; set; }
        public Decimal dimensionScreeningVal { get; set; }
        public bool isActive { get; set; }
        public int versionNo { get; set; }
        public DateTime createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime approvedDate { get; set; }
        public string approvedBy { get; set; }

        public string tpi_cut_application { get; set; }
        public string tpi_dim_name { get; set; }
        public string tpi_dim_code { get; set; }
        public string tpi_dim_scrnval { get; set; }
        public string tpi_dim_dwgval { get; set; }
        public int tpi_verno { get; set; }
        public int tpi_request_id { get; set; }
        public string tpi_request_type { get; set; }
        public string tpi_fsize_code { get; set; }
        public string tpi_ftype_code { get; set; }
        public string tpi_fstype_code { get; set; }
        public string flag { get; set; }
        public string tpi_cut_code { get; set; }
        public string tpi_cut_std_code { get; set; }
        public string tpi_cut_std_name { get; set; }
        public string tpi_isApproved { get; set; }



        public List<TpiCutStandardsEntity> cutStandardList { get; set; }
    }
}