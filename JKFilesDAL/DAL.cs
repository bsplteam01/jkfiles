﻿using JKFiles.WebApp.Entity;
using JKFIlesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace JKFilesDAL
{
    public class DAL
    {
        Utils objUtils = new Utils();
        string connectionstring;

        #region Production >>File Spec-Dhanashree Kolpe

        public void saveRawMaterial(RawMaterialMastrEntity rawmEntity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "update [dbo].[tb_production_sku] set pr_rawmtrl_cd=@pr_rawmtrl_cd," +
                    " pr_rawmtrl_height=@pr_rawmtrl_height, pr_rawmtrl_width=@pr_rawmtrl_width," +
                    "pr_rawmtrl_kgperthsnd=@pr_rawmtrl_kgperthsnd,pr_rawmtrl_netwt=@pr_rawmtrl_netwt" +
                   " where pr_fsize_code=@pr_fsize_code and pr_fstype_code=@pr_fstype_code and pr_ftype_code=@pr_ftype_code";

                SqlCommand cmd = new SqlCommand(insertStmt, con);
                cmd.Parameters.AddWithValue("@pr_rawmtrl_cd", rawmEntity.rm_code);
                cmd.Parameters.AddWithValue("@pr_rawmtrl_kgperthsnd", rawmEntity.rm_wtinkg_perthsnd);
                cmd.Parameters.AddWithValue("@pr_rawmtrl_height", rawmEntity.rm_height);
                cmd.Parameters.AddWithValue("@pr_rawmtrl_width", rawmEntity.rm_width);
                cmd.Parameters.AddWithValue("@pr_rawmtrl_netwt", rawmEntity.rm_netwtingm_persku);
                cmd.Parameters.AddWithValue("@pr_fsize_code", rawmEntity.rm_fsize_id);
                cmd.Parameters.AddWithValue("@pr_fstype_code", rawmEntity.rm_fstype_id);
                cmd.Parameters.AddWithValue("@pr_ftype_code", rawmEntity.rm_ftype_id);
                // cmd.ExecuteScalar();
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public DataTable getDrawing(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Drawingdetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@req_type", flag);
                cmd.Parameters.AddWithValue("@req_id", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //Final Submit
        public int FinalSubmit(PartNumOtherDetailsEntity partEntity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Default_Approval_Remark", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@req_type", partEntity.othdt_request_type);
            cmd.Parameters.AddWithValue("@req_id", partEntity.othdt_request_id);
            cmd.Parameters.AddWithValue("@ap_createby", partEntity.ap_createby);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get master Details-Dhanashree Kolpe
        public DataTable getMasters(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public DataSet getftimageview(string ft_desc)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ft_desc", ft_desc);
            cmd.Parameters.AddWithValue("@flag", "getFileTypeimage");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        // Get sub Type file master
        public DataTable getSubTypeMasters(string flag, string filesizecode, string filetypecode)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.Parameters.AddWithValue("@filesizecode", filesizecode);
                cmd.Parameters.AddWithValue("@filetypecode", filetypecode);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        //get filesubtype Details-Dhanashree Kolpe
        public DataTable getfileSubType(FileSpec objFilespec)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", objFilespec.flag);
                cmd.Parameters.AddWithValue("@filesizecode", objFilespec.filesizecode);
                cmd.Parameters.AddWithValue("@filetypecode", objFilespec.filetypecode);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }



        //get profile Details-Dhanashree Kolpe
        public DataTable getProfileDetails(FileSpec objFilespec)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_upcut_inclination_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@filesizecode", objFilespec.filesizecode);
                cmd.Parameters.AddWithValue("@filetypecode", objFilespec.filetypecode);
                cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }



        public int UpdateFileSpec(FileSpec objFilespec)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_File_Spec", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cust", objFilespec.CustName);
            cmd.Parameters.AddWithValue("@pr_fsize_code", objFilespec.filesizecode);
            cmd.Parameters.AddWithValue("@pr_ftype_code", objFilespec.filetypecode);
            cmd.Parameters.AddWithValue("@pr_ftype_desc", objFilespec.FileType);
            cmd.Parameters.AddWithValue("@pr_fstype_code", objFilespec.filesubtypecode);
            cmd.Parameters.AddWithValue("@pr_fstype_desc", objFilespec.FileSubType);
            cmd.Parameters.AddWithValue("@pr_hardness", objFilespec.HRC);
            cmd.Parameters.AddWithValue("@pr_tang_color", objFilespec.TangColor);
            cmd.Parameters.AddWithValue("@pr_uncutattang", objFilespec.USL);
            cmd.Parameters.AddWithValue("@pr_uncutatpoint", null);
            cmd.Parameters.AddWithValue("@pr_uncutatedge", objFilespec.UEL);
            cmd.Parameters.AddWithValue("@pr_upcutncline", objFilespec.IUP);
            cmd.Parameters.AddWithValue("@pr_overcutincline", objFilespec.IOV);
            cmd.Parameters.AddWithValue("@pr_edgecutincline", objFilespec.IEG);
            cmd.Parameters.AddWithValue("@pr_fcut_code", objFilespec.cuttypecode);
            cmd.Parameters.AddWithValue("@pr_fcut_desc", objFilespec.CutType);
            cmd.Parameters.AddWithValue("@pr_request_type", objFilespec.ReqType);
            cmd.Parameters.AddWithValue("@pr_request_id", objFilespec.req_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public string getFileSpecIds(string Name, string flag, string uniq_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "sp_get_FileSpecID";
            SqlCommand com = new SqlCommand(query, con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@flag", flag);
            com.Parameters.AddWithValue("@Name", Name);
            com.Parameters.AddWithValue("@uniq_id", uniq_id);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string Id = "";
            if (DR1.HasRows)
            {
                Id = DR1.GetValue(0).ToString();
            }
            con.Close();
            return Id;

        }


        // Master View-by Dhanashree-23.05.18
        //get FileSubTypeMastre
        public DataTable getFileSubTypeMaster(FileSpec objFilespec)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_filesubtype_master", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //public DataTable getCutSpecMaster()
        //{
        //    DataTable dt = new DataTable();
        //    connectionstring = objUtils.getConnString();
        //    SqlConnection con = new SqlConnection(connectionstring);
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    try
        //    {
        //        //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
        //        //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
        //        //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        con.Close();
        //        da.Fill(dt);
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //        con.Close();
        //        con.Dispose();
        //    }
        //}

        //get Change Note approval grid-24 May 2018 by Dhanashree

        #region Drawing by Dhanashree
        //get profile Details-Dhanashree Kolpe 17.06-2018
        public DataTable get_DrawingValues(FileSpec objFilespec)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("get_Drawing_inputs", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@filesize", objFilespec.filesizecode);
                cmd.Parameters.AddWithValue("@filetype", objFilespec.filetypecode);
                cmd.Parameters.AddWithValue("@filesubtype", objFilespec.filesubtypecode);
                cmd.Parameters.AddWithValue("@dwg_prod_sku", objFilespec.dwg_prod_sku);
                cmd.Parameters.AddWithValue("@dwg_imgname", objFilespec.dwg_imgname);
                cmd.Parameters.AddWithValue("@dwg_pdfname", objFilespec.dwg_pdfname);
                cmd.Parameters.AddWithValue("@dwg_dxfname", objFilespec.dwg_dxfname);
                cmd.Parameters.AddWithValue("@reqno", objFilespec.ReqNo);
                cmd.Parameters.AddWithValue("@cutstandard", objFilespec.cutstandard);
                cmd.Parameters.AddWithValue("@flag", objFilespec.flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }




        #endregion


        #region Label library by Dhanashree
        //get profile Details-Dhanashree Kolpe 17.06-2018
        public DataTable get_LabelLibrary()
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Label_Library", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }




        #endregion


        public DataTable getChangeNote(string flag, string userid)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ChangeNote", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@userid", userid);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //get Requests
        public DataTable getRequests(string flag, string user_id, string ReqNo, string pr_uniq_id)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Requests", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.AddWithValue("@req_no", ReqNo);
            cmd.Parameters.AddWithValue("pr_uniq_id", pr_uniq_id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public int CreateNewReq(string pr_uniq_id, string user_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_NewReq", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_type", "N");
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            cmd.Parameters.AddWithValue("@user_id", user_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        //Update Prod Approval-25.05.2018-Dhanashree
        public int UpdateProdApproval(string reqtype, int reqid)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_ProdApproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@reqid", reqid);
            cmd.Parameters.AddWithValue("@reqtype", reqtype);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //Update Prod Approval-25.05.2018-Dhanashree
        public int UpdateDispatchApproval(string reqtype, int reqid)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_DispatchApproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@reqid", reqid);
            cmd.Parameters.AddWithValue("@reqtype", reqtype);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        //Overview Process-for Production
        public DataTable getOverviewProcess(string type, int id, string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_OverviewProcess", con);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //Update Customer Approval-27.05.2018-Dhanashree
        public int UpdateCustomerApproval(string reqtype, int reqid, string flag)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Final_Approval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@req_id", reqid);
            cmd.Parameters.AddWithValue("@req_type", reqtype);
            cmd.Parameters.AddWithValue("@flag", flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //Insert into Item Part No for finalm approval-29.05.2018-by Dhanashree
        public int InsertforFinalApproval(string reqtype, int reqid)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_insrt_for_finalapproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@req_id", reqid);
            cmd.Parameters.AddWithValue("@req_type", reqtype);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //Insert into Item Part No for finalm approval-29.05.2018-by Dhanashree
        public int UpdateFinalApproval(string reqtype, int reqid, string approveflag, string remark, string status, string userid)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_final_approval_foritemgeneration", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@req_id", reqid);
            cmd.Parameters.AddWithValue("@req_type", reqtype);
            cmd.Parameters.AddWithValue("@approveflag", approveflag);
            cmd.Parameters.AddWithValue("@remark", remark);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@createdby", userid);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get Master lists 21.08.2018-by Dhanashree
        public DataTable getNewMastersList(string type, int id, string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_lists", con);
            cmd.Parameters.AddWithValue("@reuest_type", type);
            cmd.Parameters.AddWithValue("@request_id", id);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //Approval remarks-1st grid 30.05.2018-by Dhanashree
        public DataTable ApprovalRemarks(string type, int id, string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ChangeNote_ApprovalRemarks", con);
            cmd.Parameters.AddWithValue("@req_type", type);
            cmd.Parameters.AddWithValue("@req_id", id);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //Save Approval remarks
        public int SaveApprovalRemarks(ApprovalPreparationEntity objApprove)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Save_ApprovalRemark", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@req_id", objApprove.req_id);
            cmd.Parameters.AddWithValue("@req_type", objApprove.req_type);
            cmd.Parameters.AddWithValue("@remarks_from", objApprove.Appr_from);
            cmd.Parameters.AddWithValue("@remarks_for", objApprove.ap_remarks_for);
            cmd.Parameters.AddWithValue("@remarks", objApprove.ap_remarks);
            cmd.Parameters.AddWithValue("@createdby", objApprove.ap_createby);
            cmd.Parameters.AddWithValue("@createddate", objApprove.ap_createdDate);
            int i = cmd.ExecuteNonQuery();
            return i;
        }



        //Insert into Item Part No for finalm approval-29.05.2018-by Dhanashree
        public int UpdateRejectionRemark(string reqtype, int reqid, string remark, string flag)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_RejectionRemark", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@req_id", reqid);
            cmd.Parameters.AddWithValue("@req_type", reqtype);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@remark", remark);
            int i = cmd.ExecuteNonQuery();
            return i;
        }



        public DataTable GetEmailId(string Role, string reqno, string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_getEmail", con);
            cmd.Parameters.AddWithValue("@role", Role);
            cmd.Parameters.AddWithValue("@reqno", reqno);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        #region Edit Code for Req No
        public DataSet getEditDetails(string ReqNo, string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_EditDetails_for_ReqNo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@req_no", ReqNo);
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        #endregion

        public int UpdatedispatchSKU(ItemPartNumEntity itemNumEntity)
        {
            int result;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "sp_UpdateDispSKU";

                SqlCommand cmd = new SqlCommand(insertStmt, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pn_tmp_reqtype", itemNumEntity.requestType);
                cmd.Parameters.AddWithValue("@pn_tmp_request_id", itemNumEntity.requestId);
                cmd.Parameters.AddWithValue("@pn_tmp_itemno", itemNumEntity.itemNum);
                cmd.Parameters.AddWithValue("@pn_tmp_dispatchno", itemNumEntity.dispSKU);
                cmd.Parameters.Add("@result", SqlDbType.Int);
                cmd.Parameters["@result"].Direction = ParameterDirection.Output;
                // cmd.ExecuteScalar();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["@result"].Value;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public void DeactiveRequest(ItemPartNumEntity itemNumEntity)
        {
            // int result;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "sp_deactivate_duplicate_Request";

                SqlCommand cmd = new SqlCommand(insertStmt, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pn_tmp_reqtype", itemNumEntity.requestType);
                cmd.Parameters.AddWithValue("@pn_tmp_request_id", itemNumEntity.requestId);
                // cmd.ExecuteScalar();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            // return result;
        }

        #region Clone part no by Dhanashree
        public DataTable CreateCloneReq(string pr_uniq_id, int Req_id, string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Clone_PartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            cmd.Parameters.AddWithValue("@Req_id", Req_id);
            cmd.Parameters.AddWithValue("@flag", flag);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        #endregion
        #region Item master Filters
        public DataTable Itemmasterfilter(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ItemMasterDDL_List", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        #endregion


        #region Drawing Master
        public DataTable DrawingMaster(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Drawing_Master", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //New Request send for approval-by Dhanashree
        public int SendForApprovalNewRequest(string prod_sku, string flag, string req_no, string remark,
            string status)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Drawing_Master", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@prod_sku", prod_sku);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@reqno", req_no);
            cmd.Parameters.AddWithValue("@remark", remark);
            cmd.Parameters.AddWithValue("@status", status);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        #endregion



        #region Run Batch jobs for Part no-Dhanashree
        public DataTable RunBatchJobs(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_RunBatchjobs", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        //Archives
        public DataTable Archives(string flag, string userid)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Archives", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@userid", userid);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                //cmd.Parameters.AddWithValue("@filetype", objFilespec.FileType);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

       

        //Add New Stamp
        public DataTable AddNewStamp(StampMasterEntity stampObj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //string query = "select cm_cust_id from tb_customer_master where cm_cust_name='" + customerName + "'";
            string query = "sp_AddNewStamp";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@sm_chart_num", stampObj.sm_chart_num);
            cmd.Parameters.AddWithValue("@sm_cust_id", stampObj.sm_cust_id);
            cmd.Parameters.AddWithValue("@sm_brand_id", stampObj.sm_brand_id);
            cmd.Parameters.AddWithValue("@sm_ftype_id", stampObj.sm_ftype_id);
            cmd.Parameters.AddWithValue("@sm_fstype_id", stampObj.sm_fstype_id);
            cmd.Parameters.AddWithValue("@sm_fsize_id", stampObj.sm_fsize_id);
            cmd.Parameters.AddWithValue("@sm_stamp_mode", stampObj.sm_stamp_mode);
            cmd.Parameters.AddWithValue("@sm_chartdoc_name", stampObj.sm_chartdoc_name);
            cmd.Parameters.AddWithValue("@sm_chart_remarks", stampObj.sm_chart_remarks);
            cmd.Parameters.AddWithValue("@sm_ver_no", stampObj.sm_ver_no);
            cmd.Parameters.AddWithValue("@sm_createby", stampObj.sm_createby);
            cmd.Parameters.AddWithValue("@flag", stampObj.sm_flag);
            cmd.Parameters.AddWithValue("@req_type", stampObj.sm_request_type);
            cmd.Parameters.AddWithValue("@req_id", stampObj.sm_request_id);
           // cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;           
        }

        //Add New Handle
        public void AddNewHandle(HandleMasterEntity handleObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //string query = "select cm_cust_id from tb_customer_master where cm_cust_name='" + customerName + "'";
            string query = "sp_AddNewHandle";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@hm_chart_num", handleObj.hm_chart_num);
            cmd.Parameters.AddWithValue("@hm_handle_type", handleObj.hm_handle_type);
            cmd.Parameters.AddWithValue("@hm_ftype_id", handleObj.hm_ftype_id);
            cmd.Parameters.AddWithValue("@hm_fstype_id", handleObj.hm_fstype_id);
            cmd.Parameters.AddWithValue("@hm_fsize_id", handleObj.hm_fsize_id);
            cmd.Parameters.AddWithValue("@hm_chartimg_name", handleObj.hm_chartimg_name);
            cmd.Parameters.AddWithValue("@hm_chart_remarks", handleObj.hm_chart_remarks);
            cmd.Parameters.AddWithValue("@hm_createby", handleObj.hm_createby);
            cmd.Parameters.AddWithValue("@hm_verno", handleObj.hm_verno);
            cmd.Parameters.AddWithValue("@req_type", handleObj.hm_request_type);
            cmd.Parameters.AddWithValue("@req_id", handleObj.hm_request_id);
            cmd.Parameters.AddWithValue("@flag", handleObj.flag);
            cmd.ExecuteNonQuery();
        }

        #endregion

        #region For Customer Details >>CustomerLIst -Archana Mahajan
        //For Customer Details >>CustomerLIst -Archana Mahajan
        public DataSet getCustomerList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_customer_master_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }


        public void saveInnerLabel(DispatchSKUEntity entity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "update tb_dispatch_sku set dp_innerlabel_chart ='" + entity.dp_innerlabel_chart
                                    + "'where dp_request_type = '" + entity.dp_request_type
                                    + "' and dp_request_id ='" + entity.dp_request_id + "'";

                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public DataSet getCutStandards(string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(" select DISTINCT vtpi_cut_std_name, vtpi_cut_std_code from tv_cutlocn_tpi_mstr  where vtpi_fsize_code ='" + fileSize + "' AND vtpi_ftype_code ='" + fileTypeCode + "'  AND vtpi_cut_code ='" + fileCutType + "';", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }



        public DataSet getBrandList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_brand_master_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet getShipTOCountryList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_shiptocountry_master_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }



        public string getCustomerID(string customerName)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select cm_cust_id from tb_customer_master where cm_cust_name='" + customerName + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string custId = DR1.GetValue(0).ToString();
            con.Close();
            return custId;

        }





        public int getPalletId(string pallet_chart)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select pm_pallet_id from tb_pallet_master where pm_pallet_chartnum='" + pallet_chart + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            int palletId = 0;
            if (DR1.HasRows)
            {
                palletId = (int)DR1.GetValue(0);
            }

            con.Close();
            return palletId;
        }



        public DataSet getPalletDetials(int palletId)
        {
            PalletDetailsEntity DetailsEntity = new PalletDetailsEntity();
            List<PalletDetailsEntity> DetailsEntityList = new List<PalletDetailsEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);

            con.Open();
            SqlCommand cmd = new SqlCommand("select pd_pallet_id, pd_parm_scrnvalue, pd_parm_name from tb_pallet_details where pd_pallet_id ='" + palletId + "'", con);
            cmd.ExecuteNonQuery();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }


        public string getBrandId(string brandName)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select bm_brand_id from tb_brand_master where bm_brand_name='" + brandName + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string brandId = DR1.GetValue(0).ToString();
            con.Close();
            return brandId;
        }

        public string getCountryId(string countryName)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select cm_country_cd from tb_shiptocountry_master where cm_country_name='" + countryName + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string countryId = DR1.GetValue(0).ToString();
            con.Close();
            return countryId;
        }
        public string Checkinnerboxqty(int id, string type)
        {
            //connectionstring = objUtils.getConnString();
            //SqlConnection con = new SqlConnection(connectionstring);
            //con.Open();
            //SqlCommand cmd = new SqlCommand("sp_Check_innerlabelQty", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@id", id);
            //cmd.Parameters.AddWithValue("@type", type);
            //string Qty =Convert.ToString(cmd.ExecuteScalar());
            //return Qty;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select ib_qty_inbox from tb_innerbox_master where ib_chartnum=(select top 1 dp_innerbox_chart from tb_dispatch_sku where dp_request_id='" + id + "' and dp_request_type='" + type + "') ";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string Qty;
            if (DR1.HasRows == true)
            {
                Qty = DR1.GetValue(0).ToString();
            }
            else
            {
                con.Close();
                con.Open();
                string query1 = "select ib_qty_inbox from tb_innerbox_master where ib_request_id='" + id + "' and ib_request_type='" + type + "'";
                SqlCommand com1 = new SqlCommand(query1, con);
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter(com1);
                adp.Fill(ds);
                Qty = ds.Tables[0].Rows[0]["ib_qty_inbox"].ToString();
            }
            //con.Close();
            return Qty;
        }
        public int saveCustomerDetails(CustomerDetailsEntity custObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_CustomerDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.AddWithValue("@pr_request_type", 'N');
            cmd.Parameters.AddWithValue("@pr_cust_id", custObj.Cm_Cust_Id);
            cmd.Parameters.AddWithValue("@pr_cust_name", custObj.Cm_Cust_Name);
            cmd.Parameters.AddWithValue("@pr_brand_id", custObj.brandId);
            cmd.Parameters.AddWithValue("@pr_brand_desc", custObj.brandName);
            cmd.Parameters.AddWithValue("@pr_ship2country_id", custObj.countryId);
            cmd.Parameters.AddWithValue("@pr_ship2country_name", custObj.countryName);
            cmd.Parameters.AddWithValue("@pr_sap_no", custObj.sapNo);
            cmd.Parameters.AddWithValue("@pr_customer_part_no", custObj.customerPartNO);
            cmd.Parameters.AddWithValue("@pr_uniq_id", custObj.pr_uniq_id);
            cmd.Parameters.AddWithValue("@pr_request_type", custObj.cm_request_type);
            cmd.Parameters.AddWithValue("@pr_request_id", custObj.cm_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        //for Cutspecs tab -16 May 2018 -by Archana

        public CutSpecsEntity getCutLocations(string fileTypeCode)
        {
            CutSpecsEntity cutSpecs = new CutSpecsEntity();
            List<CutSpecsEntity> cutSpecsObjList = new List<CutSpecsEntity>();
            //DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM tb_filecutlocn_master where fcl_ftype_code = '" + fileTypeCode + "' ORDER BY fcl_parm_seq_no asc;", con);
                cmd.ExecuteNonQuery();
                using (SqlDataReader DR1 = cmd.ExecuteReader())
                {
                    while (DR1.Read())
                    {
                        CutSpecsEntity cutSpecsObj1 = new CutSpecsEntity();
                        cutSpecsObj1.fcl_parm_code = (string)DR1["fcl_parm_code"];
                        cutSpecsObj1.prc_parameter_value = (string)DR1["fcl_parm_value"];
                        cutSpecsObj1.fcl_overcut_flg = (bool)DR1["fcl_overcut_flg"];
                        cutSpecsObj1.fcl_parm_seq_no = (Byte)DR1["fcl_parm_seq_no"];
                        cutSpecsObj1.fcl_upcut_flg = (bool)DR1["fcl_upcut_flg"];
                        cutSpecsObj1.fcl_upcut_flg = (bool)DR1["fcl_upcut_flg"];
                        cutSpecsObjList.Add(cutSpecsObj1);
                    }

                    con.Close();
                }
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            cutSpecs.cutSpecsEntityList = cutSpecsObjList;

            return cutSpecs;
        }

        public DataSet getCutLocationTpiMasterDetails(string selectedCutStandardName, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            CutLocationTpiMasterEntity Entity = new CutLocationTpiMasterEntity();
            List<CutLocationTpiMasterEntity> DetailsEntityList = new List<CutLocationTpiMasterEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //   SqlCommand cmd = new SqlCommand("select * from tv_cutlocn_tpi_mstr where vtpi_ftype_code ='" + fileTypeCode + "' And vtpi_fstype_code = '" + FileSubType + "' and vtpi_fsize_code ='" + fileSize + "'  and vtpi_cut_code ='" + fileCutType + "' and vtpi_cut_std_code ='" + selectedCutStandardName + "' order by vtpi_parm_seq_no", con);
            SqlCommand cmd = new SqlCommand("select * from tv_cutlocn_tpi_mstr where vtpi_ftype_code ='" + fileTypeCode + "' and vtpi_fsize_code ='" + fileSize + "'  and vtpi_cut_code ='" + fileCutType + "' and vtpi_cut_std_code ='" + selectedCutStandardName + "' order by vtpi_parm_seq_no", con);

            cmd.ExecuteNonQuery();
            try
            {
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;

        }



        public void saveCutSpecs(ProductionSKUCutSpecsEntity cutSpecsObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_CutSpecsDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@prc_request_type", 'N');
            cmd.Parameters.AddWithValue("@prc_request_id", 1001);
            cmd.Parameters.AddWithValue("@prc_parameter_name", cutSpecsObj.prc_parameter_name);
            cmd.Parameters.AddWithValue("@prc_parameter_value ", cutSpecsObj.prc_parameter_value);
            cmd.Parameters.AddWithValue("@prc_upcut_flg", cutSpecsObj.prc_upcut_flg);
            cmd.Parameters.AddWithValue("@prc_overcut_flg", cutSpecsObj.prc_overcut_flg);
            cmd.Parameters.AddWithValue("@prc_edgecut_flg", cutSpecsObj.prc_edgecut_flg);
            int i = cmd.ExecuteNonQuery();
            //return i;

        }
        
      public int checkiduniqness1(string pm_fstype_code,string pm_fsize_code,string pm_ftype_code, string flag)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_CheckUiqness", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", pm_fstype_code);
            cmd.Parameters.AddWithValue("@fsizeid", pm_fsize_code);
            cmd.Parameters.AddWithValue("@ftypeid", pm_ftype_code);
            cmd.Parameters.AddWithValue("@flag", flag);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }

        public int checkiduniqnecut(int prc_request_id, string prc_request_type, string cutStandardCode, string flg)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_CheckUiqness", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", cutStandardCode);
            cmd.Parameters.AddWithValue("@request_id", prc_request_id);
            cmd.Parameters.AddWithValue("@request_type", prc_request_type);
            cmd.Parameters.AddWithValue("@flag", flg);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }

        public int checkiduniqness(string cm_Cust_Id, string flag)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_CheckUiqness", con);
            cmd.CommandType = CommandType.StoredProcedure;      
            cmd.Parameters.AddWithValue("@id",cm_Cust_Id);
            cmd.Parameters.AddWithValue("@flag", flag);
            int i =Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }

        public RequestDeatilEntity getRequestId(string uniqId)
        {
            RequestDeatilEntity requestObj = new RequestDeatilEntity();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select pr_request_type,pr_request_id  from tb_production_sku where pr_uniq_id='" + uniqId + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();

            //string countryId = DR1.GetValue(0).ToString();
            while (DR1.Read())
            {
                requestObj.requestType = DR1["pr_request_type"].ToString();
                requestObj.requestId = Convert.ToInt32(DR1["pr_request_id"]);
            }



            con.Close();
            return requestObj;
        }

        // Cutspecs By Archana 20/5/2018
        public void saveCutSpecs(CutSpecsEntity cutsSpecsEntity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();

            string tempReqType = cutsSpecsEntity.prc_request_type;
            int tempReqId = cutsSpecsEntity.prc_request_id;
            SqlCommand checkRecordExistsOrNot = new SqlCommand(" SELECT COUNT(prc_request_id) FROM tb_prodsku_cutspec WHERE prc_request_id = '" + tempReqId + "' and prc_request_type ='" + tempReqType + "' ", con);

            int recordExist = (int)checkRecordExistsOrNot.ExecuteScalar();

            if (recordExist == 0)
            {

                string insertStmt = "INSERT INTO tb_prodsku_cutspec(prc_request_type, prc_request_id, prc_parameter_name, prc_parameter_value,prc_upcut_flg,prc_overcut_flg,prc_edgecut_flg,prc_upcut_value,prc_overcut_value,prc_edgecut_value,prc_createdate,prc_createby) " +
                                  "VALUES(@prc_request_type, @prc_request_id, @prc_parameter_name, @prc_parameter_value,@prc_upcut_flg,@prc_overcut_flg,@prc_edgecut_flg,@prc_upcut_value,@prc_overcut_value,@prc_edgecut_value,@prc_createdate,@prc_createby)";

                SqlCommand cmd = new SqlCommand(insertStmt, con);
                foreach (var j in cutsSpecsEntity.CutLocationTpiMasterModelList)
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@prc_request_type", tempReqType);
                    cmd.Parameters.AddWithValue("@prc_request_id", tempReqId);
                    cmd.Parameters.AddWithValue("@prc_parameter_name", j.vtpi_parm_value);
                    cmd.Parameters.AddWithValue("@prc_parameter_value ", j.vtpi_cut_std_code);
                    //cmd.Parameters.AddWithValue("@vtpi_cut_std_name ", j.vtpi_cut_std_name);
                    cmd.Parameters.AddWithValue("@prc_upcut_flg", j.vtpi_upcut_flg);
                    cmd.Parameters.AddWithValue("@prc_overcut_flg", j.vtpi_overcut_flg);
                    cmd.Parameters.AddWithValue("@prc_edgecut_flg", j.vtpi_edgecut_flg);
                    cmd.Parameters.AddWithValue("@prc_createdate", DateTime.Now);

                    if (j.UPCUT != null)
                    {
                        cmd.Parameters.AddWithValue("@prc_upcut_value", j.UPCUT);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@prc_upcut_value", "NA");
                    }
                    if (j.OVERCUT != null)
                    {
                        cmd.Parameters.AddWithValue("@prc_overcut_value", j.OVERCUT);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@prc_overcut_value", "NA");
                    }
                    if (j.EDGE != null)
                    {
                        cmd.Parameters.AddWithValue("@prc_edgecut_value", j.EDGE);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@prc_edgecut_value", "NA");
                    }

                    cmd.Parameters.AddWithValue("@prc_createby", "Initial Upload");

                    cmd.ExecuteNonQuery();



                }
                //int i = cmd.ExecuteNonQuery();

                string updateStmt1 = "UPDATE tb_production_sku SET pr_cut_standard='" + cutsSpecsEntity.CutLocationTpiMasterModelList[0].vtpi_cut_std_code + "' WHERE pr_request_type = '" + tempReqType + "' and pr_request_id = '" + tempReqId + "'";
                SqlCommand cmd2 = new SqlCommand(updateStmt1, con);
                cmd2.ExecuteNonQuery();

                string updateStmt = "UPDATE tb_production_sku SET pr_isApproved='S',pr_approvedate = '" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' WHERE pr_request_type = '" + tempReqType + "' and pr_request_id = '" + tempReqId + "'";
                SqlCommand cmd1 = new SqlCommand(updateStmt, con);
                cmd1.ExecuteNonQuery();
            }
            else
            {
                string updateStmt1 = "UPDATE tb_production_sku SET pr_cut_standard='" +
                    cutsSpecsEntity.CutLocationTpiMasterModelList[0].vtpi_cut_std_code + "' WHERE pr_request_type = '" + tempReqType + "' and pr_request_id = '" + tempReqId + "'";
                SqlCommand cmd2 = new SqlCommand(updateStmt1, con);
                cmd2.ExecuteNonQuery();


                string updateStmt = "UPDATE tb_production_sku SET pr_isApproved='S',pr_approvedate = '" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' WHERE pr_request_type = '" + tempReqType + "' and pr_request_id = '" + tempReqId + "'";
                SqlCommand cmd1 = new SqlCommand(updateStmt, con);
                cmd1.ExecuteNonQuery();
            }
        }

        public void saveItemNum(ItemPartNumEntity itemNumEntity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "INSERT INTO tb_temp_partno(pn_tmp_reqtype ,pn_tmp_request_id,pn_tmp_itemno) " +
                                  "VALUES(@pn_tmp_reqtype, @pn_tmp_request_id,@pn_tmp_itemno)";

                SqlCommand cmd = new SqlCommand(insertStmt, con);
                cmd.Parameters.AddWithValue("@pn_tmp_reqtype", itemNumEntity.requestType);
                cmd.Parameters.AddWithValue("@pn_tmp_request_id", itemNumEntity.requestId);
                cmd.Parameters.AddWithValue("@pn_tmp_itemno", itemNumEntity.itemNum);
                // cmd.ExecuteScalar();
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public string getChartNum(StampMasterEntity stampObj)
        {
            DataSet ds = new DataSet();
            string chartNum;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //string query = "select cm_cust_id from tb_customer_master where cm_cust_name='" + customerName + "'";
            string query = "select sm_chart_num from tb_stamp_master where sm_cust_id ='" + stampObj.sm_cust_id + "' And sm_brand_id='" + stampObj.sm_brand_id + "' And sm_ftype_id='" + stampObj.sm_ftype_id + "' And sm_fstype_id='" + stampObj.sm_fstype_id + "' And sm_fsize_id='" + stampObj.sm_fsize_id + "' And sm_stamp_mode='" + stampObj.sm_stamp_mode + "' And sm_isActive= 1 And sm_isApproved='Y'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            if (DR1.HasRows)
            {
                chartNum = DR1.GetValue(0).ToString();
            }
            else
                chartNum = "No Chart Is Present!";


            //int dbFields = DR1.FieldCount;
            //if (DR1.HasRows)
            //{
            //    chartNum = DR1.GetValue(0).ToString();
            //}
            //else
            //{
            //    DR1.Close();
            //    string query1 = "select sm_chart_num from tb_stamp_master where sm_cust_id ='" + stampObj.sm_cust_id + "' And sm_brand_id='" + stampObj.sm_brand_id + "' And sm_ftype_id='" + stampObj.sm_ftype_id+ "' And sm_fsize_id='" + stampObj.sm_fsize_id + "' And sm_stamp_mode='" + stampObj.sm_stamp_mode + "' And sm_isActive= 1 And sm_isApproved='Y'";
            //    SqlCommand com1 = new SqlCommand(query1, con);
            //     DR1 = com1.ExecuteReader();
            //    DR1.Read();
            //    if (DR1.HasRows)
            //    {
            //        chartNum = DR1.GetValue(0).ToString();
            //    }
            //    else
            //    chartNum = "No Chart Is Present!";
            //}

            //  chartNum = "No Chart Is Present!";

            //chartNum = DR1.GetValue(0).ToString();
            con.Close();
            return chartNum;
        }



        public DataSet getStampModeList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_stamp_mode_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public void saveStampDetailsToDispatchSKU(DispatchSKUEntity dispatchEntity)
        {

            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand checkRecordExistsOrNot = new SqlCommand(" SELECT COUNT(dp_request_id) FROM tb_dispatch_sku WHERE dp_request_id = '" + dispatchEntity.dp_request_id + "' and dp_request_type ='" + dispatchEntity.dp_request_type + "' ", con);

                int recordExist = (int)checkRecordExistsOrNot.ExecuteScalar();
                DateTime now = DateTime.Now;
                if (recordExist > 0) //anything different from 1 should be wrong
                {
                    string updateStmt = "update tb_dispatch_sku set dp_stamp_type='" + dispatchEntity.dp_stamp_type
                                                            + "' ,dp_stamp_chart='" + dispatchEntity.dp_stamp_chart
                                                            + "' ,dp_isApproved='" + 'S'

                                                             + "' ,dp_isActive='" + 1
                                                             + "' ,dp_createdate='" + now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture)
                                                              + "',dp_approvedate='" + now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture)
                                                              + "',dp_approveby='" + "Initial Upload"
                                                           + "' where dp_request_type = '" + dispatchEntity.dp_request_type

                                                           + "' and dp_request_id ='" + dispatchEntity.dp_request_id + "'";

                    SqlCommand cmd1 = new SqlCommand(updateStmt, con);

                    cmd1.ExecuteNonQuery();
                }

                else
                {
                    string insertStmt = "INSERT INTO tb_dispatch_sku(dp_request_type ,dp_request_id,dp_stamp_type,dp_stamp_chart,dp_isApproved,dp_isActive,dp_createdate,dp_approvedate,dp_approveby) " +
                                 "VALUES(@dp_request_type, @dp_request_id,@dp_stamp_type,@dp_stamp_chart,@dp_isApproved,@dp_isActive,@dp_createdate,@dp_approvedate,@dp_approveby)";

                    SqlCommand cmd = new SqlCommand(insertStmt, con);
                    cmd.Parameters.AddWithValue("@dp_request_type", dispatchEntity.dp_request_type);
                    cmd.Parameters.AddWithValue("@dp_request_id", dispatchEntity.dp_request_id);
                    cmd.Parameters.AddWithValue("@dp_stamp_type", dispatchEntity.dp_stamp_type);
                    cmd.Parameters.AddWithValue("@dp_stamp_chart", dispatchEntity.dp_stamp_chart);
                    // cmd.ExecuteScalar();@,@,@,@,@,@
                    cmd.Parameters.AddWithValue("@dp_isApproved", "S");
                    cmd.Parameters.AddWithValue("@dp_isActive", "1");
                    cmd.Parameters.AddWithValue("@dp_createdate", now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture));
                    // cmd.Parameters.AddWithValue("@dp_createby", "Initial Load");
                    cmd.Parameters.AddWithValue("@dp_approvedate", now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture));

                    cmd.Parameters.AddWithValue("@dp_approveby", "Initial Load");
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<StampDetailsEntity> getStampDetailsForModal(string chartNum)
        {
            StampDetailsEntity stampDetailsEntity = new StampDetailsEntity();
            List<StampDetailsEntity> stampDetailsEntityList = new List<StampDetailsEntity>();
            //DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            int stampId = getStampIdByChartNum(chartNum);
            try
            {
                con.Open();
                //SqlCommand cmd = new SqlCommand("DECLARE @cols AS NVARCHAR(MAX),  @query  AS NVARCHAR(MAX);select @cols = STUFF((SELECT distinct ',' + QUOTENAME(c.sd_parm_name)  FROM tb_stamp_details c   FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)')  ,1,1,'') set @query = 'SELECT sd_stampimg_id, ' + @cols + ' from    ( select sd_stampimg_id, sd_parm_name, sd_parm_scrnvalue from tb_stamp_details where sd_stamp_id = " + stampId + ") x  pivot   ( max(sd_parm_scrnvalue)   for sd_parm_name in (' + @cols + ')   ) p ' execute(@query)", con);
                SqlCommand cmd = new SqlCommand("SELECT distinct [sm_chartdoc_name] FROM [dbo].[tb_stamp_master] WHERE [sm_chart_num]='" + chartNum + "'", con);
                cmd.ExecuteNonQuery();
                using (SqlDataReader DR1 = cmd.ExecuteReader())
                {
                    while (DR1.Read())
                    {
                        StampDetailsEntity stampDetailsEntityObj = new StampDetailsEntity();
                        stampDetailsEntityObj.sm_chartdoc_name = (string)DR1["sm_chartdoc_name"];
                        //stampDetailsEntityObj.side = (string)DR1["SIDE"];
                        ////if((string)DR1["LETTER HEIGHT"]== null)
                        ////{
                        ////    stampDetailsEntityObj.letterHeight = "-";
                        ////}
                        ////stampDetailsEntityObj.letterHeight = (string)DR1["LETTER HEIGHT"] ;

                        ////if ((string)DR1["LETTER HEIGHT IN MM"] != null)
                        ////{
                        ////    stampDetailsEntityObj.Letter_Height_in_mm = (string)DR1["LETTER HEIGHT IN MM"];
                        ////}
                        //stampDetailsEntityObj.orientation = (string)DR1["ORIENTATION"];
                        //stampDetailsEntityObj.place = (string)DR1["PLACE"];
                        //// stampDetailsEntityObj.sd_parm_code = (string)DR1["sd_parm_code"];
                        //stampDetailsEntityObj.Size = (string)DR1["SIZE"];
                        //stampDetailsEntityObj.ImageName = (string)DR1["IMAGE"];
                        // stampDetailsEntityObj.sd_stampimg_id = (int)DR1["sd_stampimg_id"];
                        stampDetailsEntityList.Add(stampDetailsEntityObj);
                        //stampDetailsEntityObj.
                    }

                    con.Close();
                }
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            stampDetailsEntity.StampDetailsEntityList = stampDetailsEntityList;

            return stampDetailsEntityList;
        }



        //for getting stampId 
        public int getStampIdByChartNum(string chartNum)
        {
            int stampId = 0;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = " select sm_stamp_id  FROM tb_stamp_master where sm_chart_num ='" + chartNum + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            if (DR1.HasRows)
            {
                stampId = (int)DR1.GetValue(0);
            }
            con.Close();
            return stampId;
        }



        public DataSet getHandelPresenceList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_handlepresence_master_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }



        public DataSet getViewWrappingDetails(int wrapperId)
        {
            WrapperDetailsEntity wrapperDetailsEntity = new WrapperDetailsEntity();
            List<WrapperDetailsEntity> wrapperDetailsEntityList = new List<WrapperDetailsEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("select  wd_wrapper_recid, wd_parm_name, wd_parm_scrnvalue from tb_wrapper_details  where wd_wrapper_id = '" + wrapperId + "' ", con);
            cmd.ExecuteNonQuery();
            try
            {

                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return ds;
        }


        public DataSet getHandleMasterList(string fileTypeCode, string FileSubType)
        {

            DataSet dt = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_handle_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@hm_ftype_id", fileTypeCode);
                cmd.Parameters.AddWithValue("@hm_fstype_id", FileSubType);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        #region get Handle Subtype by Dhanashree
        public DataSet getHandleSubType(string handleType, string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(" select distinct hm_handle_subtype from tb_handle_master where hm_ftype_id ='" + fileTypeCode + "' AND hm_fstype_id ='" + FileSubType + "' AND hm_handle_type='" + handleType + "' AND hm_fsize_id ='" + fileSize + "'AND hm_isApproved = 'Y' AND hm_isActive = 1;", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        public DataSet getHandleMasterList(string handleType, string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                //SqlCommand cmd = new SqlCommand(" select hm_chart_num from tb_handle_master where hm_ftype_id ='" + fileTypeCode + "' AND hm_fstype_id ='" + FileSubType + "' AND hm_handle_type='" + handleType + "' AND hm_fsize_id ='" + fileSize + "' AND hm_handle_subtype ='" + subtype + "'AND hm_isApproved = 'Y' AND hm_isActive = 1;", con);
                SqlCommand cmd = new SqlCommand(" select hm_chart_num from tb_handle_master where hm_ftype_id ='" + fileTypeCode + "' AND hm_fstype_id ='" + FileSubType + "' AND hm_handle_type='" + handleType + "' AND hm_fsize_id ='" + fileSize + "'AND hm_isApproved = 'Y' AND hm_isActive = 1;", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                //adp.Fill(ds);

                int results = adp.Fill(ds, "hm_chart_num");

                if (results == 0)
                {

                    SqlCommand cmd1 = new SqlCommand(" select hm_chart_num from tb_handle_master where hm_ftype_id ='" + fileTypeCode + "' AND hm_handle_type='" + handleType + "' AND hm_fsize_id ='" + fileSize + "'AND hm_isApproved = 'Y' AND hm_isActive = 1;", con);
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter adp1 = new SqlDataAdapter(cmd);
                    adp1.Fill(ds);
                }
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }



        public int getHandleId(string handleType)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select hm_handle_id from tb_handle_master where hm_chart_num='" + handleType + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            int handleID = (int)DR1.GetValue(0);
            con.Close();
            return handleID;
        }

        public DataSet getHandleDrawingDetails(int handleid)
        {
            HandleDetailsEntity handleDetailsEntity = new HandleDetailsEntity();
            List<HandleDetailsEntity> handleDetailsEntityList = new List<HandleDetailsEntity>();

            // DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            //con.Open();
            //SqlCommand cmd = new SqlCommand("sp_get_handledetails", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            //SqlCommand cmd = new SqlCommand("DECLARE @colsnew AS NVARCHAR(MAX) select @colsnew = STUFF((SELECT distinct ',' + QUOTENAME(c.hd_parm_name) FROM tb_handle_details  c FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') , 1, 1, '')print @colsnew declare @query nvarchar(max) set @query = 'SELECT hd_handle_recno, ' + @colsnew + ' from  (select  hd_handle_recno, hd_parm_name, hd_parm_scrnvalue from tb_handle_details  where hd_handle_id like ''" + handleid + "'' ) x pivot ( max(hd_parm_scrnvalue) for hd_parm_name in (' + @colsnew + ')   ) p 'execute(@query)", con);
            SqlCommand cmd = new SqlCommand("select  hd_handle_recno, hd_parm_name, hd_parm_scrnvalue from tb_handle_details  where hd_handle_id ='" + handleid + "' ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;

        }

        public string getHandleImageName(int handleId)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select hm_chartimg_name from tb_handle_master where hm_chart_num='" + handleId + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string imageName = DR1.GetValue(0).ToString();
            con.Close();
            return imageName;
        }


        public string getHandleImageName(string chartNum)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select hm_chartimg_name from tb_handle_master where hm_chart_num='" + chartNum + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string imageName = DR1.GetValue(0).ToString();
            con.Close();
            return imageName;
        }



        #region get pallet Image-Dhanashree Kolpe
        public string getPalletImageName(string chartNum)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select pm_pallet_chartimg from tb_pallet_master where pm_pallet_chartnum='" + chartNum + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string imageName = DR1.GetValue(0).ToString();
            con.Close();
            return imageName;
        }
        #endregion

        public void saveDispatchDetailsToDispatchSKU(DispatchSKUEntity dispatchObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt =

                     "update tb_dispatch_sku set dp_barcode_flg ='" + "True"
                                + "', dp_lineno_flg ='" + "True" + "', dp_hologram_flg='" + "True"
                                + "',dp_india_flg='" + "True" + "',dp_handle_presence='" + dispatchObj.dp_handle_presence
                                 + "', dp_qltyseal_flg ='" + "True" + "', dp_prcstkr_flg='" + "True"
                                + "',dp_polybag_flg='" + "True" + "',dp_silicagel_flg='" + "True"
                                + "',dp_fumigation_flg='" + "True"
                                + "',dp_promo_item_flg='" + "True" + "',dp_strap='" + "True"
                                + "',dp_cellotape_flg='" + "True" + "',dp_shrinkwrap_flg='" + "True" + "',dp_decicant_flg='" + "True"
                                + "',dp_paperwool_flg='" + "True" + "',dp_outer_mrking_type='" + "No"
                                + "',dp_outer_mrking_img='" + "NA.jpg" + "',dp_pallet_mrking_type='" + "No"
                                + "',dp_pallet_mrking_img='" + "NA.jpg"
                                + "',dp_handle_type='" + dispatchObj.dp_handle_type + "',dp_handle_chart='" + dispatchObj.dp_handle_chart
                                + "',dp_handle_subtype='" + dispatchObj.dp_handle_subtype + "'where dp_request_type = '" + dispatchObj.dp_request_type
                                + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";



                //"update tb_dispatch_sku set dp_barcode_flg ='" + "True"
                //                + "', dp_lineno_flg ='" + "True" + "', dp_hologram_flg='" + "True"
                //                + "',dp_india_flg='" + "True" + "',dp_handle_presence='" + dispatchObj.dp_handle_presence
                //                 + "', dp_qltyseal_flg ='" + "True" + "', dp_prcstkr_flg='" + "True"
                //                + "',dp_polybag_flg='" + "True" + "',dp_silicagel_flg='" + "True"
                //                + "',dp_fumigation_flg='" + "True"
                //                + "',dp_promo_item_flg='" + "True" + "',dp_strap='" + "True"
                //                + "',dp_cellotape_flg='" + "True" + "',dp_shrinkwrap_flg='" + "True" + "',dp_decicant_flg='" + "True"
                //                + "',dp_paperwool_flg='" + "True"
                //                + "',dp_handle_type='" + dispatchObj.dp_handle_type + "',dp_handle_chart='" + dispatchObj.dp_handle_chart
                //                + "',dp_handle_subtype='" + dispatchObj.dp_handle_subtype + "'where dp_request_type = '" + dispatchObj.dp_request_type
                //                + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";

                //"update tb_dispatch_sku set dp_handle_presence='" + dispatchObj.dp_handle_presence                                     
                //                    + "',dp_handle_type='" + dispatchObj.dp_handle_type + "',dp_handle_chart='" + dispatchObj.dp_handle_chart
                //                    + "',dp_handle_subtype='" + dispatchObj.dp_handle_subtype + "'where dp_request_type = '" + dispatchObj.dp_request_type
                //                    + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";


                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public List<PackingMaterialMAsterEntity> getWrappingTypeList(string usedFor)
        {

            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "SELECT distinct pkng_material FROM tb_pkngmaterial_master where pkng_usedfor = '" + usedFor + "' and(pkng_isApproved = 'Y') AND(pkng_isActive = 1)";
            SqlCommand com = new SqlCommand(query, con);
            List<PackingMaterialMAsterEntity> list = new List<PackingMaterialMAsterEntity>();
            using (SqlDataReader DR1 = com.ExecuteReader())
            {
                while (DR1.Read())
                {
                    PackingMaterialMAsterEntity obj = new PackingMaterialMAsterEntity();
                    obj.pkng_material = (string)DR1["pkng_material"];

                    list.Add(obj);
                }
                con.Close();
                return list;

            }

        }

        public void saveWrapperTabDetails(DispatchSKUEntity dispatchObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "update tb_dispatch_sku set dp_wrapping_type='" + dispatchObj.dp_wrapping_type
                                    + "', dp_wrapping_chart ='" + dispatchObj.dp_wrapping_chart + "' where dp_request_type = '" + dispatchObj.dp_request_type
                                    + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";

                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataSet getWrappingChartNums(string wrappingType, string fileTypeCode, string FileSubType, string fileSize, string custId, string brandId)
        {

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                // SqlCommand cmd = new SqlCommand(" select wm_chart_num from tb_wrapper_master where wm_ftype_id ='" + fileTypeCode + "' AND wm_fstype_id ='" + FileSubType + "' AND wm_wrappertype='" + wrappingType + "' AND wm_fsize_id ='" + fileSize + "'AND wm_cust_id='" + custId + "' and wm_brand_id= '" + brandId + "' and  wm_isApproved = 'Y' AND wm_isActive = 1;", con);
                SqlCommand cmd = new SqlCommand(" select wm_chart_num from tb_wrapper_master where wm_wrappertype='" + wrappingType + "' AND wm_fsize_id ='" + fileSize + "' and  wm_isApproved = 'Y' AND wm_isActive = 1;", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }


        public int getWrapperId(string wrapperChart)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select wm_wrapper_id from tb_wrapper_master where wm_chart_num='" + wrapperChart + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            int wrapperId = (int)DR1.GetValue(0);
            con.Close();
            return wrapperId;
        }

        public DataSet getQtyFOrInnerBox(string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(" select ib_box_id, ib_qty_inbox from tb_innerbox_master where ib_ftype_id ='" + fileTypeCode + "' AND ib_fstype_id ='" + FileSubType + "' AND  ib_fsize_id ='" + fileSize + "'AND ib_isApproved = 'Y' AND ib_pkg_isActive = 1;", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public void saveInnerBoxDetails(DispatchSKUEntity dispatchObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "update tb_dispatch_sku set dp_innerbox_type='" + dispatchObj.dp_innerbox_type + "' ,dp_innerbox_qty='" + dispatchObj.dp_innerbox_qty
                                    + "',dp_innerbox_chart='" + dispatchObj.dp_innerbox_chart + "' , dp_innerbox_id ='" + dispatchObj.dp_innerbox_id + "' where dp_request_type = '" + dispatchObj.dp_request_type
                                    + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";

                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void saveOuterDetails(DispatchSKUEntity dispatchObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "update tb_dispatch_sku set dp_outerbox_type='" + dispatchObj.dp_outerbox_type + "' ,dp_outerbox_chart='" + dispatchObj.dp_outerbox_chart
                                    + "' where dp_request_type = '" + dispatchObj.dp_request_type
                                    + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";

                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void savePalletDetails(DispatchSKUEntity dispatchObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                string insertStmt = "update tb_dispatch_sku set dp_pallet_type='" + dispatchObj.dp_pallet_type + "' ,dp_pallet_chart='" + dispatchObj.dp_pallet_chart
                                    + "' where dp_request_type = '" + dispatchObj.dp_request_type
                                    + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";

                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DataSet getInnerBoxChartNums(string innerBoxMaterial, string innerQuntity, string fileTypeCode, string FileSubType, string fileSize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                //SqlCommand cmd = new SqlCommand("select ib_chartnum from tb_innerbox_master where ib_box_type ='" + innerBoxMaterial + "' AND ib_qty_inbox ='" + innerQuntity + "' AND ib_isApproved = 'Y' AND ib_pkg_isActive = 1 and ib_ftype_id='" + fileTypeCode + "' and  ib_fstype_id ='" + FileSubType + "'and ib_fsize_id='" + fileSize + "'; ", con);
                SqlCommand cmd = new SqlCommand("select ib_chartnum from tb_innerbox_master where ib_box_type ='" + innerBoxMaterial + "' AND ib_isApproved = 'Y' AND ib_pkg_isActive = 1 and ib_ftype_id='" + fileTypeCode + "' and  ib_fstype_id ='" + FileSubType + "'and ib_fsize_id='" + fileSize + "'; ", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                // adp.Fill(ds);

                int results = adp.Fill(ds, "ib_chartnum");

                if (results == 0)
                {

                    SqlCommand cmd1 = new SqlCommand("select ib_chartnum from tb_innerbox_master where ib_box_type ='" + innerBoxMaterial + "' AND ib_isApproved = 'Y' AND ib_pkg_isActive = 1 and ib_ftype_id='" + fileTypeCode + "'and ib_fsize_id='" + fileSize + "'; ", con);
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter adp1 = new SqlDataAdapter(cmd);
                    adp1.Fill(ds);
                }
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet getOuterBoxChartNums(string dp_outerbox_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(" select ob_box_chartnum from tb_outerbox_master  where ob_box_type ='" + dp_outerbox_type + "'  AND ob_box_isApproved = 'Y' AND ob_box_isActive = 1;", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet getPalletChartNums(string dp_pallet_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(" select pm_pallet_chartnum from tb_pallet_master  where pm_pallet_type ='" + dp_pallet_type + "'  AND pm_isApproved = 'Y' AND pm_isActive = 1;", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public void saveOtherDetails(PartNumOtherDetailsEntity partEntity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {

                SqlCommand checkRecordExistsOrNot = new SqlCommand(" SELECT COUNT(othdt_request_id) FROM tb_partno_otherdtls WHERE othdt_request_id = '" + partEntity.othdt_request_id + "' and othdt_request_type ='" + partEntity.othdt_request_type + "' ", con);

                int recordExist = (int)checkRecordExistsOrNot.ExecuteScalar();

                if (recordExist > 0) //anything different from 1 should be wrong
                {
                    string insertStmt = "update tb_partno_otherdtls set othdt_prod_cost_onetm='" + partEntity.othdt_prod_cost_onetm
                                                            + "' ,othdt_prod_cost_run='" + partEntity.othdt_prod_cost_run
                                                            + "' ,othdt_dispatch_onetm='" + partEntity.othdt_dispatch_onetm
                                                             + "' ,othdt_dispatch_run='" + partEntity.othdt_dispatch_run
                                                             + "' ,othdt_req_startdt='" + Convert.ToDateTime(partEntity.othdt_req_startdt)
                                                             + "' ,othdt_req_enddt='" + Convert.ToDateTime(partEntity.othdt_req_enddt)
                                                              + "' ,othdt_approved='" + 'I'
                                                              + "' ,othdt_active='" + '1'
                                                             + "',othdt_createdate='" + DateTime.Now
                                                               + "',othdt_createby='" + "Initial Upload"
                                                               + "',othdt_approveby='" + "Initial Upload"
                                                            + "' where othdt_request_type = '" + partEntity.othdt_request_type

                                                            + "' and othdt_request_id ='" + partEntity.othdt_request_id + "'";

                    SqlCommand cmd = new SqlCommand(insertStmt, con);

                    cmd.ExecuteNonQuery();

                    if (partEntity.othdt_req_enddt_current_sku != null)
                    {
                        string updateStmt = "update tb_partno_otherdtls set othdt_req_enddt='" + partEntity.othdt_req_enddt_current_sku
                                                                + "' where othdt_request_id =(select [othdt_ref_request_id] from [dbo].[tb_partno_otherdtls] where [othdt_request_id]="
                                                                + partEntity.othdt_request_id + ")";
                        SqlCommand cmd1 = new SqlCommand(updateStmt, con);
                        cmd1.ExecuteNonQuery();



                        string updatepartnumdate = "UPDATE [tb_partnum_master] SET pn_end_date='" + partEntity.othdt_req_enddt_current_sku
                            + "'where concat(pn_request_type, pn_request_id)= (select concat(pr_ref_request_type, pr_ref_request_id)from tb_production_sku where pr_request_id =" +
                            partEntity.othdt_request_id + ")";
                        SqlCommand cmd2= new SqlCommand(updatepartnumdate, con);
                        cmd2.ExecuteNonQuery();
                    }
                }
                else

                {

                    string insertStmt = "INSERT INTO tb_partno_otherdtls(othdt_prod_cost_onetm ,othdt_prod_cost_run,othdt_dispatch_onetm,othdt_dispatch_run,othdt_req_startdt,othdt_req_enddt,othdt_request_type,othdt_request_id,othdt_approved,othdt_approveby,othdt_createdate,othdt_createby ,othdt_active,othdt_approvedate) " +
                                 "VALUES(@othdt_prod_cost_onetm ,@othdt_prod_cost_run,@othdt_dispatch_onetm,@othdt_dispatch_run,@othdt_req_startdt,@othdt_req_enddt,@othdt_request_type,@othdt_request_id,@othdt_approved,@othdt_approveby,@othdt_createdate,@othdt_createby ,@othdt_active,@othdt_approvedate)";
                    //string insertStmt = "update tb_dispatch_sku(dp_request_type ,dp_request_id,dp_stamp_type,dp_stamp_chart,dp_isApproved,dp_isActive,dp_createdate,dp_createby,dp_approvedate,dp_approveby) " +
                    //                  "VALUES(@dp_request_type, @dp_request_id,@dp_stamp_type,@dp_stamp_chart,@dp_isApproved,@dp_isActive,@dp_createdate,@dp_createby,@dp_approvedate,@dp_approveby)";

                    SqlCommand cmd = new SqlCommand(insertStmt, con);
                    cmd.Parameters.AddWithValue("@othdt_prod_cost_onetm", partEntity.othdt_prod_cost_onetm);
                    cmd.Parameters.AddWithValue("@othdt_prod_cost_run", partEntity.othdt_prod_cost_run);
                    cmd.Parameters.AddWithValue("@othdt_dispatch_onetm", partEntity.othdt_dispatch_onetm);
                    cmd.Parameters.AddWithValue("@othdt_dispatch_run", partEntity.othdt_dispatch_run);
                    // cmd.ExecuteScalar();@,@,@,@,@,@
                    cmd.Parameters.AddWithValue("@othdt_req_startdt", Convert.ToDateTime(partEntity.othdt_req_startdt));
                    cmd.Parameters.AddWithValue("@othdt_req_enddt", Convert.ToDateTime(partEntity.othdt_req_enddt));
                    cmd.Parameters.AddWithValue("@othdt_approveby", "Initial Upload");
                    cmd.Parameters.AddWithValue("@othdt_request_type", partEntity.othdt_request_type);
                    cmd.Parameters.AddWithValue("@othdt_request_id", partEntity.othdt_request_id);
                    cmd.Parameters.AddWithValue("@othdt_approvedate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@othdt_createdate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@othdt_createby", "Initial Upload");
                    cmd.Parameters.AddWithValue("@othdt_approved", "S");
                    cmd.Parameters.AddWithValue("@othdt_active", 1);
                    cmd.ExecuteNonQuery();
                }




            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public DataSet getInnerBoxDetails(string chartNum)
        {

            InnerBoxDetailsEntity Entity = new InnerBoxDetailsEntity();
            List<InnerBoxDetailsEntity> EntityList = new List<InnerBoxDetailsEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);

            con.Open();
            SqlCommand cmd = new SqlCommand("select ibd_box_recid, ibd_pkg_parm_name, ibd_pkg_parm_scrnvalue from tb_innerbox_details where  ibd_box_recid ='" + chartNum + "' ", con);

            try
            {

                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;

        }

        public string getChartNumForInnerLabel(string fileTypeCode, string FileSubType, string fileSize, string brandId, string custId, string country, string fileCutType)
        {
            string chartNu;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select ilb_chart_num from tb_innerlabel_master where ilb_ftype_code ='"
                            + fileTypeCode + "' and  ilb_fstype_code='"
                            + FileSubType + "' and ilb_fsize_code ='"
                            + fileSize + "' and  ilb_brand_id ='"
                            + brandId + "'and  ilb_cust_id ='"
                            + custId + "' and ibl_shiptocountry_cd='"
                            + country + "'and ilb_fcut_code='"
                            + fileCutType + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            if (DR1.HasRows)
            {
                chartNu = DR1.GetValue(0).ToString();
            }
            else
                chartNu = "No Label found";




            //int dbFields = DR1.FieldCount;
            //if (DR1.HasRows)
            //{
            //    chartNum = DR1.GetValue(0).ToString();
            //}
            //else
            //{
            //    DR1.Close();
            //     string query = "select ilb_chart_num from tb_innerlabel_master where ilb_ftype_code ='"
            //+fileTypeCode + "' and  ilb_fstype_code='"

            //+ fileSize + "' and  ilb_brand_id ='"
            //+ brandId + "'and  ilb_cust_id ='"
            //+ custId + "' and ibl_shiptocountry_cd='"
            //+ country + "'and ilb_fcut_code='"
            //+ fileCutType + "'";
            //SqlCommand com = new SqlCommand(query, con);
            //     DR1 = com1.ExecuteReader();
            //    DR1.Read();
            //    if (DR1.HasRows)
            //    {
            //        chartNum = DR1.GetValue(0).ToString();
            //    }
            //    else
            //    chartNum = "No Chart Is Present!";
            //}








            con.Close();
            return chartNu;
        }

        public DataSet getInnerLabelDetails(string chartNum)
        {
            InnerLabelDetailsEntity Entity = new InnerLabelDetailsEntity();
            List<InnerLabelDetailsEntity> DetailsEntityList = new List<InnerLabelDetailsEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            //con.Open();
            //SqlCommand cmd = new SqlCommand("sp_get_handledetails", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            SqlCommand cmd = new SqlCommand("select  ilbd_parm_name, ilbd_parm_scrnvalue from tb_innerlabel_details where ilbd_chart_num ='" + chartNum + "'", con);
            cmd.ExecuteNonQuery();
            try
            {

                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public int getOuterBoxId(string chartNum)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select ob_box_id from tb_outerbox_master where ob_box_chartnum='" + chartNum + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            int wrapperId = (int)DR1.GetValue(0);
            con.Close();
            return wrapperId;
        }

        public DataSet getOuterDetials(int outerBoxId)
        {
            OuterDetailsEntity Entity = new OuterDetailsEntity();
            List<OuterDetailsEntity> DetailsEntityList = new List<OuterDetailsEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("select obd_box_id, obd_parm_name, obd_parm_scrnvalue from tb_outerbox_details where obd_box_id ='" + outerBoxId + "'", con);
            cmd.ExecuteNonQuery();
            try
            {
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public RawMaterialMastrEntity getRawMaterialDetails(string fileTypeCode, string FileSubType, string fileSize)
        {
            RawMaterialMastrEntity obj = new RawMaterialMastrEntity();
            RawMaterialMastrEntity temp = new RawMaterialMastrEntity();
            temp = getRawMaterialTbDetils(fileTypeCode, FileSubType, fileSize);
            return temp;
        }

        public RawMaterialMastrEntity getRawMaterialTbDetils(string fileTypeCode, string FileSubType, string fileSize)
        {

            RawMaterialMastrEntity entityObj = new RawMaterialMastrEntity();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            //con.Open();

            con.Open();
            SqlCommand cmd = new SqlCommand("select rm_code,rm_wtinkg_perthsnd,rm_netwtingm_persku,rm_height,rm_width from tb_raw_material_master where rm_fsize_id ='" + fileSize + "' and rm_ftype_id ='" + fileTypeCode + "' and rm_fstype_id='" + FileSubType + "'", con);
            cmd.ExecuteNonQuery();
            try
            {

                using (SqlDataReader DR1 = cmd.ExecuteReader())
                {
                    while (DR1.Read())
                    {

                        entityObj.rm_code = (string)DR1["rm_code"];
                        entityObj.rm_netwtingm_persku = (Decimal)DR1["rm_netwtingm_persku"];
                        entityObj.rm_wtinkg_perthsnd = (Decimal)DR1["rm_wtinkg_perthsnd"];
                        entityObj.rm_height = (Decimal)DR1["rm_height"];
                        entityObj.rm_width = (Decimal)DR1["rm_width"];
                    }

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            //finally
            //{
            //    cmd.Dispose();
            //    con.Close();
            //    con.Dispose();
            //}


            return entityObj;

        }

        public void savePackingDetailsToDispatchSKU(DispatchSKUEntity dispatchObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {

                string insertStmt = "update tb_dispatch_sku set dp_barcode_flg ='" + dispatchObj.dp_barcode_flg
                                   + "',dp_barcodeouter_flg='" + dispatchObj.dp_barcodeouter_flg
                                   + "',dp_lineno_flg ='" + dispatchObj.dp_lineno_flg + "', dp_hologram_flg='" + dispatchObj.dp_hologram_flg
                                   + "',dp_india_flg='" + dispatchObj.dp_india_flg + "',dp_fumigation_flg='" + dispatchObj.dp_fumigation_flg
                                   + "',dp_polybag_flg='" + dispatchObj.dp_polybag_flg + "',dp_prcstkr_flg='" + dispatchObj.dp_prcstkr_flg
                                   + "',dp_qltyseal_flg='" + dispatchObj.dp_qltyseal_flg + "' , dp_silicagel_flg='" + dispatchObj.dp_silicagel_flg
                                   + "',dp_promo_item_flg='" + dispatchObj.dp_promo_flg + "',dp_strap='" + dispatchObj.dp_strap_flg
                                   + "',dp_cellotape_flg='" + dispatchObj.dp_cellotape_flg + "' , dp_shrinkwrap_flg='" + dispatchObj.dp_shrinkwrap_flg
                                   + "',dp_decicant_flg='" + dispatchObj.dp_decicant_flg + "' , dp_paperwool_flg='" + dispatchObj.dp_paperwool_flg
                                   + "',dp_outer_mrking_type='" + dispatchObj.dp_outer_mrking_type + "' , dp_outer_mrking_img='" + dispatchObj.dp_outer_mrking_img
                                   + "',dp_pallet_mrking_type='" + dispatchObj.dp_pallet_mrking_type + "' , dp_pallet_mrking_img='" + dispatchObj.dp_pallet_mrking_img
                                   + "',dp_blko_flg='" + dispatchObj.dp_blko_flg
                                   + "',dp_pono_flg='" + dispatchObj.dp_pono_flg
                                   + "',dp_tang_color='" + dispatchObj.dp_tangcolor
                                   + "',dp_pkg_remark='" + dispatchObj.dp_pkg_remarks
                                   + "' where dp_request_type = '" + dispatchObj.dp_request_type
                                   + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";


                //string insertStmt = "update tb_dispatch_sku set dp_barcode_flg ='" + dispatchObj.dp_barcode_flg
                //                    + "',dp_lineno_flg ='" + dispatchObj.dp_lineno_flg + "', dp_hologram_flg='" + dispatchObj.dp_hologram_flg
                //                    + "',dp_india_flg='" + dispatchObj.dp_india_flg + "',dp_fumigation_flg='" + dispatchObj.dp_fumigation_flg
                //                    + "',dp_polybag_flg='" + dispatchObj.dp_polybag_flg + "',dp_prcstkr_flg='" + dispatchObj.dp_prcstkr_flg
                //                    + "',dp_qltyseal_flg='" + dispatchObj.dp_qltyseal_flg + "' , dp_silicagel_flg='" + dispatchObj.dp_silicagel_flg
                //                    + "',dp_promo_item_flg='" + dispatchObj.dp_promo_flg + "',dp_strap='" + dispatchObj.dp_strap_flg
                //                    + "',dp_cellotape_flg='" + dispatchObj.dp_cellotape_flg + "' , dp_shrinkwrap_flg='" + dispatchObj.dp_shrinkwrap_flg
                //                    + "',dp_decicant_flg='" + dispatchObj.dp_decicant_flg + "' , dp_paperwool_flg='" + dispatchObj.dp_paperwool_flg
                //                    + "' where dp_request_type = '" + dispatchObj.dp_request_type
                //                    + "' and dp_request_id ='" + dispatchObj.dp_request_id + "'";

                SqlCommand cmd = new SqlCommand(insertStmt, con);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region NEW by Swapnil
        public DataSet gettangcolorddl()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_gettangcolor", con);
                cmd.CommandType = CommandType.StoredProcedure;
               
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }

        public DataTable checkpkgnewmstr(int id, string type, string fo)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_checkpkgnewmstr", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@flag", fo);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public string checkwrappertype(int id, string type, string fo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_checkwrappertype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", fo);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public string checkinnerboxtype(int id, string type, string fo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_checkinnerboxtype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", fo);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public string checkoutertype(int id, string type, string fo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_checkoutertype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", fo);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public string checkpallettype(int id, string type, string fo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_checkpallettype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", fo);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public string checknewmstr(int id, string type, string fo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_checknewmstr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", fo);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public string checknewmstrstamp(int id, string type, string fo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_checknewmstrstamp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", fo);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public DataTable checknewmstrsubtyenewsize(int id, string type, string fo)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getprofilenewChecknewsize", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@flag", type);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable checknewmstrsubtye(int id, string type, string fo)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getprofilenewCheck", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@flag", type);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet getprofileprocessCheck(int? id, string type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getprofileprocessCheck", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@type", type);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }
        public void SetReqWithdrow(int id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SetReqasWithdrow", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.ExecuteNonQuery();
        }
        public DataSet getinnerboxSinglenewfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_innerboxsingle_filedlist", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getinnerboxDoublefieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_innerboxDouble_filedlist", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public int CheckHandleTypeAvailability(string hm_handle_type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_CheckHandleTypeAvailability", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@hm_handle_type", hm_handle_type);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }
        public void InnerboxEditactive(InnerBoxDetailsEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_InnerboxMasteractive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ib_chartnum", model1.ib_chartnum);
            cmd.Parameters.AddWithValue("@ib_chartimg", model1.ib_chartimg);
            cmd.Parameters.AddWithValue("@ibd_request_id", model1.ibd_request_id);
            cmd.Parameters.AddWithValue("@ibd_request_type", model1.ibd_request_type);
            int i = cmd.ExecuteNonQuery();
        }

        public void EditInnerboxMaster(InnerBoxDetailsEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_InnerboxMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ib_chartnum", model1.ib_chartnum);
            cmd.Parameters.AddWithValue("@ib_chartimg", model1.ib_chartimg);
            cmd.Parameters.AddWithValue("@ibd_request_id", model1.ibd_request_id);
            cmd.Parameters.AddWithValue("@ibd_request_type", model1.ibd_request_type);
            int i = cmd.ExecuteNonQuery();
        }

        public void AddInnerLabelMasterEditandLinkActive(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_InnerlabelMasterandLinkActive", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@ilbd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@ilbd_request_id", model.ilbd_request_id);
            int i = cmd.ExecuteNonQuery();
        }
        public void AddInnerLabelMasterEditandLink(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_InnerlabelMasterandLink", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@ilbd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@ilbd_request_id", model.ilbd_request_id);
            int i = cmd.ExecuteNonQuery();
        }
        public void AddInnerLabelMasterandLink(InnerLabelDetailsEntity model, string fileTypeCode, string fileSubType, string fileSize, string fileCutType, string country, string brandId, string custId)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerlabelMasterandLink", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fileTypeCode", fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType", fileSubType);
            cmd.Parameters.AddWithValue("@fileSize", fileSize);
            cmd.Parameters.AddWithValue("@fileCutType", fileCutType);
            cmd.Parameters.AddWithValue("@country", country);
            cmd.Parameters.AddWithValue("@brandId", brandId);
            cmd.Parameters.AddWithValue("@custId", custId);
            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@ilbd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@ilbd_request_id", model.ilbd_request_id);
            int i = cmd.ExecuteNonQuery();
        }
        public void AddInnerboxMasterandLink(InnerBoxDetailsEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerboxMasterandLink", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fileTypeCode", fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType", FileSubType);
            cmd.Parameters.AddWithValue("@fileSize", fileSize);
            cmd.Parameters.AddWithValue("@fileCutType", fileCutType);
            cmd.Parameters.AddWithValue("@ib_chartnum", model1.ib_chartnum);
            cmd.Parameters.AddWithValue("@ib_chartimg", model1.ib_chartimg);
            cmd.Parameters.AddWithValue("@ibd_request_type", model1.ibd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model1.ibd_request_id);
            cmd.Parameters.AddWithValue("@ib_box_type", model1.ib_box_type);
            cmd.Parameters.AddWithValue("@ib_qty_inbox", model1.ib_qty_inbox);
            int i = cmd.ExecuteNonQuery();
        }
        public void AddCutspecsEditLink(int prc_request_id, string prc_request_type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Edit_link", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@prc_request_id  ", prc_request_id);
            cmd.Parameters.AddWithValue("@prc_request_type", prc_request_type);
            cmd.Parameters.AddWithValue("@EditLink", "CutStandard");
            int i = cmd.ExecuteNonQuery();
        }
        public int GetstampList(int i)
        {
            int ds = 0;
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_stamp_list", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", i);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            ds = Convert.ToInt16(cmd.ExecuteScalar());
            return ds;
        }


        public DataSet GetHistoryDetails(int pr_request_id, string pr_request_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_History_Details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_id", pr_request_id);
            cmd.Parameters.AddWithValue("@pr_request_type", pr_request_type);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }


        public DataSet getcutspeclistforEdit(int id, string type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_cutspeclistforEdit", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_id", id);
            cmd.Parameters.AddWithValue("@pr_request_type", type);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetinnerlabellistforEdit(int id, string type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_innerlabel_details_byREQid", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet getInnerBoxlistforEdit(int id, string type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_InnerBoxlistforEdit", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_id", id);
            cmd.Parameters.AddWithValue("@pr_request_type", type);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataSet GetApprovalDetails(int pr_request_id, string pr_request_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Approval_Details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_id", pr_request_id);
            cmd.Parameters.AddWithValue("@pr_request_type", pr_request_type);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }


        public DataSet GetinnerLabelList(string dp_innerlabel_chart, string pr_ftype_code, string pr_fstype_code, string pr_fsize_code, string pr_cust_id, string pr_brand_id, string pr_fcut_code, string pr_ship2country_id)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_innerLabel", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@dp_innerlabel_chart", dp_innerlabel_chart);
            cmd.Parameters.AddWithValue("@pr_ftype_code", pr_ftype_code);
            cmd.Parameters.AddWithValue("@pr_fstype_code", pr_fstype_code);
            cmd.Parameters.AddWithValue("@pr_fsize_code", pr_fsize_code);
            cmd.Parameters.AddWithValue("@pr_cust_id", pr_cust_id);
            cmd.Parameters.AddWithValue("@pr_brand_id", pr_brand_id);
            cmd.Parameters.AddWithValue("@pr_fcut_code", pr_fcut_code);
            cmd.Parameters.AddWithValue("@pr_ship2country_id", pr_ship2country_id);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetCostDetails(string partno)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_CostDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@reqId", partno);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetstampList(string dp_stamp_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_stamp_chart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_stamp_chart", dp_stamp_chart);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetPalletList(string dp_pallet_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_pallet_chart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_pallet_chart", dp_pallet_chart);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public string GetNewReqtype(string pr_uniq_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_type_productionsku", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }
        public int GetNewReq(string pr_uniq_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_id_productionsku", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }
        public DataSet GetOuterChartlist(string dp_outerbox_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Outerbox_chart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_outerbox_chart", dp_outerbox_chart);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet getinnerboxlist(string dp_innerbox_chart, string pr_fstype_code, string pr_ftype_code, string pr_fsize_code)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_Innerbox_chart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_innerbox_chart", dp_innerbox_chart);
            cmd.Parameters.AddWithValue("@pr_fstype_code", pr_fstype_code);
            cmd.Parameters.AddWithValue("@pr_ftype_code", pr_ftype_code);
            cmd.Parameters.AddWithValue("@pr_fsize_code", pr_fsize_code);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet getwrappinglist(string dp_wrapping_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_wrapping_chart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_wrapping_chart", dp_wrapping_chart);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet gethandleimgpath(string dp_handle_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_handleimgpath", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_handle_chart", dp_handle_chart);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet getcustspeclist(string reqId)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_Cutspeclist", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@Id", reqId);
                cmd.Parameters.Add(param);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public string getReqId(string Id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_ReqId_partno", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", Id);
            string i = "";
            if ((string)cmd.ExecuteScalar() != null)
            {
                i = (string)cmd.ExecuteScalar();
            }
            return i;
        }
        public DataTable getdispatchdetails(int reqId)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_ProductionDispatch", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@reqId", reqId);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        public DataSet getCustomerListbyid(int id)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_customer_master_details_byid", con);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@id", id);
                cmd.Parameters.Add(param);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet getNewMstrReqList(int reqid)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_NewMstrReq", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ma_request_id", reqid);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getCustomerfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get__fieldlist_Customer", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getBrandfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get__fieldlist_Brand", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getFilesubtypefieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_fileSubType", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getFiletypefieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get__fieldlist_fileType", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet GetRaw_MaterialfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_Raw_Material", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet GetStampMasterfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_StampMasterfieldlist", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getcutspecsfieldListonfileType(string filetype)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_cutspecsonfileType", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@filetype", filetype);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getcutspecsfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get__fieldlist_cutspecs", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getWrappingchartList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_Wrappingchart", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getinnerlableList(string c)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_innerlable", con);
                cmd.Parameters.AddWithValue("@flag", c);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet _wrappingtypeMasterAdd()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_Wrappingtype", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getinnerboxTypeList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_innerboxtype", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getPalletChartList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_Palletchart", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getPalletTypeList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_PalletType", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getOuterchartList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_OuterChart", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getOuterTypeList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_OuterType", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getHandlesubtypefieldList(string selectedhandle_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_HandleSubtype", con);
                cmd.Parameters.AddWithValue("@type", selectedhandle_type);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getHandlechartfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_Handlechart", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getHandlefieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_Handle", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getFileSizefieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get__fieldlist_fileSize", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getWrappingChartfieldList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fieldlist_wrapper", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public void newfiletypesaveImg(short ft_request_id, string ft_request_type, string fileName)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_Filetypeimg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ft_request_id", ft_request_id);
            cmd.Parameters.AddWithValue("@ft_request_type", ft_request_type);
            cmd.Parameters.AddWithValue("@fileName", fileName);
            int i = cmd.ExecuteNonQuery();
        }

        public void UpdateInnerBoxApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerBoxwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fs_request_id", id);
            cmd.Parameters.AddWithValue("@fs_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        
       public void RejectApprove(int? id, string type,string Flag)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_Rejectapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@flag", Flag);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdatesizeApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_FileSizewithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fs_request_id", id);
            cmd.Parameters.AddWithValue("@fs_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdatefiletypeApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);

            SqlCommand cmd = new SqlCommand("sp_Insert_filetypewithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ft_request_id", id);
            cmd.Parameters.AddWithValue("@ft_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }

        public void UpdateRawMaterialApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);

            SqlCommand cmd = new SqlCommand("sp_Insert_RawMaterialApprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rm_request_id", id);
            cmd.Parameters.AddWithValue("@rm_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdateStampApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);

            SqlCommand cmd = new SqlCommand("sp_Insert_Stampwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@sm_request_id", id);
            cmd.Parameters.AddWithValue("@sm_request_type", type);
            cmd.Parameters.AddWithValue("@status", "Y");
            int i = cmd.ExecuteNonQuery();
        }


        public void UpdateStampApprove(int? id, string type, string status, string remark)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);

            SqlCommand cmd = new SqlCommand("sp_Insert_Stampwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@sm_request_id", id);
            cmd.Parameters.AddWithValue("@sm_request_type", type);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@remark", remark);
            int i = cmd.ExecuteNonQuery();
        }


        public void UpdateInnerLabelApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerLabelwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ilb_request_id", id);
            cmd.Parameters.AddWithValue("@ilb_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }

        public void UpdateOuterApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);
            SqlCommand cmd = new SqlCommand("sp_Insert_Outerwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ob_request_id", id);
            cmd.Parameters.AddWithValue("@ob_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdatePalletApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);
            SqlCommand cmd = new SqlCommand("sp_Insert_Palletwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_request_id", id);
            cmd.Parameters.AddWithValue("@pm_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }

        public void UpdateHandleApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);
            SqlCommand cmd = new SqlCommand("sp_Insert_handlewithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@hm_request_id", id);
            cmd.Parameters.AddWithValue("@hm_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdateWrapperApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);
            SqlCommand cmd = new SqlCommand("sp_Insert_Wrapperwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@wm_request_id", id);
            cmd.Parameters.AddWithValue("@wm_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdatefileSubtypeApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);

            SqlCommand cmd = new SqlCommand("sp_Insert_fileSubtypewithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_request_id", id);
            cmd.Parameters.AddWithValue("@pm_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void UpdatecutspecApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_custspecwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@tpi_request_id", id);
            cmd.Parameters.AddWithValue("@tpi_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void BrandsaveApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Brandwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bm_request_id", id);
            cmd.Parameters.AddWithValue("@bm_request_type", type);
            int i = cmd.ExecuteNonQuery();
        }
        public void CustomersaveApprove(int? id, string type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            //SqlCommand cmd = new SqlCommand("insert into TryCust (cm_cust_id,cm_cust_name,cm_cust_isApproved,cm_cust_isActive) values('" + custObj.Id + "','" + custObj.Name + "','N',0);", con);

            SqlCommand cmd = new SqlCommand("sp_Insert_Customerwithapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cm_request_id", id);
            cmd.Parameters.AddWithValue("@cm_request_type", type);

            int i = cmd.ExecuteNonQuery();

        }
        public DataSet FileSizeListNoApprove()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_FileSize_master_NoApprove", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet FiletypeListNoApprove()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_filetype_master_NoApprove", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet fileTypeListinactive()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_filetype_master_details_noactive", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataTable getFileSubTypeMasters(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fileSubtype", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        public DataSet FileSubtypeListNoApprove(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fileSubtype", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet fileSubTypeListinactive(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_fileSubtype", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet brandNoApproveList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_Brand_master_NoApprove", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet FileSizeListinactive()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_filesize_master_details_noactive", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }


        public DataSet getCutSpecMastersearch(string fsize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
                cmd.Parameters.AddWithValue("@Flag", "fulllistsearch");
                cmd.Parameters.AddWithValue("@fsize", fsize);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;

        }
        public DataSet getCutSpecMaster()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
                cmd.Parameters.AddWithValue("@flag", "fulllist");
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;

        }

        public DataSet getCutSpecMastersearch1(string fsize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
                cmd.Parameters.AddWithValue("@Flag", "fulllistsearch");
                cmd.Parameters.AddWithValue("@fsize", fsize);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;

        }
        public DataSet getCutSpecNoActiveMaster()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_CutSpec_masternoActive", con);
                cmd.Parameters.AddWithValue("@Flag", "NoActive");
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet getCutSpecNoApproveMaster()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
                cmd.Parameters.AddWithValue("@flag", "NoApprove");
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet brandListNoActiveList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_brand_master_details_noactive", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet getCustListNoApprove()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_customer_master_NoApprove", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public int newfileSubtypesave(FileSubTypeEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_FileSubType", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@fieldsubtypename             ",model1.fieldsubtypename);
            //cmd.Parameters.AddWithValue("@fieldsubtypecode             ",model1.fieldsubtypecode);
            //cmd.Parameters.AddWithValue("@fieldrmcode                  ",model1.fieldrmcode                  );
            //cmd.Parameters.AddWithValue("@fieldwtinkgthousand          ",model1.fieldwtinkgthousand          );
            //cmd.Parameters.AddWithValue("@fielddescription             ",model1.fielddescription             );
            //cmd.Parameters.AddWithValue("@fieldgmssku                  ",model1.fieldgmssku                  );
            //cmd.Parameters.AddWithValue("@fieldRemarks                 ",model1.fieldRemarks                  );

            cmd.Parameters.AddWithValue("@fieldwidth                   ", model1.fieldwidth);
            cmd.Parameters.AddWithValue("@fieldthikness                ", model1.fieldthikness);
            cmd.Parameters.AddWithValue("@fieldmood_length             ", model1.fieldmood_length);
            cmd.Parameters.AddWithValue("@fieldlengthmm                ", model1.fieldlengthmm);
            cmd.Parameters.AddWithValue("@fieldlengthP1mm              ", model1.fieldlengthP1mm);
            cmd.Parameters.AddWithValue("@fieldwidthatShoulder         ", model1.fieldwidthatShoulder);
            cmd.Parameters.AddWithValue("@fieldthiknessatshoulder      ", model1.fieldthiknessatshoulder);
            cmd.Parameters.AddWithValue("@fieldwidthattip              ", model1.fieldwidthattip);
            cmd.Parameters.AddWithValue("@fieldthiknessattip           ", model1.fieldthiknessattip);
            cmd.Parameters.AddWithValue("@fieldpointsizeatexactlength  ", model1.fieldpointsizeatexactlength);
            cmd.Parameters.AddWithValue("@fieldbodydimentionwidth      ", model1.fieldbodydimentionwidth);
            cmd.Parameters.AddWithValue("@fieldbodydimentionthikness   ", model1.fieldbodydimentionthikness);
            cmd.Parameters.AddWithValue("@fieldbodypointsize           ", model1.fieldbodypointsize);
            cmd.Parameters.AddWithValue("@fieldtapperlengthmm          ", model1.fieldtapperlengthmm);
            cmd.Parameters.AddWithValue("@fieldlengthaftercuttingbefore", model1.fieldlengthaftercuttingbefore);
            cmd.Parameters.AddWithValue("@fieldatshoulder              ", model1.fieldatshoulder);
            cmd.Parameters.AddWithValue("@fieldonedge                  ", model1.fieldonedge);
            cmd.Parameters.AddWithValue("@fieldupcut                   ", model1.fieldupcut);
            cmd.Parameters.AddWithValue("@fieldovercut                 ", model1.fieldovercut);
            cmd.Parameters.AddWithValue("@fieldedgecut                 ", model1.fieldedgecut);
            cmd.Parameters.AddWithValue("@fieldhardness                ", model1.fieldhardness);
            cmd.Parameters.AddWithValue("@fieldangel                   ", model1.fieldangel);
            cmd.Parameters.AddWithValue("@fieldradious                 ", model1.fieldradious);
            cmd.Parameters.AddWithValue("@subtypename                  ", model1.subtypename);
            cmd.Parameters.AddWithValue("@subtypecode                  ", model1.subtypecode);
            cmd.Parameters.AddWithValue("@rmcode                       ", model1.rmcode);
            cmd.Parameters.AddWithValue("@wtinkgthousand               ", model1.wtinkgthousand);
            cmd.Parameters.AddWithValue("@description                  ", model1.description);
            cmd.Parameters.AddWithValue("@width                        ", model1.width);
            cmd.Parameters.AddWithValue("@thikness                     ", model1.thikness);
            cmd.Parameters.AddWithValue("@gmssku                       ", model1.gmssku);
            cmd.Parameters.AddWithValue("@mood_length                  ", model1.mood_length);
            cmd.Parameters.AddWithValue("@lengthmm                     ", model1.lengthmm);
            cmd.Parameters.AddWithValue("@lengthP1mm                   ", model1.lengthP1mm);
            cmd.Parameters.AddWithValue("@widthatShoulder              ", model1.widthatShoulder);
            cmd.Parameters.AddWithValue("@thiknessatshoulder           ", model1.thiknessatshoulder);
            cmd.Parameters.AddWithValue("@widthattip                   ", model1.widthattip);
            cmd.Parameters.AddWithValue("@thiknessattip                ", model1.thiknessattip);
            cmd.Parameters.AddWithValue("@pointsizeatexactlength       ", model1.pointsizeatexactlength);
            cmd.Parameters.AddWithValue("@bodydimentionwidth           ", model1.bodydimentionwidth);
            cmd.Parameters.AddWithValue("@bodydimentionthikness        ", model1.bodydimentionthikness);
            cmd.Parameters.AddWithValue("@bodypointsize                ", model1.bodypointsize);
            cmd.Parameters.AddWithValue("@tapperlengthmm               ", model1.tapperlengthmm);
            cmd.Parameters.AddWithValue("@lengthaftercuttingbefore     ", model1.lengthaftercuttingbefore);
            cmd.Parameters.AddWithValue("@atshoulder                   ", model1.atshoulder);
            cmd.Parameters.AddWithValue("@onedge                       ", model1.onedge);
            cmd.Parameters.AddWithValue("@upcut                        ", model1.upcut);
            cmd.Parameters.AddWithValue("@overcut                      ", model1.overcut);
            cmd.Parameters.AddWithValue("@edgecut                      ", model1.edgecut);
            cmd.Parameters.AddWithValue("@hardness                     ", model1.hardness);
            cmd.Parameters.AddWithValue("@angel                        ", model1.angel);
            cmd.Parameters.AddWithValue("@radious                      ", model1.radious);
            cmd.Parameters.AddWithValue("@Remarks                      ", model1.Remarks);
            cmd.Parameters.AddWithValue("@rmcoderange                  ", model1.rmcoderange);
            cmd.Parameters.AddWithValue("@wtinkgthousandrange          ", model1.wtinkgthousandrange);
            cmd.Parameters.AddWithValue("@descriptionrange             ", model1.descriptionrange);
            cmd.Parameters.AddWithValue("@widthrange                   ", model1.widthrange);
            cmd.Parameters.AddWithValue("@thiknessrange                ", model1.thiknessrange);
            cmd.Parameters.AddWithValue("@gmsskurange                  ", model1.gmsskurange);
            cmd.Parameters.AddWithValue("@mood_lengthrange             ", model1.mood_lengthrange);
            cmd.Parameters.AddWithValue("@lengthmmrange                ", model1.lengthmmrange);
            cmd.Parameters.AddWithValue("@lengthP1mmrange              ", model1.lengthP1mmrange);
            cmd.Parameters.AddWithValue("@widthatShoulderrange         ", model1.widthatShoulderrange);
            cmd.Parameters.AddWithValue("@thiknessatshoulderrange      ", model1.thiknessatshoulderrange);
            cmd.Parameters.AddWithValue("@widthattiprange              ", model1.widthattiprange);
            cmd.Parameters.AddWithValue("@thiknessattiprange           ", model1.thiknessattiprange);
            cmd.Parameters.AddWithValue("@pointsizeatexactlengthrange  ", model1.pointsizeatexactlengthrange);
            cmd.Parameters.AddWithValue("@bodydimentionwidthrange      ", model1.bodydimentionwidthrange);
            cmd.Parameters.AddWithValue("@bodydimentionthiknessrange   ", model1.bodydimentionthiknessrange);
            cmd.Parameters.AddWithValue("@bodypointsizerange           ", model1.bodypointsizerange);
            cmd.Parameters.AddWithValue("@tapperlengthmmrange          ", model1.tapperlengthmmrange);
            cmd.Parameters.AddWithValue("@lengthaftercuttingbeforerange", model1.lengthaftercuttingbeforerange);
            cmd.Parameters.AddWithValue("@atshoulderrange              ", model1.atshoulderrange);
            cmd.Parameters.AddWithValue("@onedgerange                  ", model1.onedgerange);
            cmd.Parameters.AddWithValue("@upcutrange                   ", model1.upcutrange);
            cmd.Parameters.AddWithValue("@overcutrange                 ", model1.overcutrange);
            cmd.Parameters.AddWithValue("@edgecutrange                 ", model1.edgecutrange);
            cmd.Parameters.AddWithValue("@hardnessrange                ", model1.hardnessrange);
            cmd.Parameters.AddWithValue("@angelrange                   ", model1.angelrange);
            cmd.Parameters.AddWithValue("@radiousrange                 ", model1.radiousrange); ;
            cmd.Parameters.AddWithValue("@filesizecode                 ", model1.filesizecode);
            cmd.Parameters.AddWithValue("@filetypecode                 ", model1.filetypecode);
            cmd.Parameters.AddWithValue("@pm_dim_parm_verno            ", '0');
            cmd.Parameters.AddWithValue("@pm_file_code                 ", model1.filetypecode + model1.subtypecode);
            cmd.Parameters.AddWithValue("@pm_request_id                ", model1.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type              ", model1.pm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int newCustomersave(masterModule5 custObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Customer", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cm_cust_id", custObj.cm_cust_id);
            cmd.Parameters.AddWithValue("@cm_cust_name", custObj.cm_cust_name);
            cmd.Parameters.AddWithValue("@cm_cust_remarks", custObj.cm_cust_remarks);
            cmd.Parameters.AddWithValue("@cm_cust_verno", 0);
            cmd.Parameters.AddWithValue("@cm_request_id", custObj.cm_request_id);
            cmd.Parameters.AddWithValue("@cm_request_type", custObj.cm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int newWrapChartsave(WrapChartModelEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();

            SqlCommand cmd = new SqlCommand("sp_Insert_WrapChart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@color", model1.color);
            cmd.Parameters.AddWithValue("@colorphantomeshade1", model1.colorphantomeshade1);
            cmd.Parameters.AddWithValue("@colorphantomeshade2", model1.colorphantomeshade2);
            cmd.Parameters.AddWithValue("@colorphantomeshade3", model1.colorphantomeshade3);
            cmd.Parameters.AddWithValue("@wrapperNote", model1.wrapperNote);
            cmd.Parameters.AddWithValue("@wrapperwidth", model1.wrapperwidth);
            cmd.Parameters.AddWithValue("@wrapperlength", model1.wrapperlength);
            cmd.Parameters.AddWithValue("@UnitBePack", model1.UnitBePack);
            cmd.Parameters.AddWithValue("@wrappingThiknes", model1.wrappingThiknes);
            cmd.Parameters.AddWithValue("@printed", model1.printed);
            cmd.Parameters.AddWithValue("@Remarks", model1.Remarks);
            cmd.Parameters.AddWithValue("@image", model1.image);
            cmd.Parameters.AddWithValue("@chartnum", model1.chartnum);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        //public int CreateNewReq(string pr_uniq_id,string user_id)
        //{
        //    connectionstring = objUtils.getConnString();
        //    SqlConnection con = new SqlConnection(connectionstring);
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("sp_Insert_NewReq", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@pr_request_type", "N");
        //    cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
        //    cmd.Parameters.AddWithValue("@user_id", user_id);
        //    int i = cmd.ExecuteNonQuery();
        //    return i;
        //}
        public int CreateNewReqSet(string pr_uniq_id, string UserId)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_NewReq", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_type", "S");
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            cmd.Parameters.AddWithValue("@user_id", UserId);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int editCutspecssavebyrow(CutSpecsrowwiseEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_Cutspecssavebyrow", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cutStandardCode", model1.cutStandardCode);
            cmd.Parameters.AddWithValue("@cutStandardName", model1.cutStandardName);
            cmd.Parameters.AddWithValue("@prc_parameter_name   ", model1.prc_parameter_name);
            cmd.Parameters.AddWithValue("@prc_parameter_value  ", model1.prc_parameter_value);
            cmd.Parameters.AddWithValue("@pm_dim_parm_dwgvalue ", model1.pm_dim_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@pm_dim_parm_scrnvalue", model1.pm_dim_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@tpi_request_type", model1.prc_request_type);
            cmd.Parameters.AddWithValue("@tpi_request_id", model1.prc_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int editCutspecssavebyrowActive(CutSpecsrowwiseEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_CutspecssavebyrowActive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cutStandardCode", model1.cutStandardCode);
            cmd.Parameters.AddWithValue("@cutStandardName", model1.cutStandardName);
            cmd.Parameters.AddWithValue("@prc_parameter_name   ", model1.prc_parameter_name);
            cmd.Parameters.AddWithValue("@prc_parameter_value  ", model1.prc_parameter_value);
            cmd.Parameters.AddWithValue("@pm_dim_parm_dwgvalue ", model1.pm_dim_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@pm_dim_parm_scrnvalue", model1.pm_dim_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@tpi_request_type", model1.prc_request_type);
            cmd.Parameters.AddWithValue("@tpi_request_id", model1.prc_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int EditInnerboxSinglesavebyrowACTIVE(InnerBoxDetailsEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_InnerBoxbyrowACTIVE", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ib_chartnum", model1.ib_chartnum);
            cmd.Parameters.AddWithValue("@ib_chartimg", model1.ib_chartimg);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_name", model1.ibd_pkg_parm_name);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_code", model1.ibd_pkg_parm_code);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_dwgvalue ", model1.ibd_pkg_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_scrnvalue", model1.ibd_pkg_parm_scrnvalue);
            // cmd.Parameters.AddWithValue("@fcl_verno", 0);
            cmd.Parameters.AddWithValue("@ibd_request_type", model1.ibd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model1.ibd_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditInnerboxSinglesavebyrow(InnerBoxDetailsEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_InnerBoxbyrow", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ib_chartnum", model1.ib_chartnum);
            cmd.Parameters.AddWithValue("@ib_chartimg", model1.ib_chartimg);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_name", model1.ibd_pkg_parm_name);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_code", model1.ibd_pkg_parm_code);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_dwgvalue ", model1.ibd_pkg_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_scrnvalue", model1.ibd_pkg_parm_scrnvalue);
            // cmd.Parameters.AddWithValue("@fcl_verno", 0);
            cmd.Parameters.AddWithValue("@ibd_request_type", model1.ibd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model1.ibd_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int InnerlabelEditavebyrowActive(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Upload_Innerlabelbyrowactive", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@ilbd_parm_name", model.ilbd_parm_name);
            cmd.Parameters.AddWithValue("@ilbd_parm_scrnvalue", model.ilbd_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@ibd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model.ilbd_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int InnerlabelEditavebyrow(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Upload_Innerlabelbyrow", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@ilbd_parm_name", model.ilbd_parm_name);
            cmd.Parameters.AddWithValue("@ilbd_parm_scrnvalue", model.ilbd_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@ibd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model.ilbd_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int Innerlabelavebyrow(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Innerlabelbyrow", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@ilbd_parm_name", model.ilbd_parm_name);
            cmd.Parameters.AddWithValue("@ilbd_parm_scrnvalue", model.ilbd_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@ibd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model.ilbd_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int InnerboxSinglesavebyrow(InnerBoxDetailsEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerBoxbyrow", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fileTypeCode", fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType", FileSubType);
            cmd.Parameters.AddWithValue("@fileSize", fileSize);
            cmd.Parameters.AddWithValue("@fileCutType", fileCutType);
            cmd.Parameters.AddWithValue("@ib_chartnum", model1.ib_chartnum);
            cmd.Parameters.AddWithValue("@ib_chartimg", model1.ib_chartimg);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_name", model1.ibd_pkg_parm_name);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_code", model1.ibd_pkg_parm_code);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_dwgvalue ", model1.ibd_pkg_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@ibd_pkg_parm_scrnvalue", model1.ibd_pkg_parm_scrnvalue);
            // cmd.Parameters.AddWithValue("@fcl_verno", 0);
            cmd.Parameters.AddWithValue("@ibd_request_type", model1.ibd_request_type);
            cmd.Parameters.AddWithValue("@ibd_request_id", model1.ibd_request_id);
            //cmd.Parameters.AddWithValue("@ib_qty_inbox", model1.ib_qty_inbox);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int Cutspecssavebyrow(CutSpecsrowwiseEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Cutspecsbyrow", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fileTypeCode", fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType", FileSubType);
            cmd.Parameters.AddWithValue("@fileSize", fileSize);
            cmd.Parameters.AddWithValue("@fileCutType", fileCutType);
            cmd.Parameters.AddWithValue("@cutStandardCode", model1.cutStandardCode);
            cmd.Parameters.AddWithValue("@cutStandardName", model1.cutStandardName);
            cmd.Parameters.AddWithValue("@prc_parameter_name   ", model1.prc_parameter_name);
            cmd.Parameters.AddWithValue("@prc_parameter_value  ", model1.prc_parameter_value);
            cmd.Parameters.AddWithValue("@pm_dim_parm_dwgvalue ", model1.pm_dim_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@pm_dim_parm_scrnvalue", model1.pm_dim_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@fcl_verno", 0);
            cmd.Parameters.AddWithValue("@tpi_request_type", model1.prc_request_type);
            cmd.Parameters.AddWithValue("@tpi_request_id", model1.prc_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int Cutspecssave(CutSpecsEntity model1, string fileTypeCode, string FileSubType, string fileSize, string fileCutType)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Cutspecs", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fileTypeCode", fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType", FileSubType);
            cmd.Parameters.AddWithValue("@fileSize", fileSize);
            cmd.Parameters.AddWithValue("@fileCutType", fileCutType);
            cmd.Parameters.AddWithValue("@cutStandardCode", model1.cutStandardCode);
            cmd.Parameters.AddWithValue("@cutStandardName", model1.cutStandardName);
            cmd.Parameters.AddWithValue("@upcutrange", model1.upcutrange);
            cmd.Parameters.AddWithValue("@upcutvalue", model1.upcutvalue);
            cmd.Parameters.AddWithValue("@ovcutrange", model1.ovcutrange);
            cmd.Parameters.AddWithValue("@ovcutvalue", model1.ovcutvalue);
            cmd.Parameters.AddWithValue("@edcutrange", model1.edcutrange);
            cmd.Parameters.AddWithValue("@edcutvalue", model1.edcutvalue);
            cmd.Parameters.AddWithValue("@fcl_verno", model1.fcl_verno);
            cmd.Parameters.AddWithValue("@tpi_request_type", model1.prc_request_type);
            cmd.Parameters.AddWithValue("@tpi_request_id", model1.prc_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int newfilesizesaveave(FileSizeDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_filesize", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fs_size_inches", model.fs_size_inches);
            cmd.Parameters.AddWithValue("@fs_size_mm", model.fs_size_mm);
            cmd.Parameters.AddWithValue("@fs_size_remarks", model.fs_size_remarks);
            cmd.Parameters.AddWithValue("@fs_size_code", model.fs_size_code);
            cmd.Parameters.AddWithValue("@fs_size_verno", 0);
            cmd.Parameters.AddWithValue("@fs_request_id", model.fs_request_id);
            cmd.Parameters.AddWithValue("@fs_request_type", model.fs_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int newfiletypesaveave(FileTypeDataEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_filetype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ft_ftype_code", model.ft_ftype_code);
            cmd.Parameters.AddWithValue("@ft_ftype_desc", model.ft_ftype_desc);
            cmd.Parameters.AddWithValue("@ft_ftype_remarks", model.ft_ftype_remarks);
            cmd.Parameters.AddWithValue("@ft_ftype_verno", 0);
            cmd.Parameters.AddWithValue("@ft_request_type", model.ft_request_type);
            cmd.Parameters.AddWithValue("@ft_request_id", model.ft_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int newBrandsave(BrandDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Brand", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bm_brand_id", model.bm_brand_id);
            cmd.Parameters.AddWithValue("@bm_brand_name", model.bm_brand_name);
            cmd.Parameters.AddWithValue("@bm_brand_remarks", model.bm_brand_remarks);
            cmd.Parameters.AddWithValue("@cm_cust_verno", 0);
            cmd.Parameters.AddWithValue("@bm_request_id", model.bm_request_id);
            cmd.Parameters.AddWithValue("@bm_request_type", model.bm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int GetHandlestatusApprove(string handletype)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_CheckHandleApprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@handletype", handletype);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }
        public int GetHandlestatus(string handletype)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_CheckHandle", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@handletype", handletype);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }
        public int UpdateStampMaster(StampDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_stamp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@sd_chart_num", model.sd_chart_num);
            //cmd.Parameters.AddWithValue("@ImageName", model.ImageName);
            cmd.Parameters.AddWithValue("@stampsize", model.stampsize);
            cmd.Parameters.AddWithValue("@stamplocation", model.stamplocation);
            cmd.Parameters.AddWithValue("@Orientation", model.Orientation);
            cmd.Parameters.AddWithValue("@fontsize", model.fontsize);
            cmd.Parameters.AddWithValue("@noofsides", model.noofsides);
            cmd.Parameters.AddWithValue("@side1image", model.side1image);
            cmd.Parameters.AddWithValue("@side2image", model.side2image);
            cmd.Parameters.AddWithValue("@side3image", model.side3image);
            cmd.Parameters.AddWithValue("@side4image", model.side4image);
            cmd.Parameters.AddWithValue("@request_type", model.request_type);
            cmd.Parameters.AddWithValue("@request_id", model.request_id);
            cmd.Parameters.AddWithValue("@sm_cust_id", model.sm_cust_id);
            cmd.Parameters.AddWithValue("@sm_brand_id", model.sm_brand_id);
            cmd.Parameters.AddWithValue("@sm_ftype_id", model.sm_ftype_id);
            cmd.Parameters.AddWithValue("@sm_fstype_id", model.sm_fstype_id);
            cmd.Parameters.AddWithValue("@sm_fsize_id", model.sm_fsize_id);
            cmd.Parameters.AddWithValue("@sm_fcut_id", model.sm_fcut_id);
            cmd.Parameters.AddWithValue("@sm_stamp_mode", model.sm_stamp_mode);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int UpdateStampMasternoapprove(StampDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_stampnoapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@sd_chart_num", model.sd_chart_num);
            cmd.Parameters.AddWithValue("@ImageName", model.ImageName);
            cmd.Parameters.AddWithValue("@request_type", model.request_type);
            cmd.Parameters.AddWithValue("@request_id", model.request_id);
            cmd.Parameters.AddWithValue("@sm_cust_id", model.sm_cust_id);
            cmd.Parameters.AddWithValue("@sm_brand_id", model.sm_brand_id);
            cmd.Parameters.AddWithValue("@sm_ftype_id", model.sm_ftype_id);
            cmd.Parameters.AddWithValue("@sm_fstype_id", model.sm_fstype_id);
            cmd.Parameters.AddWithValue("@sm_fsize_id", model.sm_fsize_id);
            cmd.Parameters.AddWithValue("@sm_fcut_id", model.sm_fcut_id);
            cmd.Parameters.AddWithValue("@sm_stamp_mode", model.sm_stamp_mode);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int newStampMastersave(RawMaterialMastrEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            int i = 0;
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("sp_Insert_stamp", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@sd_chart_num", model.sd_chart_num);
                cmd.Parameters.AddWithValue("@ImageName", model.ImageName);
                cmd.Parameters.AddWithValue("@request_type", model.request_type);
                cmd.Parameters.AddWithValue("@request_id", model.request_id);
                cmd.Parameters.AddWithValue("@sm_cust_id", model.sm_cust_id);
                cmd.Parameters.AddWithValue("@sm_brand_id", model.sm_brand_id);
                cmd.Parameters.AddWithValue("@sm_ftype_id", model.sm_ftype_id);
                cmd.Parameters.AddWithValue("@sm_fstype_id", model.sm_fstype_id);
                cmd.Parameters.AddWithValue("@sm_fsize_id", model.sm_fsize_id);
                cmd.Parameters.AddWithValue("@sm_fcut_id", model.sm_fcut_id);
                cmd.Parameters.AddWithValue("@sm_stamp_mode", model.sm_stamp_mode);
                cmd.Parameters.AddWithValue("@stampsize", model.stampsize);
                cmd.Parameters.AddWithValue("@stamplocation", model.stamplocation);
                cmd.Parameters.AddWithValue("@Orientation", model.Orientation);
                cmd.Parameters.AddWithValue("@noofsides", model.noofsides);
                cmd.Parameters.AddWithValue("@side1image", model.side1image);
                cmd.Parameters.AddWithValue("@side2image", model.side2image);
                cmd.Parameters.AddWithValue("@side3image", model.side3image);
                cmd.Parameters.AddWithValue("@side4image", model.side4image);
                cmd.Parameters.AddWithValue("@fontsize", model.fontsize);

                i = cmd.ExecuteNonQuery();
            }catch(Exception ex)
            { throw ex; }
            return i;
        }


        public int NewRaw_MaterialSave(RawMaterialMastrEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_RawMaterial", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rm_code", model.rm_code);
            cmd.Parameters.AddWithValue("@rm_desc", model.rm_desc);
            cmd.Parameters.AddWithValue("@rm_material", model.rm_material);
            cmd.Parameters.AddWithValue("@rm_wtinkg_perthsnd", model.rm_wtinkg_perthsnd);
            cmd.Parameters.AddWithValue("@rm_netwtingm_persku", model.rm_netwtingm_persku);
            cmd.Parameters.AddWithValue("@rm_height", model.rm_height);
            cmd.Parameters.AddWithValue("@rm_width", model.rm_width);
            cmd.Parameters.AddWithValue("@rm_fsize_id", model.rm_fsize_id);
            cmd.Parameters.AddWithValue("@rm_ftype_id", model.rm_ftype_id);
            cmd.Parameters.AddWithValue("@rm_fstype_id", model.rm_fstype_id);
            cmd.Parameters.AddWithValue("@rm_request_type", model.rm_request_type);
            cmd.Parameters.AddWithValue("@rm_request_id", model.rm_request_id);
            cmd.Parameters.AddWithValue("@rm_remarks", model.rm_remarks);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int NewWrapperChartSave(WrapChartModelEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_WrapChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@chartnum", model.chartnum);
            cmd.Parameters.AddWithValue("@Remarks", model.Remarks);
            cmd.Parameters.AddWithValue("@color", model.color);
            cmd.Parameters.AddWithValue("@colorphantomeshade1", model.colorphantomeshade1);
            cmd.Parameters.AddWithValue("@colorphantomeshade2", model.colorphantomeshade2);
            cmd.Parameters.AddWithValue("@colorphantomeshade3", model.colorphantomeshade3);
            cmd.Parameters.AddWithValue("@wrapperNote", model.wrapperNote);
            cmd.Parameters.AddWithValue("@wrapperwidth", model.wrapperwidth);
            cmd.Parameters.AddWithValue("@wrapperlength", model.wrapperlength);
            cmd.Parameters.AddWithValue("@UnitBePack", model.UnitBePack);
            cmd.Parameters.AddWithValue("@wrappingThiknes", model.wrappingThiknes);
            cmd.Parameters.AddWithValue("@printed", model.printed);
            cmd.Parameters.AddWithValue("@colorrange", model.colorrange);
            cmd.Parameters.AddWithValue("@colorphantomeshade1range", model.colorphantomeshade1range);
            cmd.Parameters.AddWithValue("@colorphantomeshade2range", model.colorphantomeshade2range);
            cmd.Parameters.AddWithValue("@colorphantomeshade3range", model.colorphantomeshade3range);
            cmd.Parameters.AddWithValue("@wrapperNoterange", model.wrapperNoterange);
            cmd.Parameters.AddWithValue("@wrapperwidthrange", model.wrapperwidthrange);
            cmd.Parameters.AddWithValue("@wrapperlengthrange", model.wrapperlengthrange);
            cmd.Parameters.AddWithValue("@UnitBePackrange", model.UnitBePackrange);
            cmd.Parameters.AddWithValue("@wrappingThiknesrange", model.wrappingThiknesrange);
            cmd.Parameters.AddWithValue("@printedrange", model.printedrange);
            cmd.Parameters.AddWithValue("@wm_request_id", model.wm_request_id);
            cmd.Parameters.AddWithValue("@wm_request_type", model.wm_request_type);
            cmd.Parameters.AddWithValue("@wm_fsize_id", model.wm_fsize_id);
            cmd.Parameters.AddWithValue("@img", model.img);
            cmd.Parameters.AddWithValue("@wm_ftype_id", model.wm_ftype_id);
            cmd.Parameters.AddWithValue("@wm_fstype_id", model.wm_fstype_id);
            cmd.Parameters.AddWithValue("@wm_cust_id", model.wm_cust_id);
            cmd.Parameters.AddWithValue("@wm_brand_id", model.wm_brand_id);
            cmd.Parameters.AddWithValue("@wm_wrappertype", model.wm_wrappertype);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int EditPalletchartactive(PalletDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_PalletChartNoactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_pallet_chartimg              ", model.pm_pallet_chartimg);
            cmd.Parameters.AddWithValue("@pm_pallet_chartnum              ", model.pm_pallet_chartnum);
            cmd.Parameters.AddWithValue("@pm_pallet_type                  ", model.pm_pallet_type);
            cmd.Parameters.AddWithValue("@PALLETMATERIALRANGE             ", model.PALLETMATERIALRANGE);
            cmd.Parameters.AddWithValue("@PALLETDESCRIPTIONRANGE          ", model.PALLETDESCRIPTIONRANGE);
            cmd.Parameters.AddWithValue("@PALLETTYPERANGE                 ", model.PALLETTYPERANGE);
            cmd.Parameters.AddWithValue("@PALLETLENGTHRANGE               ", model.PALLETLENGTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETWIDTHRANGE                ", model.PALLETWIDTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHEIGHTRANGE               ", model.PALLETHEIGHTRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERRANGE            ", model.PALLETHDPECOVERRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERLENGTHRANGE       ", model.PALLETHDPECOVERLENGTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERWIDTHRANGE       ", model.PALLETHDPECOVERWIDTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERHEIGHTRANGE       ", model.PALLETHDPECOVERHEIGHTRANGE);
            cmd.Parameters.AddWithValue("@PALLETMATERIAL                  ", model.PALLETMATERIAL);
            cmd.Parameters.AddWithValue("@PALLETDESCRIPTION               ", model.PALLETDESCRIPTION);
            cmd.Parameters.AddWithValue("@PALLETTYPE", model.PALLETTYPE);
            cmd.Parameters.AddWithValue("@PALLETLENGTH                    ", model.PALLETLENGTH);
            cmd.Parameters.AddWithValue("@PALLETWIDTH                     ", model.PALLETWIDTH);
            cmd.Parameters.AddWithValue("@PALLETHEIGHT                    ", model.PALLETHEIGHT);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVER                 ", model.PALLETHDPECOVER);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERLENGTH           ", model.PALLETHDPECOVERLENGTH);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERWIDTH            ", model.PALLETHDPECOVERWIDTH);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERHEIGHT           ", model.PALLETHDPECOVERHEIGHT);
            cmd.Parameters.AddWithValue("@pm_request_id            ", model.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type           ", model.pm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditPalletchart(PalletDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_PalletChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_pallet_chartimg              ", model.pm_pallet_chartimg);
            cmd.Parameters.AddWithValue("@pm_pallet_chartnum              ", model.pm_pallet_chartnum);
            cmd.Parameters.AddWithValue("@pm_pallet_type                  ", model.pm_pallet_type);
            cmd.Parameters.AddWithValue("@PALLETMATERIALRANGE             ", model.PALLETMATERIALRANGE);
            cmd.Parameters.AddWithValue("@PALLETDESCRIPTIONRANGE          ", model.PALLETDESCRIPTIONRANGE);
            cmd.Parameters.AddWithValue("@PALLETTYPERANGE                 ", model.PALLETTYPERANGE);
            cmd.Parameters.AddWithValue("@PALLETLENGTHRANGE               ", model.PALLETLENGTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETWIDTHRANGE                ", model.PALLETWIDTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHEIGHTRANGE               ", model.PALLETHEIGHTRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERRANGE            ", model.PALLETHDPECOVERRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERLENGTHRANGE       ", model.PALLETHDPECOVERLENGTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERWIDTHRANGE       ", model.PALLETHDPECOVERWIDTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERHEIGHTRANGE       ", model.PALLETHDPECOVERHEIGHTRANGE);
            cmd.Parameters.AddWithValue("@PALLETMATERIAL                  ", model.PALLETMATERIAL);
            cmd.Parameters.AddWithValue("@PALLETDESCRIPTION               ", model.PALLETDESCRIPTION);
            cmd.Parameters.AddWithValue("@PALLETTYPE", model.PALLETTYPE);
            cmd.Parameters.AddWithValue("@PALLETLENGTH                    ", model.PALLETLENGTH);
            cmd.Parameters.AddWithValue("@PALLETWIDTH                     ", model.PALLETWIDTH);
            cmd.Parameters.AddWithValue("@PALLETHEIGHT                    ", model.PALLETHEIGHT);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVER                 ", model.PALLETHDPECOVER);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERLENGTH           ", model.PALLETHDPECOVERLENGTH);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERWIDTH            ", model.PALLETHDPECOVERWIDTH);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERHEIGHT           ", model.PALLETHDPECOVERHEIGHT);
            cmd.Parameters.AddWithValue("@pm_request_id            ", model.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type           ", model.pm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int PalletchartSave(PalletDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_PalletChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_pallet_chartimg              ", model.pm_pallet_chartimg);
            cmd.Parameters.AddWithValue("@pm_pallet_chartnum              ", model.pm_pallet_chartnum);
            cmd.Parameters.AddWithValue("@pm_pallet_type                  ", model.pm_pallet_type);
            cmd.Parameters.AddWithValue("@PALLETMATERIALRANGE             ", model.PALLETMATERIALRANGE);
            cmd.Parameters.AddWithValue("@PALLETDESCRIPTIONRANGE          ", model.PALLETDESCRIPTIONRANGE);
            cmd.Parameters.AddWithValue("@PALLETTYPERANGE                 ", model.PALLETTYPERANGE);
            cmd.Parameters.AddWithValue("@PALLETLENGTHRANGE               ", model.PALLETLENGTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETWIDTHRANGE                ", model.PALLETWIDTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHEIGHTRANGE               ", model.PALLETHEIGHTRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERRANGE            ", model.PALLETHDPECOVERRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERLENGTHRANGE       ", model.PALLETHDPECOVERLENGTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERWIDTHRANGE       ", model.PALLETHDPECOVERWIDTHRANGE);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERHEIGHTRANGE       ", model.PALLETHDPECOVERHEIGHTRANGE);
            cmd.Parameters.AddWithValue("@PALLETMATERIAL                  ", model.PALLETMATERIAL);
            cmd.Parameters.AddWithValue("@PALLETDESCRIPTION               ", model.PALLETDESCRIPTION);
            cmd.Parameters.AddWithValue("@PALLETTYPE", model.PALLETTYPE);
            cmd.Parameters.AddWithValue("@PALLETLENGTH                    ", model.PALLETLENGTH);
            cmd.Parameters.AddWithValue("@PALLETWIDTH                     ", model.PALLETWIDTH);
            cmd.Parameters.AddWithValue("@PALLETHEIGHT                    ", model.PALLETHEIGHT);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVER                 ", model.PALLETHDPECOVER);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERLENGTH           ", model.PALLETHDPECOVERLENGTH);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERWIDTH            ", model.PALLETHDPECOVERWIDTH);
            cmd.Parameters.AddWithValue("@PALLETHDPECOVERHEIGHT           ", model.PALLETHDPECOVERHEIGHT);
            cmd.Parameters.AddWithValue("@pm_request_id            ", model.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type           ", model.pm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int WrappertypeAdd(WrapperTypeEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_WrapperType", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@wrapper_type", model.wrapper_type);
            cmd.Parameters.AddWithValue("@remark", model.remark);
            cmd.Parameters.AddWithValue("@pkng_request_id", model.pkng_request_id);
            cmd.Parameters.AddWithValue("@pkng_request_type", model.pkng_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int innerlablenewSave(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerLabel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@INNERPACKINGTOPLABEL", model.INNERPACKINGTOPLABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOTTOMLABEL", model.INNERPACKINGBOTTOMLABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGSIDE1LABEL", model.INNERPACKINGSIDE1LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGSIDE2LABEL", model.INNERPACKINGSIDE2LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGEND1LABEL", model.INNERPACKINGEND1LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGEND2LABEL", model.INNERPACKINGEND2LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOXSAMPLE", model.INNERPACKINGBOXSAMPLE);
            cmd.Parameters.AddWithValue("@INNERPACKINGLABELLOCATIONCHARTID", model.INNERPACKINGLABELLOCATIONCHARTID);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOARDSUPPLIER", model.INNERPACKINGBOARDSUPPLIER);
            cmd.Parameters.AddWithValue("@ilbd_request_id", model.ilbd_request_id);
            cmd.Parameters.AddWithValue("@ilbd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@fs_size_code", model.fs_size_code);
            cmd.Parameters.AddWithValue("@ft_ftype_desc", model.ft_ftype_desc);
            cmd.Parameters.AddWithValue("@pm_fstype_desc", model.pm_fstype_desc);
            cmd.Parameters.AddWithValue("@bm_brand_name", model.bm_brand_name);
            cmd.Parameters.AddWithValue("@cm_cust_name", model.cm_cust_name);
            cmd.Parameters.AddWithValue("@fc_cut_desc", model.fc_cut_desc);
            cmd.Parameters.AddWithValue("@ShipToCountryCode", model.ShipToCountryCode);

            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int NewinnerboxtypeSave(InnerBoxTypeEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_InnerBoxType", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@innerboxtype", model.innerboxtype);
            cmd.Parameters.AddWithValue("@remark", model.remark);
            cmd.Parameters.AddWithValue("@pkng_request_id", model.pkng_request_id);
            cmd.Parameters.AddWithValue("@pkng_request_type", model.pkng_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int NewOutertypeSave(OutertypeEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Outertype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Outertype", model.Outertype);
            cmd.Parameters.AddWithValue("@remark", model.remark);
            cmd.Parameters.AddWithValue("@pkng_request_id", model.pkng_request_id);
            cmd.Parameters.AddWithValue("@pkng_request_type", model.pkng_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int NewOuterchartSave(OuterDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Outerchart", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@obd_boxdtl_recid             ", model.obd_boxdtl_recid);
            cmd.Parameters.AddWithValue("@OUTERPACKMATERIAL            ", model.OUTERPACKMATERIAL);
            cmd.Parameters.AddWithValue("@OUTERPACKDESCRIPTION         ", model.OUTERPACKDESCRIPTION);
            cmd.Parameters.AddWithValue("@OUTERPACKFINISH              ", model.OUTERPACKFINISH);
            cmd.Parameters.AddWithValue("@OUTERPACKAPPLICATION         ", model.OUTERPACKAPPLICATION);
            cmd.Parameters.AddWithValue("@OUTERPACKLENGTH              ", model.OUTERPACKLENGTH);
            cmd.Parameters.AddWithValue("@OUTERPACKWIDTH               ", model.OUTERPACKWIDTH);
            cmd.Parameters.AddWithValue("@OUTERPACKHEIGHT              ", model.OUTERPACKHEIGHT);
            cmd.Parameters.AddWithValue("@OUTERPACKBATTENPOSITION      ", model.OUTERPACKBATTENPOSITION);
            cmd.Parameters.AddWithValue("@OUTERPACKIMG                 ", model.OUTERPACKIMG);
            cmd.Parameters.AddWithValue("@OTHER1                       ", model.OTHER1);
            cmd.Parameters.AddWithValue("@OTHER2                       ", model.OTHER2);
            cmd.Parameters.AddWithValue("@OTHER3                       ", model.OTHER3);
            cmd.Parameters.AddWithValue("@OTHER4                       ", model.OTHER4);
            cmd.Parameters.AddWithValue("@OTHER5                       ", model.OTHER5);
            cmd.Parameters.AddWithValue("@OUTERPACKLENGTHrange         ", model.OUTERPACKLENGTHrange);
            cmd.Parameters.AddWithValue("@OUTERPACKWIDTHrange          ", model.OUTERPACKWIDTHrange);
            cmd.Parameters.AddWithValue("@OUTERPACKHEIGHTrange         ", model.OUTERPACKHEIGHTrange);
            cmd.Parameters.AddWithValue("@OUTERPACKBATTENPOSITIONrange ", model.OUTERPACKBATTENPOSITIONrange);
            cmd.Parameters.AddWithValue("@OTHER1range                  ", model.OTHER1range);
            cmd.Parameters.AddWithValue("@OTHER2range                  ", model.OTHER2range);
            cmd.Parameters.AddWithValue("@OTHER3range                  ", model.OTHER3range);
            cmd.Parameters.AddWithValue("@OTHER4range                  ", model.OTHER4range);
            cmd.Parameters.AddWithValue("@OTHER5range                  ", model.OTHER5range);
            cmd.Parameters.AddWithValue("@ob_request_id                  ", model.ob_request_id);
            cmd.Parameters.AddWithValue("@ob_request_type                ", model.ob_request_type);
            cmd.Parameters.AddWithValue("@ob_box_type", model.ob_box_type);
            cmd.Parameters.AddWithValue("@ob_box_fsize_code              ", model.ob_box_fsize_code);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int NewPallettypeSave(PallettypeEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Pallettype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Pallettype", model.Pallettype);
            cmd.Parameters.AddWithValue("@remark", model.remark);
            cmd.Parameters.AddWithValue("@pkng_request_id", model.pkng_request_id);
            cmd.Parameters.AddWithValue("@pkng_request_type", model.pkng_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int EditInnerLabelDetailsNoApprove(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UPdate_InnerLabelnoapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@INNERPACKINGTOPLABEL", model.INNERPACKINGTOPLABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOTTOMLABEL", model.INNERPACKINGBOTTOMLABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGSIDE1LABEL", model.INNERPACKINGSIDE1LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGSIDE2LABEL", model.INNERPACKINGSIDE2LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGEND1LABEL", model.INNERPACKINGEND1LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGEND2LABEL", model.INNERPACKINGEND2LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOXSAMPLE", model.INNERPACKINGBOXSAMPLE);
            cmd.Parameters.AddWithValue("@INNERPACKINGLABELLOCATIONCHARTID", model.INNERPACKINGLABELLOCATIONCHARTID);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOARDSUPPLIER", model.INNERPACKINGBOARDSUPPLIER);
            cmd.Parameters.AddWithValue("@ilbd_request_id", model.ilbd_request_id);
            cmd.Parameters.AddWithValue("@ilbd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@fs_size_code", model.fs_size_code);
            cmd.Parameters.AddWithValue("@ft_ftype_desc", model.ft_ftype_desc);
            cmd.Parameters.AddWithValue("@pm_fstype_desc", model.pm_fstype_desc);
            cmd.Parameters.AddWithValue("@bm_brand_name", model.bm_brand_name);
            cmd.Parameters.AddWithValue("@cm_cust_name", model.cm_cust_name);
            cmd.Parameters.AddWithValue("@fc_cut_desc", model.fc_cut_desc);
            cmd.Parameters.AddWithValue("@ShipToCountryCode", model.ShipToCountryCode);

            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditInnerLabelDetails(InnerLabelDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UPdate_InnerLabel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ilbd_chart_num", model.ilbd_chart_num);
            cmd.Parameters.AddWithValue("@INNERPACKINGTOPLABEL", model.INNERPACKINGTOPLABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOTTOMLABEL", model.INNERPACKINGBOTTOMLABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGSIDE1LABEL", model.INNERPACKINGSIDE1LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGSIDE2LABEL", model.INNERPACKINGSIDE2LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGEND1LABEL", model.INNERPACKINGEND1LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGEND2LABEL", model.INNERPACKINGEND2LABEL);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOXSAMPLE", model.INNERPACKINGBOXSAMPLE);
            cmd.Parameters.AddWithValue("@INNERPACKINGLABELLOCATIONCHARTID", model.INNERPACKINGLABELLOCATIONCHARTID);
            cmd.Parameters.AddWithValue("@INNERPACKINGBOARDSUPPLIER", model.INNERPACKINGBOARDSUPPLIER);
            cmd.Parameters.AddWithValue("@ilbd_request_id", model.ilbd_request_id);
            cmd.Parameters.AddWithValue("@ilbd_request_type", model.ilbd_request_type);
            cmd.Parameters.AddWithValue("@fs_size_code", model.fs_size_code);
            cmd.Parameters.AddWithValue("@ft_ftype_desc", model.ft_ftype_desc);
            cmd.Parameters.AddWithValue("@pm_fstype_desc", model.pm_fstype_desc);
            cmd.Parameters.AddWithValue("@bm_brand_name", model.bm_brand_name);
            cmd.Parameters.AddWithValue("@cm_cust_name", model.cm_cust_name);
            cmd.Parameters.AddWithValue("@fc_cut_desc", model.fc_cut_desc);
            cmd.Parameters.AddWithValue("@ShipToCountryCode", model.ShipToCountryCode);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int EditWrapChartDetailsnoapprove(WrapChartModelEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_WrapChartNoapprove", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@chartnum", model.chartnum);
            cmd.Parameters.AddWithValue("@Remarks", model.Remarks);
            cmd.Parameters.AddWithValue("@color", model.color);
            cmd.Parameters.AddWithValue("@colorphantomeshade1", model.colorphantomeshade1);
            cmd.Parameters.AddWithValue("@colorphantomeshade2", model.colorphantomeshade2);
            cmd.Parameters.AddWithValue("@colorphantomeshade3", model.colorphantomeshade3);
            cmd.Parameters.AddWithValue("@wrapperNote", model.wrapperNote);
            cmd.Parameters.AddWithValue("@wrapperwidth", model.wrapperwidth);
            cmd.Parameters.AddWithValue("@wrapperlength", model.wrapperlength);
            cmd.Parameters.AddWithValue("@UnitBePack", model.UnitBePack);
            cmd.Parameters.AddWithValue("@wrappingThiknes", model.wrappingThiknes);
            cmd.Parameters.AddWithValue("@printed", model.printed);
            cmd.Parameters.AddWithValue("@colorrange", model.colorrange);
            cmd.Parameters.AddWithValue("@colorphantomeshade1range", model.colorphantomeshade1range);
            cmd.Parameters.AddWithValue("@colorphantomeshade2range", model.colorphantomeshade2range);
            cmd.Parameters.AddWithValue("@colorphantomeshade3range", model.colorphantomeshade3range);
            cmd.Parameters.AddWithValue("@wrapperNoterange", model.wrapperNoterange);
            cmd.Parameters.AddWithValue("@wrapperwidthrange", model.wrapperwidthrange);
            cmd.Parameters.AddWithValue("@wrapperlengthrange", model.wrapperlengthrange);
            cmd.Parameters.AddWithValue("@UnitBePackrange", model.UnitBePackrange);
            cmd.Parameters.AddWithValue("@wrappingThiknesrange", model.wrappingThiknesrange);
            cmd.Parameters.AddWithValue("@printedrange", model.printedrange);
            cmd.Parameters.AddWithValue("@wm_request_id", model.wm_request_id);
            cmd.Parameters.AddWithValue("@wm_request_type", model.wm_request_type);
            cmd.Parameters.AddWithValue("@wm_fsize_id", model.wm_fsize_id);
            cmd.Parameters.AddWithValue("@img", model.img);
            cmd.Parameters.AddWithValue("@wm_ftype_id", model.wm_ftype_id);
            cmd.Parameters.AddWithValue("@wm_fstype_id", model.wm_fstype_id);
            cmd.Parameters.AddWithValue("@wm_cust_id", model.wm_cust_id);
            cmd.Parameters.AddWithValue("@wm_brand_id", model.wm_brand_id);
            cmd.Parameters.AddWithValue("@wm_wrappertype", model.wm_wrappertype);

            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditWrapChartDetails(WrapChartModelEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_WrapChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@chartnum", model.chartnum);
            cmd.Parameters.AddWithValue("@Remarks", model.Remarks);
            cmd.Parameters.AddWithValue("@color", model.color);
            cmd.Parameters.AddWithValue("@colorphantomeshade1", model.colorphantomeshade1);
            cmd.Parameters.AddWithValue("@colorphantomeshade2", model.colorphantomeshade2);
            cmd.Parameters.AddWithValue("@colorphantomeshade3", model.colorphantomeshade3);
            cmd.Parameters.AddWithValue("@wrapperNote", model.wrapperNote);
            cmd.Parameters.AddWithValue("@wrapperwidth", model.wrapperwidth);
            cmd.Parameters.AddWithValue("@wrapperlength", model.wrapperlength);
            cmd.Parameters.AddWithValue("@UnitBePack", model.UnitBePack);
            cmd.Parameters.AddWithValue("@wrappingThiknes", model.wrappingThiknes);
            cmd.Parameters.AddWithValue("@printed", model.printed);
            cmd.Parameters.AddWithValue("@colorrange", model.colorrange);
            cmd.Parameters.AddWithValue("@colorphantomeshade1range", model.colorphantomeshade1range);
            cmd.Parameters.AddWithValue("@colorphantomeshade2range", model.colorphantomeshade2range);
            cmd.Parameters.AddWithValue("@colorphantomeshade3range", model.colorphantomeshade3range);
            cmd.Parameters.AddWithValue("@wrapperNoterange", model.wrapperNoterange);
            cmd.Parameters.AddWithValue("@wrapperwidthrange", model.wrapperwidthrange);
            cmd.Parameters.AddWithValue("@wrapperlengthrange", model.wrapperlengthrange);
            cmd.Parameters.AddWithValue("@UnitBePackrange", model.UnitBePackrange);
            cmd.Parameters.AddWithValue("@wrappingThiknesrange", model.wrappingThiknesrange);
            cmd.Parameters.AddWithValue("@printedrange", model.printedrange);
            cmd.Parameters.AddWithValue("@wm_request_id", model.wm_request_id);
            cmd.Parameters.AddWithValue("@wm_request_type", model.wm_request_type);
            cmd.Parameters.AddWithValue("@wm_fsize_id", model.wm_fsize_id);
            cmd.Parameters.AddWithValue("@img", model.img);
            cmd.Parameters.AddWithValue("@wm_ftype_id", model.wm_ftype_id);
            cmd.Parameters.AddWithValue("@wm_fstype_id", model.wm_fstype_id);
            cmd.Parameters.AddWithValue("@wm_cust_id", model.wm_cust_id);
            cmd.Parameters.AddWithValue("@wm_brand_id", model.wm_brand_id);
            cmd.Parameters.AddWithValue("@wm_wrappertype", model.wm_wrappertype);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int EditOuterchartDetailsactive(OuterDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_OuterChartNoactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@obd_boxdtl_recid", model.obd_boxdtl_recid);
            cmd.Parameters.AddWithValue("@OUTERPACKMATERIAL", model.OUTERPACKMATERIAL);
            cmd.Parameters.AddWithValue("@OUTERPACKDESCRIPTION", model.OUTERPACKDESCRIPTION);
            cmd.Parameters.AddWithValue("@OUTERPACKFINISH", model.OUTERPACKFINISH);
            cmd.Parameters.AddWithValue("@OUTERPACKAPPLICATION", model.OUTERPACKAPPLICATION);
            cmd.Parameters.AddWithValue("@OUTERPACKLENGTH", model.OUTERPACKLENGTH);
            cmd.Parameters.AddWithValue("@OUTERPACKWIDTH", model.OUTERPACKWIDTH);
            cmd.Parameters.AddWithValue("@OUTERPACKHEIGHT", model.OUTERPACKHEIGHT);
            cmd.Parameters.AddWithValue("@OUTERPACKBATTENPOSITION", model.OUTERPACKBATTENPOSITION);
            cmd.Parameters.AddWithValue("@OUTERPACKIMG", model.OUTERPACKIMG);
            cmd.Parameters.AddWithValue("@OTHER1", model.OTHER1);
            cmd.Parameters.AddWithValue("@OTHER2", model.OTHER2);
            cmd.Parameters.AddWithValue("@OTHER3", model.OTHER3);
            cmd.Parameters.AddWithValue("@OTHER4", model.OTHER4);
            cmd.Parameters.AddWithValue("@OTHER5", model.OTHER5);
            cmd.Parameters.AddWithValue("@OUTERPACKLENGTHrange", model.OUTERPACKLENGTHrange);
            cmd.Parameters.AddWithValue("@OUTERPACKWIDTHrange", model.OUTERPACKWIDTHrange);
            cmd.Parameters.AddWithValue("@OUTERPACKHEIGHTrange", model.OUTERPACKHEIGHTrange);
            cmd.Parameters.AddWithValue("@OUTERPACKBATTENPOSITIONrange ", model.OUTERPACKBATTENPOSITIONrange);
            cmd.Parameters.AddWithValue("@OTHER1range", model.OTHER1range);
            cmd.Parameters.AddWithValue("@OTHER2range", model.OTHER2range);
            cmd.Parameters.AddWithValue("@OTHER3range", model.OTHER3range);
            cmd.Parameters.AddWithValue("@OTHER4range", model.OTHER4range);
            cmd.Parameters.AddWithValue("@OTHER5range", model.OTHER5range);
            cmd.Parameters.AddWithValue("@ob_request_id", model.ob_request_id);
            cmd.Parameters.AddWithValue("@ob_request_type", model.ob_request_type);
            cmd.Parameters.AddWithValue("@ob_box_type", model.ob_box_type);
            cmd.Parameters.AddWithValue("@ob_box_fsize_code", model.ob_box_fsize_code);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int AddHandletypeDetails(HandleDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Handletype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@hm_handlesub_type", model.hm_handlesub_type);
            cmd.Parameters.AddWithValue("@hm_handle_type  ", model.hm_handle_type);
            cmd.Parameters.AddWithValue("@remark          ", model.remark);
            cmd.Parameters.AddWithValue("@hm_request_type ", model.hm_request_type);
            cmd.Parameters.AddWithValue("@hm_request_id   ", model.hm_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int AddHandlechartDetails(HandleDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_HandleChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@hm_handlesub_type", model.hm_handlesub_type);
            cmd.Parameters.AddWithValue("@hm_handle_type  ", model.hm_handle_type);
            cmd.Parameters.AddWithValue("@tempChartNum    ", model.tempChartNum);
            cmd.Parameters.AddWithValue("@hm_chartimg_name", model.hm_chartimg_name);
            cmd.Parameters.AddWithValue("@remark          ", model.remark);
            cmd.Parameters.AddWithValue("@hm_request_type ", model.hm_request_type);
            cmd.Parameters.AddWithValue("@hm_request_id   ", model.hm_request_id);
            cmd.Parameters.AddWithValue("@fileTypeCode    ", model.fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType     ", model.FileSubType);
            cmd.Parameters.AddWithValue("@fileSize        ", model.fileSize);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int AddHandlechartandpkgmasterDetails(HandleDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_HandleChartNoWithPKGmstr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@hm_handlesub_type", model.hm_handlesub_type);
            cmd.Parameters.AddWithValue("@hm_handle_type  ", model.hm_handle_type);
            cmd.Parameters.AddWithValue("@tempChartNum    ", model.tempChartNum);
            cmd.Parameters.AddWithValue("@hm_chartimg_name", model.hm_chartimg_name);
            cmd.Parameters.AddWithValue("@remark          ", model.remark);
            cmd.Parameters.AddWithValue("@hm_request_type ", model.hm_request_type);
            cmd.Parameters.AddWithValue("@hm_request_id   ", model.hm_request_id);
            cmd.Parameters.AddWithValue("@fileTypeCode    ", model.fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType     ", model.FileSubType);
            cmd.Parameters.AddWithValue("@fileSize        ", model.fileSize);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int EditHandlechartandpkgmasterDetails(HandleDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_HandleChartNoWithPKGmstr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@hm_handlesub_type", model.hm_handlesub_type);
            cmd.Parameters.AddWithValue("@hm_handle_type  ", model.hm_handle_type);
            cmd.Parameters.AddWithValue("@tempChartNum    ", model.tempChartNum);
            cmd.Parameters.AddWithValue("@hm_chartimg_name", model.hm_chartimg_name);
            cmd.Parameters.AddWithValue("@remark          ", model.remark);
            cmd.Parameters.AddWithValue("@hm_request_type ", model.hm_request_type);
            cmd.Parameters.AddWithValue("@hm_request_id   ", model.hm_request_id);
            cmd.Parameters.AddWithValue("@fileTypeCode    ", model.fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType     ", model.FileSubType);
            cmd.Parameters.AddWithValue("@fileSize        ", model.fileSize);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditHandlechartDetails(HandleDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_HandleChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@hm_handlesub_type", model.hm_handlesub_type);
            cmd.Parameters.AddWithValue("@hm_handle_type  ", model.hm_handle_type);
            cmd.Parameters.AddWithValue("@tempChartNum    ", model.tempChartNum);
            cmd.Parameters.AddWithValue("@hm_chartimg_name", model.hm_chartimg_name);
            cmd.Parameters.AddWithValue("@remark          ", model.remark);
            cmd.Parameters.AddWithValue("@hm_request_type ", model.hm_request_type);
            cmd.Parameters.AddWithValue("@hm_request_id   ", model.hm_request_id);
            cmd.Parameters.AddWithValue("@fileTypeCode    ", model.fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType     ", model.FileSubType);
            cmd.Parameters.AddWithValue("@fileSize        ", model.fileSize);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditHandlechartDetailsactive(HandleDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_HandleChartNoActive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@hm_handlesub_type", model.hm_handlesub_type);
            cmd.Parameters.AddWithValue("@hm_handle_type  ", model.hm_handle_type);
            cmd.Parameters.AddWithValue("@tempChartNum    ", model.tempChartNum);
            cmd.Parameters.AddWithValue("@hm_chartimg_name", model.hm_chartimg_name);
            cmd.Parameters.AddWithValue("@remark          ", model.remark);
            cmd.Parameters.AddWithValue("@hm_request_type ", model.hm_request_type);
            cmd.Parameters.AddWithValue("@hm_request_id   ", model.hm_request_id);
            cmd.Parameters.AddWithValue("@fileTypeCode    ", model.fileTypeCode);
            cmd.Parameters.AddWithValue("@FileSubType     ", model.FileSubType);
            cmd.Parameters.AddWithValue("@fileSize        ", model.fileSize);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int EditOuterchartDetails(OuterDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Update_OuterChartNo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@obd_boxdtl_recid", model.obd_boxdtl_recid);
            cmd.Parameters.AddWithValue("@OUTERPACKMATERIAL", model.OUTERPACKMATERIAL);
            cmd.Parameters.AddWithValue("@OUTERPACKDESCRIPTION", model.OUTERPACKDESCRIPTION);
            cmd.Parameters.AddWithValue("@OUTERPACKFINISH", model.OUTERPACKFINISH);
            cmd.Parameters.AddWithValue("@OUTERPACKAPPLICATION", model.OUTERPACKAPPLICATION);
            cmd.Parameters.AddWithValue("@OUTERPACKLENGTH", model.OUTERPACKLENGTH);
            cmd.Parameters.AddWithValue("@OUTERPACKWIDTH", model.OUTERPACKWIDTH);
            cmd.Parameters.AddWithValue("@OUTERPACKHEIGHT", model.OUTERPACKHEIGHT);
            cmd.Parameters.AddWithValue("@OUTERPACKBATTENPOSITION", model.OUTERPACKBATTENPOSITION);
            cmd.Parameters.AddWithValue("@OUTERPACKIMG", model.OUTERPACKIMG);
            cmd.Parameters.AddWithValue("@OTHER1", model.OTHER1);
            cmd.Parameters.AddWithValue("@OTHER2", model.OTHER2);
            cmd.Parameters.AddWithValue("@OTHER3", model.OTHER3);
            cmd.Parameters.AddWithValue("@OTHER4", model.OTHER4);
            cmd.Parameters.AddWithValue("@OTHER5", model.OTHER5);
            cmd.Parameters.AddWithValue("@OUTERPACKLENGTHrange", model.OUTERPACKLENGTHrange);
            cmd.Parameters.AddWithValue("@OUTERPACKWIDTHrange", model.OUTERPACKWIDTHrange);
            cmd.Parameters.AddWithValue("@OUTERPACKHEIGHTrange", model.OUTERPACKHEIGHTrange);
            cmd.Parameters.AddWithValue("@OUTERPACKBATTENPOSITIONrange ", model.OUTERPACKBATTENPOSITIONrange);
            cmd.Parameters.AddWithValue("@OTHER1range", model.OTHER1range);
            cmd.Parameters.AddWithValue("@OTHER2range", model.OTHER2range);
            cmd.Parameters.AddWithValue("@OTHER3range", model.OTHER3range);
            cmd.Parameters.AddWithValue("@OTHER4range", model.OTHER4range);
            cmd.Parameters.AddWithValue("@OTHER5range", model.OTHER5range);
            cmd.Parameters.AddWithValue("@ob_request_id", model.ob_request_id);
            cmd.Parameters.AddWithValue("@ob_request_type", model.ob_request_type);
            cmd.Parameters.AddWithValue("@ob_box_type", model.ob_box_type);
            cmd.Parameters.AddWithValue("@ob_box_fsize_code", model.ob_box_fsize_code);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int Customersaveactive(CustomerMasterModelForActive model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_CustomerDetailswithactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cm_cust_id", model.Cm_Cust_Id);
            cmd.Parameters.AddWithValue("@cm_cust_name", model.Cm_Cust_Name);
            cmd.Parameters.AddWithValue("@cm_cust_remarks", model.Cm_Cust_Remarks);
            cmd.Parameters.AddWithValue("@cm_cust_isApproved", 'N');
            cmd.Parameters.AddWithValue("@cm_cust_isActive", '1');
            cmd.Parameters.AddWithValue("@cm_cust_verno", 0);
            cmd.Parameters.AddWithValue("@_id", model._id);
            cmd.Parameters.AddWithValue("@cm_request_id", model.cm_request_id);
            cmd.Parameters.AddWithValue("@cm_request_type", model.cm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;

        }
        public int CustomerUpdate(CustomerMasterModelForActive model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_CustomerDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cm_cust_id", model.Cm_Cust_Id);
            cmd.Parameters.AddWithValue("@cm_cust_name", model.Cm_Cust_Name);
            cmd.Parameters.AddWithValue("@cm_cust_remarks", model.Cm_Cust_Remarks);
            cmd.Parameters.AddWithValue("@cm_cust_isApproved", 'N');
            cmd.Parameters.AddWithValue("@cm_cust_isActive", '0');
            cmd.Parameters.AddWithValue("@cm_cust_verno", 0);
            cmd.Parameters.AddWithValue("@cm_request_id", model.cm_request_id);
            cmd.Parameters.AddWithValue("@cm_request_type", model.cm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;

        }
        public int filesubtypeUpdate(FileSubTypeEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UPDATE_FileSubType", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fieldwidth                   ", model1.fieldwidth);
            cmd.Parameters.AddWithValue("@fieldthikness                ", model1.fieldthikness);
            cmd.Parameters.AddWithValue("@fieldmood_length             ", model1.fieldmood_length);
            cmd.Parameters.AddWithValue("@fieldlengthmm                ", model1.fieldlengthmm);
            cmd.Parameters.AddWithValue("@fieldlengthP1mm              ", model1.fieldlengthP1mm);
            cmd.Parameters.AddWithValue("@fieldwidthatShoulder         ", model1.fieldwidthatShoulder);
            cmd.Parameters.AddWithValue("@fieldthiknessatshoulder      ", model1.fieldthiknessatshoulder);
            cmd.Parameters.AddWithValue("@fieldwidthattip              ", model1.fieldwidthattip);
            cmd.Parameters.AddWithValue("@fieldthiknessattip           ", model1.fieldthiknessattip);
            cmd.Parameters.AddWithValue("@fieldpointsizeatexactlength  ", model1.fieldpointsizeatexactlength);
            cmd.Parameters.AddWithValue("@fieldbodydimentionwidth      ", model1.fieldbodydimentionwidth);
            cmd.Parameters.AddWithValue("@fieldbodydimentionthikness   ", model1.fieldbodydimentionthikness);
            cmd.Parameters.AddWithValue("@fieldbodypointsize           ", model1.fieldbodypointsize);
            cmd.Parameters.AddWithValue("@fieldtapperlengthmm          ", model1.fieldtapperlengthmm);
            cmd.Parameters.AddWithValue("@fieldlengthaftercuttingbefore", model1.fieldlengthaftercuttingbefore);
            cmd.Parameters.AddWithValue("@fieldatshoulder              ", model1.fieldatshoulder);
            cmd.Parameters.AddWithValue("@fieldonedge                  ", model1.fieldonedge);
            cmd.Parameters.AddWithValue("@fieldupcut                   ", model1.fieldupcut);
            cmd.Parameters.AddWithValue("@fieldovercut                 ", model1.fieldovercut);
            cmd.Parameters.AddWithValue("@fieldedgecut                 ", model1.fieldedgecut);
            cmd.Parameters.AddWithValue("@fieldhardness                ", model1.fieldhardness);
            cmd.Parameters.AddWithValue("@fieldangel                   ", model1.fieldangel);
            cmd.Parameters.AddWithValue("@fieldradious                 ", model1.fieldradious);
            cmd.Parameters.AddWithValue("@subtypename                  ", model1.subtypename);
            cmd.Parameters.AddWithValue("@subtypecode                  ", model1.subtypecode);
            cmd.Parameters.AddWithValue("@rmcode                       ", model1.rmcode);
            cmd.Parameters.AddWithValue("@wtinkgthousand               ", model1.wtinkgthousand);
            cmd.Parameters.AddWithValue("@description                  ", model1.description);
            cmd.Parameters.AddWithValue("@width                        ", model1.width);
            cmd.Parameters.AddWithValue("@thikness                     ", model1.thikness);
            cmd.Parameters.AddWithValue("@gmssku                       ", model1.gmssku);
            cmd.Parameters.AddWithValue("@mood_length                  ", model1.mood_length);
            cmd.Parameters.AddWithValue("@lengthmm                     ", model1.lengthmm);
            cmd.Parameters.AddWithValue("@lengthP1mm                   ", model1.lengthP1mm);
            cmd.Parameters.AddWithValue("@widthatShoulder              ", model1.widthatShoulder);
            cmd.Parameters.AddWithValue("@thiknessatshoulder           ", model1.thiknessatshoulder);
            cmd.Parameters.AddWithValue("@widthattip                   ", model1.widthattip);
            cmd.Parameters.AddWithValue("@thiknessattip                ", model1.thiknessattip);
            cmd.Parameters.AddWithValue("@pointsizeatexactlength       ", model1.pointsizeatexactlength);
            cmd.Parameters.AddWithValue("@bodydimentionwidth           ", model1.bodydimentionwidth);
            cmd.Parameters.AddWithValue("@bodydimentionthikness        ", model1.bodydimentionthikness);
            cmd.Parameters.AddWithValue("@bodypointsize                ", model1.bodypointsize);
            cmd.Parameters.AddWithValue("@tapperlengthmm               ", model1.tapperlengthmm);
            cmd.Parameters.AddWithValue("@lengthaftercuttingbefore     ", model1.lengthaftercuttingbefore);
            cmd.Parameters.AddWithValue("@atshoulder                   ", model1.atshoulder);
            cmd.Parameters.AddWithValue("@onedge                       ", model1.onedge);
            cmd.Parameters.AddWithValue("@upcut                        ", model1.upcut);
            cmd.Parameters.AddWithValue("@overcut                      ", model1.overcut);
            cmd.Parameters.AddWithValue("@edgecut                      ", model1.edgecut);
            cmd.Parameters.AddWithValue("@hardness                     ", model1.hardness);
            cmd.Parameters.AddWithValue("@angel                        ", model1.angel);
            cmd.Parameters.AddWithValue("@radious                      ", model1.radious);
            cmd.Parameters.AddWithValue("@Remarks                      ", model1.Remarks);
            cmd.Parameters.AddWithValue("@rmcoderange                  ", model1.rmcoderange);
            cmd.Parameters.AddWithValue("@wtinkgthousandrange          ", model1.wtinkgthousandrange);
            cmd.Parameters.AddWithValue("@descriptionrange             ", model1.descriptionrange);
            cmd.Parameters.AddWithValue("@widthrange                   ", model1.widthrange);
            cmd.Parameters.AddWithValue("@thiknessrange                ", model1.thiknessrange);
            cmd.Parameters.AddWithValue("@gmsskurange                  ", model1.gmsskurange);
            cmd.Parameters.AddWithValue("@mood_lengthrange             ", model1.mood_lengthrange);
            cmd.Parameters.AddWithValue("@lengthmmrange                ", model1.lengthmmrange);
            cmd.Parameters.AddWithValue("@lengthP1mmrange              ", model1.lengthP1mmrange);
            cmd.Parameters.AddWithValue("@widthatShoulderrange         ", model1.widthatShoulderrange);
            cmd.Parameters.AddWithValue("@thiknessatshoulderrange      ", model1.thiknessatshoulderrange);
            cmd.Parameters.AddWithValue("@widthattiprange              ", model1.widthattiptrange);
            cmd.Parameters.AddWithValue("@thiknessattiprange           ", model1.thiknessattiprange);
            cmd.Parameters.AddWithValue("@pointsizeatexactlengthrange  ", model1.pointsizeatexactlengthrange);
            cmd.Parameters.AddWithValue("@bodydimentionwidthrange      ", model1.bodydimentionwidthrange);
            cmd.Parameters.AddWithValue("@bodydimentionthiknessrange   ", model1.bodydimentionthiknessrange);
            cmd.Parameters.AddWithValue("@bodypointsizerange           ", model1.bodypointsizerange);
            cmd.Parameters.AddWithValue("@tapperlengthmmrange          ", model1.tapperlengthmmrange);
            cmd.Parameters.AddWithValue("@lengthaftercuttingbeforerange", model1.lengthaftercuttingbeforerange);
            cmd.Parameters.AddWithValue("@atshoulderrange              ", model1.atshoulderrange);
            cmd.Parameters.AddWithValue("@onedgerange                  ", model1.onedgerange);
            cmd.Parameters.AddWithValue("@upcutrange                   ", model1.upcutrange);
            cmd.Parameters.AddWithValue("@overcutrange                 ", model1.overcutrange);
            cmd.Parameters.AddWithValue("@edgecutrange                 ", model1.edgecutrange);
            cmd.Parameters.AddWithValue("@hardnessrange                ", model1.hardnessrange);
            cmd.Parameters.AddWithValue("@angelrange                   ", model1.angelrange);
            cmd.Parameters.AddWithValue("@radiousrange                 ", model1.radiousrange); ;
            cmd.Parameters.AddWithValue("@filesizecode                 ", model1.filesizecode);
            cmd.Parameters.AddWithValue("@filetypecode                 ", model1.filetypecode);
            cmd.Parameters.AddWithValue("@pm_dim_parm_verno            ", '0');
            cmd.Parameters.AddWithValue("@pm_file_code                 ", model1.filetypecode + model1.subtypecode);
            cmd.Parameters.AddWithValue("@pm_request_id                ", model1.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type              ", model1.pm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int filesubtypeactive(FileSubTypeEntity model1)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UPDATE_FileSubTypeactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fieldwidth                   ", model1.fieldwidth);
            cmd.Parameters.AddWithValue("@fieldthikness                ", model1.fieldthikness);
            cmd.Parameters.AddWithValue("@fieldmood_length             ", model1.fieldmood_length);
            cmd.Parameters.AddWithValue("@fieldlengthmm                ", model1.fieldlengthmm);
            cmd.Parameters.AddWithValue("@fieldlengthP1mm              ", model1.fieldlengthP1mm);
            cmd.Parameters.AddWithValue("@fieldwidthatShoulder         ", model1.fieldwidthatShoulder);
            cmd.Parameters.AddWithValue("@fieldthiknessatshoulder      ", model1.fieldthiknessatshoulder);
            cmd.Parameters.AddWithValue("@fieldwidthattip              ", model1.fieldwidthattip);
            cmd.Parameters.AddWithValue("@fieldthiknessattip           ", model1.fieldthiknessattip);
            cmd.Parameters.AddWithValue("@fieldpointsizeatexactlength  ", model1.fieldpointsizeatexactlength);
            cmd.Parameters.AddWithValue("@fieldbodydimentionwidth      ", model1.fieldbodydimentionwidth);
            cmd.Parameters.AddWithValue("@fieldbodydimentionthikness   ", model1.fieldbodydimentionthikness);
            cmd.Parameters.AddWithValue("@fieldbodypointsize           ", model1.fieldbodypointsize);
            cmd.Parameters.AddWithValue("@fieldtapperlengthmm          ", model1.fieldtapperlengthmm);
            cmd.Parameters.AddWithValue("@fieldlengthaftercuttingbefore", model1.fieldlengthaftercuttingbefore);
            cmd.Parameters.AddWithValue("@fieldatshoulder              ", model1.fieldatshoulder);
            cmd.Parameters.AddWithValue("@fieldonedge                  ", model1.fieldonedge);
            cmd.Parameters.AddWithValue("@fieldupcut                   ", model1.fieldupcut);
            cmd.Parameters.AddWithValue("@fieldovercut                 ", model1.fieldovercut);
            cmd.Parameters.AddWithValue("@fieldedgecut                 ", model1.fieldedgecut);
            cmd.Parameters.AddWithValue("@fieldhardness                ", model1.fieldhardness);
            cmd.Parameters.AddWithValue("@fieldangel                   ", model1.fieldangel);
            cmd.Parameters.AddWithValue("@fieldradious                 ", model1.fieldradious);
            cmd.Parameters.AddWithValue("@subtypename                  ", model1.subtypename);
            cmd.Parameters.AddWithValue("@subtypecode                  ", model1.subtypecode);
            cmd.Parameters.AddWithValue("@rmcode                       ", model1.rmcode);
            cmd.Parameters.AddWithValue("@wtinkgthousand               ", model1.wtinkgthousand);
            cmd.Parameters.AddWithValue("@description                  ", model1.description);
            cmd.Parameters.AddWithValue("@width                        ", model1.width);
            cmd.Parameters.AddWithValue("@thikness                     ", model1.thikness);
            cmd.Parameters.AddWithValue("@gmssku                       ", model1.gmssku);
            cmd.Parameters.AddWithValue("@mood_length                  ", model1.mood_length);
            cmd.Parameters.AddWithValue("@lengthmm                     ", model1.lengthmm);
            cmd.Parameters.AddWithValue("@lengthP1mm                   ", model1.lengthP1mm);
            cmd.Parameters.AddWithValue("@widthatShoulder              ", model1.widthatShoulder);
            cmd.Parameters.AddWithValue("@thiknessatshoulder           ", model1.thiknessatshoulder);
            cmd.Parameters.AddWithValue("@widthattip                   ", model1.widthattip);
            cmd.Parameters.AddWithValue("@thiknessattip                ", model1.thiknessattip);
            cmd.Parameters.AddWithValue("@pointsizeatexactlength       ", model1.pointsizeatexactlength);
            cmd.Parameters.AddWithValue("@bodydimentionwidth           ", model1.bodydimentionwidth);
            cmd.Parameters.AddWithValue("@bodydimentionthikness        ", model1.bodydimentionthikness);
            cmd.Parameters.AddWithValue("@bodypointsize                ", model1.bodypointsize);
            cmd.Parameters.AddWithValue("@tapperlengthmm               ", model1.tapperlengthmm);
            cmd.Parameters.AddWithValue("@lengthaftercuttingbefore     ", model1.lengthaftercuttingbefore);
            cmd.Parameters.AddWithValue("@atshoulder                   ", model1.atshoulder);
            cmd.Parameters.AddWithValue("@onedge                       ", model1.onedge);
            cmd.Parameters.AddWithValue("@upcut                        ", model1.upcut);
            cmd.Parameters.AddWithValue("@overcut                      ", model1.overcut);
            cmd.Parameters.AddWithValue("@edgecut                      ", model1.edgecut);
            cmd.Parameters.AddWithValue("@hardness                     ", model1.hardness);
            cmd.Parameters.AddWithValue("@angel                        ", model1.angel);
            cmd.Parameters.AddWithValue("@radious                      ", model1.radious);
            cmd.Parameters.AddWithValue("@Remarks                      ", model1.Remarks);
            cmd.Parameters.AddWithValue("@rmcoderange                  ", model1.rmcoderange);
            cmd.Parameters.AddWithValue("@wtinkgthousandrange          ", model1.wtinkgthousandrange);
            cmd.Parameters.AddWithValue("@descriptionrange             ", model1.descriptionrange);
            cmd.Parameters.AddWithValue("@widthrange                   ", model1.widthrange);
            cmd.Parameters.AddWithValue("@thiknessrange                ", model1.thiknessrange);
            cmd.Parameters.AddWithValue("@gmsskurange                  ", model1.gmsskurange);
            cmd.Parameters.AddWithValue("@mood_lengthrange             ", model1.mood_lengthrange);
            cmd.Parameters.AddWithValue("@lengthmmrange                ", model1.lengthmmrange);
            cmd.Parameters.AddWithValue("@lengthP1mmrange              ", model1.lengthP1mmrange);
            cmd.Parameters.AddWithValue("@widthatShoulderrange         ", model1.widthatShoulderrange);
            cmd.Parameters.AddWithValue("@thiknessatshoulderrange      ", model1.thiknessatshoulderrange);
            cmd.Parameters.AddWithValue("@widthattiprange              ", model1.widthattiptrange);
            cmd.Parameters.AddWithValue("@thiknessattiprange           ", model1.thiknessattiprange);
            cmd.Parameters.AddWithValue("@pointsizeatexactlengthrange  ", model1.pointsizeatexactlengthrange);
            cmd.Parameters.AddWithValue("@bodydimentionwidthrange      ", model1.bodydimentionwidthrange);
            cmd.Parameters.AddWithValue("@bodydimentionthiknessrange   ", model1.bodydimentionthiknessrange);
            cmd.Parameters.AddWithValue("@bodypointsizerange           ", model1.bodypointsizerange);
            cmd.Parameters.AddWithValue("@tapperlengthmmrange          ", model1.tapperlengthmmrange);
            cmd.Parameters.AddWithValue("@lengthaftercuttingbeforerange", model1.lengthaftercuttingbeforerange);
            cmd.Parameters.AddWithValue("@atshoulderrange              ", model1.atshoulderrange);
            cmd.Parameters.AddWithValue("@onedgerange                  ", model1.onedgerange);
            cmd.Parameters.AddWithValue("@upcutrange                   ", model1.upcutrange);
            cmd.Parameters.AddWithValue("@overcutrange                 ", model1.overcutrange);
            cmd.Parameters.AddWithValue("@edgecutrange                 ", model1.edgecutrange);
            cmd.Parameters.AddWithValue("@hardnessrange                ", model1.hardnessrange);
            cmd.Parameters.AddWithValue("@angelrange                   ", model1.angelrange);
            cmd.Parameters.AddWithValue("@radiousrange                 ", model1.radiousrange); ;
            cmd.Parameters.AddWithValue("@filesizecode                 ", model1.filesizecode);
            cmd.Parameters.AddWithValue("@filetypecode                 ", model1.filetypecode);
            cmd.Parameters.AddWithValue("@pm_dim_parm_verno            ", '0');
            cmd.Parameters.AddWithValue("@pm_file_code                 ", model1.filetypecode + model1.subtypecode);
            cmd.Parameters.AddWithValue("@pm_request_id                ", model1.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type              ", model1.pm_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int FileSizeUpdate(FileSizeDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            model.fs_size_mm = model.fs_size_inches * 25;
            SqlCommand cmd = new SqlCommand("sp_update_FileSizeDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fs_size_inches", model.fs_size_inches);
            cmd.Parameters.AddWithValue("@fs_size_code", model.fs_size_code);
            cmd.Parameters.AddWithValue("@fs_size_remarks", model.fs_size_remarks);
            cmd.Parameters.AddWithValue("@fs_size_isApproved", 'N');
            cmd.Parameters.AddWithValue("@fs_size_isActive", '0');
            cmd.Parameters.AddWithValue("@fs_size_verno", 0);
            cmd.Parameters.AddWithValue("@fs_size_mm", model.fs_size_mm);
            cmd.Parameters.AddWithValue("@fs_request_id", model.fs_request_id);
            cmd.Parameters.AddWithValue("@fs_request_type", model.fs_request_type);

            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int filetypeUpdate(FileTypeDataEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_FileTypeDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ft_ftype_code", model.ft_ftype_code);
            cmd.Parameters.AddWithValue("@ft_ftype_desc", model.ft_ftype_desc);
            cmd.Parameters.AddWithValue("@ft_ftype_remarks", model.ft_ftype_remarks);
            cmd.Parameters.AddWithValue("@ft_ftype_isApproved", 'N');
            cmd.Parameters.AddWithValue("@ft_ftype_isActive", '0');
            cmd.Parameters.AddWithValue("@ft_ftype_verno", 0);
            cmd.Parameters.AddWithValue("@ft_ftype_profile_img", model.ft_ftype_profile_img);
            cmd.Parameters.AddWithValue("@ft_request_id", model.ft_request_id);
            cmd.Parameters.AddWithValue("@ft_request_type", model.ft_request_type);

            int i = cmd.ExecuteNonQuery();
            return i;

        }
        public int CutSpecUpdate(CutSpecsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_CutSpecDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cutStandardName", model.cutStandardName);
            cmd.Parameters.AddWithValue("@cutStandardCode", model.cutStandardCode);
            //cmd.Parameters.AddWithValue("@cuttype", model.cuttype);
            cmd.Parameters.AddWithValue("@fcl_isApproved", 'N');
            cmd.Parameters.AddWithValue("@fcl_isActive", '0');
            cmd.Parameters.AddWithValue("@fcl_verno", 0);
            cmd.Parameters.AddWithValue("@edcutrange", model.edcutrange);
            cmd.Parameters.AddWithValue("@edcutvalue", model.edcutvalue);
            cmd.Parameters.AddWithValue("@upcutrange", model.upcutrange);
            cmd.Parameters.AddWithValue("@upcutvalue", model.upcutvalue);
            cmd.Parameters.AddWithValue("@ovcutrange", model.ovcutrange);
            cmd.Parameters.AddWithValue("@ovcutvalue", model.ovcutvalue);
            cmd.Parameters.AddWithValue("@prc_request_id", model.prc_request_id);
            cmd.Parameters.AddWithValue("@prc_request_type", model.prc_request_type);

            int i = cmd.ExecuteNonQuery();
            return i;

        }

        public int RawMaterialUpdatenoactive(RawMaterialMastrEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_RawMaterialDetailsnoactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rm_code", model.rm_code);
            cmd.Parameters.AddWithValue("@rm_desc", model.rm_desc);
            cmd.Parameters.AddWithValue("@rm_material", model.rm_material);
            cmd.Parameters.AddWithValue("@rm_wtinkg_perthsnd", model.rm_wtinkg_perthsnd);
            cmd.Parameters.AddWithValue("@rm_netwtingm_persku", model.rm_netwtingm_persku);
            cmd.Parameters.AddWithValue("@rm_height", model.rm_height);
            cmd.Parameters.AddWithValue("@rm_width", model.rm_width);
            cmd.Parameters.AddWithValue("@rm_fsize_id", model.rm_fsize_id);
            cmd.Parameters.AddWithValue("@rm_ftype_id", model.rm_ftype_id);
            cmd.Parameters.AddWithValue("@rm_fstype_id", model.rm_fstype_id);
            cmd.Parameters.AddWithValue("@rm_request_type", model.rm_request_type);
            cmd.Parameters.AddWithValue("@rm_request_id", model.rm_request_id);
            cmd.Parameters.AddWithValue("@rm_remarks", model.rm_remarks);

            int i = cmd.ExecuteNonQuery();
            return i;

        }
        public int RawMaterialUpdate(RawMaterialMastrEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_RawMaterialDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rm_code", model.rm_code);
            cmd.Parameters.AddWithValue("@rm_desc", model.rm_desc);
            cmd.Parameters.AddWithValue("@rm_material", model.rm_material);
            cmd.Parameters.AddWithValue("@rm_wtinkg_perthsnd", model.rm_wtinkg_perthsnd);
            cmd.Parameters.AddWithValue("@rm_netwtingm_persku", model.rm_netwtingm_persku);
            cmd.Parameters.AddWithValue("@rm_height", model.rm_height);
            cmd.Parameters.AddWithValue("@rm_width", model.rm_width);
            cmd.Parameters.AddWithValue("@rm_fsize_id", model.rm_fsize_id);
            cmd.Parameters.AddWithValue("@rm_ftype_id", model.rm_ftype_id);
            cmd.Parameters.AddWithValue("@rm_fstype_id", model.rm_fstype_id);
            cmd.Parameters.AddWithValue("@rm_request_type", model.rm_request_type);
            cmd.Parameters.AddWithValue("@rm_request_id", model.rm_request_id);
            cmd.Parameters.AddWithValue("@rm_remarks", model.rm_remarks);

            int i = cmd.ExecuteNonQuery();
            return i;

        }
        public int BrandUpdate(BrandDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_BrandDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bm_brand_id", model.bm_brand_id);
            cmd.Parameters.AddWithValue("@bm_brand_name", model.bm_brand_name);
            cmd.Parameters.AddWithValue("@bm_brand_remarks", model.bm_brand_remarks);
            cmd.Parameters.AddWithValue("@bm_brand_isApproved", 'N');
            cmd.Parameters.AddWithValue("@bm_brand_isActive", '0');
            cmd.Parameters.AddWithValue("@bm_brand_verno", 0);
            cmd.Parameters.AddWithValue("@bm_request_id", model.bm_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;

        }
        public int filetypesaveactive(FileTypeDataEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_FileTypeDetailswithactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ft_ftype_code", model.ft_ftype_code);
            cmd.Parameters.AddWithValue("@ft_ftype_desc", model.ft_ftype_desc);
            cmd.Parameters.AddWithValue("@ft_ftype_remarks", model.ft_ftype_remarks);
            cmd.Parameters.AddWithValue("@ft_ftype_isApproved", 'N');
            cmd.Parameters.AddWithValue("@ft_ftype_isActive", '1');
            cmd.Parameters.AddWithValue("@ft_ftype_verno", 0);
            cmd.Parameters.AddWithValue("@_id", model._id);
            ////cmd.Parameters.AddWithValue("@ft_request_id", model.ft_request_id);
            ////cmd.Parameters.AddWithValue("@ft_request_type", model.ft_request_type);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int filesizesaveactive(FileSizeDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_FileSizeDetailswithactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fs_size_inches", model.fs_size_inches);
            cmd.Parameters.AddWithValue("@fs_size_code", model.fs_size_code);
            cmd.Parameters.AddWithValue("@fs_size_remarks", model.fs_size_remarks);
            cmd.Parameters.AddWithValue("@fs_size_isApproved", 'N');
            cmd.Parameters.AddWithValue("@fs_size_isActive", '1');
            cmd.Parameters.AddWithValue("@fs_size_verno", 0);
            cmd.Parameters.AddWithValue("@_id", model._id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int CutSpecssaveactive(CutSpecsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_CutSpecsDetailswithactive", con);
            cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.AddWithValue("@fcl_ftype_code", model.fcl_ftype_code);
            //cmd.Parameters.AddWithValue("@filesize", model.filesize);
            //cmd.Parameters.AddWithValue("@cuttype", model.cuttype);
            cmd.Parameters.AddWithValue("@cutStandardName", model.cutStandardName);
            cmd.Parameters.AddWithValue("@upcutrange", model.upcutrange);
            cmd.Parameters.AddWithValue("@edcutrange", model.edcutrange);
            cmd.Parameters.AddWithValue("@ovcutrange", model.ovcutrange);
            cmd.Parameters.AddWithValue("@upcutvalue", model.upcutvalue);
            cmd.Parameters.AddWithValue("@edcutvalue", model.edcutvalue);
            cmd.Parameters.AddWithValue("@ovcutvalue", model.ovcutvalue);
            cmd.Parameters.AddWithValue("@prc_request_id", model.prc_request_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int Bandsaveactive(BrandDetailsEntity model)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_update_BrandDetailswithactive", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bm_brand_id", model.bm_brand_id);
            cmd.Parameters.AddWithValue("@bm_brand_name", model.bm_brand_name);
            cmd.Parameters.AddWithValue("@bm_brand_remarks", model.bm_brand_remarks);
            cmd.Parameters.AddWithValue("@bm_brand_isApproved", 'N');
            cmd.Parameters.AddWithValue("@bm_brand_isActive", '1');
            cmd.Parameters.AddWithValue("@bm_brand_verno", 0);
            cmd.Parameters.AddWithValue("@_id", model._id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        public DataSet getCustomerListinactive()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_customer_master_details_noactive", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public string CheckFileSizeCode(string sizecode)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select fs_size_code from tb_filesize_master where fs_size_code='" + sizecode + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string SqNo = "";
            if (DR1.HasRows)
            {
                SqNo = DR1.GetValue(0).ToString();
            }
            con.Close();
            return SqNo;
        }
        #endregion


        #region AssetMagmt >> Sanket
        public DataTable getusercount(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //User Login
        public bool OEMlogin(UsermgmtEntity u)
        {
            try
            {
                connectionstring = objUtils.getConnString();
                using (SqlConnection con = new SqlConnection(connectionstring))
                {
                    //HASHBYTES('SHA2_512', @Password)
                    //SqlCommand cmd = new SqlCommand("SELECT * FROM [tbl_mstr_login] where User_Id=@User_Id AND Password=@Password and approved_flag='Yes'", con);
                    SqlCommand cmd = new SqlCommand("sp_Login", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.Parameters.AddWithValue("@User_Id", u.User_Id);
                    cmd.Parameters.AddWithValue("@Password", u.Password);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        u.Login_Id = dt.Rows[0]["Login_Id"].ToString();
                        u.RoleName = dt.Rows[0]["RoleName"].ToString();
                        u.OEMId = dt.Rows[0]["OEMId"].ToString();
                        // u.Customer_Id = dt.Rows[0]["Customer_Id"].ToString();
                        u.OEMName = dt.Rows[0]["OEMName"].ToString();
                        u.FirstName = dt.Rows[0]["FirstName"].ToString();
                        u.LastName = dt.Rows[0]["LastName"].ToString();
                        u.Main_Login_Flag = dt.Rows[0]["Main_Login_Flag"].ToString();
                        u.Report_Login_Flag = dt.Rows[0]["Report_Login_Flag"].ToString();
                        //  u.Region = dt.Rows[0]["Region"].ToString();
                        // u.Group = dt.Rows[0]["Group"].ToString();
                        HttpContext.Current.Session["User"] = u;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        //inserting role 
        public int saveroleDetails(UsermgmtEntity roleObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleName", roleObj.RoleName);
            cmd.Parameters.AddWithValue("@Description", roleObj.Description);
            cmd.Parameters.AddWithValue("@flag", roleObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get role list 
        public DataTable getroles(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //update role 
        public int updateroleDetails(UsermgmtEntity roleObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleName", roleObj.RoleName);
            cmd.Parameters.AddWithValue("@Description", roleObj.Description);
            cmd.Parameters.AddWithValue("@Role_Id", roleObj.Role_Id);
            cmd.Parameters.AddWithValue("@flag", roleObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //delete role 
        public int deleteroleDetails(UsermgmtEntity roleObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@RoleName", roleObj.RoleName);
            //cmd.Parameters.AddWithValue("@Description", roleObj.Description);
            cmd.Parameters.AddWithValue("@Role_Id", roleObj.Role_Id);
            cmd.Parameters.AddWithValue("@flag", roleObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get plant list 
        public DataTable getplants(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //get ProfileImage 
        public DataTable getimage(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //get Cst details 
        public DataTable getcstdetails(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //inserting user 
        public int saveuserDetails(UsermgmtEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@OEMId", Obj.OEMId);
            cmd.Parameters.AddWithValue("@OEMName", Obj.OEMName);
            cmd.Parameters.AddWithValue("@User_Id", Obj.User_Id);
            cmd.Parameters.AddWithValue("@Password", Obj.Encrypt(Obj.Password));
            cmd.Parameters.AddWithValue("@FirstName", Obj.FirstName);
            cmd.Parameters.AddWithValue("@LastName", Obj.LastName);
            cmd.Parameters.AddWithValue("@Employee_Id", Obj.Employee_Id);
            cmd.Parameters.AddWithValue("@Department", Obj.Department);
            cmd.Parameters.AddWithValue("@RoleName", Obj.RoleName);
            cmd.Parameters.AddWithValue("@PlantName", Obj.PlantName);
            cmd.Parameters.AddWithValue("@ProfilePic", Obj.ProfilePic);
            cmd.Parameters.AddWithValue("@approved_flag", Obj.ApproveFlag);
            cmd.Parameters.AddWithValue("@Registration_Key", Obj.Registration_Key);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get User details 
        public DataTable getuserdetails(UsermgmtEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@RoleName", Obj.RoleName);
            cmd.Parameters.AddWithValue("@PlantName", Obj.PlantName);
            cmd.Parameters.AddWithValue("@User_Id", Obj.User_Id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //get user list 
        public DataTable getusers(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //update user login  
        public int updatemainflagDetails(UsermgmtEntity roleObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@RoleName", roleObj.RoleName);
            //cmd.Parameters.AddWithValue("@Description", roleObj.Description);
            cmd.Parameters.AddWithValue("@Login_Id", roleObj.Login_Id);
            cmd.Parameters.AddWithValue("@flag", roleObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        //update user login  
        public int deleteuserDetails(UsermgmtEntity roleObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@RoleName", roleObj.RoleName);
            //cmd.Parameters.AddWithValue("@Description", roleObj.Description);
            cmd.Parameters.AddWithValue("@Login_Id", roleObj.Login_Id);
            cmd.Parameters.AddWithValue("@flag", roleObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get admin setting list 
        public DataTable getadminsetting(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_configuration", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //get User details 
        public DataTable updateadminconfig(AdminsettingEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_configuration", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@Parameter_Value", Obj.Parameter_Value);
            cmd.Parameters.AddWithValue("@Operator", Obj.Operator);
            cmd.Parameters.AddWithValue("@Operation", Obj.Operation);
            cmd.Parameters.AddWithValue("@Config_Id", Obj.Config_Id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //Update UserApprove--by nisha
        public DataTable updateapprove(UsermgmtEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            //cmd.Parameters.AddWithValue("@RoleName", Obj.RoleName);
            //cmd.Parameters.AddWithValue("@PlantName", Obj.PlantName);
            cmd.Parameters.AddWithValue("@Login_Id", Obj.Login_Id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //get User details 
        public int insertrejection(UsermgmtEntity Obj)
        {
            //DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();

            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@Decline_Reason", Obj.Decline_Reason);
            cmd.Parameters.AddWithValue("@User_Id", Obj.User_Id);
            //cmd.Parameters.AddWithValue("@Login_Id", Obj.Login_Id);
            int i = cmd.ExecuteNonQuery();
            return i;

        }




        //get role list 
        public DataTable getrqstype(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_request_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //inserting request 
        public int saverequestDetails(RequestMgmtEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_request_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OEMId", Obj.OEMId);
            cmd.Parameters.AddWithValue("@OEMName", Obj.OEMName);
            cmd.Parameters.AddWithValue("@Customer_Id", Obj.Customer_Id);
            cmd.Parameters.AddWithValue("@RequestType", Obj.RequestType);
            cmd.Parameters.AddWithValue("@RequestDesc", Obj.RequestDesc);
            cmd.Parameters.AddWithValue("@RequestStatus", "New");
            cmd.Parameters.AddWithValue("@RequestBetween", "0");
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        //inserting request 
        public int saverequesttype(RequestMgmtEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_request_mgmt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Request_Type", Obj.RequestType);
            cmd.Parameters.AddWithValue("@Request_Desc", Obj.RequestDesc);
            cmd.Parameters.AddWithValue("@Request_Id", Obj.Request_Id);
            cmd.Parameters.AddWithValue("@Comments", Obj.Comments);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        //get user list 
        public DataTable getrequest(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_request_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //get User details 
        public DataTable updaterequeststatus(RequestMgmtEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_request_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@RequestStatus", Obj.RequestStatus);
            cmd.Parameters.AddWithValue("@Request_Id", Obj.Request_Id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //get role list 
        public DataTable getcst(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public DataSet menulist(UsermgmtEntity u)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", u.flag);
                cmd.Parameters.AddWithValue("@RoleName", u.RoleName);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet getcutspectview1(string flag, string cutStandardName, string cuttype, string fcl_ftype_code, string filetype, string filesizecode)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.Parameters.AddWithValue("@fc_cut_desc", cuttype);
                cmd.Parameters.AddWithValue("@vtpi_fsize_code", filesizecode);
                cmd.Parameters.AddWithValue("@filetypecode", fcl_ftype_code);
                cmd.Parameters.AddWithValue("@vtpi_cut_std_name", cutStandardName);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        #endregion

        #region Masters view -- sanket

        public DataTable getCutSpecMaster(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                //cmd.Parameters.AddWithValue("@filesubtype", objFilespec.FileSubType);
                //cmd.Parameters.AddWithValue("@filesizeinch", objFilespec.FileSize);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getrawmaterial(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public DataTable getrawmaterialedit(string flag, string code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@rm_code", code);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public DataTable getcstmstredit(string flag, string code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@cstname", code);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //processmasterlist
        public DataTable getmasterList(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_getmasterList", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getopreationmasterList(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getValuesteramProcess(string flag, string ppm_ftype_code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@ppm_ftype_code", ppm_ftype_code);
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        //inserting newprocess
        public int updateprocessDetails(OperationMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_getmasterList", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@opr_operation_seq", processObj.opr_operation_seq);
            cmd.Parameters.AddWithValue("@opr_operation_name", processObj.opr_operation_name);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataSet getCustomerbrandList(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataSet getrawmateriallistnoapprove(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        public DataTable wrapperNoApprove(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_wrappermasteractive_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable wrapperNoActive(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_wrappermasteractive_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable stmaplistactive(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_stmapmasteractive_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getstamp(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@stamp_image", "null");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataSet getstampimgpathview(string stamp_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@stamp_image", stamp_chart);
            cmd.Parameters.AddWithValue("@flag", "stampmastermasterviewimage");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet getGetShowStampChart(string stamp_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@stamp_image", stamp_chart);
            cmd.Parameters.AddWithValue("@flag", "getGetShowStampChart");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataTable gethndleMasters(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataSet gethandleimgpathview(string handle_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@hndlechartno", handle_chart);
            cmd.Parameters.AddWithValue("@flag", "gethandlemstrview");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetShowHandelChart(string handle_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@hndlechartno", handle_chart);
            cmd.Parameters.AddWithValue("@flag", "GetShowHandelChart");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataTable getwrapperMasters(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataSet getwrappinglistview(string wrapping_chart)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@wrapperchartno", wrapping_chart);
            cmd.Parameters.AddWithValue("@flag", "getwrappermstrview");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetShowWrapperdetails(string reqid,string reqtype)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@wm_request_id", reqid);
            cmd.Parameters.AddWithValue("@wm_request_type", reqtype);
            cmd.Parameters.AddWithValue("@flag", "GetShowWrapperdetails");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataSet GetPalletListview(string pallet_chart, string pallet_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@palletchartno", pallet_chart);
            cmd.Parameters.AddWithValue("@pallettype", pallet_type);
            cmd.Parameters.AddWithValue("@flag", "getpalletmstrview");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataSet getApprovepalletmstrview(string pallet_chart, string pallet_type)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@palletchartno", pallet_chart);
            cmd.Parameters.AddWithValue("@pallettype", pallet_type);
            cmd.Parameters.AddWithValue("@flag", "getApprovepalletmstrview");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataTable getfilesubtype(string flag, string sizecode, string filecode)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_fileSubtype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@filesizecode", sizecode);
            cmd.Parameters.AddWithValue("@filecode", filecode);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public DataTable getinnerlabledetails(string flag, string chartno)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@ilbdchartnum", chartno);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }



        public DataSet getinnerboxlistview(string innerbox_chart, string pr_ftype_desc)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@innerbox_chart", innerbox_chart);
            cmd.Parameters.AddWithValue("@pr_ftype_desc", pr_ftype_desc);
            cmd.Parameters.AddWithValue("@flag", "getinnerboxmstrview");
            //cmd.Parameters.AddWithValue("@pr_ftype_code", pr_ftype_code);
            // cmd.Parameters.AddWithValue("@pr_fsize_code", pr_fsize_code);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public DataSet Getshowinnerbox(string innerbox_chart, string pr_ftype_desc)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@innerbox_chart", innerbox_chart);
            cmd.Parameters.AddWithValue("@pr_ftype_desc", pr_ftype_desc);
            cmd.Parameters.AddWithValue("@flag", "Getshowinnerbox");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }


        public DataSet GetinnerLabelListView(string innerlabel_chart, string ft_ftype_desc)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@innerlabel_chart", innerlabel_chart);
            cmd.Parameters.AddWithValue("@pr_ftype_desc", ft_ftype_desc);
            cmd.Parameters.AddWithValue("@flag", "getinnerlabelmstrview");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet GetShowinnerlabel(string innerlabel_chart, string ft_ftype_desc)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@innerlabel_chart", innerlabel_chart);
            cmd.Parameters.AddWithValue("@pr_ftype_desc", ft_ftype_desc);
            cmd.Parameters.AddWithValue("@flag", "GetShowinnerlabel");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }


        public DataSet GetOuterChartlistview(string outerbox_chart, string boxtype)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@outerchartno", outerbox_chart);
            cmd.Parameters.AddWithValue("@oboxtype", boxtype);
            cmd.Parameters.AddWithValue("@flag", "getouterboxmstrview");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }
        public DataSet Getshowoutermaster(string outerbox_chart, string boxtype)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_master_details", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@outerchartno", outerbox_chart);
            cmd.Parameters.AddWithValue("@oboxtype", boxtype);
            cmd.Parameters.AddWithValue("@flag", "Getshowoutermaster");
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }


        //Assign Module Management
        public DataTable getmoduledata(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_asset_mgmt", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }



        public DataTable getcutspectview(string flag, string cutStandardName, string cuttype, string fcl_ftype_code, string filetype, string filesizecode)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@fc_cut_desc", cuttype);
            cmd.Parameters.AddWithValue("@vtpi_fsize_code", filesizecode);
            cmd.Parameters.AddWithValue("@filetypecode", fcl_ftype_code);
            cmd.Parameters.AddWithValue("@vtpi_cut_std_name", cutStandardName);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        //public DataSet getcutspectview1(string flag, string cutStandardName, string cuttype, string fcl_ftype_code, string filetype, string filesizecode)
        //{
        //    DataSet ds = new DataSet();
        //    connectionstring = objUtils.getConnString();
        //    SqlConnection con = new SqlConnection(connectionstring);
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand("sp_get_CutSpec_master", con);
        //        cmd.Parameters.AddWithValue("@flag", flag);
        //        cmd.Parameters.AddWithValue("@fc_cut_desc", cuttype);
        //        cmd.Parameters.AddWithValue("@vtpi_fsize_code", filesizecode);
        //        cmd.Parameters.AddWithValue("@filetypecode", fcl_ftype_code);
        //        cmd.Parameters.AddWithValue("@vtpi_cut_std_name", cutStandardName);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //        adp.Fill(ds);

        //        cmd.Dispose();
        //    }

        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return ds;
        //}

        #endregion

        #region Master Insert--sanket
        //get User details 
        public DataTable insertfilesize(FileSizeDetailsEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_insert_masters", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@fs_size_inches", Obj.fs_size_inches);
            cmd.Parameters.AddWithValue("@fs_size_code", Obj.fs_size_code);
            cmd.Parameters.AddWithValue("@fs_size_mm", Obj.fs_size_mm);
            cmd.Parameters.AddWithValue("@remarks", Obj.fs_size_remarks);
            cmd.Parameters.AddWithValue("@requestid", Obj.fs_request_id);
            cmd.Parameters.AddWithValue("@approveby", Obj.fs_size_approveby);
            cmd.Parameters.AddWithValue("@createby", Obj.fs_size_createby);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable insertnewcustomer(CustomerDetailsEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_insert_masters", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@cm_cust_id", Obj.Cm_Cust_Id);
            cmd.Parameters.AddWithValue("@cm_cust_name", Obj.Cm_Cust_Name);
            cmd.Parameters.AddWithValue("@remarks", Obj.Cm_Cust_Remarks);
            cmd.Parameters.AddWithValue("@requestid", Obj.cm_request_id);
            cmd.Parameters.AddWithValue("@approveby", Obj.Cm_Cust_Approvedby);
            cmd.Parameters.AddWithValue("@createby", Obj.Cm_Cust_Createby);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable insertnewbrand(BrandDetailsEntity Obj)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_insert_masters", con);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.AddWithValue("@bm_brand_id", Obj.bm_brand_id);
            cmd.Parameters.AddWithValue("@bm_brand_name", Obj.bm_brand_name);
            cmd.Parameters.AddWithValue("@remarks", Obj.bm_brand_remarks);
            cmd.Parameters.AddWithValue("@requestid", Obj.bm_request_id);
            cmd.Parameters.AddWithValue("@approveby", Obj.bm_brand_approveby);
            cmd.Parameters.AddWithValue("@createby", Obj.bm_brand_createby);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region Remark by Yogesh
        public int SaveCustomerRemark(PartnumgenRemarksEntity remarkObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_Partnumgen_Remarks", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.AddWithValue("@pr_request_type", 'N');
            cmd.Parameters.AddWithValue("@prm_request_type", remarkObj.prm_request_type);
            cmd.Parameters.AddWithValue("@prm_request_id", remarkObj.prm_request_id);
            cmd.Parameters.AddWithValue("@prm_remark_tab", remarkObj.prm_remark_tab);
            cmd.Parameters.AddWithValue("@prm_remarks", remarkObj.prm_remarks);
            cmd.Parameters.AddWithValue("@prm_createby", remarkObj.prm_createby);
            int i = cmd.ExecuteNonQuery();
            return i;
        }


        public DataSet GetRemarkDetails(string remark_tab, int request_id, string request_type)
        {
            PartnumgenRemarksEntity DetailsEntity = new PartnumgenRemarksEntity();
            List<PartnumgenRemarksEntity> DetailsEntityList = new List<PartnumgenRemarksEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);

            con.Open();
            // AND prm_request_id = '" + request_id + "'
            SqlCommand cmd = new SqlCommand("select prm_request_type,prm_request_id,prm_remark_tab,prm_remarks,prm_createdate from tb_partnumgen_remarks where prm_request_type ='" + request_type + "'and prm_request_id=" + request_id, con);
            cmd.ExecuteNonQuery();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        #endregion


        #region Manage Unit
        public DataTable getUnitlist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getUnitnormlist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable get_unitvaluestremlist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataSet getPlantDropownList(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_PlantDetails", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataSet getUnitDropownList(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public DataTable getAncillarylist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_AncillaryDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public int SaveUnitDetails(UnitMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@uni_unit_code", processObj.uni_unit_code);
            cmd.Parameters.AddWithValue("@uni_unit_name", processObj.uni_unit_name);
            cmd.Parameters.AddWithValue("@uni_unit_attachTo", processObj.uni_unit_attachTo);
            cmd.Parameters.AddWithValue("@uni_unit_type", processObj.uni_unit_type);
            cmd.Parameters.AddWithValue("@uni_unit_absentisum", processObj.uni_unit_absentisum);
            cmd.Parameters.AddWithValue("@uni_unit_material_downtime", processObj.uni_unit_material_downtime);
            cmd.Parameters.AddWithValue("@uni_createby", processObj.uni_createby);
            cmd.Parameters.AddWithValue("@uni_approveby", processObj.uni_approveby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int SaveValueStream(valueStreammasterEntity vsmobj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@val_valuestream_code", vsmobj.val_valuestream_code);
            cmd.Parameters.AddWithValue("@val_valuestream_name", vsmobj.val_valuestream_name);
            cmd.Parameters.AddWithValue("@flag", vsmobj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int AddUnitValueStream(UnitMasterEntity vsmobj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_UnitDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@val_unit_code", vsmobj.val_unit_code);
            cmd.Parameters.AddWithValue("@val_valuestream_code", vsmobj.val_valuestream_code);
            cmd.Parameters.AddWithValue("@val_alternate_valuestream_code", vsmobj.val_alternate_valuestream_code);
            cmd.Parameters.AddWithValue("@val_isApproved", vsmobj.val_isApproved);
            cmd.Parameters.AddWithValue("@val_isActive", vsmobj.val_isActive);
            cmd.Parameters.AddWithValue("@val_createby", vsmobj.val_createby);
            cmd.Parameters.AddWithValue("@val_approveby", vsmobj.val_approveby);
            cmd.Parameters.AddWithValue("@flag", vsmobj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public string PlantforPlantCode(string PlantID)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select pla_plant_code from tb_plant_master where pla_plant_code='" + PlantID + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string plantcode = DR1.GetValue(0).ToString();
            con.Close();
            return plantcode;

        }
        public string ShowValueStreamNo(string VSNo)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select ft_ftype_code from tb_filetype_master where ft_ftype_code='" + VSNo + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string VSMNo = DR1.GetValue(0).ToString();
            con.Close();
            return VSMNo;

        }

        public DataTable getPlantAncillarylist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_AncillaryDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public string CheckValusreamCode(string ft_ftype_code)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select ft_ftype_code from tb_filetype_master where ft_ftype_code='" + ft_ftype_code + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string valScode = "";
            if (DR1.HasRows)
            {
                valScode = DR1.GetValue(0).ToString();
            }
            con.Close();
            return valScode;
        }

        public string CheckUnitCode(string Unitcode)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select uni_unit_code from tb_unit_master where uni_unit_code='" + Unitcode + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string SqNo = "";
            if (DR1.HasRows)
            {
                SqNo = DR1.GetValue(0).ToString();
            }
            con.Close();
            return SqNo;
        }

        public int CheckValuestreamcode(string Unitcode, string valuestream)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select val_unit_code from tb_unit_valuestream_master where val_unit_code='" + Unitcode + "' AND val_valuestream_code='" + valuestream + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            int VS = 0;
            if (DR1.HasRows)
            {
                VS = 1;
            }
            con.Close();
            return VS;
        }
        #endregion

        #region Manage SKUs Selection

        public DataSet getProduSKUList()
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_parnum_dropdown", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public int saveCustomerSetDetails(CustomerDetailsEntity custObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_CustomerSetDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.AddWithValue("@pr_request_type", 'S');
            cmd.Parameters.AddWithValue("@pr_cust_id", custObj.Cm_Cust_Id);
            cmd.Parameters.AddWithValue("@pr_cust_name", custObj.Cm_Cust_Name);
            cmd.Parameters.AddWithValue("@pr_brand_id", custObj.brandId);
            cmd.Parameters.AddWithValue("@pr_brand_desc", custObj.brandName);
            cmd.Parameters.AddWithValue("@pr_ship2country_id", custObj.countryId);
            cmd.Parameters.AddWithValue("@pr_ship2country_name", custObj.countryName);
            cmd.Parameters.AddWithValue("@pr_sap_no", custObj.sapNo);
            cmd.Parameters.AddWithValue("@pr_customer_part_no", custObj.customerPartNO);
            cmd.Parameters.AddWithValue("@pr_uniq_id", custObj.pr_uniq_id);
            cmd.Parameters.AddWithValue("@pr_request_type", custObj.cm_request_type);
            cmd.Parameters.AddWithValue("@pr_request_id", custObj.cm_request_id);

            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int CreateNewSetReq(string pr_uniq_id, string user_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_Insert_NewReq", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_request_type", "S");
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            cmd.Parameters.AddWithValue("@user_id", user_id);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int GetNewSetReq(string pr_uniq_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_id_productionsku", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            int i = Convert.ToInt16(cmd.ExecuteScalar());
            return i;
        }

        public string GetNewSetReqtype(string pr_uniq_id)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_type_productionsku", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pr_uniq_id", pr_uniq_id);
            string i = Convert.ToString(cmd.ExecuteScalar());
            return i;
        }

        public DataSet getPartNumList(string custID)
        {
            PartNumMasterEntity DetailsEntity = new PartNumMasterEntity();
            List<PartNumMasterEntity> DetailsEntityList = new List<PartNumMasterEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);

            con.Open();
            // AND prm_request_id = '" + request_id + "'
            SqlCommand cmd = new SqlCommand("SELECT pn_part_no from tb_partnum_master  where pn_cust_id='" + custID + "' and pn_isActive=1 and pn_isApproved='Y' and pn_request_type<>'S'", con);
            cmd.ExecuteNonQuery();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getpartlistDetaillist(string PartNo)
        {
            PartNumMasterEntity DetailsEntity = new PartNumMasterEntity();
            List<PartNumMasterEntity> DetailsEntityList = new List<PartNumMasterEntity>();

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);

            con.Open();
            SqlCommand cmd = new SqlCommand("select a.dp_stamp_chart, a.dp_wrapping_chart, a.dp_handle_chart,b.pn_start_date,b.pn_request_type,b.pn_request_id,b.pn_end_date,p.pr_tang_color from tb_dispatch_sku a, tb_partnum_master b  left join tb_production_sku p on p.pr_request_id=b.pn_request_id and p.pr_request_type=b.pn_request_type where b.pn_part_no  = '" + PartNo + "' and b.pn_request_type = a.dp_request_type and b.pn_request_id = a.dp_request_id", con);
            cmd.ExecuteNonQuery();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public int SaveSKUSelectionMaster(SkuSelectionEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rs_request_type", Obj.rs_request_type);
            cmd.Parameters.AddWithValue("@rs_sku_code", Obj.rs_sku_code);
            cmd.Parameters.AddWithValue("@rs_quantity", Obj.rs_quantity);
            cmd.Parameters.AddWithValue("@rs_stamp_chart", Obj.rs_stamp_chart);
            cmd.Parameters.AddWithValue("@rs_wrapper_chart", Obj.rs_wrapper_chart);
            cmd.Parameters.AddWithValue("@rs_handle_chart", Obj.rs_handle_chart);
            cmd.Parameters.AddWithValue("@rs_tang_tempr", Obj.rs_tang_tempr);
            cmd.Parameters.AddWithValue("@rs_sku_start_date", Obj.rs_sku_start_date);
            cmd.Parameters.AddWithValue("@rs_sku_end_date", Obj.rs_sku_end_date);
            cmd.Parameters.AddWithValue("@rs_request_id", Obj.rs_request_id);
            cmd.Parameters.AddWithValue("@rs_createby", Obj.rs_createby);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataSet getSetWrappingChartNums(string wrappingType)
        {

            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(" select wm_chart_num from tb_wrapper_master where wm_wrappertype='" + wrappingType + "'", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        public int SaveupdateskudispatchMaster(DispatchSKUEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_wrapping_type", Obj.dp_wrapping_type);
            cmd.Parameters.AddWithValue("@dp_wrapping_chart", Obj.dp_wrapping_chart);
            cmd.Parameters.AddWithValue("@dp_request_type", Obj.dp_request_type);
            cmd.Parameters.AddWithValue("@dp_request_id", Obj.dp_request_id);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int SaveupdatewrapperskuMaster(SkuSelectionEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rs_request_type", Obj.rs_request_type);
            cmd.Parameters.AddWithValue("@rs_wrapper_chart", Obj.rs_wrapper_chart);
            cmd.Parameters.AddWithValue("@rs_request_id", Obj.rs_request_id);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataSet getSetInnerBoxChartNums(string innerBoxMaterial, string maxfileSize)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("select ib_chartnum from tb_innerbox_master where ib_box_type ='" + innerBoxMaterial + "' AND ib_qty_inbox =1 AND ib_isApproved = 'Y' AND ib_pkg_isActive = 1 and ib_ftype_id='SE' and  ib_fstype_id ='T'and ib_fsize_id='" + maxfileSize + "'; ", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public int Saveupdateskudispatchinnerbox(DispatchSKUEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_innerbox_type", Obj.dp_innerbox_type);
            cmd.Parameters.AddWithValue("@dp_innerbox_chart", Obj.dp_innerbox_chart);
            cmd.Parameters.AddWithValue("@dp_request_type", Obj.dp_request_type);
            cmd.Parameters.AddWithValue("@dp_request_id", Obj.dp_request_id);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int SaveSkuSetOuter(DispatchSKUEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_outerbox_type", Obj.dp_outerbox_type);
            cmd.Parameters.AddWithValue("@dp_outerbox_chart", Obj.dp_outerbox_chart);
            cmd.Parameters.AddWithValue("@dp_request_type", Obj.dp_request_type);
            cmd.Parameters.AddWithValue("@dp_request_id", Obj.dp_request_id);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int SaveSkuSetPallet(DispatchSKUEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dp_pallet_type", Obj.dp_pallet_type);
            cmd.Parameters.AddWithValue("@dp_pallet_chart", Obj.dp_pallet_chart);
            cmd.Parameters.AddWithValue("@dp_request_type", Obj.dp_request_type);
            cmd.Parameters.AddWithValue("@dp_request_id", Obj.dp_request_id);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataTable get_ssmskulist(string flag, int rs_request_id)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@rs_request_id", rs_request_id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public void saveSetOtherDetails(PartNumOtherDetailsEntity partEntity)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            try
            {

                SqlCommand checkRecordExistsOrNot = new SqlCommand(" SELECT COUNT(othdt_request_id) FROM tb_partno_otherdtls WHERE othdt_request_id = '" + partEntity.othdt_request_id + "' and othdt_request_type ='" + partEntity.othdt_request_type + "' ", con);

                int recordExist = (int)checkRecordExistsOrNot.ExecuteScalar();

                if (recordExist > 0) //anything different from 1 should be wrong
                {


                    //string insertStmt = "update tb_partno_otherdtls set othdt_prod_cost_onetm='" + partEntity.othdt_prod_cost_onetm
                    //                                        + "' ,othdt_prod_cost_run='" + partEntity.othdt_prod_cost_run
                    //                                        + "' ,othdt_dispatch_onetm='" + partEntity.othdt_dispatch_onetm
                    //                                         + "' ,othdt_dispatch_run='" + partEntity.othdt_dispatch_run
                    //                                         + "' ,othdt_req_startdt='CONVERT(date,partEntity.othdt_req_startdt1,105)
                    //                                          + "' ,othdt_req_enddt='CONVERT(date, " + partEntity.othdt_req_enddt1 + , 105)
                    //                                          +"' ,othdt_approved='" + 'I'
                    //                                          + "' ,othdt_active='" + '1'
                    //                                            + "',othdt_createdate='" + DateTime.Now
                    //                                           + "',othdt_createby='" + "Initial Upload"
                    //                                           + "',othdt_approveby='" + "Initial Upload"
                    //                                        + "' where othdt_request_type = '" + partEntity.othdt_request_type

                    //                                        + "' and othdt_request_id ='" + partEntity.othdt_request_id + "'";

                    //SqlCommand cmd = new SqlCommand(insertStmt, con);
                    string flag = "updateotherredetails";
                    connectionstring = objUtils.getConnString();
                    SqlConnection con1 = new SqlConnection(connectionstring);
                    con1.Open();
                    SqlCommand cmd = new SqlCommand("sp_get_EditDetails_for_ReqNo", con1);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@othdt_prod_cost_onetm", partEntity.othdt_prod_cost_onetm);
                    cmd.Parameters.AddWithValue("@othdt_prod_cost_run", partEntity.othdt_prod_cost_run);
                    cmd.Parameters.AddWithValue("@othdt_dispatch_onetm", partEntity.othdt_dispatch_onetm);

                    cmd.Parameters.AddWithValue("@othdt_dispatch_run", partEntity.othdt_dispatch_run);
                    cmd.Parameters.AddWithValue("@othdt_req_startdt", partEntity.othdt_req_startdt);
                    cmd.Parameters.AddWithValue("@othdt_req_enddt", partEntity.othdt_req_enddt);
                    cmd.Parameters.AddWithValue("@othdt_request_type", partEntity.othdt_request_type);
                    cmd.Parameters.AddWithValue("@othdt_request_id", partEntity.othdt_request_id);
                    cmd.Parameters.AddWithValue("@flag", flag);
                    cmd.ExecuteNonQuery();

                    if (partEntity.othdt_req_enddt_current_sku != null)
                    {
                        string updateStmt = "update tb_partno_otherdtls set othdt_req_enddt='" + partEntity.othdt_req_enddt_current_sku
                                                                + "' where othdt_request_id =(select [othdt_ref_request_id] from [dbo].[tb_partno_otherdtls] where [othdt_request_id]="
                                                                + partEntity.othdt_request_id + ")";
                        SqlCommand cmd1 = new SqlCommand(updateStmt, con);
                        cmd1.ExecuteNonQuery();
                    }
                }
                else

                {

                    string insertStmt = "INSERT INTO tb_partno_otherdtls(othdt_prod_cost_onetm ,othdt_prod_cost_run,othdt_dispatch_onetm,othdt_dispatch_run,othdt_req_startdt,othdt_req_enddt,othdt_request_type,othdt_request_id,othdt_approved,othdt_approveby,othdt_createdate,othdt_createby ,othdt_active,othdt_approvedate) " +
                                 "VALUES(@othdt_prod_cost_onetm ,@othdt_prod_cost_run,@othdt_dispatch_onetm,@othdt_dispatch_run,@othdt_req_startdt,@othdt_req_enddt,@othdt_request_type,@othdt_request_id,@othdt_approved,@othdt_approveby,@othdt_createdate,@othdt_createby ,@othdt_active,@othdt_approvedate)";
                    //string insertStmt = "update tb_dispatch_sku(dp_request_type ,dp_request_id,dp_stamp_type,dp_stamp_chart,dp_isApproved,dp_isActive,dp_createdate,dp_createby,dp_approvedate,dp_approveby) " +
                    //                  "VALUES(@dp_request_type, @dp_request_id,@dp_stamp_type,@dp_stamp_chart,@dp_isApproved,@dp_isActive,@dp_createdate,@dp_createby,@dp_approvedate,@dp_approveby)";

                    SqlCommand cmd = new SqlCommand(insertStmt, con);
                    cmd.Parameters.AddWithValue("@othdt_prod_cost_onetm", partEntity.othdt_prod_cost_onetm);
                    cmd.Parameters.AddWithValue("@othdt_prod_cost_run", partEntity.othdt_prod_cost_run);
                    cmd.Parameters.AddWithValue("@othdt_dispatch_onetm", partEntity.othdt_dispatch_onetm);
                    cmd.Parameters.AddWithValue("@othdt_dispatch_run", partEntity.othdt_dispatch_run);
                    // cmd.ExecuteScalar();@,@,@,@,@,@
                    cmd.Parameters.AddWithValue("@othdt_req_startdt", partEntity.othdt_req_startdt);
                    cmd.Parameters.AddWithValue("@othdt_req_enddt", partEntity.othdt_req_enddt);
                    cmd.Parameters.AddWithValue("@othdt_approveby", "Initial Upload");
                    cmd.Parameters.AddWithValue("@othdt_request_type", partEntity.othdt_request_type);
                    cmd.Parameters.AddWithValue("@othdt_request_id", partEntity.othdt_request_id);
                    cmd.Parameters.AddWithValue("@othdt_approvedate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@othdt_createdate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@othdt_createby", "Initial Upload");
                    cmd.Parameters.AddWithValue("@othdt_approved", "S");
                    cmd.Parameters.AddWithValue("@othdt_active", 1);
                    cmd.ExecuteNonQuery();
                }




            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public int SaveTempPartSku(PartNumMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pn_tmp_reqtype", Obj.pn_tmp_reqtype);
            cmd.Parameters.AddWithValue("@pn_tmp_request_id", Obj.pn_tmp_request_id);
            cmd.Parameters.AddWithValue("@pn_tmp_itemno", Obj.pn_tmp_itemno);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataSet GetSkuDetails(int pr_request_id, string pr_request_type, string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_SKUSelectionMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@rs_request_id", pr_request_id);
            cmd.Parameters.AddWithValue("@rs_request_type", pr_request_type);
            cmd.Parameters.AddWithValue("@flag", flag);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            return ds;
        }

        public string CheckMaxSizeCode(int pn_tmp_request_id, string pn_tmp_reqtype)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select pn_tmp_itemno from tb_temp_partno where pn_tmp_request_id='" + pn_tmp_request_id + "'and pn_tmp_reqtype='" + pn_tmp_reqtype + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string maxsize = "";
            if (DR1.HasRows)
            {
                maxsize = DR1.GetValue(0).ToString();
            }
            con.Close();
            return maxsize;
        }
        public string CheckWrapperSet(int dp_request_id, string dp_request_type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select dp_wrapping_chart from tb_dispatch_sku where dp_request_id='" + dp_request_id + "'and dp_request_type='" + dp_request_type + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string wrapperchart = "";
            if (DR1.HasRows)
            {
                wrapperchart = DR1.GetValue(0).ToString();
            }
            con.Close();
            return wrapperchart;
        }
        public string SetSKUWrapperSet(int rs_request_id, string rs_request_type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select rs_wrapper_chart from tb_set_sku_master where rs_request_id='" + rs_request_id + "'and rs_request_type='" + rs_request_type + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string wrapperchart = "";
            if (DR1.HasRows)
            {
                wrapperchart = DR1.GetValue(0).ToString();
            }
            con.Close();
            return wrapperchart;
        }
        public string SetNewSKUWrapperSet(int rs_request_id, string rs_request_type)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select wm_chart_remarks from tb_wrapper_master where wm_request_id='" + rs_request_id + "'and wm_request_type='" + rs_request_type + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string wrapperchartNew = "";
            if (DR1.HasRows)
            {
                wrapperchartNew = DR1.GetValue(0).ToString();
            }
            con.Close();
            return wrapperchartNew;
        }
        #endregion

        #region Manage Process
        public int saveopreatinDetails(OperationMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@opr_operation_seq", processObj.opr_operation_seq);
            cmd.Parameters.AddWithValue("@opr_operation_name", processObj.opr_operation_name);
            cmd.Parameters.AddWithValue("@opr_operation_restricted", processObj.opr_operation_restricted);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int saveProcessDetails(ProcessMasterDetailsEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ppm_process_code", processObj.ppm_process_code);
            cmd.Parameters.AddWithValue("@ppm_process_seqno", processObj.ppm_process_seqno);
            cmd.Parameters.AddWithValue("@ppm_request_type", processObj.ppm_request_type);
            cmd.Parameters.AddWithValue("@ppm_request_id", processObj.ppm_request_id);
            cmd.Parameters.AddWithValue("@ppm_ftype_code", processObj.ppm_ftype_code);
            cmd.Parameters.AddWithValue("@ppm_fstype_code", processObj.ppm_fstype_code);
            cmd.Parameters.AddWithValue("@ppm_createby", processObj.ppm_createby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataTable getProcesslist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable processtypewiselist(string flag, string process_code, int requestID, string requesttype)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.Parameters.AddWithValue("@ppm_process_code", process_code);
            cmd.Parameters.AddWithValue("@ppm_request_id", requestID);
            cmd.Parameters.AddWithValue("@ppm_request_type", requesttype);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable FileTypeprocesslist(string flag, string process_code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.Parameters.AddWithValue("@ppm_process_code", process_code);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable GetAddApproveProcess(string flag, string process_code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.Parameters.AddWithValue("@ppm_process_code", process_code);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public string processtypename(string pro_process_code)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select ft_ftype_desc from tb_filetype_master where ft_ftype_code='" + pro_process_code + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string Filetypename = DR1.GetValue(0).ToString();
            con.Close();
            return Filetypename;

        }

        public string CheckOprationSqNo(string opr_operation_seq)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select opr_operation_seq from tb_operation_master where opr_operation_seq='" + opr_operation_seq + "'";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string SqNo = "";
            if (DR1.HasRows)
            {
                SqNo = DR1.GetValue(0).ToString();
            }
            con.Close();
            return SqNo;
        }

        public DataSet DispatchoprationList(string flag, string requestId, string requesttype)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
                cmd.Parameters.AddWithValue("@dpm_request_id", requestId);
                cmd.Parameters.AddWithValue("@dpm_request_type", requesttype);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getFileTypeUnitList(string flag, string FileTypeCode)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
                cmd.Parameters.AddWithValue("@val_valuestream_code", FileTypeCode);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public int ChangeProcessStatus(ProcessMasterDetailsEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ppm_request_id", processObj.ppm_request_id);
            cmd.Parameters.AddWithValue("@ppm_request_type", processObj.ppm_request_type);
            cmd.Parameters.AddWithValue("@ppm_remarks", processObj.ppm_remarks);
            cmd.Parameters.AddWithValue("@ppm_isApproved", processObj.ppm_isApproved);
            cmd.Parameters.AddWithValue("@ppm_isActive", processObj.ppm_isActive);
            cmd.Parameters.AddWithValue("@ppm_approveby", processObj.ppm_approveby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataSet processCompletelist(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet getNewDispatchprocess(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getApprovedispatchprocess(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public int saveProcessAssetAllocation(DispatchAssetAllocationEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@daa_unit_code", Obj.daa_unit_code);
            cmd.Parameters.AddWithValue("@daa_assetno", Obj.daa_assetno);
            cmd.Parameters.AddWithValue("@daa_main_valstream", Obj.daa_main_valstream);
            cmd.Parameters.AddWithValue("@daa_request_type", Obj.daa_request_type);
            cmd.Parameters.AddWithValue("@daa_operseq_no", Obj.daa_operseq_no);
            cmd.Parameters.AddWithValue("@daa_request_id", Obj.daa_request_id);
            cmd.Parameters.AddWithValue("@daa_createby", Obj.daa_createby);
            cmd.Parameters.AddWithValue("@daa_approveby", Obj.daa_approveby);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataTable getSelectedprocesslist(string flag, int requestid)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.Parameters.AddWithValue("@daa_request_id", requestid);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public int ColseProcessDetails(ProcessMasterDetailsEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ma_request_type", processObj.ft_request_type);
            cmd.Parameters.AddWithValue("@ma_request_id", processObj.ft_request_id);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int ApproveProcessStatus(OperationMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dpm_process_code", processObj.dpm_process_code);
            cmd.Parameters.AddWithValue("@dpm_request_type", processObj.dpm_request_type);
            cmd.Parameters.AddWithValue("@dpm_request_id", processObj.dpm_request_id);
            cmd.Parameters.AddWithValue("@ma_createby", processObj.ma_createby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int RejectProcessStatus(OperationMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dpm_request_type", processObj.dpm_request_type);
            cmd.Parameters.AddWithValue("@dpm_request_id", processObj.dpm_request_id);
            cmd.Parameters.AddWithValue("@ma_createby", processObj.ma_createby);
            cmd.Parameters.AddWithValue("@ma_remarks", processObj.ma_remarks);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataTable ProcessMasterlist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable GetShowGridprocess(string flag, string partno)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_OprationDetails", con);
            cmd.Parameters.AddWithValue("@pn_part_no", partno);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        #region Manage Equipment


        public DataTable getequipmentlist(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getEquipmentDropdownList(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getEquipmentvalueStream(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        public DataTable getEquipmentAssset(string flag, int equ_id)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@equ_id", equ_id);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        public int UpdateNormsDetails(NormsMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_NormsDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nor_prod_sku", Obj.nor_prod_sku);
            cmd.Parameters.AddWithValue("@nor_output_norms", Obj.nor_output_norms);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
      

        public int saveequipmentDetails(EquipmentMasterEntity eqObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@equ_name", eqObj.equ_name);
            cmd.Parameters.AddWithValue("@equ_type", eqObj.equ_type);
            cmd.Parameters.AddWithValue("@equ_approveby", eqObj.equ_approveby);
            cmd.Parameters.AddWithValue("@equ_createby", eqObj.equ_createby);
            cmd.Parameters.AddWithValue("@flag", eqObj.flag);
            cmd.Parameters.Add("@equ_assetno", SqlDbType.Int).Direction = ParameterDirection.Output;
            int i = cmd.ExecuteNonQuery();
            string id = cmd.Parameters["@equ_assetno"].Value.ToString();

            return i = Convert.ToInt32(id);

        }

        public int SaveAssetAllocation(EquipmentMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@aa_unit_code", Obj.aa_unit_code);
            cmd.Parameters.AddWithValue("@aa_startdate", Obj.aa_startdate);
            cmd.Parameters.AddWithValue("@aa_enddate", Obj.aa_enddate);
            cmd.Parameters.AddWithValue("@aa_assetno", Obj.aa_assetno);
            cmd.Parameters.AddWithValue("@aa_createby", Obj.aa_createby);
            cmd.Parameters.AddWithValue("@aa_approveby", Obj.aa_approveby);
            cmd.Parameters.AddWithValue("@flag", "insertassetallocation");
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int saveAssetValueStream(EquipmentMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@aa_unit_code", Obj.aa_unit_code);
            cmd.Parameters.AddWithValue("@aa_assetno", Obj.aa_assetno);
            cmd.Parameters.AddWithValue("@aa_main_valstream", Obj.aa_main_valstream);
            cmd.Parameters.AddWithValue("@aa_alt_valstream", Obj.aa_alt_valstream);
            cmd.Parameters.AddWithValue("@aa_operseq_no", Obj.aa_operseq_no);
            cmd.Parameters.AddWithValue("@aa_approveby", Obj.aa_approveby);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataTable getunitvaluestreamlist(string flag, string uni_unit_code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@uni_unit_code", uni_unit_code);
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getUnitEquipmentlist(string flag, string uni_unit_code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@uni_unit_code", uni_unit_code);
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getunitequipmenttypelist(string flag, string uni_unit_code, string equ_name)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@uni_unit_code", uni_unit_code);
                cmd.Parameters.AddWithValue("@equ_name", equ_name);
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getUnitEquipmentAssetlist(string flag, string uni_unit_code, string equ_name, string equ_type)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_EquipmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@uni_unit_code", uni_unit_code);
                cmd.Parameters.AddWithValue("@equ_name", equ_name);
                cmd.Parameters.AddWithValue("@equ_type", equ_type);
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public string getAllocateequipmentqty(int aa_operseq_no, string aa_unit_code, string aa_main_valstream)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "SELECT Count( aa_operseq_no)  FROM tb_asset_allocation where aa_isApproved='S' and aa_operseq_no=" + aa_operseq_no + " and aa_unit_code='" + aa_unit_code + "' and aa_main_valstream='" + aa_main_valstream + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string AllocateCount = DR1.GetValue(0).ToString();
            con.Close();
            return AllocateCount;

        }
        public string getAssetNoTotal(string uni_unit_code, string equ_name, string equ_type, string val_valuestream_code, string opr_operation_seq)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select count(b.[equ_assetno]) from [dbo].[tb_asset_allocation] a, [dbo].[tb_equipment_master] b where b.[equ_name] = '"+ equ_name + "' and (b.[equ_type] = '"+ equ_type + "' OR b.[equ_type] is NULL) and a.[aa_assetno] = b.[equ_assetno] and equ_isApproved='Y' ";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string AllocateCount = DR1.GetValue(0).ToString();
            con.Close();
            return AllocateCount;

        }
        public string getAllocateNoTotal(string uni_unit_code, string equ_name, string equ_type, string val_valuestream_code, string opr_operation_seq)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select count(b.[equ_assetno]) from [dbo].[tb_asset_allocation] a, [dbo].[tb_equipment_master] b where a.[aa_unit_code] = '" + uni_unit_code + "'and b.[equ_name] = '" + equ_name + "' and (b.[equ_type] = '" + equ_type + "' OR b.[equ_type] is NULL) and a.[aa_assetno] = b.[equ_assetno] and aa_isApproved='S' and aa_operseq_no IS NOT NULL ";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string AllocateCount = DR1.GetValue(0).ToString();
            con.Close();
            return AllocateCount;

        }
        public string getAvailbleAsset(string uni_unit_code, string equ_name, string equ_type, string val_valuestream_code, string opr_operation_seq)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select count(b.[equ_assetno]) from [dbo].[tb_asset_allocation] a, [dbo].[tb_equipment_master] b where a.[aa_unit_code] = '" + uni_unit_code + "'and b.[equ_name] = '" + equ_name + "' and (b.[equ_type] = '" + equ_type + "' OR b.[equ_type] is NULL) and a.[aa_assetno] = b.[equ_assetno]and aa_operseq_no IS NULL";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string AllocateCount = DR1.GetValue(0).ToString();
            con.Close();
            return AllocateCount;

        }
        #endregion

        #region Manage FileSubType ECN

        public DataTable get_PartNumList(string flag)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_partnumdetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public int SaveOtherRequest(OtherRequestMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_OtherRequestDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@or_request_keydata", Obj.or_request_keydata);
            cmd.Parameters.AddWithValue("@or_request_from", Obj.or_request_from);
            cmd.Parameters.AddWithValue("@or_effective_dt", Obj.or_effective_dt);
            cmd.Parameters.AddWithValue("@or_createby", Obj.or_createby);
            cmd.Parameters.AddWithValue("@or_approveby", Obj.approveby);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            cmd.Parameters.Add("@or_request_id", SqlDbType.Int).Direction = ParameterDirection.Output;
            int i = cmd.ExecuteNonQuery();
            string id = cmd.Parameters["@or_request_id"].Value.ToString();

            return i = Convert.ToInt32(id);
        }
        public int SaveProfileMaster(FileSubTypeEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ECNProfileMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_ftype_code", Obj.pm_ftype_code);
            cmd.Parameters.AddWithValue("@pm_fstype_code", Obj.pm_fstype_code);
            cmd.Parameters.AddWithValue("@pm_file_code", Obj.pm_file_code);
            cmd.Parameters.AddWithValue("@pm_fsize_code", Obj.pm_fsize_code);
            cmd.Parameters.AddWithValue("@pm_dim_parm_catg", Obj.pm_dim_parm_catg);
            cmd.Parameters.AddWithValue("@pm_dim_parm_name", Obj.pm_dim_parm_name);
            cmd.Parameters.AddWithValue("@pm_dim_parm_code", Obj.pm_dim_parm_code);
            cmd.Parameters.AddWithValue("@pm_fstype_desc", Obj.pm_fstype_desc);
            cmd.Parameters.AddWithValue("@pm_dim_parm_scrnvalue", Obj.pm_dim_parm_scrnvalue);
            cmd.Parameters.AddWithValue("@pm_dim_parm_dwgvalue", Obj.pm_dim_parm_dwgvalue);
            cmd.Parameters.AddWithValue("@pm_request_id", Obj.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type", Obj.pm_request_type);
            cmd.Parameters.AddWithValue("@pm_dim_parm_verno", Obj.pm_dim_parm_verno);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int SaveImpactedSku(ImpactedSkQMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ECNImpactskuDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@im_request_type", Obj.im_request_type);
            cmd.Parameters.AddWithValue("@im_request_id", Obj.im_request_id);
            cmd.Parameters.AddWithValue("@im_sku", Obj.im_sku);
            cmd.Parameters.AddWithValue("@im_sku_verno", Obj.im_sku_verno);
            cmd.Parameters.AddWithValue("@im_sku_start_date", Obj.im_sku_start_date);
            cmd.Parameters.AddWithValue("@im_sku_end_date", Obj.im_sku_end_date);
            cmd.Parameters.AddWithValue("@im_pnum_updated", Obj.im_pnum_updated);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public string editSkudate(int RequestID)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select or_effective_dt from tb_other_requests where or_request_id='" + RequestID + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string ReqID = "";
            if (DR1.HasRows)
            {
                ReqID = DR1.GetValue(0).ToString();
            }
            con.Close();

            return ReqID;
        }
        public string MaxVerno(string FileSizecodde, string filecode)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select max(pm_dim_parm_verno) from tb_profile_master where pm_fsize_code='" + FileSizecodde + "' AND pm_file_code='" + filecode + "' AND pm_dim_parm_isApproved!='Y' AND pm_dim_parm_isActive!='1'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string VerNo = "";
            if (DR1.HasRows)
            {
                VerNo = DR1.GetValue(0).ToString();
            }
            con.Close();
            return VerNo;
        }
        public DataTable getnewvaluefilesubtype(string flag, string sizecode, string filecode, string pm_request_type)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_fileSubtype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@filesizecode", sizecode);
            cmd.Parameters.AddWithValue("@filecode", filecode);
            cmd.Parameters.AddWithValue("@pm_request_type", pm_request_type);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable CheckEcnRequestisUpdated(string flag, string sizecode, string filecode, string pm_request_type)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_fileSubtype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@filesizecode", sizecode);
            cmd.Parameters.AddWithValue("@filecode", filecode);
            cmd.Parameters.AddWithValue("@pm_request_type", pm_request_type);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public int ChangeRejectRequest(OtherRequestMasterEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_OtherRequestDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@or_request_id1", Obj.or_request_id);
            cmd.Parameters.AddWithValue("@or_request_type", Obj.or_request_type);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();

            return i;
        }
        public int ChangeRejectRequestProfileMaster(FileSubTypeEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_OtherRequestDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_request_id", Obj.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type", Obj.pm_request_type);
            cmd.Parameters.AddWithValue("@pm_dim_parm_isApproved", Obj.pm_dim_parm_isApproved);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();

            return i;
        }
        public int ChangeRejectRequestoldProfileMaster(FileSubTypeEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_OtherRequestDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pm_request_id", Obj.pm_request_id);
            cmd.Parameters.AddWithValue("@pm_request_type", Obj.pm_request_type);
            cmd.Parameters.AddWithValue("@pm_dim_parm_isApproved", Obj.pm_dim_parm_isApproved);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();

            return i;
        }

        public string requestform(int RequestID)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select or_request_from from tb_other_requests where or_request_id='" + RequestID + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string ReqID = "";
            if (DR1.HasRows)
            {
                ReqID = DR1.GetValue(0).ToString();
            }
            con.Close();

            return ReqID;
        }
        #endregion

        #region Manage Cut Specs ECN

        public DataTable getcutspectdetailvalue(string flag, string tpi_fsize_code, string tpi_ftype_code, string tpi_fstype_code, string tpi_cut_code, string tpi_cut_std_code)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ECNCutSpectMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@tpi_fsize_code", tpi_fsize_code);
            cmd.Parameters.AddWithValue("@tpi_ftype_code", tpi_ftype_code);
            cmd.Parameters.AddWithValue("@tpi_fstype_code", tpi_fstype_code);
            cmd.Parameters.AddWithValue("@tpi_cut_code", tpi_cut_code);
            cmd.Parameters.AddWithValue("@tpi_cut_std_code", tpi_cut_std_code);
            try
            {

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public int SaveTpiMaster(TpiCutStandardsEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ECNCutSpectMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@tpi_fsize_code", Obj.tpi_fsize_code);
            cmd.Parameters.AddWithValue("@tpi_ftype_code", Obj.tpi_ftype_code);
            cmd.Parameters.AddWithValue("@tpi_fstype_code", Obj.tpi_fstype_code);
            cmd.Parameters.AddWithValue("@tpi_cut_code", Obj.tpi_cut_code);
            cmd.Parameters.AddWithValue("@tpi_cut_std_code", Obj.tpi_cut_std_code);
            cmd.Parameters.AddWithValue("@tpi_cut_std_name", Obj.tpi_cut_std_name);
            cmd.Parameters.AddWithValue("@tpi_cut_application", Obj.tpi_cut_application);
            cmd.Parameters.AddWithValue("@tpi_dim_name", Obj.tpi_dim_name);
            cmd.Parameters.AddWithValue("@tpi_dim_code", Obj.tpi_dim_code);
            cmd.Parameters.AddWithValue("@tpi_dim_scrnval", Obj.tpi_dim_scrnval);
            cmd.Parameters.AddWithValue("@tpi_dim_dwgval", Obj.tpi_dim_dwgval);
            cmd.Parameters.AddWithValue("@tpi_request_type", "E");
            cmd.Parameters.AddWithValue("@tpi_request_id", Obj.tpi_request_id);
            cmd.Parameters.AddWithValue("@tpi_verno", Obj.tpi_verno);

            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public DataTable get_CutSpecPartNumList(string flag, string pn_part_no)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_partnumdetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@pn_part_no", pn_part_no);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable CheckEcnCutspectRequestisUpdated(string flag, string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar, string reuestType)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ECNCutSpectMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@tpi_fsize_code", sizecode);
            cmd.Parameters.AddWithValue("@tpi_ftype_code", filecode);
            cmd.Parameters.AddWithValue("@tpi_request_type", reuestType);
            cmd.Parameters.AddWithValue("@tpi_fstype_code", fileSubtypecode);
            cmd.Parameters.AddWithValue("@tpi_cut_code", cuttype);
            cmd.Parameters.AddWithValue("@tpi_cut_std_code", cutstandar);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public DataTable getecnCutSpectnewValue(string flag, string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar, string reuestType)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_ECNCutSpectMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@tpi_fsize_code", sizecode);
            cmd.Parameters.AddWithValue("@tpi_ftype_code", filecode);
            cmd.Parameters.AddWithValue("@tpi_fstype_code", fileSubtypecode);
            cmd.Parameters.AddWithValue("@tpi_cut_code", cuttype);
            cmd.Parameters.AddWithValue("@tpi_cut_std_code", cutstandar);
            cmd.Parameters.AddWithValue("@tpi_request_type", reuestType);
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }




       

        public string MaxVernoCutSpect(string sizecode, string filecode, string fileSubtypecode, string cuttype, string cutstandar)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "select max(tpi_verno) from tb_tpi_master where tpi_fsize_code='" + sizecode + "' AND tpi_ftype_code='" + filecode + "'AND tpi_fstype_code='" + fileSubtypecode + "'AND tpi_cut_code='" + cuttype + "'AND tpi_cut_std_code='" + cutstandar + "' AND tpi_isApproved!='Y' AND tpi_isActive!='1'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string VerNo = "";
            if (DR1.HasRows)
            {
                VerNo = DR1.GetValue(0).ToString();
            }
            con.Close();
            return VerNo;
        }


        public string getStringName(string name,string flag)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            string query = "sp_getName";
            SqlCommand com = new SqlCommand(query, con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@name", name);
            com.Parameters.AddWithValue("@flag", flag);
            SqlDataReader DR1 = com.ExecuteReader();
            DR1.Read();
            string VerNo = "";
            if (DR1.HasRows)
            {
                VerNo = DR1.GetValue(0).ToString();
            }
            con.Close();
            return VerNo;
        }


        public int ChangeRejectRequestCutSpectMaster(TpiCutStandardsEntity Obj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_OtherRequestDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@tpi_request_id", Obj.tpi_request_id);
            cmd.Parameters.AddWithValue("@tpi_request_type", Obj.tpi_request_type);
            cmd.Parameters.AddWithValue("@tpi_isApproved", Obj.tpi_isApproved);
            cmd.Parameters.AddWithValue("@flag", Obj.flag);
            int i = cmd.ExecuteNonQuery();

            return i;
        }


        public DataTable get_CutSpecPartNumDetailList(string flag, int requestID)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_partnumdetails", con);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.Parameters.AddWithValue("@im_request_id", requestID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }


        #endregion

        #region Manage Norms
        public DataSet getnewnormlist(string flag)
        {
            DataSet ds = new DataSet();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataTable getNormsProcesslist(string flag, string RequestId, string Requesrtype, string UnitCode, string VsCode)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
            cmd.Parameters.AddWithValue("@daa_request_id", RequestId);
            cmd.Parameters.AddWithValue("@daa_request_type", Requesrtype);
            cmd.Parameters.AddWithValue("@aa_unit_code", UnitCode);
            cmd.Parameters.AddWithValue("@aa_main_valstream", VsCode);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }

        public int SavePtypenormsvlaue(NormsMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nm_prod_sku", processObj.nm_prod_sku);
            cmd.Parameters.AddWithValue("@nm_valuestream", processObj.nm_valuestream);
            cmd.Parameters.AddWithValue("@nm_unit_code", processObj.nm_unit_code);
            cmd.Parameters.AddWithValue("@nm_opern_name", processObj.nm_opern_name);
            cmd.Parameters.AddWithValue("@nm_equ_name", processObj.nm_equ_name);
            cmd.Parameters.AddWithValue("@nm_equ_type", processObj.nm_equ_type);
            cmd.Parameters.AddWithValue("@nm_norms_value", processObj.nm_norms_value);
            cmd.Parameters.AddWithValue("@nm_request_type", processObj.nm_request_type);
            cmd.Parameters.AddWithValue("@nm_request_id", processObj.nm_request_id);
            cmd.Parameters.AddWithValue("@nm_createby", processObj.nm_createby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        public int SaveDtypenormsvlaue(NormsMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dnm_item_sku", processObj.dnm_item_sku);
            cmd.Parameters.AddWithValue("@dnm_valuestream", processObj.dnm_valuestream);
            cmd.Parameters.AddWithValue("@dnm_unit_code", processObj.dnm_unit_code);
            cmd.Parameters.AddWithValue("@dnm_opern_name", processObj.dnm_opern_name);
            cmd.Parameters.AddWithValue("@dnm_equ_name", processObj.dnm_equ_name);
            cmd.Parameters.AddWithValue("@dnm_equ_type", processObj.dnm_equ_type);
            cmd.Parameters.AddWithValue("@dnm_dnorms_value", processObj.dnm_dnorms_value);
            cmd.Parameters.AddWithValue("@dnm_request_type", processObj.dnm_request_type);
            cmd.Parameters.AddWithValue("@dnm_request_id", processObj.dnm_request_id);
            cmd.Parameters.AddWithValue("@dnm_createby", processObj.dnm_createby);
            cmd.Parameters.AddWithValue("@flag", processObj.dflag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int CloseNormsDetails(NormsMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dnm_request_type", processObj.nm_request_type);
            cmd.Parameters.AddWithValue("@dnm_request_id", processObj.nm_request_id);
            cmd.Parameters.AddWithValue("@dnm_createby", processObj.nm_createby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public int RejectNormsDetails(NormsMasterEntity processObj)
        {
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nm_request_type", processObj.nm_request_type);
            cmd.Parameters.AddWithValue("@nm_request_id", processObj.nm_request_id);
            cmd.Parameters.AddWithValue("@ma_remarks", processObj.ma_remarks);
            cmd.Parameters.AddWithValue("@nm_createby", processObj.nm_createby);
            cmd.Parameters.AddWithValue("@flag", processObj.flag);
            int i = cmd.ExecuteNonQuery();
            return i;
        }
        public DataTable getNormsProcessViewlist(string flag, string RequestId, string Requesrtype, string UnitCode)
        {
            DataTable dt = new DataTable();
            connectionstring = objUtils.getConnString();
            SqlConnection con = new SqlConnection(connectionstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_get_NormsmasterDetails", con);
            cmd.Parameters.AddWithValue("@nm_request_id", RequestId);
            cmd.Parameters.AddWithValue("@nm_request_type", Requesrtype);
            cmd.Parameters.AddWithValue("@nm_unit_code", UnitCode);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                con.Close();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        #endregion

    }
}